-- AlterTable
ALTER TABLE `unit_test_result` ADD COLUMN `class_unit_test_id` INTEGER NULL,
    ADD COLUMN `user_id` CHAR(38) NULL;

-- AddForeignKey
ALTER TABLE `unit_test_result` ADD CONSTRAINT `unit_test_result_user_id_fkey` FOREIGN KEY (`user_id`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `unit_test_result` ADD CONSTRAINT `unit_test_result_class_unit_test_id_fkey` FOREIGN KEY (`class_unit_test_id`) REFERENCES `class_unit_test`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `question_result` ADD CONSTRAINT `question_result_unit_test_section_result_id_fkey` FOREIGN KEY (`unit_test_section_result_id`) REFERENCES `unit_test_section_result`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `unit_test_section_result` ADD CONSTRAINT `unit_test_section_result_unit_test_result_id_fkey` FOREIGN KEY (`unit_test_result_id`) REFERENCES `unit_test_result`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;