-- AlterTable
ALTER TABLE `class_student` ADD COLUMN `class_group_id` INTEGER NULL;

-- CreateTable
CREATE TABLE `class_group` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` CHAR(255) NOT NULL,
    `created_by` CHAR(38) NULL,
    `created_date` DATETIME(0) NULL,
    `updated_by` CHAR(38) NULL,
    `updated_date` DATETIME(0) NULL,
    `status` INTEGER NOT NULL DEFAULT 1,
    `class_id` INTEGER NOT NULL,

    UNIQUE INDEX `id_UNIQUE`(`id`),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- CreateTable
CREATE TABLE `class_unit_test` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `unit_test_id` INTEGER NOT NULL,
    `class_id` INTEGER NOT NULL,
    `class_group_id` INTEGER NULL,
    `student_id` CHAR(38) NULL,
    `start_date` DATETIME(0) NULL,
    `end_date` DATETIME(0) NULL,
    `unit_type` TINYINT NOT NULL DEFAULT 0,
    `status` INTEGER NOT NULL DEFAULT 1,
    `created_by` CHAR(38) NULL,
    `created_date` DATETIME(0) NULL,
    `updated_by` CHAR(38) NULL,
    `updated_date` DATETIME(0) NULL,

    UNIQUE INDEX `id_UNIQUE`(`id`),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- AddForeignKey
ALTER TABLE `class_student` ADD CONSTRAINT `class_student_class_group_id_fkey` FOREIGN KEY (`class_group_id`) REFERENCES `class_group`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `class_group` ADD CONSTRAINT `class_group_class_id_fkey` FOREIGN KEY (`class_id`) REFERENCES `classes`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `class_unit_test` ADD CONSTRAINT `class_unit_test_unit_test_id_fkey` FOREIGN KEY (`unit_test_id`) REFERENCES `unit_test`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `class_unit_test` ADD CONSTRAINT `class_unit_test_class_id_fkey` FOREIGN KEY (`class_id`) REFERENCES `classes`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `class_unit_test` ADD CONSTRAINT `class_unit_test_class_group_id_fkey` FOREIGN KEY (`class_group_id`) REFERENCES `class_group`(`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE `class_unit_test` ADD CONSTRAINT `class_unit_test_student_id_fkey` FOREIGN KEY (`student_id`) REFERENCES `user`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

