-- AlterTable
ALTER TABLE `order` ADD COLUMN `sale_item_id` INTEGER NULL;

-- CreateTable
CREATE TABLE `sale_item` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(50) NOT NULL,
    `quantity` INTEGER NOT NULL,
    `exchange_price` INTEGER NOT NULL,
    `discount_price` INTEGER DEFAULT NULL,
    `is_on_sale` BIT(1) DEFAULT NULL,
    `is_flash_sale` BIT(1) DEFAULT NULL,
    `day_expire` INTEGER DEFAULT NULL,
    `item_id` INTEGER NOT NULL,
    `item_type` VARCHAR(38) NOT NULL,
    `created_by` CHAR(38) DEFAULT NULL,
    `created_date` DATETIME(0) DEFAULT NULL,
    `updated_by` CHAR(38) DEFAULT NULL,
    `updated_date` DATETIME(0) DEFAULT NULL,
    `description` TEXT DEFAULT NULL,
    `image` VARCHAR(255) DEFAULT NULL,
    `payment_unit` INTEGER DEFAULT NULL,
    `status` INTEGER NOT NULL DEFAULT 1,

    UNIQUE INDEX `id_UNIQUE`(`id`),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- AddForeignKey
ALTER TABLE `order` ADD CONSTRAINT `order_sale_item_id_fkey` FOREIGN KEY (`sale_item_id`) REFERENCES `sale_item`(`id`) ON DELETE SET NULL ON UPDATE CASCADE;

