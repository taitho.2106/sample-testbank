-- DropForeignKey
ALTER TABLE `token` DROP FOREIGN KEY `FKl10xjn274m2rkxo54knt2xqvy`;

-- DropIndex
DROP INDEX `UKobd564xomky0vr7eoh8qmurqr` ON `user_role`;

-- AlterTable
ALTER TABLE `class_student` ALTER COLUMN `student_code` DROP DEFAULT;

-- AlterTable
ALTER TABLE `order` MODIFY `actual_price` BIGINT NOT NULL;

-- AlterTable
ALTER TABLE `unit_test` ALTER COLUMN `privacy` DROP DEFAULT,
    ALTER COLUMN `is_international_exam` DROP DEFAULT;

-- AlterTable
ALTER TABLE `user` DROP COLUMN `source`,
    DROP COLUMN `status`,
    ALTER COLUMN `id` DROP DEFAULT;

-- AlterTable
ALTER TABLE `user_role` DROP COLUMN `code`;

-- DropTable
DROP TABLE `hibernate_sequence`;

-- DropTable
DROP TABLE `token`;

-- RenameIndex
ALTER TABLE `voucher_history` RENAME INDEX `order_id_UNIQUE` TO `voucher_history_order_id_key`;

