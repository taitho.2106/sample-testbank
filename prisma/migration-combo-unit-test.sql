-- CreateTable
CREATE TABLE `combo_unit_test` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `name` VARCHAR(100) NOT NULL,
    `description` TEXT NULL,
    `status` TINYINT NOT NULL DEFAULT 1,
    `limit` TINYINT NOT NULL,
    `code` VARCHAR(20) NOT NULL,
    `unit_test_types` VARCHAR(255) NULL,
    `extra` VARCHAR(255) NULL,
    `created_by` CHAR(38) NULL,
    `created_date` DATETIME(0) NULL,
    `updated_by` CHAR(38) NULL,
    `updated_date` DATETIME(0) NULL,

    UNIQUE INDEX `id_UNIQUE`(`id`),
    PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
