import React, { useMemo } from 'react'

import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
ChartJS.register(ArcElement, Tooltip, Legend, ChartDataLabels);
import classNames from 'classnames'
import { Doughnut } from 'react-chartjs-2'

import SkeletonLoading from '@/componentsV2/Common/SkeletonLoading';
import useTranslation from '@/hooks/useTranslation';
import { formatNumber } from '@/utils/index'

import Table from "../../Main/Table"
import { pluginDataTable } from '../../Main/util'
import { ABILITY_DETAILS } from "../TeacherData/data"
import styles from "./index.module.scss"
import NoDataPieChart from "./NoDataPieChart"

const sumValues = (obj) => Object.values(obj).reduce((a, b) => (a + b), 0);

const RankingChartAndTable = ({ className, data, loading }) => {
    const { t } = useTranslation()
    const options = {
        responsive: true,
        plugins: {
            legend: false,
            datalabels: pluginDataTable,
        },
        cutout: 0,
    }

    const resultCountRating = {
        1: 0,
        2: 0,
        3: 0,
        4: 0,
        5: 0,
    }

    const orders = useMemo(() => {
        const updatedOrders = [];
        if (data?.detail) {
            data.detail.forEach((item) => {
                const testResult = item.testResult;
                const correctRate =
                    testResult && Object.keys(testResult).length > 0
                        ? sumValues(testResult) / Object.keys(testResult).length
                        : null;
                item.correctRate = correctRate;

                if (correctRate === null) {
                    item.rating = null;
                } else if (correctRate >= 0.8) {
                    item.rating = 5;
                    resultCountRating[5]++;
                } else if (correctRate >= 0.65) {
                    item.rating = 4;
                    resultCountRating[4]++;
                } else if (correctRate >= 0.5) {
                    item.rating = 3;
                    resultCountRating[3]++;
                } else if (correctRate >= 0.35) {
                    item.rating = 2;
                    resultCountRating[2]++;
                } else {
                    item.rating = 1;
                    resultCountRating[1]++;
                }
            });
            updatedOrders.push(
                ...Object.keys(resultCountRating).map((key) => {
                    const abilityId = Number(key);
                    const count = resultCountRating[key];
                    const abilityDetails = ABILITY_DETAILS.find(
                        (ability) => ability.id === abilityId
                    );

                    return {
                        abilityId,
                        count,
                        labelName: abilityDetails?.labelName || '',
                        color: abilityDetails?.color || '',
                        description: abilityDetails?.description || '',
                    };
                })
            );
        }
        return updatedOrders;
    }, [
        data,
        resultCountRating
    ]);

    const dataCount = useMemo(() => {
        let labels = [], datasets = [];
        const students = orders.filter(el => !!el.count).reverse();
        if (orders) {
            labels = students.map(el => el.labelName);
            datasets.push({
                label: t("student"),
                data: students.map(el => el.count),
                backgroundColor: students.map(el => el.color)
            });
        }
        return { labels, datasets }
    }, [orders]);

    const totalCount = useMemo(() => {
        if (orders.every(item => item.count !== null)) {
            return orders.reduce((total, item) => total + item.count, 0);
        }
        return null;
    }, [orders]);

    if (loading) {
        return (
            <div className={classNames(styles.classification)}>
                <div className={styles.chart}>
                    <SkeletonLoading className={styles.wrapperSkeletonCustom}>
                        <div className={classNames(styles.chartSkeleton)}></div>
                    </SkeletonLoading>
                    <div className={styles.labels}>
                        {ABILITY_DETAILS.map((labelItem, index) => (
                            <div className={styles.labelItem} key={index}>
                                <div
                                    className={styles.itemColor}
                                    style={{
                                        backgroundColor: labelItem.color
                                    }}
                                />
                                {t("dashboard")[labelItem.labelName]}
                            </div>
                        ))}
                    </div>
                </div>
                <div className={styles.details}>
                    <Table
                        columns={[
                            {
                                title: t("dashboard")["ranked"],
                                dataIndex: 'labelName',
                                render: (rowData, colData) =>
                                    <div className={styles.abilityCell}>
                                        <span className={styles.ability}>{t("dashboard")[colData]}</span>
                                        <span className={styles.detail}>{rowData.description}</span>
                                    </div>
                            },
                            {
                                title: t("dashboard")["students-quantity"],
                                render: () =>
                                    <SkeletonLoading className={styles.skeletonCustom}>
                                        <div className={styles.cellSkeleton}></div>
                                    </SkeletonLoading>
                            },

                        ]}
                        data={ABILITY_DETAILS}
                        footer={
                            <>
                                <td>{t("dashboard")["total"]}</td>
                                <td>
                                    <SkeletonLoading className={styles.skeletonCustom}>
                                        <div className={styles.cellSkeleton}></div>
                                    </SkeletonLoading>
                                </td>
                            </>
                        }
                    />
                </div>
            </div>
        )
    }

    if (!loading) {
        return (
            <div className={classNames(styles.classification, className)}>
                <div className={styles.chart}>
                    <div className={styles.item}>
                        {totalCount ? (
                            <div className={styles.chartContainer}>
                                <Doughnut
                                    data={dataCount}
                                    options={options}
                                />
                            </div>
                        ) : (
                            <NoDataPieChart />
                        )}
                    </div>
                    <div className={styles.labels}>
                        {ABILITY_DETAILS.map((labelItem, index) => (
                            <div className={styles.labelItem} key={index}>
                                <div
                                    className={styles.itemColor}
                                    style={{
                                        backgroundColor: labelItem.color
                                    }}
                                />
                                {t("dashboard")[labelItem.labelName]}
                            </div>
                        ))}
                    </div>
                </div>
                {!!orders?.length && !(orders.every(item => item.count === 0)) ? (
                    <div className={styles.details}>
                        <Table
                            columns={[
                                {
                                    title: t("dashboard")["ranked"],
                                    dataIndex: 'labelName',
                                    render: (rowData, colData) =>
                                        <div className={styles.abilityCell}>
                                            <span className={styles.ability}>{t("dashboard")[colData]}</span>
                                            <span className={styles.detail}>{rowData.description}</span>
                                        </div>
                                },
                                {
                                    title: t("dashboard")["students-quantity"],
                                    dataIndex: 'abilityId',
                                    render: (order) => order.count != null ? formatNumber(order.count, 0) : "---",
                                },
                            ]}
                            data={orders.slice().reverse()}
                            footer={
                                <>
                                    <td>{t("dashboard")["total"]}</td>
                                    <td>{totalCount != null ? formatNumber(totalCount, 0) : "---"}</td>
                                </>
                            }
                        />
                    </div>
                ) : (
                    <div className={styles.details}>
                        <Table
                            columns={[
                                {
                                    title: t("dashboard")["ranked"],
                                    dataIndex: 'labelName',
                                    render: (rowData, colData) =>
                                        <div className={styles.abilityCell}>
                                            <span className={styles.ability}>{t("dashboard")[colData]}</span>
                                            <span className={styles.detail}>{rowData.description}</span>
                                        </div>
                                },
                                {
                                    title: t("dashboard")["students-quantity"],
                                    render: () => "---",
                                },
                            ]}
                            data={ABILITY_DETAILS}
                            footer={
                                <>
                                    <td>{t("dashboard")["total"]}</td>
                                    <td>{"---"}</td>
                                </>
                            }
                        />
                    </div>
                )}
            </div>
        )
    }
}

export default RankingChartAndTable