

import React from 'react'

import useTranslation from '@/hooks/useTranslation';

import styles from './NoDataPieChart.module.scss'

const NoDataPieChart = () => {
    const { t } = useTranslation()
    return (
        <div className={styles.noData}>
            <span>{t("no-field-data")["empty-title"]}</span>
        </div>
    );
}

export default NoDataPieChart