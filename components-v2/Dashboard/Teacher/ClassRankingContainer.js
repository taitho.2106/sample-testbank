import React, { useEffect, useState } from 'react'

import Link from "next/link";

import IconArrow from '@/assets/icons/back-arrow.svg';
import CircleLoading from '@/componentsV2/Common/Controls/Loading';
import useGrade from '@/hooks/useGrade';
import useTranslation from '@/hooks/useTranslation';
import { paths } from '@/interfaces/constants';
import { delayResult } from '@/utils/index';
import { callApiConfigError } from 'api/utils';

import SelectPicker from '../Main/Common/SelectPicker';
import { UNIT_TYPE_OPTIONS } from "../Teacher/TeacherData/data"
import styles from "./ClassRankingContainer.module.scss"
import ButtonResetAll from './Common/ButtonResetAll';
import HorizontalBarChart from "./HorizontalBarChart"

const ClassRankingContainer = () => {
    const { t } = useTranslation()
    const [loading, setLoading] = useState(true)
    const [unitTypeSelected, setUnitTypeSelected] = useState(-1);
    const [gradeSelected, setGradeSelected] = useState('')
    const [resultRanking, setResultRanking] = useState([])

    const { getGrade } = useGrade()
    const gradeList = getGrade.filter(item => /^G[0-9]{1,2}$/.test(`${item.code}`)) || []
    const gradeOptions = gradeList.map(({ code, display }) => ({
        value: code,
        label: display,
    }))

    const fetchResultRanking = async () => {
        const body = {}
        if (gradeSelected) {
            body.grade = gradeSelected
        }
        if (unitTypeSelected != -1) {
            body.unit_type = unitTypeSelected
        }
        const response = await delayResult(callApiConfigError(
            `/api/statistic/result-ranking/class`,
            'POST',
            'token',
            body
        ), 400)
        setLoading(false)
        if (response.code === 200) {
            setResultRanking(response?.data)
        }
    }

    useEffect(() => {
        fetchResultRanking()
    }, [
        gradeSelected,
        unitTypeSelected,
    ])

    const handleReset = () => {
        setGradeSelected('')
        setUnitTypeSelected(-1)
    }

    if (loading) {
        return (
            <div className={styles.classRanking}>
                <div className={styles.loadingContainer}>
                    <CircleLoading color="#B5B5B6" />
                </div>
            </div>
        )
    }

    if (!loading) {
        return (
            <div className={styles.classRanking}>
                <div className={styles.header}>
                    <div className={styles.titleSection}>
                        <span className={styles.title}>
                            {t("dashboard")["class-ranking"]}
                        </span>
                        <span className={styles.summary}>
                            {t("dashboard")["class-ranking-summary"]}
                        </span>
                    </div>
                    <div className={styles.filterSection}>
                        <SelectPicker
                            className={styles.selectRanking}
                            placeholder={t("common")["grade"]}
                            data={gradeOptions}
                            value={gradeSelected}
                            onChange={setGradeSelected}
                        />
                        <SelectPicker
                            className={styles.selectRanking}
                            data={UNIT_TYPE_OPTIONS.map(item => ({...item,label: t('unit-type')[item.label]}))}
                            value={unitTypeSelected}
                            onChange={setUnitTypeSelected}
                        />
                        <ButtonResetAll
                            onReset={handleReset}
                            disabledState={unitTypeSelected === -1 && !gradeSelected}
                        />
                    </div>
                </div>
                <div className={styles.barChartSection}>
                    <HorizontalBarChart data={resultRanking} />
                </div>
                <div className={styles.actions}>
                    <Link href={paths.dashboardClassRanking}>
                        <a className={styles.viewClassDetails}>
                            {t("dashboard")["view-class-detailed"]}
                            <IconArrow />
                        </a>
                    </Link>
                </div>
            </div>
        )

    }
}

export default ClassRankingContainer