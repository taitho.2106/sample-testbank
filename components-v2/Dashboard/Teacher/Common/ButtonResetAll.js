

import React from 'react'

import classNames from 'classnames';
import { Button } from 'rsuite'

import IconReset from '@/assets/icons/reset-icon.svg';
import useTranslation from '@/hooks/useTranslation';

import styles from "./ButtonResetAll.module.scss";

const ButtonResetAll = ({ onReset, disabledState = false }) => {
    const { t } = useTranslation()
    return (
        <Button
            appearance="ghost"
            className={classNames(styles.btnReset, {
                [styles.disabled]: disabledState
            })}
            onClick={onReset}
            disabled={disabledState}
        >
            {t("common")["reset"]}
            <IconReset />
        </Button>
    )
}

export default ButtonResetAll