import React, { useEffect, useState } from 'react';

import dayjs from 'dayjs';

import useTranslation from '@/hooks/useTranslation';

import DateSelectPicker from '../../Main/Common/DateSelectPicker'
import SelectPicker from '../../Main/Common/SelectPicker'
import ButtonResetAll from './ButtonResetAll';
import styles from './FrequencyFilterContainer.module.scss';

const FrequencyFilterContainer = ({
    children,
    gradeOptions,
    dataClassOfTeacher,
    dateRangeProps,
    gradeProps,
    classProps,
    onReset,
    disabledState,
}) => {
    const {t} = useTranslation()
    const [selectedDateStart, setSelectedDateStart] = useState(dateRangeProps.value[0]);
    const [selectedDateEnd, setSelectedDateEnd] = useState(dateRangeProps.value[1]);
    const [selectDate, setSelectDate] = useState(false); // check đã chọn ngày trong DateSelectPicker

    const handleDateStart = (timestamp) => {
        const defaultDateStart = dayjs(timestamp).startOf('date').toDate()
        setSelectedDateStart(defaultDateStart)
        dateRangeProps.onChange([defaultDateStart, selectedDateEnd])
        setSelectDate(true)
    }

    const handleDateEnd = (timestamp) => {
        const defaultDateEnd = dayjs(timestamp).endOf('date').toDate()
        setSelectedDateEnd(defaultDateEnd)
        dateRangeProps.onChange([selectedDateStart, defaultDateEnd])
        setSelectDate(true)
    }

    const currentDate = new Date();

    const disabledDateStart = (date) => {
        const isAfterToday = dayjs(date).isAfter(currentDate, 'date')
        const isAfterEnd = dayjs(date).isAfter(selectedDateEnd)
        return isAfterEnd || isAfterToday
    }

    const disabledDateEnd = (date) => {
        const isAfterToday = dayjs(date).isAfter(currentDate, 'date')
        const isBeforeStart = dayjs(date).isBefore(selectedDateStart)
        return isBeforeStart || isAfterToday
    }

    useEffect(() => {
        setSelectedDateStart(dateRangeProps.value[0])
        setSelectedDateEnd(dateRangeProps.value[1])
    }, [dateRangeProps.value])

    const handleReset = () => {
        onReset()
        setSelectDate(false)
    }

    return (
        <div className={styles.filterContainer}>
            {children}
            <DateSelectPicker
                style={{
                    width: '127px'
                }}
                value={selectedDateStart}
                onChange={handleDateStart}
                disabledDate={disabledDateStart}
            />
            <DateSelectPicker
                style={{
                    width: '127px'
                }}
                value={selectedDateEnd}
                onChange={handleDateEnd}
                disabledDate={disabledDateEnd}
            />

            <SelectPicker
                className={styles.frequencySelect}
                placeholder={t("common")["grade"]}
                menuClassName={styles.menuCustom}
                data={gradeOptions}
                value={gradeProps.value}
                onChange={(value) => gradeProps.onChange(value)}
            />
            <SelectPicker
                className={styles.frequencySelect}
                placeholder={t("class")}
                menuClassName={styles.menuCustom}
                data={dataClassOfTeacher}
                value={classProps.value}
                onChange={(value) => classProps.onChange(value)}
            />
            {!!onReset && (
                <ButtonResetAll
                    onReset={handleReset}
                    disabledState={disabledState && !selectDate}
                />
            )}
        </div>
    );
};

export default FrequencyFilterContainer;