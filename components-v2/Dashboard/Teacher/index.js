
import React from 'react'

import AssignedFrequencyContainer from "./AssignedFrequencyContainer"
import ClassRankingContainer from "./ClassRankingContainer"
import styles from "./DashboardTeacher.module.scss"
import ManagementFieldsContainer from "./ManagementFieldsContainer"

const DashboardTeacher = () => {
    return (
        <div className={styles.dashboardTeacher}>
            <ManagementFieldsContainer />

            <ClassRankingContainer />

            <AssignedFrequencyContainer />
        </div>
    )
}

export default DashboardTeacher