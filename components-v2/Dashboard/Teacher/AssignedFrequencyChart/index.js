import React, { useMemo } from 'react'

import {
    CategoryScale, Chart as ChartJS, Legend, LinearScale, LineElement, PointElement, Title, Tooltip
} from 'chart.js';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

import dayjs from 'dayjs';
import { Line } from 'react-chartjs-2'

import { DATE_DISPLAY_FORMAT } from '@/constants/index';
import useTranslation from '@/hooks/useTranslation';
import { UNIT_TYPE } from '@/interfaces/struct';

import styles from "./index.module.scss"

const convertLineChartData = (dataArray, unit_type_code) => {
    return dataArray.map((item) => {
        // duyệt theo phân loại đề [Luyện tập, Kiểm tra]
        const countWithUnitType0 = item.count.find((countItem) => countItem.unit_type === unit_type_code);
        return countWithUnitType0 ? countWithUnitType0.value : null;
    });
};

const AssignedFrequencyChart = ({ data }) => {
    const { t } = useTranslation()
    const options = {
        responsive: true,
        datasets: {
            line: {
                tension: 0.4,
            },
            borderWidth: 1,
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            datalabels: false
        },
        maintainAspectRatio: false,
        layout: {
            padding: {
                left: 15,
            }
        },
        scales: {
            x: {
                grid: {
                    display: false
                },
                ticks: {
                    autoSkip: data.length > 30, // display or skip labels
                    maxRotation: 0, // To prevent rotation of labels
                }
            },
            y: {
                beginAtZero: true,
                suggestedMin: 0,
                suggestedMax: 5,
                stepSize: 1,
                ticks: {
                    autoSkip: true,
                    precision: 0,
                },
            }
        },
    }

    const lineChartData = useMemo(() => {
        let labels = [], datasets = [];
        if (data) {
            labels = data.map((item) => dayjs(item.start_date).format(DATE_DISPLAY_FORMAT).split('/').splice(0, 2).join('/'));
            datasets.push({
                label: t("dashboard")["practice-test"],
                data: convertLineChartData(data, UNIT_TYPE[0].code),
                borderColor: '#F79945',
                backgroundColor: '#F79945',
            });
            datasets.push({
                label: t("dashboard")["exam-test"],
                data: convertLineChartData(data, UNIT_TYPE[1].code),
                borderColor: '#1890FF',
                backgroundColor: '#1890FF',
            });
        }

        return {
            labels, datasets
        }
    }, [data]);

    return (
        <div className={styles.container}>
            <div className={styles.yAxisLabel}>
                {t("dashboard")["total-unitTest"]}
            </div>
            <div className={styles.chart}>
                <Line
                    options={options}
                    data={lineChartData}
                />
            </div>
        </div>
    )
}

export default AssignedFrequencyChart