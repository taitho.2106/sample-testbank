import React, { useMemo } from 'react'

import SkeletonLoading from '@/componentsV2/Common/SkeletonLoading'
import useTranslation from '@/hooks/useTranslation'

import SelectPicker from '../../Main/Common/SelectPicker'
import Table from '../../Main/Table'
import ButtonResetAll from '../Common/ButtonResetAll'
import RankingChartAndTable from '../RankingChartAndTable'
import { UNIT_TYPE_OPTIONS, mapStudentRating } from "../TeacherData/data"
import styles from "./RankByAssignedTest.module.scss"

const RankByAssignedTest = ({
    loading,
    data,
    unitTypeProps,
    onReset,
    disabledState,
}) => {
    const { t } = useTranslation()
    const skeletonTableRow = 8
    const studentsDetails = useMemo(() => {
        if (data?.detail) {
            // Check if testResult null or empty object and update the correctRate, rating properties accordingly
            const updatedStudentsDetails = data?.detail.map(student => ({
                ...student,
                correctRate: student.testResult && Object.keys(student.testResult).length > 0 ? student.correctRate : null,
                rating: student.testResult && Object.keys(student.testResult).length > 0 ? student.rating : null,
            }));

            return updatedStudentsDetails;
        } else {
            return [];
        }
    }, [data]);

    return (
        <div className={styles.rankingByRule}>
            <div className={styles.header}>
                <div className={styles.titleSection}>
                    <span className={styles.title}>
                        {t("dashboard")["student-classification"]}
                    </span>
                    <span className={styles.summary}>
                        {t("dashboard")["student-classification-summary"]}
                    </span>
                </div>
                <div className={styles.filterSection}>
                    <SelectPicker
                        className={styles.selectExamType}
                        data={UNIT_TYPE_OPTIONS.map(item => ({...item,label: t('unit-type')[item.label]}))}
                        value={unitTypeProps.value}
                        onChange={(value) => unitTypeProps.onChange(value)}
                    />
                    <ButtonResetAll
                        onReset={onReset}
                        disabledState={disabledState}
                    />
                </div>
            </div>
            <div className={styles.body}>
                <RankingChartAndTable data={data} loading={loading} />
            </div>
            {loading && (
                <div className={styles.studentsList}>
                    <Table
                        columns={[
                            {
                                title: t("dashboard")["ranked-place"],
                                dataIndex: '',
                                render: () =>
                                    <SkeletonLoading className={styles.skeletonCustom}>
                                        <div className={styles.cellSkeleton}></div>
                                    </SkeletonLoading>
                            },
                            {
                                title: t("common")["student-name"],
                                render: () =>
                                    <SkeletonLoading className={styles.skeletonCustom}>
                                        <div className={styles.cellSkeleton}></div>
                                    </SkeletonLoading>
                            },
                            {
                                title: t("phone"),
                                render: () =>
                                    <SkeletonLoading className={styles.skeletonCustom}>
                                        <div className={styles.cellSkeleton}></div>
                                    </SkeletonLoading>
                            },
                            {
                                title: t("dashboard")["ranked"],
                                render: () =>
                                    <SkeletonLoading className={styles.skeletonCustom}>
                                        <div className={styles.cellSkeleton}></div>
                                    </SkeletonLoading>
                            },
                            {
                                title: t("dashboard")["accuracy-rate"],
                                render: () =>
                                    <SkeletonLoading className={styles.skeletonCustom}>
                                        <div className={styles.cellSkeleton}></div>
                                    </SkeletonLoading>
                            },
                        ]}
                        data={Array.from({ length: skeletonTableRow }, () => ({}))}
                    />
                </div>
            )}
            {!loading && (
                <div className={styles.studentsList}>
                    <Table
                        columns={[
                            { title: t("dashboard")["ranked-place"], dataIndex: 'rank' },
                            { title: t("common")["student-name"], dataIndex: 'name' },
                            {
                                title: t("phone"),
                                dataIndex: 'phone',
                                render: (_, colData) => <span className={styles.normalText}>{colData || '---'}</span>
                            },
                            {
                                title: t("dashboard")["ranked"],
                                dataIndex: 'rating',
                                render: (_, colData) => <span className={styles.normalText}>{t("dashboard")[mapStudentRating[colData]] || '---'}</span>
                            },
                            {
                                title: t("dashboard")["accuracy-rate"],
                                dataIndex: 'correctRate',
                                render: (_, colData) => <span className={styles.normalText}>{colData != null ? (colData * 100).toFixed(1) + '%' : '---'}</span>
                            },
                        ]}
                        data={studentsDetails}
                    />
                    {!studentsDetails?.length &&
                        <div className={styles.noDataRow}>
                            <span>{t("chosen-students")["empty"]}</span>
                        </div>
                    }
                </div>
            )}
        </div>
    )
}

export default RankByAssignedTest

// mapStudentRating[colData]