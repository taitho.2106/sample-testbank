import React, { useEffect, useState } from 'react'

import qs from "qs"
import { Button } from 'rsuite'

import IconPieChart from "@/assets/icons/ic-pie-chart.svg"
import CircleLoading from '@/componentsV2/Common/Controls/Loading'
import Tabs from '@/componentsV2/Common/Tabs'
import useGrade from '@/hooks/useGrade'
import useTranslation from '@/hooks/useTranslation'
import { delayResult } from '@/utils/index'
import { paths } from 'api/paths'
import { callApiConfigError } from 'api/utils'
import ExclamationIcon from 'assets/icons/exclamation-icon.svg'

import SelectPicker from '../../Main/Common/SelectPicker'
import { STATUS_RANKING_RULE } from "../TeacherData/data"
import styles from "./ClassRankingDetail.module.scss"
import RankByAssignedTest from "./RankByAssignedTest"

const ClassRankingDetail = () => {
    const { t } = useTranslation()
    const ClassRuleTab = [
        {
            code: STATUS_RANKING_RULE.ONLY_CLASS_TARGET,
            groupName: t(`${t("dashboard")["assign-whole-class-tab"]}`),
            icon: <IconPieChart />,
            tooltipIcon: <ExclamationIcon />,
            tooltipText: t(`${t("dashboard")["tooltip-assign-whole-class"]}`)
        },
        {
            code: STATUS_RANKING_RULE.ALL_TARGET,
            groupName: t(`${t("dashboard")["assign-all-tab"]}`),
            icon: <IconPieChart />,
            tooltipIcon: <ExclamationIcon />,
            tooltipText: t(`${t("dashboard")["tooltip-assign-all"]}`)
    
        },
    ]
    const [tabIndex, setTabIndex] = useState(STATUS_RANKING_RULE.ONLY_CLASS_TARGET);
    const { getGrade } = useGrade();

    const [noClassSelect, setNoClassSelect] = useState(true)
    const [loadingPage, setLoadingPage] = useState(true)
    const [loading, setLoading] = useState(false);
    const [gradeSelected, setGradeSelected] = useState('');
    const [dataClassOfTeacher, setDataClassOfTeacher] = useState([]);
    const [classSelected, setClassSelected] = useState('');
    const [unitTypeSelected, setUnitTypeSelected] = useState(-1);
    const [detailsResult, setDetailsResult] = useState([]);

    const gradeList = getGrade.filter(item => /^G[0-9]{1,2}$/.test(`${item.code}`)) || [];
    const gradeOptions = gradeList.map(({ code, display }) => ({
        value: code,
        label: display,
    }));

    useEffect(() => {
        const fetcherClassIdByGrade = async () => {
            const queryString = qs.stringify({
                grade: gradeSelected || undefined,
                size: 100
            });
            const response = await callApiConfigError(`${paths.api_teacher_classes}?${queryString}`);
            setLoadingPage(false)
            if (response.code === 200) {
                const dataOption = response?.data?.content?.map((item) => ({
                    value: item.id.toString(),
                    label: item.name,
                }));
                setDataClassOfTeacher(dataOption);
            }
        };
        fetcherClassIdByGrade();
    }, [gradeSelected]);

    const fetcher = async (body) => {
        if (!classSelected) return;
        setNoClassSelect(false)
        setLoading(true);
        const response = await delayResult(callApiConfigError(
            `/api/statistic/result-ranking/student`,
            'POST',
            'token',
            body
        ), 400);
        setLoading(false);
        if (response.code === 200) {
            setDetailsResult(response?.data);
        }
    };

    const handleGradeSelected = (value) => {
        setGradeSelected(value);
        setClassSelected('');
    };

    const handleApply = () => {
        setNoClassSelect(true)
        setTabIndex(STATUS_RANKING_RULE.ONLY_CLASS_TARGET)
        setUnitTypeSelected(-1)
        const body = {
            get_all: true,
            class_id: Number(classSelected)
        };
        fetcher(body)
    };

    const handleTabSelect = (value) => {
        setTabIndex(value);
        setUnitTypeSelected(-1)
        const body = {
            class_id: Number(classSelected),
            get_all: value === STATUS_RANKING_RULE.ONLY_CLASS_TARGET,  // get all: true ~ tỷ lệ chính xác kết quả những bài thi đã kết thúc, và chỉ tính trên những đề thi giao cho cả lớp
        };
        fetcher(body)
    }

    const handleUnitTypeSelect = (value) => {
        setUnitTypeSelected(value);
        const body = {
            class_id: Number(classSelected),
            get_all: tabIndex === STATUS_RANKING_RULE.ONLY_CLASS_TARGET,
            unit_type: value !== -1 ? value : undefined,
        };
        fetcher(body)
    };

    const handleResetAction = () => {
        setUnitTypeSelected(-1)
        const body = {
            class_id: Number(classSelected),
            get_all: tabIndex === STATUS_RANKING_RULE.ONLY_CLASS_TARGET,
        };
        fetcher(body)
    }

    const renderUiWithTab = () => {
        return (
            <RankByAssignedTest
                loading={loading}
                data={detailsResult}
                unitTypeProps={{
                    value: unitTypeSelected,
                    onChange: (value) => handleUnitTypeSelect(value),
                }}
                onReset={() => handleResetAction()}
                disabledState={unitTypeSelected === -1}
            />
        );
    };

    return (
        <div className={styles.classRankingContainer}>
            <div className={styles.header}>
                <span className={styles.title}>{t("dashboard")["select-class-to-view"]}</span>
                <div className={styles.actionFilter}>
                    <SelectPicker
                        className={styles.rankingSelect}
                        placeholder={t("common")["grade"]}
                        data={gradeOptions}
                        value={gradeSelected}
                        onChange={handleGradeSelected}
                    />
                    <SelectPicker
                        className={styles.rankingSelect}
                        menuClassName={styles.menuCustom}
                        placeholder={t("class")}
                        data={dataClassOfTeacher}
                        value={classSelected}
                        onChange={setClassSelected}
                    />
                    <Button
                        className={styles.applyBtn}
                        onClick={handleApply}
                    >
                        {t("dashboard")["view-btn"]}
                    </Button>
                </div>
            </div>
            {loadingPage && (
                <div className={styles.noFieldSelect}>
                    <CircleLoading color="#B5B5B6" />
                </div>
            )}
            {!loadingPage && noClassSelect && (
                <div className={styles.noFieldSelect}>
                    <img
                        src={`/images-v2/dashboard-teacher-no-class-select.png`}
                        alt=''
                    />
                    <span
                        dangerouslySetInnerHTML={{
                            __html: t(`${t("dashboard")["choose-class-msg"]}`)
                        }}
                    />
                </div>
            )}
            {!loadingPage && !noClassSelect && (
                <>
                    <Tabs
                        className={styles.tabHeader}
                        data={ClassRuleTab}
                        step={tabIndex}
                        onClickStep={handleTabSelect}
                    />
                    <div className={styles.content}>{renderUiWithTab()}</div>
                </>
            )}
        </div>
    )
}

export default ClassRankingDetail