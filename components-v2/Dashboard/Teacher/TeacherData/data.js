export const UNIT_TYPE_OPTIONS = [
    { label: 'all-tests', value: -1 },
    { label: 'practice', value: 0 },
    { label: 'exam', value: 1 },
]

export const STATUS_RANKING_RULE = {
    ONLY_CLASS_TARGET: 1, // Thống kê dựa trên đề thi giao cho cả lớp
    ALL_TARGET: 2, // Thống kê dựa trên toàn bộ đề thi đã giao
}

//  Rating:
//  5 → Giỏi
// 	4 → Khá
// 	3 → Trung bình
// 	2 → Yếu
// 	1 → Kém

export const mapStudentRating = {
    [5]: "very-good-level",
    [4]: "good-level",
    [3]: "average-level",
    [2]: "weak-level",
    [1]: "bad-level",
}

export const ABILITY_DETAILS = [
    {
        id: 5,
        labelName: "very-good-level",
        color: '#5DACDA',
        description: '(X ≥ 80%)',
    },
    {
        id: 4,
        labelName: "good-level",
        color: '#64CA99',
        description: '(X ≥ 65%)',
    },
    {
        id: 3,
        labelName: "average-level",
        color: '#FFCC8A',
        description: '(X ≥ 50%)',
    },
    {
        id: 2,
        labelName: "weak-level",
        color: '#F2765F',
        description: '(X ≥ 35%)',
    },
    {
        id: 1,
        labelName: "bad-level",
        color: '#8878EB',
        description: '(X < 35%)',
    },
]