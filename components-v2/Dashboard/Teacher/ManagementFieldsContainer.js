import { useEffect, useState } from 'react';

import classNames from 'classnames';
import Link from "next/link";

import ArrowLeftBtn from "@/assets/icons/arrow-left.svg";
import ExamBankIcon from '@/assets/icons/exam-bank.svg';
import ITestClasses from '@/assets/icons/ic-classes.svg';
import UnitTestStatistics from '@/assets/icons/statistics-icon.svg';
import UserAlt from '@/assets/icons/user_alt_fill.svg';
import CircleLoading from '@/componentsV2/Common/Controls/Loading';
import useTranslation from '@/hooks/useTranslation';
import { callApiConfigError } from 'api/utils';

import { paths } from '../../../interfaces/constants';
import { formatNumber } from "../../../utils";
import styles from "./ManagementFieldsContainer.module.scss";

const ManagementFieldsContainer = () => {
    const { t, locale, subPath } = useTranslation()
    const [loading, setLoading] = useState(true)
    const [totalClass, setTotalClass] = useState(0)
    const [totalStudent, setTotalStudent] = useState(0)
    const [totalExam, setTotalExam] = useState(0)
    const [unitTestStatistics, setUnitTestStatistics] = useState(null)

    const fetchData = async () => {
        try {
            const [classResponse, countStudent, countExam, unitTestStatistics] = await Promise.all([
                callApiConfigError(`/api/classes`),
                callApiConfigError(`/api/teacher/students/count`),
                callApiConfigError(`/api/teacher/unit-tests/count`),
                callApiConfigError(`/api/unit-test/statistics`),
            ]);

            setLoading(false)
            if (classResponse.code === 200) {
                setTotalClass(classResponse?.data?.total)
            }
            if (countStudent.code === 200) {
                setTotalStudent(countStudent?.data?.count)
            }
            if (countExam.code === 200) {
                setTotalExam(countExam?.data?.count)
            }
            if (unitTestStatistics.code === 200) {
                setUnitTestStatistics(unitTestStatistics.data)
            }

        } catch (error) {
            console.error('Error fetching data:', error)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    if (loading) {
        return (
            <div className={styles.container}>
                <div className={styles.loadingContainer}>
                    <CircleLoading color="#B5B5B6" />
                </div>
            </div>
        )
    }

    if (!loading) {
        return (
            <div className={styles.container}>
                <div className={styles.fieldsPart}>
                    <Link href={`${subPath}${paths.classes}`}>
                        <a className={styles.fieldSection}>
                            <div className={styles.boxContent}>
                                <div className={classNames(styles.icon, styles.classes)}>
                                    <ITestClasses />
                                </div>
                                <div className={styles.details}>
                                    <div className={styles.fieldName}>
                                        {t("class")}
                                    </div>
                                    <div className={styles.quantity}>
                                        {totalClass}
                                    </div>
                                </div>

                            </div>
                        </a>
                    </Link>
                    <Link href={`${subPath}${paths.studentDetail}`}>
                        <a className={styles.fieldSection}>
                            <div className={styles.boxContent}>
                                <div className={classNames(styles.icon, styles.students)}>
                                    <UserAlt />
                                </div>
                                <div className={styles.details}>
                                    <div className={styles.fieldName}>
                                        {t("student")}
                                    </div>
                                    <div className={styles.quantity}>
                                        {formatNumber(totalStudent, 0)}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </Link>
                    <Link href={`${subPath}${paths.unitTest}?m=mine`}>
                        <a className={styles.fieldSection}>
                            <div className={styles.boxContent}>
                                <div className={classNames(styles.icon, styles.teacherExams)}>
                                    <ExamBankIcon />
                                </div>
                                <div className={styles.details}>
                                    <div className={styles.fieldName}>
                                        {t("dashboard")["my-unitTest"]}
                                    </div>
                                    <div className={styles.quantity}>
                                        {formatNumber(totalExam, 0)}
                                    </div>
                                </div>
                            </div>
                        </a>
                    </Link>
                    <Link href={`${subPath}${paths.unitTest}`}>
                        <a className={styles.fieldSection}>
                            <div className={styles.boxContent}>
                                <div className={classNames(styles.icon, styles.unitTestStatistics)}>
                                    <UnitTestStatistics />
                                </div>
                                <div className={styles.details}>
                                    <div className={styles.fieldName}>
                                        {t("dashboard")["unitTest-from-iTest"]}
                                    </div>
                                    <span className={styles.totalUnitTest}>
                                        {formatNumber(unitTestStatistics?.total, 0)}
                                    </span>
                                    <span className={styles.totalNewUnitTest}>
                                        {t("dashboard")["recently-add-unitTest"]}: {formatNumber(unitTestStatistics?.totalNewUnitTest, 0)}
                                    </span>
                                </div>
                            </div>
                        </a>
                    </Link>
                    <Link href={`${subPath}${paths.learningResult}`}>
                        <a className={styles.resultsPart}>
                            <span className={styles.text}>
                                {t("dashboard")["learning-tracking"]}
                            </span>
                            <div className={styles.icon}>
                                <ArrowLeftBtn />
                            </div>
                        </a>
                    </Link>
                </div>
            </div>
        )
    }
}

export default ManagementFieldsContainer