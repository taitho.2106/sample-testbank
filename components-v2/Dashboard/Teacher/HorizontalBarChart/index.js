import React, { useEffect, useMemo, useRef } from 'react'

import {
    Chart as ChartJS,
    ArcElement,
    Tooltip,
    Legend,
    BarElement,
    Title,
} from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
ChartJS.register(
    ArcElement,
    Tooltip,
    Legend,
    ChartDataLabels,
    BarElement,
    Title
);
import { Bar } from 'react-chartjs-2';

import useTranslation from '@/hooks/useTranslation';

import styles from './index.module.scss';

const BAR_COLOR = '#64CA99'

const findDataWithMaxCorrectRate = (data) => {
    if (!Array.isArray(data) || data.length === 0) {
        return null;
    }

    return data.reduce((maxData, currentData) => {
        if (currentData.correctRate !== null && currentData.correctRate > maxData.correctRate) {
            return currentData;
        }
        return maxData;
    });
};

const HorizontalBarChart = ({ data }) => {
    const { t } = useTranslation()
    const maxCorrectRateData = findDataWithMaxCorrectRate(data);

    const options = useMemo(() => {
        const maxValue =
            maxCorrectRateData?.correctRate > 0.1
                || maxCorrectRateData?.correctRate === null
                || maxCorrectRateData === null
                ? 100 : undefined;

        return {
            indexAxis: 'y',
            responsive: true,
            maintainAspectRatio: false,
            label: undefined,
            plugins: {
                legend: false,
                datalabels: {
                    labels: {
                        title: {
                            font: {
                                size: 10,
                                weight: '500',
                            },
                        },
                    },
                    align: 'left',
                    color: 'white',
                    anchor: 'end',
                    borderRadius: 20,
                    formatter: function (value, context) {
                        if (value > 0) {
                            return value + '%';
                        }
                        return '';
                    },
                },
                tooltip: {
                    callbacks: {
                        label: (tooltipItem) => {
                            const value = tooltipItem.formattedValue;
                            const label = tooltipItem.dataset.label
                            return `${label}: ${value}%`;
                        },
                    },
                },
            },
            scales: {
                x: {
                    grid: {
                        display: false,
                    },
                    grace: 1,
                    min: 0,
                    max: maxValue,
                    afterFit: (axis) => {
                        axis.paddingRight = 100;
                    },
                },
                y: {
                    title: {
                        display: true,
                        text: t("class"),
                        color: '#000000',
                        font: {
                            size: '12px',
                            weight: '400',
                        },
                    },
                    ticks: {
                        display: false,
                    },
                    grid: {
                        display: false,
                    },
                },
            },
        };
    }, [
        data,
        maxCorrectRateData
    ]);

    const customTopLabel = useMemo(() => ({
        afterDatasetsDraw(chart, args, pluginOptions) {
            const { ctx } = chart
            const meta = chart.getDatasetMeta(0);
            const labelArray = chart.data.labels || [];
            ctx.save();
            ctx.font = '8px';
            ctx.fillStyle = '#4F4F4F';

            const chartWidth = chart.canvas.clientWidth;

            labelArray.forEach((label, index) => {
                const xPos = meta.data[index].x + 5;
                const yPos = meta.data[index].y + 5;

                const baseWidth = meta.data[index].base // width từ khung ngoài đến vị trí base/gốc tọa độ
                const barHeight = meta.data[index].x - baseWidth;

                const labelWidth = ctx.measureText(label).width; // Tính width của label
                let totalBarAndLabelWidth = barHeight + labelWidth + 5 + baseWidth;
                let truncatedLabel = label;

                if (totalBarAndLabelWidth > chartWidth) {
                    // tính available space phù hợp để hiển thi label
                    const availableSpace = chartWidth - barHeight - 5 - baseWidth;
                    for (let i = 1; i <= label.length; i++) {
                        const truncatedString = label.substring(0, label.length - i);
                        const truncatedLabelWidth = ctx.measureText(truncatedString + '...').width;

                        if (truncatedLabelWidth <= availableSpace) {
                            // Cắt chuỗi Label đề thu gọn vừa khoảng space cho phép
                            truncatedLabel = truncatedString + '...';
                            totalBarAndLabelWidth = barHeight + truncatedLabelWidth + 5;
                            break;
                        }
                    }
                }

                ctx.fillText(truncatedLabel, xPos, yPos);
            });

            ctx.restore();
        },
    }), [data]);

    const dataCount = useMemo(() => {
        let labels = [], datasets = [];
        if (data) {
            labels = data.map(el => el.name);
            datasets.push({
                label: t("dashboard")["accuracy-rate"],
                data: data.map(el => {
                    if (el.correctRate)
                        return (el.correctRate * 100).toFixed(1)
                    else return el.correctRate
                }),
                borderRadius: 5,
                backgroundColor: BAR_COLOR,
                barThickness: 24,
                barPercentage: 1,
                categoryPercentage: 0.6,
                skipNull: true,
            });
        }
        return { labels, datasets }
    }, [data]);

    const chartRef = useRef(null);
    const chartBoxHeight = 150;
    const MIN_VISIBLE_BARS = 4;
    const BAR_HEIGHT = 40;

    const updateChartHeight = () => {
        if (!chartRef.current) return;
        const chartContainer = chartRef.current;
        const totalBars = chartContainer.data.labels.length;
        let newHeight = chartBoxHeight + (totalBars > MIN_VISIBLE_BARS ? (totalBars - MIN_VISIBLE_BARS) * BAR_HEIGHT : 0);

        const chartContainerParent = chartContainer.canvas.parentNode;
        chartContainerParent.style.height = newHeight + 'px';
    };

    useEffect(() => {
        updateChartHeight();
    }, [data]);

    return (
        <div className={styles.barChartContainer}>
            <div className={styles.note}>
                <div className={styles.labelItem}></div>
                <span className={styles.textNote}>{t("dashboard")["accuracy-rate"]} {t("dashboard")["accuracy-unit"]}</span>
            </div>
            <div className={styles.chart}>
                <Bar
                    ref={chartRef}
                    data={dataCount}
                    options={options}
                    plugins={[customTopLabel]}
                />
            </div>
        </div>
    )
}

export default HorizontalBarChart