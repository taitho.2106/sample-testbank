import React, { useEffect, useState } from 'react'

import qs from "qs"

import CircleLoading from '@/componentsV2/Common/Controls/Loading';
import useGrade from '@/hooks/useGrade'
import useTranslation from '@/hooks/useTranslation';
import { delayResult } from '@/utils/index';
import { paths } from 'api/paths';
import { callApiConfigError } from 'api/utils';

import { getInitDate } from '../Main/util';
import AssignedFrequencyChart from "../Teacher/AssignedFrequencyChart"
import styles from "./AssignedFrequencyContainer.module.scss"
import FrequencyFilterContainer from "./Common/FrequencyFilterContainer"

const convertDates = (dateArray) => {
    return dateArray.map((dateString) => new Date(dateString).toISOString());
};

const AssignedFrequencyContainer = () => {
    const {t} = useTranslation()
    const [dateFilter, setDateFilter] = useState(getInitDate());
    const { getGrade } = useGrade()

    const [loading, setLoading] = useState(true)
    const [gradeSelected, setGradeSelected] = useState('')
    const [dataClassOfTeacher, setDataClassOfTeacher] = useState([])
    const [classSelected, setClassSelected] = useState('')
    const [unitTestFrequency, setUnitTestFrequency] = useState([])
    const gradeList = getGrade.filter(item => /^G[0-9]{1,2}$/.test(`${item.code}`)) || []
    const gradeOptions = gradeList.map(({ code, display }) => ({
        value: code,
        label: display,
    }))

    const fetcherClassIdByGrade = async () => {
        const queryString = qs.stringify({
            grade: gradeSelected || undefined,
            size: 100
        })
        const response = await callApiConfigError(`${paths.api_teacher_classes}?${queryString}`)
        if (response.code === 200) {
            const dataOption = response?.data?.content?.map((item) => {
                return {
                    value: item.id.toString(),
                    label: item.name
                }
            })
            setDataClassOfTeacher(dataOption)
        }
    }

    const fetcher = async () => {
        const body = {
            start_date: convertDates(dateFilter)[0],
            end_date: convertDates(dateFilter)[1],
        }
        if (gradeSelected) {
            body.grade = gradeSelected
        }
        if (classSelected) {
            body.class_id = Number(classSelected)
        }
        const response = await delayResult(callApiConfigError(
            `/api/statistic/unit-tests`,
            'POST',
            'token',
            body
        ), 400)
        setLoading(false)
        if (response.code === 200) {
            setUnitTestFrequency(response?.data)
        }
    }

    useEffect(() => {
        setDataClassOfTeacher([])
        fetcherClassIdByGrade().finally()
    }, [
        gradeSelected
    ])

    useEffect(() => {
        fetcher()
    }, [
        dateFilter,
        gradeSelected,
        classSelected,
    ])

    const handleGradeSelect = (value) => {
        setClassSelected('')
        setGradeSelected(value)
    }

    if (loading) {
        return (
            <div className={styles.container}>
                <div className={styles.loadingContainer}>
                    <CircleLoading color="#B5B5B6" />
                </div>
            </div>
        )
    }

    if (!loading) {
        return (
            <div className={styles.container}>
                <div className={styles.header}>
                    <div className={styles.titleSection}>
                        <span className={styles.chartTitle}>
                            {t("dashboard")["assigned-frequency"]}
                        </span>
                        <span className={styles.summary}>
                            {t("dashboard")["assigned-frequency-summary"]}
                        </span>
                    </div>
                    <FrequencyFilterContainer
                        gradeOptions={gradeOptions}
                        dataClassOfTeacher={dataClassOfTeacher}
                        dateRangeProps={{
                            value: dateFilter,
                            onChange: setDateFilter,
                        }}
                        gradeProps={{
                            value: gradeSelected,
                            onChange: (val) => handleGradeSelect(val),
                        }}
                        classProps={{
                            value: classSelected,
                            onChange: setClassSelected,
                        }}
                        onReset={() => {
                            setDateFilter(getInitDate())
                            setGradeSelected('')
                            setClassSelected('')
                        }}
                        disabledState={!gradeSelected && !classSelected}
                    />
                </div>
                <div className={styles.body}>
                    <AssignedFrequencyChart data={unitTestFrequency} />
                </div>
            </div>
        )
    }
}

export default AssignedFrequencyContainer