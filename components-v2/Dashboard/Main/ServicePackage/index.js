import React, { useMemo } from 'react';
import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import { Doughnut } from 'react-chartjs-2';
import Link from 'next/link';
import ChartDataLabels from 'chartjs-plugin-datalabels';

ChartJS.register(ArcElement, Tooltip, Legend, ChartDataLabels);

import { formatNumber } from '@/utils/index';

import IconArrow from '@/assets/icons/back-arrow.svg';

import styles from './index.module.scss';
import { paths } from '@/interfaces/constants';
import classNames from 'classnames';
import Table from '../Table';
import NoData from "../NoData";
import { pluginDataTable } from "../util";
import { PACKAGES, PACKAGES_FOR_STUDENT, USER_ROLES } from "../../../../interfaces/constants";
import IconNotice from "assets/icons/info-outlined-circle.svg"
import { SALE_ITEM_TYPE } from "../../../../constants";
import useTranslation from '@/hooks/useTranslation';

const ServicePackage = ({ orders, showViewDetail, className, role }) => {
    const { t } = useTranslation()
    const options = {
        responsive: true,
        plugins: {
            legend: false,
            datalabels: pluginDataTable,
        },
        cutout: 0,
    }
    const getColorWithPackage = (packageName) => {
        switch (packageName) {
            case PACKAGES.BASIC.CODE:
            case PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE:
                return '#7BB2F9';
            case PACKAGES.ADVANCE.CODE:
            case PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE:
                return '#FFCA87';
            case PACKAGES.INTERNATIONAL.CODE:
            case PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE:
                return '#92CEA6';
            case PACKAGES.COMBO.CODE:
            case SALE_ITEM_TYPE.UNIT_TEST:
                return '#FC686A';
        }
    }

    const dataCount = useMemo(() => {
        let labels = [], datasets = [];
        if (orders) {
            labels = orders.map(el => el.servicePackage.name);
            datasets.push({
                label: t("dashboard")["purchase"],
                data: orders.map(el => el.orderCount),
                backgroundColor: orders.map(el => getColorWithPackage(el.servicePackage.code))
            });
        }
        return { labels, datasets }
    }, [orders]);

    const dataAmount = useMemo(() => {
        let labels = [], datasets = [];
        if (orders) {
            labels = orders.map(el => el.servicePackage.name);
            datasets.push({
                label: t("dashboard")["revenue"],
                data: orders.map(el => el.totalAmount),
                backgroundColor: orders.map(el => getColorWithPackage(el.servicePackage.code))
            });
        }

        return { labels, datasets }
    }, [orders]);

    const totalCount = orders?.reduce((rs, cur) => rs + cur.orderCount, 0) || 0;
    const totalAmount = orders?.reduce((rs, cur) => rs + cur.totalAmount, 0) || 0;

    return (
        <div className={classNames(styles.servicePackageInfo, className)}>
            <div className={styles.chart}>
                <div className={styles.list}>
                    <div className={styles.item}>
                        <h3 className={styles.title}>{t("dashboard")["service-package-purchases"]}</h3>
                        {!!totalCount ? (
                            <div className={styles.chartContainer}>
                                <Doughnut data={dataCount} options={options}/>
                            </div>
                            ) : (
                                <NoData />
                            )}
                    </div>
                    <div className={styles.item}>
                        <h3 className={styles.title}>{t("dashboard")["service-package-revenue"]}</h3>
                        {!!totalAmount ? (
                            <div className={styles.chartContainer}>
                                <Doughnut data={dataAmount} options={options}/>
                            </div>
                        ) : (
                            <NoData />
                        )}
                    </div>
                </div>
                <div className={styles.labels}>
                    <div className={styles.boxLabel}>
                        {orders?.map((el, index) => index % 2 === 0 && (
                            <div className={styles.labelItem} key={index}>
                                <div className={styles.itemColor} style={{ backgroundColor: getColorWithPackage(el.servicePackage.code) }} />
                                <span>{el.servicePackage.name}</span>
                            </div>
                        ))}
                    </div>
                    <div className={styles.boxLabel}>
                        {orders?.map((el, index) => index % 2 === 1 && (
                            <div className={styles.labelItem} key={index}>
                                <div className={styles.itemColor} style={{ backgroundColor: getColorWithPackage(el.servicePackage.code) }} />
                                <span>{el.servicePackage.name}</span>
                            </div>
                        ))}
                    </div>
                </div>
                {showViewDetail && (
                    <div className={styles.actions}>
                        <Link href={`${paths.dashboardServicePackage}?role=${role}`}>
                            <a className={styles.viewDetails}>
                                {t("dashboard")["view-detailed"]}
                                <IconArrow />
                            </a>
                        </Link>
                    </div>
                )}
            </div>
            {!!orders?.length && <div className={styles.details}>
                {
                    role === USER_ROLES.Student && (
                        <div className={styles.notiPackageStudent}>
                            <IconNotice />
                            <span>
                                {t("dashboard")["noti-package-student"]}
                            </span>
                        </div>
                    )
                }
                <Table
                    columns={[
                        {
                            title: t("service-pack"),
                            dataIndex: 'servicePackage',
                            render: (_, colData) => <span className={styles.packageName}>{colData.name}</span>
                        },
                        {
                            title: t("dashboard")["purchase"],
                            render: (order) => formatNumber(order.orderCount, 0),
                        },
                        {
                            title: `${t("dashboard")["revenue"]} (VNĐ)`,
                            render: (order) => formatNumber(order.totalAmount, 0),
                        }
                    ]}
                    data={orders}
                    footer={
                        <>
                            <td>{t("dashboard")["total"]}</td>
                            <td>{formatNumber(totalCount, 0)}</td>
                            <td>{formatNumber(totalAmount, 0)}</td>
                        </>
                    }
                />
            </div>}
        </div>
    );
};

export default ServicePackage;