import React, { useEffect, useState } from "react";

import classNames from "classnames";

import ExamBankIcon from "@/assets/icons/exam-bank.svg";
import ITestClasses from "@/assets/icons/ic-classes.svg";
import UserAlt from "@/assets/icons/user_alt_fill.svg";
import useTranslation from "@/hooks/useTranslation";

import { callApi } from "../../../api/utils";
import { delayResult, formatNumber } from "../../../utils";
import Loading from "../../Common/Controls/Loading";
import styles from "./StatisticsCard.module.scss";

const StatisticsCard = () => {
	const { t } = useTranslation()
	const [loading, setLoading] = useState(true);
	const [generalStatistic, setGeneralStatistic] = useState({});
	const fetchData = async () => {
		try {
			setLoading(true);
			const response = await delayResult(callApi("/api/statistic/admin/general-statistics"), 300);
			setLoading(false);
			if (response.code === 200) {
				setGeneralStatistic(response.data);
			}
		} catch (e) {
			setLoading(false);
		}
	};
	useEffect(() => {
		fetchData();
	}, []);
	return (
		<>
			{
				loading ? (
					<div className={styles.cardContainer}>
						<div className={styles.loading}>
							<Loading color="#B5B5B6" />
						</div>
					</div>
				) : (
					<div className={styles.cardContainer}>
						<div className={styles.cardSection}>
							<div className={classNames(styles.icon, styles.teacher)}>
								<ITestClasses />
							</div>
							<div className={styles.details}>
								<span className={styles.fieldName}>
									{t("teacher")}
								</span>
								<span className={styles.quantity}>
									{formatNumber(generalStatistic?.user?.totalTeachers)}
								</span>
							</div>
						</div>
						<div className={styles.divider}></div>
						<div className={styles.cardSection}>
							<div className={classNames(styles.icon, styles.students)}>
								<UserAlt />
							</div>
							<div className={styles.details}>
								<span className={styles.fieldName}>
									{t("student")}
								</span>
								<span className={styles.quantity}>
									{formatNumber(generalStatistic?.user?.totalStudents)}
								</span>
							</div>
						</div>
						<div className={styles.divider}></div>
						<div className={styles.cardSection}>
							<div className={classNames(styles.icon, styles.unitTest)}>
								<ExamBankIcon />
							</div>
							<div className={styles.details}>
								<span className={styles.fieldName}>
									{t('dashboard')["national-test"]}
								</span>
								<span className={styles.quantity}>
									{formatNumber(generalStatistic?.unit_test?.totalUnitTestNotIE)}
								</span>
								<i>
									{t('dashboard')["released"]}: {formatNumber(generalStatistic?.unit_test?.totalUnitTestNotIEPublished)}
								</i>
							</div>
						</div>
						<div className={styles.divider}></div>
						<div className={styles.cardSection}>
							<div className={classNames(styles.icon)}>
								<ExamBankIcon />
							</div>
							<div className={styles.details}>
								<span className={styles.fieldName}>
									{t('dashboard')["international-test"]}
								</span>
								<span className={styles.quantity}>
									{formatNumber(generalStatistic?.unit_test?.totalUnitTestIE)}
								</span>
								<i>
									{t('dashboard')["released"]}: {formatNumber(generalStatistic?.unit_test?.totalUnitTestIEPublished)}
								</i>
							</div>
						</div>
					</div>
				)
			}
		</>
	);
};

export default StatisticsCard;