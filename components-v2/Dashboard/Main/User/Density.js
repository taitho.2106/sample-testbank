import { useMemo } from "react";

import { Chart as ChartJS, ArcElement, Tooltip, Legend } from 'chart.js';
import ChartDataLabels from 'chartjs-plugin-datalabels';
import { Doughnut } from 'react-chartjs-2';

ChartJS.register(ArcElement, Tooltip, Legend, ChartDataLabels);

import useTranslation from '@/hooks/useTranslation';
import { USER_ROLES } from '@/interfaces/constants';

import NoData from "../NoData";
import { pluginDataTable } from "../util";
import styles from './Density.module.scss';

const Density = ({ userByRole, userByPackage = [] }) => {
    const {t} = useTranslation()
    const options = {
        responsive: true,
        plugins: {
            htmlLegend: {
                // ID of the container to put the legend in
                containerID: 'legend-container',
            },
            legend: {
                display: false,
                position: 'bottom',
                align: 'start',
            },
            datalabels: pluginDataTable
        },
        cutout: 0,
    }
    const getOrCreateLegendList = (chart, id) => {
        const legendContainer = document.getElementById(id);
        let listContainer = legendContainer?.querySelector('ul');
        
        if (!listContainer) {
            listContainer = document.createElement('ul');
            listContainer.style.display = 'flex';
            listContainer.style.flexDirection = 'row';
            listContainer.style.margin = 0;
            listContainer.style.padding = 0;
            listContainer.style.alignItems = "center";
            listContainer.style.gap = "24px";
            listContainer.style.flexWrap = "wrap";
            
            legendContainer?.appendChild(listContainer);
        }
        
        return listContainer;
    };
    
    const htmlLegendPlugin = {
        id: 'htmlLegend',
        afterUpdate(chart, args, options) {
            const ul = getOrCreateLegendList(chart, options.containerID);
            
            // Remove old legend items
            while (ul.firstChild) {
                ul.firstChild.remove();
            }
            
            // Reuse the built-in legendItems generator
            const items = chart.options.plugins.legend.labels.generateLabels(chart);
            
            items.forEach(item => {
                const li = document.createElement('li');
                li.style.alignItems = 'center';
                li.style.cursor = 'pointer';
                li.style.display = 'flex';
                li.style.flexDirection = 'row';
                li.style.width = '40%';
                
                li.onclick = () => {
                    const {type} = chart.config;
                    if (type === 'pie' || type === 'doughnut') {
                        // Pie and doughnut charts only have a single dataset and visibility is per item
                        chart.toggleDataVisibility(item.index);
                    } else {
                        chart.setDatasetVisibility(item.datasetIndex, !chart.isDatasetVisible(item.datasetIndex));
                    }
                    chart.update();
                };
                
                // Color box
                const boxSpan = document.createElement('span');
                boxSpan.style.background = item.fillStyle;
                boxSpan.style.borderColor = item.strokeStyle;
                boxSpan.style.borderWidth = item.lineWidth + 'px';
                boxSpan.style.display = 'inline-block';
                boxSpan.style.flexShrink = 0;
                boxSpan.style.height = '16px';
                boxSpan.style.marginRight = '10px';
                boxSpan.style.width = '35px';
                
                // Text
                const textContainer = document.createElement('p');
                textContainer.style.color = item.fontColor;
                textContainer.style.margin = 0;
                textContainer.style.padding = 0;
                textContainer.style.textDecoration = item.hidden ? 'line-through' : '';
                
                const text = document.createTextNode(item.text);
                textContainer.appendChild(text);
                
                li.appendChild(boxSpan);
                li.appendChild(textContainer);
                ul.appendChild(li);
            });
        }
    };

    const totalUserByRole = useMemo(() => {
        let dataArr = [];
        if (!!userByPackage.length) {
            dataArr = userByPackage
        } else {
            dataArr = userByRole
        }
         return (dataArr || []).reduce((rs, cur) => rs + cur.total, 0);
    }, [userByPackage.length])
    console.log(t("student"))

    const data = useMemo(() => {
        let labels = [t("teacher"), t("student")], datasets = [];
        if (!!userByPackage.length) {
            labels = userByPackage.map(item => item.name)
            datasets.push({
                label: t("dashboard")["new-user"],
                data: userByPackage.map(item => item.total),
                backgroundColor: [
                    "#7BB2F9",
                    "#FFCA87",
                    "#92CEA6",
                    "#FC686A"
                ]
            })
            return { labels, datasets }
        }
        if (userByRole) {
            datasets.push({
                label: t("dashboard")["new-user"],
                data: [
                    userByRole?.find(el => el.user_role_id === USER_ROLES.Teacher)?.total || 0,
                    userByRole?.find(el => el.user_role_id === USER_ROLES.Student)?.total || 0,
                ],
                backgroundColor: [
                    '#1890FF',
                    '#8A79F4',
                ],
            });
            
            return { labels, datasets }
        }

    }, [userByRole, userByPackage.length]);

    return (
        <div className={styles.density}>
            <div className={styles.doughnut}>
                {!!totalUserByRole ? <Doughnut data={data} options={options} plugins={[htmlLegendPlugin]} /> : (
                    <NoData />
                )}
            </div>
            <div id="legend-container"></div>
        </div>
    );
};

export default Density;