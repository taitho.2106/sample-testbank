import React, { useMemo } from 'react';

import {
    CategoryScale, Chart as ChartJS, Legend, LinearScale, LineElement, PointElement, Title,
    Tooltip
} from 'chart.js';
import { Line } from 'react-chartjs-2';

import useTranslation from '@/hooks/useTranslation';
import { formatNumber } from '@/utils/index';

import styles from './NewUser.module.scss';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const NewUser = ({ totalUser, users }) => {
    const { t } = useTranslation()
    const options = {
        responsive: true,
        datasets: {
            line: {
                tension: 0.4,
            }
        },
        plugins: {
            legend: {
                position: 'bottom'
            },
            datalabels: false,
        },
        maintainAspectRatio: false,
        scales: {
            x: {
                grid: {
                    display: false
                }
            },
            y: {
                beginAtZero: true,
                suggestedMin: 0,
                suggestedMax: 120,
            }
        }
    }

    const chartData = useMemo(() => {
        let labels = [], datasets = [];
        if (users) {
            labels = users.map(el => el.date.split('/').splice(0, 2).join('/'));

            datasets.push({
                label: t("dashboard")["user-quantity"],
                data: users.map(el => el.total),
                borderColor: '#F79945',
                backgroundColor: '#F79945',
            });
        }

        return {
            labels, datasets
        }
    }, [users]);

    const totalNewUser = users?.reduce((rs, cur) => rs + cur.total, 0) || 0;

    return (
        <div className={styles.newUser}>
            <h3 className={styles.users}>
                {t("dashboard")["new-user"]}: <b className={styles.new}>{formatNumber(totalNewUser, 0)}</b>
                <span className={styles.separate} />
                {t("dashboard")["total-user"]}: <b className={styles.total}>{formatNumber(totalUser, 0)}</b>
            </h3>

            <div className={styles.chart}>
                <Line
                    options={options}
                    data={chartData}
                />
            </div>
        </div>
    );
};

export default NewUser;