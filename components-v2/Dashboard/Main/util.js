import dayjs from "dayjs";

export const getInitDate = () => ([
    dayjs().subtract(29, 'd').toDate(),
    dayjs().toDate(),
]);

export const reverseCloneArray = (array) => [ ...(array || []) ].reverse()

const getPercentList = (data) => {
    const total = data.reduce((rs, cur) => rs + cur, 0);
    let currentPercent = 0;
    const percentList = [];

    for (let i = 0; i < data.length; i++) {
        let percent;
        if (i === data.length - 1) {
            percent = +(100 - currentPercent).toFixed(1);
        } else {
            percent = +(data[i] * 100 / total).toFixed(1);
            currentPercent += percent;
        }

        percentList.push(percent);
    }

    return percentList;
}

export const pluginDataTable = {
    color: "white",
    borderWidth: 2,
    font: {
        size: 15,
        weight: "bold",
    },
    formatter: (value, ctx) => {
        const { dataIndex, dataset } = ctx;
        const percentList = getPercentList(dataset.data);
        const percent = percentList[dataIndex];
        return percent ? `${percent}%` : '';
    }
}