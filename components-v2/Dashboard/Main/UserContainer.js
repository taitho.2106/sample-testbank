import dayjs from 'dayjs';
import Link from 'next/link';
import { useEffect } from 'react';

import apiConfig from '@/constants/apiConfig';
import { DATE_VALUE_FORMAT } from '@/constants/index';
import useFetch from '@/hooks/useFetch';
import useTranslation from '@/hooks/useTranslation';
import { paths } from '@/interfaces/constants';

import Loading from '@/componentsV2/Common/Controls/Loading';
import Density from './User/Density';
import NewUser from './User/NewUser';

import IconArrow from '@/assets/icons/back-arrow.svg';

import styles from './UserContainer.module.scss';
const UserContainer = ({ dateFilter }) => {
    const { t } = useTranslation()
    const { execute, loading, data } = useFetch(apiConfig.dashboard.user, { delay: 250 });

    const { totalUser, users, userByRole } = data || {};

    useEffect(() => {
        const [ startDate, endDate ] = dateFilter;

        execute({
            params: {
                startDate: dayjs(startDate).format(DATE_VALUE_FORMAT),
                endDate: dayjs(endDate).format(DATE_VALUE_FORMAT),
            },
            onCompleted: () => { },
            onError: () => { }
        });
    }, [ dateFilter ]);

    return (
        <div className={styles.userSummary}>
            {loading && (
                <div className={styles.loadingContainer}>
                    <Loading color="#B5B5B6" />
                </div>
            )}
            {userByRole && (
                <>
                    <div className={styles.main}>
                        <div className={styles.new}>
                            <NewUser
                                totalUser={totalUser}
                                users={users}
                                loading={loading}
                            />
                        </div>
                        <div className={styles.density}>
                            <div className={styles.head}>
                                <h3>
                                    {t("dashboard")["user-proportion"]}
                                </h3>
                                <h4>
                                    {t("dashboard")["proportion-desc"]}
                                </h4>
                            </div>
                            <Density userByRole={userByRole} />
                        </div>
                    </div>
                    <div className={styles.actions}>
                        <Link href={paths.dashboardUser}>
                            <a className={styles.viewDetails}>
                                {t("dashboard")["view-detailed"]}
                                <IconArrow />
                            </a>
                        </Link>
                    </div>
                </>
            )}
        </div>
    );
};

export default UserContainer;