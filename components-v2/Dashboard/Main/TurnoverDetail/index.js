import { useEffect, useMemo, useState } from 'react';

import dayjs from 'dayjs';

import apiConfig from '@/constants/apiConfig';
import { DATE_VALUE_FORMAT } from '@/constants/index';
import useFetch from '@/hooks/useFetch';
import { formatNumber } from '@/utils/index';

import { USER_ROLES } from "../../../../interfaces/constants";
import FilterContainer from '../Common/FilterContainer';
import SelectPicker from "../Common/SelectPicker";
import Table from '../Table';
import TurnoverSummary from '../TurnoverSummary';
import { getInitDate, reverseCloneArray } from "../util";
import styles from './index.module.scss';
import Loading from "../../../Common/Controls/Loading";


const TurnoverDetail = () => {
    const userRoleOptions = [
        { label: 'Tất cả người dùng', value: '' },
        { label: 'Giáo viên', value: USER_ROLES.Teacher },
        { label: 'Học viên', value: USER_ROLES.Student },
    ]
    const { execute, loading, data } = useFetch(apiConfig.dashboard.turnover, { delay: 250 });
    const [userRoleSelected, setUserRoleSelected] = useState('');
    const [ dateFilter, setDateFilter ] = useState(getInitDate());

    const { succeed, failed } = data || {};

    const mergeData = useMemo(() => {
        const result = [];
        if (succeed?.data && failed?.data) {
            const length = succeed.data.length;
            for (let i = 0; i < length; i++) {
                const orderFailed = failed.data[i];
                const orderSucceed = succeed.data[i];

                result.push({
                    date: orderSucceed.date,
                    total: orderSucceed.total + orderFailed.total,
                    count: orderSucceed.orderCount + orderFailed.orderCount,
                    totalSucceed: orderSucceed.total,
                    countSucceed: orderSucceed.orderCount,
                    totalFailed: orderFailed.total,
                    countFailed: orderFailed.orderCount,
                })
            }
        }

        return result;
    }, [succeed, failed]);

    useEffect(() => {
        const [ startDate, endDate ] = dateFilter;
        const params = {
            startDate: dayjs(startDate).format(DATE_VALUE_FORMAT),
            endDate: dayjs(endDate).format(DATE_VALUE_FORMAT)
        };
        
        if (userRoleSelected) {
            params.userRoles = userRoleSelected;
        }
        execute({
            params,
            onCompleted: () => { },
            onError: () => { }
        });
    }, [dateFilter, userRoleSelected]);

    return (
        <div className={styles.turnoverDetail}>
            <div className={styles.head}>
                <h3>Chi tiết báo cáo</h3>
                <FilterContainer
                    dateRangeProps={{
                        value: dateFilter,
                        onChange: setDateFilter,
                    }}
                    onReset={() => {
                        setDateFilter(getInitDate());
                        setUserRoleSelected('')
                    }}
                >
                    <SelectPicker
                        data={userRoleOptions}
                        value={userRoleSelected}
                        onChange={setUserRoleSelected}
                    />
                </FilterContainer>
            </div>

            <TurnoverSummary loading={loading} data={data} />
            
            {loading && (
                <div className={styles.loadingContainer}>
                    <div className={styles.loading}>
                        <Loading color="#B5B5B6" />
                    </div>
                </div>
            )}

            {!!mergeData?.length && !loading && (
                <div className={styles.detail}>
                    <Table
                        columns={[
                            { title: 'Ngày', dataIndex: 'date' },
                            {
                                title: 'Toàn bộ giao dịch',
                                render: (rowData) => (
                                    <>
                                        {formatNumber(rowData.count) || 0}
                                        <br />
                                        {formatNumber(rowData.total) || 0}
                                    </>
                                )
                            },
                            {
                                title: 'Giao dịch thất bại',
                                render: (rowData) => (
                                    <>
                                        {formatNumber(rowData.countFailed) || 0}
                                        <br />
                                        {formatNumber(rowData.totalFailed) || 0}
                                    </>
                                )
                            },
                            {
                                title: 'Giao dịch thành công',
                                render: (rowData) => (
                                    <>
                                        {formatNumber(rowData.countSucceed) || 0}
                                        <br />
                                        {formatNumber(rowData.totalSucceed) || 0}
                                    </>
                                )
                            },
                        ]}
                        data={reverseCloneArray((mergeData))}
                    />
                </div>
            )}
        </div>
    );
};

export default TurnoverDetail;