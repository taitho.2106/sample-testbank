import dayjs from 'dayjs';
import { useEffect } from 'react';

import apiConfig from '@/constants/apiConfig';
import { DATE_VALUE_FORMAT } from '@/constants/index';
import useFetch from '@/hooks/useFetch';

import TurnoverSummary from './TurnoverSummary';

const TurnoverSummaryContainer = ({ dateFilter }) => {
    const { execute, loading, data } = useFetch(apiConfig.dashboard.turnover, { delay: 250 });

    useEffect(() => {
        const [ startDate, endDate ] = dateFilter;

        execute({
            params: {
                startDate: dayjs(startDate).format(DATE_VALUE_FORMAT),
                endDate: dayjs(endDate).format(DATE_VALUE_FORMAT),
            },
            onCompleted: () => {},
            onError: () => {}
        });
    }, [ dateFilter ]);

    return (
        <TurnoverSummary loading={loading} data={data} showViewDetail />
    );
};

export default TurnoverSummaryContainer;