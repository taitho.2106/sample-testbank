import dayjs from 'dayjs';
import { useEffect, useState } from 'react';

import apiConfig from '@/constants/apiConfig';
import { DATE_VALUE_FORMAT } from '@/constants/index';
import useFetch from '@/hooks/useFetch';
import { USER_ROLES } from '@/interfaces/constants';
import { formatNumber } from '@/utils/index';
import { getInitDate, reverseCloneArray } from "../util";

import Loading from '@/componentsV2/Common/Controls/Loading';
import FilterContainer from '../Common/FilterContainer';
import SelectPicker from '../Common/SelectPicker';
import Table from '../Table';
import Density from '../User/Density';
import NewUser from '../User/NewUser';

import styles from './index.module.scss';
import useTranslation from '@/hooks/useTranslation';

const mapUserRoleLabel = {
    [USER_ROLES.Teacher]: 'Giáo viên',
    [USER_ROLES.Student]: 'Học sinh',
}

const UserDetail = () => {
    const { t } = useTranslation()
    const { execute, loading, data } = useFetch(apiConfig.dashboard.user);
    const [dateFilter, setDateFilter] = useState(getInitDate());
    const [userRoleSelected, setUserRoleSelected] = useState('');

    const userRoleOptions = [
        { label: t("all-users-type"), value: '' },
        { label: t("teacher"), value: USER_ROLES.Teacher },
        { label: t("student"), value: USER_ROLES.Student },
    ]

    const { totalUser, users, userByRole, userByPackage } = data || {};
    const userByRoleList = Object.values(userByRole || {});
    const totalNewUser = userByRoleList.reduce((rs, cur) => rs + cur.total, 0);

    useEffect(() => {
        const [startDate, endDate] = dateFilter;
        const params = {
            startDate: dayjs(startDate).format(DATE_VALUE_FORMAT),
            endDate: dayjs(endDate).format(DATE_VALUE_FORMAT),
        }

        if (userRoleSelected) {
            params.userRoles = userRoleSelected;
        }

        execute({
            params,
            onCompleted: () => { },
            onError: () => { }
        });
    }, [dateFilter, userRoleSelected]);

    return (
        <div className={styles.userDetail}>
            <div className={styles.head}>
                <h3>{t("dashboard")["report-detail"]}</h3>
                <FilterContainer
                    dateRangeProps={{
                        value: dateFilter,
                        onChange: setDateFilter,
                    }}
                    onReset={() => {
                        setDateFilter(getInitDate());
                        setUserRoleSelected('');
                    }}
                >
                    <SelectPicker
                        data={userRoleOptions}
                        value={userRoleSelected}
                        onChange={setUserRoleSelected}
                    />
                </FilterContainer>
            </div>
            {loading ? (
                <div className={styles.loadingContainer}><Loading color="#B5B5B6" /></div>
            ) : (
                <>
                    <div className={styles.new}>
                        <NewUser
                            totalUser={totalUser}
                            users={users}
                            loading={loading}
                        />
                    </div>
                    <div className={styles.detail}>
                        <div className={styles.density}>
                            <h3 className={styles.title}>{t("dashboard")["user-proportion"]}</h3>

                            <Density userByRole={userByRole} userByPackage={userByPackage} />

                            {!!userByRoleList.length && !userByPackage?.length && (
                                <Table
                                    columns={[
                                        { title: t("common")["target"], dataIndex: 'user_role_id', render: (_, colData) => mapUserRoleLabel[colData] || '-' },
                                        { title: t("dashboard")["new-account-quantity"], dataIndex: 'total' },
                                    ]}
                                    data={userByRoleList}
                                    footer={
                                        <>
                                            <td>{t("dashboard")["total"]}</td>
                                            <td>{formatNumber(totalNewUser, 0)}</td>
                                        </>
                                    }
                                />
                            )}
                            {!!userByPackage?.length && (
                                <Table
                                    columns={[
                                        { title: t("common")["target"], dataIndex: 'name', render: (_, colData) => colData || '-' },
                                        { title: t("dashboard")["new-account-quantity"], dataIndex: 'total' },
                                    ]}
                                    data={userByPackage}
                                    footer={
                                        <>
                                            <td>{t("dashboard")["total"]}</td>
                                            <td>{formatNumber(totalNewUser, 0)}</td>
                                        </>
                                    }
                                />
                            )}
                        </div>
                        <div className={styles.list}>
                            <Table
                                columns={[
                                    { title: t("dashboard")["date"], dataIndex: 'date' },
                                    { title: t("dashboard")["new-account-quantity"], dataIndex: 'total' },
                                ]}
                                data={reverseCloneArray(users)}
                            />
                        </div>
                    </div>
                </>
            )}
        </div>
    );
};

export default UserDetail;