import {
    CategoryScale, Chart as ChartJS, Legend, LinearScale, LineElement, PointElement, Title,
    Tooltip
} from 'chart.js';
import Link from 'next/link';
import { useMemo } from 'react';
import { Line } from 'react-chartjs-2';

import Loading from '@/componentsV2/Common/Controls/Loading';

import useTranslation from '@/hooks/useTranslation';
import { formatNumber } from '@/utils/index';

import IconArrow from '@/assets/icons/back-arrow.svg';

import styles from './index.module.scss';
import { paths } from '@/interfaces/constants';
import { Whisper,Tooltip as TooltipRs } from "rsuite";
import InfoIcon from 'assets/icons/icon-info-outline.svg'
ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const TurnoverSummary = ({ loading, data, showViewDetail }) => {
    const { t } = useTranslation()
    const { failed, succeed } = data || {}
    const countSucceed = succeed?.orderCount || 0;
    const countFailed = failed?.orderCount || 0;

    const options = {
        responsive: true,
        datasets: {
            line: {
                tension: 0.4,
            }
        },
        plugins: {
            legend: {
                position: 'bottom',
            },
            datalabels: false
        },
        maintainAspectRatio: false,
        scales: {
            x: {
                grid: {
                    display: false
                }
            },
            y: {
                beginAtZero: true,
                suggestedMin: 0,
                suggestedMax: 1000000,
            }
        }
    }

    const chartData = useMemo(() => {
        let labels = [], datasets = [];
        if (failed && succeed) {
            const { data } = failed;

            labels = data.map(el => el.date.split('/').splice(0, 2).join('/'));
            datasets.push({
                label: t("result-payment")["success"],
                data: succeed.data.map(el => el.total),
                borderColor: '#58B778',
                backgroundColor: '#58B778',
            });
            datasets.push({
                label: t("result-payment")["fail"],
                data: failed.data.map(el => el.total),
                borderColor: '#EF4E2B',
                backgroundColor: '#EF4E2B',
            });
        }

        return {
            labels, datasets
        }
    }, [failed, succeed]);

    return (
        <div className={styles.turnoverSummary}>
            {loading && (
                <div className={styles.loadingContainer}>
                    <Loading color="#B5B5B6" />
                </div>
            )}
            {failed && succeed && (
                <>
                    <div className={styles.head}>
                        <div className={styles.summary}>
                            <div className={styles.turnover}>
                                {t("dashboard")["revenue"]}: <b className={styles.succeed} style={{
                                    marginLeft: '5px'
                                }}>{formatNumber(succeed?.total, 0)} VNĐ</b>
                                <Whisper
                                    placement={'auto'}
                                    trigger={'hover'}
                                    container={() => document.querySelector(('.t-wrapper__main'))}
                                    speaker={<TooltipRs arrow={false}>{t("dashboard")["tooltip-revenue"]}</TooltipRs>}
                                >
                                    <span className={styles.tooltip}><InfoIcon /></span>
                                </Whisper>
                            </div>
                            <div className={styles.turnoverTotal}>
                                {t("common")["total-transaction"]}: <b>{formatNumber(countSucceed + countFailed, 0)} {t("common")["transaction"]}</b>
                                {' '}(<span className={styles.succeed}>{formatNumber(countSucceed, 0)} {t("common")["succeed"]}</span> | <span className={styles.failed}>{formatNumber(countFailed, 0)} {t("common")["failed"]}</span>)
                            </div>
                        </div>
                        {showViewDetail && (
                            <Link href={paths.dashboardTurnover}>
                                <a className={styles.viewDetails}>
                                    {t("dashboard")["view-detailed"]}
                                    <IconArrow />
                                </a>
                            </Link>
                        )}
                    </div>

                    <div className={styles.chart}>
                        <Line
                            options={options}
                            data={chartData}
                        />
                    </div>
                </>
            )}
        </div>
    );
};

export default TurnoverSummary;