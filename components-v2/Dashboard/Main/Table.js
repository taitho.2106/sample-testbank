import React from 'react';
import classNames from 'classnames';

import styles from './Table.module.scss';

const Table = ({
    columns,
    data = [],
    className,
    footer,
}) => {
    return (
        <table className={classNames(styles.table, className)}>
            <thead>
                <tr>
                    {columns.map((el, index) => {
                        const thStyle = {
                            textAlign: el.align || 'left',
                        }
                        if (el.width) {
                            thStyle.width = el.width;
                        }
                        return <th style={thStyle} key={index}>{el.title}</th>;
                    })}
                </tr>
            </thead>
            <tbody>
                {data.map((rowData, rowIndex) => (
                    <tr key={rowIndex}>
                        {columns.map((colConfig, colIndex) => {
                            const colData = rowData[colConfig.dataIndex];
                            const tdStyle = {
                                textAlign: colConfig.align || 'left',
                            }
                            if (colConfig.width) {
                                tdStyle.width = colConfig.width;
                            }

                            return (
                                <td
                                    key={colIndex}
                                    style={tdStyle}
                                >
                                    {colConfig.render
                                        ? colConfig.render(rowData, colData)
                                        : colData}
                                </td>
                            );
                        })}
                    </tr>
                ))}
                {footer && (
                    <tr className={styles.footer}>
                        {footer}
                    </tr>
                )}
            </tbody>
        </table>
    );
};

export default Table;