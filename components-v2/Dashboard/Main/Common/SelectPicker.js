import React from 'react';

import classNames from 'classnames';
import { SelectPicker as SelectPickerRSuite, Dropdown } from 'rsuite';

import styles from './SelectPicker.module.scss';
import useTranslation from "../../../../hooks/useTranslation";

const SelectPicker = ({
    className,
    menuClassName,
    data,
    ...props
}) => {
    const renderMenuItem = (label) => {
        return (
            <span title={label}>{label}</span>
        );
    };

    const renderMenu = (menu) => {
        if (data && data.length > 0) {
            return menu;
        } else {
            return (
                <Dropdown.Menu className={styles.dropDownMenu}>
                    <Dropdown.Item disabled>Chưa có dữ liệu</Dropdown.Item>
                </Dropdown.Menu>
            );
        }
    };

    return (
        <SelectPickerRSuite
            searchable={false}
            cleanable={false}
            style={{ width: '184px' }}
            className={classNames(styles.selectPicker, className)}
            container={() => document.querySelector('.t-wrapper__main')}
            menuClassName={classNames(styles.selectPickerMenu, menuClassName)}
            renderMenuItem={renderMenuItem}
            renderMenu={renderMenu}
            data={data}
            {...props}
        />
    );
};

export default SelectPicker;