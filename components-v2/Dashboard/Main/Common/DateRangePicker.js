import classNames from 'classnames';
import React from 'react';
import { DateRangePicker as DateRangePickerRSuite } from 'rsuite';

const { allowedMaxDays, combine, afterToday } = DateRangePickerRSuite;

import styles from './DateRangePicker.module.scss';

const DateRangePicker = props => {
    return (
        <DateRangePickerRSuite
            format="dd/MM/yyyy"
            placement="bottomEnd"
            cleanable={false}
            character=" - "
            ranges={[]}
            editable={false}
            className={classNames(styles.dateRangePickerCustom, props.disabled && styles.disabled)}
            style={{ width: '228px' }}
            disabledDate={combine(afterToday(), allowedMaxDays(90))}
            container={() => document.querySelector('.t-wrapper__main')}
            {...props}
        />
    );
};

export default DateRangePicker;