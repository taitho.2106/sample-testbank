import React, { useEffect, useState } from 'react';

import dayjs from 'dayjs';
import { Button } from 'rsuite';

import IconReset from '@/assets/icons/reset-icon.svg';
import useTranslation from '@/hooks/useTranslation';

import DateSelectPicker from './DateSelectPicker'
import styles from './FilterContainer.module.scss';

const FilterContainer = ({ children, onReset, dateRangeProps }) => {
    const { t } = useTranslation()
    const [selectedDateStart, setSelectedDateStart] = useState(dateRangeProps.value[0]);
    const [selectedDateEnd, setSelectedDateEnd] = useState(dateRangeProps.value[1]);

    const handleDateStart = (timestamp) => {
        const defaultDateStart = dayjs(timestamp).startOf('date').toDate()
        setSelectedDateStart(defaultDateStart)
    }

    const handleDateEnd = (timestamp) => {
        const defaultDateEnd = dayjs(timestamp).endOf('date').toDate()
        setSelectedDateEnd(defaultDateEnd)
    }
    
    const currentDate = new Date();

    const disabledDateStart = (date) => {
        const isAfterToday = dayjs(date).isAfter(currentDate, 'date')
        const isAfterEnd = dayjs(date).isAfter(selectedDateEnd)
        return isAfterEnd || isAfterToday
    }

    const disabledDateEnd = (date) => {
        const isAfterToday = dayjs(date).isAfter(currentDate, 'date')
        const isBeforeStart = dayjs(date).isBefore(selectedDateStart)
        return isBeforeStart || isAfterToday
    }

    useEffect(() => {
        setSelectedDateStart(dateRangeProps.value[0])
        setSelectedDateEnd(dateRangeProps.value[1])
    }, [dateRangeProps.value])

    const handleApply = () => {
        dateRangeProps.onChange([selectedDateStart, selectedDateEnd])
    }

    return (
        <div className={styles.filterContainer}>
            {children}
            <DateSelectPicker
                value={selectedDateStart}
                onChange={handleDateStart}
                disabledDate={disabledDateStart}
            />
            <DateSelectPicker
                value={selectedDateEnd}
                onChange={handleDateEnd}
                disabledDate={disabledDateEnd}
            />

            {!!onReset && (
                <Button
                    appearance="ghost"
                    className={styles.btnReset}
                    onClick={onReset}
                >
                    {t("common")["reset"]}
                    <IconReset />
                </Button>
            )}
            <Button
                className={styles.applyBtn}
                onClick={handleApply}
            >
                {t("common")["apply"]}
            </Button>
        </div>
    );
};

export default FilterContainer;