import React, { useState } from 'react';

import classNames from 'classnames';
import { DatePicker } from 'rsuite';

import styles from './DateSelectPicker.module.scss';

const DateSelectPicker = ({
    value,
    onChange = () => null,
    disabledDate,
    ...props
}) => {
    const [key, setKey] = useState((new Date()).getTime()) // trick to render datePicker again

    const handleChange = (date) => {
        if (disabledDate && disabledDate(date)) {
            return;
        }
        if (onChange) {
            onChange(date)
        }
    }

    return (
        <DatePicker
            key={key}
            className={classNames(styles.dateSelectPickerCustom, props.disabled && styles.disabled)}
            cleanable={false}
            oneTap={false}
            disabled={false}
            format={'dd/MM/yyyy'}
            value={value}
            placement='bottomEnd'
            onSelect={handleChange}
            onClose={() => setKey((new Date()).getTime())}
            disabledDate={disabledDate}
            style={{ width: 150 }}
            container={() => document.querySelector('.t-wrapper__main')}
            {...props}
        />
    );
};

export default DateSelectPicker;