import dayjs from 'dayjs';
import React, { useEffect, useMemo, useState } from 'react';

import apiConfig from '@/constants/apiConfig';
import { DATE_VALUE_FORMAT } from '@/constants/index';
import useFetch from '@/hooks/useFetch';
import { formatNumber } from '@/utils/index';
import { getInitDate, reverseCloneArray } from "../util";

import FilterContainer from '../Common/FilterContainer';
import SelectPicker from '../Common/SelectPicker';
import ServicePackage from '../ServicePackage';
import Loading from '@/componentsV2/Common/Controls/Loading';
import Table from '../Table';

import styles from './index.module.scss';
import { paths, USER_ROLES } from "../../../../interfaces/constants";
import { SALE_ITEM_TYPE } from "../../../../constants";
import { useRouter } from "next/router";
import IconNotice from "../../../../assets/icons/info-outlined-circle.svg";
import useTranslation from '@/hooks/useTranslation';

const ServicePackageDetail = () => {
    const {t} = useTranslation()
    const userRoleOptions = [
        { label: 'Giáo viên', value: USER_ROLES.Teacher },
        { label: 'Học viên', value: USER_ROLES.Student },
    ]
    const { query, replace, isReady } = useRouter();
    const { execute, loading, data } = useFetch(apiConfig.dashboard.servicePackage);
    const { execute: executeDetail, loading: loadingDetail, data: dataDetail } = useFetch(apiConfig.dashboard.servicePackageDetail);
    const { execute: executeServicePackageList, data: dataServicePackage } = useFetch(apiConfig.servicePackage.list);
    const { execute: executeComboUnitTestList, data: dataComboUnitTest } = useFetch(apiConfig.comboUnitTest.list);

    const [dateFilter, setDateFilter] = useState(getInitDate());
    const [servicePackageSelected, setServicePackageSelected] = useState('');
    const roleDefault = +query?.role
    const [userRoleSelected, setUserRoleSelected] = useState(roleDefault);
    useEffect(() => {
        if (isNaN(Number(roleDefault)) || ![USER_ROLES.Teacher, USER_ROLES.Student].includes(roleDefault)) {
            replace(`${paths.dashboardServicePackage}?role=${USER_ROLES.Teacher}`)
        }
        setUserRoleSelected(roleDefault)
    }, [roleDefault]);
    
    const servicePackageOptions = useMemo(() => {
        let options = [
            { label: t(`${t("dashboard")["all-service-packages"]}`), value: '' }
        ];
        if (userRoleSelected === USER_ROLES.Teacher && dataServicePackage) {
            options.push(...dataServicePackage.map(el => ({ label: el.name, value: el.id })));
        }
        if (userRoleSelected === USER_ROLES.Student && dataComboUnitTest) {
            options.push(...dataComboUnitTest.map(el => ({ label: el.name, value: el.id })));
            options.push({label: t(`${t("dashboard")["single-unitTest"]}`), value: SALE_ITEM_TYPE.UNIT_TEST })
        }
        
        return options;
    }, [dataServicePackage, userRoleSelected, dataComboUnitTest]);

    useEffect(() => {
        const [startDate, endDate] = dateFilter;
        const params = {
            startDate: dayjs(startDate).format(DATE_VALUE_FORMAT),
            endDate: dayjs(endDate).format(DATE_VALUE_FORMAT),
        }
        
        if (userRoleSelected) {
            params.userRoles = userRoleSelected;
        }

        if (servicePackageSelected) {
            params.packages = servicePackageSelected;
        }

        execute({
            params,
            onCompleted: () => { },
            onError: () => { }
        });

        executeDetail({
            params,
            onCompleted: () => { },
            onError: () => { }
        });
    }, [dateFilter, servicePackageSelected, userRoleSelected]);

    useEffect(() => {
        executeServicePackageList({
            onCompleted: res => console.log({ res }),
        });
        executeComboUnitTestList({
            onCompleted: res => console.log({ res }),
        });
    }, []);
    const onChangeSelectUser = (value) => {
        setUserRoleSelected(value)
        setServicePackageSelected('');
        replace(`${paths.dashboardServicePackage}?role=${value}`)
    }
    return (
        <div className={styles.servicePackageDetail}>
            <div className={styles.head}>
                <h3>{t("dashboard")["report-detail"]}</h3>
                <FilterContainer
                    dateRangeProps={{
                        value: dateFilter,
                        onChange: setDateFilter,
                    }}
                    onReset={() => {
                        setDateFilter(getInitDate());
                        setServicePackageSelected('');
                        setUserRoleSelected(+query?.role);
                    }}
                >
                    <SelectPicker
                        data={userRoleOptions}
                        value={userRoleSelected}
                        onChange={onChangeSelectUser}
                    />
                    <SelectPicker
                        data={servicePackageOptions}
                        value={servicePackageSelected}
                        onChange={setServicePackageSelected}
                        disabled={!userRoleSelected}
                    />
                </FilterContainer>
            </div>
            <div className={styles.main}>
                <div className={styles.summary}>
                    {loading ? (
                        <div className={styles.loadingContainer}><Loading color="#B5B5B6" /></div>
                    ) : (
                        <ServicePackage
                            loading={loading}
                            orders={data?.orders}
                            className={styles.summaryContent}
                            role={userRoleSelected}
                        />
                    )}
                </div>
                <div className={styles.list}>
                    {loadingDetail ? (
                        <div className={styles.loadingContainer}><Loading color="#B5B5B6" /></div>
                    ) : (
                        <Table
                            columns={[
                                { title: t("dashboard")["date"], dataIndex: 'date' },
                                { title: t("dashboard")["purchase"], dataIndex: 'orderCount', render: (_, colData) => formatNumber(colData, 0) },
                                { title: `${t("dashboard")["revenue"]} (VNĐ)`, dataIndex: 'totalAmount', render: (_, colData) => formatNumber(colData, 0) },
                            ]}
                            data={reverseCloneArray(dataDetail?.orders)}
                        />
                    )}
                </div>
            </div>
        </div>
    );
};

export default ServicePackageDetail;