import { useState } from 'react';

import useTranslation from '@/hooks/useTranslation';

import { getInitDate } from "./util";

import ServicePackageContainer from './ServicePackageContainer';
import TurnoverSummaryContainer from './TurnoverSummaryContainer';
import UserContainer from './UserContainer';
import FilterContainer from './Common/FilterContainer';

import styles from './index.module.scss';
import StatisticsCard from "./StatisticsCard";

const DashboardMain = () => {
    const {t} = useTranslation()
    const [dateFilter, setDateFilter] = useState(getInitDate());
    return (
        <div className={styles.dashboardMain}>
            <StatisticsCard />
            <div className={styles.head}>
                <h3>{t('dashboard')["activity-overview"]}</h3>
                <div className={styles.filter}>
                    <FilterContainer
                        dateRangeProps={{
                            value: dateFilter,
                            onChange: setDateFilter,
                        }}
                        onReset={() => setDateFilter(getInitDate())}
                    />
                </div>
            </div>

            <TurnoverSummaryContainer dateFilter={dateFilter} />

            <UserContainer dateFilter={dateFilter} />

            <ServicePackageContainer dateFilter={dateFilter} />
        </div>
    );
};

export default DashboardMain;