import dayjs from 'dayjs';
import { useEffect, useState } from "react";

import apiConfig from '@/constants/apiConfig';
import { DATE_VALUE_FORMAT } from '@/constants/index';
import useFetch from '@/hooks/useFetch';

import Loading from '@/componentsV2/Common/Controls/Loading';
import useTranslation from '@/hooks/useTranslation';

import ServicePackage from './ServicePackage';

import styles from './ServicePackageContainer.module.scss';
import Switch from "../../Common/Controls/Switch";
import { USER_ROLES } from "../../../interfaces/constants";

const ServicePackageContainer = ({ dateFilter }) => {
    const { t } = useTranslation()
    const { execute, loading, data } = useFetch(apiConfig.dashboard.servicePackage, { delay: 500 });
    const [role, setRole] = useState(USER_ROLES.Teacher)

    useEffect(() => {
        const [startDate, endDate] = dateFilter;

        execute({
            params: {
                startDate: dayjs(startDate).format(DATE_VALUE_FORMAT),
                endDate: dayjs(endDate).format(DATE_VALUE_FORMAT),
                userRoles: role
            },
            onCompleted: () => { },
            onError: () => { }
        });
    }, [ dateFilter, role ]);
    
    const onchangeRole = (e) => {
        const { checked } = e.target
        if (checked) {
            setRole(USER_ROLES.Student)
        } else {
            setRole(USER_ROLES.Teacher)
        }
    }

    return (
        <div className={styles.servicePackage}>
            {data?.orders &&
                <div className={styles.headerContainer}>
                    <div className={styles.head}>
                        <h3>{t("dashboard")["package-report"]}</h3>
                        <h4>{t("dashboard")["package-report-desc"]}</h4>
                    </div>
                    <Switch
                        height={48}
                        width={250}
                        labelLeft={t("teacher")}
                        labelRight={t("student")}
                        onChange={onchangeRole}
                        checked={role === USER_ROLES.Student}
                    />
                </div>}

            {loading && (
                <div className={styles.loadingContainer}>
                    <Loading color="#B5B5B6" />
                </div>
            )}

            {data?.orders && (
                <ServicePackage
                    orders={data?.orders}
                    showViewDetail
                    role={role}
                />
            )}
        </div>
    );
};

export default ServicePackageContainer;