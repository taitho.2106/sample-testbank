import React from 'react';

import useTranslation from '@/hooks/useTranslation';

import styles from './NoData.module.scss'
const NoData = () => {
    const {t} = useTranslation()
    return (
        <div className={styles.noData}>
            <span>{t("no-data")}</span>
        </div>
    );
};

export default NoData;