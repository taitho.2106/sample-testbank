import classNames from 'classnames'

import styles from './CustomButtonSelect.module.scss'

type Props = {
    className?: string,
    labelText: string,
    isSelected?: boolean,
    onClick?: (val: any) => void
    disabled?: boolean
}

function CustomButtonSelect({
    className,
    labelText,
    isSelected = false,
    disabled,
    onClick,
    ...props
}: Props) {
    return (
        <div
            className={classNames(styles.customButtonCheckbox, {
                [styles.checked]: isSelected,
                className
            })}
            {...props}
        >
            <button
                className={classNames(styles.buttonBox, {
                    [styles.checked]: isSelected,
                    [styles.disabled]: disabled,
                })}
                onClick={onClick}
                disabled={disabled}
            >
                <span className={styles.labelText}>{labelText}</span>
            </button>
        </div>
    )
}

export default CustomButtonSelect
