import { useEffect, useState } from 'react'

import classNames from 'classnames'
import { useSession } from 'next-auth/client';
import { useRouter } from 'next/router';
import { AiOutlineDownload } from 'react-icons/ai'
import Modal from 'react-modal'
import { Popover, Tooltip, Whisper } from 'rsuite';

import AdvanceIcon from '@/assets/icons/magic-star.svg';
import RightSideIcon from '@/assets/icons/right-side-btn-icon.svg'
import { studentPricings } from '@/componentsV2/Home/data/price';
import useAuth from '@/hooks/useAuth';
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'
import useSeries from '@/hooks/useSeries';
import useTranslation from '@/hooks/useTranslation';
import { PACKAGES, USER_ROLES } from '@/interfaces/constants';
import ExpandIcon from 'assets/icons/expand_arrow.svg'
import PopoverUpgradeContent from 'components/molecules/unitTestTable/PopoverUpgradeContent';

import CloseIcon from "../../../assets/icons/close.svg"
import { unitTestTypeOptions } from '../../../constants'
import ButtonCustom from '../SideBar/ButtonCustom/Button'
import {
    excludedOptions01,
    excludedOptions02,
    gradeHasNoOutline,
    filterOptionsByExcluded,
    linkSeriesOutline,
} from './data'
import styles from './DrawerFilter.module.scss'
import FilterSection from './FilterSection'

interface UnitTestTypeProps {
    onSubmit?: (value?: any) => void
    filterUnitTestTypes?: any[]
    onReset?: () => void
}

Modal.setAppElement('#__next');

const DrawerFilter = ({ onSubmit, filterUnitTestTypes, onReset }: UnitTestTypeProps) => {
    const { t } = useTranslation()
    const { namePackage, listPlans, dataPlan } = useCurrentUserPackage();
    const { isSystem } = useAuth();
    const [isOpen, setIsOpen] = useState(false);
    const [appliedFilter, setAppliedFilter] = useState(filterUnitTestTypes)

    const toggleModal = () => {
        setIsOpen(prev => !prev)
    }

    const handleResetAll = () => {
        setIsOpen(false)
        onReset()
    }

    const handleSubmit = () => {
        setIsOpen(false)
        onSubmit(appliedFilter)
    }

    const checkLocked = (accessPackages: any[]) => {
        if (isSystem) {
            return false;
        }

        return accessPackages?.length && !accessPackages.includes(namePackage.code);
    }

    useEffect(() => {
        if (isOpen) {
            setAppliedFilter(filterUnitTestTypes);
        }
    }, [isOpen]);

    const [session] = useSession()
    const user: any = session.user
    const userRole = user.user_role_id
    const { query } = useRouter()
    const { getSeries } = useSeries()

    const dataPackageAdvance = studentPricings.teacher.find(item => item.code === PACKAGES.ADVANCE.CODE)?.descriptions || []
    const packageAdvance = listPlans.find((item: any) => item.code === PACKAGES.ADVANCE.CODE)
    const priceAdvance = packageAdvance?.price || 0
    const itemIdAdvance = packageAdvance?.id || ''

    const seriesHasUpdatedOutline = linkSeriesOutline.map(item => item.series_id) // get all series has outline documents 
    const currentSeriesId = +query?.series_id
    const findGradeBySeriesId = (seriesId: number) => {
        const objectSeries = getSeries.find((item: any) => item.code === seriesId)?.grade_id
        return objectSeries
    }

    const [currentGrade, setCurrentGrade] = useState<any>('');

    useEffect(() => {
        const foundGrade = findGradeBySeriesId(currentSeriesId);
        setCurrentGrade(foundGrade);
    }, [
        getSeries,
        currentSeriesId
    ]);

    const renderOptionsWithGrade = (grade: any) => {
        if (userRole !== USER_ROLES.Teacher && userRole !== USER_ROLES.Student) {
            return unitTestTypeOptions
        }
        if (gradeHasNoOutline.includes(grade)) {
            return unitTestTypeOptions.filter(group => group.groupOrder !== 3);
            // Không hiển thị nhóm "Luyện thi"
        }
        if (['G8', 'G9'].includes(grade)) {
            return filterOptionsByExcluded(unitTestTypeOptions, excludedOptions01);
        }
        if (['G10', 'G11', 'G12'].includes(grade)) {
            return filterOptionsByExcluded(unitTestTypeOptions, excludedOptions02);
        }
    };

    const handleDownload = (seriesId: number) => {
        const directoryLink = linkSeriesOutline.find(item => item.series_id === seriesId).zipName
        window.open(`${window.location.origin}/contents/SeriesOutline/${directoryLink}`, '_blank')
    };

    return (
        <>
            <button
                className={classNames(styles.btnExpand, {
                    [styles.expandMode]: isOpen,
                })}
                onClick={toggleModal}
            >
                <div className={styles.content}>
                    {t(`${t("drawer-filter-unit-test")["tag"]}`)} <RightSideIcon />
                </div>
            </button>
            <Modal
                isOpen={isOpen}
                onRequestClose={toggleModal}
                overlayClassName={styles.overlay}
                className={styles.drawer}
                closeTimeoutMS={300}
            >
                <div className={styles.filterContainer}>
                    <Whisper
                        placement="bottom"
                        trigger="hover"
                        enterable
                        speaker={
                            <Popover className={styles.popoverText}>
                                {t("common")["collapse"]}
                            </Popover>
                        }
                    >
                        <button
                            className={classNames(styles.btnExpandDrawer, {
                                [styles.expandMode]: isOpen,
                            })}
                            onClick={toggleModal}
                        >
                            <ExpandIcon />
                        </button>
                    </Whisper>
                    <div className={styles.filterBox}>
                        <div className={styles.header}>
                            <span>{t(`${t("drawer-filter-unit-test")["title"]}`)}</span>
                            <div className={styles.actionSection}>
                                <div
                                    className={styles.reset}
                                    onClick={handleResetAll}
                                >
                                    {t("common")["reset"]}
                                </div>
                                <ButtonCustom type="none" onClick={() => setIsOpen(false)}>
                                    <CloseIcon />
                                </ButtonCustom>
                            </div>
                        </div>
                        <div className={styles.body}>
                            {currentGrade && renderOptionsWithGrade(currentGrade)?.map((group: any, gIndex: number) => {
                                // const isLocked = checkLocked(group.accessPackages);
                                return (
                                    <FilterSection
                                        options={group.options}
                                        groupName={`${t('unit-test-group-name')[group.groupName]}`}
                                        // locked={isLocked}
                                        filterSelected={appliedFilter}
                                        setFilterSelected={setAppliedFilter}
                                        key={gIndex}
                                        keyName="unit-test-group"
                                    />
                                )
                            })}
                            {userRole === USER_ROLES.Teacher && !gradeHasNoOutline.includes(currentGrade) &&
                                (
                                    <Whisper
                                        trigger={!seriesHasUpdatedOutline.includes(currentSeriesId) ? 'hover' : 'none'}
                                        placement="bottom"
                                        speaker={
                                            <Tooltip className={styles.tooltipFailed}>
                                                <span dangerouslySetInnerHTML={{
                                                    __html: t("drawer-filter-unit-test")["tooltip-failed"]
                                                }} />
                                            </Tooltip>
                                        }
                                    >
                                        <button
                                            className={classNames(styles.btnDownload, {
                                                [styles.upgradeAdvance]: dataPlan.code === PACKAGES.BASIC.CODE,
                                                [styles.disabled]: !seriesHasUpdatedOutline.includes(currentSeriesId),
                                            })}
                                            onClick={() => {
                                                if (
                                                    dataPlan.code !== PACKAGES.BASIC.CODE &&
                                                    seriesHasUpdatedOutline.includes(currentSeriesId)
                                                ) handleDownload(currentSeriesId)
                                            }}
                                        >
                                            <AiOutlineDownload className={styles.downloadIcon} />
                                            <span className={styles.downloadText}>
                                                {t("drawer-filter-unit-test")["download-outline"]}
                                            </span>
                                            <div
                                                onClick={(e) => {
                                                    e.stopPropagation()
                                                    e.preventDefault()
                                                }}
                                            >
                                                {seriesHasUpdatedOutline.includes(currentSeriesId) && dataPlan.code === PACKAGES.BASIC.CODE && (
                                                    <Whisper
                                                        trigger='hover'
                                                        placement="leftEnd"
                                                        preventOverflow
                                                        enterable
                                                        speaker={
                                                            <Popover className={styles.popoverModal} arrow={false}>
                                                                <PopoverUpgradeContent
                                                                    dataPackage={dataPackageAdvance}
                                                                    price={priceAdvance}
                                                                    itemId={itemIdAdvance}
                                                                />
                                                            </Popover>
                                                        }
                                                    >
                                                        <div style={{ position: 'relative' }}>
                                                            <AdvanceIcon className={styles.packageIcon} />
                                                        </div>
                                                    </Whisper>
                                                )}
                                            </div>
                                        </button>
                                    </Whisper>
                                )}
                        </div>
                        <div className={styles.footer}>
                            <ButtonCustom
                                className={styles.applyBtn}
                                onClick={handleSubmit}
                            >
                                {t("common")["apply"]}
                            </ButtonCustom>
                        </div>
                    </div>
                </div>
            </Modal>
        </>
    )
}

export default DrawerFilter