import { UNIT_TEST_TYPE } from '../../../constants'

export const gradeHasNoOutline = ['G1', 'G2', 'G3', 'G4', 'G5', 'G6', 'G7']

export const excludedOptions01 = [UNIT_TEST_TYPE.FOCUS_REINFORCE, UNIT_TEST_TYPE.FOCUS_SUPPLEMENT];

export const excludedOptions02 = [UNIT_TEST_TYPE.FOCUS_GOOD_STUDENT];

export const filterOptionsByExcluded = (typeOptions, excludedOptions) => {
    const updatedOptions = typeOptions.map((group) => {
        const filteredOptions = group.options.filter((option) => !excludedOptions.includes(option.value));

        return {
            ...group,
            options: filteredOptions,
        };
    });

    return updatedOptions;
};

export const linkSeriesOutline = [
    {
        series_id: 159,
        name: "Tiếng Anh 8 i-Learn Smart World",
        zipName: 'TA8 ISW.zip',
    },
    {
        series_id: 160,
        name: "Tiếng Anh 8 Right On!",
        zipName: 'TA8 RO.zip',
    },
    {
        series_id: 70,
        name: "Tiếng Anh 10 i-Learn Smart World",
        zipName: 'TA10 ISW.zip',
    },
    {
        series_id: 78,
        name: "Tiếng Anh 10 BRIGHT",
        zipName: 'TA10 Bright.zip',
    },
    {
        series_id: 161,
        name: "Tiếng anh 11 BRIGHT",
        zipName: 'TA11 Bright.zip',
    },
    {
        series_id: 162,
        name: "Tiếng Anh 11 i-Learn Smart World",
        zipName: 'TA11 ISW.zip',
    },
]