import React from 'react'

import classNames from 'classnames'
import { Tooltip, Whisper } from 'rsuite'

import styles from './CustomCheckbox.module.scss'

type CheckboxProps = {
    ref?: any,
    className?: string,
    labelName?: string,
    disabled?: boolean,
    onClick?: (value?: any) => void
    state: string,
}

const CHECKBOX_STATES: any = {
    checked: 'checked',
    indeterminate: 'indeterminate',
    empty: 'empty',
};

const CustomCheckbox = ({
    className,
    labelName,
    onClick,
    disabled,
    state = CHECKBOX_STATES.empty,
}: CheckboxProps) => {
    return (
        <div
            className={classNames(styles.customCheckbox, disabled && styles.disabled, styles[CHECKBOX_STATES[state]])}
            onClick={() => !disabled && onClick()}
        >
            <input
                type="checkbox"
                className={styles.checkBox}
                disabled={disabled}
            />
            <div className={classNames(styles.labelName, className)}>{labelName}</div>
            {/* {disabled && (
                <Whisper
                    placement="bottom"
                    controlId="premium-only"
                    trigger="hover"
                    speaker={<Tooltip className={styles.tooltipPremium}>Bạn cần nâng cấp gói cước để sử dụng nhóm đề thi này</Tooltip>}
                >
                    <div className={styles.premiumOnlyIcon}>
                        <PremiumIcon />
                    </div>
                </Whisper>
            )} */}
        </div>
    )
}

CustomCheckbox.CHECKBOX_STATES = CHECKBOX_STATES;

export default CustomCheckbox

