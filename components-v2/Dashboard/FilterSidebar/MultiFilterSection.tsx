

import React, { useCallback, useRef, useState } from 'react'

import classNames from 'classnames'

import ChevronIcon from '../../../assets/icons/arrow.svg'
import CustomCheckbox from './CustomCheckbox'
import FilterSection from './FilterSection'
import styles from './MultiFilterSection.module.scss'

interface Props  {
    parentFieldName: string,
    data: any[]
}

const checkAllSelected = (arr: any[]) => {
    if(arr.every(item => item === true)){
        return 'Checked'
    } else if(arr.some(item => item === true)) {
        return 'Indeterminate'
    } 
    return 'Empty'
}

const MultiFilterSection = ({parentFieldName, data}: Props) => {
    const [showFull, setShowFull] = useState(false)
    const [selectChild, setSelectChild] = useState(Array.from(Array(data.length),()=> false))
    const [parentClicked, setParentClicked] = useState(false)    
    const selectChildRefs = useRef(selectChild)

    const handleParentClick = () => {
        if (!parentClicked) {
            setSelectChild(Array.from(Array(data.length), () => true))
        } else {
            const prevSelect = selectChild.map((item, index) => selectChildRefs.current[index]);
            setSelectChild(prevSelect)
        }
        setParentClicked(!parentClicked)
    }


    const handleSelectChild = (index: number) => {
        const newSelectChild = [...selectChild];
        newSelectChild[index] = !newSelectChild[index];
        setSelectChild(newSelectChild);
    };

    // console.log('selectChildRefs------', selectChildRefs);
    // console.log('selectChild------multi', selectChild);
    return (
        <>
            <div className={styles.parentFieldSubject}>
                <div className={styles.parentFieldSubjectTitle}>
                <CustomCheckbox 
                    labelName={parentFieldName} 
                    onClick={() => handleParentClick()}
                    value={checkAllSelected(selectChild)}
                />
                <ChevronIcon 
                    onClick={() => setShowFull(!showFull)}
                    className={classNames(styles.chevronIcon, {
                        [styles.active]: showFull,
                    })} />

                </div>
            </div>
            {showFull && data.map((field: any, fIndex: number) => {
                return (
                    <div key={fIndex}>
                        <FilterSection 
                            data={field.dataSelect} 
                            fieldName={field.subject} 
                            // ref={selectChildRefs} 
                            onSelect={() => handleSelectChild(fIndex)}
                            className={'child-sub-title'}
                            // isSelectedChild={selectChild[fIndex]}
                        />
                    </div>
                )
            })}
        </>
    )
}

export default MultiFilterSection