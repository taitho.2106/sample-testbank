import React, { useMemo, useState } from 'react'

import classNames from 'classnames'

import ChevronIcon from '../../../assets/icons/chevron.svg'
import CustomButtonSelect from './CustomButtonSelect'
import CustomCheckbox from './CustomCheckbox'
import styles from './FilterSection.module.scss'
import useTranslation from "@/hooks/useTranslation";
interface Props {
    className?: string
    options: any[]
    groupName: string
    locked?: boolean
    setFilterSelected?: (prev: any) => void
    filterSelected: any[],
    keyName?:string
}

const collapseCount = 99;

const FilterSection = ({
    className,
    options,
    groupName,
    locked,
    setFilterSelected,
    filterSelected,
    keyName = ''
}: Props) => {
    const optionSelected = options.filter(option => filterSelected.includes(option.value)).length;
    const { t } = useTranslation();
    const bulkState = useMemo(() => {
        const totalOptions = options.length

        if (!optionSelected) {
            return CustomCheckbox.CHECKBOX_STATES.empty;
        }

        return optionSelected === totalOptions
            ? CustomCheckbox.CHECKBOX_STATES.checked
            : CustomCheckbox.CHECKBOX_STATES.indeterminate;

    }, [filterSelected]);

    const [showMore, setShowMore] = useState(false)
    // const [showOptions, setShowOptions] = useState(bulkState !== CustomCheckbox.CHECKBOX_STATES.empty);
    const [showOptions, setShowOptions] = useState(true)

    const hasShowMore = options.length > collapseCount;
    const optionValues = options.map(({ value }) => value);

    const hanldeToggleBulk = () => {
        if (bulkState === CustomCheckbox.CHECKBOX_STATES.checked) {
            // deselect all
            setFilterSelected((prev: any) => prev.filter((el: number) => !optionValues.includes(el)));
        } else {
            // select all
            setShowOptions(true);
            setFilterSelected((prev: any) => ([...new Set([...prev, ...optionValues])]));
        }
    }

    const handleToggleOnce = (optionValue: number, isSelected: boolean) => {
        setFilterSelected((prev: any) => isSelected ? [...prev.filter((el: number) => el !== optionValue)] : [...prev, optionValue]);
    }

    return (
        <div className={styles.fieldSubject}>
            <div className={styles.fieldSubjectTitle}>
                <div className={styles.selectSection}>
                    <CustomCheckbox
                        labelName={groupName}
                        disabled={locked}
                        onClick={hanldeToggleBulk}
                        className={className}
                        state={bulkState}
                    />
                    {(!locked && optionSelected !== 0) && 
                        (
                            <div className={styles.selectNotify}>
                                {optionSelected}
                            </div>
                        )
                    }
                </div>
                <div
                    className={styles.iconBox}
                    onClick={() => setShowOptions(!showOptions)}
                >
                    <ChevronIcon className={classNames(styles.chevronIcon, showOptions && styles.active)} />
                </div>
            </div>

            <div className={classNames(styles.fieldSubjectItems, showOptions && styles.showOptions)}>
                <div className={styles.list}>
                    {(showMore ? options : options.slice(0, collapseCount))
                        .map((option: any) => {
                            console.log({option});
                            const isOptionSelected = filterSelected.findIndex(value => option.value === value) !== -1;
                            return (
                                <CustomButtonSelect
                                    isSelected={isOptionSelected}
                                    labelText={!!keyName.length ? t(keyName)[option.label] : option.label}
                                    onClick={() => handleToggleOnce(option.value, isOptionSelected)}
                                    disabled={locked}
                                    key={option.value}
                                />
                            )
                        })
                    }
                </div>
                {hasShowMore && (
                    <div
                        className={classNames(styles.showMoreInfo, showMore && styles.show)}
                        onClick={() => setShowMore(prev => !prev)}
                    >
                        {showMore ? t('common')['collapse'] : t('common')['show-more']}
                        <ChevronIcon className={classNames(styles.chevronIcon, showMore && styles.active)} />
                    </div>
                )}
            </div>
        </div>
    )
}

export default FilterSection