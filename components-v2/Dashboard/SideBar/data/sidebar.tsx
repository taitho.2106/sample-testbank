import ExamBankIcon from '@/assets/icons/exam-bank.svg'
import ITestExamBank from '@/assets/icons/i-test-exam-bank.svg'
import AssignIcon from '@/assets/icons/ic-assign.svg'
import ITestClasses from '@/assets/icons/ic-classes.svg'
import OverviewIcon from '@/assets/icons/overview-sidebar.svg'
import CalendarIcon from '@/assets/icons/calendar.svg'
import { paths, USER_ROLES } from '@/interfaces/constants'

import { propsType } from './type'
export const SIDENAV_ITEMS1: propsType[] = [
  {
    id: 0,
    name: 'overview',
    url: paths.dashboard,
    icon: <OverviewIcon />,
    lockRole: [USER_ROLES.Student],
    hideRole: [USER_ROLES.Operator],
    activePaths: [
      { url: paths.dashboardServicePackage },
      { url: paths.dashboardTurnover },
      { url: paths.dashboardUser },
      { url: paths.dashboardClassRanking },
    ],
  },
  {
    id: 5,
    name: 'test-schedule',
    url: paths.testSchedule,
    icon: <CalendarIcon />,
    access_role: [-1, USER_ROLES.Teacher, USER_ROLES.Student],
    lockRole: [],
    hideRole: [-1, USER_ROLES.Operator],
  },
  {
    id: 6,
    name: 'unit-test-assigned',
    url: paths.assignedUnitTest,
    icon: <AssignIcon />,
    access_role: [-1, USER_ROLES.Student],
    lockRole: [],
    hideRole: [USER_ROLES.Operator,USER_ROLES.Teacher],
    activePaths: [
      { url: paths.assignedUnitTest, query: {} },
      { url: paths.assignedUnitTestSlug, query: {} }
    ]
  },
  {
    id: 1,
    name: 'my-test-bank',
    icon: <ExamBankIcon />,
    access_role: [-1, USER_ROLES.Teacher],
    lockRole: [],
    hideRole: [],
    list: [
      {
        id: 1,
        name: 'exam-format',
        url: '/templates?m=mine',
        baseUrl: [{ url: '/templates/[templateSlug]', query: { m: 'mine' } }],
        active_role: [-1, USER_ROLES.Teacher],
        lockRole: [],
      },
      {
        id: 2,
        name: 'questions-bank',
        url: '/questions?m=mine',
        baseUrl: [{ url: '/questions/[questionSlug]', query: { m: 'mine' } }],
        active_role: [-1, USER_ROLES.Teacher],
        lockRole: [],
      },
      {
        id: 3,
        name: 'list-unit-tests',
        url: '/unit-test?m=mine',
        baseUrl: [
          {
            url: '/unit-test/[templateSlug]',
            query: { m: 'mine' },
          },
          { url: '/unit-test/[templateSlug]/export-zip', query: { m: 'mine' } },
        ],
        active_role: [-1, USER_ROLES.Teacher],
        lockRole: [],
      },
    ],
  },
  {
    id: 1,
    name: 'my-test-bank',
    url: '/unit-test?m=mine',
    icon: <ExamBankIcon />,
    access_role: [-1, USER_ROLES.Student],
    lockRole: [],
    hideRole: [],
    activePaths: [
      { url: paths.unitTestSlug, query: { m: 'mine' } },
      { url: paths.unitTest, query: { m: 'mine' } },
      { url: paths.student_unit_test_result, query: {} },
    ],
  },
  {
    id: 2,
    name: 'teacher-test-bank',
    icon: <ITestExamBank />,
    access_role: [],
    lockRole: [USER_ROLES.Teacher, USER_ROLES.Student],
    hideRole: [-1, USER_ROLES.Teacher, USER_ROLES.Student],
    list: [
      {
        id: 1,
        name: 'exam-format',
        url: '/templates?m=teacher',
        baseUrl: [
          { url: '/templates/[templateSlug]', query: { m: 'teacher' } },
        ],
        lockRole: [],
      },
      {
        id: 2,
        name: 'questions-bank',
        url: '/questions?m=teacher',
        baseUrl: [
          { url: '/questions/[questionSlug]', query: { m: 'teacher' } },
        ],
        lockRole: [],
      },
      {
        id: 3,
        name: 'list-unit-tests',
        url: '/unit-test?m=teacher',
        baseUrl: [
          { url: '/unit-test/[templateSlug]', query: { m: 'teacher' } },
          {
            url: '/unit-test/[templateSlug]/export-zip',
            query: { m: 'teacher' },
          },
        ],
        lockRole: [],
      },
    ],
  },
  {
    id: 3,
    name: "i-Test-test-bank",
    icon: <ITestExamBank />,
    lockRole: [],
    hideRole: [-1, USER_ROLES.Student],
    list: [
      {
        id: 1,
        name: 'exam-format',
        url: '/templates',
        baseUrl: [{ url: '/templates/[templateSlug]' }],
        active_role: [USER_ROLES.Operator, USER_ROLES.Teacher, USER_ROLES.Student],
        lockRole: [USER_ROLES.Student],
      },
      {
        id: 2,
        name: 'questions-bank',
        url: '/questions',
        baseUrl: [{ url: '/questions/[questionSlug]' }],
        active_role: [USER_ROLES.Operator, USER_ROLES.Teacher, USER_ROLES.Student],
        lockRole: [ USER_ROLES.Student],
      },
      {
        id: 3,
        name: 'list-unit-tests',
        url: '/unit-test',
        baseUrl: [
          { url: '/unit-test' },
          { url: '/unit-test/[templateSlug]' },
          { url: '/unit-test/[templateSlug]/export-zip' },
          { url: '/unit-test/series-detail' },
        ],
        active_role: [USER_ROLES.Operator, USER_ROLES.Teacher, USER_ROLES.Student],
        regexUrls: [
          /(\?g=\w+)*(\&s=\w)/gm, //open by grade and series
        ],
        lockRole: [USER_ROLES.Student],
      },
      {
        id: 4,
        name: 'unit-test-integrated-partners',
        url: '/integrate-exam',
        baseUrl: [{ url: '/integrate-exam/[integrateExamSlug]' }],
        active_role: [USER_ROLES.Operator],
        lockRole: [USER_ROLES.Teacher],
      },
    ],
  },
  {
    id: 4,
    name: 'classroom-management',
    icon: <ITestClasses />,
    access_role: [-1, USER_ROLES.Teacher],
    lockRole: [],
    hideRole: [-1,USER_ROLES.Operator],
    list: [
      {
        id: 1,
        name: 'classroom',
        url: '/classes',
        baseUrl: [{ url: '/classes' }, { url: '/classes/[classeSlug]' }],
        active_role: [-1, USER_ROLES.Teacher],
        lockRole: [],
      },
      {
        id: 2,
        name: 'students',
        url: '/students/details',
        baseUrl: [{ url: '/students/[studentSlug]' }],
        active_role: [-1, USER_ROLES.Teacher],
        lockRole: [],
      },
      {
        id: 3,
        name: 'learning-result',
        url: paths.learningResult,
        baseUrl: [{ url: paths.learningResult, query: {} },{ url: paths.learningResultSlug }],
        active_role: [-1, USER_ROLES.Teacher],
        lockRole: [],
      },
    ],
  },
  {
    id: 3,
    name: 'i-Test-test-bank',
    icon: <ITestExamBank/>,
    url: paths.unitTest,
    lockRole: [],
    access_role: [-1, USER_ROLES.Student],
    activePaths: [
      { url: paths.detailSeries, query: {} },
      { url: paths.unitTest, query: {} },
    ],
  }
]