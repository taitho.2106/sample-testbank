import OverviewIcon from '@/assets/icons/overview-sidebar.svg'
import ExamBankIcon from '@/assets/icons/exam-bank.svg'
import ITestExamBank from '@/assets/icons/i-test-exam-bank.svg'
import { ReactElement } from 'react';
export type propsType = {
    id?:number,
    name?:string,
    icon?: ReactElement,
    url?:string;
    access_role?: number[];
    baseUrl?:{ url: string; query?: { [key: string]: string } }[],
    list?: ListProps[],
    lockRole?:number[],
    hideRole?:number[],
    activePaths?: baseUrlProps[],
    comingDate?: string,
}
export type ListProps = {
    id?: number,
    name?: string,
    url?: string,
    baseUrl?: baseUrlProps[],
    active_role?:  number[],
    regexUrls?:RegExp[],
    lockRole?:number[];
    comingDate?: string,
}
export type baseUrlProps = {
    url?:string,
    query?:any
}