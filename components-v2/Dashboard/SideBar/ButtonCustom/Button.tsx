import React from 'react'

import classNames from 'classnames'

import styles from './Button.module.scss'
type Props = {
    [x:string]:any,
    children?: any,
    className?: string,
    disable?: boolean,
    type?: string,
    iconLeft?:any,
    iconRight?:any,
    onClick?:(e?:any ,value?: any) => void
    onMouseDown?:(value?: any) => void
    onMouseMove?:(value?: any) => void
    onMouseUp?:(value?: any) => void
    buttonType?:string
    isLoading?:boolean
}
const ButtonCustom = ({
                    children,
                    className,
                    disable,
                    type = 'primary',
                    buttonType = "button",
                    iconLeft,
                    iconRight,
                    onClick,
                    onMouseDown,
                    onMouseMove,
                    onMouseUp,
                    isLoading = false,
                    ...props
                }:Props) => {
    return (
        <button disabled={disable}
                className={classNames(styles.button, styles[type], className,{
                    [styles.loading]:isLoading
                })}
                onClick={onClick}
                onMouseDown={onMouseDown}
                onMouseMove={onMouseMove}
                onMouseUp={onMouseUp}
                type={buttonType as 'button' | 'submit' | 'reset'}
                {...props}>
            {iconLeft}
            <>
                {isLoading && <Loading />}
                {children}
            </>
            {iconRight}
        </button>
    )
}

export default ButtonCustom

const Loading = () => {
    return <span className={styles.spinLoading}></span>
}