import React, { useContext, useState, Fragment } from 'react'

import classNames from 'classnames'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import Link from 'next/link'
import { Tooltip, Whisper } from 'rsuite'

import { SIDENAV_ITEMS1 } from '@/componentsV2/Dashboard/SideBar/data/sidebar'
import { ListProps } from "@/componentsV2/Dashboard/SideBar/data/type";
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'
import useTranslation from '@/hooks/useTranslation'
import { USER_ROLES } from '@/interfaces/constants'
import { AppContext, WrapperContext } from '@/interfaces/contexts'
import { userInRight } from '@/utils/index'

import ChevronIcon from '../../../assets/icons/arrow.svg'
import LogoCompact from '../../../assets/icons/logo-compact.svg'
import LogoIcon from '../../../assets/icons/logo.svg'
import styles from './SideBarDashboard.module.scss'

type SideBarProps = {
    isExpand: boolean,
    onToggle: () => void
}
const SideBarDashboard: React.FC<SideBarProps> = ({ isExpand, onToggle }) => {
    const { globalModal } = useContext(WrapperContext)
    const [session] = useSession()
    const router = useRouter()
    const {t} = useTranslation()
    const [tooltipIndex, setTooltipIndex] = useState<number>(-1)
    const { namePackage, isExpired } = useCurrentUserPackage()
    const user:any = session.user
    const checkRegexUrl = (router: any, regexes: RegExp[] | null | undefined) => {
        if (!regexes || !regexes.length) return false
        const url = router.asPath
        for (let i = 0; i < regexes.length; i++) {
            const found = url.match(regexes[i])
            if (found) return true
        }
        return false
    }

    const checkUrl = (path1: any, router: any) => {
        const query1: any = path1.query ?? {}
        const query2: any = router.query ?? {}
        if (path1.url !== router.pathname) return false
        if (!query1.hasOwnProperty('m') && router.query.hasOwnProperty('m'))
            return false

        if (query1.hasOwnProperty('m') && !router.query.hasOwnProperty('m'))
            return false
        if (
            query1.hasOwnProperty('m') &&
            router.query.hasOwnProperty('m') &&
            query1['m'] !== query2['m']
        )
            return false
        return true
    }

    const toggleSideBarItem = (data: any, url: string) => {
      if (!userInRight(data.hideRole, session)){
          const currentCollapseList = openingCollapseList.state
          const checkExist = checkCollapseExist(data)
          openingCollapseList.setState(
            checkExist || !data.list?.length
              ? [...currentCollapseList.filter((item: any) => item !== data.id)]
              : [...currentCollapseList, data.id],
          )

          if (url) {
            router.push(url);
          }
      } else {
        router.push(url)
      }
    }

    const checkIncludeActive = (data: any) => {
        const pathname = router.asPath
        let check = false
        if (data?.list) {
            data.list.forEach((item: any) => {
                if (
                    pathname === item.url ||
                    item.baseUrl?.some((m: any) => checkUrl(m, router)) ||
                    checkRegexUrl(router, item.regexUrls)
                ) {
                    check = true
                    return
                }
            })
        }
        if (data?.activePaths) {
            check = check || data?.activePaths?.some((item:any) => checkUrl(item, router));
        }
        return check
    }

    const { openingCollapseList } = useContext(AppContext)
    const checkCollapseExist = (data: any) => openingCollapseList.state.includes(data?.id)
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const checkMenuLocked = (lockRoles: number[]) => {
        if (isSystem) {
          return false
        }

        return !user?.user_role_id || lockRoles.includes(user?.user_role_id);
    }

    const getToolTip = (date:string) => {
        let message = ''
        if(date){
            const d = new Date(date)
            message = `(${t('left-menu')['time-line']} ${d.getMonth() + 1}\u00A0-\u00A0${d.getFullYear()})`
        }else{
            message = t('left-menu')['expired-plan-text']
        }
        return message
    }
    const renderSubItem = (list: any[]) => {
        return list
            .filter((subMenu: ListProps) => userInRight(subMenu.active_role, session))
            .map((subMenu: ListProps, index: number) => {
                const fullPath = router.asPath
                const isActiveSub = fullPath === subMenu.url ||
                    subMenu.baseUrl?.some((m: any) => checkUrl(m, router)) || checkRegexUrl(router, subMenu.regexUrls)
                    const isMenuLockRole = checkMenuLocked(subMenu.lockRole);
                    const isLockExpired = !isSystem && isExpired
                    const isMenuLocked = isMenuLockRole || isLockExpired;
                const Comp = isMenuLocked
                    ? ({children}: any) => <Fragment key={index}>{children}</Fragment>
                    : ({children}: any) => <Link href={subMenu.url} key={index}>{children}</Link>;
                const toolTipMessage = getToolTip(subMenu?.comingDate)
                return (
                  <Comp key={index}>
                    <Whisper
                      placement="right"
                      controlId="feature"
                      trigger={isMenuLocked ? ['hover'] : []}
                      speaker={
                        <Tooltip className={styles.developmentTooltip}>
                          {subMenu?.comingDate ? (
                            <>
                              <h5>{t('in-developing-feature')}</h5>
                                {!isStudent && <p
									dangerouslySetInnerHTML={{
                                        __html: toolTipMessage
                                    }}
								/>}
                            </>
                          ) : (
                            <h5>{toolTipMessage}</h5>
                          )}
                        </Tooltip>
                      }
                    >
                      <div
                        key={index}
                        className={classNames(styles.subMenuItem, {
                          [styles.active]: isActiveSub,
                          [styles.locked]: isMenuLocked,
                        })}
                      >
                        {t(`${t('left-menu')[subMenu.name]}`)}
                        {isMenuLocked && (
                          <div className={styles.lockedTag}>
                            <img src="/images-v2/lock-tag.png" alt="" />
                          </div>
                        )}
                      </div>
                    </Whisper>
                  </Comp>
                )
            })
    }

    return (
        <div className={styles.sideBarMenuWrapper}>
            <div className={classNames(styles.top, {
                [styles.compactMode]: !isExpand,
            })}>
                <Link href={'/'}>
                    <span>{isExpand ? <LogoIcon /> : <LogoCompact />}</span>
                </Link>
            </div>

            <div className={classNames(styles.center, {
                [styles.compactMode]: !isExpand,
            })}>
                {SIDENAV_ITEMS1
                    //filter sidebar item base on account role
                    ?.filter((item: any) => userInRight(item.access_role, session) && !item?.hideRole?.includes(user.user_role_id))
                    ?.map((item, sIndex:number) => {
                        const isActive = ((!isExpand || !checkCollapseExist(item)) && checkIncludeActive(item)) ||
                            (item?.url?.length > 0 && item.url === router.asPath)
                          const isMenuLockRole = checkMenuLocked(item.lockRole)
                          const isLockExpired = !isSystem && isExpired && !isStudent;
                          const isMenuLocked = isMenuLockRole || isLockExpired;
                        const tooltipMessage = getToolTip(item?.comingDate)
                        return (
                          <Fragment key={sIndex}>
                            <Whisper
                              controlId="feature"
                              enterable
                              trigger={isMenuLocked ? ['hover'] : []}
                              placement="right"
                              speaker={
                                <Tooltip className={styles.developmentTooltip}>
                                  {item?.comingDate || isStudent ? (
                                    <>
                                      <h5>{t('in-developing-feature')}</h5>
                                        {!isStudent &&
											<p
												dangerouslySetInnerHTML={{
                                                    __html: tooltipMessage
                                                }}
											/>}
                                    </>
                                  ) : (
                                    <h5>{tooltipMessage}</h5>
                                  )}
                                </Tooltip>
                              }
                            >
                              <div className={styles.sideItemBox}>
                                <div
                                  className={classNames(styles.sideItem, {
                                    [styles.hasItem]:
                                      !item?.list?.length ||
                                      userInRight(item.hideRole, session),
                                    [styles.active]: isActive,
                                    [styles.compactMode]: !isExpand,
                                    [styles.menuLocked]: isMenuLocked,
                                  })}
                                  onClick={() =>
                                    !isMenuLocked &&
                                    toggleSideBarItem(item, item?.url)
                                  }
                                  //event render tooltip hover when sidebar compact mode
                                  onMouseEnter={() =>
                                    !isMenuLocked &&
                                    !isExpand &&
                                    setTooltipIndex(item.id)
                                  }
                                  onMouseLeave={() =>
                                    !isMenuLocked &&
                                    !isExpand &&
                                    setTooltipIndex(-1)
                                  }
                                >
                                  <span
                                    className={classNames({
                                        [styles.leftIcon]: ((isExpand && checkCollapseExist(item)) || isActive)
                                        })
                                    }
                                  >
                                    {item.icon}
                                  </span>
                                  <a
                                    className={classNames({
                                      [styles.activeName]:
                                        checkCollapseExist(item) || isActive,
                                    })}
                                    dangerouslySetInnerHTML={{
                                        __html: t(`${t('left-menu')[item.name]}`)
                                    }}
                                  />
                                  {isMenuLocked ? (
                                    <div className={styles.lockedTag}>
                                      <img
                                        src="/images-v2/lock-tag.png"
                                        alt=""
                                      />
                                    </div>
                                  ) : (
                                    !userInRight(item.hideRole, session) &&
                                    !!item.list?.length && (
                                      <ChevronIcon
                                        className={classNames(
                                          styles.chevronIcon,
                                            {
                                                [styles.activeChevIcon]: checkCollapseExist(item)
                                            }
                                        )}
                                      />
                                    )
                                  )}
                                  {tooltipIndex === item.id && !isExpand && (
                                    //render tooltip when sidebar is compact mode
                                    // true &&
                                    <div
                                      className={classNames(
                                        styles.tooltipMenu,
                                        {
                                          [styles.withItem]:
                                            !userInRight(
                                              item.hideRole,
                                              session,
                                            ) && item.list?.length > 0,
                                        },
                                      )}
                                    >
                                      {!userInRight(item.hideRole, session) &&
                                      item.list?.length > 0 ? (
                                        //tooltip with list items
                                        <>
                                          <div className={styles.title}>
                                            <span dangerouslySetInnerHTML={{
                                                __html: t(`${t('left-menu')[item.name]}`)
                                            }}></span>
                                          </div>
                                          <div
                                            className={
                                              styles.tooltipMenuListWithItem
                                            }
                                          >
                                            {renderSubItem(item.list)}
                                          </div>
                                        </>
                                      ) : (
                                        //tooltip without list items
                                        <div
                                          className={
                                            styles.tooltipMenuListWithoutItem
                                          }
                                          onClick={() =>
                                            router.push(item.url)
                                          }
                                          dangerouslySetInnerHTML={{
                                            __html: t(`${t('left-menu')[item.name]}`)
                                          }}
                                        >
                                        </div>
                                      )}
                                    </div>
                                  )}
                                </div>
                              </div>
                            </Whisper>
                            {!userInRight(item.hideRole, session) &&
                              item.list?.length &&
                              isExpand && checkCollapseExist(item) && (
                                //render collapse sidebar item when sidebar is expand
                                <div
                                  className={classNames(styles.sideBarContent, {
                                    [styles.active]: checkCollapseExist(item),
                                  })}
                                >
                                  {renderSubItem(item.list)}
                                </div>
                              )}
                          </Fragment>
                        )
                    })}
            </div>
            <div className={classNames(styles.bottom, {
                [styles.compactMode]: !isExpand,
            })}>
                <span className={styles.btnExpand} onClick={onToggle}>
                    <ChevronIcon />
                </span>
            </div>
        </div>
    )
}

export default React.memo(SideBarDashboard)