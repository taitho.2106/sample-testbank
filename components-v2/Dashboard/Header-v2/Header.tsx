import React, { useState } from 'react'

import classNames from 'classnames'
import { useSession } from 'next-auth/client';

import NotificationIcon from '@/assets/icons/notification-icon.svg';
import SettingIcon from '@/assets/icons/setting-icon.svg'
import { USER_ROLES } from '@/interfaces/constants';
import { DefaultPropsType } from '@/interfaces/types'
import { userInRight } from '@/utils/index';

import ButtonHeader from './components/ButtonHeaderDropdown/ButtonHeader';
import LanguageSelector from './components/LanguageSelector/LanguageSelector';
import UserInfoDropdown from './components/UserInfo/UserInfoDropdown';
import UserManual from './components/UserManual/UserManual';
import styles from './Header.module.scss';
interface Props extends DefaultPropsType  {
  papeTitle?:string
  subTitle?:string
}
function HeaderV2({ className = '', style, papeTitle , subTitle}: Props) {
  const [session] = useSession()
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const [isShowBtn, setIsShowBtn] = useState<boolean>(false)
  const [isShowUser, setIsShowUser] = useState<boolean>(false)
  const [isShowUserManual, setIsShowUserManual] = useState(false)
  const [isShowLanguage, setIsShowLanguage] = useState(false)

  return (
      <div className={classNames(styles.headerWrapper, className)} style={style} id='header-page'>
          <div className={styles.boxTitle}>
            <div className={styles.titleHeader} title={papeTitle}><span>{papeTitle}</span></div>
            {subTitle && <div className={styles.subTitle}><span>{subTitle}</span></div>}
          </div>
          <div className={styles.actionHeader}>
            {!isSystem && <ButtonHeader 
              id='header-page'
              session={session} 
              isShow={isShowBtn} 
              setIsShow={setIsShowBtn} 
              setIsShowUser={setIsShowUser}
              setIsShowUserManual={setIsShowUserManual}
              setIsShowLanguage={setIsShowLanguage}
            />}
            <UserManual 
              id='header-page'
              isShowUserManual={isShowUserManual}
              setIsShowUserManual={setIsShowUserManual}
              onClick={() => {
                setIsShowBtn(false)
                setIsShowUser(false)
              }}
            />
            <LanguageSelector
              isShowLanguage={isShowLanguage}
              setIsShowLanguage={setIsShowLanguage}
              onClick={() => {
                setIsShowUser(false)
                setIsShowUserManual(false)
              }}
              shouldCLose={isShowBtn || isShowUserManual || isShowUser}
            />
            <UserInfoDropdown 
              session={session} 
              isShow={isShowUser} 
              setIsShow={setIsShowUser} 
              setIsShowUserManual={setIsShowUserManual}
            />
          </div>
      </div>
  )
}

export default HeaderV2