import React, { CSSProperties, SetStateAction, useContext, useEffect, useRef, useState } from 'react'

import classNames from 'classnames'
import { Session } from 'next-auth'
import { signOut } from 'next-auth/client'
import Link from 'next/link'
import { useRouter } from 'next/router'

import CallIcon from '@/assets/icons/call.svg'
import Camera from '@/assets/icons/camera.svg'
import Chevron from '@/assets/icons/chevron-down.svg'
import LogoutIcon from '@/assets/icons/logout.svg'
import MailIcon from '@/assets/icons/mail.svg'
import Overview from '@/assets/icons/logo-icon.svg'
import BaseDropdownMenu from '@/componentsV2/Common/Controls/BaseDropdownMenu'
import { USER_ROLES, paths } from '@/interfaces/constants'
import { WrapperContext } from '@/interfaces/contexts'
import { paths as p } from 'api/paths'
import { callApi } from 'api/utils'
import { useUserInfo } from 'lib/swr-hook'

import useTranslation from '../../../../../hooks/useTranslation'
import styles from './UserInfoDropdown.module.scss'
import { userInRight } from '@/utils/index'
import useInfo from "@/hooks/useInfo";

type PropsType = {
    session: any,
    className?: string,
    style?: CSSProperties,
    isShow: boolean,
    setIsShow: (value: SetStateAction<boolean>) => void
    setIsShowUserManual: (value: SetStateAction<boolean>) => void
}
const UserInfoDropdown = ({ session, className, style, isShow, setIsShow, setIsShowUserManual }: PropsType) => {
    const { t } = useTranslation()
    const { user, mutateUserInfo, userDisplayName } = useUserInfo()
    const handlerOnClick = (e: any) => {
        e.stopPropagation()
        setIsShow(!isShow)
        setIsShowUserManual(false)
    }
    const userAvatar = user?.avatar ?? session?.user?.avatar
    return (
        <div className={styles.boxUserInfo}>
            <div
                className={classNames(styles.userInfoWrapper, className)}
                style={style}
                onClick={handlerOnClick}
            >
                <div className={styles.imgWrap}>
                    {userAvatar ?<img src={`/upload/${userAvatar}`} alt=''/> :  <Overview height={40} width={40}/>}
                </div>
                <div className={styles.iconAction}><Chevron/></div>
            </div>
            {isShow && (
                <DropDown
                    t={t}
                    onClose={() => setIsShow(false)}
                    user={user}
                    mutateUserInfo={mutateUserInfo}
                    session={session}
                    userDisplayName={userDisplayName}
                />
            )}
        </div>
    )
}
type PropsDropDown = {
    user?: any,
    onClose?: (value: SetStateAction<boolean>) => void
    t?: any
    mutateUserInfo?:any
    session?:any
    userDisplayName?: string
}
const DropDown = ({user, onClose, t,mutateUserInfo,session, userDisplayName}: PropsDropDown) => {
    // const ref: any = useOutsideClick(onClose, true)
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const [error, setError] = useState(false)
    const {globalModal} = useContext(WrapperContext)
    const router = useRouter()
    const userInfo: any = session?.user
    const { getRoleUser } = useInfo(userInfo)
    const userAvatar = user?.avatar ?? (userInfo?.avatar ?? '')
    const handleFileChange = async (e:any) => {
        e.preventDefault()
        const file = e.target.files[0]
        if(!file) {
            setError(false)
            return
        }

        if (!file.type.startsWith("image/")) {
            setError(true)
            return;
        }
        const fileSizeLimit = 2 * 1024 * 1024; 
        const allowedTypes = ["image/jpeg", "image/jpg", "image/png"];
        if (file.size > fileSizeLimit) {
            setError(true)
            return
        }
        if (!allowedTypes.includes(file.type)) {
            setError(true)
            return
        }
        const formData = new FormData();
        formData.append("file_avatar", file);
        const res:any = await callApi(p.api_users_avatar, 'post', 'token', formData)
        if(res?.isSuccess){
            setError(false)
            mutateUserInfo({ ...user, avatar: res.avatar }, { revalidate: false })
        }

    }

    const role = getRoleUser()
    const handleSignOut = () => {
        if (
            router.asPath.includes('/unit-test/create-new-unittest') ||
            (router.pathname === '/unit-test/[templateSlug]' &&
                router?.query?.mode === 'edit')
        ) {
            globalModal.setState({
                id: 'confirm-modal',
                type:
                    router?.query?.mode === 'edit'
                        ? 'cancel-unit-test-update'
                        : 'cancel-unit-test',
                content: {
                    closeText: t("common")["back"],
                    submitText: t("modal-cancel-update")["submit-action"],
                    onSubmit: () => signOut({ callbackUrl: '/login', redirect: false }),
                },
            })
        } else {
            sessionStorage.removeItem('visitCount')
            signOut({callbackUrl: '/login', redirect: false})
        }
    }
    return (
        <div className={styles.dropdownUserInfoWrapper}>
            <BaseDropdownMenu className={styles.dropdownUser} textHeader={''} onClickOutside={onClose}>
            <div className={styles.userGroup}>
                        <div className={classNames(styles.avatar,{
                            [styles.error]:error
                        })}>
                            <div className={classNames(styles['wrapper-img'],'cursor-pointer')}>
                                <label htmlFor='file-input' className='cursor-pointer'>
                                    {userAvatar ? 
                                    <img 
                                        src={`/upload/${userAvatar}`}
                                        alt=''
                                    /> :
                                    <Overview height={80} width={80}/>
                                    }
                                    <Camera className={styles.camera}/>
                                </label>
                            <input type='file' id='file-input' accept='' 
                            onChange={handleFileChange}
                            />
                            </div>
                        {error && (
                            <div className={styles.tooltipError}>
                                <span dangerouslySetInnerHTML={{ __html: t("avatar-error") }} />
                            </div>
                        )}
                        </div>
                        <div className={styles.userInfo}>
                            <span className={styles.username}>{userDisplayName}</span>
                            <span className={styles.userRole}> {role}</span>
                            {user.email ? <span className={styles.email}><MailIcon /> {user.email}</span> : null}
                            {user.phone ? <span className={styles.numberphone}><CallIcon /> {user.phone}</span> : null}
                        </div>
                    </div>
                    <div className={styles.menuList}>
                        <Link href={paths.userInfo} passHref>
                            <div className={classNames(styles.userInfomation)}>
                                <span>
                                    {t('personal-info')}
                                </span>
                            </div>
                        </Link>
                        {!isSystem && <>
                            {
                                !isStudent ?
                                    <Link href={paths.payment}>
                                        <div className={classNames(styles.userInfomation)}>
                                    <span>
                                        {t('payment-service')}
                                    </span>
                                        </div>
                                    </Link>
                                    : null
                            }
                            <Link href={paths.transactionHistory}>
                                <div className={classNames(styles.userInfomation)}>
                                <span>
                                    {t('transaction-history')}
                                </span>
                                </div>
                            </Link>
                        </>}
                    </div>
                    <div className={styles.logout}>
                        <LogoutIcon onClick={handleSignOut}/>
                    </div>
            </BaseDropdownMenu>
        </div>
    )
}

export default UserInfoDropdown