import { useEffect } from 'react';

import classNames from 'classnames';

import Chevron from '@/assets/icons/chevron-down.svg';
import BaseDropdownMenu from '@/componentsV2/Common/Controls/BaseDropdownMenu';

import Cookies from 'js-cookie';
import useTranslation from '../../../../../hooks/useTranslation';
import styles from './LanguageSelector.module.scss';

const LanguageSelector = ({
    isShowLanguage,
    setIsShowLanguage,
    className = "",
    onClick,
    shouldCLose = false
}) => {
    const languageOptions = ['vn', 'en']
    const { switchLocale, locale } = useTranslation()
    const toggleDropdown = (e) => {
        e.stopPropagation()
        setIsShowLanguage(!isShowLanguage);
        onClick()
    };

    const handleLanguageSelect = (code) => {
        const locale = code === 'vn' ? 'vi' : 'en'
        switchLocale(locale)
        setIsShowLanguage(!isShowLanguage);
        Cookies.set('locale', locale)
    };

    useEffect(() => {
        if (shouldCLose) {
            setIsShowLanguage(false);
        }
    }, [shouldCLose])

    const filteredLanguageOptions = languageOptions.filter(option => {
        if (locale === 'vi'){
            return option !== 'vn'
        }
        return option !== locale
    });
    const renderLanguageOptions = () => {
        return filteredLanguageOptions.map((option, index) => (
            <div
                key={index}
                className={styles.languageOption}
                onClick={() => handleLanguageSelect(option)}
            >
                {option.toUpperCase()}
            </div>
        ));
    };

    return (
        <div className={classNames(styles.box, {
            [styles.scroll]: !!className.length
        })}>
            <div onClick={(e) => toggleDropdown(e)} className={styles.container}>
                {locale === 'vi' ? 'VN' : locale.toUpperCase()}
                <Chevron />
            </div>
            {isShowLanguage && (
                <BaseDropdownMenu
                    className={styles.languageWrapper}
                    textHeader=""
                    onClickOutside={() => {
                        setIsShowLanguage(false)
                    }}
                >
                    {renderLanguageOptions()}
                </BaseDropdownMenu>
            )}
        </div>
    );
};

export default LanguageSelector;