import React from 'react';

import classNames from 'classnames';
import { Tooltip, Whisper } from 'rsuite';

import HelpIcon from '@/assets/icons/ic-help.svg';
import BaseDropdownMenu from '@/componentsV2/Common/Controls/BaseDropdownMenu';
import useTranslation from '@/hooks/useTranslation';

import styles from './UserManual.module.scss';

const UserManual = ({
    id,
    isShowUserManual,
    setIsShowUserManual,
    onClick,
}) => {
    const { t } = useTranslation()
    const handlerOnClick = (e) => {
        e.stopPropagation()
        setIsShowUserManual(!isShowUserManual)
        onClick()
    }

    return (
        <>
            <Whisper
                trigger="hover"
                placement="bottom"
                delayOpen={0}
                container={() => document.getElementById(id)}
                speaker={
                    <Tooltip className={classNames(styles.tooltipText, {
                        [styles.disabled]: isShowUserManual
                    })}>
                        <span>{t("common")["user-manual"]}</span>
                    </Tooltip>
                }
            >
                <div style={{ position: 'relative' }}>
                    <div
                        className={styles.helpIcon}
                        onClick={handlerOnClick}
                    >
                        <HelpIcon />
                    </div>
                    {isShowUserManual && (
                        <DropDownInstructions
                            onClose={() => setIsShowUserManual(false)}
                        />
                    )}
                </div>
            </Whisper>
        </>
    );
};

export default UserManual;

const DropDownInstructions = ({ onClose }) => {
    const { t, locale } = useTranslation()
    return (
        <BaseDropdownMenu
            className={styles.cardWrapper}
            textHeader={''}
            onClickOutside={onClose}
        >
            <div className={styles.title}>
                <HelpIcon />
                <span>{t("common")["user-manual"]}</span>
            </div>
            <a
                className={styles.instructionLink}
                target="_blank"
                href={locale === "en" ? "https://help.i-test.vn/i-test-teachers-user-manual/general-features/register" : "https://help.i-test.vn/huong-dan-su-dung-danh-cho-giao-vien/chuc-nang-chung/dang-ky"}
            >
                <span>{t("common")["teacher-user-manual"]}</span>
            </a>
            <a
                className={styles.instructionLink}
                target="_blank"
                href={locale === "en" ? "https://help.i-test.vn/i-test-learners-user-manual/general-features/register" : "https://help.i-test.vn/huong-dan-su-dung-danh-cho-hoc-vien/chuc-nang-chung/dang-ky"}
            >
                <span>{t("common")["student-user-manual"]}</span>
            </a>
        </BaseDropdownMenu>
    );
};
