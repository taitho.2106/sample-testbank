import React, { SetStateAction } from 'react'

import classNames from 'classnames';
import { Session } from 'next-auth'
import {useSession} from "next-auth/client";
import Link from 'next/dist/client/link';
import {useRouter} from "next/dist/client/router";
import { Popover, Whisper } from 'rsuite';

import IconPackageAdvance from '@/assets/icons/ic-package-advance.svg'
import IconPackageBasic from '@/assets/icons/ic-package-basic.svg'
import IconPackageInternational from '@/assets/icons/ic-package-international.svg'
import IconCountBuyUnitTestWhite from '@/assets/icons/icon-count-buy-white.svg';
import IconCountBuyUnitTest from '@/assets/icons/icon-count-buy.svg';
import StudentPlan199Icon from '@/assets/icons/student-plan-199.svg';
import StudentPlan299Icon from '@/assets/icons/student-plan-299.svg';
import StudentPlan99Icon from '@/assets/icons/student-plan-99.svg';
import StudentPlanFreeIcon from '@/assets/icons/student-plan-free.svg';
import IconTimeLeft from "@/assets/icons/time-icon.svg"
import SkeletonLoading from '@/componentsV2/Common/SkeletonLoading';
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button';
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage';
import useTranslation from '@/hooks/useTranslation';
import { PACKAGES, PACKAGES_FOR_STUDENT, paths, PAYMENT_TYPE, STEPS, USER_ROLES } from "@/interfaces/constants";
import {cleanObject, userInRight} from '@/utils/index';
import { usePackageUser, useUserInfo } from "lib/swr-hook";

import styles from './ButtonHeader.module.scss';

type PropsButton = {
    session: Session
    isShow:boolean
    setIsShow: (value: SetStateAction<boolean>)=> void
    setIsShowUser: (value: SetStateAction<boolean>)=> void
    setIsShowUserManual: (value: SetStateAction<boolean>) => void
    setIsShowLanguage: (value: SetStateAction<boolean>) => void
    style?: any
    id?:string
}
function ButtonHeader({ 
    session, 
    isShow, 
    setIsShow, 
    setIsShowUser, 
    setIsShowUserManual, 
    setIsShowLanguage,
    style, 
    id 
}: PropsButton) {
    const {t} = useTranslation()
    const isStudent = userInRight([-1, USER_ROLES.Student], session);
    const { dataPackage, isLoading } = usePackageUser()
    const { textTimeLeft } = useCurrentUserPackage()
    const { user } = useUserInfo()
    const renderIconWithPackage = (plan:any) => {
        switch (plan?.code) {
            case PACKAGES.TRIAL.CODE:
            case PACKAGES.BASIC.CODE:
                return <><IconPackageBasic /></>
            case PACKAGES.ADVANCE.CODE:
                return <><IconPackageAdvance /></>
            case PACKAGES.INTERNATIONAL.CODE:
                return <><IconPackageInternational /></>
            case PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE:
                return <StudentPlanFreeIcon/>
            case PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE:
                return <StudentPlan99Icon/>
            case PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE:
                return <StudentPlan299Icon/>
            case PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE:
                return <StudentPlan199Icon/>
            default:
                return <></>
        }
    }
    const renderColorBackgroundWithPackage = (plan:any) => {
        switch (plan?.code) {
            case PACKAGES.TRIAL.CODE:
            case PACKAGES.BASIC.CODE:
            case PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE:
                return '--basic'
            case PACKAGES.ADVANCE.CODE:
            case PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE:
                return '--advance'
            case PACKAGES.INTERNATIONAL.CODE:
            case PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE:
                return '--international'
            case PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE:
                return '--master'
            default:
                return ''
        }
    }
    const renderNameByCode = (item:any) => {
        switch(item.code){
            case PACKAGES.BASIC.CODE:
                return t('basic-plan');
            case PACKAGES.ADVANCE.CODE:
                return t('medium-plan');
            case PACKAGES.INTERNATIONAL.CODE:
                return t('advanced-plan');
            case PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE:
                return t('student-basic-plan');
            case PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE:
                return t('student-medium-plan');
            case PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE:
                return t('student-advanced-plan');
            case PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE:
                return t('student-international-plan');
            default:
                return '';
        }
    }
    return (
      <>
        {!isLoading ?
        <Link href={isStudent ? paths.assignedUnitTest :paths.unitTest}>
            <Whisper
                placement="bottomEnd"
                trigger="hover"
                enterable
                delayOpen={0}
                container={() => document.getElementById(id)}
                speaker={
                    <Popover className={styles.poperverModal} arrow={false}>
                          <DropDownBtn user={user} renderDatePlan={textTimeLeft} namePackage={renderNameByCode(dataPackage)}/>
                    </Popover>
                }
            >
                <button
                    className={classNames(styles.btnActionHeader, styles[renderColorBackgroundWithPackage(dataPackage)])}
                    onMouseEnter={() => {
                        setIsShowUser(false)
                        setIsShowUserManual(false)
                        setIsShowLanguage(false)
                    }}
                    style={style}
                >
                    {renderIconWithPackage(dataPackage)}
                    <div className={styles.btnContent} id='container-header'>
                    <>
                        <span className={styles.content}>{renderNameByCode(dataPackage)}</span>
                        <span className={styles.datetime}><IconTimeLeft /> {textTimeLeft} {isStudent && !dataPackage.isExpired && dataPackage.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE ? <span>{"| "}<IconCountBuyUnitTestWhite /> {dataPackage?.quantity || "0"}  {t("turn-of-use")}</span> : ''}</span>
                    </>
                    </div>
                </button>
            </Whisper>
        </Link>
        : 
        <><SkeletonLoading width='190px' height='50px' borderRadius='12px'/></>
        }
      </>
    )
}
type PropsDropDownBtn = {
    user?: any,
    namePackage?: string
    textTimeLeft?:string,
    renderDatePlan: string,
}
const DropDownBtn = ({renderDatePlan, namePackage}: PropsDropDownBtn) => {
    const {t} = useTranslation()
    const { push } = useRouter()
    const [session] = useSession();
    const {isExpired, isHighestPackage, dataPlan} = useCurrentUserPackage()
    const { userDisplayName } = useUserInfo();
    const isStudent = userInRight([-1, USER_ROLES.Student], session);
    const handleClick = (e: React.MouseEvent<HTMLButtonElement>, forceType: string) => {
        e.stopPropagation();
        const type = forceType ?? (isExpired ? PAYMENT_TYPE.UPGRADE : PAYMENT_TYPE.RENEW);
        push({
            pathname: paths.payment,
            query: cleanObject({ type }),
        })
    }

    return (
        <div className={styles.cardInfoWrapper} onClick={(e:any)=>e.preventDefault()}>
            <div className={styles.wrapperContent}>
                <span className={styles.title}>{t("hello")} <br/>{userDisplayName}</span>
                <div className={styles.boxTimer}>
                    {isExpired || dataPlan.code === PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE ? (
                        <>
                            <span className={styles.textSpan}>{t("using")}
                                <span className={styles.textUpgrade}> {namePackage}</span>
                                <br/><span className={styles.italic}>{`(${renderDatePlan})`}</span>
                            </span>
                        </>
                    ) : (
                        <>
                            <span className={styles.textSpan}
                                dangerouslySetInnerHTML={{
                                    __html: t(`remain`,[namePackage])
                                }}
                            />
                            <span className={styles.textInfoUpgrade}>{renderDatePlan}</span>
                        </>

                    )}
                </div>
                {isStudent && !isExpired && dataPlan.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE && (
                    <div className={styles.boxUnitTestInfo}>
                        <div className={styles.icon}>
                            <IconCountBuyUnitTest />
                        </div>
                        <div className={styles.content}>
                            <span>{t("remaining-used")}: </span>
                            <span className={styles.countUsing}>{dataPlan?.quantity} {t("turn-of-use")}</span>
                        </div>
                    </div>
                )}
                {!isStudent ?
                    <>
                        <div className={styles.dirivered}></div>
                        <div className={styles.btnAction}>
                            {!isExpired && !isHighestPackage &&
                                <ButtonCustom className={classNames(styles.btn, styles.upgrade)} type="outline"
                                              onClick={e => handleClick(e, PAYMENT_TYPE.UPGRADE)}>
                                    {t("upgrade")}
                                </ButtonCustom>
                            }
                            {(dataPlan?.code !== PACKAGES.TRIAL.CODE) &&
                                <ButtonCustom className={classNames(styles.btn, styles.upgrade)}
                                              onClick={handleClick}>
                                    {isExpired ? t("buy-now") : t("extend")}
                                </ButtonCustom>
                            }
                        </div>
                    </>
                    :
                    <>
                        <div className={styles.dirivered}></div>
                        <div className={styles.btnAction}>
                            <ButtonCustom className={classNames(styles.btn, styles.upgrade)}
                                          onClick={e => handleClick(e, PAYMENT_TYPE.RENEW)}>
                                {t(`${t("package")["title-combo-unit-test"]}`)}
                            </ButtonCustom>
                        </div>
                    </>
                }
            </div>
        </div>
    );
}
export default ButtonHeader