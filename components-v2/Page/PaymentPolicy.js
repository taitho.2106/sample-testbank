import React from "react";

import useTranslation from '@/hooks/useTranslation'

import { EMAIL_COMPANY } from "../../constants";
import Container from "../Common/Container";
import styles from "./Page.module.scss";

const PaymentPolicy = () => {
    const { t } = useTranslation()
    const website = `<a target="_blank" href="https://i-Test.vn" style="color: '#0088ff';"> https://i-Test.vn</a>`
    const email = `<a href=mailto:${EMAIL_COMPANY} style="color: '#0088ff';">  ${EMAIL_COMPANY}</a>`

    return (
        <Container>
            <div className={styles.pageWrapper}>
                <h2>{t('payment-policy')['title']}</h2>
                <div className={styles.content}>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('payment-policy')['intro']}`, ['<span>i-Test.vn</span>', website])
                    }} />
                    <ul>
                        <li>{t('payment-policy')['method-1']}</li>
                        <li> {t('payment-policy')['method-2']}</li>
                    </ul>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('payment-policy')['support']}`, ['<span>i-Test.vn</span>', email])
                    }} />
                </div>
            </div>
        </Container>
    )
}

export default PaymentPolicy