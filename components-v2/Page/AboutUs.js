import React from 'react'

import useTranslation from '@/hooks/useTranslation'

import { EMAIL_COMPANY } from "../../constants";
import Container from '../Common/Container'
import styles from "./Page.module.scss";

const AboutUs = () => {
    const { t } = useTranslation()
    return (
        <Container>
            <div className={styles.pageWrapper}>
                <h2>{t('about-us')['title']}</h2>
                <div className={styles.content}>
                    <h5>{t('about-us')['heading-1']}</h5>
                    <p>{t('about-us')['company-desc-1']}</p>
                    <p>{t('about-us')['company-desc-2']}</p>
                    <ul>
                        <li>{t('about-us')['li-1']}</li>
                        <li>{t('about-us')['li-2']}</li>
                        <li>{t('about-us')['li-3']}<a href={`mailto:${EMAIL_COMPANY}`} style={{ color: '#0088ff' }}>  {EMAIL_COMPANY}</a></li>
                    </ul>
                    <h5>{t('about-us')['heading-2']}</h5>
                    <p>{t('about-us')['product-intro']}</p>
                    <h6>{t('about-us')['title-1']}</h6>
                    <p>{t('about-us')['product1-desc']}</p>
                    <h6>{t('about-us')['title-2']}</h6>
                    <p>{t('about-us')['product2-desc-1']}</p>
                    <p>{t('about-us')['product2-desc-2']}</p>
                    <h6>{t('about-us')['title-3']}</h6>
                    <p>{t('about-us')['product3-desc']}</p>
                    <h6>{t('about-us')['title-4']}</h6>
                    <p>{t('about-us')['product4-desc']}</p>
                    <h6>{t('about-us')['title-5']}</h6>
                    <p>{t('about-us')['product5-desc-1']}</p>
                    <p>{t('about-us')['product5-desc-2']}</p>
                    <p>{t('about-us')['product5-desc-3']}</p>
                    <p>{t('about-us')['product5-desc-4']}</p>
                </div>
            </div>
        </Container>
    )
}

export default AboutUs