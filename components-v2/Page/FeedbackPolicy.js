import React from 'react'

import useTranslation from '@/hooks/useTranslation'

import Container from '../Common/Container'
import styles from "./Page.module.scss";

const FeedbackPolicy = () => {
    const { t } = useTranslation()
    return (
        <Container>
            <div className={styles.pageWrapper}>
                <h2>{t('feedback-policy')['title']}</h2>
                <div className={styles.content}>
                    <ul>
                        <li>
                            {t('feedback-policy')['content-1']}
                        </li>
                        <li>
                            {t('feedback-policy')['content-2']}
                        </li>
                        <li>
                            {t('feedback-policy')['content-3']}
                        </li>
                    </ul>
                </div>
            </div>
        </Container>
    )
}

export default FeedbackPolicy
