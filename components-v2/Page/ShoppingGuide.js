import React from 'react'

import useTranslation from '@/hooks/useTranslation'

import Container from '../Common/Container'
import styles from "./Page.module.scss";

const ShoppingGuide = () => {
    const { t } = useTranslation()
    const iTestLink = `<a href="https://i-test.vn">i-Test</a>`

    return (
        <Container>
            <div className={styles.pageWrapper}>
                <h2>{t('shopping-guide')['title']}</h2>
                <div className={styles.content}>
                    <h5>
                        {t('shopping-guide')['highlight-1']}
                    </h5>
                    <h6>1.1 {t('shopping-guide')['subject-1']}</h6>
                    <i>a. {t('shopping-guide')['desc-1']}</i>
                    <img src="/images/layout/image17.png" alt="" />

                    <i>b. {t('shopping-guide')['desc-2']}</i>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('shopping-guide')['h1-step1']}`, [iTestLink])
                    }} />
                    <p>{t('shopping-guide')['h1-step2']}</p>
                    <p>{t('shopping-guide')['h1-step3']}</p>
                    <p>{t('shopping-guide')['h1-step4']}</p>
                    <ul>
                        <li>{t('shopping-guide')['feature-1']}</li>
                        <li>{t('shopping-guide')['feature-2']}</li>
                        <li>{t('shopping-guide')['feature-3']}</li>
                        <li>{t('shopping-guide')['feature-4']}</li>
                        <li>{t('shopping-guide')['feature-5']}</li>
                    </ul>

                    <h6>1.2. {t('shopping-guide')['subject-2']}</h6>
                    <h6>1.3. {t('shopping-guide')['subject-3']}</h6>
                    <h6>1.4. {t('shopping-guide')['subject-4']}</h6>

                    <h5>{t('shopping-guide')['highlight-2']}</h5>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('shopping-guide')['h2-step1']}`, [iTestLink])
                    }} />
                    <p>
                        {t('shopping-guide')['h2-step2']}
                    </p>
                    <p>{t('shopping-guide')['step2-desc-1']}</p>
                    <img src="/images/layout/image2.png" alt="" />

                    <p>{t('shopping-guide')['step2-desc-2']}</p>
                    <img src="/images/layout/image11.png" alt="" />

                    <p>{t('shopping-guide')['step2-desc-3']}</p>
                    <img src="/images/layout/image10.png" alt="" />

                    <p>{t('shopping-guide')['h2-step3']}</p>
                    <p>{t('shopping-guide')['step3-desc-1']}</p>
                    <img src="/images/layout/image4.jpg" alt="" />

                    <p>{t('shopping-guide')['step3-desc-2']}</p>
                    <img src="/images/layout/image6.jpg" alt="" />

                    <p>{t('shopping-guide')['step3-desc-3']}</p>
                    <img src="/images/layout/image7.jpg" alt="" />

                    <p>{t('shopping-guide')['h2-step4']}</p>
                    <img src="/images/layout/image19.jpg" alt="" />

                    <p>{t('shopping-guide')['h2-step5']}</p>
                    <img src="/images/layout/image18.png" alt="" />

                    <p>{t('shopping-guide')['h2-step6']}</p>
                    <img src="/images/layout/image13.png" alt="" />
                    <p>{t('shopping-guide')['step6-desc']}</p>
                    <img src="/images/layout/image1.png" alt="" />

                    <p>{t('shopping-guide')['h2-step7']}</p>
                    <img src="/images/layout/image9.png" alt="" />

                    <p>{t('shopping-guide')['h2-step8']}</p>
                    <p>{t('shopping-guide')['step8-desc-1']}</p>
                    <img src="/images/layout/image3.jpg" alt="" />

                    <p>{t('shopping-guide')['step8-desc-2']}</p>
                    <img src="/images/layout/image14.jpg" alt="" />

                    <p>{t('shopping-guide')['step8-desc-3']}</p>
                    <img src="/images/layout/image15.jpg" alt="" />
                </div>
            </div>
        </Container>
    )
}

export default ShoppingGuide
