import React from "react";

import useTranslation from '@/hooks/useTranslation';

import { EMAIL_COMPANY } from "../../constants";
import Container from "../Common/Container";
import styles from "./Page.module.scss";

const SecurityPolicy = () => {
    const { t } = useTranslation()
    const email = `<a href=mailto:${EMAIL_COMPANY} style="color: '#0088ff';">  ${EMAIL_COMPANY}</a>`

    return (
        <Container>
            <div className={styles.pageWrapper}>
                <h2>{t('security-policy')['title']}</h2>
                <div className={styles.content}>
                    <p className={styles.highlight}>{t('security-policy')['highlight-1']}</p>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-1-1']}`, ['<span>i-Test.vn </span>'])
                    }} />
                    <p>{t('security-policy')['desc-1-2']}</p>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-1-3']}`, ['<span>i-Test.vn </span>'])
                    }} />
                    <ul>
                        <li>{t('security-policy')['info-1']}</li>
                        <li>{t('security-policy')['info-2']}</li>
                        <li>{t('security-policy')['info-3']}</li>
                        <li>{t('security-policy')['info-4']}</li>
                        <li>{t('security-policy')['info-5']}</li>
                        <li>{t('security-policy')['info-6']}</li>
                        <li>{t('security-policy')['info-7']}</li>
                        <li>{t('security-policy')['info-8']}</li>
                    </ul>

                    <p className={styles.highlight}>{t('security-policy')['highlight-2']}</p>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-2']}`, ['<span>i-Test.vn </span>'])
                    }} />
                    <ul>
                        <li>{t('security-policy')['purpose-1']}</li>
                        <li>{t('security-policy')['purpose-2']}</li>
                        <li>{t('security-policy')['purpose-3']}</li>
                        <li>{t('security-policy')['purpose-4']}</li>
                        <li>{t('security-policy')['purpose-5']}</li>
                    </ul>

                    <p className={styles.highlight}>{t('security-policy')['highlight-3']}</p>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-3']}`, ['<span>i-Test.vn </span>', email])
                    }} />

                    <p className={styles.highlight}>{t('security-policy')['highlight-4']}</p>
                    <p>{t('security-policy')['desc-4']}</p>
                    <ul>
                        <li>{t('security-policy')['subject-1']}</li>
                        <li>{t('security-policy')['subject-2']}</li>
                    </ul>

                    <p className={styles.highlight}>{t('security-policy')['highlight-5']}</p>
                    <ul>
                        <li>{t('security-policy')['address']}</li>
                        <li>{t('security-policy')['phone']}: 1800 6242</li>
                        <li>Website: <a target="_blank" href="https://i-Test.vn" style={{ color: '#0088ff' }}> i-Test.vn</a></li>
                        <li>Email: <a href={`mailto:${EMAIL_COMPANY}`} style={{ color: '#0088ff' }}>  {EMAIL_COMPANY}</a></li>
                    </ul>
                    <p className={styles.highlight}>{t('security-policy')['highlight-6']}</p>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-6-1']}`, ['<span>i-Test.vn </span>', email])
                    }} />
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-6-2']}`, ['<span>i-Test.vn </span>'])
                    }} />

                    <p className={styles.highlight}>{t('security-policy')['highlight-7']}</p>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-7-1']}`, ['<span>i-Test.vn </span>', '<span>i-Test.vn </span>'])
                    }} />
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-7-2']}`, ['<span>i-Test.vn </span>'])
                    }} />
                    <ul>
                        <li>{t('security-policy')['case-1']}</li>
                        <li>{t('security-policy')['case-2']}</li>
                        <li>{t('security-policy')['case-3']}</li>
                    </ul>
                    <p dangerouslySetInnerHTML={{
                        __html: t(`${t('security-policy')['desc-7-3']}`, ['<span>i-Test.vn </span>', '<span>i-Test.vn </span>', email, '<b> 1800 6242</b>'])
                    }} />
                </div>
            </div>
        </Container>
    )
}

export default SecurityPolicy