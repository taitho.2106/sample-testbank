import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';
import { Modal } from 'rsuite';

import apiConfig from '@/constants/apiConfig';
import { unitTestGiftByGrade } from "@/constants/index";
import useFetch from '@/hooks/useFetch';
import useNoti from '@/hooks/useNoti';
import { paths } from '@/interfaces/constants';
import useGrade from 'hooks/useGrade';
import { USER_ROLES } from 'interfaces/constants';
import { useUserInfo } from 'lib/swr-hook';

import BasicForm from "@/componentsV2/Common/Controls/BasicForm";
import Loading from '@/componentsV2/Common/Controls/Loading';
import SelectField from '@/componentsV2/Common/Form/SelectField';
import Button from '../Common/Controls/Button';

import IconCongratulations from '@/assets/icons/congratulations.svg';

import { useMemo } from 'react';
import useTranslation from "../../hooks/useTranslation";
import styles from './ModalStudentGift.module.scss';

let receivedLocalEachUser = {};

const ModalStudentGift = () => {
    const { t } = useTranslation()
    const { userDisplayName, user } = useUserInfo();
    const { execute, loading } = useFetch(apiConfig.student.claimGift);
    const { push, pathname } = useRouter();
    const { getNoti } = useNoti();
    const { gradeList } = useGrade();
    const [ gradeSelected, setGradeSelected ] = useState();

    const gradeOptions = useMemo(() => {
        return gradeList.map(grade => {
            const disabled = !unitTestGiftByGrade[grade.code]?.length;
            return {
                value: grade.code,
                label: [grade.display, disabled && t('modal-claim-gift')['not-available']].filter(Boolean).join(' '),
                disabled,
            }
        })
    }, [ gradeList ]);

    const [showClaimGift, setShowClaimGift] = useState(false);

    const handleClaimGift = () => {
        execute({
            params: {
                grade: gradeSelected,
            },
            onCompleted: () => {
                push({
                    pathname: paths.unitTest,
                    query: {
                        m: 'mine',
                        t: new Date().getTime(), // trick trigger refresh unit-test
                    }
                });
                getNoti('success', 'topCenter', t('modal-claim-gift')['claim-success']);
                receivedLocalEachUser[user.id] = true;
                setShowClaimGift(false);
            },
            onError: () => {
                getNoti('error', 'topCenter', t('modal-claim-gift')['claim-failed']);
            }
        });
    }

    useEffect(() => {
        const excludePaths = [
            paths.signIn, `${paths.practiceTest}/[practiceTestSlug]`,
        ]
        if (
            user &&
            user.user_role_id === USER_ROLES.Student &&
            user.is_received_gift !== 1 &&
            !excludePaths.includes(pathname) &&
            !receivedLocalEachUser[user.id]
        ) {
            setShowClaimGift(true);
        }
    }, [ user, pathname ]);

    return (
        <Modal
            className={styles.wrapperModal}
            open={showClaimGift}
            backdrop={false}
        >
            <div className={styles.head}>
                <div className={styles.icon}>
                    <IconCongratulations />
                </div>
                <div className={styles.title}>
                    {t("modal-claim-gift")['title']}
                    <div className={styles.user}>{userDisplayName}</div>
                </div>
            </div>
            <div className={styles.content}>
                <p dangerouslySetInnerHTML={{__html: t("modal-claim-gift")['description']}}></p>
                <ul>
                    <li>{t("modal-claim-gift")['description-1']}</li>
                    <li>{t("modal-claim-gift")['description-2']}</li>
                    <li>{t("modal-claim-gift")['description-3']}</li>
                </ul>

                <div className={styles.grade}>
                    <h3 className={styles.titleGrade}>{t("modal-claim-gift")['selected-grade']}</h3>

                    <BasicForm
                        className={styles.actions}
                        initialValues={{}}
                        onSubmit={() => {}}
                    >
                        <SelectField
                            name="grade"
                            placeHolder={t('common')['grade']}
                            options={gradeOptions}
                            menuClassName={styles.menuGrade}
                            onChange={({ value }) => setGradeSelected(value)}
                            scrollIntoViewOnOpen={true}
                        />
                        <Button
                            onClick={handleClaimGift}
                            className={styles.btnClaim}
                            disabled={!gradeSelected || loading}
                        >
                            {t("modal-claim-gift")['action-claim']}
                            {loading && <Loading width={18} height={18} />}
                        </Button>
                    </BasicForm>
                </div>
            </div>
        </Modal>
    );
};

export default ModalStudentGift;