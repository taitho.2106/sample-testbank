import { useContext, useEffect, useState } from 'react';

import { useSession } from 'next-auth/client';
import { useRouter } from 'next/router.js';
import useSWR from 'swr';

import Cookies from 'js-cookie';
import { paths } from '../../api/paths.ts';
import { callApi } from '../../api/utils';
import { SALE_ITEM_TYPE } from "../../constants";
import useTranslation from '../../hooks/useTranslation';
import { PACKAGES_FOR_STUDENT } from "../../interfaces/constants";
import { AppContext } from '../../interfaces/contexts';
import { usePackageUser } from "../../lib/swr-hook";

const UserPackage = () => {
    const { setListPlans, orderData, setOrderData, setListPlansStudent, setListComboServicePackage } = useContext(AppContext)
    const [session] = useSession();
    const { query } = useRouter();
    const [orderId, setOrderId] = useState('');
    const [isTimeoutOrder, setIsTimeoutOrder] = useState(false);

    const fetcher = url => fetch(url).then(r => r.json())
    const url = !orderData?.vnp_response_code && !orderData?.vnp_transaction_status && !isTimeoutOrder ? `${paths.api_order_detail}?order_id=${orderId}` : null;
    const { data } = useSWR(orderId ? url : null, fetcher, { refreshInterval: 3000, refreshWhenHidden: true })
    const { dataPackage, mutateUserPackage } = usePackageUser()
    const { locale } = useTranslation()
    const studentDefaultPlan = {
        id: 1,
        code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE,
        description: "Phá đảo đề thi miễn phí",
        name: "Phá đảo đề thi miễn phí",
        price: 0,
        status: 1,
        asset_type: SALE_ITEM_TYPE.COMBO_UNIT_TEST,
    }
    const getUserPackage = () => {
        mutateUserPackage()
    }

    useEffect(() => {
        if (query.vnp_TxnRef) {
            setOrderId(query.vnp_TxnRef);

            setTimeout(() => {
                setIsTimeoutOrder(true);
            }, 60000)
        }
    }, [query.vnp_TxnRef]);

    useEffect(() => {
        if (data) {
            setOrderData(data.data);

            if (data.data?.status === 1 && session?.user?.id) {
                getUserPackage();
            }
        }
    }, [data, session?.user?.id]);

    useEffect(() => {
        Cookies.set('locale', locale, {
            secure: true
        })
        callApi(`${paths.api_sale_item}?type=${SALE_ITEM_TYPE.SERVICE_PACKAGE}`).then(resData => {
            const dataPlan = []
            if (resData?.code === 200) {
                resData?.data?.forEach(res => {
                    delete res?.item.id
                    const data = res?.item
                    delete  res?.item
                    const dataPackages = { ...res, ...data, price: res.exchange_price };
                    dataPlan.push(dataPackages)
                })
            }
            setListPlans(dataPlan || []);
        })
        callApi(`${paths.api_sale_item}?type=${SALE_ITEM_TYPE.COMBO_UNIT_TEST}`).then(resData => {
            const dataPlanStudent = [studentDefaultPlan]
            if (resData?.code === 200) {
                resData?.data?.forEach(res => {
                    delete res?.item.id
                    const data = res?.item
                    delete  res?.item
                    const dataPackages = { ...res, ...data, price: res.exchange_price };
                    dataPlanStudent.push(dataPackages)
                })
            }
            setListPlansStudent(dataPlanStudent || []);
        })
        callApi(`${paths.api_sale_item}?type=${SALE_ITEM_TYPE.COMBO_SERVICE_PACKAGE}`).then(resData => {
            const dataComboServicePackage = []
            if (resData?.code === 200) {
                resData?.data?.forEach(res => {
                    delete res?.item.id
                    const data = res?.item
                    delete  res?.item
                    const dataPackages = { ...res, ...data, price: res.exchange_price };
                    dataComboServicePackage.push(dataPackages)
                })
            }
            setListComboServicePackage(dataComboServicePackage || [])
        })
    }, [])

    useEffect(() => {
        if (session) {
            getUserPackage();
        }
    }, [session]);

    return null;
}

export default UserPackage;