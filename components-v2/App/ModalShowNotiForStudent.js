import React, { useCallback, useEffect, useMemo, useRef, useState } from "react";

import dayjs from "dayjs";
import isBetween from 'dayjs/plugin/isBetween';
import { useSession } from "next-auth/client";

import IconClose from "assets/icons/icon-close.svg"

import useCurrentUserPackage from "../../hooks/useCurrentUserPackage";
import { PACKAGES_FOR_STUDENT, paths, USER_ROLES } from "../../interfaces/constants";
import { eventConfig, userInRight } from "../../utils";
import ModalWrapper from "../Common/ModalWrapper";
import styles from "./ModalShowNotiForStudent.module.scss"
import { useRouter } from "next/router";
import classNames from "classnames";
dayjs.extend(isBetween)
import LogiItest from "assets/icons/logo-itest.svg"
import useTranslation from "../../hooks/useTranslation";
const ModalShowNotiForStudent = () => {
	const { t } = useTranslation();
	const { startDate, endDate } = eventConfig;
	const startTimeActiveModal = startDate;
	const endTimeActiveModal = endDate;
	const timeLoopShowModal = 3
	const isRunning = dayjs().isBetween(startTimeActiveModal, endTimeActiveModal, 'minute', '[]')
	const [session, loading] = useSession()
	const { pathname } = useRouter();
	const [isShow, setIsShow] = useState(false)
	const isStudent = userInRight([-1, USER_ROLES.Student], session);
	const [isTimeRun, setIsTimeRun] = useState(false)
	const { dataPlan, isLoading } = useCurrentUserPackage();
	
	const isLogged = useMemo(() => {
		return !!(!loading && session?.user);
	}, [loading])
	
	const isMasterCerf = useMemo(() => {
		if (dataPlan?.code === PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE && dataPlan?.quantity > 0) {
			return true;
		}
		return false;
	}, [isLoading, dataPlan?.code]);
	
	const onCloseModal = () => {
		setIsShow(false)
		if (isMasterCerf) {
			window.localStorage.removeItem('nextTimeRun')
		} else {
			setIsTimeRun(prevState => !prevState)
			const timeLine = dayjs().add(timeLoopShowModal, 'h').valueOf()
			window.localStorage.setItem('nextTimeRun', `${timeLine}`)
		}
	}
	useEffect(() => {
		const pathNameNotShow = [paths.signIn, paths.signUp, paths.forgotPassword, paths.payment, paths.paymentResults, paths.practiceTestSlug, paths.practiceTest, paths.createTemplate, paths.createUnitTest]
		if (isLogged && isStudent && isRunning && dataPlan?.code && !pathNameNotShow.includes(pathname)) {
			let timeout;
			let delay = 0;
			if (!isMasterCerf) {
				const timeShowModal = window.localStorage.getItem("nextTimeRun") || dayjs().valueOf();
				if (dayjs(Number(timeShowModal)).diff(dayjs()) > 0) {
					delay = dayjs(Number(timeShowModal)).diff(dayjs());
				}
			}
			timeout = setTimeout(() => {
				setIsShow(true)
			}, delay)
			return () => {
				if (timeout) {
					clearTimeout(timeout)
				}
			}
		}
	}, [isLogged, dataPlan?.code, isMasterCerf, isTimeRun]);
	console.log({isShow});
	if (isLoading) return null
	
	const style = {
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center',
	}
	
	return (
		<>
			{
				isMasterCerf ? (
					<ModalWrapper
						className={classNames(styles.modalContainer, styles.modalText)}
						isOpen={isShow}
						backdrop={true}
						onCloseModal={onCloseModal}
						style={{ ...style }}
					>
						<div className={styles.contentModal}>
							<div className={classNames(styles.iconClose, styles.center)} onClick={onCloseModal}><IconClose /></div>
							<div className={styles.content}>
								<LogiItest />
								<span dangerouslySetInnerHTML={{__html: t('modal-back-to-school')['content']}}></span>
								<div className={styles.action}>
									<button onClick={onCloseModal}>{t('common')['close']}</button>
								</div>
							</div>
						</div>
					</ModalWrapper>
				) : (
					<ModalWrapper
						className={styles.modalContainer}
						isOpen={isShow}
						backdrop={true}
						onCloseModal={onCloseModal}
						style={{ ...style }}
					>
						<div className={styles.contentModal}>
							<div className={classNames(styles.iconClose, styles.center)} onClick={onCloseModal}><IconClose /></div>
							<img src='/images-v2/home/popup-student.png' alt="" className={styles.imageContent}/>
						</div>
					</ModalWrapper>
				)
			}
		</>
	);
};

export default ModalShowNotiForStudent;