import React, { useContext, useEffect, useMemo, useState } from 'react'

import classNames from 'classnames';
import { FormikContext, useFormik } from 'formik';
import { useRouter } from 'next/router'
import qs from "qs";

import ExtendIcon from '@/assets/icons/extend.svg'
import ResetIcon from '@/assets/icons/reset-icon.svg'
import SearchIcon from '@/assets/icons/search.svg'
import SelectField from '@/componentsV2/Common/Form/SelectField'
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button'
import useNoti from '@/hooks/useNoti';
import { CODE_ASSIGN_OPTION, paths as path } from "@/interfaces/constants";
import { WrapperContext } from '@/interfaces/contexts'
import { cleanObject, delayResult } from "@/utils/index";
import { paths } from 'api/paths';
import { callApiConfigError } from 'api/utils'

import BaseForm from '../Common/Controls/BaseForm'
import CheckSelectPicker from '../Common/Form/CheckSelectPicker';
import DateTimePicker from '../Common/Form/DateTimePicker'
import InputTextField from '../Common/Form/InputTextField'
import NoFieldData from '../NoDataSection/NoFieldData';
import styles from './AssignExams.module.scss'
import AssignExamsGrid from './AssignExamsGrid';
import { OBJECT_ASSIGN } from '@/constants/index';
import useTranslation from "@/hooks/useTranslation";

const formatDate = (dateString: any) => {
  const date = new Date(dateString)
  const formattedDate = date.toISOString().substring(0, 10)
  return formattedDate
}

const EMPTY_TARGET = 0



export default function AssignExams() {
  const { t } = useTranslation();
  const targetOptions = [
    {
      value: OBJECT_ASSIGN.CLASS,
      label: t('assigned-subject')['class'],
    },
    {
      value: OBJECT_ASSIGN.GROUP,
      label: t('assigned-subject')['group'],
    },
    {
      value: OBJECT_ASSIGN.STUDENT,
      label: t('assigned-subject')['student'],
    }
  ]
  const { globalModal } = useContext(WrapperContext)
  const { getNoti } = useNoti()
  const router = useRouter()
  const { push, query } = router
  const [sentUnitTestList, setSentUnitTestList] = useState([] as any[])
  const [totalPage, setTotalPage] = useState(1)
  const [total, setTotal] = useState(0)
  const [searching, setSearching] = useState(false)
  const [isLoading, setIsLoading] = useState(false)
  const [showMoreFilter, setShowMoreFilter] = useState(false)
  const [valueName, setValueName] = useState('')
  const currentClassId = Number(query.classeSlug)
  const currentPage = Number(query.page) || 1

  const [groupData, setGroupData] = useState([])
  const [studentData, setStudentData] = useState([])
  const [targetValue, setTargetValue] = useState(EMPTY_TARGET)
  const [isFirstRender, setIsFirstRender] = useState(true)

  const wrapperDiv: HTMLDivElement = document.querySelector('#assign-container')
  const scrollToTop: any = () => {
    if (wrapperDiv) {
      wrapperDiv.scrollTop = 0
      wrapperDiv.style.scrollBehavior = 'smooth'
    }
  }

  const fetchClassGroups = async () => {
    const response: any = await callApiConfigError(
      `${paths.api_teacher_classes}/${currentClassId}/groups`,
    )
    if (response?.result && response?.data) {
      setGroupData(response.data)
    }
  }

  const fetchClassStudents = async () => {
    const response: any = await callApiConfigError(
      `${paths.api_teacher_classes}/${currentClassId}/students?includeDeleted=1`,
    )
    if (response?.result && response?.data) {
      setStudentData(response.data)
    }
  }

  useEffect(() => {
    fetchClassGroups()
    fetchClassStudents()
  }, [])

  const groupOptions = groupData.map(({ id, name }) => ({
    value: id,
    label: name,
  }))

  const [studentOptions, setStudentOptions] = useState(studentData.map(({ id, full_name }) => ({
    value: id,
    label: full_name
  })))

  const onSubmit = async (values: any) => {
    // console.log('value----assign-', { values })
  }

  const formikFormUnitTest = useFormik({
    initialValues: {
      name: '',
      startDate: '',
      endDate: '',
      examStatus: '',
      assignFor: 0,
      groupId: '',
      students: '',
    },
    onSubmit: onSubmit,
  })

  useEffect(() => {
    if (formikFormUnitTest.values.name === '') {
      setIsFirstRender(true)
    }
    const delay = 300
    const timerId = setTimeout(() => {
      setValueName(formikFormUnitTest.values.name)
    }, delay)

    return () => {
      clearTimeout(timerId)
    }
  }, [
    formikFormUnitTest.values.name
  ])

  const handleSelectStatus = (selectedOption: any) => {
    formikFormUnitTest.setFieldValue('examStatus', selectedOption.value)
  }

  const handleSelectTarget = (selectedOption: any) => {
    setTargetValue(selectedOption.value);
    formikFormUnitTest.setFieldValue('groupId', '')
    formikFormUnitTest.setFieldValue('students', '')
  }

  const handleSelectGroup = (selectedOption: any) => {
    formikFormUnitTest.setFieldValue('groupId', selectedOption.value)
  }

  const objQuery = useMemo(() => {
    return {
      step: 3,
      page: valueName != '' ||
        formikFormUnitTest.values.startDate != '' ||
        formikFormUnitTest.values.endDate != '' ||
        formikFormUnitTest.values.groupId != '' ||
        formikFormUnitTest.values.students != '' ||
        formikFormUnitTest.values.assignFor ||
        formikFormUnitTest.values.examStatus != '' ? Math.max(currentPage - 1, 0) : currentPage - 1,
      name: valueName != '' ? valueName : undefined,
      startDate: formikFormUnitTest.values.startDate != '' ? formatDate(formikFormUnitTest.values.startDate) : undefined,
      endDate: formikFormUnitTest.values.endDate != '' ? formatDate(formikFormUnitTest.values.endDate) : undefined,
      examStatus: formikFormUnitTest.values.examStatus != '' ? formikFormUnitTest.values.examStatus : undefined,
      assignFor: formikFormUnitTest.values.assignFor ? formikFormUnitTest.values.assignFor : undefined,
      groupId: formikFormUnitTest.values.groupId != '' ? formikFormUnitTest.values.groupId : undefined,
      students: formikFormUnitTest.values.students != '' ? formikFormUnitTest.values.students : undefined,
    }
  }, [
    currentPage,
    valueName,
    formikFormUnitTest.values.startDate,
    formikFormUnitTest.values.endDate,
    formikFormUnitTest.values.examStatus,
    formikFormUnitTest.values.groupId,
    formikFormUnitTest.values.students,
    formikFormUnitTest.values.assignFor,
  ])

  const handlePageChange = async (page: number) => {
    scrollToTop()
    const nonEmptyQuery = cleanObject(objQuery)
    nonEmptyQuery.page = page
    push({
      pathname: `${path.classes}/${currentClassId}`,
      query: {
        ...nonEmptyQuery
      }
    })
  }

  const fetcher = async () => {
    setIsLoading(true)
    scrollToTop()
    const queryString = qs.stringify(objQuery, { arrayFormat: 'repeat' })
    const response: any = await delayResult(
      callApiConfigError(
        `${paths.api_teacher_classes}/${currentClassId}/unit-tests?${queryString}
        `), 400)
    setIsLoading(false)
    if (response.result && response.data) {
      setSentUnitTestList(response.data.content)
      setTotalPage(response.data.totalPage)
      setTotal(response.data.total)
      setIsFirstRender(false)
    }
  }

  const pushQuery = (data: any) => {
    const nonEmptyQuery = cleanObject(data)
    nonEmptyQuery.page = 1
    push({
      pathname: `${path.classes}/${currentClassId}`,
      query: {
        ...nonEmptyQuery,
      }
    })
  }

  useEffect(() => {
    fetcher().finally()
    // console.log('render here')
  }, [
    currentPage,
    objQuery
  ])

  useEffect(() => {
    pushQuery(objQuery)
    if (
      valueName ||
      formikFormUnitTest.values.startDate ||
      formikFormUnitTest.values.endDate ||
      formikFormUnitTest.values.examStatus ||
      formikFormUnitTest.values.groupId ||
      formikFormUnitTest.values.students ||
      formikFormUnitTest.values.assignFor
    ) {
      setSearching(true)
    } else {
      setSearching(false)
    }
  }, [
    valueName,
    formikFormUnitTest.values.startDate,
    formikFormUnitTest.values.endDate,
    formikFormUnitTest.values.examStatus,
    formikFormUnitTest.values.groupId,
    formikFormUnitTest.values.students,
    formikFormUnitTest.values.assignFor
  ])

  const handleReset = () => {
    setIsFirstRender(true)
    formikFormUnitTest.resetForm()
    setValueName('')
    setTargetValue(EMPTY_TARGET)
  }

  const isSearchEmpty = useMemo(() => {
    if (!sentUnitTestList || sentUnitTestList.length === 0 && !searching) {
      return false
    }
    return true
  }, [searching, sentUnitTestList])

  const handleDeleteUniTtest = async (id: number, data: any, nameOfClass: string) => {
    const response: any = await callApiConfigError(`${paths.api_teacher_classes}/${id}/unit-test/${data.id}`,
      'DELETE',
      'Token',
    )
    if (response?.result) {
      getNoti('success', 'topCenter', t(t('common-get-noti')['delete-assign-unitTest'], [nameOfClass]))
      if (currentPage === totalPage && sentUnitTestList.length === 1 && currentPage !== 1) {
        push({
          pathname: `${path.classes}/${currentClassId}`,
          query: {
            page: Math.max(currentPage - 1, 1),
          },
        })
      } else {
        await fetcher()
      }
    }
  }

  const handleSendMoreUnitTest = async (id: number) => {
    const response: any = await callApiConfigError(`${paths.api_teacher_classes}/${id}`,
      'GET',
      'Token',
    )
    if (response.result && response.data) {
      globalModal.setState({
        id: 'send-unit-test',
        dataClass: response.data,
        content: {
          onSubmit: () => {
            setIsFirstRender(true)
            fetcher().finally()
          }
        },
      })
    }
  }

  const handleCancelSendUnitTest = async (id: number, data?: any) => {
    const response: any = await callApiConfigError(`${paths.api_teacher_classes}/${id}`,
      'GET',
      'Token',
    )
    if (response.result && response.data) {
      globalModal.setState({
        id: 'action-confirm',
        type: 'cancel-send-unit-test',
        content: {
          class: response.data?.name,
          onSubmit: () => {
            handleDeleteUniTtest(id, data, response.data?.name)
          }
        }
      })
    }
  }
  const onSearch = (searchKeyword: string) => {
    console.log({searchKeyword});
    
    setStudentOptions(prevs => {
      console.log({studentData});
      
      return studentData.filter(item => item.label.toLowerCase().includes(searchKeyword.toLowerCase()))
    })
  }
  return (
    <>
      <div className={styles.assignExamsWrapper} id='assign-wrapper'>
      {(isSearchEmpty || isFirstRender) && (
        <div className={styles.boxAction}>
          <FormikContext.Provider value={formikFormUnitTest}>
            <BaseForm className={styles.actionFilterSearch}>
              <div className={styles.leftSection}>
                <InputTextField
                  name="name"
                  autoFocus={undefined}
                  className={styles.aInputWithLabel}
                  type={'text'}
                  label=""
                  maxLength={100}
                  onChange={formikFormUnitTest.handleChange}
                  disabled={false}
                  hideErrorMessage={undefined}
                  iconLeft={<SearchIcon />}
                  iconRight={undefined}
                  placeholder={t('assign-exam-filter')['search-name']}
                  autoComplete="off"
                  onlyNumber={false}
                />
                {showMoreFilter && (
                  <>
                    <DateTimePicker
                      name="startDate"
                      placement={'autoVertical'}
                      style={undefined}
                      onChange={undefined}
                      className={styles.timeInput}
                      label={t('common')['start-date']}
                      disabled={false}
                    />
                    <DateTimePicker
                      name="endDate"
                      placement={'autoVertical'}
                      style={undefined}
                      onChange={undefined}
                      className={styles.timeInput}
                      label={t('common')['end-date']}
                      disabled={false}
                    />
                    <SelectField
                      options={CODE_ASSIGN_OPTION.map(item => ({...item,label: t('status')[item.label]}))}
                      className={styles.selectField}
                      menuClassName={styles.menuCustom}
                      placeHolder={t('assign-exam-filter')['status-exam']}
                      titleDropdown={undefined}
                      name={'examStatus'}
                      disable={false}
                      onChange={handleSelectStatus}
                      key={'examStatus'}
                    />
                    <SelectField
                      name="assignFor"
                      key="assignFor"
                      options={targetOptions}
                      className={styles.selectField}
                      menuClassName={styles.menuCustom}
                      placeHolder={t('assign-exam-filter')['target']}
                      onChange={handleSelectTarget}
                      titleDropdown={undefined}
                      disable={false}
                    />
                    {targetValue == OBJECT_ASSIGN.GROUP && (
                      <SelectField
                        options={groupOptions}
                        className={styles.selectField}
                        menuClassName={styles.menuCustom}
                        placeHolder={t('assigned-subject')['group']}
                        titleDropdown={undefined}
                        name='groupId'
                        disable={false}
                        onChange={handleSelectGroup}
                        key='groupId'
                      />
                    )}
                    {targetValue == OBJECT_ASSIGN.STUDENT && (
                      <CheckSelectPicker
                        name="students"
                        label={t('left-menu')['students']}
                        searchable={true}
                        cleanable={false}
                        className={styles.selectStudents}
                        data={studentOptions}
                        menuClassName={styles.menuStudents}
                        onSearch={onSearch}
                      />
                    )}
                  </>
                )}
              </div>
              <div className={styles.rightSection}>
                <div
                  className={classNames(styles.extendBtn, {
                    [styles.active]: showMoreFilter
                  })}
                  onClick={() => setShowMoreFilter(!showMoreFilter)}
                >
                  <ExtendIcon />
                </div>
                <ButtonCustom
                  onClick={handleReset}
                  iconRight={<ResetIcon />}
                  className={classNames(styles.aButtonReset, {
                    [styles.disabled]:
                      formikFormUnitTest.values.name.length === 0 &&
                      formikFormUnitTest.values.startDate.length === 0 &&
                      formikFormUnitTest.values.endDate.length === 0 &&
                      formikFormUnitTest.values.examStatus.length === 0 &&
                      formikFormUnitTest.values.groupId.length === 0 &&
                      formikFormUnitTest.values.students.length === 0 &&
                      targetValue == EMPTY_TARGET
                  })}
                >
                  {t('common')['reset']}
                </ButtonCustom>
              </div>
            </BaseForm>
          </FormikContext.Provider>
          <div className={styles.assignMoreBtn}>
            <button onClick={() => handleSendMoreUnitTest(currentClassId)}>
              <span>{t('assign-exam-filter')['assign-new-test']}
              </span>
            </button>
          </div>
        </div>
      )}
        {sentUnitTestList.length > 0 || isLoading ? (
          <AssignExamsGrid
            currentPage={currentPage}
            total={total}
            totalPage={totalPage}
            handlePageChange={handlePageChange}
            sentUnitTestList={sentUnitTestList}
            isLoading={isLoading}
            handleCancelSendUnitTest={handleCancelSendUnitTest}
          />
        ) : (
            <NoFieldData
              field='assign_exams'
              isSearchEmpty={isSearchEmpty}
              handleCreate={() => handleSendMoreUnitTest(currentClassId)}
            />
        )}
      </div>
    </>
  )
}