import React, {useEffect, useState} from "react";
import classNames from "classnames";
import dayjs from "dayjs";
import {useSession} from "next-auth/client";
import {useRouter} from "next/router";
import {Button, Loader} from "rsuite";

import IconBack from "@/assets/icons/back-arrow.svg";
import {commonDeleteStatus} from "@/constants/index";
import {paths as pathsUrl} from "@/interfaces/constants";
import {paths} from "api/paths";
import {callApi} from "api/utils";
import {Wrapper} from "components/templates/wrapper";
import { toFixedNumber } from "@/utils/number";

import styles from "./index.module.scss";
import useTranslation from "@/hooks/useTranslation";

const Index = () => {
    const { t, locale, subPath } = useTranslation()
    const [session] = useSession();
    const user: any = session?.user;
    const {isReady, push, query, back} = useRouter();
    const [dataAssign, setDataAssign] = useState([]);
    const [dataUnitTest, setDataUnitTest] = useState(null);
    const [isLoad, setIsLoad] = useState(true);

    const fetchApi = async () => {
        try {
            setIsLoad(true);
            const response: any = await callApi(`${paths.api_get_student_unit_test_result}?unitTestId=${query?.unitTestId}`);
            setIsLoad(false);
            if (response.code === 200) {
                const data = response?.data;
                setDataAssign(data?.content);
                setDataUnitTest(data?.unitTest);
            }
        } catch (e) {
            push(pathsUrl.assignedUnitTest).finally()
        }

    };

    useEffect(() => {
        if (isReady) {
            fetchApi().finally();
        }
    }, [isReady]);

    const onBack = () => {
        back();
    }

    const getPropButtonByStatus = (unitTestInfo: any) => {
        const urlPracticeTest = `${subPath}/practice-test/${user?.id}===${unitTestInfo?.id}`

        return {
                label: unitTestInfo?.deleted === commonDeleteStatus.DELETED ? t('learning-result-page')['deleted-exam'] : t('learning-result-page')['start-exam'],
                className: "ongoing",
                url: urlPracticeTest,
            };
    }
    const renderTextWithIndex = (index: number) => {
        if (locale === 'vi') {
            return `Lần thi ${index}`;
        }
        switch (index) {
            case 1:
                return `${index}st times`;
            case 2:
                return `${index}nd times`;
            case 3:
                return `${index}rd times`;
            default:
                return `${index}th times`;
        }
    }

    return (
        <Wrapper pageTitle={dataUnitTest?.name} hasStickyFooter={false}>
            <div className={styles.containerAssign}>
                <div className={styles.headerBack} onClick={onBack}><IconBack /><span>{t('common')['back']}</span></div>
                <div className={styles.contentAssign}>
                    {isLoad ? (
                        <Loader backdrop vertical content="Loading..." />
                    ) : (
                        <>
                            <div className={styles.assignItem}>
                                <span className={styles.title}>{dataUnitTest?.name}</span>
                                <div className={styles.contentData}>
                                    <div className={classNames(styles.itemData, styles.taskName)}>
                                        <span>{t('learning-result-page')['submit-time']}:</span>
                                        <span>{t('learning-result-page')['total-question']}:</span>
                                    </div>
                                    <div className={styles.itemData}>
                                        <span>{dataUnitTest?.time} {t('time')['minutes']}</span>
                                        <span>{dataUnitTest?.total_question} {t('learning-result-page')['questions']}</span>
                                    </div>
                                </div>
                                <Button as="a"
                                        target="_blank"
                                        href={getPropButtonByStatus(dataUnitTest).url}
                                        appearance="primary"
                                        className={classNames(styles.buttonAction,
                                            styles[getPropButtonByStatus(dataUnitTest).className])}
                                >
                                    {getPropButtonByStatus(dataUnitTest).label}
                                </Button>
                            </div>
                            {!!dataAssign.length &&
                                dataAssign.map((unitAssign: any, index: number) => {
                                    const href = `${subPath}/practice-test/${user?.id}===${unitAssign?.unit_test_id}===${unitAssign?.id}`
                                    return (
                                        <div className={styles.assignItem} key={unitAssign?.id}>
                                            <span className={classNames(styles.title, styles.ended)}>{renderTextWithIndex(dataAssign.length - index)}: {dataUnitTest?.name}</span>
                                            <div className={classNames(styles.contentData, styles.ended)}>
                                                <div className={classNames(styles.itemData, styles.taskName)}>
                                                    <span>{t('learning-result-page')['point']}:</span>
                                                    <span>{t('learning-result-page')['deadline-time']}:</span>
                                                </div>
                                                <div className={styles.itemData}>
                                                    <span>{toFixedNumber(unitAssign?.point)}/{unitAssign?.max_point}</span>
                                                    <span>{dayjs(unitAssign?.created_date).format("DD/MM/YYYY HH:mm")}</span>
                                                </div>
                                            </div>
                                            <Button as={"a"} target="_blank" href={href} appearance="primary" className={classNames(styles.buttonAction, styles.ended)}>{t('learning-result-page')['exam-review']}</Button>
                                        </div>
                                    );
                                })
                            }
                        </>
                    )}
                </div>
            </div>
        </Wrapper>
    );
};

export default Index;
