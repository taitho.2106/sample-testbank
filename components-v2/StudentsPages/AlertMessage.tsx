import React from 'react'

import classNames from 'classnames';

import CloseIcon from '@/assets/icons/icon-close.svg'
import ErrorIcon from '@/assets/icons/icon-error.svg'

import styles from './AlertMessage.module.scss';
import useTranslation from "@/hooks/useTranslation";


type Props = {
    onClose?:() => void
    onShow?: () => void
    className?:string
    iconErr?:any
    iconClose?:any
    style?:any
}
export default function AlertMessage({ onClose, onShow, className = '', style={} }: Props) {
    const { t } = useTranslation();
  return (
    <div className={classNames(styles.importFailWrapper, className)} style={style}>
        <div className={styles.importFail}>
            <div className={styles.contentDes}>
                <ErrorIcon />
                <span>
                    {t('alert-message')['content']}{' '}
                    <span className={styles.underline} onClick={onShow}>
                    {t('alert-message')['view-action']}
                    </span>
                </span>
            </div>
            <div className={styles.iconClose}>
                <CloseIcon onClick={onClose} />
            </div>
        </div>
    </div>
  )
}
