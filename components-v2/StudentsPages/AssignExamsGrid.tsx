import React, { Fragment, useState } from 'react'

import classNames from 'classnames';
import dayjs from 'dayjs';
import { useSession } from 'next-auth/client';
import Link from 'next/dist/client/link';
import { useRouter } from 'next/router'
import { AiOutlineCloseCircle, AiOutlineDownload, AiOutlineEdit, AiOutlineEye } from 'react-icons/ai'
import { BsBookmark } from 'react-icons/bs'
import { GoKebabVertical } from 'react-icons/go'
import { Popover, Tooltip, Whisper } from 'rsuite'

import IconLinkUrl from '@/assets/icons/link-url.svg'
import { DATE_DISPLAY_FORMAT } from '@/constants/index';
import useNoti from '@/hooks/useNoti';
import { STATUS_CODE_EXAM, CODE_ASSIGN_OPTION, paths as pathUrl } from "@/interfaces/constants";
import { UNIT_TYPE } from '@/interfaces/struct'
import { StructType } from '@/interfaces/types'
import { onCopyUrl } from '@/utils/index';

import PaginationComponent from '../Common/Pagination/PaginationComponent';
import SkeletonLoading from '../Common/SkeletonLoading'
import styles from './AssignExamsGrid.module.scss'
import useTranslation from "@/hooks/useTranslation";

interface Props {
    currentPage?: number
    total?: number
    totalPage?: number
    handlePageChange?: any
    sentUnitTestList: any[]
    isLoading?: boolean
    handleCancelSendUnitTest?: (id: number, data?: any) => void
}

const AssignExamsGrid = ({
    currentPage,
    total,
    totalPage,
    handlePageChange,
    sentUnitTestList,
    isLoading,
    handleCancelSendUnitTest
}: Props) => {
    const { t, subPath } = useTranslation();
    const itemSkeleton = 10
    const router = useRouter()
    const { push, query } = router

    const [session] = useSession()
    const user: any = session.user
    const { getNoti } = useNoti()
    const [isTooltipVisible, setIsTooltipVisible] = useState(true);

    const currentClassId = Number(query.classeSlug)

    const mapExamStatusThumbnail = {
        [STATUS_CODE_EXAM.UPCOMING]: '/images-v2/unit-test/exam-upcoming.png',
        [STATUS_CODE_EXAM.ONGOING]: '/images-v2/unit-test/exam-ongoing.png',
        [STATUS_CODE_EXAM.ENDED]: '/images-v2/unit-test/exam-ended.png',
    }

    const mapExamStatusStyle = {
        [STATUS_CODE_EXAM.UPCOMING]: styles.upcoming,
        [STATUS_CODE_EXAM.ONGOING]: styles.ongoing,
        [STATUS_CODE_EXAM.ENDED]: styles.ended,
    }

    const onClickCopy = (unit: any) => {
        const url = `${subPath}/practice-test/${user?.id}===${unit.unitTest.id}?idAssign=${unit.id}`
        onCopyUrl(url)
        getNoti('success', 'topCenter', t('common-get-noti')['copy-url-assign-succeed'])
    }

    const handleLearningResult = (assignedExam: any) => {
        if (assignedExam.student) {
            return router.push(`${pathUrl.learningResult}/${assignedExam.id}?studentId=${assignedExam.student?.id}`)
            
        } else return router.push(`${pathUrl.learningResult}/${assignedExam.id}`)
    }    

    return (
        <>
            <div className={styles.mainContainer} id='assign-container'>
            {isLoading ? (
                <div className={styles.gridContainer}>
                    {Array.from(Array(itemSkeleton), (_, index: number) => {
                        return (
                            <Fragment key={index}>
                                <SkeletonLoading className={styles.containerItem}>
                                    <div
                                        className={`${styles.boxSkeleton} ${styles.boxImageSkeleton}`}
                                    ></div>
                                    <span
                                        className={styles.unitTestName}
                                        style={{
                                            width: '50%',
                                        }}
                                    ></span>
                                    <span className={styles.unitTestName}></span>
                                    <div
                                        className={`${styles.boxSkeleton} ${styles.boxInfoSkeleton}`}
                                    ></div>
                                </SkeletonLoading>
                            </Fragment>
                        )
                    })}
                </div>
            ) : (
                <>
                    <div className={styles.gridContainer} id='grid-container'>
                        {sentUnitTestList.map((exam: any, index: number) => {
                            const statusThumbnail = mapExamStatusThumbnail[exam.examStatus]
                            const statusClassName = mapExamStatusStyle[exam.examStatus]
                            const statusLabel = CODE_ASSIGN_OPTION.filter((item) => item.value == exam.examStatus)[0].label                           

                            return (
                                <Whisper                             
                                    key={exam.id}
                                    trigger={exam.examStatus === STATUS_CODE_EXAM.UPCOMING ? 'hover' : 'none'}
                                    placement='auto'
                                    followCursor
                                    container={() =>
                                        document.querySelector(`.t-wrapper__main`)
                                    }
                                    preventOverflow
                                    delayOpen={200}
                                    speaker={
                                        <Tooltip 
                                            arrow={false}
                                            className={classNames(styles.toolTipWarning, {
                                                [styles.disabled]: !isTooltipVisible
                                            })}
                                        >
                                            <span>{t('assign-exam')['no-result']}</span>
                                        </Tooltip>
                                    }
                                >
                                <div
                                    className={classNames(
                                        styles.containerItem,
                                        statusClassName,
                                        styles.hover,
                                    )}                                
                                    onClick={(e) => {
                                        e.stopPropagation()
                                        
                                        if(exam.examStatus !== STATUS_CODE_EXAM.UPCOMING) {
                                            handleLearningResult(exam)
                                        }
                                    }}
                                >
                                    <div className={styles.thumbnail}>
                                        <div
                                            className={classNames(
                                                styles.statusBox,
                                                statusClassName,
                                            )}
                                        >
                                            <span className={statusClassName}>{t('status')[statusLabel]}</span>
                                        </div>
                                        <img src={statusThumbnail} alt="" />
                                        <Whisper
                                            placement={'auto'}
                                            trigger="click"
                                            container={() =>
                                                document.querySelector('#assign-container')
                                            }
                                            speaker={
                                                <Popover
                                                    arrow={false}
                                                    className={styles.popoverModal}
                                                    onMouseEnter={() => {
                                                        if (exam.examStatus === STATUS_CODE_EXAM.UPCOMING) {
                                                            setIsTooltipVisible(false)
                                                        }
                                                    }}
                                                    onMouseLeave={() => {
                                                        if (exam.examStatus === STATUS_CODE_EXAM.UPCOMING) {
                                                            setIsTooltipVisible(true)
                                                        }
                                                    }}
                                                >
                                                    <div className={styles.tableActionPopover}>
                                                        {exam.examStatus !== STATUS_CODE_EXAM.ENDED &&
                                                            <div 
                                                            className={styles.actionItem} 
                                                            onClick={() => onClickCopy(exam)}
                                                            >
                                                                <IconLinkUrl />
                                                                <span>{t('assign-exam')['copy-url']}</span>
                                                            </div>
                                                        }                                                    
                                                        <Link
                                                            href={`/unit-test/${exam.unitTest.id}?mode=view&m=mine`}
                                                        >
                                                            <a
                                                                className={styles.actionItem}
                                                                target="_blank"
                                                            >
                                                                <AiOutlineEye />
                                                                <span>{t('assign-exam')['content-view']}</span>
                                                            </a>
                                                        </Link>
                                                        {exam.examStatus !== STATUS_CODE_EXAM.UPCOMING && (                                                            
                                                            <Link
                                                                href={exam.student ?
                                                                    `${pathUrl.learningResult}/${exam.id}?studentId=${exam.student?.id}` :
                                                                    `${pathUrl.learningResult}/${exam.id}`}
                                                            >
                                                                <a
                                                                    className={styles.actionItem}
                                                                >
                                                                    <BsBookmark />
                                                                    <span>{t('assign-exam')['learing-result-view']}</span>
                                                                </a>
                                                            </Link>
                                                        )}
                                                        {exam.examStatus === STATUS_CODE_EXAM.UPCOMING && (
                                                            <>
                                                                {/*<div*/}
                                                                {/*    className={styles.actionItem}*/}
                                                                {/*    onClick={() => {*/}
                                                                {/*        //*/}
                                                                {/*    }}*/}
                                                                {/*>*/}
                                                                {/*    <AiOutlineEdit />*/}
                                                                {/*    <span>Chỉnh sửa thông tin giao đề</span>*/}
                                                                {/*</div>*/}
                                                                <div
                                                                    className={styles.actionItem}
                                                                    onClick={() =>
                                                                        handleCancelSendUnitTest(
                                                                            currentClassId,
                                                                            exam,
                                                                        )
                                                                    }
                                                                >
                                                                    <AiOutlineCloseCircle />
                                                                    <span>{t('assign-exam')['delete-assign-exam']}</span>
                                                                </div>
                                                            </>
                                                        )}
                                                        {/* {exam.examStatus !== STATUS_CODE_EXAM.ENDED && (
                                                            <div
                                                                className={styles.actionItem}
                                                                onClick={() => {
                                                                    //
                                                                }}
                                                            >
                                                                <AiOutlineDownload />
                                                                <span>Xuất kết quả thi</span>
                                                            </div>
                                                        )} */}
                                                    </div>
                                                </Popover>
                                            }
                                        >
                                            <div className={styles.actionBtn} 
                                                onClick={(e) => {
                                                    e.stopPropagation()
                                                    e.preventDefault()
                                                }}
                                                onMouseEnter={() => {
                                                    if (exam.examStatus === STATUS_CODE_EXAM.UPCOMING) {
                                                        setIsTooltipVisible(false)
                                                    }
                                                }}
                                                onMouseLeave={() => {
                                                    if (exam.examStatus === STATUS_CODE_EXAM.UPCOMING) {
                                                        setIsTooltipVisible(true)
                                                    }
                                                }}
                                            >
                                                <GoKebabVertical color="#7C68EE" />
                                            </div>
                                        </Whisper>
                                    </div>
                                    <div className={styles.unitTestName}>
                                        <span>{exam.unitTest.name}</span>
                                    </div>
                                    <div className={styles.infoContent}>
                                        <div
                                            className={styles.testType}
                                            title={t('unit-type')[UNIT_TYPE.find(
                                                (find: StructType) =>
                                                    find?.code === `${exam?.unit_type}`,
                                            )?.key] ||
                                                exam?.unit_type ||
                                                '---'}>
                                            <span>
                                                {t('unit-type')[UNIT_TYPE.find(
                                                    (find: StructType) =>
                                                        find?.code === `${exam?.unit_type}`,
                                                )?.key] ||
                                                    exam?.unit_type ||
                                                    '---'}
                                            </span>
                                        </div>
                                        <div
                                            className={styles.assignTarget}
                                            title={exam.group ? exam.group.name : exam.student ? exam.student.full_name : t('assigned-subject')['class']}
                                        >
                                            <span>
                                                {exam.group ? exam.group.name : exam.student ? exam.student.full_name : t('assigned-subject')['class']}
                                            </span>
                                        </div>
                                        <div className={styles.dateAndTime}>
                                            <div
                                                className={classNames(
                                                    styles.textDisplay,
                                                    statusClassName,
                                                )}
                                            >
                                                <span>{t('unit-test-assigned-page')['from']}</span>
                                                <span>{t('unit-test-assigned-page')['to']}</span>
                                            </div>
                                            <div
                                                className={classNames(
                                                    styles.timeDisplay,
                                                    statusClassName,
                                                )}
                                            >
                                                <span>{dayjs(exam.start_date).format("HH:mm")}</span>
                                                <span>{dayjs(exam.end_date).format("HH:mm")}</span>
                                            </div>
                                            <div
                                                className={classNames(
                                                    styles.dateDisplay,
                                                    statusClassName,
                                                )}
                                            >
                                                <span>{dayjs(exam.start_date).format(DATE_DISPLAY_FORMAT)}</span>
                                                <span>{dayjs(exam.end_date).format(DATE_DISPLAY_FORMAT)}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </Whisper>                    
                            )
                        })}
                    </div>
                    {totalPage > 1 && (
                        <div className={styles.newPagination}>
                            <PaginationComponent
                                total={total}
                                pageSize={20}
                                current={currentPage}
                                showTotal={() => {
                                    //
                                }}
                                onChange={handlePageChange}
                                showSizeChanger={false}
                            />
                        </div>
                    )}
                </>
            )}
            </div>
        </>
    )
}

export default AssignExamsGrid