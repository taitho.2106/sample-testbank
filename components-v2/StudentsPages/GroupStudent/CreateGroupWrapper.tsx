import React, { Fragment, useEffect, useState } from "react";

import { useRouter } from "next/router";

import { delayResult } from "@/utils/index";

import { callApi, callApiConfigError } from "../../../api/utils";
import CreateGroup from "./CreateGroup";
import styles from "./CreateGroupWrapper.module.scss"
import SkeletonLoading from "@/componentsV2/Common/SkeletonLoading";
import axios from "axios";
import login from "../../../pages/login";
import { Loader } from "rsuite";

const CreateGroupWrapper = ({isLoadPage}:any) => {
    const { query,push, isReady } = useRouter()
    const [isLoad, setIsLoad] = useState(true)
    const [dataStudents, setDataStudents] = useState([])
    const [studentsInGroup, setStudentsInGroup] = useState(null)
    const fetcher = async () => {
        const request1 = axios.get(`/api/classes/${query.classeSlug}/students`)
        let request2:any
        if(query.group){
            request2 = axios.get( `/api/classes/${query.classeSlug}/group/${query.group}`)
        }else{
            request2 = {}
        }
        Promise.all([request1, request2]).then((values:any) => {
            // const studentOfClass = values[0].data?.data
            // const studentOfGroupClass = values[1].data?.data?.students
            // const dataAllowToCreateGroup = studentOfClass?.filter((std: any) => !studentOfGroupClass?.some((item: any) => item.id === std.id))
            setDataStudents(values[0].data?.data)
            setStudentsInGroup(values[1].data?.data)
            setIsLoad(false)
        }).catch(e=>{
            setIsLoad(false)
            push(`/classes/${query.classeSlug}?step=2`).finally()
        })
    }

    useEffect(() => {
        const timer = setTimeout(  async () => {
            fetcher().finally()
        },400)
        return () => clearTimeout(timer)
    }, [isReady])

    return (
        <>
            {isLoad ? (
                <Loader backdrop vertical content="Loading..."/>
            ) : (
                <CreateGroup
                    isReLoad={isLoadPage}
                    dataStudents={dataStudents}
                    dataInGroup={studentsInGroup}
                />
            )}

        </>
    );
};

export default CreateGroupWrapper;