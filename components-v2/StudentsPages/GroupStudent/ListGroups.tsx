import React, { useEffect, useState } from "react";

import classNames from 'classnames';
import { useRouter } from 'next/router';
import { GoKebabVertical } from 'react-icons/go'
import { Popover, Whisper } from 'rsuite';
import GroupStds from '@/assets/icons/ic-group-stds.svg';
import DisbandIcon from '@/assets/icons/disband-group.svg';
import EditIcon from '@/assets/icons/icon-edit-group.svg';
import ModalWrapper from '@/componentsV2/Common/ModalWrapper';
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button';
import useNoti from '@/hooks/useNoti';
import { callApiConfigError } from 'api/utils';

import styles from './ListGroups.module.scss';
import useTranslation from "@/hooks/useTranslation";

type ListGroups = {
    data?:any[]
    className?:string
    isReLoad?:() => void
}
export default function ListGroups({data = [],className='', isReLoad}:ListGroups) {
    const { t } = useTranslation();
    const { getNoti } = useNoti()
    const { query, push } = useRouter()
    const [isShowModal, setIsShowModal] = useState(false)
    const [dataGroup, setDataGroup] = useState(null)
    const [isLoading, setIsLoading] = useState(false)
    const handleSubmitModal = async (value:any) => {
        setIsLoading(true)
        const response: any = await callApiConfigError(
            `/api/classes/${query.classeSlug}/group/${value?.id}`,
            'DELETE'
            )
        if(response.code === 200){
            setIsShowModal(false)
            getNoti("success", "topCenter", t('common-get-noti')['disband-group-succeed']);
            if(isReLoad){
                setIsLoading(false)
                isReLoad()
            }
        }else{
            setIsLoading(false)
            getNoti('error', 'topCenter', response?.message)
        }
    }
    const handleDisBand = (value:any,e:any) => {
        e.preventDefault()
        e.stopPropagation()
        setIsShowModal(true)
        setDataGroup(value)
    }
    const handleEdit = (val:any,e:any) => {
        e.preventDefault()
        e.stopPropagation()
        push({
            pathname: `/classes/${query.classeSlug}`,
            query:{
                step: 2,
                type: 'edit',
                group: val?.id
            }
        })
    }
    const handleClickView = (val:any) => {
        push({
            pathname: `/classes/${query.classeSlug}`,
            query:{
                step: 2,
                type: 'view',
                group: val?.id
            }
        })
    }


    return (
        <div className={classNames(styles.groupItem,className)}>
            {isShowModal && <ModalDisBand
                isOpen={isShowModal}
                onCloseModal={() => setIsShowModal(false)}
                onSubmitModal={handleSubmitModal}
                dataGroup={dataGroup}
                isLoading={isLoading}
            />}
            {data.map((item:any,index:number)=>{
            return (
              <div key={index} className={styles.item}
                onClick={()=> handleClickView(item)}>
                <div className={styles.name}>
                    <div><GroupStds /><span title={item.name}>{item.name}</span></div>
                    <Whisper
                        placement={'autoHorizontalStart'}
                        container={() => document.getElementById("container-group")}
                        trigger="click"
                        speaker={
                            <Popover arrow={false} className={styles.popoverContainer}>
                                <div className={styles.popoverContent}>
                                    <div className={styles.itemPopover} 
                                        onClick={(event)=>handleEdit(item,event)}>
                                        <EditIcon /><span>{t('common')['edit']}</span>
                                    </div>
                                    <div className={styles.itemPopover} 
                                        onClick={(event)=>handleDisBand(item,event)}>
                                        <DisbandIcon /><span>{t('groups')['disband-group']}</span>
                                    </div>
                                </div>
                            </Popover>
                        }
                    >
                        <div className={styles.actionBtn} onClick={(e)=>{
                            e.preventDefault()
                            e.stopPropagation()
                        }}>
                            <GoKebabVertical color="#28303F" />
                        </div>
                    </Whisper>
                </div>
                <div className={styles.count}>
                    <span>{t('groups')['student-quantity']}</span>
                    <span>{item.totalStudent}</span>
                </div>
              </div>
            )
        })}</div>
    )
}

type modalDisBand = {
    isOpen?: boolean
    onCloseModal?: () => void
    onSubmitModal?: (value?:any) => void
    dataGroup?:any
    isLoading?:boolean
}
const ModalDisBand = ({ isOpen = false, onCloseModal, onSubmitModal, dataGroup = {}, isLoading = false }: modalDisBand) => {
    const { t } = useTranslation();
    return (
      <ModalWrapper
        className={styles.modalDisDand}
        isOpen={isOpen}
        onCloseModal={onCloseModal}
      >
        <div className={styles.container}>
            <span className={styles.title}>{t(t('modal-disband-group')['title'],[dataGroup?.name])}</span>
            <div className={styles.content}>
                <span className={styles.des}>{t("modal-disband-group")["when-confirm"]}</span>
                <ul>
                    <li>{t("modal-disband-group")["warning-msg-1"]}</li>
                    <li>{t("modal-disband-group")["warning-msg-2"]}</li>
                </ul>
                <span>{t("modal-disband-group")["confirm-msg"]}</span>
            </div>
            <div className={styles.action}>
                <ButtonCustom 
                    className={styles.cancel} 
                    disable={isLoading} 
                    onClick={onCloseModal}
                >{t('common')['cancel']}</ButtonCustom>
                <ButtonCustom 
                    onClick={()=>onSubmitModal(dataGroup)} 
                    isLoading={isLoading}
                >{t('common')['submit-btn']}</ButtonCustom>
            </div>
        </div>
      </ModalWrapper>
    )
}