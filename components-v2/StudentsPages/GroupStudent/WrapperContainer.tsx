import React, { useContext } from "react";

import classNames from "classnames";

import { ClassesContext } from "@/interfaces/contexts";

import styles from "./WrapperContainer.module.scss";
type PropWrap = {
    className?:string
    children:any
}
const WrapperContainer = ({ className = '', children }: PropWrap) => {
    return <div className={classNames(styles.groupStudentContainer,className)}>{children}</div>
};

export default WrapperContainer;