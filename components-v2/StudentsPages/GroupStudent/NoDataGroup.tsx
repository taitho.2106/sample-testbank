import React from "react";

import ButtonCustom from "@/componentsV2/Dashboard/SideBar/ButtonCustom/Button";

import styles from "./NoDataGroup.module.scss";

type NoData = {
    handleCreateGroup?:() => void
}
const NoData = ({handleCreateGroup}:NoData) => {
    return (
        <div className={styles.container}>
            <div className={styles.content}>
                <img src="/images/collections/clt-emty-result.png" alt="" />
                <span className={styles.title}>Chưa có nhóm được tạo</span>
                <span className={styles.des}>Lớp này chưa được chia nhóm</span>
            </div>
            <div className={styles.action}>
                <ButtonCustom onClick={handleCreateGroup}>Tạo nhóm học tập</ButtonCustom>
            </div>
        </div>
    )
}
export default NoData