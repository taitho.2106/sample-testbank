import React, { useEffect, useState } from 'react'

import { useRouter } from 'next/router';

import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button';
import NoFieldData from '@/componentsV2/NoDataSection/NoFieldData';
import { paths } from '@/interfaces/constants';
import { delayResult } from '@/utils/index';
import { callApiConfigError } from 'api/utils';

import styles from './GroupStudentContainer.module.scss';
import ListGroups from './ListGroups';
import WrapperContainer from "./WrapperContainer";
import CreateGroupWrapper from "@/componentsV2/StudentsPages/GroupStudent/CreateGroupWrapper";
import { Loader } from "rsuite";
import useTranslation from "@/hooks/useTranslation";

export default function GroupStudentContainer() {
    const { t } = useTranslation();
    const { query, push, asPath, isReady } = useRouter()
    const [isLoad, setIsLoad] = useState(0)
    const [dataGroup, setDataGroup] = useState([])

    const typeChoose = ['create', 'edit', 'view']
    const isChoonse = typeChoose.includes(query?.type?.toString())
    const fetcher = async () => {
        try {   
                setIsLoad(prev => prev + 1)
                const response1 : any = await callApiConfigError(`/api/classes/${query.classeSlug}/groups`)
                setDataGroup(response1?.data)
                setIsLoad(prev => Math.max(prev - 1, 0))
        } catch (error) {
            push('/classes')
            setIsLoad(prev => Math.max(prev - 1, 0))
        }
    }

    useEffect(() => {
        if(isReady && !query.type){
            delayResult(fetcher(),400).finally()
        }
    }, [query])

    const handleAddNewGroup = () => {
        push({
            pathname: paths.classeSlug,
            query: {
                ...query,
                type: 'create',
            },
        })
    }
    if(!isLoad && !query.type && dataGroup.length === 0){
        return (
            <WrapperContainer className={styles.noData}>                
                <NoFieldData field='group_student' handleCreate={handleAddNewGroup}/>
            </WrapperContainer>
        )
    }

    if(!isLoad && !query.type && dataGroup.length !== 0){
        return (
            <WrapperContainer className={styles.hasData}>
                <div className={styles.action}>
                    <ButtonCustom onClick={handleAddNewGroup}>{t('groups')['create-group']}</ButtonCustom>
                </div>
                <div className={styles.wrapper} id='container-group'>
                    <ListGroups className={styles.container} data={dataGroup} isReLoad={fetcher}/>
                </div>
            </WrapperContainer>
        )
    }

    if (!isLoad && isChoonse) {
        return (
            <WrapperContainer className={styles.createGroup}>
                <CreateGroupWrapper
                    isLoadPage={fetcher}
                />
            </WrapperContainer>
      )
    }

    return (
        <WrapperContainer className={styles.loading}>
            <Loader backdrop content="loading..." vertical />
        </WrapperContainer>
    )
}