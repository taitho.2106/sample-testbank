import React, { useContext, useEffect, useRef, useState } from 'react'

import classNames from 'classnames';
import { FormikContext, useFormik } from 'formik';
import { useRouter } from 'next/router';
import * as yup from 'yup'

import ArrowLeftBtn from "@/assets/icons/arrow-left.svg"
import ResetIcon from '@/assets/icons/reset-icon.svg'
import SearchIcon from '@/assets/icons/search.svg'
import BaseForm from '@/componentsV2/Common/Controls/BaseForm';
import InputTextField from '@/componentsV2/Common/Form/InputTextField';
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button';
import useNoti from '@/hooks/useNoti';
import { AppContext } from '@/interfaces/contexts';
import { callApiConfigError } from 'api/utils';
import { StickyFooter } from 'components/organisms/stickyFooter';

import DragTransferStudent from '../common/DragTransferStudent';
import styles from './CreateGroup.module.scss';
import { delayResult } from "@/utils/index";
import { removeAccents } from "../../../practiceTest/utils/functions";
import useTranslation from "@/hooks/useTranslation";

type CreateGroup = {
    className?:string
    isReLoad?:() => void
    dataStudents?:any[]
    dataInGroup?: any
    onLoad?: () => void
}
export default function CreateGroup({ className = '', isReLoad, dataStudents = [], dataInGroup = {}}:CreateGroup) {
    const { t } = useTranslation();
    const { getNoti } = useNoti()
    const { fullScreenLoading } = useContext(AppContext)
    const { push, query, isReady } = useRouter()
    const isCreate = query?.type === 'create' 
    const isView = query?.type ==='view'
    const idGroup = dataInGroup?.id || 0
    const nameGroup = dataInGroup?.name || '' 
    const [idSelect, setIdSelect] = useState([])
    const [idUnSelect, setIdUnSelect] = useState([])
    const initData = [...dataStudents]
    const initDataSelected:any[] = dataInGroup?.students || []
    const [oldStudentsGroup, setOldStudentsGroup] = useState(initData)
    const [newStudentsGroup, setNewStudentsGroup] = useState(initDataSelected)
    // const originStdGroup = useRef(initDataSelected)
    const originStdGroup = useRef([])
    const [isDrag, setIsDrag] = useState(false)
    useEffect(() => {
        if(isReady){
            const temp = initData.filter((item: any) => !item.class_group_id)
            setOldStudentsGroup(temp)
            const tempArr = initData.filter((item: any) => item.class_group_id === idGroup)
            setNewStudentsGroup(tempArr)
            originStdGroup.current = tempArr
        }
    }, [])
    const onSubmit = async (values: any) => {
        //
    }
    const formikFormStudentFilter = useFormik({
        initialValues: {
            search: '',
        },
        onSubmit: onSubmit,
    })
    const handleOnChange = () => {
        //
    }

    useEffect(() => {
        const delay = 300;
        const timerId = setTimeout(() => {
            const keyWord = removeAccents(formikFormStudentFilter.values.search?.toLowerCase() || '')
            setOldStudentsGroup(prevState => {
                const data = [...dataStudents].filter(item => !item.class_group_id)
                return data
                    ?.filter(item =>
                        removeAccents(item?.full_name?.toLowerCase() || '')?.includes(keyWord) ||
                        removeAccents(item?.phone?.toLowerCase() || '')?.includes(keyWord))
            })
        }, delay);

        return () => {
            clearTimeout(timerId);
        };
    } , [formikFormStudentFilter.values.search])
    const onSubmitAdd = async (values: any) => {
        if(isView){
            push({
              pathname: `/classes/${query.classeSlug}`,
              query: {
                ...query,
                type:'edit'
              },
            })
        }else{
            const listIdStdAdd = newStudentsGroup.filter(item => !originStdGroup.current?.some(m => m.id === item.id)).map(item => item.id)
            const removeStd = originStdGroup.current.filter(std => !newStudentsGroup?.some(m => m.id === std.id)).map(item => item.id)
            const body = {
              name: values.groupName,
              addStudents: listIdStdAdd,
              removeStudents: isCreate ? undefined : removeStd,
            }
            fullScreenLoading.setState(true)
            const response: any = await delayResult(callApiConfigError(
                isCreate ?
                    `/api/classes/${query.classeSlug}/group` :
                    `/api/classes/${query.classeSlug}/group/${idGroup}`,
                isCreate ? 'post' : 'put',
                'token',
                body,
            ),400)
            fullScreenLoading.setState(false)
            if(response.code === 200){
                if(isReLoad){
                    isReLoad()
                }
                getNoti('success','topCenter',`${isCreate ? t('groups')['create-group-success'] : t('groups')['update-group-success']}`)
                push({
                  pathname: `/classes/${query.classeSlug}`,
                  query: {
                    step: 2,
                  },
                })
            }else{
                getNoti('error', 'topCenter', response?.message)
            }
        }
    }
    const groupNameSchema = yup.object().shape({
        groupName: yup.string()
                        .required(t('groups')['validation-required-name-group'])
                        .max(225,t('groups')['validation-max-length-name-group'])
    })
    
    const formikFormStudentAdd = useFormik({
        initialValues: {
            groupName: nameGroup,
        },
        onSubmit: onSubmitAdd,
        validationSchema: groupNameSchema
    })

    const handleReset = () => formikFormStudentFilter.resetForm()

    const handleSelectItem = (value?:string|number,checked?:boolean) => {
        const tempArr = [...idSelect]
        if(checked){
            tempArr.push(value)
            setIdSelect(tempArr)
        }else{
            const temp = idSelect.filter((id) => id !== value)
            setIdSelect(temp)
        }
        
    }
    const handleSelectTransfer = () => {
        const stdSelected = oldStudentsGroup.filter((std) =>
          idSelect.includes(std.id),
        )
        const stdUnSelected = oldStudentsGroup.filter(
          (std) => !idSelect.includes(std.id),
        )
        setNewStudentsGroup((prev) => [...stdSelected, ...prev])
        setOldStudentsGroup(stdUnSelected)
        setIdSelect([])
    }

    const handleUnSelectItem = (value?:string|number,checked?:boolean) => {
        const tempArr = [...idUnSelect]
        if(checked){
            tempArr.push(value)
            setIdUnSelect(tempArr)
        }else{
            const temp = idUnSelect.filter((id) => id !== value)
            setIdUnSelect(temp)
        }
    }
    const handleUnSelectTransfer = () => {
        const stdUnSelected = newStudentsGroup.filter((std) =>
          !idUnSelect.includes(std.id),
        )
        const stdSelected = newStudentsGroup.filter(
          (std) => idUnSelect.includes(std.id),
        )
        setNewStudentsGroup(stdUnSelected)
        setOldStudentsGroup((prev) => [...stdSelected, ...prev])
        setIdUnSelect([])
    }
    const handleClickOld = (value:any,checked:boolean) => {
        const idSelected = oldStudentsGroup.map(item => item.id)
        setIdSelect(checked ? idSelected : [])
    }
    const handleClickNew = (value:any,checked:boolean) => {
        const idSelected = newStudentsGroup.map(item => item.id)
        setIdUnSelect(checked ? idSelected : [])
    }
    const handleDropOld = (event:any) => {
        const item = event.dataTransfer.getData('text/plain')
        const idOldStd = oldStudentsGroup.filter(std => std?.id)
        if(idOldStd.includes(item)){
            return
        }
        const indexStd = newStudentsGroup.findIndex((std) => std?.id === item)
        if(indexStd !== -1){
            setNewStudentsGroup((prevItems) => {
              const newItems = [...prevItems]
              newItems.splice(indexStd, 1)
              return newItems
            })
    
            setOldStudentsGroup((prevItems) => [newStudentsGroup[indexStd] ,...prevItems])
            setIdSelect([])
            setIdUnSelect([])
        }
    }
    const handleDropNew = (event:any) => {
        const item = event.dataTransfer.getData('text/plain')
        const idNewStd = newStudentsGroup.filter(std => std?.id)
        if (idNewStd.includes(item)) {
          return
        }
        const indexStd = oldStudentsGroup.findIndex((std) => std?.id === item)
        if(indexStd !== -1){
            setOldStudentsGroup((prevItems) => {
              const newItems = [...prevItems]
              newItems.splice(indexStd, 1)
              return newItems
            })
    
            setNewStudentsGroup((prevItems) => [oldStudentsGroup[indexStd], ...prevItems])
            setIdSelect([])
            setIdUnSelect([])
        }
    }
    return (
        <div className={classNames(styles.createGroupContainer,className)}>
            <div className={styles.baseContent}>
                <div className={styles.boxData}>
                    <FormikContext.Provider value={formikFormStudentFilter}>
                        <BaseForm className={styles.boxFilterAction}>
                            <InputTextField
                                name="search"
                                autoFocus={undefined}
                                className={styles.aInputWithLabel}
                                type={'text'}
                                label=""
                                maxLength={100}
                                onChange={handleOnChange}
                                disabled={false}
                                hideErrorMessage={undefined}
                                iconLeft={<SearchIcon />}
                                iconRight={undefined}
                                placeholder={t('student-management')['placeholder-search']}
                                autoComplete="off"
                                key="search"
                                onlyNumber={false}
                            />
                            <ButtonCustom
                                onClick={handleReset}
                                iconRight={<ResetIcon />}
                                className={classNames(styles.aButtonReset, {
                                [styles.disable]:
                                    formikFormStudentFilter.values.search?.length === 0,
                                })}
                            >
                                {t('common')['reset']}
                            </ButtonCustom>
                        </BaseForm>
                    </FormikContext.Provider>
                    <div className={styles.boxTableData}>
                        <DragTransferStudent
                            keyName='data-left'
                            data={oldStudentsGroup} 
                            messageNodata={t('chosen-students')['empty']}
                            onClickItem={handleSelectItem}
                            onClickAllItem={handleClickOld}
                            value={idSelect}
                            handleChange={(value)=>setIdSelect(value)}
                            onDrop={handleDropOld}
                            isDrag={isDrag}
                            setIsDrag={setIsDrag}
                            disabled={isView}
                        />
                    </div>
                </div>
                <div className={styles.boxAction}>
                    <ButtonCustom
                        className={classNames(styles.btnSelected, {
                            [styles.disable]: idSelect.length === 0,
                        })}
                        onClick={handleSelectTransfer}
                        iconRight={<ArrowLeftBtn />}
                        >
                        {t('chosen-students')['selected']}
                    </ButtonCustom>
                    <ButtonCustom
                        className={classNames(styles.btnUnselected, {
                            [styles.disable]: idUnSelect.length === 0,
                        })}
                        onClick={handleUnSelectTransfer}
                        iconLeft={<ArrowLeftBtn />}
                        >
                        {t('chosen-students')['un-selected']}
                    </ButtonCustom>
                </div>
                <div className={styles.boxData}>
                    <FormikContext.Provider value={formikFormStudentAdd}>
                        <BaseForm className={styles.boxFilterAction} id='groupName'>
                            <InputTextField
                                name="groupName"
                                autoFocus={undefined}
                                className={styles.aInputWithLabel}
                                type={'text'}
                                label=""
                                maxLength={100}
                                onChange={handleOnChange}
                                disabled={isView}
                                hideErrorMessage={undefined}
                                iconLeft={undefined}
                                iconRight={undefined}
                                placeholder={t('groups')['group-name']}
                                autoComplete="off"
                                key="groupName"
                                onlyNumber={false}
                            />
                        </BaseForm>
                    </FormikContext.Provider>
                    <div className={styles.boxTableData}>
                        <DragTransferStudent
                            keyName='data-right'
                            data={newStudentsGroup} 
                            messageNodata={t('groups')['empty-learner-selected']}
                            onClickItem={handleUnSelectItem}
                            onClickAllItem={handleClickNew}
                            value={idUnSelect}
                            handleChange={(value)=>setIdUnSelect(value)}
                            onDrop={handleDropNew}
                            isDrag={isDrag}
                            setIsDrag={setIsDrag}
                            disabled={isView}
                        />
                    </div>
                </div>
            </div>
            <StickyFooter className={styles.baseSticky}>
                <div className={styles.action}>
                    <ButtonCustom className={styles.cancel} 
                    onClick={() =>push({
                        pathname: `/classes/${query.classeSlug}`,
                        query: {
                            step: 2,
                        },
                    })}>{t('common')['cancel']}</ButtonCustom>
                    <ButtonCustom 
                        buttonType='submit'
                        form='groupName'
                        // className={classNames({
                        //     [styles.disable]: newStudentsGroup.length === 0 && isCreate
                        // })}
                        >{isCreate ? t('groups')['create-new-group'] : isView ? t('common')['edit'] : t('common')['update']}</ButtonCustom>
                </div>
            </StickyFooter>
        </div>
    )
}
