import React, { useContext, useEffect, useState } from "react";

import classNames from "classnames";
import dayjs from "dayjs";
import { FormikContext, useFormik } from "formik";
import Link from "next/link";
import { useRouter } from "next/router";
import qs from "qs";
import { Input, Loader, Tooltip, Whisper } from "rsuite";

import IconV from "@/assets/icons/ic-check.svg";
import CirclePlusIcon from "@/assets/icons/ic-circle-plus.svg";
import DeleteIcon from "@/assets/icons/ic-del-std.svg";
import EditIcon from "@/assets/icons/ic-edit-std.svg";
import ImportIcon from "@/assets/icons/ic-import.svg";
import IconX from "@/assets/icons/ic-x.svg";
import ResetIcon from "@/assets/icons/reset-icon.svg";
import SearchIcon from "@/assets/icons/search.svg";
import DropDownAction, { DataMenuAction } from "@/componentsV2/StudentsPages/common/DropDownAction";
import DropDownLink from "@/componentsV2/StudentsPages/common/DropDownLink";
import { ClassError } from "@/constants/errorCodes";
import { DATE_DISPLAY_FORMAT, LIMIT_CLASS_STUDENT_PACKAGE_BASIC } from "@/constants/index";
import useNoti from "@/hooks/useNoti";
import { PACKAGES, paths } from "@/interfaces/constants";
import { ClassesContext, WrapperContext } from "@/interfaces/contexts";
import { delayResult } from "@/utils/index";
import { mapMessageError } from "@/utils/message";
import { callApiConfigError } from "api/utils";

import { validStudentCode } from "../../validations";
import BaseForm from "../Common/Controls/BaseForm";
import InputTextField from "../Common/Form/InputTextField";
import ButtonCustom from "../Dashboard/SideBar/ButtonCustom/Button";
import NoFieldData from "../NoDataSection/NoFieldData";
import AlertMessage from "./AlertMessage";
import ImportNewStudents from "./ImportNewStudents";
import styles from "./StudentsList.module.scss";
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import useTranslation from "@/hooks/useTranslation";

type DataFetch = {
    isLoading?: boolean,
    isShow?: boolean
    message?: string
    type?: "success" | "warning" | "error" | "info",
    shouldUpdateCountStudent?: boolean
}

export default function StudentsList() {
    const { t } = useTranslation();
    const { dataPlan } = useCurrentUserPackage();
    const { globalModal, dataImportError, countRowData } = useContext(WrapperContext)
    const { dataClasses } = useContext(ClassesContext)
    const { getNoti } = useNoti()
    const { query, isReady } = useRouter()
    const [isLoadPage, setIsLoadPage] = useState(false)
    const [isLoadTable, setIsLoadTable] = useState(true)
    const [dataTable, setDataTable] = useState([])
    const [totalStudents, setTotalStudents] = useState(0)
    const [searchValue, setSearchValue] = useState('')
    const [isSearch, setIsSearch] = useState(false)
    const showErr = dataImportError.state?.length > 0;
    const formikFormStudent = useFormik({
        initialValues: {
            search: '',
        },
        onSubmit: values => {
            //
        },
    })
    const fetchData = async ({
                                 isLoading = true,
                                 isShow = false,
                                 message = '',
                                 type,
                                 shouldUpdateCountStudent = false
                             }: DataFetch) => {
        const objQuery = {
            search: searchValue
        }
        const queryString = qs.stringify(objQuery)
        setIsLoadTable(isLoading)
        const response: any = await delayResult(callApiConfigError(`/api/classes/${query.classeSlug}/students?${queryString}`), 400)
        setIsLoadTable(false)
        if (response.result && response.code === 200) {
            setDataTable(response.data)
            if (shouldUpdateCountStudent && dataPlan?.code === PACKAGES.BASIC.CODE) {
                setTotalStudents(response.data.length)
            }
            if (isShow) {
                getNoti(type || 'success', 'topCenter', message)
            }
        }
    }

    useEffect(() => {
        if(isReady){
            setIsLoadPage(true)
            fetchData({isLoading:false, shouldUpdateCountStudent: true}).then(() => setIsLoadPage(false)).finally()
        }
    }, [dataPlan.code])

    useEffect(() => {
        const delay = 300; // Set the debounce delay (e.g., 300 milliseconds)
        const timerId = setTimeout(() => {
            setSearchValue(formikFormStudent.values.search);
        }, delay);

        return () => {
            clearTimeout(timerId);
        };
    }, [
        formikFormStudent.values.search
    ])

    useEffect(() => {
        fetchData({}).finally()
    }, [searchValue])

    const handleReset = () => {
        formikFormStudent.resetForm()
    }

    const onClickDeleteItem = (item: any) => {
        globalModal.setState({
            id: 'delete-student-to-classes',
            type: 'delete-student-into-class',
            content: {
                nameStudent: item.full_name,
                class: dataClasses?.name,
                onSubmit: () => handleDelItem(item),
            },
        })
    }
    const handleDelItem = async (item:any)=> {
      const response: any = await callApiConfigError(
        `/api/classes/${query.classeSlug}/student/${item.id}`,
        'DELETE',
      )
      if(response.result){
        await fetchData({shouldUpdateCountStudent: true})
        globalModal.setState(null)
        getNoti('success','topCenter', t('common-get-noti')['delete-student-from-class'])
      }else{
        getNoti('success', 'topCenter', t('common-get-noti')['error'])
      }
    }
    const handleAfterUpload = () => {
        fetchData({
            isLoading: false,
            isShow: true,
            message: t('import-new-students')['upload-succeed-msg'],
            shouldUpdateCountStudent: true,
        })
    }

    const handleClickShowErr = () => {
        globalModal.setState({
            id: 'import-fail',
            content: {
                data: dataImportError.state,
                countRow: countRowData.state,
                isClassImport: true,
            },
        })
    }
    const handleHideErr = () => {
      dataImportError.setState([])
    }
    const handleChange = () => {
        setIsSearch(true)
    }

    const dataActionOrther:DataMenuAction[] = [
        {
            label: "Xuất DS học viên",
            icon: <CirclePlusIcon />,
            onClick: () => {
                console.log("Xuất DS học viên");
            }
        },
        {
            label: "Cập nhật số báo danh nhanh",
            icon: <ImportIcon />,
            onClick: () => {
                console.log("Cập nhật số báo danh nhanh");
            }
        }
    ]


    if (!isLoadPage && (dataTable.length || isSearch)){
        return (
          <div className={classNames(styles.studentsList,styles.hasData)}>
            {showErr && (
              <AlertMessage
                onClose={handleHideErr}
                onShow={handleClickShowErr}
              />
            )}
            <div className={styles.boxActionBtn}>
              <FormikContext.Provider value={formikFormStudent}>
                <BaseForm className={styles.boxFilterAction}>
                  <InputTextField
                    name="search"
                    autoFocus={undefined}
                    className={styles.aInputWithLabel}
                    type={'text'}
                    label=""
                    maxLength={100}
                    onChange={handleChange}
                    disabled={false}
                    hideErrorMessage={undefined}
                    iconLeft={<SearchIcon />}
                    iconRight={undefined}
                    placeholder={t('classes-page')['search-placeholder-classes']}
                    autoComplete="off"
                    key={'search'}
                    onlyNumber={false}
                    title={undefined}
                  />
                  <ButtonCustom
                    onClick={handleReset}
                    iconRight={<ResetIcon />}
                    className={classNames(styles.aButtonReset, {
                      [styles.disable]:
                        formikFormStudent.values.search?.length === 0,
                    })}
                  >
                      {t('common')['reset']}
                  </ButtonCustom>
                </BaseForm>
              </FormikContext.Provider>
                {!isLoadPage && dataTable && totalStudents < LIMIT_CLASS_STUDENT_PACKAGE_BASIC &&
					<div className={styles.btn}>
                        {/* <ButtonCustom type="outline">
                          Cập nhật số báo danh nhanh
                        </ButtonCustom> */}
                        {/* <ButtonCustom type="outline">Xuất DS học viên</ButtonCustom> */}
                        {/* <DropDownAction type="outline" title="Thao tác khác" dataMenu={dataActionOrther} /> */}
						<DropDownLink type="outline" query={query} onClick={handleHideErr} title={t('student-management')['add-student']}/>
					</div>}
            </div>
            <div className={styles.boxTableData}>
              <table>
                <thead>
                  <tr>
                    <th>{t('common')['student-code']}</th>
                    <th>{t('phone')}</th>
                    <th align="left">{t('common')['full-name']}</th>
                    <th align="left">Email</th>
                    <th>{t('common')['birthday']}</th>
                    <th>{t('common')['gender']}</th>
                    <th></th>
                  </tr>
                </thead>
                {!isLoadTable && dataTable.length > 0 && (
                  <tbody>
                    {dataTable.map((item) => {
                      return (
                        <tr key={item.id}>
                          <td>
                              <InputChangeStudentCode
                                  item={item}
                                  setDataTable={setDataTable}
                              />
                          </td>
                          <td title={item.phone ? item.phone : ''}>
                            {item.phone || '---'}
                          </td>
                          <td title={item.full_name ? item.full_name : ''}>
                            <Link href={`${paths.studentInfo}?id=${item.id}`}>
                              <a>{item.full_name || '---'}</a>
                            </Link>
                          </td>
                          <td title={item.email ? item.email : ''}>
                            {item.email || '---'}
                          </td>
                          <td title={item.birthday ? dayjs(item.birthday).format(DATE_DISPLAY_FORMAT) : ''}>
                            {item.birthday
                              ? dayjs(item.birthday).format(DATE_DISPLAY_FORMAT)
                              : '---'}
                          </td>
                          <td>
                            {item.gender
                              ? item.gender === 1
                                ? t('gender')['male']
                                : t('gender')['female']
                              : '---'}
                          </td>
                          <td>
                            <DeleteIcon
                              onClick={() => onClickDeleteItem(item)}
                            />
                          </td>
                        </tr>
                      )
                    })}
                  </tbody>
                )}
              </table>
                {!isLoadTable && !dataTable.length && (    
                    <div className={styles.noData}>
                        <NoFieldData 
                            field='students_list'
                            hasAction={false}
                            isSearchEmpty={true}
                        />
                    </div>               
                )}
                {isLoadTable && (
                    <div className={styles.noData}>
                        <Loader backdrop content="loading..." vertical/>
                    </div>
                )}
            </div>
          </div>
        )
    }

    if(!isLoadPage && (dataClasses && !isSearch)) {
      return (
        <div className={classNames(styles.studentsList, styles.noData)}>
          <div className={styles.btnAction}>
            <DropDownLink query={query} showImport={false} onClick={handleHideErr} title={t('student-management')['add-student']}/>
          </div>
          {showErr && !dataTable.length && (
            <AlertMessage onClose={handleHideErr} onShow={handleClickShowErr} />
          )}
          <ImportNewStudents isShowBack={false} callData={handleAfterUpload} />
        </div>
      )
    }

    return <Loader backdrop content="loading..." vertical />
}

const InputChangeStudentCode = ({ item, setDataTable }: any) => {
  const {t} = useTranslation()
    const { query } = useRouter()
    const { getNoti } = useNoti()
    const [valueInput, setValueInput] = useState(item?.student_code)
    const [errorStudentCode, setErrorStudentCode] = useState('')
    const [isEdit, setIsEdit] = useState(false)

    const onClickChangeStudentCode = async () => {
        try {
            await validStudentCode(t).validate({ studentCode: valueInput });
            if(valueInput === item?.student_code) {
                onCancel()
            } else {
                const response: any = await callApiConfigError(`/api/classes/${query.classeSlug}/student/${item.id}`, "PUT", "token", {
                    studentCode: valueInput
                });

                if(response.code === 200) {
                    setIsEdit(false)
                    setErrorStudentCode('')
                    getNoti('success', 'topCenter', t('common-get-noti')['update-student-code'])
                    setDataTable((prevState :any[]) => {
                        const dataArr = [...prevState]
                        const indexItem = prevState.findIndex((m :any) => m?.id === item.id)
                        dataArr[indexItem].student_code = valueInput
                        return dataArr
                    })
                }else if (ClassError.STUDENT_EXISTED_CODE === response.code) {
                    setIsEdit(true)
                    setErrorStudentCode(mapMessageError({t})[response.code])
                } else {
                    setIsEdit(true)
                    setErrorStudentCode(response?.errors[0]?.message?.toString())
                }
            }
        } catch (e: any) {
            console.log({ e });
            setErrorStudentCode(e.message)
        }
    }

    const tooltip = (
        <Tooltip>{errorStudentCode}</Tooltip>
    )
    const onChangeInput = (value:string) => {
        setValueInput(value)
    }

    const onCancel = () => {
        setIsEdit(false)
        setErrorStudentCode('')
        setValueInput(item?.student_code || '')
    }
    return (
        <>
            { isEdit ? (
                <div className={styles.boxEditor}>
                    {!!errorStudentCode.length ?
                        <Whisper
                            trigger="hover"
                            placement="topStart"
                            speaker={tooltip}
                        >
                            <Input
                                className={classNames(styles.editorInput, {
                                    [styles.error]: !!errorStudentCode.length
                                })}
                                value={valueInput}
                                onChange={onChangeInput}
                                type="text"
                            />
                        </Whisper>
                        :
                        (
                            <Input
                                className={styles.editorInput}
                                value={valueInput}
                                onChange={onChangeInput}
                                type="text"
                            />
                        )}
                    <div className={styles.actionStudentCode}>
                        <IconV onClick={onClickChangeStudentCode} />
                        <IconX onClick={onCancel} />
                    </div>
                </div>
            ) : (
                <div className={styles.boxContent}>
                    <EditIcon
                        onClick={() => setIsEdit(true)}
                    />
                    {` ${valueInput || "---"}`}
                </div>
            )}
        </>
    )
}
