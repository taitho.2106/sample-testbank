import React, { useEffect, useRef, useState } from "react";

import classNames from "classnames";
import { FormikContext, useFormik } from "formik";
import { useRouter } from "next/router";
import { Button, Loader } from "rsuite";

import ArrowLeftBtn from "@/assets/icons/arrow-left.svg";
import ChevronIcon from "@/assets/icons/arrow.svg";
import ResetIcon from "@/assets/icons/reset-icon.svg";
import SearchIcon from "@/assets/icons/search.svg";
import DragTransferStudent from "@/componentsV2/StudentsPages/common/DragTransferStudent";
import useNoti from "@/hooks/useNoti";
import { paths } from "@/interfaces/constants";
import { delayResult } from "@/utils/index";
import { callApiConfigError } from "api/utils";

import BaseForm from "../Common/Controls/BaseForm";
import InputTextField from "../Common/Form/InputTextField";
import ButtonCustom from "../Dashboard/SideBar/ButtonCustom/Button";
import styles from "./ChoosenStudents.module.scss";
import { removeAccents } from "../../practiceTest/utils/functions";
import { ClassError } from "@/constants/errorCodes";
import { LIMIT_CLASS_STUDENT_PACKAGE_BASIC } from "@/constants/index";
import useTranslation from "@/hooks/useTranslation";

export default function ChoosenStudents() {
    const { t } = useTranslation();
    const { getNoti } = useNoti()
    const { back, query, push, isReady } = useRouter();
    const dataPackup = useRef([])
    const [dataLeft, setDataLeft] = useState([]);
    const [dataRight, setDataRight] = useState([]);
    const [idStdTransfer, setIdStdTransfer] = useState([]);
    const [idStdUnTransfer, setIdStdUnTransfer] = useState([]);
    const [isLoad, setIsLoad] = useState(true);
    const [isDrag, setIsDrag] = useState(false);
    const fetchData = async ({ isLoading = true }: any) => {
        try {
            setIsLoad(isLoading);
            const response: any = await delayResult(callApiConfigError(`/api/classes/${query?.idClass}/student-add-available`), 400);
            setIsLoad(false);
            if (response.code === 200) {
                setDataLeft([...response?.data]);
                dataPackup.current = [...response?.data]
            }
        } catch (e) {
            push({
                pathname: `${paths.classes}/${query?.idClass}`
            }).finally()
        }
    };
    useEffect(() => {
        if (isReady) {
            fetchData({}).finally();
        }
    }, [isReady]);
    const onSubmit = async (values: any) => {
        console.log({ values });
    };
    const handleReset = () => formikForm.handleReset({});

    const formikForm = useFormik({
        initialValues: {
            search: ""
        },
        onSubmit: onSubmit
    });
    useEffect(() => {
        const delay = 300;
        const timerId = setTimeout(() => {
            const keyWord = removeAccents(formikForm.values.search?.toLowerCase() || '')
            setDataLeft(prevState => {
                const data = [...dataPackup.current]
                return data
                    ?.filter(item =>
                        removeAccents(item?.full_name?.toLowerCase() || '')?.includes(keyWord) ||
                        removeAccents(item?.phone?.toLowerCase() || '')?.includes(keyWord))
            })
        }, delay);

        return () => {
            clearTimeout(timerId);
        };
    } , [formikForm.values.search])
    const handleAddAll = async () => {
        const idStudentJoining = dataRight.map(item => item.id)
        const response:any = await callApiConfigError(`/api/classes/${query?.idClass}/student/update`,
            "put", "token", { addStudents: idStudentJoining })

        if (response.code === 200) {
            getNoti("success", "topCenter", t('add-student-noti')['add-to-class-succeed']);
            push({
                pathname: `/classes/${query.idClass}`,
                query: {
                    class: query.class,
                    startYear: query.startYear,
                    endYear: query.endYear
                }
            }).finally();
        } else {
            if (response.code === ClassError.IMPORT_STUDENT_REACH_LIMIT) {
                getNoti("error", "topCenter", t(t('add-student-noti')['error-limit'], [`${LIMIT_CLASS_STUDENT_PACKAGE_BASIC}`]));
            } else {
                getNoti("error", "topCenter", response?.message);
            }
        }
    };
    const handleSelectItem = (value?: string | number, checked?: boolean) => {
        const tempArr = [...idStdTransfer];
        if (checked) {
            tempArr.push(value);
            setIdStdTransfer(tempArr);
        } else {
            const temp = idStdTransfer.filter((id) => id !== value);
            setIdStdTransfer(temp);
        }

    };
    const handleClickLeftAll = (value: any, checked: boolean) => {
        const itemSelected = dataLeft.map(item => item.id);
        setIdStdTransfer(checked ? itemSelected : []);
    };
    const handleDropLeft = (event: any) => {
        const item = event.dataTransfer.getData("text/plain");
        const idStdLeft = dataLeft.filter(std => std?.id);
        if (idStdLeft.includes(item)) {
            return;
        }
        const indexStd = dataRight.findIndex((std) => std?.id === item);
        if (indexStd !== -1) {
            setDataRight((prevItems) => {
                const newItems = [...prevItems];
                newItems.splice(indexStd, 1);
                return newItems;
            });

            setDataLeft((prevItems) => [dataRight[indexStd], ...prevItems]);
            setIdStdTransfer([]);
            setIdStdUnTransfer([]);
        }
    };
    const handleTransferLeft = () => {
        const stdSelected = dataLeft.filter((std) =>
            idStdTransfer.includes(std.id)
        );
        const stdUnSelected = dataLeft.filter(
            (std) => !idStdTransfer.includes(std.id)
        );
        setDataRight((prev) => [...stdSelected, ...prev]);
        setDataLeft(stdUnSelected);
        setIdStdTransfer([]);
    };

    const handleUnSelectItem = (value?: string | number, checked?: boolean) => {
        const tempArr = [...idStdUnTransfer];
        if (checked) {
            tempArr.push(value);
            setIdStdUnTransfer(tempArr);
        } else {
            const temp = idStdUnTransfer.filter((id) => id !== value);
            setIdStdUnTransfer(temp);
        }
    };
    const handleClickRightAll = (value: any, checked: boolean) => {
        const idSelected = dataRight.map(item => item.id);
        setIdStdUnTransfer(checked ? idSelected : []);
    };
    const handleTransferRight = () => {
        const stdUnSelected = dataRight.filter((std) =>
            !idStdUnTransfer.includes(std.id)
        );
        const stdSelected = dataRight.filter(
            (std) => idStdUnTransfer.includes(std.id)
        );
        setDataRight(stdUnSelected);
        setDataLeft((prev) => [...stdSelected, ...prev]);
        setIdStdUnTransfer([]);
    };
    const handleDropRight = (event: any) => {
        const item = event.dataTransfer.getData("text/plain");
        const idStdRight = dataRight.filter(std => std?.id);
        if (idStdRight.includes(item)) {
            return;
        }
        const indexStd = dataLeft.findIndex((std) => std?.id === item);
        if (indexStd !== -1) {
            setDataLeft((prevItems) => {
                const newItems = [...prevItems];
                newItems.splice(indexStd, 1);
                return newItems;
            });

            setDataRight((prevItems) => [dataLeft[indexStd], ...prevItems]);
            setIdStdTransfer([]);
            setIdStdUnTransfer([]);
        }
    };

    return (
        <div className={styles.choosenWrapper}>
            <div className={styles.layoutWrapper}>
                <div className={styles.layoutContent}>
                    <div className={styles.headerBack} onClick={() => back()}>
                        <div className={styles.iconBack}>
                            <ChevronIcon />
                        </div>
                        <span className={styles.textBack}>{t('common')['back']}</span>
                    </div>
                    <div className={styles.boxActionBtn}>
                        <FormikContext.Provider value={formikForm}>
                            <BaseForm className={styles.boxFilterAction}>
                                <InputTextField
                                    name="search"
                                    autoFocus={false}
                                    className={styles.aInputWithLabel}
                                    placeholder={t('common')['placeholder-search-student']}
                                    autoComplete="off"
                                    onlyNumber={false}
                                    type="text"
                                    disabled={false}
                                    hideErrorMessage={false}
                                    iconLeft={<SearchIcon />}
                                    iconRight={undefined}
                                    label={""}
                                    key={"search"}
                                    onChange={undefined}
                                />
                                <ButtonCustom
                                    onClick={handleReset}
                                    iconRight={<ResetIcon />}
                                    className={classNames(styles.aButtonReset, {
                                        [styles.disable]: formikForm.values.search.length === 0
                                    })}
                                >
                                    {t('common')['reset']}
                                </ButtonCustom>
                            </BaseForm>
                        </FormikContext.Provider>
                    </div>
                    {isLoad ? (
                            <div className={styles.boxActionStudent}>
                                <Loader vertical center backdrop content="Loading..." />
                            </div>
                        ) :
                        (<div className={styles.boxActionStudent}>
                            <div className={styles.left}>
                                <DragTransferStudent
                                    data={dataLeft}
                                    keyName="data-left"
                                    messageNodata={t('chosen-students')['empty']}
                                    onClickItem={handleSelectItem}
                                    onClickAllItem={handleClickLeftAll}
                                    value={idStdTransfer}
                                    handleChange={(value) => setIdStdTransfer(value)}
                                    onDrop={handleDropLeft}
                                    isDrag={isDrag}
                                    setIsDrag={setIsDrag}
                                />
                            </div>
                            <div className={styles.boxActionBtn}>
                                <ButtonCustom
                                    className={classNames(styles.btnSelected, {
                                        [styles.disable]: idStdTransfer.length === 0
                                    })}
                                    onClick={handleTransferLeft}
                                    iconRight={<ArrowLeftBtn />}
                                >
                                    {t('chosen-students')['selected']}
                                </ButtonCustom>
                                <ButtonCustom
                                    className={classNames(styles.btnUnselected, {
                                        [styles.disable]: idStdUnTransfer.length === 0
                                    })}
                                    onClick={handleTransferRight}
                                    iconLeft={<ArrowLeftBtn />}
                                >
                                    {t('chosen-students')['un-selected']}
                                </ButtonCustom>
                            </div>
                            <DragTransferStudent
                                data={dataRight}
                                keyName="data-right"
                                messageNodata={t('chosen-students')['empty']}
                                onClickItem={handleUnSelectItem}
                                onClickAllItem={handleClickRightAll}
                                value={idStdUnTransfer}
                                handleChange={(value) => setIdStdUnTransfer(value)}
                                onDrop={handleDropRight}
                                isDrag={isDrag}
                                setIsDrag={setIsDrag}
                            />
                        </div>)}
                </div>
                {!isLoad && <div className={styles.layoutFooter}>
					<Button
						className={styles.cancel}
						appearance="default"
						onClick={() => back()}
					>
                        {t('common')['cancel']}
					</Button>
					<Button className={styles.btnSubmit} disabled={!dataRight.length} onClick={handleAddAll}>{t('chosen-students')['add-classes']}</Button>
				</div>}
            </div>
        </div>
    );
}

