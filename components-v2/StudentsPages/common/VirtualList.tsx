import React, { useState } from "react";

import classNames from "classnames";
import List from "rc-virtual-list";
import { Checkbox } from "rsuite";

import DagIcon from '@/assets/icons/dots-six-vertical.svg'

import styles from "./DragTransferStudent.module.scss";

type Props = {
    value?: any[],
    data?: any[],
    onClickItem?: (value?: any, checked?:boolean) => void,
    targetItem?: any,
    disabled?: boolean,
    onDragEnter?: (e:any) => void,
    handleDragLeave?: (e:any) => void,
    handleDragStart?: (e:any, id:any) => void
    height?:number
}
const VirtualList = ({
         value,
         data,
         onClickItem,
         targetItem,
         disabled,
         onDragEnter,
         handleDragLeave,
         handleDragStart,
         height
    }: Props) => {
    return (
        <>
            <List data={data} itemKey="id" itemHeight={10} height={height}>
                {(item, index, props): any => {
                    return (
                        <div
                            key={item.id}
                            className={classNames(styles.tableRow, {
                                [styles.dragItem]: targetItem === item.id,
                                [styles.disabled]: disabled
                            })}
                            onDragEnter={onDragEnter}
                            onDragLeave={handleDragLeave}
                            onDragStart={(e: any) => handleDragStart(e, item.id)}
                            draggable={!disabled}
                        >
                            <div className={styles.action}>
                                <DagIcon />
                                <Checkbox
                                    disabled={disabled}
                                    checked={value.some((m: any) => m === item.id)}
                                    onChange={(_, check) => onClickItem(item.id, check)}
                                    value={item.id} />
                            </div>
                            <div className={styles.lable}>
                                <span>{item.full_name}</span>
                                <span>{item.phone}</span>
                            </div>
                        </div>
                    );
                }
                }
            </List>
        </>
    );
};

export default VirtualList;