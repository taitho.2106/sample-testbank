import React from "react";

import classNames from "classnames";
import { Dropdown } from "rsuite";

import { DataMenu, DropDownLink } from "@/componentsV2/StudentsPages/common/DropDownLink";

import styles from "./DropDownLink.module.scss";

export interface DataMenuAction extends DataMenu {
    onClick?: () => void
}
export interface DropDownAction extends DropDownLink {
    dataMenu: DataMenuAction[]
}
const DropDownAction = ({
    className = "",
    type = "primary",
    title = "Thêm học viên",
    dataMenu = [],
}: DropDownAction
) => {

    return (
        <Dropdown
            className={classNames(styles.dropDown, styles[type], className)}
            placement="bottomEnd"
            title={title}
        >
            {dataMenu.map((item, index: number) => {
                return (
                    <Dropdown.Item
                        icon={item.icon}
                        key={index}
                        onSelect={item.onClick}
                    >
                        {item.label}
                    </Dropdown.Item>
                );
            })}

        </Dropdown>
    );
};

export  default DropDownAction