import React, { useEffect, useMemo, useRef, useState } from "react";

import classNames from 'classnames'
import { Checkbox, CheckboxGroup } from 'rsuite'

import DagIcon from '@/assets/icons/dots-six-vertical.svg'

import styles from './DragTransferStudent.module.scss'
import VirtualList from "@/componentsV2/StudentsPages/common/VirtualList";
import { useWindowSize } from "@/utils/hook";
import useTranslation from "@/hooks/useTranslation";

type DragTransferStudent = {
    data?:any
    className?:string
    messageNodata?:string
    onClickAllItem?: (value?:string|number,checked?:boolean) => void
    onClickItem?: (value?:string|number,checked?:boolean) => void
    handleChange?: (value?:any[]) => void
    value?:any[]
    handleDrag?:(e:any,val:string|number) => void
    onDrop?:(e:any) => void
    isDrag?:boolean
    setIsDrag?:any
    keyName?:string
    disabled?:boolean
    onScroll?: (event:any) => void
}
export default function DragTransferStudent({
    data = [],
    className = '',
    messageNodata = '',
    onClickAllItem,
    onClickItem,
    value = [],
    handleChange,
    onDrop,
    isDrag = false,
    setIsDrag,
    keyName='',
    disabled = false,
    onScroll
}: DragTransferStudent) {
    const { t } = useTranslation();
  const [targetItem, setTargetItem] = useState('')
    const refBody = useRef<HTMLDivElement>(null)
    const [ w,h ] = useWindowSize()
  const onDragEnter = (event: any) => {
    event.preventDefault()
    event.stopPropagation()
    setIsDrag(true)
  }
  const handleDragStart = (event:any,value:string|number) => {
    setTargetItem(value?.toString())
    setIsDrag(true)
    event.dataTransfer.setData('text/plain', value)
  }
  const handleDragLeave = () => {
    setIsDrag(false)
    setTargetItem('')
  }
  const onDragOver = (event:any) => {
    event.preventDefault()
    setIsDrag(true)
  }
  const onDropBox = (event: any) => {
    event.preventDefault()
    if(disabled) return false
    setTargetItem('')
    setIsDrag(false)
    if(onDrop) onDrop(event)
  }
    const height  = useMemo(() => {
        return refBody.current?.offsetHeight || 100
    }, [w, h]);
  return (
    <div className={classNames(styles.tableData, className)}>
      <div className={styles.tableHeader}>
        <div className={styles.action}>{data.length > 0 && 
        <Checkbox 
          disabled={disabled}
          indeterminate={value.length > 0 && value.length < data.length} 
          onChange={onClickAllItem} 
          checked={value.length === data.length}/>}
        </div>
        <div className={styles.lable}>
          <span>{t('common')['student-name']}</span>
        </div>
      </div>
      <div className={classNames(styles.tableBody,{
          [styles.box]:isDrag
        })}
           ref={refBody}
        onScroll={onScroll}
        onDragOver={onDragOver} onDrop={onDropBox}>
          {data.length > 0 ?
              <CheckboxGroup name="checkboxList" value={value} onChange={handleChange}>
                  <VirtualList
                      value={value}
                      data={data}
                      onClickItem={onClickItem}
                      targetItem={targetItem}
                      disabled={disabled}
                      onDragEnter={onDragEnter}
                      handleDragLeave={handleDragLeave}
                      handleDragStart={handleDragStart}
                      height={height}
                  />
              </CheckboxGroup>
              : (
                  <div className={styles.noData}>{messageNodata}</div>
              )
          }
      </div>
    </div>
  )
}
