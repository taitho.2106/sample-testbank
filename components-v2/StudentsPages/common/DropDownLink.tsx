import React from "react";

import classNames from "classnames";
import Link from "next/link";
import { Dropdown } from "rsuite";

import CirclePlusIcon from "@/assets/icons/ic-circle-plus.svg";
import ImportIcon from "@/assets/icons/ic-import.svg";
import ListsBullets from "@/assets/icons/lists-bullets.svg";

import styles from "./DropDownLink.module.scss";
import { paths } from "@/interfaces/constants";
import useTranslation from "@/hooks/useTranslation";

export interface DropDownLink  {
    className?:string
    title?:string
    type?: "outline" | "primary"
    dataMenu?:DataMenu[]
    query?: any
    showImport?: boolean
    onClick?: () => void
}

export interface DataMenu  {
    label?: string,
    icon?: any,
    linkUrl?: any
}

const DropDownLink = ({
      className = "",
      type = "primary",
      title = "Thêm học viên",
      dataMenu = [],
      query,
      showImport = true,
      onClick
    }: DropDownLink
) => {
    const { t } = useTranslation();
    const defaultData: DataMenu[] = [
        {
            label: t('student-management')['add-individual'],
            icon: <CirclePlusIcon />,
            linkUrl: {
                pathname: paths.studentAddClass,
                query: {
                    idClass: query.classeSlug
                }
            }
        },
        {
            label: t('student-management')['import-list'],
            icon: <ImportIcon />,
            linkUrl: {
                pathname: paths.studentAddClass,
                query: {
                    idClass: query.classeSlug,
                    type: "import"
                }
            }
        },
        {
            label: t('student-management')['choose-from-list'],
            icon: <ListsBullets />,
            linkUrl: {
                pathname: paths.studentChosen,
                query: {
                    idClass: query.classeSlug,
                },
            }
        }
    ];
    const data = !!dataMenu.length ? dataMenu : defaultData
    return (
        <Dropdown
            className={classNames(styles.dropDown, styles[type], className)}
            placement="bottomEnd"
            title={title}
            onSelect={onClick}
        >
            {data.filter(item => {
                if(!showImport) return item.linkUrl?.query?.type !== "import"
                return item
            }).map((item, index: number) => {
                return (
                    <Dropdown.Item
                        icon={item.icon}
                        as={MyLink}
                        href={item.linkUrl}
                        key={index}
                    >
                        {item.label}
                    </Dropdown.Item>
                );
            })}

        </Dropdown>
    );
};
export const MyLink = React.forwardRef((props: any, ref: any) => {
    const { href, as, ...rest } = props
    return (
        <Link href={href} as={as}>
            <a ref={ref} {...rest} />
        </Link>
    )
})
MyLink.displayName = ''

export default DropDownLink