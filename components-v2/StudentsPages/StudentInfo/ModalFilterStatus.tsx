import React, { useCallback, useEffect, useState } from 'react'

import Modal from 'react-modal'

import FilterSection from '@/componentsV2/Dashboard/FilterSidebar/FilterSection'
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button'
import { CODE_ASSIGN_OPTION } from "@/interfaces/constants";

import CloseIcon from "../../../assets/icons/close.svg"
import styles from './ModalFilterStatus.module.scss'
import useTranslation from "@/hooks/useTranslation";

type Props = {
    isOpen?: boolean
    setIsOpen?: React.Dispatch<React.SetStateAction<boolean>>
    onSubmit?: (value?: any) => void
    filterTestStatus?: any[]
    onReset?: any
}

Modal.setAppElement('#__next');

const ModalFilterStatus = ({
    isOpen,
    setIsOpen,
    onSubmit,
    filterTestStatus,
    onReset,
}: Props) => {
    const { t } = useTranslation();
    const [appliedFilter, setAppliedFilter] = useState(filterTestStatus)

    const handleResetAll = useCallback(() => {
        setIsOpen(false)
        onReset()
    }, [
        setIsOpen,
        onReset
    ])

    const handleSubmit = useCallback(() => {
        setIsOpen(false)
        onSubmit(appliedFilter)
    }, [
        setIsOpen,
        onSubmit,
        appliedFilter
    ])

    useEffect(() => {
        if (isOpen) {
            setAppliedFilter(filterTestStatus);
        }
    }, [
        isOpen,
        filterTestStatus
    ])

    return (
        <Modal
            isOpen={isOpen}
            onRequestClose={() => setIsOpen(false)}
            overlayClassName={styles.overlay}
            className={styles.drawer}
            closeTimeoutMS={300}
            shouldCloseOnOverlayClick={false}
        >
            <div className={styles.filterContainer}>
                <div className={styles.filterBox}>
                    <div className={styles.header}>
                        <span>{t('student-info')['filter-title']}</span>
                        <div className={styles.actionSection}>
                            <div
                                className={styles.reset}
                                onClick={handleResetAll}
                            >
                                {t('common')['reset']}
                            </div>
                            <ButtonCustom type="none" onClick={() => setIsOpen(false)}>
                                <CloseIcon />
                            </ButtonCustom>
                        </div>
                    </div>
                    <div className={styles.body}>
                        <FilterSection
                            options={CODE_ASSIGN_OPTION}
                            groupName={t('student-info')['status']}
                            filterSelected={appliedFilter}
                            setFilterSelected={setAppliedFilter}
                            keyName="status"
                        />
                    </div>
                    <div className={styles.footer}>
                        <ButtonCustom
                            className={styles.applyBtn}
                            onClick={handleSubmit}
                        >
                            {t('common')['apply']}
                        </ButtonCustom>
                    </div>
                </div>
            </div>
        </Modal>
    )
}

export default ModalFilterStatus