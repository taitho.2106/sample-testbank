
import React, { useEffect, useMemo, useRef, useState } from 'react'

import classNames from 'classnames'
import Link from "next/link";
import { useRouter } from 'next/router'
import qs from "qs";

import ExtendIcon from '@/assets/icons/extend.svg'
import ClassIcon from '@/assets/icons/ic-classes.svg'
import CustomCarousel from '@/componentsV2/Common/CustomCarousel'
import { DateTimePickerInput } from '@/componentsV2/Common/DateTimePickerCustom'
import { InputWithLabel } from '@/componentsV2/Common/InputWithLabel/InputWithLabel'
import SkeletonLoading from '@/componentsV2/Common/SkeletonLoading'
import { CODE_ASSIGN_OPTION, STATUS_CODE_EXAM, paths as pathsUrl } from "@/interfaces/constants";
import { getDisplayTimeOut } from "@/utils/date";
import { cleanObject, delayResult } from '@/utils/index'
import { paths } from 'api/paths';
import { callApi, callApiConfigError } from 'api/utils'

import ChevronIcon from '../../../assets/icons/arrow.svg'
import ModalFilterStatus from './ModalFilterStatus'
import styles from './StudentInfo.module.scss'
import useTranslation from "@/hooks/useTranslation";

type INFO = {
    id: string,
    user_name: string,
    full_name: string,
    email: string,
    address: string,
    avatar: string,
    created_date: string,
    phone: string,
    birthday: string,
    gender: number,
}

const GENDER = [
    { display: 'male', code: 1 },
    { display: 'female', code: 2 },
]

const StudentInfo = () => {
    const { t } = useTranslation();
    const { push, query } = useRouter()
    const [studentInfo, setStudentInfo] = useState<INFO | null>(null)
    const [classesInfo, setClassedInfo] = useState([])
    const [assignedInfo, setAssignedInfo] = useState([])
    const [loadingInfo, setLoadingInfo] = useState(false)
    const [loadingClass, setLoadingClass] = useState(false)
    const [loadingAssign, setLoadingAssign] = useState(true)

    const showBtn = 2
    const carouselRef = useRef(null)
    const [currentClass, setCurrentClass] = useState(0)
    const [currentClassID, setCurrentClassID] = useState(0)

    const [openFilter, setOpenFilter] = useState(false)
    const [filterTestStatus, setFilterTestStatus] = useState([])

    const studentID = query.id

    const mapStatusToClass = {
        [STATUS_CODE_EXAM.UPCOMING]: styles.upcoming,
        [STATUS_CODE_EXAM.ONGOING]: styles.ongoing,
        [STATUS_CODE_EXAM.ENDED]: styles.ended,
    }

    useEffect(() => {
        if (classesInfo.length > 0 && query.classId) {
            const initClassId = Number(query.classId)
            const indexQueryClass = classesInfo.findIndex((obj) => obj.id === initClassId)
            setCurrentClass(indexQueryClass)
            scrollToClass(indexQueryClass)
        }
    }, [
        query.classId,
        classesInfo
    ])

    useEffect(() => {
        if (query.examStatus) {
            const initStatus: string[] = Array.isArray(query.examStatus) ? query.examStatus : [query.examStatus]
            const statusNumbers: number[] = initStatus.map((item: string) => Number(item))
            setFilterTestStatus(statusNumbers)
        }
    }, [query.examStatus])

    const fetchData = async () => {
        try {
            setLoadingInfo(true)
            setLoadingClass(true)
            const studentResponse: any = await delayResult(
                callApi(`${paths.api_teacher_student}/${studentID}`), 400
            )
            setLoadingInfo(false)
            setLoadingClass(false)
            if (studentResponse.result && studentResponse.data) {
                setStudentInfo(studentResponse.data.student)
                setClassedInfo(studentResponse.data.classes)
            }
        } catch (error) {
            console.error('Error fetching data:', error)
        }
    }

    useEffect(() => {
        fetchData()
    }, [])

    useEffect(() => {
        if (classesInfo.length > 0 && !query.classId) {
            //set first class data
            setCurrentClassID(classesInfo[0].id)
        }
        else if (classesInfo.length === 0) {
            const timeoutId = setTimeout(() => {
                setLoadingAssign(false)
            }, 400)

            return () => clearTimeout(timeoutId)
        }
    }, [
        classesInfo,
    ])

    const objQuery = useMemo(() => {
        const obj = cleanObject({
            id: studentID,
            classId: query?.classId,
            examStatus: query?.examStatus,
        })
        if (classesInfo.length > 0 && !query.classId) {
            obj.classId = classesInfo[0].id.toString()
        }
        return obj
    }, [query, classesInfo])

    const fetcherAssignedExamData = async () => {
        try {
            const queryString = qs.stringify(
                objQuery,
                { arrayFormat: 'repeat' }
            )
            const response: any = await delayResult(
                callApiConfigError(`${paths.api_teacher_student}/${studentID}/unit-test-assigned?${queryString}
                `), 400)
            setLoadingAssign(false)
            if (response?.result && response?.data) {
                const result = response.data
                setAssignedInfo(result)
            }
        }
        catch (error: any) {
            console.error('Error fetching assign exam:', error)
        }
    }

    useEffect(() => {
        if (objQuery.classId) {
            fetcherAssignedExamData().finally()
        }
    }, [objQuery])

    const scrollToClass = (index: number) => {
        if (carouselRef.current) {
            carouselRef.current.goToSlide(index);
        }
    };

    const handleClassChange = async (classIdValue: number, index: number) => {
        if (classIdValue === currentClassID) { // return if the selected class is already the current class 
            return
        }

        setLoadingAssign(true)
        setCurrentClass(index)
        setCurrentClassID(classIdValue)
        push({
            pathname: `${pathsUrl.studentInfo}`,
            query: {
                ...objQuery,
                classId: classIdValue
            }
        }).finally();
    }

    const handleOpenFilter = () => {
        setOpenFilter(!openFilter)
    }

    const submitFilter = (statusValue: any[]) => {
        setLoadingAssign(true)
        setAssignedInfo([])
        setFilterTestStatus(statusValue)
        push({
            pathname: `${pathsUrl.studentInfo}`,
            query: {
                ...objQuery,
                examStatus: statusValue,
            }
        })
    }

    const resetFilterState = () => {
        setFilterTestStatus([])
        setAssignedInfo([])
        setLoadingAssign(true)
        push({
            pathname: `${pathsUrl.studentInfo}`,
            query: {
                ...objQuery,
                examStatus: [],
            }
        }).finally()
    }

    return (
        <>
            <ModalFilterStatus
                isOpen={openFilter}
                setIsOpen={setOpenFilter}
                onSubmit={submitFilter}
                filterTestStatus={filterTestStatus}
                onReset={resetFilterState}
            />
            <div className={styles.container}>
                <div className={styles.leftSection}>
                    <div className={styles.headContent}>
                        <div className={styles.avatarContent}>
                            <div className={styles.avatar}>
                                <img src='/images-v2/sample-avatar.png' alt='' />
                            </div>
                        </div>
                    </div>
                    {loadingInfo && (
                        <div className={styles.bodyContent}>
                            {Array.from(Array(5), (val, index: number) => {
                                return (
                                    <SkeletonLoading className={styles.inputField} key={index}>
                                        <div
                                            className={`${styles.boxSkeleton} ${styles.boxInputSkeleton}`}
                                        ></div>
                                    </SkeletonLoading>
                                )
                            })}
                        </div>
                    )}
                    {studentInfo && !loadingInfo && (
                        <div className={styles.bodyContent}>
                            <InputWithLabel
                                className={styles.inputField}
                                label={t('common')['full-name']}
                                type="text"
                                defaultValue={studentInfo.full_name}
                                disabled={true}
                            />
                            <InputWithLabel
                                className={classNames(styles.inputField, {
                                    [styles.disabled]: !studentInfo.phone,
                                })}
                                label={t('phone')}
                                type="text"
                                defaultValue={studentInfo.phone}
                                disabled={true}
                            />
                            <InputWithLabel
                                className={classNames(styles.inputField, {
                                    [styles.disabled]: !studentInfo.email,
                                })}
                                label="Email"
                                type="text"
                                defaultValue={studentInfo.email}
                                disabled={true}
                            />
                            <DateTimePickerInput
                                name="birthday"
                                className={classNames(styles.timeField, {
                                    [styles.disabled]: !studentInfo.birthday
                                })}
                                label={t('common')['birthday']}
                                time={true}
                                defaultValue={studentInfo.birthday ? studentInfo.birthday : undefined}
                                disabled={true}
                                onChange={undefined}
                                onlyDate={true}
                            />
                            <InputWithLabel
                                className={classNames(styles.inputField, {
                                    [styles.disabled]: !studentInfo.gender,
                                })}
                                label={t('common')['gender']}
                                type="text"
                                defaultValue={
                                    studentInfo.gender ? t('gender')[GENDER.filter(
                                        (item) => item.code == Number(studentInfo.gender),
                                    )[0].display] : undefined}
                                disabled={true}
                            />
                        </div>
                    )}
                </div>
                <div className={styles.rightSection}>
                    <div className={styles.header}>
                        <span className={styles.title}>{t('student-info')['right-header']}</span>
                        {classesInfo.length != 0 && (
                            <div
                                className={styles.btnFilter}
                                onClick={handleOpenFilter}
                            >
                                <ExtendIcon />
                            </div>
                        )}
                    </div>
                    {loadingClass && (
                        <div className={classNames(styles.boxCarouselTemp, styles.boxCarousel)}>
                            <button className={styles.leftArrow}>
                                <ChevronIcon />
                            </button>
                            <div className={styles.boxTemp}>
                                {Array.from(Array(2), (val, cIndex: number) => {
                                    return (
                                        <SkeletonLoading
                                            className={styles.classBtnSkeleton}
                                            key={cIndex}
                                        />
                                    )
                                })}
                            </div>
                            <button className={styles.rightArrow}>
                                <ChevronIcon />
                            </button>
                        </div>
                    )}
                    {!loadingClass && classesInfo.length == 0 && (
                        <div className={styles.emptyBody}>
                            <img
                                src='/images-v2/empty-class-result.png'
                                alt="/"
                            />
                            <span className={styles.description}>
                                {t('student-info')['not-inclued-in-class']}
                            </span>
                        </div>
                    )}
                    {!loadingClass && classesInfo.length > 2 && (
                        <div className={styles.boxCarousel}>
                            <CustomCarousel
                                dragging={null}
                                className={styles.carousel}
                                slideIndex={0}
                                carouselRef={carouselRef}
                                adaptiveHeight={true}
                                autoPlay={false}
                                cellSpacing={12}
                                disableEdgeSwiping={true}
                                slidesToScroll={1}
                                slidesToShow={showBtn}
                                withoutControls={false}
                                isInfinity={true}
                                swiping={false}
                                wrapAround={false}
                                renderTopLeftControls={null}
                                renderBottomCenterControls={null}
                                renderCenterLeftControls={({ previousDisabled, previousSlide, currentSlide }: any) => {
                                    const isFirstSlide = currentSlide === 0
                                    return (
                                        <button onClick={previousSlide} disabled={previousDisabled || isFirstSlide}>
                                            <ChevronIcon />
                                        </button>
                                    )
                                }}
                                renderCenterRightControls={({ nextDisabled, nextSlide, currentSlide }: any) => {
                                    const isLastSlide = currentSlide >= classesInfo.length - showBtn
                                    return (
                                        <button onClick={nextSlide} disabled={nextDisabled || isLastSlide}>
                                            <ChevronIcon />
                                        </button>
                                    )
                                }}
                            >
                                {classesInfo.map((item: any, index: number) => {
                                    console.log({item});
                                    return (
                                        <button
                                            key={item.id}
                                            title={item.name}
                                            className={classNames(styles.carouselItem, {
                                                [styles.active]: currentClass === index,
                                            })}
                                            onClick={() => handleClassChange(item.id, index)}
                                        >
                                            <span>{item.name}
                                            </span>
                                        </button>
                                    )
                                })}
                            </CustomCarousel>
                        </div>
                    )}
                    {!loadingClass && classesInfo.length < 3 && (
                        <div className={styles.classSection}>
                            {classesInfo.map((item, index) => {
                                return (
                                    <button
                                        key={item.id}
                                        className={classNames(styles.classBtn, {
                                            [styles.active]: currentClass === index
                                        })}
                                        title={item.name}
                                        onClick={() => handleClassChange(item.id, index)}
                                    >
                                        <span>{item.name}
                                        </span>
                                    </button>
                                )
                            })}
                        </div>
                    )}
                    {loadingAssign && (
                        <div className={styles.unitTestLadder}>
                            {Array.from(Array(3), (val, tIndex: number) => {
                                return (
                                    <SkeletonLoading className={styles.unitTestItem} key={tIndex}>
                                        <div
                                            className={`${styles.boxSkeleton} ${styles.unitTestSkeleton}`}
                                        ></div>
                                    </SkeletonLoading>
                                )
                            })}
                        </div>
                    )}
                    {!loadingAssign && classesInfo.length > 0 && assignedInfo?.length == 0 && (
                        <div className={styles.emptyExamResult}>
                            <img
                                src='/images-v2/empty-exam-result.png'
                                alt="/"
                            />
                            <span className={styles.description}>
                                {t('student-info')['no-unit-test']}
                            </span>
                        </div>
                    )}
                    {!loadingAssign && !loadingClass && assignedInfo.length > 0 && (
                        <div className={styles.unitTestLadder}>
                            <>
                                {assignedInfo.map((assignedTest: any, index: number) => {
                                    const statusClassName = mapStatusToClass[assignedTest.examStatus]
                                    const statusLabel = CODE_ASSIGN_OPTION.filter((item) => item.value == assignedTest.examStatus)[0].label
                                    return (
                                        <div
                                            key={assignedTest.id}
                                            className={styles.unitTestItem}
                                        >
                                            {assignedTest.examStatus !== STATUS_CODE_EXAM.UPCOMING &&
                                                <Link href={`${pathsUrl.learningResult}/${assignedTest.id}?studentId=${studentID}`}>
                                                    <a
                                                        className={classNames(styles.header, statusClassName)}
                                                        title={assignedTest.unitTest.name}
                                                    >
                                                        <span>{assignedTest.unitTest.name}</span>
                                                    </a>
                                                </Link>
                                            }
                                            {assignedTest.examStatus === STATUS_CODE_EXAM.UPCOMING && (
                                                <div
                                                    className={classNames(styles.header, statusClassName)}
                                                    title={assignedTest.unitTest.name}
                                                >
                                                    <span>{assignedTest.unitTest.name}</span>
                                                </div>
                                            )}
                                            <div className={styles.body}>
                                                <div className={styles.leftInfo}>
                                                    <div className={styles.classInfo}>
                                                        <ClassIcon />
                                                        <div className={styles.nameOfClass} title={classesInfo[currentClass]?.name}>
                                                            {classesInfo[currentClass]?.name}
                                                        </div>
                                                    </div>
                                                    <div className={classNames(styles.testStatus, statusClassName)}>
                                                        {t('status')[statusLabel]}
                                                    </div>
                                                </div>
                                                <div className={styles.rightInfo}>
                                                    <div className={styles.textNoti}>
                                                        {assignedTest.examStatus == STATUS_CODE_EXAM.UPCOMING ? t('student-info')['will-start-in'] :
                                                            assignedTest.examStatus == STATUS_CODE_EXAM.ONGOING ? t('student-info')['will-end-in'] : ''
                                                        }
                                                    </div>
                                                    <div className={styles.timeNoti}>{assignedTest.examStatus != STATUS_CODE_EXAM.ENDED ? getDisplayTimeOut(assignedTest.timeout, t) : ''}</div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </>
                        </div>
                    )}
                </div>
            </div>
        </>
    )
}

export default StudentInfo