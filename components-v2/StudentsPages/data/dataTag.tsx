import BookIcon from '@/assets/icons/book-open-text.svg';
import ChatIcon from '@/assets/icons/chat_plus_fill.svg';
import GroupStds from '@/assets/icons/ic-group-stds.svg';
import Magazine from '@/assets/icons/magazine.svg';
import UserAlt from '@/assets/icons/user_alt_fill.svg';

export const dataTag = [
    {
        code:1,
        groupName:'students-list',
        icon: <UserAlt/>
    },
    {
        code:2,
        groupName:'groups-list',
        icon: <GroupStds/>
    },
    {
        code:3,
        groupName:'assigned-unit-test',
        icon: <Magazine/>
    },
    {
        code:4,
        groupName:'references',
        icon: <BookIcon/>
    },
    {
        code:5,
        groupName:'join-class-request',
        icon: <ChatIcon/>
    },
]

export const dataTabStudentManagement = [
    {
        code: 1,
        groupName: 'student-management-tab-managing',
        icon: <UserAlt />,
    },
    {
        code: 2,
        groupName: 'student-management-tab-request',
        icon: <ChatIcon/>,
    },
]