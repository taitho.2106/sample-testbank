import React, { useContext, useEffect, useState } from 'react'

import classNames from 'classnames';
import { FormikContext, useFormik } from 'formik';
import { useRouter } from 'next/router';

import ChevronIcon from '@/assets/icons/arrow.svg'
import StudentIcon from '@/assets/icons/avt-default.svg'
import SubmitIcon from '@/assets/icons/icon-submit.svg'
import { ClassError } from '@/constants/errorCodes';
import useNoti from '@/hooks/useNoti';
import { paths, phoneRegExp1 } from "@/interfaces/constants";
import { AppContext } from '@/interfaces/contexts';
import { cleanObject, delayResult } from "@/utils/index";
import { mapMessageError } from '@/utils/message';
import { callApiConfigError } from 'api/utils'
import { addStudentSchema, validPhone } from 'validations'

import BaseForm from '../Common/Controls/BaseForm';
import CheckSelectPicker from '../Common/Form/CheckSelectPicker'
import DateTimePicker from '../Common/Form/DateTimePicker'
import InputTextField from '../Common/Form/InputTextField';
import SelectFields from '../Common/Form/SelectFields'
import ButtonCustom from '../Dashboard/SideBar/ButtonCustom/Button';
import styles from './AddNewStudent.module.scss';
import ImportNewStudents from './ImportNewStudents';
import * as yup from "yup";
import dayjs from "dayjs";
import { DATE_DISPLAY_FORMAT, LIMIT_CLASS_STUDENT_PACKAGE_BASIC } from "@/constants/index";
import useTranslation from "@/hooks/useTranslation";

export default function AddNewStudent() {
    const {query} = useRouter()

    const renderUIWithQuery = () => {
        switch (query.type) {
            case 'form':
                return <AddStudentWithForm />
            case 'import':
                return <ImportNewStudents />
            default:
                return <AddStudentWithPhone />
        }
    }
    return <div className={styles.addNewStudent}>{renderUIWithQuery()}</div>
}

const AddStudentWithPhone = () => {
    const { t } = useTranslation();
    const { back, push, query } = useRouter()
    const { fullScreenLoading } = useContext(AppContext)
    const { getNoti } = useNoti()
    const pathname = query.idClass ? paths.classeSlug : paths.studentSlug
    const querySlub = query.idClass
      ? { classeSlug:query.idClass}
      : { studentSlug: 'details' }
    const message = query.idClass
      ? t('add-student-noti')['add-to-class-succeed']
      : t('add-student-noti')['add-succeed']

    const messgaErr =  query.idClass
        ? t('add-student-noti')['error-msg']
        : t('add-student-noti')['error-msg-2']
    const onSubmit = async (values: any) => {
      const body = {
        phone: values.phone,
        classId: query?.idClass ? Number(query?.idClass) : undefined,
      }
      fullScreenLoading.setState(true)
      const response: any = await delayResult(callApiConfigError(
        '/api/teacher/students/add-student-by-phone',
        'POST',
        'token',
        body,
      ),400)
      fullScreenLoading.setState(false)
      if(response){
        switch (response.code) {
          case 200:
            push({ pathname, query: querySlub }).finally(() => {
              getNoti('success', 'topCenter', message)
            })
            break;
          case 400:
              getNoti("error", "topCenter", messgaErr);
            break;
          case ClassError.USER_NOT_STUDENT:
            getNoti(
              'error',
              'topCenter',
              mapMessageError({t})[ClassError.USER_NOT_STUDENT],
            )
            break;
          case ClassError.STUDENT_NOT_FOUND:
            query?.idClass
              ? push({
                  pathname: '/classes/add-new-student',
                  query: {
                    idClass: query.idClass,
                    phone: values.phone,
                    type: 'form',
                  },
                })
              : push({
                  pathname: '/students/add-new-student',
                  query: {
                    phone: values.phone,
                    type: 'form',
                  },
                })
            break;
          case ClassError.IMPORT_STUDENT_REACH_LIMIT:
            getNoti('error', 'topCenter', t(t('add-student-noti')['error-limit'], [`${LIMIT_CLASS_STUDENT_PACKAGE_BASIC}`]))
            break;
          default:
            getNoti('error', 'topCenter', response?.message)
            break;
        }
      }
    }
    const formikForm = useFormik({
      initialValues: {
        phone: '',
      },
      onSubmit: onSubmit,
      validationSchema: yup.object().shape({ phone: yup.string()
              .required(t(t('formikForm')['required'], [t('phone')]))
              .length(10, t(t('formikForm')['phone-length'], ['10']))
              .matches(phoneRegExp1, t('phone-validate'))
      }),
    })
    return (
      <div className={styles.addWrapper}>
        <div className={styles.headerBack} onClick={() => back()}>
          <div className={styles.iconBack}>
            <ChevronIcon />
          </div>
          <span className={styles.textBack}>{t('common')['back']}</span>
        </div>
        <div className={styles.contentAdd}>
          <div>
            <StudentIcon />
          </div>
          <span>{t('add-new-student')['required']} {query.idClass && t('add-new-student')['need-to-add']}</span>
          <FormikContext.Provider value={formikForm}>
            <BaseForm className={styles.addStudent}>
              <InputTextField
                name="phone"
                autoFocus={false}
                className={styles.inputField}
                placeholder={t('phone')}
                autoComplete="off"
                onlyNumber={true}
                type="tel"
                disabled={false}
                hideErrorMessage={false}
                iconLeft={undefined}
                iconRight={undefined}
                label={''}
                key="phone"
                onChange={undefined}
              />
              <button className={styles.iconAction} type="submit">
                <SubmitIcon />
              </button>
            </BaseForm>
          </FormikContext.Provider>
        </div>
      </div>
    )
}

const AddStudentWithForm = () => {
    const { t } = useTranslation();
    const { back, push, query } = useRouter()
    const { fullScreenLoading } = useContext(AppContext)
    const {getNoti} = useNoti()
    const pathname = query.idClass ? paths.classeSlug : paths.studentSlug
    const querySlug = query.idClass
      ? {
          classeSlug: query.idClass,
        }
      : {
        studentSlug: 'details'
      }
    const message = query.idClass
        ? t('add-student-noti')['add-to-class-succeed']
        : t('add-student-noti')['add-succeed']
    const [dataClasses, setDataClasses] = useState([])
    const fecthClasses = async () => {
      const response: any = await callApiConfigError(`/api/classes?size=200`)
      if(response.code === 200 && response.data){
          const tempData = response.data?.content.map((item: any) => {
            return {
              label: item.name.trim(),
              value: item.id,
            }
          })
          setDataClasses(tempData)
      }
    }
    useEffect(() => {
      if (!query.idClass) {
        fecthClasses()
      }
    }, [])
    const onSubmit = async (values: any) => {
        const body = cleanObject({
          classId: query.idClass ? Number(query.idClass) : values.classId,
          phone: values.phone,
          email: values.email?.length > 0 ? values.email : undefined,
          fullName: values.fullName,
          birthday: values.birthday.length > 0 ? dayjs(values.birthday).format(DATE_DISPLAY_FORMAT) : undefined,
          studentCode: values.studentCode.length > 0 ? values.studentCode : undefined,
          gender: values.gender,
        })
        fullScreenLoading.setState(true)
        const response: any = await delayResult(callApiConfigError(
          '/api/teacher/students/add-new-student',
          'POST',
          'token',
            body,
        ),400)
        fullScreenLoading.setState(false)
        if(response){
          switch (response?.code) {
            case 200:
              push({ pathname, query: querySlug }).finally(() =>
                getNoti('success', 'topCenter', message),
              )
              break
            case ClassError.STUDENT_EXISTED:
              formikFormStudent.setFieldError(
                'email',
                mapMessageError({t})[ClassError.STUDENT_EXISTED],
              )
              break;
            case ClassError.STUDENT_EXISTED_CODE:
              formikFormStudent.setFieldError(
                'studentCode',
                mapMessageError({t})[ClassError.STUDENT_EXISTED_CODE],
              )
              break;
            case ClassError.IMPORT_STUDENT_REACH_LIMIT:
              getNoti('error', 'topCenter', t(t('add-student-noti')['error-limit'], [`${LIMIT_CLASS_STUDENT_PACKAGE_BASIC}`]))
              break;
            default:
              getNoti('error','topCenter',response?.message)
              break;
          }
        }
    }
    const baseSchema = yup.object().shape({
        birthday: yup.string()
    })

    const validationSchema = baseSchema.concat(addStudentSchema(t).omit(['birthday']))
    const formikFormStudent = useFormik({
      initialValues: {
        phone: query?.phone,
        fullName: '',
        email: '',
        birthday: '',
        studentCode: '',
        gender: undefined,
        classId: [],
      },
      onSubmit: onSubmit,
      validationSchema: validationSchema,
    })
    return (
      <div className={styles.layoutWrapper}>
        <div className={styles.layoutContent}>
            <div>
                <StudentIcon />
            </div>
            <FormikContext.Provider value={formikFormStudent}>
                <BaseForm className={styles.addNewAStudent}
                    id="form-add-new-student"
                >
                <InputTextField
                    name="phone"
                    autoFocus={false}
                    className={classNames(styles.inputField,styles.disabled)}
                    placeholder={t('phone')}
                    autoComplete="off"
                    onlyNumber={true}
                    type="tel"
                    disabled={true}
                    hideErrorMessage={false}
                    iconLeft={undefined}
                    iconRight={undefined}
                    label={''}
                    key='phone'
                    onChange={undefined}
                />
                <InputTextField
                    name="fullName"
                    autoFocus={false}
                    className={styles.inputField}
                    placeholder={t('common')['full-name']}
                    autoComplete="off"
                    onlyNumber={false}
                    type="text"
                    disabled={false}
                    hideErrorMessage={false}
                    iconLeft={undefined}
                    iconRight={undefined}
                    label={''}
                    key={'fullName'}
                    onChange={undefined}
                />
                <InputTextField
                    name="email"
                    autoFocus={false}
                    className={styles.inputField}
                    placeholder="Email"
                    autoComplete="off"
                    onlyNumber={false}
                    type="text"
                    disabled={false}
                    hideErrorMessage={false}
                    iconLeft={undefined}
                    iconRight={undefined}
                    label={''}
                    key={'email'}
                    onChange={undefined}
                />
                <DateTimePicker
                    key='birthday'
                    name="birthday"
                    placement={'autoVertical'}
                    style={undefined}
                    onChange={undefined}
                    cleanable={true}
                    className={styles.inputField}
                    label={t('common')['birthday']}
                    isDisableDateCurrentToFuture={true}
                />
                {query.idClass ? <InputTextField
                    name="studentCode"
                    autoFocus={false}
                    className={styles.inputField}
                    placeholder={t('common')['student-code']}
                    autoComplete="off"
                    onlyNumber={false}
                    type="text"
                    disabled={false}
                    hideErrorMessage={false}
                    iconLeft={undefined}
                    iconRight={undefined}
                    label=''
                    key='studentCode'
                    onChange={undefined}
                  /> : <CheckSelectPicker
                      name='classId'
                      className={styles.inputField}
                      placement={'autoHorizontalEnd'}
                      data={dataClasses}
                      key='classId'
                      label={t('add-student-noti')['class-id']}
                      cleanable={false}
                    />}
                <SelectFields
                  name='gender'
                  className={styles.inputField}
                  data={[
                    { label: t('gender')['male'], value: 1 },
                    { label: t('gender')['female'], value: 2 },
                  ]}
                  key='gender'
                  label={t('common')['gender']}
                  onChange={undefined}
                />
                </BaseForm>
            </FormikContext.Provider>
        </div>
        <div className={styles.layoutFooter}>
          <ButtonCustom
            className={styles.cancel}
            type="outline"
            onClick={() => back()}
          >
              {t('common')['back']}
          </ButtonCustom>
          <ButtonCustom form='form-add-new-student' buttonType='submit'>{t('add-student-noti')['add-more']}</ButtonCustom>
        </div>
      </div>
    )
}
