import React, { useContext, useState } from 'react'

import { useRouter } from 'next/router'

import { paths } from '@/interfaces/constants'
import { WrapperContext } from '@/interfaces/contexts'

import Tabs from '../Common/Tabs'
import AssignExams from './AssignExams'
import { dataTag } from './data/dataTag'
import styles from './StudentsContainer.module.scss'
import StudentsList from './StudentsList'
import GroupStudentContainer from './GroupStudent/GroupStudentContainer'
import ComingSoonComponent from "@/componentsV2/Common/ComingSoonPage";

export default function StudentsContainer() {
    const { dataImportError } = useContext(WrapperContext)
    const {push, query} = useRouter()
    const indexCurrent = +query?.step || 1
    const [indexActive, setIndexActive] = useState(indexCurrent)
    const renderContentWithTag = () => {
        switch (indexActive) {
            case 1:
                return <><StudentsList /></>
            case 2:
                return <GroupStudentContainer />
            case 3:
                return <><AssignExams /></>
            default:
                return <><ComingSoonComponent /></>
        }
    }
    const onClickStep = (index:number) => {
        setIndexActive(index)
        dataImportError.setState([])
        push({
            pathname:paths.classeSlug,
            query:{
                classeSlug: query.classeSlug,
                step:index
            }
        })
    }
    return (
        <div className={styles.studentsContainer}>
            <Tabs 
                className={styles.boxTag}
                data={dataTag}
                step={indexActive}
                onClickStep={onClickStep}
            />
            <div className={styles.boxContent}>
                {renderContentWithTag()}
            </div>
        </div>
    )
}

