import React, { useState } from 'react'

import Tabs from '@/componentsV2/Common/Tabs';

import { dataTabStudentManagement } from '../data/dataTag';
import styles from './StudentContainer.module.scss';
import StudentManagement from './StudentManagement';
import ComingSoonComponent from "@/componentsV2/Common/ComingSoonPage";
import useTranslation from "@/hooks/useTranslation";

export default function StudentContainer() {
    const { t } = useTranslation();
    const [tabIndex, setTabIndex] = useState(1)
    const renderUiWithTab = () => {
        switch (tabIndex) {
            case 1:
                return <><StudentManagement /></>
        
            case 2:
                return <><ComingSoonComponent/></>
        }
    }
    return (
        <div className={styles.studentContainer}>
            <Tabs
                className={styles.tabHeader}
                data={dataTabStudentManagement}
                step={tabIndex}
                onClickStep={setTabIndex}
            />
            <div className={styles.content}>{renderUiWithTab()}</div>
        </div>
    )
}
