import React from 'react'

import { useRouter } from 'next/router'

import StudentContainer from '.'
import AddNewStudent from '../AddNewStudent'
import ImportNewStudents from '../ImportNewStudents'
import styles from './StudentSlugContainer.module.scss';
import StudentInfo from '../StudentInfo'

export default function StudentSlugContainer({query}:any) {
  const studentSlug = ['details', 'add-new-student', 'info']
  const {push} = useRouter()
  if (!studentSlug.includes(query?.studentSlug)){
    push('/_error')
    return <></>
  }
  if (query?.type === 'import') {
    return <Wrap><ImportNewStudents /></Wrap>
  }
  if (query?.studentSlug === 'details') {
    return <StudentContainer />
  }
  if (query?.studentSlug === 'info') {
    return <StudentInfo />
  }
  return <AddNewStudent />
}

const Wrap = ({children = <></>}:any) => {
  return <div className={styles.wrap}>{children}</div>
}