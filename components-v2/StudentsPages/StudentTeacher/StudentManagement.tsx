import React, { useContext, useEffect, useMemo, useRef, useState } from "react";

import classNames from "classnames";
import dayjs from "dayjs";
import { FormikContext, useFormik } from "formik";
import Link from "next/link";
import { useRouter } from "next/router";
import qs from "qs";
import { Loader, Pagination } from "rsuite";

import CirclePlusIcon from "@/assets/icons/ic-circle-plus.svg";
import DeleteIcon from "@/assets/icons/ic-del-std.svg";
import ImportIcon from "@/assets/icons/ic-import.svg";
import ResetIcon from "@/assets/icons/reset-icon.svg";
import SearchIcon from "@/assets/icons/search.svg";
import BaseForm from "@/componentsV2/Common/Controls/BaseForm";
import CheckSelectPicker from "@/componentsV2/Common/Form/CheckSelectPicker";
import InputTextField from "@/componentsV2/Common/Form/InputTextField";
import ButtonCustom from "@/componentsV2/Dashboard/SideBar/ButtonCustom/Button";
import NoFieldData from "@/componentsV2/NoDataSection/NoFieldData";
import DropDownLink, { DataMenu } from "@/componentsV2/StudentsPages/common/DropDownLink";
import { DATE_DISPLAY_FORMAT } from '@/constants/index';
import useNoti from "@/hooks/useNoti";
import { paths as path } from "@/interfaces/constants";
import { WrapperContext } from "@/interfaces/contexts";
import { cleanObject, delayResult } from "@/utils/index";
import { paths } from "api/paths";
import { callApiConfigError } from "api/utils";
import { StickyFooter } from "components/organisms/stickyFooter";

import AlertMessage from "../AlertMessage";
import styles from "./StudentManagement.module.scss";
import useTranslation from "@/hooks/useTranslation";

export default function StudentManagement() {
    const { t } = useTranslation();
    const { globalModal, dataImportError, countRowData } = useContext(WrapperContext)
    const { query, isReady, push } = useRouter()
    const { getNoti } = useNoti()
    const [isLoad, setIsLoad] = useState(true)
    const [dataTable, setDataTable] = useState([])
    const [dataClassOfTeacher, setDataClassOfTeacher] = useState([])
    const showErr = dataImportError.state?.length > 0
    const refTable = useRef(null)
    const page = Number(query?.page) || 1
    const limit = 10
    const [totalPages, setTotalPages] = useState(1)
    const [searching, setSearching] = useState(false)
    const [isFirstRender, setIsFirstRender] = useState(true)
    const formikFormStudent = useFormik({
        initialValues: {
            search: '',
            classes: [] as string[]
        },
        onSubmit: values => {
            //
        },
    })
    useEffect(() => {
        if (isReady) {
            formikFormStudent.setValues({
                search: query?.search?.toString() ||'',
                classes: query?.classIds?.toString()?.split(',') || []
            })
        }
    } , [])
    const objQuery = useMemo(()=>{
        return cleanObject({
            page: Math.max(page - 1, 0),
            size: limit,
            search: query?.search,
            classIds: query?.classIds,
        })
    }, [query?.page, query?.search, query?.classIds])
    const fetcher = async ({ isLoading = true }) => {
        setIsLoad(isLoading)
        scrollToTop()
        const queryString = qs.stringify(objQuery, { arrayFormat: 'repeat' })
        const response: any = await delayResult(callApiConfigError(`${paths.api_teacher_student}?${queryString}`), 400)
        setIsLoad(false)
        if (response.result && response.data) {
            setDataTable(response.data.content)
            setTotalPages(response.data.totalPage)
            setIsFirstRender(false)
        }
    }
    const fetcherClassOfTeacher = async () => {
        try {
            const response: any = await callApiConfigError(`${paths.api_teacher_classes}?size=100`)
            if (response.code === 200) {
                const dataTemp = response.data.content.map((item: any) => {
                    return {
                        value: item.id.toString(),
                        label: item.name
                    }
                })
                setDataClassOfTeacher(dataTemp)
            }
        } catch (error) {
            console.log({ error })
        }
    }
    useEffect(() => {
        if (isReady) {
            fetcherClassOfTeacher().finally()
        }
    }, [])

    useEffect(() => {
        if (formikFormStudent.values.search === '') {
            setIsFirstRender(true)
        }
        const delay = 300;
        const timerId = setTimeout(() => {
            push({
                pathname: path.studentDetail,
                query: {
                    ...objQuery,
                    page: 1,
                    search: formikFormStudent.values.search,
                    classIds: formikFormStudent.values.classes,
                }
            }).finally()
        }, delay);

        return () => {
            clearTimeout(timerId);
        };
    }, [
        formikFormStudent.values.search
    ])

    useEffect(() => {
        if(isReady){
            fetcher({}).finally()
        }
    }, [objQuery])

    const handleReset = () => {
        formikFormStudent.resetForm()
        push({
            pathname: path.studentDetail,
            query: {
                page: 1,
            }
        }).finally()
        setIsFirstRender(true)
    }

    const onClickDeleteItem = (item: any) => {
        globalModal.setState({
            id: 'delete-student-to-classes',
            type: 'delete-student-into-teacher',
            content: {
                nameStudent: item.full_name,
                onSubmit: () => handleDelItem(item),
            },
        })
    }
    const handleDelItem = async (item: any) => {
        setIsLoad(true)
        const response: any = await callApiConfigError(
            `/api/teacher/students/${item.id}`,
            'DELETE',
        )
        setIsLoad(false)
        globalModal.setState(null)
        if (response.result) {
            getNoti('success', 'topCenter', t('common-get-noti')['delete-student'])
            if (page === totalPages && dataTable.length === 1 && page !== 1) {
                push({
                    pathname: path.studentDetail,
                    query: {
                        ...objQuery,
                        page: Math.max(page - 1, 1),
                    },
                })
            } else {
                await fetcher({})
            }

        } else {
            getNoti('error', 'topCenter', t('common-get-noti')['error'])
        }

    }

    const scrollToTop = () => {
        if (refTable.current) {
            refTable.current.scrollTo({
                top: 0,
                behavior: 'smooth',
            })
        }
    }
    const handleChanePage = (page: number) => {
        push({
            pathname: path.studentDetail,
            query: {
                ...objQuery,
                page
            }
        })
    }
    const onChangeClasses = (value:any) => {
        push({
            pathname: path.studentDetail,
            query: {
                ...objQuery,
                page: 1,
                classIds: value,
            }
        })
    }
    const handleClickShowErr = () => {
        globalModal.setState({
            id: 'import-fail',
            content: {
                data: dataImportError.state,
                countRow: countRowData.state,
                isClassImport: false,
            },
        })
    }
    const handleHideErr = () => {
        dataImportError.setState([])
    }
    const dataDropDownLink:DataMenu[] = [
        {
            label: t('student-management')['add-individual'],
            icon: <CirclePlusIcon />,
            linkUrl: {
                pathname: '/students/add-new-student',
            }
        },
        {
            label: t('student-management')['import-list'],
            icon: <ImportIcon />,
            linkUrl: {
                pathname: '/students/add-new-student',
                query: {
                    type: 'import',
                },
            }
        }
    ]

    useEffect(() => {
        if (query?.search || query?.classIds) setSearching(true)
        else setSearching(false)
    }, [
        query?.search,
        query?.classIds
    ])

    const isSearchEmpty = useMemo(() => {
        if (!dataTable || dataTable.length === 0 && !searching) {
            return false
        }
        return true
    }, [searching, dataTable])

    const ButtonAddNewStudent = () =>
    (
        <div className={styles.dropDownCustom}>
            <DropDownLink
                dataMenu={dataDropDownLink}
                type="outline"
                query={query}
                title={t('student-management')['add-student']}
            />
        </div>
    )

    return (
        <>
            {(isSearchEmpty || isFirstRender) && (
                <div className={styles.studentManagement}>
                    {showErr && (
                        <AlertMessage
                            className={styles.alertMessage}
                            onClose={handleHideErr}
                            onShow={handleClickShowErr}
                        />
                    )}
                    <div className={styles.boxAction}>
                        <FormikContext.Provider value={formikFormStudent}>
                            <BaseForm className={styles.actionFilterSearch}>
                                <InputTextField
                                    name="search"
                                    autoFocus={undefined}
                                    className={styles.aInputWithLabel}
                                    type={'text'}
                                    label=""
                                    maxLength={100}
                                    onChange={formikFormStudent.handleChange}
                                    disabled={false}
                                    hideErrorMessage={undefined}
                                    iconLeft={<SearchIcon />}
                                    iconRight={undefined}
                                    placeholder={t('student-management')['placeholder-search']}
                                    autoComplete="off"
                                    key="search"
                                    onlyNumber={false}
                                />
                                <CheckSelectPicker
                                    name="classes"
                                    label={t('learring-result')['class']}
                                    searchable={false}
                                    cleanable={false}
                                    style={{ width: 161 }}
                                    data={dataClassOfTeacher}
                                    onChange={onChangeClasses}
                                />
                                <ButtonCustom
                                    onClick={handleReset}
                                    iconRight={<ResetIcon />}
                                    disable={!formikFormStudent.values.search?.length
                                        && !formikFormStudent.values.classes?.length}
                                    className={classNames(styles.aButtonReset, {
                                        [styles.disable]: !formikFormStudent.values.search?.length
                                            && !formikFormStudent.values.classes?.length,
                                    })}
                                >
                                    {t('common')['reset']}
                                </ButtonCustom>
                            </BaseForm>
                        </FormikContext.Provider>
                        <div className={styles.actionBtn}>
                            {/* <ButtonCustom className={styles.exportStudent} type="outline">Xuất DS học viên</ButtonCustom> */}
                            <DropDownLink dataMenu={dataDropDownLink} type="outline" query={query} title={t('student-management')['add-student']}/>
                        </div>
                    </div>
                    <div
                        className={classNames(styles.tableDataContent, {
                            [styles.noFooter]: dataTable.length === 0,
                        })}
                    >
                        <div className={styles.boxTableData} ref={refTable}>
                            <table>
                                <thead>
                                    <tr>
                                        <th>{t('phone')}</th>
                                        <th align="left">{t('common')['name-and-email']}</th>
                                        <th>{t('common')['birthday']}</th>
                                        <th>{t('common')['gender']}</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                {!isLoad && dataTable.length > 0 && (
                                    <tbody>
                                        {dataTable.map((item) => {
                                            return (
                                                <tr key={item.id}>
                                                    <td title={item.phone ? item.phone : ''}>{item.phone || '---'}</td>
                                                    <td>
                                                        <Link href={`${path.studentInfo}?id=${item.id}`}>
                                                            <a>
                                                                <span className={styles.name} title={item.full_name ? item.full_name : ''}>
                                                                    {item.full_name || '---'}
                                                                </span>
                                                            </a>
                                                        </Link>
                                                        <span className={styles.email} title={item.email ? item.email : ''}>
                                                            {item.email || '---'}
                                                        </span>
                                                    </td>
                                                    <td title={item.birthday ? dayjs(item.birthday).format(DATE_DISPLAY_FORMAT) : ''}>
                                                        {item.birthday
                                                            ? dayjs(item.birthday).format(DATE_DISPLAY_FORMAT)
                                                            : '---'}
                                                    </td>
                                                    <td>
                                                        {item.gender
                                                            ? item.gender === 1
                                                                ? t('gender')['male']
                                                                : t('gender')['female']
                                                            : '---'}
                                                    </td>
                                                    <td>
                                                        <DeleteIcon onClick={() => onClickDeleteItem(item)} />
                                                    </td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                )}
                            </table>
                            {!isLoad && dataTable.length === 0 && (
                                <div className={styles.noStudent}>
                                    <NoFieldData
                                        field='student_management'
                                        isSearchEmpty={isSearchEmpty}
                                        hasAction={false}
                                    />
                                </div>
                            )}
                            {isLoad && <div className={styles.loading}><Loader backdrop content="loading..." vertical /></div>}
                        </div>
                    </div>
                    {dataTable.length > 0 && (
                        <div className={styles.footer}>
                            <StickyFooter>
                                <div className="__pagination">
                                    <Pagination
                                        prev
                                        next
                                        ellipsis
                                        size="md"
                                        total={totalPages * limit}
                                        maxButtons={10}
                                        limit={limit}
                                        activePage={page}
                                        onChangePage={handleChanePage}
                                    />
                                </div>
                            </StickyFooter>
                        </div>
                    )}
                </div>
            )}
            {!isLoad && !isSearchEmpty && (
                <div className={styles.studentManagement}>
                    <NoFieldData
                        field='student_management'
                        isSearchEmpty={isSearchEmpty}
                        hasAction={false}
                        renderElement={() => <ButtonAddNewStudent />}
                    />
                </div>
            )}
        </>
    )
}