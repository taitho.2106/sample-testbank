import React, { useContext, useEffect, useRef, useState } from 'react'

import classNames from 'classnames';
import Excel from 'exceljs';
import { useRouter } from 'next/router';

import ChevronIcon from '@/assets/icons/arrow.svg'
import DeleteIcon from '@/assets/icons/ic-del-std.svg'
import ExcelIcon from '@/assets/icons/ic-excel.svg'
import ImportStudentsIcon from '@/assets/icons/ic-import-students.svg'
import { ClassError } from '@/constants/errorCodes';
import useNoti from '@/hooks/useNoti';
import { paths } from '@/interfaces/constants';
import { WrapperContext } from '@/interfaces/contexts';
import { callApiConfigError } from 'api/utils';

import ButtonCustom from '../Dashboard/SideBar/ButtonCustom/Button';
import AlertMessage from './AlertMessage';
import styles from './ImportNewStudents.module.scss';
import { LIMIT_CLASS_STUDENT_PACKAGE_BASIC } from "@/constants/index";
import useTranslation from "@/hooks/useTranslation";


type PropsPage = {
  isShowBack?:boolean
  callData?:() => void
}
export default function ImportNewStudents({ isShowBack = true, callData }: PropsPage) {
  const { t, locale } = useTranslation();
  const { dataImportError, countRowData, globalModal } = useContext(WrapperContext)
  const isCheckData = dataImportError.state?.length > 0
  const { back, query, push } = useRouter()
  const isCondition = query.idClass || query.classeSlug
  const path =
      isCondition
      ? `/api/classes/${
          query.idClass || query.classeSlug
        }/student/import`
      : `/api/teacher/students/import`
    const pathname =
        isCondition
        ? `/classes/${query.idClass ? query.idClass : query.classeSlug}`
        : paths.studentSlug
    const querySlub =
        isCondition
        ? undefined
        : {
            studentSlug: 'details',
          }
  const { getNoti } = useNoti()
  const [file, setFile] = useState(null)
  const [fileName, setFileName] = useState('')
  const [error, setError] = useState({
    load: false,
    isErr:false,
    message:''
  })
  const [isLoad, setIsLoad] = useState(false)
  const [isDrag, setIsDrag] = useState(false)
  const inputRef = useRef<HTMLInputElement>(null)
  const allowedTypes = [
    'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'application/vnd.ms-excel',
  ]
  const handleClick = () => {
    inputRef.current.value = null
    inputRef.current.click()
  }
  const handleChangeFile = async(e: any) => {
    e.preventDefault()
    const files = inputRef.current.files
    if (files && files[0]) {
      try {
          await handleReadFile(files[0])
      } catch (error) {
        setError({
          load: false,
          isErr: true,
          message: error as string,
        })
      }
    }
  }
  const handleReadFile = (file: File) => {
    const MAX_ROWS = 1000;
    return new Promise((resolve, reject) => {
      const wb = new Excel.Workbook();
      const reader = new FileReader();
      reader.onloadend = (e) => {
        if (!allowedTypes.includes(file.type)) {
          reject(t('import-new-students')['error-msg-1']);
          return;
        }
        const buffer: any = e.target.result;
        wb.xlsx
            .load(buffer)
            .then((workbook) => {
              workbook.eachSheet((sheet) => {
                let countRow = 0;
                sheet.eachRow((row: any) => {
                  const rowData: any = row.values.slice(1, 7);
                  const hasData = rowData.some(
                      (cell: any) =>
                          cell !== null && cell !== undefined && cell !== ""
                  );
                  if (hasData) {
                    countRow++;
                  }
                });
                if (countRow <= 1) {
                  reject(t('import-new-students')['error-msg-2']);
                  return;
                }
                if (countRow > MAX_ROWS + 1) {
                  reject(t('import-new-students')['error-msg-3']);
                  return;
                }
                setFile(file);
                setFileName(file?.name);
                setError({
                  load: false,
                  isErr: false,
                  message: ""
                });
              });
            })
            .catch(() => reject(t('import-new-students')['error-msg-4']));
      };
      reader.onerror = () => {
        reject(t('import-new-students')['error-msg-5']);
      };
      reader.readAsArrayBuffer(file);
    });
  };
  const handleDelFile = () => {
    inputRef.current.value = null
    setFile(null)
    setIsDrag(false)
    setFileName('')
    setError({
      load:false,
      isErr: false,
      message: '',
    })
  }
  const handleSubmitFile = async () => {
    if (!error.isErr) {
      const formData = new FormData()
      setIsLoad(true)
      formData.append('file', file)
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onloadend = async () => {
        if (!reader.result) {
          setIsLoad(false)
          getNoti(
            'error',
            'topCenter',
              t('import-new-students')['error-msg-6'],
          )
        }else{
            setError({
              load: false,
              isErr: false,
              message: '',
            })
          const response: any = await callApiConfigError(path,'post','token',formData)
          if (response.code === 200) {
              countRowData.setState(response.data.length)
              const dataError = response.data.filter((item: any) =>item.status && item.status!== ClassError.IMPORT_STUDENT_OLD) || []
              dataImportError.setState(dataError)
            if (dataError.length > 0) {
              if (dataError.length === response.data.length) {
                setIsLoad(false)
                getNoti(
                  'error',
                  'topCenter',
                    t('import-new-students')['error-msg-7'],
                )
              } else {
                if (callData) {
                  callData()
                } else {
                  push({ pathname, query: querySlub })
                }
              }
            } else {
              if (callData) {
                callData()
              } else {
                getNoti(
                  'success',
                  'topCenter',
                    t('import-new-students')['upload-succeed-msg'],
                )
                push({ pathname, query:querySlub })
              }
            }
          } else {
            setIsLoad(false)
            dataImportError.setState([])
            if (response.code === ClassError.IMPORT_STUDENT_REACH_LIMIT) {
              getNoti(
                  "error",
                  "topCenter",
                  t(t('common-get-noti')['student-reach-limit'],[`${LIMIT_CLASS_STUDENT_PACKAGE_BASIC}`])
              );
            } else {
              getNoti(
                  "error",
                  "topCenter",
                  t('import-new-students')['error-msg-8']
              );
            }
          }
        }
      }
    }
  }

  const handleDrag = (e: any) => {
    e.preventDefault()
    e.stopPropagation()
    switch (e.type) {
      case 'dragenter':
      case 'dragover':
        setIsDrag(true)
        break
      case 'dragleave':
        setIsDrag(false)
        break
    }
  }
  const handleDrop = async (e: any) => {
    e.preventDefault()
    e.stopPropagation()
    setIsDrag(false)
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      try {
        await handleReadFile(e.dataTransfer.files[0])
      } catch (error) {
        setError({
          load: false,
          isErr: true,
          message: error as string,
        })
      }
    }
  }
  const handleClickShowErr = () => {
    globalModal.setState({
      id: 'import-fail',
      content: {
        data: dataImportError.state,
        countRow: countRowData.state,
        isClassImport: !!isCondition,
      },
    })
  }
  const handleHideErr = () => {
    dataImportError.setState([])
  }
  const handleBack = () => {
    back()
    handleHideErr()
  }
  const onClickCancel = () => {
    const pathname = query.classeSlug ? query.idClass ? `/classes/${query.idClass}` : `/classes` : '/students/details'
    push({ pathname })
  }
  return (
    <div className={styles.importStudents}>
      <div className={styles.layoutContent}>
        {isShowBack && (
          <div className={styles.headerBack} onClick={handleBack}>
            <div className={styles.iconBack}>
              <ChevronIcon />
            </div>
            <span className={styles.textBack}>{t('common')['back']}</span>
          </div>
        )}
        {isShowBack && isCheckData && (
          <AlertMessage onClose={handleHideErr} onShow={handleClickShowErr} />
        )}
        {isLoad ? (
          <div className={styles.layoutImport}>
            <div className={styles.loading}></div>
            <span className={styles.textLoading}>
              {t('import-new-students')['waiting-msg']}
            </span>
          </div>
        ) : (
          <div className={styles.layoutImport}>
            <div
              className={classNames(styles.boxImport, {
                [styles.boxDrag]: !file,
                [styles.hover]: isDrag && !file,
              })}
              onDragEnter={handleDrag}
              onDragLeave={handleDrag}
              onDragOver={handleDrag}
              onDrop={handleDrop}
            >
              <div>
                <ImportStudentsIcon />
              </div>
              {!file && (
                <>
                  {!file && !error.isErr && (
                    <span>{t('import-new-students')['upload-action']}</span>
                  )}
                  {error.isErr && (
                    <span className={styles.errText}>{error.message}</span>
                  )}
                  <div>
                    <ButtonCustom onClick={handleClick} type="outline">
                      {t('import-new-students')['select-file']}
                    </ButtonCustom>
                  </div>
                </>
              )}
              {file && (
                <>
                  <div className={styles.hasFile}>
                    <ExcelIcon />
                    <span>{`Student import ${fileName}`}</span>
                    <div className={styles.delIcon} onClick={handleDelFile}>
                      <DeleteIcon />
                    </div>
                  </div>
                </>
              )}
              <input
                ref={inputRef}
                type="file"
                hidden
                accept=".xls,.xlsx"
                onChange={handleChangeFile}
              />
            </div>
            <ul className={styles.boxDescription}>
              <li>
                {t('import-new-students')['download-sample']}{' '}
                <a
                  download={true}
                  href={ isCondition ? locale === "en" ? `/contents/Files/Import_list-student_class_en.xlsx` : `/contents/Files/Import_list-student_class.xlsx` : locale === "en" ? `/contents/Files/Import_list-student_en.xlsx` : `/contents/Files/Import_list-student.xlsx`}
                >
                  {t('import-new-students')['here']}
                </a>
                , {t('import-new-students')['require-msg']}
              </li>
              <li>{t('import-new-students')['require-msg-2']}</li>
              <li>{t('import-new-students')['require-msg-3']}</li>
              <li>
                {t('import-new-students')['require-msg-4']}
              </li>
            </ul>
          </div>
        )}
      </div>
      <div
        className={classNames(styles.layoutFooter, {
          [styles.hide]: isLoad,
        })}
      >
        <ButtonCustom
          className={styles.cancel}
          type="outline"
          onClick={onClickCancel}
        >
          {t('common')['cancel']}
        </ButtonCustom>
        <ButtonCustom
          onClick={handleSubmitFile}
          className={classNames(styles.upload, {
            [styles.disabled]: !file || error.isErr || isLoad,
          })}
        >
          {t('common')['upload']}
        </ButtonCustom>
      </div>
    </div>
  )
}
