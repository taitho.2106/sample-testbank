import React, { CSSProperties, useContext } from 'react'

import classNames from 'classnames'
import { useRouter } from 'next/router'
import { Modal } from 'rsuite'

import IconClose from '@/assets/icons/close.svg'
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button'
import { WrapperContext } from '@/interfaces/contexts'

import styles from './ModalExpiredPackage.module.scss'
import { PAYMENT_TYPE, STEPS } from '@/interfaces/constants'
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'
import useTranslation from "@/hooks/useTranslation";
type PropsModal = {
  className?: string
  isActive?: boolean
  style?: CSSProperties
  onCloseModal?: () => void
}

export default function ModalExpiredPackage({
  className = '',
  isActive,
  style,
  onCloseModal
}: PropsModal) {
  const { t } = useTranslation()
  const router = useRouter()
  const handleOnClose = () => {
    onCloseModal()
  }
  const handleClick = () => {
    onCloseModal()
  }

  return (
    <Modal
      className={classNames(styles.wrapperModal, className)}
      open={isActive}
      onClose={handleOnClose}
      backdrop
      style={style}
    >
      <Modal.Body>
        <div className={styles.contentWrapper}>
          <div className={styles.boxContent}>
            <div className={styles.icon} onClick={handleOnClose}>
              {/*<IconClose />*/}
            </div>
            <div className={styles.img}>
              <img src="/images-v2/background-package.png" alt="" />
            </div>
            <div className={styles.text}>
              <h5>{t('modal-expired-package')['desc-1']}</h5>
              <span>
                {t('modal-expired-package')['desc-2']}
              </span>
            </div>
          </div>
          <div className={styles.boxBtn}>
            <ButtonCustom onClick={handleClick}>{t('modal-expired-package')['confirm-action']}</ButtonCustom>
          </div>
        </div>
      </Modal.Body>
    </Modal>
  )
}