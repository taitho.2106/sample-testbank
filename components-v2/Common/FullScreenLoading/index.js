import React, {useContext, useEffect} from 'react';

import styles from './FullScreenLoading.module.scss';
import {AppContext} from "../../../interfaces/contexts";
import {CircleLoading} from "../Controls/Loading";

const FullScreenLoading = () => {
    const {fullScreenLoading} = useContext(AppContext);

    useEffect(() => {
        let prevOverflowStyle
        if (fullScreenLoading.state) {
            prevOverflowStyle = document.body.style.overflow || ''
            document.body.style.overflow = 'hidden'
        }

        return () => {
            document.body.style.overflow = prevOverflowStyle
        }
    }, [fullScreenLoading.state])

    if (!fullScreenLoading.state) {
        return null
    }

    return (
        <div className={styles.fullScreenLoading}>
            <CircleLoading/>
        </div>
    );
};

export default FullScreenLoading;