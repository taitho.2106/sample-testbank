import { useEffect, useState } from 'react';

import classNames from 'classnames';
import { useField } from 'formik';
import { CheckPicker } from 'rsuite';

// @ts-ignore
import styles from './CheckSelectPicker.module.scss';

const CheckSelectPicker = ({
    name,
    className = '',
    style = {},
    onChange = (value) => undefined,
    data = [],
    label='',
    checkValue = true,
    placeholder = '',
    disabled =false,
    cleanable = true,
    menuStyle = {},
    searchable = true,
    placement = undefined,
    preventOverflow= false,
    menuClassName = '',
    container= null,
    ...propsSelect
}) => {
    const [field, meta, helpers] = useField(name)
    const isError = meta.error && meta.touched;
    const selectedItemList = field.value || []
    const [isFocus, setIsFocus] = useState(false)

    const [display, setDisplay] = useState(selectedItemList || [])
    
    const handleItemSelect = (value,e) => {
        onChange?.(value)
        helpers.setValue(value || [])
        setDisplay(value)
    }
    useEffect(() => {
        setDisplay(selectedItemList || [])
    },[field.value?.length])
    return (
        <div className={classNames(styles.aSelectPickerWrapper, className,{
            [styles.error]:isError
        })}
             style={style}
        >
            <div
                className={styles[`a-select-picker`]}
                data-focus={isFocus}
                data-active={display.length > 0}
                >
                <CheckPicker 
                    className={classNames(styles[`a-select-picker__dropdown`],{
                            [styles['focus']]:isFocus,
                            [styles.hasData]: display.length > 0
                        })}
                    data={data}
                    cleanable={cleanable}
                    disabled={disabled}
                    onChange={handleItemSelect}
                    menuStyle={menuStyle}
                    searchable={searchable}
                    preventOverflow={preventOverflow}
                    value={display}
                    placement={placement || 'bottomStart'}
                    onClose={() => setIsFocus(false)}
                    onOpen={() => setIsFocus(true)}
                    menuClassName={classNames(styles.menuSelected,menuClassName)}
                    container={container}
                    {...propsSelect}
                />
                {label && (
                    <label className={classNames({[styles.active]: isFocus || display.length > 0})}>
                        <span>
                            {isFocus || (checkValue && display) ? label : placeholder || label}
                        </span>
                    </label>
                )}

                
            </div>
            {isError && (
                <div className={styles.feedback}>{meta.error}</div>
            )}
        </div>
  )
}

export default CheckSelectPicker