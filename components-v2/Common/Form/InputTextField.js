import { useCallback, useEffect, useRef, useState } from 'react';

import classNames from 'classnames';
import { useField } from 'formik';


import CloseIcon from '../../../assets/icons/close.svg';
import EyeShowIcon from "../../../assets/icons/eye.svg";
import EyeHideIcon from "../../../assets/icons/eye2.svg";

import Button from '../Controls/Button';
import styles from './InputTextField.module.scss';

const InputTextField = ({
                            iconLeft,
                            iconRight,
                            label,
                            placeholder,
                            disabled,
                            className,
                            type = 'text',
                            onChange,
                            hideErrorMessage,
                            autoFocus,
                            autoComplete = 'on',
                            onlyNumber = false,
                            ignoreSpace = false,
                            error = false,
                            errorFeedback = "",
                            clearButton = false,
                            title = "",
                            spellCheck = false,
                            ...props
                        }) => {
    const [field, meta, helpers] = useField(props)
    const isError = error || (meta.touched && meta.error)
    const ref = useRef(null)
    const [isPassword, setIsPassword] = useState(type === 'password')

    const onChangeValue = (evt) => {
        let value = evt.target.value
        if (onlyNumber) {
            value = evt.target.value.replace(/\D/g,'')
        }
        if (ignoreSpace && value) {
            value = value.replace(/\s/g, '')
        }
        helpers?.setValue(value || '')
        onChange && onChange(value)
    }

    const togglePassword = useCallback(() => {
        if (ref.current.type === 'password') {
            ref.current.type = 'text';
            setIsPassword(false);
        }else{
            ref.current.type = 'password';
            setIsPassword(true);
        }
    }, [type]);
    const [wasInitiallyAutofilled, setWasInitiallyAutofilled] = useState(false)

    const handleClear = () => {
        if (ref.current) {
            ref.current.value = "";
        }

        helpers?.setValue("", false);
        ref.current?.focus();

        onChange && onChange("");
    }

    useEffect(() => {
        autoFocus ? ref.current.focus() : undefined;

        /**
         * The field can be prefilled on the very first page loading by the browser
         * By the security reason browser limits access to the field value from JS level and the value becomes available
         * only after first user interaction with the page
         * So, even if the Formik thinks that the field is not touched by user and empty,
         * it actually can have some value, so we should process this edge case in the form logic
         */
        const checkAutofilled = () => {
            const autofilled = !!ref.current?.matches('*:-webkit-autofill')
            setWasInitiallyAutofilled(autofilled)
        }
        // The time when it's ready is not very stable, so check few times
        const timer = setTimeout(checkAutofilled, 500)
        // setTimeout(checkAutofilled, 1000)
        // setTimeout(checkAutofilled, 1500)
        return () => clearTimeout(timer)
    }, [])

    return (
        <div
            className={classNames(
                styles.inputTextField,
                isError && styles.error,
                className,
            )}>
            <div className={styles.inputGroup}>
                {
                    iconLeft &&
                    <span className={styles.iconLeft}>{iconLeft}</span>
                }
                <div className={classNames(styles.inputContent,{
                    [styles.hasIconLeft]: !!iconLeft,
                    [styles.hasIconRight]: !!iconRight,
                    [styles.hasDataInput]: wasInitiallyAutofilled ||  field.value,
                })}>
                    <input
                        ref={ref}
                        {...field}
                        type={type}
                        disabled={disabled}
                        title={title}
                        spellCheck={spellCheck}
                        autoComplete={autoComplete}
                        className={classNames({
                            [styles.hasIconLeft]: !!iconLeft,
                            [styles.hasIconRight]: !!iconRight,
                            [styles.isPassword]: type === 'password',
                            [styles.hasDataInput]: wasInitiallyAutofilled ||  field.value,
                        })}
                        onChange={onChangeValue}
                        onDragOver={e => e.preventDefault()}
                    />
                    <label className={classNames(styles.label, {
                        [styles.hasData]: wasInitiallyAutofilled || field.value,
                    })}>
                        {placeholder}
                    </label>
                    {
                        clearButton ? !(type === "password") && <div className={styles.clearIcon}>
                        <Button type="none" variant="blank" onClick={handleClear} shape={"rounded-circle"} className={styles.buttonClear}>
                            <CloseIcon className={styles.closeIcon} />
                        </Button>
                    </div> : <></>
                    }
                </div>
                {
                    iconRight &&
                    <span className={classNames({
                        [styles.iconRight]: true,
                        [styles.iconRightHasData]: wasInitiallyAutofilled ||  field.value,
                    })}>{iconRight}</span>
                }
                {type === 'password' &&
                    <span className={styles.iconRight}>
                       {isPassword ?
                           <EyeHideIcon onClick={() => togglePassword()}/>
                           :
                           <EyeShowIcon onClick={() => togglePassword()}/>}
                    </span>
                }
            </div>
            {isError &&
                <div className={styles.feedback}>{errorFeedback || meta.error}</div>
            }
        </div>
    )
}

export default InputTextField
