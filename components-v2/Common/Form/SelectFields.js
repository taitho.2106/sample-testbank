import React, {useState, useCallback, useEffect} from 'react'

import classNames from 'classnames'
import {useField} from 'formik'
import { SelectPicker } from 'rsuite'

import styles from './SelectFields.module.scss';

const SelectFields = ({
    name,
    className = '',
    style = {},
    onChange,
    data = [],
    label='',
    checkValue = true,
    placeholder = '',
    disabled =false,
    cleanable = false,
    menuStyle = {},
    searchable = false,
    placement = undefined,
}) => {
    const [field, meta, helpers] = useField(name)
    const isError = meta.error && meta.touched;
    const selectedItem = data.find(item => item.value === field.value)
    const [isFocus, setIsFocus] = useState(false)
    const [display, setDisplay] = useState(selectedItem?.value || null)
    
    const handleItemSelect = (value,e) => {
        onChange?.(value)
        helpers.setValue(value || undefined)
        setDisplay(value)
    }
    useEffect(() => {
        setDisplay(selectedItem?.value || null)
    },[field.value])
    return (
        <div className={classNames(styles.aSelectPickerWrapper, className,{
            [styles.error]:isError
        })}
             style={style}
        >
            <div
                className={styles[`a-select-picker`]}
                data-focus={isFocus}
                >
                <input name={name} type="hidden" value={selectedItem?.value || ''} />
                <SelectPicker 
                    className={classNames(styles[`a-select-picker__dropdown`],{
                            [styles['focus']]:isFocus,
                            [styles.hasData]: field.value
                        })}
                    data={data}
                    virtualized
                    cleanable={cleanable}
                    disabled={disabled}
                    onChange={handleItemSelect}
                    menuStyle={menuStyle}
                    searchable={searchable}
                    value={display}
                    placement={placement || 'bottomStart'}
                    onClose={() => setIsFocus(false)}
                    onOpen={() => setIsFocus(true)}
                    caretAs={() => 
                    <img
                        className={styles["a-select-picker__caret"]}
                        src="/images/icons/ic-chevron-down-dark.png"
                        alt="toggle"
                    />}
                    menuClassName={styles.menuSelected}
                />
                {label && (
                    <label className={classNames({[styles["active"]]: isFocus || (checkValue && display)})}>
                    <span>
                        {isFocus || (checkValue && display) ? label : placeholder || label}
                    </span>
                    </label>
                )}

                
            </div>
            {isError && (
                <div className={styles.feedback}>{meta.error}</div>
            )}
        </div>
  )
}

export default SelectFields