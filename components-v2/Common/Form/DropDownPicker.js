import React, {useState, useCallback, useEffect} from 'react'

import classNames from 'classnames'
import {useField} from 'formik'
import { Dropdown } from 'rsuite'

// @ts-ignore
import styles from './DropDownPicker.module.scss';

const DropDownPicker = ({
    name,
    className = '',
    style = {},
    onChange,
    data = [],
    label='',
    checkValue = true,
    placeholder = '',
    disabled =false,
    hiddenValue = false,
}) => {
    const [field, meta, helpers] = useField(name)
    const isError = meta.error && meta.touched;
    const selectedItem = data.find(item => item.code === field.value)
    const [isFocus, setIsFocus] = useState(false)

    const [display, setDisplay] = useState(selectedItem?.display || '')
    
    const handleItemSelect = useCallback((e, item) => {
        onChange?.(item)
        helpers.setValue(item?.code || null)
        setDisplay(item.display)
    },[])
    useEffect(() => {
        setDisplay(selectedItem?.display || "")
    },[field.value])
    return (
        <div className={classNames(styles.aSelectPickerWrapper,className,{
            [styles.error]:isError
        })}
            style={style}
        >
            <div
                className={styles[`a-select-picker`]}
                data-focus={isFocus}
                style={style}
                >
                <input name={name} type="hidden" value={selectedItem?.display || ''} />
                {label && (
                    <label className={classNames({[styles["active"]]: isFocus || (checkValue && display)})}>
                    <span>
                        {isFocus || (checkValue && display) ? label : placeholder || label}
                    </span>
                    </label>
                )}

                <img
                    className={styles["a-select-picker__caret"]}
                    src="/images/icons/ic-chevron-down-dark.png"
                    alt="toggle"
                />
                <Dropdown
                    className={classNames(styles[`a-select-picker__dropdown`],{
                        [styles["hidden"]]:data.length == 0,
                        [styles['focus']]:isFocus
                    })}
                    disabled={disabled}
                    title={hiddenValue ? '' : display}
                    onClose={() => setIsFocus(false)}
                    onOpen={() => setIsFocus(true)}
                    onSelect={(eventKey, e) => handleItemSelect(e, eventKey)}
                >
                    {data.map((item) => (
                    <Dropdown.Item key={item.code} eventKey={item}>
                        {item.display}
                    </Dropdown.Item>
                    ))}
                </Dropdown>
            </div>
            {isError && (
                <div className={styles.feedback}>{meta.error}</div>
            )}
        </div>
  )
}

export default DropDownPicker