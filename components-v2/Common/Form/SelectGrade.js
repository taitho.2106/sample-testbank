import React, { useEffect, useState } from 'react'

import classNames from 'classnames'
import { useField } from 'formik'

import ChevronIcon from '../../../assets/icons/chevron-down.svg'
import { useOutsideClick } from '../../../hooks/useOutsideClick'
import styles from './SelectField.module.scss'

const SelectGrade = ({
    name,
    className = '',
    menuClassName = '',
    onChange,
    options = [],
    titleDropdown = '',
    placeHolder = '',
    disable = false,
    ...rest
}) => {
    const [field, meta, helpers] = useField(name)
    const isError = meta.error && meta.touched;
    const [isShow, setIsShow] = useState(false)
    const ref = useOutsideClick(() => setIsShow(false), true, false)
    const selectedItem = options.find(item => item.value === field.value)

    const onChangeValue = (item) => {
        helpers.setValue(item?.value || '')
        onChange?.(item)
    }

    useEffect(() => {
        if (isShow && selectedItem) {
            // Scroll to the checked option element when the dropdown is opened
            const checkedOption = document.getElementById(`${selectedItem.value}`)
            checkedOption.scrollIntoView({
                behavior: 'auto',
                // block: 'nearest',
                block: 'start',
            })
        }
    }, [isShow])

    return (
        <div className={classNames(styles.selectFieldWrapper, className, {
            [styles.error]: isError,
        })}
            onClick={() => disable ? undefined : setIsShow(!isShow)} ref={ref}>
            <div className={classNames(styles.selectGroup, {
                [styles.hasData]: isShow,
                [styles.disable]: disable
            })}>
                <div style={{ width: '100%' }}>
                    <div className={styles.select}>
                        <div className={styles.content}>
                            <div className={styles.imgLabel}>
                                {selectedItem?.icon}
                            </div>
                            <input {...field} value={selectedItem?.label || ''} disabled={disable} readOnly />
                        </div>
                    </div>
                    <label className={classNames(styles.label, {
                        [styles.hasData]: field.value || isShow,
                    })}>
                        {placeHolder}
                    </label>
                </div>
                <ChevronIcon />
            </div>
            {isError && (
                <div className={styles.feedback}>{meta.error}</div>
            )}
            {isShow && <div className={classNames(styles.selectMenu, menuClassName)}>
                {titleDropdown && <p>{titleDropdown}</p>}
                {options.map((item, index) => (
                    <div key={index}
                        className={classNames(styles.item, {
                            [styles.active]: item.value === selectedItem?.value,
                        })}
                        onClick={() => onChangeValue(item)}
                        id={item.value}
                    >
                        {item.label}
                    </div>
                ))}
            </div>}
        </div>
    )
}

export default SelectGrade