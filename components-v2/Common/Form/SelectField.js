import React, {useEffect, useRef, useState} from 'react'
import {useField} from 'formik'
import classNames from 'classnames'

import {useOutsideClick} from '../../../hooks/useOutsideClick'

import ChevronIcon from '../../../assets/icons/chevron-down.svg'

import styles from './SelectField.module.scss'

const SelectField = ({
    name,
    className,
    menuClassName,
    onChange,
    options,
    titleDropdown,
    placeHolder,
    disable,
    scrollIntoViewOnOpen = false,
    ...rest
}) => {
    const [field, meta, helpers] = useField(name)
    const isError = meta.error && meta.touched;
    const [isShow, setIsShow] = useState(false)
    const ref = useOutsideClick(() => setIsShow(false), true, false)
    const selectedItem = options.find(item => item.value === field.value)
    const selectedRef = useRef(null);

    const onChangeValue = (item) => {
        helpers.setValue(item?.value || '')
        helpers.setTouched(false, true)
        onChange?.(item)
    }

    const handleItemClick = (e, item) => {
        if (item.disabled) {
            e.stopPropagation();
        } else {
            onChangeValue(item)
        }
    }

    useEffect(() => {
        if(isShow && scrollIntoViewOnOpen) {
            selectedRef.current?.scrollIntoView({ block: "center", inline: 'nearest' });
        }
    }, [ isShow, scrollIntoViewOnOpen ]);

    return (
        <div className={classNames(styles.selectFieldWrapper, className, {
            [styles.error]: isError,
        })}
            onClick={() => disable ? undefined : setIsShow(!isShow)} ref={ref}>
            <div className={classNames(styles.selectGroup, {
                [styles.hasData]: isShow,
                [styles.disable]: disable
            })}>
            <div style={{width: '100%'}}>
                    <div className={styles.select}>
                        <div className={styles.content}>
                            <div className={styles.imgLabel}>
                                {selectedItem?.icon}
                            </div>
                            <input {...field} value={selectedItem?.label || ''} disabled={disable} readOnly/>
                        </div>
                    </div>
                    <label className={classNames(styles.label, {
                        [styles.hasData]: field.value || isShow,
                    })}>
                        {placeHolder}
                    </label>
                </div>
                <ChevronIcon/>
            </div>
            {isError && (
                <div className={styles.feedback}>{meta.error}</div>
            )}
            {isShow && <div className={classNames(styles.selectMenu, menuClassName)}>
                {titleDropdown && <p>{titleDropdown}</p>}
                {options.map((item, index) => (
                    <div key={index}
                        className={classNames(styles.item, {
                            [styles.active]: item.value === selectedItem?.value,
                            [styles.disabled]: item.disabled,
                        })}
                        onClick={e => handleItemClick(e, item)}
                        id={item.value}
                        ref={item.value === field.value ? selectedRef : null}
                    >
                        <span>
                            {item.label}
                        </span>
                    </div>
                ))}
            </div>}
        </div>
    )
}

export default SelectField