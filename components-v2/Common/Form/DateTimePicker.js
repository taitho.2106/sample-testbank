// @ts-nocheck
import React, { useEffect, useState } from 'react'

import classNames from 'classnames';
import dayjs from 'dayjs';
import isToday from 'dayjs/plugin/isToday';
import {useField} from 'formik'
import { DatePicker } from 'rsuite'

import styles from './DateTimePicker.module.scss'
dayjs.extend(isToday)

const DateTimePicker = ({
  className = '',
  disabled = false,
  label='',
  name = '',
  placement,
  time = false,
  style={},
  isDisableDateCurrentToFuture = false,
  onChange = () => null,
  oneTap = true,
  cleanable = false
}) => {
    const format = time ? 'dd/MM/yyyy HH:mm' : 'dd/MM/yyyy'
    const [field, meta, helpers] = useField(name)
    const isError = meta.touched && meta.error
    const [isFocus, setIsFocus] = useState(false)
    const defaultValue = field.value?.length > 0 ? new Date(field.value) : null
    const [value, setValue] = useState(defaultValue)
    const currentDate = new Date()
    const handleChange = (date) => {
        if(isDisableDateCurrentToFuture && disabledDate(date)){
            return
        }
        helpers.setValue(!!date ? new Date(date).toISOString() : "")
        const d = !!date ? new Date(date) : null
        setValue(d)
        const timestamp = d?.getTime()
        if (onChange) onChange(timestamp)
    }
    const disabledDate = (date) => isDisableDateCurrentToFuture && (dayjs(date).isAfter(currentDate,'date') || dayjs(date).isToday())
    useEffect(() => {
        setValue(defaultValue)
    },[field.value])
    return (
        <div
            className={classNames(styles.aDateInput, className, {
                [styles.disabledInput]:disabled,
                [styles.error]:isError
            })}
            style={style}
        >
            <div
                className={styles["a-date-input__cover"]}
                data-active={!!value}
                data-disabled={disabled}
                data-focus={isFocus}
            >
                <label>
                    <span>{label}</span>
                </label>
                <DatePicker
                    className={styles["a-date-input__picker"]}
                    cleanable={cleanable}
                    oneTap={oneTap}
                    disabled={disabled}
                    format={format}
                    value={value}
                    placement={placement || 'bottomEnd'}
                    onEnter={() => setIsFocus(true)}
                    onExit={() => setIsFocus(false)}
                    onChange={(date) => handleChange(date)}
                    onSelect={(date) => handleChange(date)}
                    onNextMonth={(date) => handleChange(date)}
                    onPrevMonth={(date) => handleChange(date)}
                    disabledDate={(date)=>disabledDate(date)}
                />
            </div>
            {isError &&
                <div className={styles.feedback}>{meta.error}</div>
            }
        </div>
    )
}
export default DateTimePicker;