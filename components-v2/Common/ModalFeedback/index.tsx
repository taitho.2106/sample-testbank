import React from 'react'

import { Modal } from 'rsuite'

import IconClose from '@/assets/icons/icon-close.svg';

import styles from './ModalFeedback.module.scss';
import useTranslation from "@/hooks/useTranslation";

export default function ModalFeedback({isShow = false,setIsShow ,onReset}:any) {
    const { t } = useTranslation()
    const handleOnClose = () =>{
        setIsShow(false)
        onReset()
    }
  return (
    <Modal
        className={styles.modalFeedback}
        open={isShow}
        style={{
            overflow: 'hidden',
            maxHeight: '100%',
            background: "rgba(0, 0, 0, 0.5)",
            display:'flex',
        }}
        onClose={handleOnClose} 
        backdrop={false}
    >
        <Modal.Body>
            <div className={styles.modalContainer}>
                <div className={styles.iconClose} onClick={handleOnClose}>
                   <IconClose />
                </div>
                <div className={styles.backgroundImage}>
                    <img 
                        src='/images-v2/image-feedback.png'
                        alt=''
                    />
                </div>
                <div className={styles.text}>
                    {t('feedback')}
                </div>
            </div>
            </Modal.Body>
    </Modal>
  )
}
