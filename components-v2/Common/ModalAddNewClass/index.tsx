import React from 'react'

import classNames from 'classnames'
import { FormikContext, useFormik } from 'formik'
import { useRouter } from 'next/router'
import { Modal } from 'rsuite'
import * as Yup from "yup";

import InformationIcon from '@/assets/icons/information.svg'
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button'
import useGrade from '@/hooks/useGrade'
import useNoti from '@/hooks/useNoti'
import { paths } from 'api/paths'
import { callApiConfigError } from 'api/utils'

import BaseForm from '../Controls/BaseForm'
import InputTextField from '../Form/InputTextField'
import SelectGrade from '../Form/SelectGrade'
import SelectYearPicker from '../Form/SelectYearPicker'
import styles from './ModalAddNewClass.module.scss'
import useTranslation from "@/hooks/useTranslation";

export default function ModalAddNewClass({
    isOpen,
    onClose,
    onReload,
    className,
    dataEdit,
}: any) {
    const { t } = useTranslation();
    const { getGrade } = useGrade()
    const { getNoti } = useNoti()
    const { push } = useRouter()

    const currentYear = new Date().getFullYear()
    const gradeList = getGrade.filter(item => /^G[0-9]{1,2}$/.test(`${item.code}`)) || []
    const gradeOptions = gradeList.map(({ code, display }) => ({
        value: code,
        label: display,
    }))
    const isEdit = dataEdit ? true : false
    const handleOnclose = () => {
        onClose()
    }

    const onSubmit = async (values: any) => {
        const endYear = values.start_year + 1

        const response: any = await callApiConfigError(
            `${paths.api_teacher_classes}/${isEdit ? dataEdit.id : ''}`,
            isEdit ? 'put' : 'post',
            'token',
            {
                ...values,
                end_year: endYear,
            }
        )
        onClose()
        if (response?.code == 200) {
            if (!isEdit) {
                const classItem = response?.data
                push({
                    pathname: `/classes/${classItem?.id}`,
                })
                getNoti('success', 'topCenter', t('common-get-noti')['add-class-succeed'])
            } else {
                onReload(true)
                getNoti('success', 'topCenter', t('common-get-noti')['edit-class-info-succeed'])
            }
        }
        if (response?.code == 400) {
            getNoti('error', 'topCenter', t('common-get-noti')['add-class-failed'])
        }
    }

    const formValidationSchema = () => {
        return Yup.object().shape({
            name: Yup.string().trim()
                .required(t('modal-add-edit-class')['required-name-msg'])
                .max(100, t('modal-add-edit-class')['limit-msg']),
            grade: Yup.string()
                .required(t('modal-add-edit-class')['required-grade-msg']),
        })
    }

    const formikForm = useFormik({
        initialValues: dataEdit ? dataEdit : { name: '', grade: '', start_year: 0, end_year: 0 },
        onSubmit: onSubmit,
        validationSchema: formValidationSchema,
    })

    return (
        <Modal
            className={classNames(styles.addClassModal, className)}
            open={isOpen}
            style={{
                overflow: 'hidden',
                background: 'rgba(0, 0, 0, 0.2)',
            }}
            backdrop={false}
        >
            <Modal.Body>
                <div className={styles.headerName}>
                    {isEdit ? t('modal-add-edit-class')['edit-class-info'] : t('modal-add-edit-class')['add-class']}
                </div>
                <FormikContext.Provider value={formikForm}>
                    <BaseForm className={styles.form}>
                        <InputTextField
                            name='name'
                            className={styles.selectClassName}
                            placeholder={t('modal-add-edit-class')['name-of-class']}
                            autoComplete='off'
                        />
                        <div className={styles.selectGradeAndYear}>
                            <SelectGrade
                                className={styles.selectGrade}
                                menuClassName={styles.menu}
                                options={gradeOptions}
                                placeHolder={t('common')['grade']}
                                name={'grade'}
                            />
                            <SelectYearPicker
                                className={styles.selectYear}
                                menuClassName={styles.menu}
                                placeHolder={t('modal-add-edit-class')['school-years']}
                                name={'start_year'}
                                defaultValue={isEdit ? '' : currentYear}
                            />
                        </div>
                        {/* <div className={styles.classPassCode}>
                            <InputTextField
                                name='here'
                                className={styles.classPassCode}
                                placeholder="Mã tham gia lớp "
                                type="text"
                                autoComplete='off'
                            />
                        </div>
                        <div className={styles.informationTag}>
                            <InformationIcon />
                            <div className={styles.infoText}>
                                Bạn nên nhập Mã tham gia lớp học trong trường hợp một số học
                                viên gặp sự cố không thể tham gia lớp học, khi đó, học viên
                                có thể dùng Mã tham gia lớp học và gửi yêu cầu hỗ trợ đến
                                bạn ngay trên hệ thống i-Test.
                            </div>
                        </div> */}
                        <div className={styles.footer}>
                            <ButtonCustom className={styles.cancel} type='reset' onClick={handleOnclose}>
                                {t('common')['cancel']}
                            </ButtonCustom>
                            <ButtonCustom
                                className={styles.submit}
                                buttonType={'submit'}
                            >
                                {isEdit ? t('common')['update'] : t('classes')['create-class']}
                            </ButtonCustom>
                        </div>
                    </BaseForm>
                </FormikContext.Provider>
            </Modal.Body>
        </Modal>
    )
}