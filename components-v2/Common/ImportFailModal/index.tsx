import React, { useContext } from 'react'

import classNames from 'classnames';
import dayjs from 'dayjs';
import Excel from 'exceljs';
import { Modal } from 'rsuite'

import ExportIcon from '@/assets/icons/icon-export.svg'
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button';
import { ClassError } from '@/constants/errorCodes';
import { WrapperContext } from '@/interfaces/contexts';
import { mapMessageError } from '@/utils/message';

import styles from './ModalImportFail.module.scss';
import useTranslation from "@/hooks/useTranslation";

type PropsModal = {
  isActive?: boolean
  className?: string
  style?:any
}
export default function ModalImportFail({isActive = false, className = '', style = {}}:PropsModal) {
  const { t } = useTranslation();
  const { globalModal } = useContext(WrapperContext)
  const dataTable:any[] = globalModal.state?.content?.data || []
  const isClassImport = globalModal.state?.content?.isClassImport || false
  const data: any[] = [...dataTable]
  const countRowErr = dataTable.filter(item => item.status && item.status !==  ClassError.IMPORT_STUDENT_OLD)
  const countRowData = globalModal.state?.content?.countRow || 0
  const handleOnClose = () => globalModal.setState(null)
  
  const exportFailResult = async () => {
    const workbook = new Excel.Workbook()
    const worksheet = workbook.addWorksheet('Sheet1')
    const tableObj = isClassImport ? cellArrayObjectFailClasses : cellArrayObjectFailStudent
    const columns = []
    const lenColumns = tableObj.length
    for (let i = 0; i < lenColumns; i++) {
      const itemCell = tableObj[i]
      columns.push({ header: t('excel-row')[itemCell.display], key: itemCell.key, width: 30 })
      const cell = worksheet.getCell(`${itemCell.code}1`)
      cell.fill = {
        type: 'pattern',
        bgColor: {
          argb: '6AA84F',
        },
        pattern: 'solid',
        fgColor: { argb: '6AA84F' },
      }
      cell.font = {
        size: 11,
        bold: true,
        color: { argb: 'FFFFFF' },
      }
      cell.alignment = {
        vertical: 'middle',
        horizontal: 'center',
        wrapText: true,
      }
      cell.border = {
        top: { style: 'thin', color: { argb: '79C440' } },
        left: { style: 'thin', color: { argb: '79C440' } },
        bottom: { style: 'thin', color: { argb: '79C440' } },
        right: { style: 'thin', color: { argb: '79C440' } },
      }
    }
    worksheet.columns = columns
    worksheet.getColumn(tableObj.length).style.alignment = {
      wrapText: true,
    }
    worksheet.getColumn(tableObj.length).style.font = {
      size: 11,
    }
    for (let i = 0; i < data.length; i++) {
      const itemResult = data[i]
      const errorTextArr = data[i].errors
        ? data[i].errors
            ?.map((item: any) => item.message)
            ?.reduce((a: string[], b: string[]) => [...a, ...b])
        : [mapMessageError({ isImportIntoClass : isClassImport, t})[data[i].status]]
      itemResult['errorText'] = errorTextArr.join(' ')
      worksheet.addRow(itemResult)
    }
    const buffer = await workbook.xlsx.writeBuffer()
    const url = URL.createObjectURL(
      new Blob([buffer], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      }),
    )
    const link = document.createElement('a')
    link.setAttribute("download", isClassImport ? "Import_list_student_class_fail.xlsx" : "Import_list_student_fail.xlsx");
    link.href = url
    link.click()
  }
  return (
    <Modal
      open={isActive}
      backdrop={false}
      style={{
        overflow: 'hidden',
        ...style,
      }}
      className={classNames(styles.modalWrapper, className)}
      onClose={handleOnClose}
    >
      <Modal.Body>
          <span className={styles.titleHeader}>
            {t('modal-student-import-fail')['title']}
          </span>
          <div className={styles.tableData}>
            <table>
              <thead>
                <tr>
                  <th className={classNames(styles.studentCode,{
                    [styles.classes]: isClassImport
                  })}
                    title={t('common')['student-code']}>{t('SBD')}</th>
                  <th className={styles.fullName}>{t('common')['full-name']}</th>
                  <th className={styles.phone}>{t('phone-SDT')}</th>
                  <th className={styles.email}>Email</th>
                  <th className={styles.birthday}>{t('common')['birthday-new']}</th>
                  <th className={styles.gender}>{t('common')['gender']}</th>
                  <th className={styles.note}>{t('common')['note']}</th>
                </tr>
              </thead>
              <tbody>
                {data.map((item: any, index: number) => {
                  const errorTextArr = !!item?.errors?.length
                    ? item.errors
                        ?.map((item: any) => item.message)
                        ?.reduce((a:string[], b:string[]) => [...a, ...b],[])
                    : [mapMessageError({ isImportIntoClass : isClassImport, t})[item.status]]
                  return (
                    <tr key={index}>
                      <td className={classNames(styles.studentCode,{
                        [styles.classes]: isClassImport
                      })}>{item.studentCode || '---'}</td>
                      <td className={styles.fullName}>{item.fullName || '---'}</td>
                      <td className={styles.phone}>{item.phone || '---'}</td>
                      <td className={styles.email}>{item.email || '---'}</td>
                      <td className={styles.birthday}>{item.birthday || '---'}</td>
                      <td className={styles.gender}>{item.gender || '---'}</td>
                      <td className={styles.note}>{errorTextArr.map((itemErr:string, index:number)=>{
                        return (
                          <div key={index}>
                            {itemErr}
                          </div>
                        )
                      })}</td>
                    </tr>
                  )
                })}
              </tbody>
            </table>
          </div>
          <div className={styles.footer}>
             <span className={styles.desError}>{`${t('modal-student-import-fail')['error-desc']} (${countRowErr.length}/${countRowData})`}</span>
             <div className={styles.actionButton}>
              <ButtonCustom
                className={styles.exportData}
                iconLeft={<ExportIcon />}
                onClick={exportFailResult}
              >
                {t('modal-student-import-fail')['download-list-fail']}
              </ButtonCustom>
              <ButtonCustom
                className={styles.cancel}
                onClick={handleOnClose}
                type="outline"
              >
                {t('common')['close']}
              </ButtonCustom>
             </div>
          </div>
      </Modal.Body>
    </Modal>
  )
}
const cellArrayObjectFailClasses = [
  { code: 'A', key: 'studentCode', display: 'student-code' },
  { code: 'B', key: 'fullName', display: 'full-name' },
  { code: 'C', key: 'phone', display: 'phone' },
  { code: 'D', key: 'email', display: 'email' },
  { code: 'E', key: 'birthday', display: 'birthday' },
  { code: 'F', key: 'gender', display: 'gender' },
  { code: 'G', key: 'errorText', display: 'note' },
]
const cellArrayObjectFailStudent = [
  { code: 'A', key: 'fullName', display: 'full-name' },
  { code: 'B', key: 'phone', display: 'phone' },
  { code: 'C', key: 'email', display: 'email' },
  { code: 'D', key: 'birthday', display: 'birthday' },
  { code: 'E', key: 'gender', display: 'gender' },
  { code: 'F', key: 'errorText', display: 'note' },
]

