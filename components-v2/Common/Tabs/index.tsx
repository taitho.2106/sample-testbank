import React from 'react'

import classNames from 'classnames';
import { Tooltip, Whisper } from 'rsuite';

import IconTailLeft from '@/assets/icons/tail-left.svg';
import IconTailRight from '@/assets/icons/tail-right.svg';
import useTranslation from '@/hooks/useTranslation';

import styles from './Tabs.module.scss';

type PropsTabs = {
    data:Data[]
    onClickStep: (index: number) => void
    step: number
    className?:string
}
type Data = {
  code?: number
  groupName?:string,
  icon?:any
  tooltipIcon?:any
  tooltipText?:string
}
export default function Tabs({data = [], onClickStep, step,className = ''}:PropsTabs) {
  const {t} = useTranslation()
  return (
    <div className={classNames(styles.tabsWrapper,className)}>
         {data.map((item,index:number)=>{
        return (
          <div
            key={index}
            className={classNames(styles.boxItem, {
              [styles['active']]: step === item?.code
            })}
            onClick={()=>onClickStep(item?.code)}
          >            
            {index !== 0 && <div className={styles.afterTemp}><IconTailLeft /></div>}
            <div className={styles.tabItem}>
              {item.icon || 
              <img 
                src='/images-v2/icons/tab-book.png'
                alt=''
              />}
              <span>{t(item.groupName)}</span>
              <div className={styles.tooltipItem}>
                {item.tooltipIcon && item.tooltipText && (
                  <Whisper
                    trigger="hover"
                    placement="auto"
                    container={() => document.querySelector(`.t-wrapper__main`)}
                    speaker={
                      <Tooltip arrow={false}>
                        <span
                          className={styles.tooltipText}
                          dangerouslySetInnerHTML={{ __html: item.tooltipText }}>
                        </span>
                      </Tooltip>
                    }
                  >
                    <div style={{ position: 'relative' }}>
                      {item.tooltipIcon}
                    </div>
                  </Whisper>
                )}
              </div>
            </div>
            <div className={styles.beforeTemp}><IconTailRight /></div>
          </div>
        )
      })}
    </div>
   
  )
}
