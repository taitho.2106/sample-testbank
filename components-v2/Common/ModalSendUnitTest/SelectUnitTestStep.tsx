import React, { useState } from 'react'

import classNames from 'classnames'
import dayjs from 'dayjs'

import IconX from '@/assets/icons/ic-x.svg'

import { DateTimePickerInput } from '../DateTimePickerCustom'
import { SingleSelectPicker } from '../SingleSelectPicker/SingleSelectPicker'
import styles from './SelectUnitTestStep.module.scss'
import useTranslation from "@/hooks/useTranslation";

type SentUnitTest = {
    key: number
    id: number
    name: string
    unit_type: number
    time: number
    start_date: string
    end_date: string
}

type TriggerState = {
    [index: number]: Date;
}

interface Props {
    dataOptions: SentUnitTest[]
    selectTest: SentUnitTest[]
    setSelectTest: React.Dispatch<React.SetStateAction<SentUnitTest[]>>
    error: any[]
    onAddMore?: () => void
    setIsAddingMore: React.Dispatch<React.SetStateAction<any>>
}

const SelectUnitTestStep = ({
    dataOptions,
    selectTest,
    setSelectTest,
    error,
    onAddMore,
    setIsAddingMore,
}: Props) => {
    const { t } = useTranslation();
    const UNIT_TYPE = [
        { code: '0', display: t('unit-type')['practice'] },
        { code: '1', display: t('unit-type')['exam'] },
    ]
    const [triggerEndDate, setTriggerEndDate] = useState<TriggerState>({})
    const unitTestSelectOptions = dataOptions.map(({ id, name }: SentUnitTest) => ({
        code: id,
        display: name,
    }))

    const getDefaultUniTestName = (data: SentUnitTest) => {
        const defaultName = {
            code: data.id,
            display: data.name,
        }
        return defaultName
    }

    const currentDate = new Date()

    const updateSelectTest = (index: number, updatedTest: Partial<SentUnitTest>) => {
        setSelectTest((prevSelectTest) =>
            prevSelectTest.map((test, i) => (i === index ? { ...test, ...updatedTest } : test))
        )
    }

    const handleUnitTestSelect = (value: any, chosenIndex: any) => {
        const chosenTest = dataOptions.find((unitTest: any) => unitTest.id === value);
        if (chosenTest) {
            setIsAddingMore(false)
            updateSelectTest(chosenIndex, {
                key: new Date().getTime(),
                id: value,
                name: chosenTest.name,
                time: chosenTest.time,
                unit_type: chosenTest.unit_type,
                start_date: new Date(currentDate.getTime()).toISOString(),
                end_date: new Date(currentDate.getTime() + chosenTest.time * 60 * 1000).toISOString(),
            })
        }
    };

    const handleUnitTypeChange = (value: string, chosenIndex: number, key: number) => {
        updateSelectTest(chosenIndex, { unit_type: Number(value) })

        if (value === '1' && selectTest[chosenIndex].start_date !== '') {
            const parseTime = selectTest[chosenIndex].time
            const startDateTimestamp = new Date(selectTest[chosenIndex].start_date).getTime()
            const endDateTimestamp = startDateTimestamp + parseTime * 60 * 1000;
            setTriggerEndDate((prevState) => ({ ...prevState, [key]: new Date(endDateTimestamp) }))
            updateSelectTest(chosenIndex, { end_date: new Date(endDateTimestamp).toISOString() })
        }
    }

    const handleStartDateChange = (timestamp: number, chosenIndex: number, key: number) => {
        const endDateTimestamp = timestamp + selectTest[chosenIndex].time * 60 * 1000;
        setTriggerEndDate((prevState) => ({ ...prevState, [key]: new Date(endDateTimestamp) }))
        updateSelectTest(chosenIndex, {
            start_date: new Date(timestamp).toISOString(),
            end_date: new Date(endDateTimestamp).toISOString(),
        })
    }

    const handleEndDateChange = (timestamp: number, chosenIndex: number) => {
        updateSelectTest(chosenIndex, { end_date: new Date(timestamp).toISOString() })
    }

    const disabledBeforeToday = (date: Date) => {
        return dayjs(date).isBefore(currentDate, 'date')
    }

    const disabledEndDate = (date: Date, startDate?: string) => {
        const tempStartDate = dayjs(new Date(startDate))
        const isBeforeStart = dayjs(date).isBefore(tempStartDate)
        const isBeforeToday = dayjs(date).isBefore(currentDate, 'date')
        return isBeforeToday || isBeforeStart
    }

    const onDelete = (index: number) => {
        const updatedSelectTest = [...selectTest]
        updatedSelectTest.splice(index, 1)
        setSelectTest(updatedSelectTest)
    }

    // console.log('error---fff', error);

    const getErrorClassName = (field: string, chosenIndex: number) => {
        const errorRow = error.find((item) => item.index === chosenIndex)
        return errorRow?.fields.includes(field)
    }

    return (
        <div className={styles.tableSelectContainer}>
            <table>
                <thead>
                    <tr className={styles.rowSticky}>
                        <th>{t('modal-send-unit-test')['unit-test']}</th>
                        <th>{t('common')['time']}</th>
                        <th>{t('common')['unit-type']}</th>
                        <th>{t('common')['start-date']}</th>
                        <th>{t('common')['end-date']}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {selectTest.map((chosenTest: SentUnitTest, chosenIndex: number) => (
                        <tr
                            key={chosenTest.key}
                            style={{
                                zIndex: selectTest.length - chosenIndex,
                                position: 'relative',
                            }}
                        >
                            <td>
                                <SingleSelectPicker
                                    name="name"
                                    className={classNames(styles.selectUnitName, {
                                        [styles.error]: getErrorClassName("name", chosenIndex)
                                    })}
                                    data={unitTestSelectOptions}
                                    label={t('modal-send-unit-test')['select-unit-test']}
                                    defaultValue={chosenTest.name != '' ? getDefaultUniTestName(chosenTest) : undefined}
                                    onChange={(e) => {
                                        handleUnitTestSelect(e, chosenIndex)
                                    }}
                                    toolTip={getErrorClassName("name", chosenIndex) ? t('modal-send-unit-test')['required-unit-test'] : undefined}
                                    inputSearchShow={true}
                                    labelSearch={t('assign-exam-filter')['search-name']}
                                />
                            </td>
                            <td>
                                {chosenTest.time != 0 ? chosenTest.time : '---'}
                            </td>
                            <td>
                                <SingleSelectPicker
                                    name="unit_type"
                                    className={classNames(styles.selectUnitType, {
                                        [styles.error]: getErrorClassName("unit_type", chosenIndex),
                                        [styles.disabled]: chosenTest.name == ''
                                    })}
                                    data={UNIT_TYPE}
                                    label={t('common')['unit-type']}
                                    defaultValue={UNIT_TYPE.filter(
                                        (item) => item.code == chosenTest.unit_type.toString(),
                                    )[0]}
                                    disabled={chosenTest.name == ''}
                                    onChange={(e) => handleUnitTypeChange(e, chosenIndex, chosenTest.key)}
                                    toolTip={getErrorClassName("unit_type", chosenIndex) ? t('modal-send-unit-test')['required-unit-type'] : undefined}
                                />
                            </td>
                            <td>
                                <DateTimePickerInput
                                    name="start_date"
                                    className={classNames(styles.selectTime, {
                                        [styles.error]: getErrorClassName("start_date", chosenIndex),
                                        [styles.disabled]: chosenTest.unit_type === -1
                                    })}
                                    label={t('common')['start-date']}
                                    time={true}
                                    defaultValue={chosenTest?.start_date}
                                    valueProp={chosenTest?.start_date}
                                    disabled={chosenTest.unit_type === -1}
                                    onChange={(e) => handleStartDateChange(e, chosenIndex, chosenTest.key)}
                                    placement="autoVertical"
                                    disabledDate={disabledBeforeToday}
                                    disabledMinutes={(minute) => {
                                        const currentDateSelect = new Date()
                                        const startDate = dayjs(new Date(chosenTest?.start_date)).set('m', minute);
                                        return startDate.isBefore(currentDateSelect, "minute");
                                    }}
                                    disabledHours={(hour) => {
                                        const currentDateSelect = new Date();
                                        const startDate = dayjs(new Date(chosenTest?.start_date)).set('h', hour);
                                        return startDate.isBefore(currentDateSelect, 'minute');
                                    }}
                                    toolTip={
                                        getErrorClassName('invalid_date', chosenIndex)
                                            ? t('modal-send-unit-test')['invalid-date']
                                            : getErrorClassName('start_date', chosenIndex)
                                                ? t('modal-send-unit-test')['required-start-date']
                                                : undefined
                                    }
                                />
                            </td>
                            <td>
                                <DateTimePickerInput
                                    name="end_date"
                                    className={classNames(styles.selectTime, {
                                        [styles.error]: getErrorClassName("end_date", chosenIndex),
                                        [styles.disabled]: chosenTest.unit_type === 1 || chosenTest.unit_type === -1
                                    })}
                                    label={t('common')['end-date']}
                                    time={true}
                                    defaultValue={chosenTest?.end_date}
                                    valueProp={chosenTest?.end_date}
                                    onChange={(e) => handleEndDateChange(e, chosenIndex)}
                                    disabled={chosenTest.unit_type === 1 || chosenTest.unit_type === -1}
                                    placement="autoVertical"
                                    triggerSetValue={triggerEndDate[chosenTest.key]}
                                    disabledDate={(date) => disabledEndDate(date, chosenTest?.start_date)}
                                    disabledMinutes={(minute) => {
                                        const startDate = chosenTest?.start_date;
                                        const endDate = dayjs(new Date(chosenTest?.end_date)).set('m', minute);
                                        const minDate = dayjs(new Date(startDate)).add(chosenTest.time, 'm');
                                        return endDate.isBefore(minDate);
                                    }}
                                    disabledHours={(hour) => {
                                        const startDate = chosenTest?.start_date;
                                        const endDate = dayjs(new Date(chosenTest?.end_date)).set('h', hour);
                                        const minDate = dayjs(new Date(startDate)).add(chosenTest.time, 'm');
                                        return endDate.isBefore(minDate);
                                    }}
                                    toolTip={
                                        getErrorClassName('invalid_date', chosenIndex)
                                            ? t('modal-send-unit-test')['invalid-date']
                                            : getErrorClassName('end_date', chosenIndex)
                                                ? t('modal-send-unit-test')['required-end-date']
                                                : undefined
                                    }
                                />
                            </td>
                            {selectTest.length > 1 ? (
                                <td align="center">
                                    <div onClick={onDelete.bind(null, chosenIndex)}>
                                        <IconX />
                                    </div>
                                </td>
                            ) : (
                                <td></td>
                            )}
                        </tr>
                    )
                    )}
                    {dataOptions.length > 0 && (
                        <tr className={styles.action}>
                            <td onClick={onAddMore}>{t("modal-send-unit-test")["select-more"]}</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    )}
                </tbody>
            </table>
        </div>
    )
}

export default SelectUnitTestStep
