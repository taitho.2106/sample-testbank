import React, { useCallback, useEffect, useMemo, useState } from 'react'

import classNames from 'classnames'
import { FormikContext, useFormik } from 'formik'
import qs from "qs";
import { Loader } from 'rsuite'

import ResetIcon from '@/assets/icons/reset-icon.svg'
import SearchIcon from '@/assets/icons/search.svg'
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button'
import { delayResult } from '@/utils/index'
import { paths } from 'api/paths'
import { callApiConfigError } from 'api/utils'

import BaseForm from '../Controls/BaseForm'
import InputTextField from '../Form/InputTextField'
import { CheckBoxCustom } from './CheckBoxCustom'
import styles from './SelectTargetStep.module.scss'
import useTranslation from "@/hooks/useTranslation";

interface Props {
    data: any,
    selectedType: string
    setSelectedType: React.Dispatch<React.SetStateAction<string>>
    chosenGroup: any[]
    setChosenGroup: React.Dispatch<React.SetStateAction<any[]>>
    chosenStudentList: any[]
    setChosenStudentList: React.Dispatch<React.SetStateAction<any[]>>
}

const SelectTargetStep = ({
    data,
    selectedType,
    setSelectedType,
    chosenGroup,
    setChosenGroup,
    chosenStudentList,
    setChosenStudentList
}: Props) => {
    const { t } = useTranslation();
    const [groupData, setGroupData] = useState([])
    const [studentData, setStudentData] = useState([])
    const [studentDataOG, setStudentDataOG] = useState([])
    const [searchValue, setSearchValue] = useState('')
    const [isLoadTable, setIsLoadTable] = useState(false)
    const [searching, setSearching] = useState(false)

    const handleTypeChange = useCallback((type) => {
        setSelectedType(type);

        if (type === 'all') {
            setChosenStudentList([]);
            setChosenGroup([]);
        }
    }, [setSelectedType, setChosenStudentList, setChosenGroup])

    const fetchClassGroups = async () => {
        const response: any = await callApiConfigError(
            `${paths.api_teacher_classes}/${data.id}/groups`,
        )
        if (response.result && response.data) {
            setGroupData(response.data)
        }
    }

    useEffect(() => {
        fetchClassGroups().finally()
    }, [])

    const handleChooseGroup = (id: number) => {
        const chosenItem = groupData.find((item: any) => item?.id === id);
        if (chosenItem) {
            const itemIndex = chosenGroup.findIndex((item: any) => item?.id === id);
            if (itemIndex !== -1) {
                const updatedList = [...chosenGroup]
                updatedList.splice(itemIndex, 1)
                setChosenGroup(updatedList)
            } else {
                setChosenGroup([...chosenGroup, chosenItem])
                setChosenStudentList([])
            }
        }
    }

    const handleChooseStudent = (id: number) => {
        const chosenItem = studentData.find((item: any) => item?.id === id);
        if (chosenItem) {
            const itemIndex = chosenStudentList.findIndex((item: any) => item?.id === id);
            if (itemIndex !== -1) {
                const updatedList = [...chosenStudentList]
                updatedList.splice(itemIndex, 1)
                setChosenStudentList(updatedList)
            } else {
                setChosenStudentList([...chosenStudentList, chosenItem])
                setChosenGroup([])
            }
        }
    }

    const formikFormStudent = useFormik({
        initialValues: {
            search: '',
        },
        onSubmit: values => {
            //
        },
    })

    const handleReset = () => {
        formikFormStudent.resetForm()
    }

    const handleChange = () => {
        setSearching(true)
    }

    useEffect(() => {
        const delay = 300
        const timerId = setTimeout(() => {
            setSearchValue(formikFormStudent.values.search)
        }, delay)

        return () => {
            clearTimeout(timerId)
        }
    }, [
        formikFormStudent.values.search
    ])


    const fetcher = async () => {
        const objQuery = {
            search: searchValue != '' ? searchValue : undefined
        }
        const queryString = qs.stringify(objQuery)
        setIsLoadTable(true)
        const response: any = await delayResult(
            callApiConfigError(
                `${paths.api_teacher_classes}/${data.id}/students?${queryString}`
            ), 400)
        setIsLoadTable(false)
        if (response.result && response.data) {
            if (queryString == '') {
                setStudentDataOG(response.data)
            }
            setStudentData(response.data)
        }
    }

    useEffect(() => {
        fetcher().finally()
    }, [searchValue])

    const isSearchEmpty = useMemo(() => {
        if (!studentData || studentData.length === 0 && !searching) {
            return false
        }
        return true
    }, [searching, studentData])

    return (
        <div className={styles.container}>
            <div className={styles.typeSelectSection}>
                <div
                    className={classNames(styles.typeItem, {
                        [styles.active]: selectedType === 'all'
                    })}

                    onClick={() => handleTypeChange('all')}
                >
                    <div className={styles.radio}>
                        <input
                            id="all"
                            name="type"
                            type="radio"
                            checked={selectedType === 'all'}
                            onChange={() => handleTypeChange('all')}
                        />
                    </div>
                    <div className={styles.textInfo}>
                        <span className={styles.title}>{t('modal-send-unit-test')['send-to-all']}</span>
                        <span className={styles.description}>{t("modal-send-unit-test")["send-to-all-definition"]}</span>
                    </div>
                </div>
                <div
                    className={classNames(styles.typeItem, {
                        [styles.active]: selectedType === 'group'
                    })}
                    onClick={() => handleTypeChange('group')}
                >
                    <div className={styles.radio}>
                        <input
                            id="group"
                            name="type"
                            type="radio"
                            checked={selectedType === 'group'}
                            onChange={() => handleTypeChange('group')}
                        />
                    </div>
                    <div className={styles.textInfo}>
                        <span className={styles.title}>{t('modal-send-unit-test')['send-to-group']}</span>
                        <span className={styles.d}>{t('modal-send-unit-test')['send-to-group-definition']}</span>
                    </div>
                </div>
                <div
                    className={classNames(styles.typeItem, {
                        [styles.active]: selectedType === 'individual'
                    })}
                    onClick={() => handleTypeChange('individual')}
                >
                    <div className={styles.radio}>
                        <input
                            id="individual"
                            name="type"
                            type="radio"
                            checked={selectedType === 'individual'}
                            onChange={() => handleTypeChange('individual')}
                        />
                    </div>
                    <div className={styles.textInfo}>
                        <span className={styles.title}>{t('modal-send-unit-test')['send-to-student']}</span>
                        <span className={styles.description}>{t('modal-send-unit-test')['send-to-student-definition']}</span>
                    </div>
                </div>
            </div>
            <div className={styles.contentSection}>
                {selectedType === 'all' && (
                    <div className={styles.textConfirm}>
                        <p
                            dangerouslySetInnerHTML={{
                                __html: t(t('modal-send-unit-test')['send-to-all-description'], [`<span style="font-weight: 700; word-break: break-word;">${data.name}</span>`])
                            }}
                        ></p>
                    </div>
                )}
                {selectedType === 'group' && (
                    <>
                        {groupData.length > 0 && (
                            <>
                                <div className={styles.textHeader}>
                                    <span>{t('modal-send-unit-test')['select-group']}</span>
                                </div>
                                <div className={styles.groupGrid}>
                                    {groupData.map((group: any, groupIndex: number) => {
                                        return (
                                            <div className={styles.groupItem} key={group.id}>
                                                <div className={styles.header}>
                                                    <div className={styles.groupName} title={group.name}>
                                                        {group.name}
                                                    </div>
                                                    <div className={styles.checkBoxPosition}>
                                                        <CheckBoxCustom
                                                            checked={chosenGroup.some((chosenItem: any) => chosenItem.id === group.id)}
                                                            onChange={() => handleChooseGroup(group.id)}
                                                        />
                                                    </div>

                                                </div>
                                                <div className={styles.groupTotalStudent}>
                                                    {t('modal-send-unit-test')['student-quantity']}: {group.totalStudent}
                                                </div>
                                            </div>
                                        )
                                    })}
                                </div>
                            </>
                        )}
                        {groupData.length == 0 &&
                            <div className={styles.textHeader}>
                                <span>{t('modal-send-unit-test')['no-data-group']}</span>
                            </div>
                        }
                    </>
                )}
                {selectedType === 'individual' && (
                    <>
                        <div className={styles.textHeader}>
                            <span>{t('modal-send-unit-test')['select-student']}</span>
                            {chosenStudentList.length > 0 && (
                                <div className={styles.textTotalSelect}>
                                    <span
                                        dangerouslySetInnerHTML={{
                                            __html: t(t('modal-send-unit-test')['count-selected-student'], [`<span style="color: #EF4E2B;">${chosenStudentList.length}/${studentDataOG.length}</span>`])
                                        }}
                                    ></span>
                                </div>
                            )}
                        </div>
                        <FormikContext.Provider value={formikFormStudent}>
                            <BaseForm className={styles.studentSearch}>
                                <InputTextField
                                    name="search"
                                    autoFocus={undefined}
                                    className={styles.aInputWithLabel}
                                    type={'text'}
                                    label=""
                                    maxLength={100}
                                    onChange={handleChange}
                                    disabled={false}
                                    hideErrorMessage={undefined}
                                    iconLeft={<SearchIcon />}
                                    iconRight={undefined}
                                    placeholder={t('modal-send-unit-test')['placeholder-search-student']}
                                    autoComplete="off"
                                    onlyNumber={false}
                                />
                                <ButtonCustom
                                    onClick={handleReset}
                                    iconRight={<ResetIcon />}
                                    className={classNames(styles.aButtonReset, {
                                        [styles.disabled]:
                                            formikFormStudent.values.search?.length === 0
                                    })}
                                >
                                    {t('common')['reset']}
                                </ButtonCustom>
                            </BaseForm>
                        </FormikContext.Provider>
                        <div id="tableContainer" className={styles.tableStudent}>
                            <table>
                                <thead>
                                    <tr className={styles.rowSticky}>
                                        <th></th>
                                        <th>{t('common')['student-code']}</th>
                                        <th>{t('common')['student-name']}</th>
                                        <th>{t('phone')}</th>
                                    </tr>
                                </thead>
                                {!isLoadTable && studentData.length > 0 && (
                                    <tbody>
                                        {studentData.map((student: any, studentIndex: number) => {
                                            return (
                                                <tr key={student.id}>
                                                    <td>
                                                        <div
                                                            style={{
                                                                height: '100%',
                                                                display: 'flex',
                                                                alignItems: 'center',
                                                                justifyContent: 'center',
                                                            }}
                                                        >
                                                            <CheckBoxCustom
                                                                checked={chosenStudentList.some((chosenItem: any) => chosenItem.id === student.id)}
                                                                onChange={() => handleChooseStudent(student.id)}
                                                            />
                                                        </div>
                                                    </td>
                                                    <td title={student.student_code}>{student.student_code || '---'}</td>
                                                    <td title={student.full_name}>{student.full_name}</td>
                                                    <td title={student.phone}>{student.phone}</td>
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                )}
                            </table>
                            {isLoadTable && (
                                <div className={styles.tableNoData}>
                                    <Loader content="loading..." vertical />
                                </div>
                            )}
                            {!isLoadTable && studentData.length == 0 && (
                                <div className={styles.tableNoData}>
                                    <img
                                        className={styles.banner}
                                        src={
                                            isSearchEmpty
                                                ? '/images/collections/clt-emty-result.png'
                                                : '/images/collections/clt-emty-template.png'
                                        }
                                        alt="banner"
                                    />
                                    <span className={styles.text}>
                                        {isSearchEmpty ? (
                                            <>{t('no-field-data')['search-empty']}</>
                                        ) : (
                                            <>{t('no-field-data')['no-data-learner']}</>
                                        )}
                                    </span>
                                </div>
                            )}
                        </div>
                    </>
                )}
            </div>
        </div>
    )
}

export default SelectTargetStep