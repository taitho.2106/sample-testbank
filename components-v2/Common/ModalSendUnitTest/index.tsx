import React, { useContext, useEffect, useState } from 'react'

import classNames from 'classnames'
import dayjs from 'dayjs'
import { useRouter } from 'next/router'
import { Modal } from 'rsuite'

import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button'
import useNoti from '@/hooks/useNoti'
import { paths as path } from '@/interfaces/constants';
import { WrapperContext } from '@/interfaces/contexts'
import { paths } from 'api/paths'
import { callApiConfigError } from 'api/utils'

import styles from './ModalSendUnitTest.module.scss'
import { ProgressSendUnitTest } from './ProgressSendUnitTest'
import SelectTargetStep from './SelectTargetStep'
import SelectUnitTestStep from './SelectUnitTestStep'
import useTranslation from "@/hooks/useTranslation";

type ModalProps = {
    isActive?: boolean
    className?: string
}

type SentUnitTest = {
    key: number
    id: number
    name: string
    unit_type: number
    time: number
    start_date: string
    end_date: string
}

export default function ModalSendUnitTest({
    isActive = false,
    className = '',
}: ModalProps) {
    const { t } = useTranslation();
    const { globalModal } = useContext(WrapperContext)
    const content = globalModal.state.content || {}
    const { getNoti } = useNoti()
    const { push } = useRouter()
    const [unitTestList, setUnitTestList] = useState([] as any[])
    const classData = globalModal.state?.dataClass

    const [slider, setSlider] = useState(0)
    const [max, setMax] = useState(1)
    const [nextStep, setNextStep] = useState(false) // nextStep == false là đang ở trang đầu/mặc định
    const [error, setError] = useState<{ id: number; fields: string[] }[]>([])
    const [isAddingMore, setIsAddingMore] = useState(true)
    const [selectedType, setSelectedType] = useState('all')

    const onClickStep = (value: any) => {
        const emptyFields = checkSelectTestEmptyFields(selectTest)
        if (value == slider || value == 0) return
        setIsAddingMore(false)
        if (emptyFields.length > 0) {
            setError(emptyFields)
        }
        else {
            setError([])
            setSlider(value)
            setNextStep(!nextStep)
            setMax(2)
        }
    }

    const handleStep = () => {
        const emptyFields = checkSelectTestEmptyFields(selectTest)
        setIsAddingMore(false)
        if (emptyFields.length > 0) {
            setError(emptyFields)
        }
        else {
            setError([])
            setMax(2)
            setNextStep(!nextStep)
            if (!nextStep) setSlider(2)
            else setSlider(1)
        }
    }

    const handleOnclose = () => {
        globalModal.setState(null)
        setNextStep(false)
        setSlider(0)
    }

    useEffect(() => {
        const fetchData = async () => {
            const response: any = await callApiConfigError(
                `${paths.api_teacher_unit_tests}?grade=${classData.grade}`
            )
            const responseIE: any = await callApiConfigError(
                `${paths.api_teacher_unit_tests}?grade=IE`
            )
            if (response?.result && response.data) {
                if (responseIE?.result && responseIE.data) {
                    setUnitTestList([...response.data, ...responseIE.data])
                } else {
                    setUnitTestList(response.data)
                }
            }
        }
        fetchData()
    }, [])

    const [selectTest, setSelectTest] = useState<SentUnitTest[]>([
        {
            key: (new Date()).getTime(),
            id: 0,
            name: '',
            unit_type: -1,
            time: 0,
            start_date: '',
            end_date: '',
        }
    ])

    // console.log('selectTest---', selectTest)

    const checkSelectTestEmptyFields = (selectTest: any[]) => {
        const emptyFields: any[] = [];
        selectTest.forEach((test: any, index: number) => {
            const fields = []

            const currentDate = new Date()
            const tempEndDate = new Date(test?.end_date)
            const isEndDateBeforeCurrent = dayjs(tempEndDate).isBefore(currentDate, "minute")

            if (isEndDateBeforeCurrent && test.start_date != '' && test.end_date != '') {
                fields.push('start_date')
                fields.push('end_date')
                fields.push('invalid_date')
            }
            if (test.name === '') {
                fields.push('name')
            }
            if (test.unit_type === -1) {
                fields.push('unit_type')
            }
            if (test.start_date === '') {
                fields.push('start_date')
            }
            if (test.end_date === '') {
                fields.push('end_date')
            }
            if (fields.length > 0) {
                emptyFields.push({ index, fields })
            }
        })
        return emptyFields
    }

    const [chosenGroup, setChosenGroup] = useState([])
    const [chosenStudentList, setChosenStudentList] = useState([])

    const handleSendUnitTest = async () => {
        const unitTests = selectTest.map((item) => {
            const { start_date, end_date, unit_type, id } = item
            return {
                start_date,
                end_date,
                unit_type,
                unit_test_id: id,
            }
        })
        const body: any = {
            unitTests,
        }
        if (chosenGroup.length > 0) {
            const groups = chosenGroup.map(group => group.id)
            body.groups = groups
        }
        if (chosenStudentList.length > 0) {
            const students = chosenStudentList.map(student => student.id)
            body.students = students
        }
        const response: any = await callApiConfigError(
            `${paths.api_teacher_classes}/${classData.id}/unit-test`,
            'post',
            'token',
            body
        )
        if (response.result) {
            handleOnclose()
            getNoti('success', 'topCenter', t(t('common-get-noti')['assign-unitTest-succeed'], [classData.name]))
            push({
                pathname: `${path.classes}/${classData.id}`,
                query: {
                    step: 3,
                    page: 1,
                }
            })
            if (content?.onSubmit) {
                content.onSubmit()
            }
        }
    }

    const onAddMore = () => {
        setIsAddingMore(true)
        if (selectTest) {
            setSelectTest([
                ...selectTest,
                {
                    key: (new Date()).getTime(),
                    id: 0,
                    name: '',
                    unit_type: -1,
                    time: 0,
                    start_date: '',
                    end_date: ''
                }
            ])
        }
    }

    // console.log({error})
    useEffect(() => {
        const emptyFields = checkSelectTestEmptyFields(selectTest)
        if (!isAddingMore && error.length > 0) {
            setError(emptyFields)
        }
    }, [selectTest])

    return (
        <Modal
            className={classNames(styles.modalSendUnitTest, className)}
            open={isActive}
            style={{
                overflow: 'hidden',
                background: 'rgba(0, 0, 0, 0.2)',
            }}
            backdrop={false}
        >
            <Modal.Body>
                <div className={styles.header}>
                    <div className={styles.title}>
                        <span >
                            {t('modal-send-unit-test')['title']}
                        </span>
                    </div>
                    <div className={styles.progress}>
                        <ProgressSendUnitTest
                            max={max}
                            value={slider}
                            onClickStep={onClickStep}
                            setValue={setSlider}
                        />
                    </div>
                </div>
                <div className={styles.body}>
                    {!nextStep && (
                        <SelectUnitTestStep
                            dataOptions={unitTestList}
                            selectTest={selectTest}
                            setSelectTest={setSelectTest}
                            error={error}
                            onAddMore={onAddMore}
                            setIsAddingMore={setIsAddingMore}
                        />
                    )}
                    {nextStep && (
                        <SelectTargetStep
                            data={classData}
                            selectedType={selectedType}
                            setSelectedType={setSelectedType}
                            chosenGroup={chosenGroup}
                            setChosenGroup={setChosenGroup}
                            chosenStudentList={chosenStudentList}
                            setChosenStudentList={setChosenStudentList}
                        />
                    )}
                </div>
                <div className={styles.footer}>
                    <ButtonCustom className={styles.cancel} type='reset' onClick={handleOnclose}>
                        {t('common')['cancel']}
                    </ButtonCustom>
                    <ButtonCustom
                        type={!nextStep ? "primary" : "outline"}
                        onClick={handleStep}
                    >
                        {!nextStep ? t('common')['next-btn'] : t('common')['back']}
                    </ButtonCustom>
                    {nextStep && (
                        <ButtonCustom
                            className={classNames(styles.submit, {
                                [styles.disabled]: selectedType == 'group' && chosenGroup.length == 0 || selectedType == 'individual' && chosenStudentList.length == 0
                            })}
                            buttonType={'submit'}
                            onClick={handleSendUnitTest}
                            disable={selectedType == 'group' && chosenGroup.length == 0 || selectedType == 'individual' && chosenStudentList.length == 0}
                        >
                            {t('classes')['assign-unit-test']}
                        </ButtonCustom>)}
                </div>
            </Modal.Body>
        </Modal>
    )
}