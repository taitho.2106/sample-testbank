import React, { CSSProperties } from 'react'

import IconStep1 from '@/assets/icons/step1.svg'
import IconStep2 from '@/assets/icons/step2.svg'

import styles from './ProgressSendUnitTest.module.scss'
import useTranslation from "@/hooks/useTranslation";

interface Props {
    max: number
    value: number
    setValue: (val: number) => void
    onClickStep: (val: number) => void
}

export const ProgressSendUnitTest = ({
    max,
    value,
    setValue,
    onClickStep,
}: Props) => {
    const { t } = useTranslation();

    const steps = [
        { id: 1, value: 1, name: t('modal-send-unit-test')['select-unit-test'], icon: <IconStep1 /> },
        { id: 2, value: 2, name: t('modal-send-unit-test')['select-target'], icon: <IconStep2 /> },
    ]
    return (
        <div
            className={styles.progressBar}
            data-value={(50 * 100) / steps.length}
            style={
                { '--value': `${((max - 1) * 100) / (steps.length - 1)}%` } as CSSProperties
            }
        >
            {steps.map((item) => (
                <div
                    key={item.id}
                    className={styles.progressItem}
                    data-active={item.value === value}
                    data-disabled={value === 0}
                    onClick={() =>
                        item.value !== value &&
                        onClickStep(item.value)
                    }
                >
                    <div className="badge">{item.icon}</div>
                    <span className="title">{item.name}</span>
                </div>
            ))}
        </div>
    )
}