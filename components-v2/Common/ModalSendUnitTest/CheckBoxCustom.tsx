import classNames from 'classnames'
import { Tooltip, Whisper } from 'rsuite'

import { DefaultPropsType } from '../../../interfaces/types'
import styles from './CheckBoxCustom.module.scss'

interface Props extends DefaultPropsType {
    checked?: boolean
    disabled?: boolean
    name?: string
    onChange?: (e: any) => void
    toolTip?: string
}

export const CheckBoxCustom = ({
    className = '',
    checked = false,
    disabled = false,
    name = '',
    style,
    onChange = () => null,
    toolTip,
}: Props) => {
    return (
        <Whisper
            placement="topStart"
            trigger="hover"
            speaker={
                <Tooltip style={{ visibility: toolTip ? "visible" : "hidden" }}>{toolTip}</Tooltip>
            }
        >
            <div
                className={classNames(styles.aCheckbox, className)}
                data-checked={checked}
                data-disabled={disabled}
                style={style}
            >
                <input
                    type="checkbox"
                    name={name}
                    checked={checked}
                    disabled={disabled}
                    onChange={(e: any) => onChange(e)}
                />
            </div>
        </Whisper>
    )
}
