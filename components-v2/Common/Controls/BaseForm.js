import React from "react";
import {Form, useFormikContext} from "formik";

const BaseForm = ({className='', children, id=''}) => {
    const formikBag = useFormikContext();

    return (
        <Form className={className} onSubmit={formikBag.handleSubmit} id={id}>
            {children}
        </Form>
    );
}

export default BaseForm;