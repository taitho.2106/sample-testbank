import React from "react";

import styles from "./Loading.module.scss";

const CircleLoading = ({width = 40, height = 40, color = 'white'}) => {
    return (
        <div
            style={{
                width,
                height,
                borderColor: color,
                borderRightColor: 'transparent',
            }}
            className={styles.circleLoading}
        />
    )
}

export { CircleLoading }

export default CircleLoading
