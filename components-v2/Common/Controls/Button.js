import React from 'react'
import classNames from 'classnames'

import styles from './Button.module.scss'

const Button = React.forwardRef(({
    children,
    className,
    disable,
    type = 'primary',
    buttonType = "button",
    iconLeft,
    iconRight,
    onClick,
    ...props
}, ref) => {
    return (
        <button
            disabled={disable}
            onClick={onClick}
            className={classNames(styles.button, styles[type], className)}
            type={buttonType}
            ref={ref}
            {...props}
        >
            {iconLeft}
            {children}
            {iconRight}
        </button>
    )
})

export default Button