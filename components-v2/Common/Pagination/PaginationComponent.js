import React from 'react'

import Pagination from 'rc-pagination'

import 'rc-pagination/assets/index.css'
import ChevronIcon from '../../..//assets/icons/arrow.svg'
import styles from './PaginationComponent.module.scss'
function PaginationComponent({
  total,
  pageSize,
  current,
  showTotal,
  onChange,
  showSizeChanger = false,
}) {
  const PrevNextArrow = (current, type, originalElement) => {
    if (type === 'prev') {
      return (
        <>
          <ChevronIcon />
        </>
      )
    }
    if (type === 'next') {
      return (
        <>
          <ChevronIcon />
        </>
      )
    }
    return originalElement
  }
  return (
    <Pagination
      total={total}
      pageSize={pageSize}
      current={current}
      onChange={onChange}
      showTotal={showTotal}
      showSizeChanger={showSizeChanger}
      itemRender={PrevNextArrow}
      className={styles.paginationWrapper}
      showTitle={false}
    />
  )
}

export default PaginationComponent
