import React, { useContext } from 'react'

import { Modal } from 'rsuite'

import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button';

import styles from './ModalDelete.module.scss';
import classNames from 'classnames';
import { WrapperContext } from '@/interfaces/contexts';
import useTranslation from "@/hooks/useTranslation";
type Props = {
    isActive?:boolean
    className?:string
    style?:any
}
export default function ModalDelete({isActive = false, className='', style={}}:Props) {
    const { t } = useTranslation();
    const { globalModal } = useContext(WrapperContext)
    const content = globalModal.state.content || {}
    const type = globalModal.state.type || ''
    const handleClose = () => {
        if(content?.onClose){
            content.onClose()
        }else{
            globalModal.setState(null)
        }
    }
    const handleSubmit = () => {
        if(content?.onSubmit){
            content.onSubmit()
        }
    }
    return (
        <Modal
            className={classNames(styles.wrapperModal, className)}
            open={isActive}
            backdrop={false}
            style={style}
            onClose={handleClose}
        >
            <Modal.Body>
                <div className={styles.contentBox}>
                    {type === 'delete-student-into-class' && 
                    <>
                        <span className={styles.title}>{t('modal-delete-student-into-class')['title']}</span>
						<span className={styles.textContent} dangerouslySetInnerHTML={{__html: t(t('modal-delete-student-into-class')['description'], [content?.nameStudent,`<span style="word-break: break-word;"> ${content?.class}</span>`])}}></span>
                        <span className={styles.textContent}>{t('modal-delete-student-into-class')['confirm-msg']}</span>
                    </>}
                    {type === 'delete-student-into-teacher' && 
                    <div className={styles.boxDeleteWithTeacher}>
                        <span className={styles.title}>{t('modal-delete-student-into-teacher')['title']}</span>
						<span className={styles.textContent} dangerouslySetInnerHTML={{__html: t(t('modal-delete-student-into-teacher')['description'],[content.nameStudent])}}></span>
                        <span className={styles.textContent}>{t('modal-delete-student-into-teacher')['confirm-msg']}</span>
                    </div>}
                    <div className={styles.btnBox}>
                        <ButtonCustom className={styles.cancel} onClick={handleClose} type='outline'>{content?.textClose || t('common')['cancel']}</ButtonCustom>
                        <ButtonCustom onClick={handleSubmit}>{content?.textSubmit ||t('common')['submit-btn']}</ButtonCustom>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    )
}
