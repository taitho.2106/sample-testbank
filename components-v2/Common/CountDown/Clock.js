import React from "react";

import useCountDown from "../../../hooks/useCountDown";
import styles from "./index.module.scss";

function Clock({ totalTime }) {
	const { hours, minutes, seconds } = useCountDown(totalTime);
	
	return (
		<div className={styles.clock}>
			<div className={styles.timer}>
				<span>
					{formatNumber(Math.max(hours, 0))}
				</span>
			</div>
			<div className={styles.separator}><span>:</span></div>
			<div className={styles.timer}>
				<span>
					{formatNumber(Math.max(minutes, 0))}
				</span>
			</div>
			<div className={styles.separator}><span>:</span></div>
			<div className={styles.timer}>
				<span>
					{formatNumber(Math.max(seconds, 0))}
				</span>
			</div>
		</div>
	);
}

export default Clock;

function formatNumber(number) {
	return `0${number}`.slice(-2);
}