import React, { useEffect, useState } from 'react'

import classNames from 'classnames';

import styles from './SkeletonLoading.module.scss';

type Prop = {
    className?: string,
    children?:any,
    height?:string,
    width?:string,
    borderRadius?:string
}
export default function SkeletonLoading({className,children, height, width, borderRadius = '8px'}:Prop) {
    const style ={height, width, borderRadius};
   
    return (
        <div className={classNames(styles.wrapperSkeletion,className)} style={{...style}}>
            {children}
        </div>
    )
}
