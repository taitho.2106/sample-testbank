import React from 'react';

import ComingSoonImage from '@/assets/icons/coming-soon.svg'
import useTranslation from '@/hooks/useTranslation';

import styles from './index.module.scss'

const ComingSoonComponent = () => {
    const { t } = useTranslation()
    return (
        <div className={styles.container}>
            <ComingSoonImage/>
            <div className={styles.contentText}>
                <span className={styles.title}>{t('coming-soon-page')['title']}</span>
                <span>{t('coming-soon-page')['team']}<span
                    className={styles.textHighlight}>i-Test</span>{t('coming-soon-page')['action']}</span>
            </div>
        </div>
    );
};

export default ComingSoonComponent;