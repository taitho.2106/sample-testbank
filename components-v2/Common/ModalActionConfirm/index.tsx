import React, { useContext } from 'react'

import classNames from 'classnames'
import { Modal } from 'rsuite'

import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button'
import { WrapperContext } from '@/interfaces/contexts'

import styles from './ModalActionConfirm.module.scss'
import useTranslation from "@/hooks/useTranslation";

type Props = {
    isActive?: boolean
    className?: string
    style?: any
}

export default function ModalActionConfirm({
    isActive = false,
    className = '',
    style = {}
}: Props) {
    const { t } = useTranslation();
    const { globalModal } = useContext(WrapperContext)
    const content = globalModal.state.content || {}
    const type = globalModal.state.type || ''
    const handleClose = () => {
        if (content?.onClose) {
            content.onClose()
        } else {
            globalModal.setState(null)
        }
    }
    const handleSubmit = () => {
        handleClose()
        if (content?.onSubmit) {
            content.onSubmit()
        }
    }

    return (
        <Modal
            className={classNames(styles.actionConfirmModal, className)}
            open={isActive}
            style={{
                ...style,
                overflow: 'hidden',
            }}
            backdrop={false}
            onClose={handleClose}
        >
            <Modal.Body>
                {type === 'cancel-send-unit-test' &&
                    <>
                        <div className={styles.header}>
                            {t('modal-cancel-send-unittest')['title']} <span style={{wordBreak: 'break-word'}}>{content?.class}</span>
                        </div>
                        <div className={styles.warningText}>
                            <span style={{ fontWeight: 700, wordBreak: 'break-word' }}>
                                {t('modal-cancel-send-unittest')['description']}
                            </span>
                            <ul className={styles.ul}>
                                <li>{t('modal-cancel-send-unittest')['description-1']}</li>
                                <li>{t('modal-cancel-send-unittest')['description-2']}</li>
                            </ul>
                            <p>{t('modal-cancel-send-unittest')['confirm-msg']}</p>
                        </div>
                    </>
                }
                {type === 'delete-class' &&
                    <>
                        <div className={styles.header}>
                            {t('modal-delete-class')['title']}
                        </div>
                        <div className={styles.warningText}>
							<p dangerouslySetInnerHTML={{
                                __html: t(t('modal-delete-class')['warning-msg'], [`<span style="font-weight: 700; word-break: break-word;">${content?.class}</span>`])
                            }}></p>
                            <p>{t('modal-delete-class')['confirm-msg']}</p>
                        </div>
                    </>
                }
                {type === 'set-class-status' &&
                    <>
                        <div className={styles.header}>
                            Xác nhận ngừng kích hoạt lớp học
                        </div>
                        <div className={styles.warningText}>
                            <p>
                            Khi nhấn xác nhận, lớp học <span style={{ fontWeight: 700, wordBreak: 'break-word' }}>{content?.class}</span> sẽ được chuyển sang trạng thái “Ngưng kích hoạt” và tất cả đề thi được giao cho lớp sẽ bị vô hiệu hóa                                
                            </p>
                            <p>
                            Bạn có chắc chắn muốn ngừng kích hoạt lớp học này?
                            </p>
                        </div>
                    </>
                }
                <div className={styles.footer}>
                    <ButtonCustom className={styles.cancel} type='reset' onClick={handleClose}>
                        {t('common')['cancel']}
                    </ButtonCustom>
                    <ButtonCustom
                        className={styles.submit}
                        onClick={handleSubmit}
                    >
                        {content?.textSubmit || t('common')['submit-btn']}
                    </ButtonCustom>
                </div>
            </Modal.Body>
        </Modal>
    )
}