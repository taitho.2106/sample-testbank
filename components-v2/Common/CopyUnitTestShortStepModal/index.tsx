import React, { useContext, useEffect, useState } from 'react'

import classNames from 'classnames'
import { Modal } from 'rsuite'

import useNoti from '@/hooks/useNoti'
import { AppContext, WrapperContext } from '@/interfaces/contexts'
import { callApi } from 'api/utils'

import { DateTimePickerInput } from '../DateTimePickerCustom'
import { InputWithLabel } from '../InputWithLabel/InputWithLabel'
import { SingleSelectPicker } from '../SingleSelectPicker/SingleSelectPicker'
import styles from './CopyUnitTestShortStep.module.scss'
import ga from '@/utils/ga'
import useTranslation from "@/hooks/useTranslation";


type ModalProps = {
  isActive?: boolean
  className?: string
}
export default function CopyUnitTestShortStep({
  isActive = false,
  className = '',
}: ModalProps) {
    const { t } = useTranslation()
    const UNIT_TYPE = [
        { code: '0', display: t('unit-type')['practice'] },
        { code: '1', display: t('unit-type')['exam'] },
    ]
    const { globalModal } = useContext(WrapperContext)
    const { fullScreenLoading } = useContext(AppContext)
    const { getNoti } = useNoti()
    const newDataUnitTest = {
        ...globalModal.state?.dataUnitTest,
        id: null,
        scope: 1,
    }
    const onClose = () => globalModal.state?.onClose()
    const [dataUnitTest, setDataUnitTest] = useState(newDataUnitTest || {})
    const [triggerEndDate, setTriggerEndDate] = useState(null)
    const [error, setError] = useState(false)
    const [textErr, setTextErr] = useState('')
    useEffect(() => {
        let detail: any = dataUnitTest
        if (!detail) detail = {}
        if (detail?.sections) {
          const sections: any = detail.sections.map((item: any) => ({
            section_old_id: item?.id || null,
            parts: item?.parts
              ? item.parts.map((part: any) => ({
                  id: part?.id || null,
                  name: part?.name || '',
                  points: part?.points || 0,
                  questions: part?.questions
                    ? part.questions.map((question: any) => question.id)
                    : [],
                  question_types: part?.question_types || '',
                  total_question: part?.total_question || 0,
                }))
              : [],
            section:  (item?.section || '')?.split(',') || [],
          }))
          detail.sections = sections
          setDataUnitTest({ ...detail })
        }
    },[])
    const handleCancel = () => {
      if(globalModal.state?.onClose){
          globalModal.state?.onClose()
      }
        globalModal.setState(null)
    }
    const handleNameChange = (value:string) => {
        if (value.trim().length === 0) {
            setError(true)
            setTextErr(t('common-get-noti')['template-name-validate'])
        }else if (value.length > 100) {
          setError(true)
          setTextErr(t('copy-unit-test-short-step')['message-test-max'])
        }else{
          setError(false)
          setTextErr('')
        }
         let detail: any = dataUnitTest
         if (!detail) detail = {}
         detail.name = value.trim()
        setDataUnitTest({ ...detail })
    }
    const handleUnitTypeChange = (value:string) => {
         let detail: any = dataUnitTest
         if (!detail) detail = {}
         detail.unit_type = Number(value)
        setDataUnitTest({
          ...detail
        })
        if(value === '1'){
            if (detail.time) {
              const parseTime = detail.time
              if (detail.start_date) {
                handleEndDateChange(
                  new Date(detail.start_date).getTime() + parseTime * 60 * 1000,
                )
                setTriggerEndDate(
                  new Date(
                    new Date(detail.start_date).getTime() +
                      parseTime * 60 * 1000,
                  ),
                )
              }
            }
        }
    }
    
    const handleStartDateChange = (timestamp: number) => {
        // dataUnitTest.start_date = new Date(timestamp).toString()
        let detail:any = dataUnitTest
        if (!detail) detail = {}
        detail.start_date = new Date(timestamp).toString()
        setDataUnitTest({
          ...detail,
        })
        if (detail.unit_type === 1) {
          if (detail.time && timestamp) {
            handleEndDateChange(timestamp + detail.time * 60 * 1000)
            setTriggerEndDate(new Date(timestamp + detail.time * 60 * 1000))
          }
        }
    }
    const handleEndDateChange = (timestamp: number) => {
        // dataUnitTest.end_date = new Date(timestamp).toString()
        let detail: any = dataUnitTest
        if (!detail) detail = {}
         detail.end_date = new Date(timestamp).toString()
        setDataUnitTest({
          ...detail
        })
    }
    const handleCopyUnitTest = async () => {
        if(error) return
        dataUnitTest.start_date =
            Math.floor(
                new Date(dataUnitTest.start_date).getTime() / 1000,
            ) * 1000
        dataUnitTest.end_date =
            Math.floor(
                new Date(dataUnitTest.end_date).getTime() / 1000,
            ) * 1000
        if (dataUnitTest.start_date > dataUnitTest.end_date){
            getNoti(
              'error',
              'topEnd',
              t('common-get-noti')['date-time'],
            )
            return
        }
        globalModal.setState(null)
        fullScreenLoading.setState(true)
          const response: any = await callApi(
            '/api/unit-test',
            'post',
            'token',
            dataUnitTest,
          )
        fullScreenLoading.setState(false)
        if (response?.id && response?.userId) {
            ga.unitTestUse(dataUnitTest);
            globalModal.setState({
                id: 'share-link',
                content: {
                    id: response.id,
                    dataShare: dataUnitTest,
                    title: t('unit-test-create')['title'],
                    userId: response.userId,
                    onClose: onClose,
                },
            })
        }
    }

    return (
      <>
        <Modal
          className={classNames(styles.wrapperModal, className)}
          open={isActive}
          style={{
            overflow: 'hidden',
            background: 'rgba(0, 0, 0, 0.2)',
          }}
          backdrop={false}
        >
          <Modal.Body>
            <div className={styles.boxHeader}>
              <span className={styles.title}>{t('copy-unit-test-short-step')['title']}</span>
              <span className={styles.timer}>
                {t('common')['time']}:{' '}
                <span className={styles.countTimer}>
                  {dataUnitTest?.time} {t('time')['minutes']}
                </span>
              </span>
            </div>
            <div className={styles.description}>
              {t('copy-unit-test-short-step')['description']}
            </div>
            <div className={styles.boxNameUnitTest}>
              <InputWithLabel
                className={classNames(styles.nameUnitTest, {
                  [styles.error]: error,
                })}
                defaultValue={dataUnitTest?.name}
                label={t('common')['unit-test-name']}
                type="text"
                onBlur={handleNameChange}
              />
              {error && <span className={styles.errorText}>{textErr}</span>}
            </div>
            <SingleSelectPicker
              className={styles.selectUnitTest}
              data={UNIT_TYPE}
              label={t('common')['unit-type']}
              onChange={handleUnitTypeChange}
              defaultValue={
                UNIT_TYPE.filter(
                  (item) => item.code == dataUnitTest?.unit_type,
                )[0]
              }
            />
            <div className={styles.boxDateTime}>
              <DateTimePickerInput
                className="midbox time"
                defaultValue={new Date(dataUnitTest?.start_date)}
                label={t('common')['start-date']}
                time={true}
                onChange={handleStartDateChange}
                placement={'topStart'}
              />
              <DateTimePickerInput
                className="midbox time"
                defaultValue={new Date(dataUnitTest?.end_date)}
                label={t('common')['end-date']}
                time={true}
                triggerSetValue={triggerEndDate}
                onChange={handleEndDateChange}
                disabled={dataUnitTest?.unit_type === 1}
                placement={'topStart'}
              />
            </div>
            <div className={styles.boxAction}>
              <button className={styles.cancel} onClick={handleCancel}>
                  {t('common')['cancel']}
              </button>
              <button className={styles.submit} onClick={handleCopyUnitTest}>
                  {t('common')['create-test']}
              </button>
            </div>
          </Modal.Body>
        </Modal>
      </>
    )
}
