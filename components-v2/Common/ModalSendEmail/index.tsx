import React, { useContext } from 'react'

import { FormikContext, useFormik } from 'formik'
import { Modal } from 'rsuite'
import * as Yup from 'yup'

import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button'
import useNoti from '@/hooks/useNoti'
import { AppContext, WrapperContext } from '@/interfaces/contexts'
import { paths } from 'api/paths'
import { callApi } from 'api/utils'

import BaseForm from '../Controls/BaseForm'
import TextAreaField from '../Form/TextAreaField'
import styles from './ModalSendEmail.module.scss'
import { delayResult } from '@/utils/index'
import { useRouter } from 'next/router'
import useTranslation from "@/hooks/useTranslation";


type PropsModal = {
    isActive?:boolean
}
export default function ModalSendEmail({isActive}:PropsModal) {
    const { t } = useTranslation()
    const { globalModal } = useContext(WrapperContext)
    const router = useRouter()
    const { fullScreenLoading } = useContext(AppContext)
    const content = globalModal.state?.content || ''
    const { getNoti } = useNoti()
    const handlerOnClose = () => {
        globalModal.setState(null)
        router.push('/unit-test?m=mine')
    }

     const onSubmit = async (values:any) => {
        const body = {
            emails: values?.email,
            unitTestURL:content
        }
        fullScreenLoading.setState(true)
        const response :any = await delayResult(callApi(paths.api_send_email_student, 'POST', 'token', body), 400)
        fullScreenLoading.setState(false)
        console.log('response', response)
        if(response){
            globalModal.setState(null)
            router.push('/unit-test?m=mine')
            getNoti('success','topCenter',response?.message)
        }
    }

    const validationSchema = () => {
        return Yup.object().shape({
            email: Yup.string().trim()
                .required(t('formikForm')['student-email'])
        })
    }
    const formikBag = useFormik({
      initialValues: { email: ''},
      validationSchema,
      onSubmit,
      validateOnBlur: true,
    })

    return (
        <Modal className={styles.wrapperModal}
            open={isActive}
            backdrop={false}
            onClose={handlerOnClose}
            >
            <Modal.Body>
                <FormikContext.Provider value={formikBag}>
                    <BaseForm className={styles.wrapperContent}>
                        <h5 className={styles.title}>{t('send-email-student')['title']}</h5>
                        <TextAreaField
                            name='email'
                            rows={5}
                            maxLength={null}
                            placeholder={t('send-email-student')['placeholder-textarea']}
                            iconLeft={undefined} 
                            iconRight={undefined} 
                            label={''} 
                            disabled={false} 
                            className={styles.boxEmail} 
                            onChange={undefined} 
                            hideErrorMessage={undefined} 
                            showCountCharacter={false} 
                            textareaStyle={undefined} 
                            autoFocus={false}
                            />
                        <div className={styles.boxBtn}>
                            <ButtonCustom className={styles.cancel} onClick={handlerOnClose}>{t('send-email-student')['action-cancel']}</ButtonCustom>
                            <ButtonCustom className={styles.submit} buttonType={'submit'}>{t('send-email-student')['action-submit']}</ButtonCustom>
                        </div>
                    </BaseForm>
                </FormikContext.Provider>
            </Modal.Body>
        </Modal>
    )
}
