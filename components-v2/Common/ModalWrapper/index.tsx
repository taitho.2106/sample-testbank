import React, { CSSProperties } from "react";

import classNames from 'classnames';
import { Modal } from 'rsuite'

import styles from './ModalWrapper.module.scss';

type ModalWrapper = {
    children?: any
    isOpen?: boolean
    onCloseModal?: () => void
    className?: string
    style?: CSSProperties
    backdrop?:boolean | 'static'
}
export default function ModalWrapper({ 
    children, 
    isOpen = false,
    onCloseModal, 
    className = '',
    style = {} ,
    backdrop = false
}: ModalWrapper) {
    return (
        <Modal
        className={classNames(styles.modalDisDand, className)}
        open={isOpen}
        backdrop={backdrop}
        onClose={onCloseModal}
        style={{
            overflow:'hidden',
            ...style,
        }}
        >
        <Modal.Body>{children}</Modal.Body>
        </Modal>
    )
}
