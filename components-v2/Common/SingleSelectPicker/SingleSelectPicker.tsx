import { ChangeEvent, useEffect, useRef, useState } from 'react'

import classNames from 'classnames'
import { Dropdown, Tooltip, Whisper } from 'rsuite'

import ChevronIcon from '../../../assets/icons/chevron-down.svg'
import { DefaultPropsType, StructType } from 'interfaces/types'
import { removeAccents, removeAccentsWithoutTrim } from 'practiceTest/utils/functions'

import styles from './SingleSelectPicker.module.scss'
import useTranslation from '@/hooks/useTranslation'
interface PropsType extends DefaultPropsType {
  checkValue?: boolean
  data: StructType[]
  disabled?: boolean
  defaultValue?: StructType
  hiddenValue?: boolean
  isMultiChoice?: boolean
  label?: string
  name?: string
  placeholder?: string
  reverseData?: boolean
  triggerReset?: boolean
  toolTip?:string
  placement?: any
  trigger?: any
  onChange?: (val: any) => void
  inputSearchShow?: boolean
  labelSearch?: string
  isProvince?: boolean
  isUserInfo?: boolean
}

export const SingleSelectPicker = ({
  className = '',
  checkValue = true,
  data,
  disabled = false,
  defaultValue = { code: '', display: '' },
  hiddenValue = false,
  isMultiChoice = false,
  label = '',
  name = '',
  placeholder = '',
  reverseData,
  triggerReset = false,
  style,
  toolTip = '',
  placement = 'topStart',
  trigger = 'hover',
  onChange = () => null,
  inputSearchShow = false,
  labelSearch = '',
  isProvince = false,
  isUserInfo = false,
}: PropsType) => {
  const {t} = useTranslation()
  const inputRef = useRef(null)
  const currentItemRef = useRef(null)

  const [filterData, setFilterData] = useState<StructType[]>(data)

  const [isFirstRender, setIsFirstRender] = useState(true)
  const [isFocus, setIsFocus] = useState(false)

  const [code, setCode] = useState(defaultValue.code as string | number)
  const [display, setDisplay] = useState(defaultValue.display)
  const [tmpCode, setTmpCode] = useState(defaultValue.code as string | number)
  const [tmpDisplay, setTmpDisplay] = useState(defaultValue.display)

  const handleItemSelect = (e: MouseEvent, item: StructType) => {
    if (!item.code && isProvince) {
      onChange(0)
      setCode(0)
      setDisplay("")
      return
    }
    if (isMultiChoice) e.stopPropagation()
    setTmpCode(code)
    setTmpDisplay(display)
    setTimeout(() => {
      onChange(item.code)
      setCode(item.code)
      setDisplay(item.display)
    }, 0)
  }

  const handleClickItem = (item: StructType) => {
    if (!item.code && isProvince) {
      onChange(0)
      setCode(0)
      setDisplay("")
      setIsFocus(false);
      return
    }
    
    setTmpCode(code)
    setTmpDisplay(display)
    setTimeout(() => {
      onChange(item.code)
      setCode(item.code)
      setDisplay(item.display)
    }, 0)
    setIsFocus(false);
  }

  useEffect(() => {
    if (defaultValue.code && isProvince) {
      onChange(defaultValue.code)
      setCode(defaultValue.code)
      setDisplay(defaultValue.display)
    }
  }, [defaultValue])

  useEffect(() => {
    setCode(tmpCode)
    setDisplay(tmpDisplay)
  }, [reverseData])

  // use for case reset trigger
  useEffect(() => {
    if (isFirstRender) {
      setIsFirstRender(false)
      return
    }
    setCode('')
    setDisplay('')
  }, [triggerReset])

  useEffect(()=>{
    currentItemRef.current?.scrollIntoView();
  }, [isFocus])

  const handleOnSearch = (event: any) => {
    let filterByDisplay: StructType[] = data
    if (event.target.value && event.target.value.length > 0) {
      filterByDisplay = data.filter((item) => {
        const stringDisplay = removeAccents(item.display.toLowerCase())
        const stringSearch = isProvince ? removeAccentsWithoutTrim(event.target.value.toLowerCase()) : removeAccents(event.target.value.toLowerCase())
        return stringDisplay.includes(stringSearch)
      })
    }
    setFilterData(filterByDisplay)
  }

  const handleOnOpen = () => {
    setIsFocus(true)
    setFilterData([...data])
  }
  const handleOnClose = () => {
    setIsFocus(false)
    if (inputRef.current) {
      inputRef.current.value = ""
      setFilterData([...data])
    }
  }

  return (
    <Whisper
      placement={placement}
      trigger={trigger}
      speaker={
        <Tooltip style={{ visibility: toolTip ? 'visible' : 'hidden' }}>
          {toolTip}
        </Tooltip>
      }
    >
      <div
        className={classNames(styles[`a-select-picker`], className, {
          [styles['is-province']]: isProvince,
        })}
        data-focus={isFocus}
        style={style}
      >
        <input name={name} type="hidden" value={code} />
        {label && (
          <label className={classNames({[styles["active"]]: isFocus || (checkValue && display)})}>
            <span>
              {isFocus || (checkValue && display) ? label : placeholder || label}
            </span>
          </label>
        )}

        {
          !isProvince && <img
            className={styles["a-select-picker__caret"]}
            src="/images/icons/ic-chevron-down-dark.png"
            alt="toggle"
          />
        }

        
        <Dropdown
        className={classNames(styles[`a-select-picker__dropdown`],{
              [styles["hidden"]]:data.length == 0,
              [styles['focus']]:isFocus
          })}
          open={isFocus}
          noCaret={isProvince}
          placement={isUserInfo ? 'topEnd' : "bottomStart"}
          disabled={disabled}
          style={{height: isProvince ? "100%" : "auto"}}
          title={hiddenValue ? '' : display}
          onOpen={handleOnOpen}
          onClose={handleOnClose}
          onSelect={(eventKey: any, e: any) => handleItemSelect(e, eventKey)}
        >
          {inputSearchShow ? (
            <div className={styles.searching}>
              <img
                className={styles.image}
                src="/images/icons/ic-search-dark.png"
                alt="search"
              />
              <input
                ref={inputRef}
                type={'text'}
                onChange={handleOnSearch}
                placeholder={labelSearch}
              />
            </div>
          ) : (
            ''
          )}
          <div className={styles.dropdownContent}>
            {isProvince ? filterData.map((item) => (
              <div onClick={() => {handleClickItem(item)}} ref={item.code === code ? currentItemRef : null} className={classNames({
                [styles.isSelected]: item.code === code,
                "rs-dropdown-item": true,
                
                })} key={item.code}>
                {item.display}
              </div>
            )) : filterData.map((item) => (
              <Dropdown.Item ref={item.code === code ? currentItemRef : null} className={item.code === code ? styles.isSelected : ""} key={item.code} eventKey={item}>
                {item.display}
              </Dropdown.Item>
            ))}
          </div>
          {filterData.length == 0 && (
          <span style={{padding: '5px 15px', fontWeight: '500'}}>
              {t('no-field-data')['empty-title']}
            </span>
          )}
        </Dropdown>
        {isProvince && <ChevronIcon style={isUserInfo && isFocus ? { position: "absolute", right: 13, top: 17, transform: "rotate(180deg)", transition: ".2s" } : { position: "absolute", right: 13, top: 17, transform: "rotate(0deg)", transition: ".2s" }} />}
      </div>
    </Whisper>
  )
}
