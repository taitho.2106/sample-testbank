import { useEffect, useState } from 'react'

import classNames from 'classnames';
import { DatePicker, Tooltip, Whisper } from 'rsuite'

import { formatDate } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'
import styles from './DateTimePickerInput.module.scss'

interface Props extends DefaultPropsType {
  defaultValue?: string | Date | null
  disabled?: boolean
  label: string
  name?: string
  placement?: any
  time?: boolean
  triggerReset?: boolean
  triggerSetValue?: Date | null
  onChange?: (val: number) => void
  toolTip?: string
  valueProp?: string | Date | null
  onlyDate?: boolean
  disabledDate?: (date?: Date) => boolean
  disabledMinutes?: (minute: number, date: Date) => boolean
  disabledHours?: (hour: number, date: Date) => boolean
}

export const DateTimePickerInput = ({
  className = '',
  defaultValue = '',
  disabled = false,
  label,
  name = '',
  placement,
  time = false,
  triggerReset,
  triggerSetValue,
  style,
  onChange,
  toolTip,
  valueProp,
  onlyDate = false,
  disabledDate,
  disabledMinutes,
  disabledHours,
}: Props) => {
  const [isFirstRender, setIsFirstRender] = useState(true)
  const [isFocus, setIsFocus] = useState(false)
  const [value, setValue] = useState(
    defaultValue && !onlyDate ? formatDate(defaultValue, time) : 
    defaultValue && onlyDate ? formatDate(defaultValue, false) : '',
  )
  const [tmpValue, setTmpValue] = useState(
    defaultValue ? new Date(`${defaultValue}`) : new Date(),
  )

  const dateValue = valueProp ? new Date(`${valueProp}`) : new Date()

  const HandleChange = (date: Date) => {
    if(disabledDate && disabledDate(date)){
      return
    }
    const d = new Date(date)
    const timestamp = d.getTime()
    setTmpValue(date)
    setValue(formatDate(date, time))
    if (onChange) onChange(timestamp)
  }

  // use for case reset trigger
  useEffect(() => {
    if (isFirstRender) {
      setIsFirstRender(false)
      return
    }
    if (onChange) onChange(null)
    setValue('')
    setTmpValue(new Date())
  }, [triggerReset])

  useEffect(() => {
    if (triggerSetValue) HandleChange(triggerSetValue)
  }, [triggerSetValue])

  return (
      <Whisper
          placement="topStart"
          trigger="hover"
          speaker={
              <Tooltip style={{ visibility: toolTip ? 'visible' : 'hidden' }}>
                  {toolTip}
              </Tooltip>
          }
      >
          <div
              className={classNames(styles.aDateInput, className, {
                [styles.disabledInput]:disabled
              })}
              style={style}
          >
              <div
                  className={styles["a-date-input__cover"]}
                  data-active={value ? true : false}
                  data-disabled={disabled}
                  data-focus={isFocus}
              >
                  <label>
                      <span>{label}</span>
                  </label>
                  {value && (
                      <div className={styles["__value"]} data-disabled={disabled}>
                          {value}
                      </div>
                  )}
                  <img
                      className={styles["__toggle"]}
                      src="/images/icons/ic-calendar-dark.png"
                      alt="calendar"
                  />
              </div>
              <DatePicker
                  className={styles["a-date-input__picker"]}
                  cleanable={false}
                  disabled={disabled}
                  format={time ? 'yyyy-MM-dd HH:mm' : 'yyyy-MM-dd'}
                  name={name}
                  // value={tmpValue}
                  value={valueProp ? dateValue : tmpValue}
                  placement={placement || 'bottomEnd'}
                  onEnter={() => setIsFocus(true)}
                  onExit={() => setIsFocus(false)}
                  onSelect={(date: Date) => HandleChange(date)}
                  onNextMonth={(date: Date) => HandleChange(date)}
                  onPrevMonth={(date: Date) => HandleChange(date)}
                  disabledDate={disabledDate}
                  disabledMinutes={disabledMinutes ? disabledMinutes : () => false}
                  disabledHours={disabledHours ? disabledHours : () => false}
              />
          </div>
      </Whisper>
  )
}
