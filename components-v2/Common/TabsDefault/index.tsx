import React from 'react'

import styles from './TabsDefault.module.scss';
import classNames from 'classnames';

type PropsTabs = {
  data: Data[]
  onClickStep?: (index: number) => void
  step?: number
  className?: string
}
type Data = {
    code?:number
    groupName?:string
    iconLeft?:any
    iconRight?:any
}
export default function TabsDefault({data = [], onClickStep, step = 1, className =''}:PropsTabs) {
  return (
    <div className={classNames(styles.tabsWrapper, className)}>
        {data.map((item) => {
            return (
            <div
                key={item.code}
                className={classNames(styles.boxItem, {
                [styles.active]: step === item?.code,
                })}
                onClick={() => onClickStep(item?.code)}
            >
                {item.iconLeft && <div className={styles.iconLeft}>{item.iconLeft}</div>}
                <span>{item.groupName}</span>
                {item.iconRight && <div className={styles.iconRight}>{item.iconRight}</div>}
            </div>
            )
        })}
    </div>
  )
}
