import React from "react";

import Carousel from "nuka-carousel";

const CustomCarousel = ({
                            children,
                            slidesToScroll = 1,
                            slidesToShow = 1,
                            isInfinity,
                            autoPlay,
                            withoutControls,
                            renderTopLeftControls,
                            renderBottomCenterControls,
                            renderCenterLeftControls,
                            renderCenterRightControls,
                            cellSpacing = 40,
                            dragging,
                            ...props
                        }) => {

    return (
        <Carousel
            ref={props.carouselRef}
            cellSpacing={cellSpacing}
            slidesToScroll={slidesToScroll}
            slidesToShow={slidesToShow}
            autoplay={autoPlay}
            wrapAround={isInfinity}
            withoutControls={withoutControls}
            renderTopLeftControls={renderTopLeftControls}
            renderBottomCenterControls={renderBottomCenterControls}
            renderCenterLeftControls={renderCenterLeftControls}
            renderCenterRightControls={renderCenterRightControls}
            dragging={dragging}
            {...props}>
            {children}
        </Carousel>
    )
}

export default CustomCarousel;