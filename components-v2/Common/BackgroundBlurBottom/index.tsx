import React, { useEffect, useRef, useState } from 'react'

import classNames from 'classnames';

import styles from './BackgroundBlurBottom.module.scss';
type Type = {
    className?: string
    defaultRef?: HTMLElement
    paddingBottom?:number
}
export default function BackgroundBlurBottom({className ='' ,defaultRef, paddingBottom = 0}:Type) {
    const elementRef = useRef<HTMLDivElement>(null)
    const [isBlur, setIsBlur] = useState(true)
    useEffect(() => {
        const element = defaultRef || elementRef?.current?.parentElement?.children[0]
        function handleScroll() {
          const { scrollTop, scrollHeight, clientHeight } = element;
          const isScroll = Math.abs(scrollHeight - clientHeight - scrollTop ) > 1 + paddingBottom
          setIsBlur(isScroll)
        }
      
          element?.addEventListener("scroll", handleScroll);
      
          return () => {
            element?.removeEventListener("scroll", handleScroll);
          };
    } ,[])

  return (
    isBlur && <div className={classNames(styles.blurSticky, className)} ref={elementRef}></div>
  )
}
