import React, { Fragment, useContext, useEffect, useMemo, useRef, useState } from 'react'

import classNames from 'classnames'
import { FormikContext, useFormik } from 'formik'
import { useRouter } from 'next/router'
import qs from "qs";
import {
    FaPencilAlt,
    FaTrashAlt,
} from 'react-icons/fa'
import { GoKebabVertical } from 'react-icons/go'
import { Pagination, Popover, Whisper } from 'rsuite'

import ResetIcon from '@/assets/icons/reset-icon.svg'
import RightUpIcon from '@/assets/icons/right-up-arrow.svg'
import SearchIcon from '@/assets/icons/search.svg'
import StudentIcon from '@/assets/icons/students.svg'
import SelectField from '@/componentsV2/Common/Form/SelectField'
import { LIMIT_CLASS_PACKAGE_BASIC } from "@/constants/index";
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import useGrade from '@/hooks/useGrade'
import useNoti from '@/hooks/useNoti'
import { PACKAGES, paths as path } from "@/interfaces/constants";
import { WrapperContext } from '@/interfaces/contexts'
import { delayResult } from "@/utils/index";
import { paths } from 'api/paths';
import { callApiConfigError } from 'api/utils'
import { StickyFooter } from 'components/organisms/stickyFooter'

import BaseForm from '../Common/Controls/BaseForm'
import CheckSelectPicker from '../Common/Form/CheckSelectPicker';
import InputTextField from '../Common/Form/InputTextField'
import ModalAddNewClass from '../Common/ModalAddNewClass'
import SkeletonLoading from '../Common/SkeletonLoading'
import ButtonCustom from '../Dashboard/SideBar/ButtonCustom/Button'
import NoFieldData from '../NoDataSection/NoFieldData';
import styles from './ClassList.module.scss'
import useTranslation from "@/hooks/useTranslation";

interface LoadingToggleState {
    [classId: number]: boolean;
}
const ClassList = () => {
    const { t } = useTranslation();
    const parentContainerRef = useRef(null)
    const { globalModal } = useContext(WrapperContext)
    const { dataPlan } = useCurrentUserPackage()
    const { push, query, isReady } = useRouter()
    const { getNoti } = useNoti()
    const { getGrade } = useGrade()
    const [isLoading, setIsLoading] = useState(false)
    const [dataClasses, setDataClasses] = useState([])
    const [onReload, setOnReload] = useState(false)
    const [openModal, setOpenModal] = useState(false)
    const [dataSelect, setDataSelect] = useState(null)

    const [totalPage, setTotalPage] = useState(1)
    const itemSkeleton = 10
    const currentPage = Number(query.page) || 1
    const [searching, setSearching] = useState(false)
    const [valueName, setValueName] = useState('')
    const [loadingToggle, setLoadingToggle] = useState<LoadingToggleState>({})
    const [isFirstRender, setIsFirstRender] = useState(true)

    const gradeList = getGrade.filter(item => /^G[0-9]{1,2}$/.test(`${item.code}`)) || []
    const gradeOptions = gradeList.map(({ code, display }) => ({
        value: code,
        label: display,
    }))

    const [countClasses, setCountClasses] = useState(null)

    const wrapperDiv: HTMLDivElement = document.querySelector('.t-wrapper__main')
    const scrollToTop = () => {
        if (wrapperDiv) {
            wrapperDiv.scrollTop = 0
            wrapperDiv.style.scrollBehavior = 'smooth'
        }
    }

    const handlePageChange = async (page: number) => {
        scrollToTop()
        const nonEmptyQuery: any = Object.entries(objQuery)
            .filter(([, value]) => value !== undefined && value !== '')
            .reduce((query, [key, value]) => ({ ...query, [key]: value }), {})
        push({
            pathname: path.classes,
            query: {
                ...nonEmptyQuery,
                page
            }
        })
    }

    const isSearchEmpty = useMemo(() => {
        if (!dataClasses || dataClasses.length === 0 && !searching) {
            return false
        }
        return true
    }, [searching, dataClasses])

    const handleAddNewClass = () => {
        setOpenModal(true)
        setDataSelect(null)
    }

    const closeModal = () => {
        setOpenModal(false)
    }

    const openEditClassModal = (data: any) => {
        setOpenModal(true)
        setDataSelect(data)
    }

    const openUpdateClassStatus = (data: any, checked: boolean) => {
        const status = checked ? 1 : 0
        if (!checked) {
            globalModal.setState({
                id: 'action-confirm',
                type: 'set-class-status',
                content: {
                    class: data.name,
                    onSubmit: () => handleUpdateClassStatus(data, status)
                }
            })
        } else {
            handleUpdateClassStatus(data, status)
        }
    }

    const openDeleteClassModal = (data: any) => {
        globalModal.setState({
            id: 'action-confirm',
            type: 'delete-class',
            content: {
                class: data.name,
                onSubmit: () => handleDeleteClass(data)
            }
        })
    }

    const handleUpdateClassStatus = async (data: any, status: number) => {
        const body: any = {
            status: status,
        }
        setLoadingToggle((prevState) => ({ ...prevState, [data.id]: true }))
        const response: any = await delayResult(callApiConfigError(
            `${paths.api_teacher_classes}/${data.id}/update-status`,
            'put',
            'token',
            body
        ), 400)
        setLoadingToggle((prevState) => ({ ...prevState, [data.id]: false }))
        if (response.result) {
            setOnReload(true)
            getNoti('success', 'topCenter', status == 1 ? t('common-get-noti')['set-class-active'] : t('common-get-noti')['set-class-inactive'])
        }
    }

    const handleDeleteClass = async (data: any) => {
        const response: any = await callApiConfigError(
            `${paths.api_teacher_classes}/${data.id}`,
            'DELETE',
        )
        if (response.result) {
            getNoti('success', 'topCenter', t('common-get-noti')['delete-class-succeed'])
            if (currentPage === totalPage && dataClasses.length === 1 && currentPage !== 1) {
                push({
                    pathname: path.classes,
                    query: {
                        page: Math.max(currentPage - 1, 1),
                    },
                })
            } else {
                await fetcher()
            }
        }
    }

    const handleSendUnitTest = (dataClass: any) => {
        globalModal.setState({
            id: 'send-unit-test',
            dataClass: dataClass
        })
    }

    const formikFormClass = useFormik({
        initialValues: {
            name: '',
            grade: '',
            status: '',
        },
        onSubmit: values => {
            //
        },
    })

    useEffect(() => {
        if (formikFormClass.values.name === '') {
            setIsFirstRender(true)
        }
        const delay = 500;
        const timerId = setTimeout(() => {
            setValueName(formikFormClass.values.name)
        }, delay);

        return () => {
            clearTimeout(timerId);
        };
    }, [
        formikFormClass.values.name
    ])

    const handleSelectStatus = (selectedOption: any) => {
        formikFormClass.setFieldValue('status', selectedOption.value)
    }

    const objQuery = useMemo(() => {
        return {
            page: valueName != '' || formikFormClass.values.grade != '' || formikFormClass.values.status != '' ? 0 : currentPage - 1,
            name: valueName != '' ? valueName : undefined,
            grade: formikFormClass.values.grade != '' ? formikFormClass.values.grade : undefined,
            status: formikFormClass.values.status != '' ? formikFormClass.values.status : undefined,
        }
    }, [
        valueName,
        formikFormClass.values.grade,
        formikFormClass.values.status,
        currentPage
    ])

    const fetcher = async () => {
        setIsLoading(true)
        scrollToTop()
        const queryString = qs.stringify(objQuery, { arrayFormat: 'repeat' })
        const response: any = await delayResult(
            callApiConfigError(
                `${paths.api_teacher_classes}?${queryString}
            `), 400)
        setIsLoading(false)
        if (response.result && response.data) {
            setDataClasses(response.data.content)
            setTotalPage(response.data.totalPage)
            setIsFirstRender(false)
            fetchCountClasses()
        }
    }
    const fetchCountClasses = () => {
        callApiConfigError(paths.api_teacher_classes).then(res => {
            if (res.code === 200) {
                setCountClasses(res.data.total)
            }
        }).catch(err => {
            console.log({err});
        })
    }

    useEffect(() => {
        fetchCountClasses()
    }, []);

    const isCreateClassesWithPackage = useMemo(() => {
        return countClasses !== null && (dataPlan?.code !== PACKAGES.BASIC.CODE || countClasses < LIMIT_CLASS_PACKAGE_BASIC);
    }, [countClasses]);

    const fetcherReload = async () => {
        scrollToTop()
        const queryString = qs.stringify(objQuery, { arrayFormat: 'repeat' })
        const response: any = await delayResult(
            callApiConfigError(
                `${paths.api_teacher_classes}?${queryString}`
            ), 400)
        if (response.result && response.data) {
            setDataClasses(response.data.content)
            setTotalPage(response.data.totalPage)
        }
    }

    const pushQuery = (data: any) => {
        const nonEmptyQuery: any = Object.entries(data)
            .filter(([, value]) => value !== undefined && value !== '')
            .reduce((query, [key, value]) => ({ ...query, [key]: value }), {})
        nonEmptyQuery.page = 1
        push({
            pathname: path.classes,
            query: {
                ...nonEmptyQuery
            }
        })
    }

    useEffect(() => {
        const isQueryNotEmpty = JSON.stringify(query) !== JSON.stringify({})
        if (isReady && isQueryNotEmpty) {
            fetcher()
        }
    }, [
        isReady,
        currentPage,
        query
    ])

    useEffect(() => {
        pushQuery(objQuery)
        if (valueName || formikFormClass.values.grade || formikFormClass.values.status) {
            setSearching(true)
        } else {
            setSearching(false)
        }
    }, [
        valueName,
        formikFormClass.values.grade,
        formikFormClass.values.status
    ])

    useEffect(() => {
        if (onReload) {
            fetcherReload()
            setOnReload(false)
        }
    }, [onReload])

    const handleReset = () => {
        setIsFirstRender(true)
        formikFormClass.resetForm()
        setValueName('')
    }

    return (
        <>
            {openModal && (
                <ModalAddNewClass
                    isOpen={openModal}
                    onClose={closeModal}
                    dataEdit={dataSelect}
                    onReload={setOnReload}
                />
            )}
            <div className={styles.classListContainer} ref={parentContainerRef}>
            {(isSearchEmpty || isFirstRender ) && (
                <div className={styles.classInfoCardContainer}>
                    <FormikContext.Provider value={formikFormClass}>
                        <BaseForm className={styles.leftSection}>
                            <InputTextField
                                name="name"
                                autoFocus={undefined}
                                className={styles.aInputWithLabel}
                                type={'text'}
                                label=""
                                maxLength={100}
                                onChange={formikFormClass.handleChange}
                                disabled={false}
                                hideErrorMessage={undefined}
                                iconLeft={<SearchIcon />}
                                iconRight={undefined}
                                placeholder={t('classes-page')['search-placeholder']}
                                autoComplete="off"
                                onlyNumber={false}
                            />
                            <CheckSelectPicker
                                name="grade"
                                label={t('common')['grade']}
                                searchable={false}
                                cleanable={false}
                                className={styles.selectMultiGrade}
                                data={gradeOptions}
                                container={parentContainerRef.current}
                            />
                            <SelectField
                                name="status"
                                options={[
                                    { label: t('status')['deactivate'], value: '0' },
                                    { label: t('status')['active'], value: '1' },
                                ]}
                                className={styles.selectField}
                                menuClassName={styles.menuCustom}
                                placeHolder={t('learring-result')['status']}
                                titleDropdown={undefined}
                                onChange={handleSelectStatus}
                                disable={false}
                            />
                            <ButtonCustom
                                onClick={handleReset}
                                iconRight={<ResetIcon />}
                                className={classNames(styles.aButtonReset, {
                                    [styles.disable]:
                                        !formikFormClass.values.name?.length &&
                                        !formikFormClass.values.grade.length &&
                                        !formikFormClass.values.status.length
                                })}
                            >
                                {t('common')['reset']}
                            </ButtonCustom>
                        </BaseForm>
                    </FormikContext.Provider>
                    {isCreateClassesWithPackage && <div className={styles.rightSection}>
                        <ButtonCustom
                            className={styles.addClassBtn}
                            onClick={handleAddNewClass}
                        >
                            {t('classes')['add-class-btn']}
                        </ButtonCustom>
                    </div>}
                </div>
            )}
                {isLoading && (
                    <div className={styles.classWrapper}>
                        {Array.from(Array(itemSkeleton), (_,index: number) => {
                            return (
                                <Fragment key={`SkeletonLoading-${index}`}>
                                    <SkeletonLoading className={styles.classInfoItem}>
                                        <span
                                            className={styles.header}
                                            style={{
                                                width: '50%',
                                            }}
                                        ></span>
                                        <span className={styles.schoolYear}></span>
                                        <div
                                            className={`${styles.boxSkeleton} ${styles.boxInfoSkeleton}`}
                                        ></div>
                                    </SkeletonLoading>
                                </Fragment>
                            )
                        })}
                    </div>
                )}
                {!isLoading && dataClasses.length > 0 && (
                    <div className={styles.classWrapper}>
                        {dataClasses.map((classItem: any, cIndex: number) => {
                            // const isToggleLoading = loadingToggle[classItem.id]
                            return (
                                <div
                                    className={styles.classInfoItem}
                                    key={classItem?.id}
                                    onClick={() => {
                                        push({
                                            pathname: `${path.classes}/${classItem?.id}`,
                                        })
                                    }}
                                >
                                    <div className={styles.header}>
                                        <div className={styles.className} title={classItem.name}>
                                            {classItem.name}
                                        </div>
                                        <div
                                            className={styles.actionGroup}
                                            onClick={(e) => {
                                                e.stopPropagation()
                                                e.preventDefault()
                                            }}
                                        >
                                            <Whisper
                                                placement={'auto'}
                                                trigger="click"
                                                container={() => document.querySelector('.t-wrapper__main')}
                                                speaker={
                                                    <Popover                                                     
                                                        style={{
                                                            zIndex: 250
                                                        }}
                                                    >
                                                        <div className={styles.tableActionPopover}>
                                                            <div className={styles.actionItem}
                                                                onClick={() => openEditClassModal(classItem)}
                                                            >
                                                                <FaPencilAlt />
                                                                <span>{t('common')['edit']}</span>
                                                            </div>
                                                            <div
                                                                className={styles.actionItem}
                                                                onClick={() => openDeleteClassModal(classItem)}
                                                            >
                                                                <FaTrashAlt />
                                                                <span>{t('classes')['delete-class']}</span>
                                                            </div>
                                                            <div
                                                                className={styles.actionItem}
                                                                onClick={() => handleSendUnitTest(classItem)}
                                                            >
                                                                <RightUpIcon />
                                                                <span>{t('classes')['assign-unit-test']}</span>
                                                            </div>

                                                        </div>
                                                    </Popover>
                                                }
                                            >
                                                <div className={styles.actionBtn}>
                                                    <GoKebabVertical color="#7C68EE" />
                                                </div>
                                            </Whisper>
                                        </div>
                                    </div>
                                    <div className={styles.schoolYear}>
                                        {t('common')['school-year']}: <span style={{ fontWeight: '600' }}>{classItem.start_year}-{classItem.end_year}</span>
                                    </div>
                                    <div className={styles.infoContent}>
                                        <div className={styles.lineInfo}>
                                            <div className={styles.totalStudents}>
                                                <StudentIcon />
                                                <div className={styles.text}>
                                                    {classItem.totalStudent}
                                                </div>
                                            </div>
                                            <div className={styles.grade}>
                                                {(getGrade.length > 0 && getGrade.find((m) => m.code === classItem.grade)?.display) ||
                                                    '---'}
                                            </div>
                                        </div>
                                    </div>
                                    {/* <div className={styles.toggleActive} onClick={e => e.stopPropagation()}>
                                        <Toggle
                                            size="md"
                                            checked={classItem.status}
                                            onChange={(checked) =>
                                                openUpdateClassStatus(classItem, checked)
                                            }
                                            loading={isToggleLoading}
                                        />
                                    </div> */}
                                </div>
                            )
                        })}
                    </div>
                )}
                {!isLoading && dataClasses.length == 0 && (
                    <NoFieldData
                        field='classes'
                        isSearchEmpty={isSearchEmpty}
                        handleCreate={handleAddNewClass}
                    />
                )}
            </div>
            {dataClasses.length > 0 && (
                <StickyFooter>
                    <div className="__pagination">
                        <Pagination
                            prev
                            next
                            ellipsis
                            size="md"
                            total={totalPage * 10}
                            maxButtons={10}
                            limit={10}
                            activePage={currentPage}
                            onChangePage={(page: number) => handlePageChange(page)}
                        />
                    </div>
                </StickyFooter>
            )}
        </>
    )
}

export default ClassList