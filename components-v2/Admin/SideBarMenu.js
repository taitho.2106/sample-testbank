import React, { useContext, useState } from 'react'
import Link from 'next/link'

import Button from '@/componentsV2/Common/Controls/Button'
import classNames from 'classnames'
import { SIDENAV_ITEMS1 } from '@/componentsV2/Admin/data/sidebar'
import { useSession } from 'next-auth/client'
import { userInRight } from '@/utils/index'
import { useRouter } from 'next/dist/client/router'
import { AppContext } from '@/interfaces/contexts'
import { paths } from '@/interfaces/constants'

import LogoIcon from '../../assets/icons/logo.svg'
import ChevronIcon from '../../assets/icons/arrow.svg'
import LogoCompact from '../../assets/icons/logo-compact.svg'

import styles from './SideBarMenu.module.scss'
import Guid from '@/utils/guid'

const SideBarMenu = ({ isExpand, onToggle }) => {
    const [session] = useSession()
    const router = useRouter()
    const [tooltipIndex, setTooltipIndex] = useState(-1)

    const checkRegexUrl = (router, regexes) => {
        if (!regexes || !regexes.length) return false
        const url = router.asPath
        for (let i = 0; i < regexes.length; i++) {
            const found = url.match(regexes[i])
            if (found) return true
        }
        return false
    }

    const checkUrl = (path1, router) => {
        const query1 = path1.query ?? {}
        const query2 = router.query ?? {}
        if (path1.url !== router.pathname) return false
        if (!query1.hasOwnProperty('m') && router.query.hasOwnProperty('m'))
            return false

        if (query1.hasOwnProperty('m') && !router.query.hasOwnProperty('m'))
            return false

        return !(query1.hasOwnProperty('m') &&
            router.query.hasOwnProperty('m') &&
            query1['m'] !== query2['m'])
    }

    const toggleSideBarItem = (data, url) => {
        const currentCollapseList = openingCollapseList.state
        const checkExist = checkCollapseExist(data)
        openingCollapseList.setState(
            checkExist || !data.list?.length
                ? [...currentCollapseList.filter((item) => item !== data.id)]
                : [...currentCollapseList, data.id],
        )

        if (url) {
            router.push(url === '/' ? '/home' : url).then()
        }
    }

    const checkIncludeActive = (data) => {
        const pathname = router.asPath
        let check = false
        if (data?.list) {
            data.list.forEach((item) => {
                if (
                    pathname === item.url ||
                    item.baseUrl?.some((m) => checkUrl(m, router)) ||
                    checkRegexUrl(router, item.regexUrls)
                ) {
                    check = true
                }
            })
        }
        return check
    }

    const { openingCollapseList } = useContext(AppContext)
    const checkCollapseExist = (data) => openingCollapseList.state.includes(data?.id)

    return (
        <div className={styles.sideBarMenuWrapper}>
            <div className={classNames(styles.top, {
                [styles.compactMode]: !isExpand,
            })}>
                <Link href={paths.home}>
                    {isExpand ? <LogoIcon /> : <LogoCompact />}
                </Link>

                <span className={styles.btnExpand} onClick={onToggle}>
                    <ChevronIcon />
                </span>
            </div>

            <div className={styles.center}>
                {SIDENAV_ITEMS1
                    //filter sidebar item base on account role
                    ?.filter(item => userInRight(item.access_role, session))
                    ?.map((item) => {
                        const isActive = ((!isExpand || !checkCollapseExist(item)) && checkIncludeActive(item)) ||
                            (item.url === '/'
                                ? item.url === router.pathname
                                : !item.list && router.pathname.startsWith(item.url))
                        return (
                            <>
                                <div className={classNames(styles.sideItem, {
                                    [styles.active]: isActive,
                                    [styles.compactMode]: !isExpand,
                                })}
                                     onClick={() => toggleSideBarItem(item, item?.url)}
                                    //event render tooltip hover when sidebar compact mode
                                     onMouseEnter={!isExpand ? () => setTooltipIndex(item.id) : undefined}
                                     onMouseLeave={!isExpand ? () => setTooltipIndex(-1) : undefined}>
                                 <span
                                     className={((isExpand && checkCollapseExist(item)) || isActive) && styles.leftIcon}>
                                    {item.icon}
                                 </span>
                                    <a className={classNames({
                                        [styles.activeName]: (checkCollapseExist(item) || isActive),
                                    })}>
                                        {item.name}
                                    </a>
                                    {
                                        item.list?.length &&
                                        <ChevronIcon className={classNames(styles.chevronIcon, {
                                            [styles.active]: checkCollapseExist(item),
                                        })} />
                                    }
                                    {
                                        //render tooltip when sidebar is compact mode
                                        tooltipIndex === item.id && !isExpand &&
                                        <div className={styles.tooltipMenu}>
                                            {item.list?.length && <span>{item.name}</span>}
                                            {
                                                item.list?.length ?
                                                    //tooltip with list items
                                                    <div className={styles.tooltipMenuList}>
                                                        {item.list
                                                            ?.filter(list => userInRight(list.access_role, session))
                                                            ?.map(list => {
                                                                const fullPath = router.asPath
                                                                const isActiveSub = fullPath === list.url ||
                                                                    item.baseUrl?.some((m) => checkUrl(m, router)) || checkRegexUrl(router, list.regexUrls)
                                                                return (
                                                                    <div
                                                                        key={Guid.newGuid()}
                                                                        className={isActiveSub && styles.activeNameCompact}
                                                                        onClick={() => router.push(list.url)}>
                                                                        {list.name}
                                                                    </div>
                                                                )
                                                            })
                                                        }
                                                    </div>
                                                    :
                                                    //tooltip without list items
                                                    <div className={styles.activeNameCompact2}
                                                         onClick={() => router.push(item.url === '/' ? '/home' : item.url)}>
                                                        {item.name}
                                                    </div>
                                            }
                                        </div>
                                    }
                                </div>
                                {
                                    //render collapse sidebar item when sidebar is expand
                                    item.list?.length && isExpand &&
                                    <div className={classNames(styles.sideBarContent, {
                                        [styles.active]: checkCollapseExist(item),
                                    })}>
                                        {item.list
                                            .filter(list => userInRight(list.access_role, session))
                                            .map(list => {
                                                const fullPath = router.asPath
                                                const isActiveSub = fullPath === list.url ||
                                                    item.baseUrl?.some((m) => checkUrl(m, router)) || checkRegexUrl(router, list.regexUrls)
                                                return (
                                                    <div 
                                                    key={Guid.newGuid()}    
                                                    className={classNames(styles.item, {
                                                        [styles.active]: isActiveSub,
                                                    })}
                                                         onClick={() => router.push(list?.url).then()}>
                                                        {list.name}
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                }
                            </>
                        )
                    })}
            </div>
            {
                isExpand &&
                <div className={styles.bottom}>
                    <p>Còn <span>6</span> ngày <br />dùng thử miễn phí</p>
                    <div className={styles.btnGroup}>
                        <Button>
                            Thanh toán
                        </Button>
                        <Button type='outline'>
                            Nâng cấp
                        </Button>
                    </div>
                </div>
            }
        </div>
    )
}

export default SideBarMenu