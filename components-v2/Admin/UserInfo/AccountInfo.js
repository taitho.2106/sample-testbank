import { useRef, useState } from "react";

import classNames from "classnames";
import { FormikContext, useFormik } from 'formik';
import { useRouter } from 'next/router';
import * as Yup from "yup";

import SkeletonLoading from "@/componentsV2/Common/SkeletonLoading";
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage';
import { useOutsideClick } from "@/hooks/useOutsideClick";
import {
    PACKAGES,
    PAYMENT_TYPE,
    USER_ROLES,
    emailRegExp,
    numberRegex,
    passwordRegex,
    phoneRegExp1,
    whiteSpaceRegex
} from "@/interfaces/constants";
import { renderNameByCode, userInRight } from "@/utils/index";
import AlertIcon from "assets/icons/alert.svg";

import { SingleSelectPicker } from "@/componentsV2/Common/SingleSelectPicker/SingleSelectPicker";
import useInfo from "@/hooks/useInfo";
import useProvinces from "@/hooks/useProvince";
import { paths } from '../../../api/paths';
import Camera from '../../../assets/icons/camera.svg';
import CloseIcon from "../../../assets/icons/close.svg";
import Overview from '../../../assets/icons/logo-icon.svg';
import WarningIcon from "../../../assets/icons/warning.svg";
import useNoti from '../../../hooks/useNoti';
import useTranslation from '../../../hooks/useTranslation';
import { paths as urlPaths } from "../../../interfaces/constants";
import BaseForm from '../../Common/Controls/BaseForm';
import BasicModal from "../../Common/Controls/BasicModal";
import Button from '../../Common/Controls/Button';
import InputTextField from '../../Common/Form/InputTextField';
import styles from "./AccountInfo.module.scss";

const AccountInfo = ({session, user, mutateUserInfo}) => {
    const isSystem = userInRight([USER_ROLES.Operator],session)
    const { optionProvinces } = useProvinces();
    const isStudent = userInRight([-1, USER_ROLES.Student],session)
    const { getNoti } = useNoti()
    const {t} = useTranslation()
    const {push} = useRouter()
    const fileAvatar = useRef(null)
    const imgRef = useRef(null)
    const [error, setError] = useState(false)
    const { namePackage, isExpired, dataPlan, isHighestPackage, textTimeLeft} = useCurrentUserPackage()
    const userSession = session.user
    const email = user.email || '';
    const phone = user.phone || '';
    const full_name = user.full_name || '';
    const province_id = user.province_id || 0;

    const [showModalPass, setShowModalPass] = useState(false)
    const { getRoleUser } = useInfo(userSession)
    const role = getRoleUser()

    const onCloseModal = () => {
        formikPass.resetForm()
        setShowModalPass(false)
    }
    const ref = useOutsideClick(() => setError(false), true);

    const onShowModalPass = () => {
        setShowModalPass(true)
    }

    const handleClick = (e, forceType) => {
        e.stopPropagation();
        const type = forceType ?? (isExpired ? PAYMENT_TYPE.UPGRADE : PAYMENT_TYPE.RENEW);
        push({
            pathname: urlPaths.payment,
            query: { type },
        })
    }
    
    const renderFullNameWithRole = (userSession, values) => {
        switch (userSession.user_role_id) {
            case USER_ROLES.Student:
                return `${t('role')['student']} ${values.phone}`;
            case USER_ROLES.Teacher:
                return `${t('role')['teacher']} ${values.phone}`;
            case USER_ROLES.Operator:
                return `${t('role')['operator']} ${values.phone}`;
            case USER_ROLES.Sale:
                return `${t('role')['sale']} ${values.phone}`;
            default:
                return `${t('role')['admin']} ${values.phone}`;
        }
    }

    const onSubmit = async (values) => {
        const body = {
            ...values,
            full_name: !!values.full_name.length ?
                values.full_name : renderFullNameWithRole(userSession, values)
        }
        const res = await fetch(paths.api_users_save, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(body),
        })
        const json = await res.json()
        if (!res.ok || !json.isSuccess) {
            formikForm.setFieldError(json.field, json.message || 'Error undefined!');
            return
        }
        formikForm.setValues(body)
        getNoti('success', 'topCenter', t('common-get-noti')['save-info-succeed'])
        mutateUserInfo({
            ...user,
            ...body,
        }, { revalidate: false })
    }

    const onSubmitPass = async (values) => {
        const res = await fetch(paths.api_users_change_password, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(values),
        })
        const json = await res.json()
        if (!res.ok || !json.isSuccess) {
            getNoti('error', 'topCenter', json.message)
            return
        }
        getNoti('success', 'topCenter', t('common-get-noti')['change-password-succeed'])
        onCloseModal()
    }

    const onChangeAvatar = async () => {
        const file = fileAvatar.current.files[0]
        if(!file) {
            setError(false)
            return
        }

        if (!file.type.startsWith("image/")) {
            setError(true)
            return;
        }
        const fileSizeLimit = 2 * 1024 * 1024; 
        const allowedTypes = ["image/jpeg", "image/jpg", "image/png"];
        if (file.size > fileSizeLimit) {
            setError(true)
            return
        }
        if (!allowedTypes.includes(file.type)) {
            setError(true)
            return
        }
        setError(false)
        const formData = new FormData()
        formData.append('file_avatar', file)
        const res = await fetch(paths.api_users_avatar, {
            method: 'POST',
            body: formData,
        })
        const json = await res.json()
        if (!res.ok) throw Error(json.message)
        getNoti('success', 'topCenter', t('common-get-noti')['change-avatar-succeed'])
        mutateUserInfo({ ...user, avatar: json.avatar }, { revalidate: false })
        imgRef.current.setAttribute(
            'src',
            URL.createObjectURL(file),
        )
    }

    const formValidationSchema = () => {
        return Yup.object().shape({
            email: Yup.string().trim().required(t(t('formikForm')['required'], ['Email']))
                .matches(emailRegExp, t(t('formikForm')['incorrect'], ['Email'])),
            phone: Yup.string()
                    .required(t(t('formikForm')['required'], [t('phone')]))
                    .matches(numberRegex, t(t('formikForm')['incorrect'], [t('phone')]))
                    .length(10, t(t('formikForm')['phone-length'], ['10']))
                    .matches(phoneRegExp1, t(t('formikForm')['incorrect'], [t('phone')])),
            full_name: Yup.string()
                    .matches(whiteSpaceRegex, t(t('formikForm')['white-space-regex'], [t('common')['full-name']]))
                    .max(45, t('full-name-max',["45"]))
        })
    }

    const passwordValidationSchema = () => {
        return Yup.object().shape({
            password: Yup.string().trim()
                .required(t('auth')['password-required']),
            new_password: Yup.string()
                .required(t('auth')['new-pass-required'])
                .matches(whiteSpaceRegex, t('auth')['new-pass-space'])
                .matches(passwordRegex, t('auth')['new-pass-valid'])
                .min(8, t('auth')['new-pass-min']),
            confirm_password: Yup.string()
                .required(t('auth')['confirm-pass'])
                .oneOf([Yup.ref('new_password'), ''], t('auth')['correct-pass']),
        })
    }

    const formikForm = useFormik({
        initialValues: {
            email,
            phone,
            full_name,
            province_id
        },
        onSubmit: onSubmit,
        validationSchema: formValidationSchema,
    })

    const formikPass = useFormik({
        initialValues: {password: '', new_password: '', confirm_password: ''},
        onSubmit: onSubmitPass,
        validationSchema: passwordValidationSchema,
    })

    const renderDatePlan = () => {
        if (dataPlan.isExpired || dataPlan.price === 0) {
            return <p>{textTimeLeft}</p>;
        }
        return <p dangerouslySetInnerHTML={{__html: t(t('package')['time-line'],[`<span>${textTimeLeft}</span>`])}}></p>
    }

    const isNotPackageStudent = isExpired && !isStudent;

    return (
        <>
            <div className={styles.accountInfoWrapper}>
            {!isSystem && (dataPlan ?
                        <div className={classNames(styles.currentPlan, {
                            [styles.isExpired]: isNotPackageStudent
                        })}>
                            <div className={classNames(styles.left, {
                                [styles.isExpired]: isNotPackageStudent
                            })}>
                                {isNotPackageStudent ? <AlertIcon/> : <WarningIcon/>}
                                <div>
                                    <span>{renderNameByCode(namePackage, t)}</span>
                                    <>{renderDatePlan()}</>
                                </div>
                            </div>
                            {
                                !isStudent ?
                                    <div className={styles.right}>
                                        {!isExpired && !isHighestPackage &&
                                            <Button type="outline" onClick={e => handleClick(e, PAYMENT_TYPE.UPGRADE)}>
                                                {t('upgrade')}
                                            </Button>
                                        }
                                        {(namePackage.code !== PACKAGES.TRIAL.CODE) &&
                                            <Button onClick={handleClick}>
                                                {isExpired ? t("buy-now") : t('extend')}
                                            </Button>
                                        }
                                    </div>
                                    :
                                    null
                            }
                        </div>
                        :
                        <SkeletonLoading
                                width="100%"
                                height="104px"
                        />
                    )}
                <div className={styles.infoAndForm}>
                    <div className={styles.main}>
                        <div className={styles.head}>
                            <h3>{t("personal")['account-info']}</h3>
                            <button onClick={onShowModalPass}>{t('auth')['change-password']}</button>
                        </div>
                        <div className={styles.contentHead}>
                            <div className={styles.avatarSection}>
                                <div className={styles.avatar}>
                                    <label htmlFor='file-input'>
                                        {user.avatar ? (
                                            <img
                                                ref={imgRef}
                                                src={user.avatar.startsWith('blob') ? `${user.avatar}` : `/upload/${user.avatar}`}
                                                alt=""
                                            />
                                        ) : (
                                            <Overview
                                                height={70}
                                                width={70}
                                                className={styles.avatarDefault}
                                            />
                                        )}
                                        <Camera className={styles.camera} />
                                    </label>
                                    <input
                                        id='file-input'
                                        ref={fileAvatar}
                                        type='file'
                                        accept=".png,.jpeg,.jpg"
                                        onChange={onChangeAvatar}
                                    />
                                </div>
                                {error && (
                                    <div className={styles.tooltipError} ref={ref}>
                                        <span dangerouslySetInnerHTML={{__html: t('avatar-error')}}></span>
                                    </div>
                                )}
                            </div>
                            <div className={styles.userRole}>
                                {role}
                            </div>
                        </div>
                        <FormikContext.Provider value={formikForm}>
                                <BaseForm className={styles.form} id="account-form">
                                        <InputTextField
                                            name='email'
                                            disabled={!!email}
                                            className={styles.inputField}
                                        placeholder={<>Email <span style={{ color: "#f33060" }}>*</span></>}
                                            autoComplete='off'
                                            ignoreSpace
                                        />
                                        <InputTextField
                                            name='phone'
                                            className={styles.inputField}
                                            placeholder={<>{t('phone')} <span style={{ color: "#f33060" }}>*</span></>}
                                            autoComplete='off'
                                            onlyNumber={true}
                                            type="tel"
                                            disabled={!!phone}
                                            ignoreSpace
                                        />
                                        <InputTextField
                                            name='full_name'
                                            className={styles.inputField}
                                            placeholder={t('common')['full-name']}
                                            autoComplete='off'
                                        />
                                        <SingleSelectPicker
                                            isUserInfo={true}
                                            isProvince={true}
                                            name='province_id'
                                            placement={"topStart"}
                                            style={{height: 56}}
                                            className={styles["filter-box"]}
                                            data={optionProvinces || []}
                                            inputSearchShow={true}
                                            defaultValue={optionProvinces && optionProvinces.find(item => item.code === province_id)}
                                            label={t('common')['province']}
                                            onChange={(newValue) => {
                                                formikForm.setFieldValue("province_id", newValue)
                                            }}
                                        />
                                </BaseForm>
                        </FormikContext.Provider>
                    </div>
                    <div className={styles.actions}>
                        <Button
                            className={styles.btnSave}
                            type="success"
                            buttonType="submit"
                            form="account-form"
                        >
                            {t('common')['save']}
                        </Button>
                    </div>
                </div>
            </div>

            <BasicModal
                isOpen={showModalPass}
                contentClassName={styles.changePassModal}
            >
                <div className={styles.header}>
                    <p>{t('personal')['forgot-password']}</p>
                    <Button type="none" onClick={onCloseModal}>
                        <CloseIcon />
                    </Button>
                </div>
                <FormikContext.Provider value={formikPass}>
                    <div className={styles.body}>
                        <BaseForm className={styles.passwordForm}>
                            <div className={styles.changePassInput}>
                                <InputTextField
                                    type='password'
                                    name='password'
                                    className={styles.inputField}
                                    autoComplete='off'
                                    placeholder={t('personal')['old-pass']}
                                />
                                <InputTextField
                                    type='password'
                                    name='new_password'
                                    className={styles.inputField}
                                    autoComplete='off'
                                    placeholder={t('common')['new-password']}
                                />
                                <InputTextField
                                    type='password'
                                    name='confirm_password'
                                    className={styles.inputField}
                                    autoComplete='off'
                                    placeholder={t("personal")['press-old-pass']}
                                />
                            </div>
                            <div className={styles.footer}>
                                <Button buttonType={'submit'}>{t('common')['save']}</Button>
                            </div>
                        </BaseForm>
                    </div>
                </FormikContext.Provider>
            </BasicModal>
        </>
    )
}

export default AccountInfo;