import React, {} from "react";
import classNames from "classnames";
import {useRouter} from "next/router";

import Button from "../../Common/Controls/Button";
import {USER_ROLES, paths} from "../../../interfaces/constants";

import styles from "./UserInfo.module.scss";
import { useSession } from "next-auth/client";
import { userInRight } from "@/utils/index";
import useTranslation from "../../../hooks/useTranslation";

const ProfileLayout = ({children}) => {
    const { t } = useTranslation()
    const {push, pathname} = useRouter();
    const [session] = useSession()
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const switchTab =(url) => {
        push(url).then();
    }

    return (
        <div className={styles.userInfoWrapper}>
            <div className={styles.tabs}>
                <div className={classNames(styles.tabItem, {
                    [styles.active]: pathname === paths.userInfo
                })} onClick={() => switchTab(paths.userInfo)}>
                    <Button type="none">{t('personal')['account-info']}</Button>
                </div>
                {!isSystem && <>
                    <div className={classNames(styles.tabItem, {
                        [styles.active]: pathname === paths.transactionHistory
                    })} onClick={() => switchTab(paths.transactionHistory)}>
                        <Button type="none">{t("transaction-history")}</Button>
                    </div>
                </>}
            </div>

            <div className={styles.tabContents}>
                <div className={styles.tabContentItem}>
                    {children}
                </div>
            </div>
        </div>
    )
}

export default ProfileLayout;