import { USER_ROLES } from '@/interfaces/constants'
import OverviewIcon from '../../../assets/icons/overview-sidebar.svg'
import ExamBankIcon from '../../../assets/icons/exam-bank.svg'
import ITestExamBank from '../../../assets/icons/i-test-exam-bank.svg'

export const SIDENAV_ITEMS1 = [
    {
        id: 0,
        name: 'Tổng quan',
        icon: <OverviewIcon />,
        url: '/',
    },
    {
        id: 1,
        name: 'Ngân hàng đề thi của tôi',
        icon: <ExamBankIcon />,
        access_role: [-1, USER_ROLES.Teacher],
        list: [
            {
                id: 1,
                name: 'Định dạng đề thi',
                url: '/templates?m=mine',
                baseUrl: [{ url: '/templates/[templateSlug]', query: { m: 'mine' } }],
                active_role: [-1, USER_ROLES.Teacher],
            },
            {
                id: 2,
                name: 'Ngân hàng câu hỏi',
                url: '/questions?m=mine',
                baseUrl: [{ url: '/questions/[questionSlug]', query: { m: 'mine' } }],
                active_role: [-1, USER_ROLES.Teacher],
            },
            {
                id: 3,
                name: 'Danh sách đề thi',
                url: '/unit-test?m=mine',
                baseUrl: [{
                    url: '/unit-test/[templateSlug]',
                    query: { m: 'mine' },
                }, { url: '/unit-test/[templateSlug]/export-zip', query: { m: 'mine' } }],
                active_role: [-1, USER_ROLES.Teacher],
            },
        ],
    },
    {
        id: 2,
        name: 'Ngân hàng đề thi của giáo viên',
        icon: <ITestExamBank />,
        access_role: [],
        list: [
            {
                id: 1,
                name: 'Định dạng đề thi',
                url: '/templates?m=teacher',
                baseUrl: [
                    { url: '/templates/[templateSlug]', query: { m: 'teacher' } },
                ],
            },
            {
                id: 2,
                name: 'Ngân hàng câu hỏi',
                url: '/questions?m=teacher',
                baseUrl: [
                    { url: '/questions/[questionSlug]', query: { m: 'teacher' } },
                ],
            },
            {
                id: 3,
                name: 'Danh sách đề thi',
                url: '/unit-test?m=teacher',
                baseUrl: [
                    { url: '/unit-test/[templateSlug]', query: { m: 'teacher' } },
                    { url: '/unit-test/[templateSlug]/export-zip', query: { m: 'teacher' } },
                ],
            },
        ],
    },
    {
        id: 3,
        name: 'Ngân hàng đề thi của i-Test',
        icon: <ITestExamBank />,
        list: [
            {
                id: 1,
                name: 'Định dạng đề thi',
                url: '/templates',
                baseUrl: [{ url: '/templates/[templateSlug]' }],
                active_role: [USER_ROLES.Operator, USER_ROLES.Teacher],
            },
            {
                id: 2,
                name: 'Ngân hàng câu hỏi',
                url: '/questions',
                baseUrl: [{ url: '/questions/[questionSlug]' }],
                active_role: [USER_ROLES.Operator, USER_ROLES.Teacher],
            },
            {
                id: 3,
                name: 'Danh sách đề thi',
                url: '/unit-test',
                baseUrl: [{ url: '/unit-test/[templateSlug]' }, { url: '/unit-test/[templateSlug]/export-zip' }],
                active_role: [USER_ROLES.Operator, USER_ROLES.Teacher],
                regexUrls: [
                    /(\?g=\w+)*(\&s=\w)/gm, //open by grade and series
                ],
            },
            {
                id: 4,
                name: 'Đề thi tích hợp với đối tác',
                url: '/integrate-exam',
                baseUrl: [{ url: '/integrate-exam/[integrateExamSlug]' }],
                active_role: [USER_ROLES.Operator],
            },
        ],
    },
]