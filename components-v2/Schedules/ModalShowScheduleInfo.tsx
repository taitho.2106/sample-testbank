import React, { useEffect, useMemo } from "react";

import SearchIcon from "@rsuite/icons/Search";
import classNames from "classnames";
import dayjs from "dayjs";
import updateLocale from 'dayjs/plugin/updateLocale'
import { useSession } from "next-auth/client";
import Link from "next/link";
import { Button, Input, InputGroup } from "rsuite";

import IconClose from "@/assets/icons/icon-close.svg";
import IconReset from "@/assets/icons/reset-icon.svg";
import ModalWrapper from "@/componentsV2/Common/ModalWrapper";
import 'dayjs/locale/en';
import NoFieldData from "@/componentsV2/NoDataSection/NoFieldData";
import useTranslation from "@/hooks/useTranslation";
import { CODE_ASSIGN_OPTION, paths as pathsURL, STATUS_CODE_EXAM, USER_ROLES } from "@/interfaces/constants";
import { userInRight } from "@/utils/index";

import styles from "./ModalShowScheduleInfo.module.scss";

dayjs.extend(updateLocale)

// dayjs.updateLocale('en', {
//     weekdays: ['chủ nhật', 'thứ 2', 'thứ 3', 'thứ 4', 'thứ 5', 'thứ 6', 'thứ 7']
// })
const ModalShowScheduleInfo = ({ data = {}, onClose }: { data: any, onClose: () => void }) => {
    const { t, locale } = useTranslation()
    const [session] = useSession();
    const isStudent = userInRight([-1, USER_ROLES.Student], session);
    const dataSchedules = data.data || []
    const date = data.date
    const customConfig = useMemo(() => {
        return locale === 'en' ? ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'] : ['chủ nhật', 'thứ 2', 'thứ 3', 'thứ 4', 'thứ 5', 'thứ 6', 'thứ 7']
    }, [locale])
    useEffect(() => {
        dayjs.updateLocale('en', {
            weekdays: customConfig
        })
    }, [customConfig]);
    const onClickReset = () => {
        //
    };
    const getPathUrl = (unitTestAssign:any) => {
        if (isStudent)
            return `${pathsURL.assignedUnitTest}/${unitTestAssign.id}`;
        return unitTestAssign.student ?
            `${pathsURL.learningResult}/${unitTestAssign.id}?studentId=${unitTestAssign.student.id}`
            :
            `${pathsURL.learningResult}/${unitTestAssign.id}`;
    }
    return (
        <ModalWrapper
            isOpen={data.isShow}
            onCloseModal={onClose}
            className={styles.modalWrapperContainer}
        >
            <div className={styles.headerTitle}>
                <span>{t(t('calendar-page')['title'],[date.format(t('calendar-page')['format'])])}</span>
                <div onClick={onClose}>
                    <IconClose />
                </div>
            </div>
            {/*<div className={styles.searchBox}>*/}
            {/*    <InputGroup inside style={{ width: 300 }}>*/}
            {/*        <InputGroup.Addon>*/}
            {/*            <SearchIcon />*/}
            {/*        </InputGroup.Addon>*/}
            {/*        <Input placeholder="Tìm kiếm theo tên đề thi"/>*/}
            {/*    </InputGroup>*/}
            {/*    <Button*/}
            {/*        disabled={false}*/}
            {/*        appearance="ghost"*/}
            {/*        className={styles.btnReset}*/}
            {/*        onClick={onClickReset}*/}
            {/*    >*/}
            {/*        Đặt lại*/}
            {/*        <IconReset />*/}
            {/*    </Button>*/}
            {/*</div>*/}
            <div className={classNames(styles.tableData, {
                [styles.tableDataForStudent] : isStudent,
                [styles.tableDataForTeacher] : !isStudent,
                [styles.noData]: !dataSchedules.length
            })}>
                {!!dataSchedules.length ?
                    (
                        <>
                            <div className={styles.header}>
                                <div className={styles.item}>
                                    <div className={styles.itemInfo}>
                                        <div>{t('STT')}</div>
                                        <div>{t('table')['unit-test']}</div>
                                        {isStudent ? (
                                            <>
                                                <div>{t('table')['teacher']}</div>
                                                <div>{t('table')['classroom']}</div>
                                            </>
                                            ) :
                                            <div>{t('assign-exam-filter')['target']}</div>}
                                        <div>{t('table')['submit-time']}</div>
                                        <div>{t('assign-exam-filter')['status-exam']}</div>
                                    </div>
                                </div>
                            </div>
                            <div className={styles.body} id="container-body">
                                {!!dataSchedules.length && Array.from(dataSchedules, (unitTestAssign: any, index: number) => {
                                    const assignObject = unitTestAssign?.student ?
                                        ` - ${unitTestAssign?.student?.full_name}` :
                                        unitTestAssign?.group ?
                                            ` - ${unitTestAssign?.group?.name}` : "";
                                    const Com = unitTestAssign.examStatus !== STATUS_CODE_EXAM.UPCOMING || isStudent ?
                                        ({ children }:any) => <Link href={getPathUrl(unitTestAssign)}>{children}</Link> :
                                        ({ children }:any) => <span>{children}</span>
                                    return (
                                        <div className={styles.item} key={index}>
                                            <div className={styles.itemInfo}>
                                                <div>{index + 1}</div>
                                                <div title={unitTestAssign.unitTest.name}><Com>{unitTestAssign.unitTest.name}</Com></div>
                                                {isStudent ? (
                                                    <>
                                                        <div title={unitTestAssign.class.owner.full_name}>
                                                            <span>{unitTestAssign.class.owner.full_name}</span></div>
                                                        <div title={unitTestAssign.class.name}>
                                                            <span>{unitTestAssign.class.name}</span></div>
                                                    </>
                                                ) : <div title={`${unitTestAssign.class.name}${assignObject}`}>
                                                    <span>{unitTestAssign.class.name}{assignObject}</span></div>}
                                                <div className={classNames({
                                                    [styles.ongoing]: unitTestAssign.examStatus === STATUS_CODE_EXAM.ONGOING,
                                                    [styles.ended]: unitTestAssign.examStatus === STATUS_CODE_EXAM.ENDED
                                                })}>
                                                    <div className={styles.title}>
                                                        <span>{t('unit-test-assigned-page')['from']}</span>
                                                        <span>{t('unit-test-assigned-page')['to']}</span>
                                                    </div>
                                                    <div className={styles.timer}>
                                                        <span>{dayjs(unitTestAssign.start_date).format("HH:mm")}</span>
                                                        <span>{dayjs(unitTestAssign.end_date).format("HH:mm")}</span>
                                                    </div>
                                                    <div className={styles.date}>
                                                        <span>{dayjs(unitTestAssign.start_date).format("DD/MM/YYYY")}</span>
                                                        <span>{dayjs(unitTestAssign.end_date).format("DD/MM/YYYY")}</span>
                                                    </div>
                                                </div>
                                                <div>
                                            <span className={classNames({
                                                [styles.ongoing]: unitTestAssign.examStatus === STATUS_CODE_EXAM.ONGOING,
                                                [styles.ended]: unitTestAssign.examStatus === STATUS_CODE_EXAM.ENDED
                                            })}>{t('status')[CODE_ASSIGN_OPTION[unitTestAssign.examStatus-1].label]}</span>
                                                </div>
                                            </div>
                                        </div>
                                    );
                                })}
                            </div>
                        </>
                    ) :
                    (
                        <NoFieldData
                            field='schedules_calendar'
                            hasAction={false}
                        />
                    )
                }
            </div>
        </ModalWrapper>
    );
};

export default ModalShowScheduleInfo;