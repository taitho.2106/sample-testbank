import { useEffect, useMemo, useState } from "react";

import { PickerPanel } from "rc-picker";
import dayjsGenerateConfig from "rc-picker/lib/generate/dayjs";
import enUS from "rc-picker/lib/locale/en_US";
import { Checkbox, DatePicker, Loader } from "rsuite";

import ModalShowScheduleInfo from "@/componentsV2/Schedules/ModalShowScheduleInfo";
import useTranslation from "@/hooks/useTranslation";
import { STATUS_CODE_EXAM, USER_ROLES } from "@/interfaces/constants";
import { delayResult, userInRight } from "@/utils/index";

import { paths } from "../../api/paths";
import { callApi } from "../../api/utils";
import styles from "./SchedulesContainer.module.scss";

import "rc-picker/assets/index.css";
import { Dayjs } from "dayjs";
import dayjs from "dayjs";
import "dayjs/locale/es";
import classNames from "classnames";
import { useSession } from "next-auth/client";
import { CellRenderInfo } from "rc-picker/es/interface";

// const localeObject = {
//     ...enUS,
//     shortWeekDays: ["CHỦ NHẬT", "THỨ 2", "THỨ 3", "THỨ 4", "THỨ 5", "THỨ 6", "THỨ 7"],
// };
// dayjs.locale('en');
export default function SchedulesContainer() {
    const { t, locale } = useTranslation()
    const [session] = useSession()
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const [date, setDate] = useState(dayjs());
    const [schedules, setSchedules] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    const [dataModal, setDataModal] = useState({
        isShow: false,
        data: [],
        date: dayjs()
    });
    const [isCheck, setIsCheck] = useState(false)
    const localeObject = useMemo(() => {
        const shortWeekDays = locale === 'en' ? ['SUNDAY', 'MONDAY', 'TUESDAY', 'WEDNESDAY', 'THURSDAY', 'FRIDAY', 'SATURDAY'] : ["CHỦ NHẬT", "THỨ 2", "THỨ 3", "THỨ 4", "THỨ 5", "THỨ 6", "THỨ 7"]
        return {
            ...enUS,
            shortWeekDays,
        }
    }, [locale])
    useEffect(() => {
        dayjs.locale('en');
    }, [localeObject]);
    const onSelect = (date: Date) => {
        setDate(dayjs(date));
    };

    const fetchDataSchedule = async (date: Dayjs) => {
        try {
            setIsLoading(true)
            const yearCurrent = date.year();
            const monthCurrent = date.month() + 1
            const response: any = await delayResult(
                callApi(`${isStudent ? paths.api_calendar_student : paths.api_calendar_teacher}?year=${yearCurrent}&month=${monthCurrent}`),
                300
            );
            setIsLoading(false)
            if (response.code === 200) {
                setSchedules(response.data || [])
            }
        }catch (e) {
            console.log(e);
            setIsLoading(false)
        }

    }

    useEffect(() => {
        fetchDataSchedule(date)
    }, [date])

    const dateRender = (currentDate: Dayjs, info: CellRenderInfo<Dayjs>) => {
        const { today } = info
        const activeToday = currentDate.isSame(today, "date");
        const dataSchedules = schedules.filter(item => dayjs(item.start_date).isSame(currentDate, 'date'));
        const dataFilter = isCheck ? dataSchedules.filter(item => item.examStatus === STATUS_CODE_EXAM.UPCOMING) : dataSchedules;
        const countPlan = dataFilter.length
        const activeMonth = currentDate.isSame(date, 'months')
        const onClickShowSchedulesInfo = () => {
            if (!activeMonth) return
            setDataModal({
                isShow: true,
                data: dataFilter,
                date: currentDate
            })
        }
        return (
            <div
                className={classNames(styles.cellDate, {
                    [styles.todayCellDate]: activeToday
                })}
                onClick={onClickShowSchedulesInfo}
            >
                <div className={styles.contentDate}><span>{currentDate.date()}</span></div>
                <div className={classNames(styles.countSchedules, {
                    [styles.hiddenText]: !countPlan
                })}><span>{countPlan} {t("calendar-page")["schedule"]}</span></div>
            </div>
        );
    };
    const onCloseModal = () => {
        setDataModal(prevState => ({...prevState, isShow: false}));
    }

    const dayjsConfig = useMemo(() => ({
        ...dayjsGenerateConfig,
        locale: {
            ...dayjsGenerateConfig.locale,
            getWeekFirstDay: () => 1,
            format: (locale: string, date: Dayjs, format: string) => date.format("DD/MM/YYYY")
        }
    }), []);

    return (
        <>
            <div className={styles.calendarContainer}>
                <div className={styles.datePicker}>
                    <DatePicker
                        className={styles.datePickerContent}
                        cleanable={false}
                        locale={{ formattedMonthPattern: 'MM/yyyy' }}
                        oneTap
                        format="MM/yyyy"
                        value={new Date(date.format("YYYY-MM"))}
                        onSelect={onSelect}
                    />
                    <div className={styles.actionFilter}>
                        <label className={styles.text}>
                            <Checkbox checked={isCheck} disabled={isLoading} onChange={() => setIsCheck(!isCheck)} /> {t("calendar-page")["checkbox-condition-content"]}
                        </label>
                    </div>
                </div>
                {isLoading
                    ?
                    (
                        <div className={styles.loadingContainer}>
                            <Loader backdrop={false} vertical content="loading..." />
                        </div>
                    )
                    :
                    <PickerPanel<Dayjs>
                        className={styles.calendarWrapper}
                        generateConfig={dayjsConfig}
                        locale={localeObject}
                        pickerValue={date}
                        cellRender={dateRender}
                        mode="date"
                        picker="date"
                        value={dayjs()}
                        tabIndex={null}
                    />
                }
            </div>
            <ModalShowScheduleInfo data={dataModal} onClose={onCloseModal}/>
        </>
    )
}

