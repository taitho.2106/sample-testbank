import React from "react";
import Link from "next/link";

import useTranslation from "../../hooks/useTranslation";
import Button from "../Common/Controls/Button";
import {paths} from "../../interfaces/constants";
import {Desktop, Mobile} from "../Common/Media";

import Logo from "../../assets/icons/logo.svg";

import styles from "./SignUp/SignUpSuccess.module.scss";

const SuccessPage = ({title = '', description = ''}) => {
    const {t} = useTranslation()

    return (
        <div className={styles.signUpSuccessWrapper}>
            <Mobile>
                <div className={styles.bannerSuccess}>
                    <img src={"images/authentication/success-center-mobile.png"} alt=""/>
                </div>
            </Mobile>
            <div className={styles.top}>
                <Logo/>
                <h3>
                    {title}
                </h3>
                <Desktop>
                    <p>{description}</p>
                </Desktop>
                <Mobile><p>{description}</p></Mobile>
            </div>
            <div className={styles.form}>
                <Link href={paths.signIn}>
                    <Button>{t('auth')['back-home']}</Button>
                </Link>
            </div>
        </div>
    )
}
export default SuccessPage;