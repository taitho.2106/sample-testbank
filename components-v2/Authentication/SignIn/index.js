import React, {useContext, useEffect, useState} from 'react'
import * as Yup from 'yup'
import classNames from 'classnames'
import {useRouter} from 'next/router'
import {signIn} from 'next-auth/client'
import {FormikContext, useFormik} from 'formik'
import Link from 'next/link'

import AuthLayout from '../index'
import InputTextField from '../../Common/Form/InputTextField'
import Button from '../../Common/Controls/Button'
import useTranslation from '../../../hooks/useTranslation'
import BaseForm from '../../Common/Controls/BaseForm'
import {LoginContext} from '../../../interfaces/contexts'
import {paths} from '../../../interfaces/constants'

import Logo from '../../../assets/icons/logo.svg'
import FacebookIcon from '../../../assets/icons/fb.svg'
import GoogleIcon from '../../../assets/icons/google.svg'
import EyeIcon from '../../../assets/icons/eye.svg'

import styles from './SignIn.module.scss'
import useDevices from '@/hooks/useDevices'

const SignIn = () => {
    const {csrfToken} = useContext(LoginContext)
    const { t, locale } = useTranslation()
    const { query } = useRouter()
    const { isMobile } = useDevices();

    const [error, setError] = useState('')
    const onSubmit = async (values) => {
        const result = await signIn('credentials', {
            redirect: false,
            email: values.email.trim(),
            password: values.password.trim(),
            callbackUrl: query?.next || '',
            locale
        })
        if (result.ok) {
            if (!result.error) {
                if (isMobile && !query?.next) {
                    window.location.pathname = '/';
                } else {
                    window.location.reload();
                }
            } else {
                setError(result.error)
            }
        }
    }

    const validationSchema = () => {
        return Yup.object().shape({
            email: Yup.string().trim().required(t("auth")['email-required']),
            password: Yup.string()
                .required(t('auth')['password-required'])
        })
    }

    const formikBag = useFormik({
        initialValues: {email: '', password: ''},
        onSubmit,
        validationSchema,
        validateOnBlur: true
    })

    return (
        <AuthLayout image={'/images/authentication/bg-left.png'}>
            <div className={styles.signInWrapper}>
                <div className={styles.top}>
                    <Logo/>
                    <h3>
                        {`${t('auth')['welcome']} \n i-Test`}
                    </h3>
                </div>
                <FormikContext.Provider value={formikBag}>
                    {error ? <p className='error-feedback'>{error}</p> : null}
                    <BaseForm className={styles.form}>
                        <input name='csrfToken' type='hidden' defaultValue={csrfToken}/>
                        <InputTextField
                            name='email'
                            autoFocus
                            className={styles.inputField}
                            placeholder={t('auth')['email-pld']}
                            autoComplete='email'/>
                        <InputTextField
                            type='password'
                            name='password'
                            className={styles.inputField}
                            autoComplete='password'
                            placeholder={t("auth")['password']}/>
                        <Button buttonType={'submit'}>
                            {t('sign-in')}
                        </Button>
                        <Link href={paths.forgotPassword}>
                            <Button type='none' buttonType={'button'}>
                                {t('auth')['forgot-password']}?
                            </Button>
                        </Link>
                    </BaseForm>
                </FormikContext.Provider>
                <div className={styles.bottom}>
                    {/*<p>Hoặc đăng ký bằng</p>*/}
                    {/*<div className={styles.externalSignUp}>*/}
                    {/*    <span className={classNames(styles.icon, styles.facebook)}>*/}
                    {/*        <FacebookIcon />*/}
                    {/*    </span>*/}
                    {/*    <span className={classNames(styles.icon)}>*/}
                    {/*        <GoogleIcon />*/}
                    {/*    </span>*/}
                    {/*</div>*/}
                    <div className={styles.signUp}>
                        {t('auth')['description']}
                        <Link href={paths.signUp}>
                            <Button type={'none'}>
                                {t('auth')['register']}
                            </Button>
                        </Link>
                    </div>
                </div>
            </div>
        </AuthLayout>
    )
}

export default SignIn