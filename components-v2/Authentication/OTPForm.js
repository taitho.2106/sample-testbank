import React, { useContext } from "react";
import classNames from "classnames";
import {FormikContext} from "formik";

import {LoginContext} from "../../interfaces/contexts";
import BaseForm from "../Common/Controls/BaseForm";
import Button from "../Common/Controls/Button";

import useTranslation from "../../hooks/useTranslation";
import {numberRegex} from "../../interfaces/constants";

import Logo from "../../assets/icons/logo.svg";

import styles from "./OTPForm.module.scss";

const OTPForm = ({error, formikBag, countdown, reSendOtp, title, otpCount = 6}) => {
    const {csrfToken} = useContext(LoginContext)
    const {t} = useTranslation()
    const {setFieldValue, values, errors: formikError, handleSubmit, touched} = formikBag

    const submitOtp = (focusIndex) => {
        const otpText = values.otp.join('');
        if (focusIndex === otpCount && otpText.length === otpCount) {
            handleSubmit()
        }
    }

    const updateOtpIndex = (index, value) => {
        const newOtp = [ ...values.otp ];
        newOtp[index] = value;

        setFieldValue('otp', newOtp)
        return;
    }

    const onKeyUpOtp = (ev) => {
        const {value, tabIndex} = ev.target;
        if (ev.key === 'Tab' || ev.key === 'Control' || (!value && ev.key !== "Delete" && ev.key !== "Backspace")) return;

        if (ev.key !== "Delete" && ev.key !== "Backspace") {
            const next = tabIndex + 1;

            if (next <= otpCount) {
                ev.target.form.elements[next].focus();
            }
        } else {
            const prev = Math.max(1, tabIndex - 1);

            ev.target.form.elements[prev].focus();
            ev.target.form.elements[prev].select();
        }

        updateOtpIndex(tabIndex - 1, value)
        submitOtp(tabIndex)
    }

    const onChangeValue = (e) => {
        let { value, tabIndex } = e.target;
        if (!numberRegex.test(value)) {
            value = '';
        }

        updateOtpIndex(tabIndex - 1, value);
    }

    const onPaste = (e) => {
        e.preventDefault();
        const currentIndex = e.target.tabIndex - 1;

        const pastedData = e.clipboardData
            .getData('text/plain')
            .split('');

        const newOtp = [ ...values.otp ];
        const maxFillIndex = Math.min(otpCount - currentIndex, pastedData.length);
        let focusTabIndex = currentIndex;
        for (let i = 0; i < maxFillIndex; i++) {
            const currentValue = pastedData[i];
            if (numberRegex.test(currentValue)) {
                focusTabIndex++;
                newOtp[currentIndex + i] = currentValue;
            } else {
                newOtp[currentIndex + i] = '';
                break;
            }
        }
        setFieldValue('otp', newOtp);
        e.target.form.elements[focusTabIndex].focus();
        submitOtp(focusTabIndex);
    }

    const onFocus = (e) => {
        e.target.select();
    }

    return (
        <div className={styles.otpFormWrapper}>
            <div className={styles.top}>
                <Logo/>
                <h3>{title}</h3>

                <p>{t('OTP-form')['msg']}</p>
                <b>{values.email}</b>
            </div>
            <FormikContext.Provider value={formikBag}>
                <BaseForm className={styles.form}>
                    <div className={styles.groupOtpInput}>
                        <input name='csrfToken' type='hidden' defaultValue={csrfToken}/>
                        {Array.from(Array(6).keys()).map((item, index) => {
                            const currentIndex = index + 1;
                            const fieldName = `otp${currentIndex}`;
                            const currentFieldValue = values.otp[index];
                            // const isError = (formikError.otp || {})[fieldName] && touched.otp[fieldName];
                            return (
                                <input
                                    key={index}
                                    name={fieldName}
                                    tabIndex={index + 1}
                                    maxLength={1}
                                    type={'tel'}
                                    autoFocus={index === 0}
                                    value={currentFieldValue}
                                    onChange={onChangeValue}
                                    onPaste={onPaste}
                                    className={classNames({
                                        [styles.hasNoValue]: !currentFieldValue,
                                        [styles.hasError]: error
                                    })}
                                    onFocus={onFocus}
                                    autoComplete={fieldName}
                                    onKeyUp={onKeyUpOtp}/>
                            )
                        })}
                    </div>
                    <p className='error-feedback'>{error}</p>
                </BaseForm>
            </FormikContext.Provider>

            <div className={styles.bottom}>
                <p dangerouslySetInnerHTML={{__html: t(t('OTP-form')['confirm'], [`<span>${countdown.minutes.toString().padStart(2, '0')}:${countdown.seconds.toString().padStart(2, '0')}</span>`])}}></p>
                <Button type="none" onClick={reSendOtp}>{t('auth')['otp-resend']}</Button>
            </div>
        </div>
    )
}
export default OTPForm;