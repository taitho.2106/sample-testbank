import { useFormik } from "formik";
import { useRouter } from "next/router";
import { useEffect, useState } from 'react';
import * as Yup from "yup";

import { paths } from "../../../api/paths";
import useTranslation from "../../../hooks/useTranslation";
import {
    USER_ROLES,
    emailRegExp,
    optionRole,
    passwordRegex,
    phoneRegExp1,
    whiteSpaceRegex
} from "../../../interfaces/constants";
import { OPT_TIME_MINUTES } from "../../../utils/constant";
import OTPForm from "../OTPForm";
import SuccessPage from "../SuccessPage";
import AuthLayout from '../index';
import SignUpForm from "./SignUpForm";

const SignUpProcess = builder => {
    const { t } = useTranslation()
    const {query} = useRouter();
    const [error, setError] = useState('')

    const [currentProcess, setCurrentProcess] = useState('register')
    const [timeCountdown, setTimeCountdown] = useState({minutes: OPT_TIME_MINUTES, seconds: 0});

    async function makeOtp() {
        const res = await fetch(paths.api_users_otp, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({user_name: formikBag.values.email.trim()}),
        })
        const json = await res.json()

        return !(json.error || !res.ok);
    }

    const onSubmit = async (values) => {

        setError('')
        if (currentProcess === 'register') {
            const res = await fetch(paths.api_users_user_exist, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    email: values.email.trim(),
                    phone: values.phone,
                }),
            })
            const json = await res.json()
            if (json.isExist) {
                formikBag.setFieldError(json.field, json.message);
            } else {
                setCurrentProcess('otp')
                const isSuccess = makeOtp()
                if (!isSuccess) {
                    setError(t('auth')['error-mess'])
                }
            }
        } else if (currentProcess === 'otp') {
            const res = await fetch(paths.api_users_otp, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    user_name: values.email.trim(),
                    otp_code: values.otp.join(''),
                }),
            })
            const json = await res.json()
            if (json.error || !res.ok) {
                setError(json?.message || t('auth')['error-otp'])
            } else {
                const formData = {
                    email: values.email.trim(),
                    phone: values.phone.trim(),
                    password: values.password.trim(),
                    user_role_id: values.user_role_id,
                    full_name: !!values.full_name.length ?
                        values.full_name :
                        values.user_role_id  === USER_ROLES.Teacher ?
                            `${t('role')['teacher']} ${values.phone}` :
                            `${t('role')['student']} ${values.phone}`,
                    province_id: values.province_id
                }
                const res = await fetch(paths.api_users_register, {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify(formData),
                })
                const json = await res.json()

                if (json.error || !res.ok) {
                    setError(json?.message || t('auth')['error-valid'])
                } else {
                    setCurrentProcess('success')
                }
            }
        }
    }

    const otpValidation = () => {
        let otp = {};
        Array.from(Array(6).keys()).map((item) => {
            otp[`otp${item + 1}`] = Yup.string().required();
        })

        return currentProcess === 'otp' ? otp : {};
    }

    const validationSchema = () => {
        return Yup.object().shape({
            email: Yup.string().trim()
                .required(t(t('formikForm')['required'],['Email']))
                .matches(emailRegExp, t(t('formikForm')['incorrect'],['Email'])),
            password: Yup.string()
                .required(t('auth')['password-required'])
                .matches(whiteSpaceRegex, t('auth')['new-pass-space'])
                .matches(passwordRegex, t('auth')['new-pass-valid'])
                .min(8, t('auth')['new-pass-min']),
            phone: Yup.string()
                    .required(t(t('formikForm')['required'],[t('excel-row')['phone']]))
                    .length(10, t('phone-validation'))
                    .matches(phoneRegExp1, t('phone-incorrect')),
            rePassword: Yup.string()
                .required(t('auth')['confirm-pass'])
                .oneOf([Yup.ref('password'), ''], t('auth')['correct-pass']),
            policy: Yup.boolean()
                .oneOf([true], t('auth')['rule']),
            user_role_id: Yup.number().required(t(t('formikForm')['required'],[t('common')['role']])),
            full_name: Yup.string()
                    .matches(whiteSpaceRegex, t(t('formikForm')['white-space-regex'], [t('common')['full-name']]))
                    .max(45, t(t('formikForm')['max-length'], [t('common')['full-name'], '45']))
        })
    }
    
    const roleIDRegisterValid = optionRole.findIndex(item => item.value === +query?.role) !== -1
    
    const formikBag = useFormik({
        initialValues: {
            email: '',
            password: '',
            user_role_id: roleIDRegisterValid ? +query?.role : "",
            phone: query.phone || '',
            rePassword: '',
            policy: false,
            full_name: '',
            otp: ['', '', '', '', '', ''],
            province_id: 0
        },
        onSubmit,
        validationSchema,
        validateOnBlur: true
    })

    const reSendOtp = () => {
        const {seconds, minutes} = timeCountdown
        if (minutes === 0 && seconds === 0) {
            const isSuccess = makeOtp()
            if (!isSuccess) {
                setError(t('auth')['not-send-otp'])
            } else {
                setError('');
                setTimeCountdown({minutes: OPT_TIME_MINUTES, seconds: 0});
            }
        }
    }

    useEffect(() => {
        const myInterval = setInterval(() => {
            const {seconds, minutes} = timeCountdown
            if (currentProcess === "otp") {
                if (seconds > 0) {
                    setTimeCountdown({...timeCountdown, seconds: seconds - 1})
                }
                if (seconds === 0) {
                    if (minutes === 0) {
                        clearInterval(myInterval)
                    } else {
                        setTimeCountdown({minutes: minutes - 1, seconds: 59})
                    }
                }
            }
        }, 1000)
        return () => {
            clearInterval(myInterval)
        }
    }, [currentProcess, timeCountdown])

    const renderLeftImage = () => {
        switch (currentProcess) {
            case "register":
                return 'images/authentication/bg-left-signup.png'
            case "otp":
                return "images/authentication/bg-left-otp.png"
            case "success":
                return "images/authentication/bg-left-signup-success.png"
            default:
                return 'images/authentication/bg-left-signup.png'
        }
    }

    const renderSignUpProcess = () => {
        switch (currentProcess) {
            case "register":
                return <SignUpForm error={error} formikBag={formikBag} setCurrentProcess={setCurrentProcess}/>
            case "otp":
                return <OTPForm error={error} formikBag={formikBag} countdown={timeCountdown} reSendOtp={reSendOtp}
                                title={t('common')['create-new-account']}/>
            case "success":
                return <SuccessPage title={t('common')['create-new-account']}
                                    description={t('auth')['true-register']}/>
            default:
                return '';
        }
    }

    return (
        <AuthLayout image={renderLeftImage()}>
            {renderSignUpProcess()}
        </AuthLayout>
    )
}

export default SignUpProcess;