import { FormikContext } from 'formik';
import Link from 'next/link';
import { useContext } from 'react';

import Logo from '../../../assets/icons/logo.svg';
import Button from '../../../components-v2/Common/Controls/Button';
import useTranslation from "../../../hooks/useTranslation";
import { optionRole, paths } from "../../../interfaces/constants";
import { LoginContext } from '../../../interfaces/contexts';
import BaseForm from '../../Common/Controls/BaseForm';
import InputTextField from '../../Common/Form/InputTextField';
import RadioField from "../../Common/Form/RadioField";
import SelectField from "../../Common/Form/SelectField";

import { SingleSelectPicker } from '@/componentsV2/Common/SingleSelectPicker/SingleSelectPicker';
import useProvinces from '@/hooks/useProvince';
import styles from './SignUpForm.module.scss';

const SignUpForm = ({error, formikBag}) => {
    const {csrfToken} = useContext(LoginContext)
    const {t, locale, subPath} = useTranslation()
    const { optionProvinces } = useProvinces();

    return (
        <div className={styles.signUpWrapper}>
            <div className={styles.top}>
                <Logo/>
                <h3>
                    {t("common")["create-new-account"]}
                </h3>
                <p className='error-feedback'>{error}</p>
            </div>
            <FormikContext.Provider value={formikBag}>
                <BaseForm className={styles.form}>
                    <input name='csrfToken' type='hidden' defaultValue={csrfToken}/>
                    <SelectField
                        options={optionRole.map(item => ({...item, label: t('role')[item.label]}))}
                        className={styles.selectField}
                        menuClassName={styles.menu}
                        placeHolder={t('common')['role']}
                        titleDropdown={t('auth')['pld-role']}
                        name={'user_role_id'}/>
                    <InputTextField
                        name='full_name'
                        className={styles.inputField}
                        placeholder={t('common')['full-name']}
                        autoComplete='off'
                    />
                    <div className={styles.groupPhoneEmail}>
                        <InputTextField
                            name='phone'
                            autoFocus={false}
                            className={styles.inputField}
                            placeholder={t('phone')}
                            autoComplete='off'
                            onlyNumber={true}
                            type='tel'
                            ignoreSpace
                        />
                        <InputTextField
                            name='email'
                            className={styles.inputField}
                            placeholder='Email'
                            autoComplete='off'
                            ignoreSpace
                        />
                    </div>
                    <SingleSelectPicker
                        name='province_id'
                        isProvince={true}
                        placement={"bottomStart"}
                        style={{ height: 56, width: "100%" }}
                        className={styles["filter-box"]}
                        data={optionProvinces || []}
                        inputSearchShow={true}
                        label={t('common')['province']}
                        onChange={(newValue) => {
                            formikBag.setFieldValue("province_id", newValue)
                        }}
                    />
                    <InputTextField
                        type='password'
                        name='password'
                        className={styles.inputField}
                        autoComplete='off'
                        placeholder={t('common')['password']}/>
                    <InputTextField
                        type='password'
                        name='rePassword'
                        className={styles.inputField}
                        autoComplete='off'
                        placeholder={t("auth")['confirm-new-pass']}/>
                    <RadioField
                        name="policy"
                        className={styles.inputField}
                        label={t(t('common')['agreement'],[`<span>${t('auth')['term']}</span>`,`<a href="${subPath}${paths.policySecurity}" target='_blank'><span>${t('auth')['privacy-policy']}</span></a>`])}
                        />
                    <Button buttonType={'submit'}>
                        {t('sign-up')}
                    </Button>
                    {/*<Link href={paths.forgotPassword}>*/}
                    {/*    <Button type='none' buttonType={'button'}>*/}
                    {/*        Quên mật khẩu?*/}
                    {/*    </Button>*/}
                    {/*</Link>*/}
                </BaseForm>
            </FormikContext.Provider>
            <div className={styles.bottom}>
                {/*<p>Hoặc đăng ký bằng</p>*/}
                {/*<div className={styles.externalSignUp}>*/}
                {/*        <span className={classNames(styles.icon, styles.facebook)}>*/}
                {/*            <FacebookIcon/>*/}
                {/*        </span>*/}
                {/*    <span className={classNames(styles.icon)}>*/}
                {/*            <GoogleIcon/>*/}
                {/*        </span>*/}
                {/*</div>*/}
                <div className={styles.signUp}>
                    {t("common")['did-you-have-account']}?
                    <Link href={paths.signIn}>
                        <Button type={'none'}>
                            {t('sign-in-menu')}
                        </Button>
                    </Link>
                </div>
            </div>
        </div>
    )
}

export default SignUpForm;