import React from 'react'

import styles from './AuthLayout.module.scss'

const AuthLayout = ({ children, image }) => {
    return (
        <div className={styles.authLayoutWrapper}>
            <div className={styles.left}>
                <img src={image} alt='' />
            </div>
            <div className={styles.right}>
                {children}
            </div>
        </div>
    )
}

export default AuthLayout