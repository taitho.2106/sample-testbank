import React, {useContext, useEffect, useState} from "react";
import Logo from "../../../assets/icons/logo.svg";
import {FormikContext} from "formik";
import BaseForm from "../../Common/Controls/BaseForm";

import InputTextField from "../../Common/Form/InputTextField";

import Button from "../../Common/Controls/Button";


import styles from "./ForgotPasswordForm.module.scss";
import useTranslation from "../../../hooks/useTranslation";
import {LoginContext} from "../../../interfaces/contexts";

const ForgotPasswordForm = ({error, formikBag}) => {
    const {t} = useTranslation()
    const {csrfToken} = useContext(LoginContext)

    return (
        <div className={styles.forgotPasswordFormWrapper}>
            <div className={styles.top}>
                <Logo/>
                <h3>
                    {t('common')['forgot-passowrd']}
                </h3>
                <p>{t('common')['please-enter-your-email']}</p>
                <p className='error-feedback'>{error}</p>
            </div>

            <FormikContext.Provider value={formikBag}>
                <BaseForm className={styles.form}>
                    <input name='csrfToken' type='hidden' defaultValue={csrfToken}/>
                    <InputTextField
                        name='email'
                        autoFocus
                        className={styles.inputField}
                        placeholder={t('enter-email')}
                        autoComplete='off'/>
                    <Button buttonType={'submit'}>
                        {t('common')['continue']}
                    </Button>
                </BaseForm>
            </FormikContext.Provider>
        </div>
    )
}

export default ForgotPasswordForm;