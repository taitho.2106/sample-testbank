import React, {useContext} from "react";

import styles from "./ChangePasswordForm.module.scss";
import Logo from "../../../assets/icons/logo.svg";
import {FormikContext} from "formik";
import BaseForm from "../../Common/Controls/BaseForm";
import InputTextField from "../../Common/Form/InputTextField";
import Button from "../../Common/Controls/Button";
import {LoginContext} from "../../../interfaces/contexts";
import useTranslation from "../../../hooks/useTranslation";

const ChangePasswordForm = ({error, formikBag}) => {
    const { t } = useTranslation()
    const {csrfToken} = useContext(LoginContext)

    return (
        <div className={styles.changePasswordFormWrapper}>
            <div className={styles.top}>
                <Logo/>
                <h3>
                    {t('auth')["create-new-pass"]}
                </h3>
                <p className='error-feedback'>{error}</p>
            </div>
            <FormikContext.Provider value={formikBag}>
                <BaseForm className={styles.form}>
                    <input name='csrfToken' type='hidden' defaultValue={csrfToken}/>
                    <InputTextField
                        name='newPassword'
                        autoFocus
                        className={styles.inputField}
                        type={"password"}
                        placeholder={t('auth')["new-pass"]}
                        autoComplete='off'/>
                    <InputTextField
                        name='newPasswordConfirm'
                        className={styles.inputField}
                        type={"password"}
                        placeholder={t('auth')["confirm-new-pass"]}
                        autoComplete='off'/>
                    <Button buttonType={'submit'}>
                        {t('common')['submit-btn']}
                    </Button>
                </BaseForm>
            </FormikContext.Provider>
        </div>
    )
}

export default ChangePasswordForm;