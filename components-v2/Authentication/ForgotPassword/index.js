import React, {useEffect, useState} from "react";
import * as Yup from "yup";
import {useFormik} from "formik";

import AuthLayout from "../index";
import OTPForm from "../OTPForm";
import SuccessPage from "../SuccessPage";
import {OPT_TIME_MINUTES} from "../../../utils/constant";
import ForgotPasswordForm from "./ForgotPasswordForm";
import {paths} from "../../../api/paths";
import {emailRegExp, passwordRegex, whiteSpaceRegex} from "../../../interfaces/constants";
import ChangePasswordForm from "./ChangePasswordForm";
import useTranslation from "../../../hooks/useTranslation";

const STEPS = {
    FORGOT_PASSWORD: 'forgot-password',
    OTP: 'otp',
    NEW_PASSWORD: 'new-password',
    SUCCESS: 'success',
}
const ForgotPassword = () => {
    const { t } = useTranslation()
    const [currentProcess, setCurrentProcess] = useState(STEPS.FORGOT_PASSWORD);
    const [error, setError] = useState('');
    const [timeCountdown, setTimeCountdown] = useState({minutes: OPT_TIME_MINUTES, seconds: 0});

    async function makeOtp() {
        const res = await fetch(paths.api_users_otp, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({user_name: formikBag.values.email.trim()}),
        })
        const json = await res.json()

        return !(json.error || !res.ok);
    }

    const onSubmit = async (values) => {
        setError('')
        if (currentProcess === STEPS.FORGOT_PASSWORD) {
            const res = await fetch(paths.api_users_user_exist, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ email: values.email }),
            })
            const json = await res.json()
            if (json.isExist) {
                setError(null)
                setCurrentProcess(STEPS.OTP)
                const isSuccess = makeOtp()
                if (!isSuccess) {
                    setError(t('auth')['error-mess'])
                }
            } else {
                formikBag.setFieldError('email', t('auth')['error-email'])
            }
        } else if (currentProcess === STEPS.OTP) {
            const res = await fetch(paths.api_users_otp, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    user_name: values.email.trim(),
                    otp_code: values.otp.join(''),
                }),
            })
            const json = await res.json()
            if (json.error || !res.ok) {
                setError(json?.message || t('auth')['error-otp'])
            } else {
                setCurrentProcess(STEPS.NEW_PASSWORD)
            }
        } else if (currentProcess === STEPS.NEW_PASSWORD) {
            const res = await fetch(paths.api_users_forgot_password, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    password: values.newPassword.trim(),
                    user_name: values.email.trim(),
                }),
            })
            const json = await res.json()
            if (json.error || !res.ok) {
                setError(json?.message || t('auth')['error-valid'])
            } else {
                setCurrentProcess(STEPS.SUCCESS)
            }
        }
    }

    const reSendOtp = () => {
        const {seconds, minutes} = timeCountdown
        if (minutes === 0 && seconds === 0) {
            const isSuccess = makeOtp()
            if (!isSuccess) {
                setError(t('auth')['not-send-otp'])
            } else {
                setError('');
                setTimeCountdown({minutes: OPT_TIME_MINUTES, seconds: 0});
            }
        }
    }

    const validationSchema = () => {
        return Yup.object().shape({
            email: Yup.string().required(t(t('formikForm')['required'],['Email']))
                .matches(emailRegExp, t(t('formikForm')['incorrect'],['Email'])),
        }).when({
            is: () => currentProcess === STEPS.NEW_PASSWORD,
            then:(schema) => {
              return schema.concat(Yup.object().shape({
                    newPassword: Yup.string()
                        .required(t('auth')['new-pass-required'])
                        .matches(whiteSpaceRegex, t('auth')['new-pass-space'])
                        .matches(passwordRegex, t('auth')['new-pass-valid'])
                        .min(8, t('auth')['new-pass-min']),
                    newPasswordConfirm: Yup.string()
                        .required(t('auth')['confirm-pass'])
                        .oneOf([Yup.ref('newPassword'), ''], t('auth')['correct-pass']),
                }))
            },
        });
    }

    const formikBag = useFormik({
        initialValues: {email: '', otp: ['', '', '', '', '', ''], newPassword: '', newPasswordConfirm: ''},
        onSubmit,
        validationSchema
    })

    useEffect(() => {
        const myInterval = setInterval(() => {
            const {seconds, minutes} = timeCountdown
            if (currentProcess === "otp") {
                if (seconds > 0) {
                    setTimeCountdown({...timeCountdown, seconds: seconds - 1})
                }
                if (seconds === 0) {
                    if (minutes === 0) {
                        clearInterval(myInterval)
                    } else {
                        setTimeCountdown({minutes: minutes - 1, seconds: 59})
                    }
                }
            }
        }, 1000)
        return () => {
            clearInterval(myInterval)
        }
    }, [currentProcess, timeCountdown])

    useEffect(() => {
        if (currentProcess === STEPS.NEW_PASSWORD) {
            formikBag.setTouched({
                newPassword: false,
                newPasswordConfirm: false,
            });
        }
    }, [ currentProcess ]);

    const renderLeftImage = () => {
        switch (currentProcess) {
            case STEPS.OTP:
            case STEPS.FORGOT_PASSWORD:
                return "images/authentication/bg-left-otp.png"
            case STEPS.SUCCESS:
                return "images/authentication/bg-left-signup-success.png"
            default:
                return 'images/authentication/bg-left-otp.png'
        }
    }

    const renderSignUpProcess = () => {
        switch (currentProcess) {
            case STEPS.FORGOT_PASSWORD:
                return <ForgotPasswordForm error={error} formikBag={formikBag} setCurrentProcess={setCurrentProcess}/>
            case STEPS.OTP:
                return <OTPForm error={error} formikBag={formikBag} countdown={timeCountdown} reSendOtp={reSendOtp}
                                title={t('auth')['forgot-password']}/>
            case STEPS.NEW_PASSWORD:
                return <ChangePasswordForm error={error} formikBag={formikBag}/>
            case STEPS.SUCCESS:
                return <SuccessPage title={t('auth')['true-forgot-pass']}
                                    description={t('auth')['true-forgot-pass-des']}/>
            default:
                return '';
        }
    }

    return (
        <AuthLayout image={renderLeftImage()}>
            {renderSignUpProcess()}
        </AuthLayout>
    )
}

export default ForgotPassword