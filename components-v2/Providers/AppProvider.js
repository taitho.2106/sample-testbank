import React, { useState } from 'react';

import { AppContext } from '../../interfaces/contexts';

const AppProvider = ({ children }) => {
    const [isExpandSidebar, setIsExpandSidebar] = useState(true)
    const [openingCollapseList, setOpenCollapseList] = useState([])
    const [isFullScreenLoading, setIsFullScreenLoading] = useState(false);
    const [dataPlan, setDataPlan] = useState(null)
    const [listPlans, setListPlans] = useState([])
    const [orderData, setOrderData] = useState({});
    const [listPlansStudent, setListPlansStudent] = useState([])
    const [listComboServicePackage, setListComboServicePackage] = useState([])

    return (
        <AppContext.Provider
            value={{
                isExpandSidebar: {
                    state: isExpandSidebar,
                    setState: setIsExpandSidebar,
                },
                openingCollapseList: {
                    state: openingCollapseList,
                    setState: setOpenCollapseList,
                },
                fullScreenLoading: {
                    state: isFullScreenLoading,
                    setState: setIsFullScreenLoading
                },
                dataPlan,
                setDataPlan,
                listPlans,
                orderData,
                setListPlans,
                setOrderData,
                listPlansStudent,
                setListPlansStudent,
                listComboServicePackage,
                setListComboServicePackage,
            }}
        >
            {children}
        </AppContext.Provider>
    );
};

export default AppProvider;