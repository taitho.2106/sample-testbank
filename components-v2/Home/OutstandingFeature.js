import React, { useState } from 'react'
import classNames from 'classnames'

import { Desktop, Mobile } from '@/componentsV2/Common/Media'
import CustomCarousel from '@/componentsV2/Common/CustomCarousel'
import Container from '@/componentsV2/Common/Container'
import { TABS } from '@/interfaces/constants'
import { outstandingFeatures } from '@/componentsV2/Home/data/out-standing-feature'
import { generateUniqueId } from '@/utils/index'
import useTranslation from '../../hooks/useTranslation'

import Overview from '../../assets/icons/overview.svg'
import Student from '../../assets/icons/student.svg'
import Parent from '../../assets/icons/parent.svg'
import Teacher from '../../assets/icons/teacher.svg'
import School from '../../assets/icons/school.svg'

import styles from './OutstandingFeature.module.scss'

const OutstandingFeature = () => {
    const { t } = useTranslation()
    const tabItems = [
        {
            tabName: 'overview',
            tabValue: TABS.OVERVIEW,
            icon: "/images/home/outstanding-feature/overview-icon.png",
            activeIcon: "/images/home/outstanding-feature/overview-icon-active.png"
        },
        {
            tabName: 'student',
            tabValue: TABS.STUDENT,
            icon: "/images/home/outstanding-feature/student-icon.png",
            activeIcon: "/images/home/outstanding-feature/student-icon-active.png"
        },
        {
            tabName: 'parent',
            tabValue: TABS.PARENTS,
            icon: "/images/home/outstanding-feature/parent-icon.png",
            activeIcon: "/images/home/outstanding-feature/parent-icon-active.png"
        },
        {
            tabName: 'teacher',
            tabValue: TABS.TEACHER,
            icon: "/images/home/outstanding-feature/teacher-icon.png",
            activeIcon: "/images/home/outstanding-feature/teacher-icon-active.png"
        },
        {
            tabName: 'school',
            tabValue: TABS.SCHOOL,
            icon: "/images/home/outstanding-feature/school-icon.png",
            activeIcon: "/images/home/outstanding-feature/school-icon-active.png"
        },
    ]

    const [activeTab, setActiveTab] = useState(TABS.OVERVIEW)
    const [ positionTop, setPositionTop] = useState(0)

    const changeTopItem = (activeIndex) => {
        let topPosition = activeIndex * (68 + 40)

        setPositionTop(topPosition)
    }

    const switchTab = (tabName) => {
        setActiveTab(tabName)
    }

    const onClickTab = (item, index) => {
        changeTopItem(index)
        switchTab(item.tabValue)
    }

    const renderBottomControl = (slideControl) => {
        return (
            <div className={styles.dotWrapper}>
                {[...Array(slideControl.slideCount).keys()].map((item, index) => (
                    <div key={index}
                         className={classNames(styles.dotItem, {
                             [styles.active]: index === slideControl.currentSlide,
                             [styles.secondaryAtive]: index === slideControl.currentSlide && index % 2 === 0,
                         })}
                         onClick={() => slideControl.goToSlide(index)} />
                ))}
            </div>
        )
    }

    const isSecondaryColor = (positionTop / 108) % 2 === 0;

    return (
        <section className={classNames(styles.outstandingFeatureWrapper)} id={'feature'}>
            <h3>{t('outstanding-feature-title')}</h3>
            <h2>{t('outstanding-feature-des')}</h2>
            <Desktop>
                <Container>
                    <div className={styles.featureTabs}>
                        <div className={styles.tabs}>
                            <div className={styles.tabItems}>
                                <div className={classNames(styles.tabActive, {
                                    [styles.secondary]: isSecondaryColor
                                })} style={{
                                    top: `${positionTop}px`
                                }}/>
                                {tabItems.map((item, index) => (
                                    <button
                                        key={item.tabValue}
                                        className={classNames(styles.item, {
                                            [styles.active]: activeTab === item.tabValue,
                                            [styles.secondaryName]: activeTab === item.tabValue && isSecondaryColor,
                                        })}
                                        onClick={() => onClickTab(item, index)}>
                                        <div>
                                            <img src={activeTab === item.tabValue ? item.activeIcon : item.icon} alt=''/>
                                        </div>
                                        {t(item.tabName)}
                                    </button>
                                ))}
                            </div>
                            <div className={styles.tabContent}>
                                {outstandingFeatures.map((item) => {
                                    let currentStartIndex = item.startIndex
                                    return (
                                        <div key={generateUniqueId('feature')}
                                             className={classNames(styles.contentWrapper, {
                                                 [styles.active]: activeTab === item.tabName,
                                             })}>
                                            <img src={item.bgImage} alt=''/>
                                            {item.descriptions.map((des, index) => (
                                                <div key={generateUniqueId('des-item')}
                                                     className={classNames(styles.descriptionGroup, styles[`number${currentStartIndex++}`],{
                                                        [styles.secondaryGroup]: isSecondaryColor
                                                     }
                                                     )}>
                                                    <span><p>{index + 1}</p></span>
                                                    <p className={styles.description}>{t(des)}</p>
                                                </div>
                                            ))}
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>
                </Container>
            </Desktop>

            <Mobile>
                <div className={styles.featureTabs}>
                    <div className={styles.tabs}>
                        <div className={styles.tabContent}>
                            <CustomCarousel
                                // autoPlay={5000}
                                // isInfinity={true}
                                renderTopLeftControls={renderBottomControl}
                                renderBottomCenterControls={null}
                                renderCenterRightControls={null}
                                renderCenterLeftControls={null}>
                                {outstandingFeatures.map((item, index) => {
                                    return (
                                        <div key={generateUniqueId('feature')}>
                                            <button className={classNames(styles.item, {
                                                [styles.secondary]: index % 2 === 0,
                                                [styles.secondaryName]: index % 2 === 0,
                                            })}>
                                                <div>
                                                    <img src={tabItems[index].activeIcon} alt=""/>
                                                </div>
                                                {t(tabItems[index].tabName)}
                                            </button>
                                            <div className={classNames(styles.contentWrapper)}>
                                                <img src={item.bgImage} alt='' />
                                                {item.descriptions.map((des, index2) => (
                                                    <div key={generateUniqueId('des-item')}
                                                         className={classNames(styles.descriptionGroup, {
                                                            [styles.secondaryGroup]: index % 2 === 0
                                                         })}>
                                                        <span><p>{index2 + 1}</p></span>
                                                        <p className={styles.description}>{t(des)}</p>
                                                    </div>
                                                ))}
                                            </div>
                                        </div>
                                    )
                                })}
                            </CustomCarousel>
                        </div>
                    </div>
                </div>
            </Mobile>
        </section>
    )
}

export default OutstandingFeature
