import { Fragment, useCallback, useEffect, useMemo, useRef, useState } from "react";

import classNames from 'classnames';

import useCurrentUserPackage from '@/hooks/useCurrentUserPackage';
import useTranslation from '@/hooks/useTranslation';
import { PACKAGES, PACKAGES_FOR_STUDENT } from '@/interfaces/constants';
import { formatNumber } from '@/utils/index';

import ChevronIcon from '../../assets/icons/chevron-down.svg';
import TickFailedIcon from '../../assets/icons/tick-failed.svg';
import TickIcon from '../../assets/icons/tick.svg';
import { TABS } from "../../interfaces/constants";
import Button from '../Common/Controls/Button';
import { Desktop, Mobile } from '../Common/Media';
import { tableDatas, tableDatasStudent } from "./data/detailFeatureAndPrice";
import styles from './DetailFeatureAndPrice.module.scss';

const DetailFeatureAndPrice = ({activeTab}) => {
    const {t} = useTranslation();
    const { listPlans, listPlansStudent } = useCurrentUserPackage();
    const [isActive, setIsActive] = useState(true)
    const [subActive, setSubActive] = useState([])
    const headerStickyRef = useRef();
    
    const dataFeature = useMemo(() => {
        if (activeTab === TABS.TEACHER) return tableDatas
        return tableDatasStudent
    } , [activeTab])
    
    const dataPackage = useMemo(() => {
        if (activeTab === TABS.TEACHER) return listPlans
        return listPlansStudent
    }, [activeTab, listPlans])
    

    const toggleSubActive = (index) => {
        let arr = [...subActive]
        if (arr.includes(index))
            arr = arr.filter(item => item !== index)
        else {
            arr = [...arr, index]
        }

        setSubActive(arr)
    }

    const ColumnContent = useCallback(({ children, className }) => {
        return (
            <div className={classNames(styles.columnContent, className)}>
                {children}
            </div>
        )
    }, [])

    const RowContent = useCallback(({ children, className, notBorderBottom, style }) => {
        return (
            <div className={classNames(styles.rowContent, className)} style={style} not-border-bottom={notBorderBottom}>
                {children}
            </div>
        )
    }, [])

    useEffect(() => {
        const handleScroll = () => {
            if (headerStickyRef.current) {
                const offsetTop = headerStickyRef.current.getBoundingClientRect().top;

                if (offsetTop <= 97) {
                    headerStickyRef.current.classList.add(styles.sticky);
                } else {
                    headerStickyRef.current.classList.remove(styles.sticky);
                }
            }
        }
        document.addEventListener('scroll', handleScroll);

        return () => {
            document.removeEventListener('scroll', handleScroll);
        }
    }, []);

    const tableData = dataFeature.map(item => (
        {
            ...item,
            descriptions: item.descriptions.map(des => 
                des.map(col => ({...col, ...dataPackage.find(m => m.code === col.code)}))
            )
        }));
    
    const renderNameByCode = (item) => {
        switch(item.code){
            case PACKAGES.BASIC.CODE:
                return t('basic');
            case PACKAGES.ADVANCE.CODE:
                return t('medium');
            case PACKAGES.INTERNATIONAL.CODE:
                return t('advanced');
            case PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE:
                return t('student-basic-plan');
            case PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE:
                return t('student-medium-plan');
            case PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE:
                return t('student-advanced-plan');
            case PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE:
                return t('student-international-plan');
            default:
                return '';
        }
    }

    return (
        <div className={styles.detailFeatureAndPriceWrapper}>
            <Button type='none'
                onClick={() => setIsActive(!isActive)}
                iconRight={<ChevronIcon fill={'#7C68EE'} className={classNames(styles.chevronIcon, {
                    [styles.active]: isActive,
                })} />}
                className={styles.btnSeeMore}>
                {t('details-feature-title')}
            </Button>

            <Desktop>
                <div
                    id="detail-content"
                    className={classNames(styles.content, {
                        [styles.active]: isActive
                    })}
                >
                    <div className={classNames(styles.rowHeader, styles.rowSticky)} ref={headerStickyRef} id="test">
                        <div className={styles.columnHeader}>{t('service-pack')}</div>
                        {dataPackage?.map((item, index) => (
                            <div className={styles.columnHeader} key={index}>
                                {renderNameByCode(item)}
                                {
                                    activeTab === TABS.TEACHER ? (
                                        <>
                                            <p>{`${formatNumber(item?.price)}đ`} <span>/{t('month')}</span></p>
                                            <i>({t('application-time')})</i>
                                        </>
                                    ) : (
                                        <>
                                            <p
                                                className={classNames({
                                                    [styles.freePackage]: item?.price === 0
                                                })}
                                            >{item?.price === 0 ? t('free') : `${formatNumber(item?.price)}đ`}</p>
                                        </>
                                    )
                                }
                            </div>
                        ))}
                    </div>
                    <div className={styles.section}>
                        {dataFeature.map((item, index) => (
                            <Fragment key={index}>
                                {index > 0 ? 
                                    <div className={styles.rowHeader}>
                                        <div className={styles.columnHeader}>{t(`${item.name}`)}</div>
                                        {item.commingSoonText ? 
                                            <div className={styles.commingSoonWrapper}>
                                                <div className={styles.commingSoon}/>
                                                <div className={styles.text}>{t(`${item?.commingSoonText}`)}</div>
                                            </div>
                                            : null
                                        }
                                    </div>
                                    : null
                                }
                                {item.descriptions.map((des, desIndex) => {
                                    const isLast = index === dataFeature.length - 1 && desIndex === item.descriptions.length - 1;
                                    return (
                                        <RowContent key={desIndex} className={classNames({
                                            [styles.developing]: item.isDeveloping
                                        })} style={{margin: isLast ? '1px 0 0' : '1px 0'}}>
                                            {des.map((col, cIndex) => (
                                                <Fragment key={cIndex}>
                                                    {
                                                        typeof col.value === "string" ? (
                                                            <div className={!col.package ? styles.dot : ""}
                                                                dangerouslySetInnerHTML={{
                                                                    __html: t(`${col.value}`, col?.optional)
                                                                }}
                                                            />
                                                        ) : (
                                                            <div className={!col.package ? styles.dot : ""}>
                                                                {col.value ? <TickIcon /> : <TickFailedIcon />}
                                                            </div>
                                                        )
                                                    }
                                                </Fragment>
                                            ))}
                                        </RowContent>
                                    )
                                })}
                            </Fragment>
                        ))}
                    </div>
                </div>
            </Desktop>

            <Mobile>
                <div className={classNames(styles.content, {
                    [styles.active]: isActive,
                })}>
                    {
                        tableData.map((item, index) => (
                            <div className={styles.section} key={index}>
                                <div className={classNames(styles.rowHeader, {
                                    [styles.activeRowHeader]: subActive.includes(index)
                                })} onClick={() => toggleSubActive(index)}>
                                    <div className={styles.columnHeader}>{t(`${item.name}`)}</div>                                    
                                    <ChevronIcon fill={'#7C68EE'} className={classNames(styles.chevronIcon, {
                                        [styles.active]: subActive.includes(index),
                                    })} />
                                </div>
                                {subActive.includes(index) &&
                                    item.descriptions.map((des, desIdx) => {
                                        const rows = des.filter(item => item.code)
                                        return (
                                            <Fragment key={desIdx}>
                                               {index > 0 ?
                                                    <div className={classNames(styles.subTitleMobile, {
                                                        [styles.developing]: item.isDeveloping
                                                    })}
                                                        dangerouslySetInnerHTML={{
                                                            __html: t(`${des[0].value}`)
                                                        }}
                                                    />
                                                    : null
                                               }
                                                {rows.map((col, colIdx) => (
                                                    <div key={colIdx} className={classNames(styles.rowContent, {
                                                        [styles.developing]: item.isDeveloping
                                                    })} not-border-bottom={`${colIdx === rows.length - 1}`}>
                                                        <div className={styles.columnContent}>
                                                            {renderNameByCode(col)}
                                                            {
                                                                activeTab === TABS.TEACHER ? (
                                                                    <>
                                                                        <p>{`${formatNumber(col?.price)}đ`} <span>/{t('month')}</span></p>
                                                                        <i>({t('application-time')})</i>
                                                                    </>
                                                                ) : (
                                                                    <>
                                                                        <p
                                                                            className={classNames({
                                                                                [styles.freePackage]: col?.price === 0
                                                                            })}
                                                                        >{col?.price === 0 ? t('free') : `${formatNumber(col?.price)}đ`}</p>
                                                                    </>
                                                                )
                                                            }
                                                        </div>
                                                        {/*<div className={styles.columnContent}>*/}
                                                        {/*    {typeof col.value === 'string' ? col.value : col.value ? <TickIcon/> : <TickFailedIcon/>}*/}
                                                        {/*</div>*/}
                                                        {typeof col.value === "string" ? (
                                                            <div className={styles.columnContent}
                                                                 dangerouslySetInnerHTML={{
                                                                     __html: t(`${col.value}`, col?.optional),
                                                                 }}
                                                            >
                                                            </div>
                                                        ) : (
                                                            <div className={styles.columnContent}>
                                                                {col.value ? <TickIcon /> : <TickFailedIcon />}
                                                            </div>
                                                        )}
                                                    </div>
                                                ))}
                                                {item.commingSoonText ? 
                                                    <div className={styles.commingSoonWrapper} margin={`${desIdx !== item.descriptions.length - 1}`}>
                                                        <div className={styles.commingSoon}/>
                                                        <div className={styles.text}>{t(`${item?.commingSoonText}`)}</div>
                                                    </div>
                                                    : null
                                                }
                                            </Fragment>
                                        )
                                    })
                                }
                            </div>
                        ))
                    }
                </div>
            </Mobile>
        </div>
    )

}

export default DetailFeatureAndPrice