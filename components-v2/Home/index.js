import React, { useState } from 'react'

import { FormikContext, useFormik } from 'formik'
import { useSession } from "next-auth/client";
import dynamic from "next/dynamic";
import * as Yup from 'yup'

import LocationIcon from '@/assets/icons/location.svg'
import MailIcon from '@/assets/icons/mail.svg'
import CallIcon from '@/assets/icons/phone.svg'
import BaseForm from '@/componentsV2/Common/Controls/BaseForm'
import InputTextField from '@/componentsV2/Common/Form/InputTextField'
import TextAreaField from '@/componentsV2/Common/Form/TextAreaField'
import MetaWrapper from '@/componentsV2/Common/MetaWrapper'
import Banner from '@/componentsV2/Home/Banner'
import OutstandingFeature from '@/componentsV2/Home/OutstandingFeature'
import { emailRegExp, phoneRegExp1 } from '@/interfaces/constants'
import { paths } from 'api/paths';
import { callApi } from 'api/utils';

import { EMAIL_COMPANY } from "../../constants";
import useTranslation from '../../hooks/useTranslation'
import { USER_ROLES } from "../../interfaces/constants";
import { userInRight } from "../../utils";
import Button from '../Common/Controls/Button'
import ModalFeedback from '../Common/ModalFeedback';
import CardInfoPackage from "./Common/CardInfoPackage";
import styles from './Home.module.scss'
import Product from './Product';

const SubscribeComponent = dynamic(() => import("././Subscribe"), {ssr: false});

const Home = () =>{
    const [ session ] = useSession()
    const userInfo = session?.user
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const { t } = useTranslation()
    const [isShow, setIsShow] = useState(false)
    const onSubmit = async (values) => {
        const response = await callApi(paths.api_user_feedback,'post','token',values)
        if(response.isSuccess){
            setIsShow(true)
        }
    }

    const validationSchema = () => {
        return Yup.object().shape({
            name: Yup.string()
                .required(t('name-required'))
                .max(60, t('name-validation')),
            phone: Yup.string()
                .required(t('phone-required'))
                .length(10, t('phone-validation'))
                .matches(phoneRegExp1, t('phone-incorrect')),
            email: Yup.string()
                .required(t('email-required-1'))
                .max(80, t('email-validation'))
                .matches(emailRegExp, t('email-incorrect')),
            feedback: Yup.string()
                .required(t('feedback-required'))
                .max(255, t('feedback-validation')),
        })
    }

    const formikBag = useFormik({
        initialValues: {name: '', phone: '', email: '', feedback: ''},
        validationSchema,
        onSubmit,
        validateOnBlur: true,
    })

    
    return (
        <MetaWrapper>
            <div className={styles.homeWrapper}>
                {session && !isSystem && <div className={styles.card}><CardInfoPackage user={userInfo} isStudent={isStudent}/></div>}
                <Banner />
                <Product />
                <OutstandingFeature />
                <SubscribeComponent />
                {/*<CustomerReview />*/}

                <section className={styles.map} id={'contact'}>
                    <iframe
                        src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1959.6037148091982!2d106.6785536!3d10.7954191!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752929ffaa45e7%3A0xeacba3ee960d23a0!2sDAI%20TRUONG%20PHAT%20EDUCATION%20JSC!5e0!3m2!1sen!2s!4v1641433661908!5m2!1sen!2s'
                        width={'100%'}
                        height={'100%'}
                        allowFullScreen
                        loading='lazy'
                        style={{ border: 0, marginTop: -150 }}
                    />
                    <FormikContext.Provider value={formikBag}>
                        <BaseForm className={styles.form}>
                            <h3>
                                <span>{`${t('contact')} `}</span>
                                {t('with-us')}
                            </h3>
                            <div className={styles.inputField}>
                                <InputTextField
                                    name={'name'}
                                    placeholder={t('your', [t('name')])} />
                            </div>
                            <div className={styles.inputField}>
                                <InputTextField
                                    name={'phone'}
                                    placeholder={t('your', [t('phone-number')])} />
                            </div>
                            <div className={styles.inputField}>
                                <InputTextField
                                    name={'email'}
                                    placeholder={t('your', [t('email')])} />
                            </div>

                            <div className={styles.inputField}>
                                <TextAreaField
                                    name='feedback'
                                    rows={5}
                                    maxLength={255}
                                    showCountCharacter
                                    placeholder={t('let-us-know-your-question')} />
                            </div>
                            <div className={styles.contactOur}>
                                <div className={styles.contentContact}>
                                    <p>{t('contact-our-seller')}</p>
                                    <p><CallIcon />1800 6242</p>
                                </div>
                                <Button buttonType={'submit'}>{t('send')}</Button>
                            </div>
                            
                            <div className={styles.info}>
                                <h4>{t('company-name-new')}</h4>
                                <div className={styles.infoItem}>
                                    <div><LocationIcon /></div>
                                    <p>{t('company-address')}</p>
                                </div>
                                <div className={styles.infoItem}>
                                    <CallIcon />
                                    <p>1800 6242</p>
                                </div>
                                <div className={styles.infoItem}>
                                    <MailIcon />
                                    <p>{EMAIL_COMPANY}</p>
                                </div>
                            </div>
                        </BaseForm>
                    </FormikContext.Provider>
                </section>
                {/*<Desktop>*/}
                {/*    <SignupTrial />*/}
                {/*</Desktop>*/}
                {isShow && <ModalFeedback isShow={true} setIsShow={setIsShow} onReset={() => formikBag.handleReset()}/>}
            </div>
        </MetaWrapper>
    )
}

export default React.memo(Home)
