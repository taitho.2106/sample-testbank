import { useEffect, useMemo, useState } from "react";

import classNames from 'classnames';
import { useSession } from "next-auth/client";
import { useRouter } from "next/router";
import { Progress, Tooltip, Whisper } from "rsuite";

import BottomLine from '@/assets/icons/bottom-line.svg';
import IconFlashSale from '@/assets/icons/flash-sale.svg';
import LabelBestChoice from '@/assets/icons/label-best-choice.svg';
import TickIcon from '@/assets/icons/tick.svg';
import Container from '@/componentsV2/Common/Container';
import Button from '@/componentsV2/Common/Controls/Button';
import Select from '@/componentsV2/Common/Controls/Select';
import { Desktop, Mobile } from '@/componentsV2/Common/Media';
import { studentPricings } from '@/componentsV2/Home/data/price';
import useDevices from '@/hooks/useDevices';
import usePackages from '@/hooks/usePackages';
import useTranslation from '@/hooks/useTranslation';
import useCurrentUserPackage from "../../hooks/useCurrentUserPackage";

import {
    numberRegex, PACKAGES, PACKAGES_FOR_STUDENT,
    paths,
    phoneRegExp1,
    USER_ROLES
} from "../../interfaces/constants";
import { useDateNow } from "../../lib/swr-hook";
import { formatNumber, userInRight } from "../../utils";
import CustomCarousel from '../Common/CustomCarousel';
import DetailFeatureAndPrice from './DetailFeatureAndPrice';
import styles from './Subscribe.module.scss';
// import { dayjs } from "../../utils/date";
import dayjs from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';
import Clock from "../Common/CountDown/Clock";
dayjs.extend(isBetween)

const IncrementCounter = ({number, duration, isInViewport}) => {
    const [counter, setCounter] = useState(number)

    useEffect(() => {
        if (isInViewport) {
            let start = 0;
            // first three numbers from props
            const end = parseInt(number.substring(0, 3))
            // if zero, return
            if (start === end) return;

            // find duration per increment
            let totalMilSecDur = parseInt(duration);
            let incrementTime = (totalMilSecDur / end) * 1000;
            
            let timer = setInterval(() => {
                start += 1;
                setCounter(String(start) + number.substring(3))
                if (start === end) clearInterval(timer)
            }, incrementTime);
        }
    }, [number, duration, isInViewport]);

    return (
        <>{formatNumber(counter)}</>
    )
}

const TABS = {
    STUDENT: 'student',
    TEACHER: 'teacher',
    PARENTS: 'parent',
    SCHOOL: 'school',
}

const Subscribe = () => {
    const startTime = process.env.NEXT_PUBLIC_TIME_START_COMBO;
    const endTime = process.env.NEXT_PUBLIC_TIME_END_COMBO;
    const startTimeFlashSale = process.env.NEXT_PUBLIC_TIME_START_FLASH_SALE;
    const endTimeFlashSale = process.env.NEXT_PUBLIC_TIME_END_FLASH_SALE;
    const [session] = useSession();
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const {t} = useTranslation()
    const {push} = useRouter();
    const {isDesktop} = useDevices();
    const { dataPlan, getUpgradeAblePackages } = useCurrentUserPackage()
    const tabItems = [
        {
            label: 'student',
            value: TABS.STUDENT,
            icon: <img src="/images/home/outstanding-feature/student-icon.png" alt=''/>,
            disabled: false,
        },
        {
            label: 'parent',
            value: TABS.PARENTS,
            icon: <img src="/images/home/outstanding-feature/parent-icon.png" alt=''/>,
            disabled: true,
        },
        {
            label: 'teacher',
            value: TABS.TEACHER,
            icon: <img src="/images/home/outstanding-feature/teacher-icon.png" alt=''/>,
            disabled: false,
        },
        {
            label: 'school',
            value: TABS.SCHOOL,
            icon: <img src="/images/home/outstanding-feature/school-icon.png" alt=''/>,
            disabled: true,
        },
    ]
    const [activeTab, setActiveTab] = useState(TABS.TEACHER)
    const { data, mutate } = useDateNow();
    const TOTAL_SLOT_CAN_BUY = 50
    const [slot, setSlot] = useState(TOTAL_SLOT_CAN_BUY)
    useEffect(() => {
        if (session) {
            setActiveTab(isStudent ? TABS.STUDENT : TABS.TEACHER)
        }
    } , [session])
    
    const isTimeRunningComboServicePackage = useMemo(() => {
        return dayjs(data).isBetween(startTime, endTime, "day", "[]");
    } , [data])
    const isRunningFlashSale = useMemo(() => {
        return dayjs(data).isBetween(startTimeFlashSale, endTimeFlashSale, "day", "[]");
    } , [data])
    
    const activePlan = useMemo(() => {
        if (activeTab === TABS.TEACHER) return (isTimeRunningComboServicePackage || isRunningFlashSale) ? PACKAGES.COMBO.CODE : PACKAGES.ADVANCE.CODE;
        return PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE;
    }, [activeTab, isTimeRunningComboServicePackage, isRunningFlashSale]);
    
    const { packageDescriptions, packageDescriptionsForStudent } = usePackages(activeTab);
    const [error, setError] = useState('')
    const [values, setValues] = useState({phone: ''});

    const switchTab = (tabName) => {
        setActiveTab(tabName)
    }

    const findTextToHighLight = (text, highLightText) => {
        if (text && highLightText) {
            const re = new RegExp(highLightText, 'g')
            return text.replace(re, `<mark>${highLightText}</mark>`)
        }

        return text
    }

    const onBuyNow = (item) => {
        if (!item?.url.length || item.disabled || item.isHidden || getHiddenButton) return
        push(item.url).catch()
    }

    const onChangeValue = (e) =>{
        setError('')
        if(e.target.value && !numberRegex.test(e.target.value)) return

        setValues({phone: e.target.value})
    }

    function useIsInViewport(ref) {
        const [isIntersecting, setIsIntersecting] = useState(false);

        const observer = useMemo(
            () =>
                new IntersectionObserver(([entry]) =>
                    setIsIntersecting(entry.isIntersecting),
                ),
            [],
        );

        useEffect(() => {
            observer.observe(ref.current);

            return () => {
                observer.disconnect();
            };
        }, [ref, observer]);

        return isIntersecting;
    }

    // const isInViewport = useIsInViewport(ref)

    const scrollToElement = (elementId) => {
        const element = document.getElementById(elementId)
        //add offset to scroll
        element.style.scrollMargin = isDesktop ? '100px' : '77px'
        element.scrollIntoView({ behavior: 'smooth' })
    }

    const renderBottomControl = (slideControl) => {
        return (
            <div className={styles.dotWrapper}>
                
                {[...Array(slideControl.slideCount).keys()].map((item, index) => (
                    <div key={index}
                        className={classNames(styles.dotItem, {
                            [styles.active]: index === slideControl.currentSlide,
                            [styles.secondaryActive]: index === slideControl.currentSlide && index % 2 === 1,
                            [styles.thirdActive]: index === slideControl.currentSlide && index === slideControl.slideCount - 1
                        })}
                        onClick={() => slideControl.goToSlide(index)} />
                ))}
            </div>
        )
    }
    const getPropButton = (value) => {
        const upgradeAblePackagesArr = getUpgradeAblePackages()
        
        if (!session) {
            const conditionShowTextButton = value?.price !== 0
            return {
                text: conditionShowTextButton ? t('buy-now') : t('subscribe-now'),
                disabled: false,
                url: conditionShowTextButton ? paths.signIn : `${paths.signUp}?role=${USER_ROLES.Student}`,
                isHidden: false,
            }
        }
        
        if (value.code === PACKAGES.COMBO.CODE && !isStudent) {
            if (isRunningFlashSale) {
                return {
                    text: t('buy-now'),
                    disabled: dataDetailFlashSale.disabled || slot === 0,
                    url: session ? `${paths.payment}?type=buy-combo` : paths.signIn,
                    isHidden: false,
                }
            }
            return {
                text: t('buy-now'),
                disabled: !isTimeRunningComboServicePackage,
                url: session ? `${paths.payment}?type=buy-combo` : paths.signIn,
                isHidden: false,
            }
        }
        
        if(dataPlan?.code === value.code) {
            return {
                text: t('explore-now'),
                disabled: false,
                url: isStudent? paths.assignedUnitTest : paths.unitTest,
                isHidden: false,
            }
        }

        if(!isStudent && !!upgradeAblePackagesArr.length && upgradeAblePackagesArr.find(item => item?.code === value.code) && session?.user){
            return {
                text: t('upgrade'),
                disabled: false,
                url: `${paths.payment}?type=upgrade&itemId=${value?.id}`,
                isHidden: false,
            }

        }
        
        if (isStudent && dataPlan?.code !== value.code && value.price !== 0) {
            return {
                text: t('buy-now'),
                disabled: false,
                url: `${paths.payment}?type=upgrade&itemId=${value?.id}`,
                isHidden: false,
            }
        }

        return {
            text: activeTab === TABS.TEACHER ? t('buy-now') : t('subscribe-now'),
            disabled: true,
            url: "",
            isHidden: true,
        }
    }
    
    const getHiddenButton = useMemo(() => {
        if (!session) return false
        if (isSystem) return true
        if (isStudent && activeTab === TABS.TEACHER) return true
        return !isStudent && activeTab === TABS.STUDENT;
    }, [session, activeTab])
    const handleClickBtn = (item) => {
        if(!item?.url.length || item.disabled || item.isHidden || getHiddenButton) return
        push(item?.url).finally()
    }
    const onClickBuyMore = (e,item) => {
        e.preventDefault()
        e.stopPropagation()
        push(`${paths.payment}?itemId=${item.id}`).finally()
    }
    const dataSubscribe = activeTab === TABS.STUDENT ? packageDescriptionsForStudent : packageDescriptions
    
    const schedulesFlashSale = useMemo(() => {
        const date = dayjs(data).format('YYYY-MM-DD')
        if (!dayjs(data).isBetween(process.env.NEXT_PUBLIC_TIME_START_FLASH_SALE, process.env.NEXT_PUBLIC_TIME_END_FLASH_SALE, 'day', "[]")) {
            return false
        }
        if (dayjs(data).isSame(endTimeFlashSale, 'day')) {
            return dayjs(data).isBetween(`${date} 06:00`, `${date} 23:59`, "minute", "[]");
        }
        return dayjs(data).isBetween(`${date} 08:00`, `${date} 12:30`, "minute", "[]");
    }, [data, slot])
    
    const slotRandom = [
        {
            time: '08:00',
            totalSlot: 50,
        },
        {
            time: '08:15',
            totalSlot: 49,
        },
        {
            time: '08:30',
            totalSlot: 48,
        },
        {
            time: '08:45',
            totalSlot: 43,
        },
        {
            time: '09:00',
            totalSlot: 41,
        },
        {
            time: '09:15',
            totalSlot: 40,
        },
        {
            time: '09:30',
            totalSlot: 39,
        },
        {
            time: '09:45',
            totalSlot: 37,
        },
        {
            time: '10:00',
            totalSlot: 36,
        },
        {
            time: '10:15',
            totalSlot: 29,
        },
        {
            time: '10:30',
            totalSlot: 19,
        },
        {
            time: '10:45',
            totalSlot: 15,
        },
        {
            time: '11:00',
            totalSlot: 9,
        },
        {
            time: '11:15',
            totalSlot: 8,
        },
        {
            time: '11:30',
            totalSlot: 7,
        },
        {
            time: '11:45',
            totalSlot: 5,
        },
        {
            time: '12:00',
            totalSlot: 4,
        },
        {
            time: '12:15',
            totalSlot: 3,
        },
        {
            time: '12:30',
            totalSlot: 0,
        },
    ];
    
    const slotRandomEndFlash = [
        {
            time: '06:00',
            totalSlot: 50,
        },
        {
            time: '06:45',
            totalSlot: 49,
        },
        {
            time: '07:30',
            totalSlot: 49,
        },
        {
            time: '08:15',
            totalSlot: 48,
        },
        {
            time: '09:00',
            totalSlot: 41,
        },
        {
            time: '09:45',
            totalSlot: 40,
        },
        {
            time: '10:30',
            totalSlot: 39,
        },
        {
            time: '11:15',
            totalSlot: 37,
        },
        {
            time: '12:00',
            totalSlot: 36,
        },
        {
            time: '12:45',
            totalSlot: 29,
        },
        {
            time: '13:30',
            totalSlot: 25,
        },
        {
            time: '14:15',
            totalSlot: 23,
        },
        {
            time: '15:00',
            totalSlot: 21,
        },
        {
            time: '15:45',
            totalSlot: 20,
        },
        {
            time: '16:30',
            totalSlot: 16,
        },
        {
            time: '17:15',
            totalSlot: 15,
        },
        {
            time: '18:00',
            totalSlot: 12,
        },
        {
            time: '18:45',
            totalSlot: 11,
        },
        {
            time: '19:30',
            totalSlot: 10,
        },
        {
            time: '20:15',
            totalSlot: 9,
        },
        {
            time: '21:00',
            totalSlot: 8,
        },
        {
            time: '21:45',
            totalSlot: 4,
        },
        {
            time: '22:30',
            totalSlot: 3,
        },
        {
            time: '23:15',
            totalSlot: 1,
        },
        {
            time: '23:59',
            totalSlot: 0,
        },
    ];
    
    useEffect(() => {
        const canStartFlashSale = dayjs(data).isBetween(process.env.NEXT_PUBLIC_TIME_START_FLASH_SALE, process.env.NEXT_PUBLIC_TIME_END_FLASH_SALE, 'day', "[]")
        if (data && canStartFlashSale && slot) {
            const date = dayjs(data).format('YYYY-MM-DD')
            const endDateFlash = dayjs(data).isSame(endTimeFlashSale, 'day')
            const minuteAdd = endDateFlash  ? 45 : 15;
            const listSlot = endDateFlash ? slotRandomEndFlash : slotRandom
            
            const currentSlot = listSlot.find(item => {
                const startDate = dayjs(`${date} ${item.time}`).format("YYYY-MM-DD HH:mm")
                const endDate = dayjs(`${date} ${item.time}`).add(minuteAdd, 'minute').format("YYYY-MM-DD HH:mm")
                return dayjs(data).isBetween(startDate, endDate, "minute", "[)");
            })
            
            if (currentSlot) {
                setSlot(currentSlot?.totalSlot)
                const nextSlot = listSlot.find(item => {
                    const time = dayjs(`${date} ${currentSlot.time}`).add(minuteAdd, 'minute').format("HH:mm")
                    return item.time === time
                })
                if (nextSlot) {
                    const delay = dayjs(`${date} ${nextSlot?.time}`).diff(data, 'millisecond')
                    const timeout = setTimeout(() => {
                        setSlot(nextSlot.totalSlot)
                    }, delay)
                    
                    return () => clearTimeout(timeout)
                }
            }
        }
    }, [data]);
    
    const getDetailFlashSale = () => {
        const date = dayjs(data).format("YYYY-MM-DD")
        
        if (dayjs(data).isSame(endTimeFlashSale, 'day')) {
            if (dayjs(data).isBefore(`${date} 06:00`, 'second')) {
                return {
                    title: `06:00 ngày ${dayjs(data).format("DD/MM")}`,
                    countTime: dayjs(`${date} 06:00`).diff(data,'millisecond'),
                    disabled: true,
                    isSlot: 0,
                }
            }
            return {
                title: `06:00 ngày ${dayjs(data).format("DD/MM")}`,
                countTime: 0,
                disabled: false,
                isSlot: 0,
            }
        }
        
        if (dayjs(data).isBefore(`${date} 08:00`, 'second')) {
            return {
                title: `08:00 ngày ${dayjs(data).format("DD/MM")}`,
                countTime: dayjs(`${date} 08:00`).diff(data, 'millisecond'),
                disabled: true,
                isSlot: 0,
            }
        }
        
        if (dayjs(data).isBetween(`2023-09-05 12:30`,'2023-09-05 23:59', 'minute','(]')) {
            const nextDate = dayjs(data).add(1,'day').format("YYYY-MM-DD")
            const textNextDate =  dayjs(data).add(1,'day').format("DD/MM")
            return {
                title: `06:00 ngày ${textNextDate}`,
                countTime: dayjs(`${nextDate} 06:00`).diff(data, 'millisecond'),
                disabled: true,
                isSlot: 1,
            }
        }
        
        if (dayjs(data).isAfter(`${date} 12:30`, 'minute')) {
            const nextDate = dayjs(date).add(1,'day').format("YYYY-MM-DD")
            const textNextDate =  dayjs(date).add(1,'day').format("DD/MM")
            return {
                title: `08:00 ngày ${textNextDate}`,
                countTime: dayjs(`${nextDate} 08:00`).diff(data, 'millisecond'),
                disabled: true,
                isSlot: 1,
            }
        }
        return {
            title: `08:00 ngày ${dayjs(data).format('DD/MM')}`,
            countTime: 0,
            disabled: false,
            isSlot: 0,
        }
    }
    
    const dataDetailFlashSale = getDetailFlashSale()
    
    useEffect(() => {
        const listDateMutate = [
            '2023-09-01 00:00:00',
            '2023-09-01 08:00:00',
            '2023-09-01 12:31:00',
            '2023-09-02 00:00:00',
            '2023-09-02 08:00:00',
            '2023-09-02 12:31:00',
            '2023-09-03 00:00:00',
            '2023-09-03 08:00:00',
            '2023-09-03 12:31:00',
            '2023-09-04 00:00:00',
            '2023-09-04 08:00:00',
            '2023-09-04 12:31:00',
            '2023-09-05 00:00:00',
            '2023-09-05 08:00:00',
            '2023-09-05 12:31:00',
            '2023-09-06 00:00:00',
            '2023-09-06 06:00:00',
            '2023-09-07 00:00:00',
        ]
        let timeout;
        let delay = 0;
        for (let timeline of listDateMutate) {
            const diff = dayjs(timeline).diff(dayjs(data),'millisecond')
            if (diff > 0) {
                delay = diff;
                break;
            }
        }
        if (delay) {
            timeout = setTimeout(() => {
                mutate();
            }, delay);
        }
        return () => clearTimeout(timeout);
    }, [data]);
    
    return (
        <>
            <section className={classNames(styles.subscribeWrapper)} id={'pricing'}>
                <div className={styles.top}>
                    <h3>{t('subscribe-title')}</h3>
                        <h2 dangerouslySetInnerHTML={{__html: findTextToHighLight(t('try-i-test',['i-Test']), 'i-Test')}}/>
                        <Mobile>
                            <Container style={{ zIndex: 1 }}>
                                    <div style={{display: 'flex', justifyContent: 'flex-start'}}>
                                    <Select
                                        options={tabItems.filter(item => !item.disabled)}
                                        defaultValue={tabItems.find(item => item.value === activeTab)}
                                        onChange={(e) => switchTab(e.value)}
                                        // disable
                                        className={styles.dropdown}/>
                                </div>
                            </Container>
                        </Mobile>
                        <div className={styles.tabs}>
                            <Desktop>
                                <Container style={{
                                    maxWidth: 1280
                                }}>
                                    <div className={styles.tabItems}>
                                        {tabItems.map((item, index) => (
                                            <Whisper
                                                key={index}
                                                trigger="hover"
                                                placement={"top"}
                                                controlId={`control-id-topStart`}
                                                speaker={item.disabled ?
                                                    <Tooltip>{t('in-developing-feature')}</Tooltip> : <div></div>
                                                }>
                                                <button key={item.value} className={
                                                    classNames({
                                                        [styles.active]: activeTab === item.value,
                                                    })}
                                                    onClick={!item.disabled ? () => switchTab(item.value) : undefined}>
                                                    {t(item.label)}
                                                </button>
                                            </Whisper>
                                        ))}
                                    </div>
                                </Container>
                            </Desktop>
                            <div className={styles.background}>
                                <Container className={styles.container}>
                                    <Desktop>
                                        <div className={styles.tabContent} data-count={studentPricings[activeTab].length || 0}>
                                            {dataSubscribe.map((item, index) => {
                                                const btnContentDek = getPropButton(item)
                                                const isPurpleBg = session && (dataPlan?.code === item?.code) && (!btnContentDek.disabled)
                                                return (
                                                    <div
                                                        key={`${activeTab + index}`}
                                                        className={classNames(styles.pricingItem, styles.hover, {
                                                            [styles.active]: activePlan === item.code
                                                        })}
                                                        onClick={() => !session?.user && !item.isCustom ? onBuyNow(btnContentDek) : handleClickBtn(btnContentDek)}
                                                    >
                                                        <div className={styles.backgroundImage}
                                                             style={{
                                                                 backgroundImage: `url(${item.backgroundImage})`
                                                             }}>
                                                            {item.icon}
                                                            <span>{t(item.label)}</span>
                                                        </div>
                                                        {item.code === PACKAGES.COMBO.CODE ?
                                                            (
                                                                <div className={classNames(styles.title, styles.comboTitle)}>
                                                                    <div style={{margin: '15px 0'}}>
                                                                        <LabelBestChoice />
                                                                    </div>
                                                                   <span
                                                                       className={styles.note}
                                                                       dangerouslySetInnerHTML={{
                                                                       __html: item.lableTemp
                                                                   }}/>
                                                                    <h2 className={styles.priceCombo}>{formatNumber(item.price)}đ<span> /{t(item.unit)}</span></h2>
                                                                    <span className={styles.discountComboPrice}>{item.discountPrice}/{t(item.unit)}</span>
                                                                    {(isTimeRunningComboServicePackage || isRunningFlashSale) && <span className={styles.timeLineApply}>
                                                                        {item.timeLeftDescription}
                                                                    </span>}
                                                                </div>
                                                            )
                                                            :
                                                            (
                                                                <div className={styles.title}>
                                                                    <h2>{item.isFree ? t('free') : `${formatNumber(item.price)}đ`}
                                                                        <span>{activeTab === TABS.TEACHER && item.unit ? ` /${t(item.unit)}` : ""}</span>
                                                                    </h2>
                                                                    <i>{item.isFree ? `(${t('time-unlimited')})` : `(${t('application-time')})`}</i>
                                                                </div>
                                                            )
                                                        }
                                                        <BottomLine/>
                                                        <div className={classNames(styles.descriptions, {
                                                            [styles.comboDescription]: item.code === PACKAGES.COMBO.CODE,
                                                            [styles.flashSale]: isRunningFlashSale && item.code === PACKAGES.COMBO.CODE
                                                        })}
                                                        
                                                        >
                                                            {item.code === PACKAGES.COMBO.CODE && (
                                                                <div className={styles.desItem}
                                                                     style={{ textAlign: "center" }}>
                                                                    <div>
                                                                        <p className="break-line"
                                                                           dangerouslySetInnerHTML={{
                                                                            __html: item.note
                                                                        }}/>
                                                                    </div>
                                                                </div>
                                                            )}
                                                            {item.descriptions.map((des, index) => {
                                                                const labelOption = des.labelTranslateOption?.map(item => (t(item)))
                                                                const label = findTextToHighLight( t(des.label, labelOption), labelOption?.join(' '),)
                                                                return (
                                                                    <div key={index}
                                                                         className={styles.desItem}>
                                                                        <span><TickIcon/></span>
                                                                        <div>
                                                                            <p className="break-line"
                                                                               dangerouslySetInnerHTML={{ __html: label }}/>
                                                                            {des.option &&
                                                                                <p className={styles.option}>{t(des.option)}</p>}
                                                                            {des.lists?.length &&
                                                                                <ul>
                                                                                    {des.lists.map((list, index) => {
                                                                                        const labelOption = list.labelTranslateOption?.map(item => (t(item)))
                                                                                        const label = findTextToHighLight( t(list.label, labelOption), labelOption?.join(' '),)
                                                                                        return (
                                                                                            <li key={index} dangerouslySetInnerHTML={{__html: label}}></li>
                                                                                        );
                                                                                    })}
                                                                                </ul>}
                                                                        </div>
                                                                    </div>
                                                                )
                                                            })}
                                                        </div>
                                                        
                                                        {(isRunningFlashSale && item.code === PACKAGES.COMBO.CODE) ? (
                                                            <div className={styles.flashSaleContainer}>
                                                                {(slot === 0 || dataDetailFlashSale.isSlot) ? <span className={styles.expiredTime}>Đã hết lượt khuyến mãi hôm nay</span> : null}
                                                                <div className={styles.flashSaleBox}>
                                                                    <div className={styles.titleFlashSale}>
                                                                        <IconFlashSale />
                                                                        <span className={styles.nameFlash}>Flash Sale</span>
                                                                        <span className={styles.dayFlash}>{dataDetailFlashSale.title}</span>
                                                                    </div>
                                                                    {schedulesFlashSale ? (
                                                                        <div className={styles.runningActive}>
                                                                            <div className={styles.slotCanBuy}>
                                                                                <span>{t('remain')}</span>
                                                                                <div className={styles.slot}>
                                                                                    <span>{slot}</span></div>
                                                                                <span>{t('purchase')}</span>
                                                                            </div>
                                                                            <Progress.Line
                                                                                percent={(TOTAL_SLOT_CAN_BUY - slot) / TOTAL_SLOT_CAN_BUY * 100}
                                                                                showInfo={false} />
                                                                        </div>
                                                                    ) : (
                                                                        <div className={styles.upcomingActive}>
                                                                            <span>sẽ bắt đầu sau</span>
                                                                            <div className={styles.downTimer}>
                                                                                <Clock totalTime={dataDetailFlashSale.countTime} />
                                                                            </div>
                                                                        </div>
                                                                    )}
                                                                </div>
                                                            </div>
                                                        ) : <div className={styles.temp}></div>}
                                                        <div
                                                            style={{
                                                                margin: 'auto 0 0'
                                                            }}
                                                        >
                                                            <Button
                                                                className={classNames(styles.selectBtn, {
                                                                    [styles.disable]: btnContentDek.disabled,
                                                                    [styles.hidden]: getHiddenButton || btnContentDek.isHidden,
                                                                    [styles.backgroundPurple]: isPurpleBg
                                                                })}
                                                            >
                                                                {btnContentDek.text}
                                                            </Button>
                                                            <div
                                                                className={styles.buyMoreTurns}
                                                                onClick={(e) => isStudent && isPurpleBg && onClickBuyMore(e, item)}
                                                            >
                                                                {isStudent && isPurpleBg && dataPlan?.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE &&
                                                                    <i>{t('buy-more-uses')}</i>
                                                                } 
                                                            </div>
                                                        </div>
                                                    </div>
                                                )
                                            })}
                                        </div>
                                    </Desktop>
                                    <Mobile>
                                        <div className={classNames(styles.tabContent, styles.mobile)} data-count={studentPricings[activeTab].length || 0}>
                                            <CustomCarousel
                                                // autoPlay={8000}
                                                slidesToScroll={1}
                                                slidesToShow={1}
                                                // isInfinity={true}
                                                renderTopLeftControls={null}
                                                renderBottomCenterControls={renderBottomControl}
                                                renderCenterRightControls={null}
                                                renderCenterLeftControls={null}
                                                pauseOnHover={true}
                                            >
                                                {dataSubscribe.map((item, index) => {
                                                    const btnContent = getPropButton(item)
                                                    const isPurpleBg = session && (dataPlan?.code === item?.code) && (!btnContent.disabled)
                                                    return (
                                                        <div
                                                            key={`${activeTab + index}`}
                                                            className={classNames(styles.pricingItem, styles.hover, {
                                                                [styles.active]: activePlan === item.code
                                                            })}
                                                            onClick={() => !session?.user && !item.isCustom ? onBuyNow(btnContent) : handleClickBtn(btnContent)}
                                                        >
                                                            <div className={styles.backgroundImage}
                                                                 style={{
                                                                     backgroundImage: `url(${item.backgroundImage})`
                                                                 }}>
                                                                {item.icon}
                                                                <span>{t(item.label)}</span>
                                                            </div>
                                                            {item.code === PACKAGES.COMBO.CODE ?
                                                                (
                                                                    <div className={classNames(styles.title, styles.comboTitle)}>
                                                                        <div style={{margin: '15px 0'}}>
                                                                            <LabelBestChoice />
                                                                        </div>
                                                                        <span
                                                                           className={styles.note}
                                                                           dangerouslySetInnerHTML={{
                                                                               __html: item.lableTemp
                                                                           }}/>
                                                                        <h2 className={styles.priceCombo}>{formatNumber(item.price)}đ<span> /{t(item.unit)}</span></h2>
                                                                        <span className={styles.discountComboPrice}>{item.discountPrice}/{t(item.unit)}</span>
                                                                        {(isTimeRunningComboServicePackage || isRunningFlashSale) && <span className={styles.timeLineApply}>
                                                                            {item.timeLeftDescription}
                                                                        </span>}
                                                                    </div>
                                                                ) :
                                                                (
                                                                    <div className={styles.title}>
                                                                        <h2>{item.isFree ? t('free') : `${formatNumber(item.price)}đ`}
                                                                            <span>{activeTab === TABS.TEACHER && item.unit ? ` /${t(item.unit)}` : ""}</span>
                                                                        </h2>
                                                                        <i>{item.isFree ? `(${t('time-unlimited')})` : `(${t('application-time')})`}</i>
                                                                    </div>
                                                                )}
                                                            <BottomLine />
                                                            <div className={styles.descriptions}>
                                                                {item.code === PACKAGES.COMBO.CODE && (
                                                                    <div className={styles.desItem} style={{textAlign: 'center'}}>
                                                                        <div>
                                                                            <p className="break-line"
                                                                               dangerouslySetInnerHTML={{
                                                                                   __html: item.note
                                                                               }}/>
                                                                        </div>
                                                                    </div>
                                                                )}
                                                                {item.descriptions.map((des, index) => {
                                                                    const labelOption = des.labelTranslateOption?.map(item => (
                                                                        t(item)
                                                                    ))
                                                                    const label = findTextToHighLight(t(des.label,
                                                                        labelOption),
                                                                        labelOption?.join(' '),
                                                                    )
                                                                    return (
                                                                        <div key={index} className={styles.desItem}>
                                                                            <span><TickIcon /></span>
                                                                            <div>
                                                                                <p className="break-line" dangerouslySetInnerHTML={{ __html: label }} />
                                                                                {des.option &&
                                                                                    <p className={styles.option}>{t(des.option)}</p>}
                                                                                {des.lists?.length &&
                                                                                    <ul>
                                                                                        {des.lists.map((list, index) => {
                                                                                            const labelOption = list.labelTranslateOption?.map(item => (t(item)))
                                                                                            const label = findTextToHighLight( t(list.label, labelOption), labelOption?.join(' '),)
                                                                                            return (
                                                                                                <li key={index} dangerouslySetInnerHTML={{__html: label}}></li>
                                                                                            );
                                                                                        })}
                                                                                    </ul>}
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                })}
                                                            </div>
                                                            {(isRunningFlashSale && item.code === PACKAGES.COMBO.CODE) ? (
                                                                <div className={styles.flashSaleContainer}>
                                                                    {(slot === 0 || dataDetailFlashSale.isSlot) ? <span className={styles.expiredTime}>Đã hết lượt khuyến mãi hôm nay</span> : null}
                                                                    <div className={styles.flashSaleBox}>
                                                                        <div className={styles.titleFlashSale}>
                                                                            <IconFlashSale />
                                                                            <span className={styles.nameFlash}>Flash Sale</span>
                                                                            <span className={styles.dayFlash}>{dataDetailFlashSale.title}</span>
                                                                        </div>
                                                                        {schedulesFlashSale ? (
                                                                            <div className={styles.runningActive}>
                                                                                <div className={styles.slotCanBuy}>
                                                                                    <span>{t('remain')}</span>
                                                                                    <div className={styles.slot}>
                                                                                        <span>{slot}</span></div>
                                                                                    <span>{t('purchase')}</span>
                                                                                </div>
                                                                                <Progress.Line
                                                                                    percent={(TOTAL_SLOT_CAN_BUY - slot) / TOTAL_SLOT_CAN_BUY * 100}
                                                                                    showInfo={false} />
                                                                            </div>
                                                                        ) : (
                                                                            <div className={styles.upcomingActive}>
                                                                                <span>sẽ bắt đầu sau</span>
                                                                                <div className={styles.downTimer}>
                                                                                    <Clock totalTime={dataDetailFlashSale.countTime} />
                                                                                </div>
                                                                            </div>
                                                                        )}
                                                                    </div>
                                                                </div>
                                                            ) : <div className={styles.temp}></div>}
                                                            <Button
                                                                className={classNames(styles.selectBtn, {
                                                                    [styles.disable]: btnContent.disabled,
                                                                    [styles.hidden]: getHiddenButton || btnContent.isHidden,
                                                                    [styles.backgroundPurple]: isPurpleBg
                                                                })}
                                                            >
                                                                {btnContent.text}
                                                            </Button>
                                                            <div
                                                                className={styles.buyMoreTurns}
                                                                onClick={(e) => isStudent && isPurpleBg && onClickBuyMore(e,item)}
                                                            >
                                                                {isStudent && isPurpleBg && dataPlan?.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE &&
                                                                    <i>{t('buy-more-uses')}</i>
                                                                }
                                                            </div>
                                                        </div>
                                                    )
                                                })}
                                            </CustomCarousel>
                                        </div>
                                    </Mobile>
                                </Container>
                            </div>
                            <Container>
                                <div className={styles.detailFeature}>
                                    <DetailFeatureAndPrice activeTab={activeTab}/>
                                </div>
                                <section className={styles.trialGroup} id="trial">
                                    {!session?.user && (
                                        <>
                                            <h4>{t('trial-experience')}</h4>
                                            <form
                                                onSubmit={(e) => {
                                                    e.preventDefault()
    
                                                    if (values.phone?.length !== 10) {
                                                        setError(t('phone-validation'))
                                                        return
                                                    } else if (
                                                        !phoneRegExp1.test(values.phone)
                                                    ) {
                                                        setError(t('phone-incorrect'))
                                                        return
                                                    }
    
                                                    push({pathname: paths.signUp, query: {phone: e.target[0]?.value}})
                                                }}
                                            >
                                                <div className={styles.trial}>
                                                    <div className={error ? styles.error : ''}>
                                                        {isDesktop ?
                                                            <input rows={2}
                                                                placeholder={t('enter-email-to-try-free')}
                                                                maxLength={255}
                                                                onChange={onChangeValue}
                                                                value={values.phone}
                                                                name={"phone"}
                                                            />
                                                            :
                                                            <textarea rows={2}
                                                                placeholder={t('enter-email-to-try-free')}
                                                                maxLength={255}
                                                                onChange={onChangeValue}
                                                                value={values.phone}
                                                                        name={"phone"}/>
                                                        }
                                                        {error && <p className={"error-feedback"}>{error}</p>}
                                                    </div>
                                                    <Button type='primary' buttonType={"submit"}>{t('try-now')}</Button>
                                                </div>
                                            </form>
                                        </>
                                    )}
                                </section>
                            </Container>
                        </div>
                </div>
            </section>
            {/*<div className={styles.backgroundBottom}  ref={ref}>*/}
            {/*    <Container>*/}
            {/*        <div className={styles.bottom}>*/}
            {/*            <h3>{t('impressive-numbers')}</h3>*/}
            {/*            <div className={styles.content}>*/}
            {/*                <div className={styles.item}>*/}
            {/*                    <p><IncrementCounter number={"2467"} duration={"1"} isInViewport={isInViewport}/></p>*/}
            {/*                    <p>*/}
            {/*                        <span>{t('account')}</span><br/> {t('using')}*/}
            {/*                    </p>*/}
            {/*                </div>*/}
            {/*                <div className={classNames(styles.item)}>*/}
            {/*                    <p><IncrementCounter number={"2467"} duration={"1"} isInViewport={isInViewport}/></p>*/}
            {/*                    <p>*/}
            {/*                        <span>{`Học sinh & phụ huynh `}</span><br/> {t('trusting')}*/}
            {/*                    </p>*/}
            {/*                </div>*/}
            {/*                <div className={classNames(styles.item)}>*/}
            {/*                    <p><IncrementCounter number={"0470"} duration={"1"} isInViewport={isInViewport}/></p>*/}
            {/*                    <p>*/}
            {/*                        <span>{`${t('teacher')} & ${t('school')} `}</span><br/>{t('join-organizing-online-exam')}*/}
            {/*                    </p>*/}
            {/*                </div>*/}
            {/*                <div className={classNames(styles.item, 'order-1')}>*/}
            {/*                    <p><IncrementCounter number={"1364"} duration={"1"} isInViewport={isInViewport}/> +</p>*/}
            {/*                    <p>*/}
            {/*                        <span>{t('exam')}</span><br/> {t('created')}*/}
            {/*                    </p>*/}
            {/*                </div>*/}
            {/*            </div>*/}
            {/*        </div>*/}
            {/*    </Container>*/}
            {/*</div>*/}
        </>
    )
}

export default Subscribe