import React from 'react'

import { useRouter } from 'next/dist/client/router'

import useCurrentUserPackage from '../../../hooks/useCurrentUserPackage'
import { PACKAGES, PACKAGES_FOR_STUDENT, paths, PAYMENT_TYPE, USER_ROLES } from "../../../interfaces/constants";
import { cleanObject, userInRight } from '../../../utils'
import ButtonCustom from '../../Dashboard/SideBar/ButtonCustom/Button'
import styles from './CardInfoPackage.module.scss'
import { useUserInfo } from "../../../lib/swr-hook";
import IconCountBuyUnitTest from "../../../assets/icons/icon-count-buy.svg";
import classNames from "classnames";
import useTranslation from "../../../hooks/useTranslation";

const CardInfoPackage = ({ user, isStudent }) => {
    const { t } = useTranslation()
    const { push, query } = useRouter()
    const { dataPlan, textTimeLeft, isExpired, isHighestPackage, namePackage } = useCurrentUserPackage()
    const { userDisplayName } = useUserInfo()
    
    const renderNameByCode = (item) => {
        switch(item?.code){
            case PACKAGES.BASIC.CODE:
                return t('basic-plan');
            case PACKAGES.ADVANCE.CODE:
                return t('medium-plan');
            case PACKAGES.INTERNATIONAL.CODE:
                return t('advanced-plan');
            case PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE:
                return t('student-basic-plan');
            case PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE:
                return t('student-medium-plan');
            case PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE:
                return t('student-advanced-plan');
            case PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE:
                return t('student-international-plan');
            default:
                return '';
        }
    }

    const handleClick = (e, forceType) => {
        e.stopPropagation()
        const type = forceType ?? (isExpired ? PAYMENT_TYPE.UPGRADE : PAYMENT_TYPE.RENEW);
        push({
            pathname: paths.payment,
            query: cleanObject({ type }),
        })
    }

    return (
        <div className={styles.cardContainer}>
            <div className={styles.cardContent}>
                <div className={styles.title}>
                    <span>{t("hello")}, {userDisplayName}</span>
                </div>
                {isExpired || dataPlan.code === PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE ? (
                    <div className={styles.packageInfo}>
                        <span className={styles.des}>
                            {t("using")}{" "}
                            <span className={styles.package}>
                                {renderNameByCode(dataPlan)}
                            </span>
                        </span>
                        <span className={styles.des}><i>({textTimeLeft})</i></span>
                    </div>
                ) : (
                    <div className={styles.packageInfo}>
                        <span className={styles.des}
                              dangerouslySetInnerHTML={{
                                  __html: t(`remain`,[renderNameByCode(dataPlan)])
                              }}
                        >
                        </span>
                        <span className={styles.timer}>{textTimeLeft}</span>
                    </div>
                )}
                {isStudent && !isExpired && dataPlan.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE && (
                    <div className={styles.boxUnitTestInfo}>
                        <div className={styles.icon}>
                            <IconCountBuyUnitTest />
                        </div>
                        <div className={styles.content}>
                            <span>{t("remaining-used")}: </span>
                            <span className={styles.countUsing}>{dataPlan?.quantity} {t("turn-of-use")}</span>
                        </div>
                    </div>
                )}
                {
                    !isStudent ?
                        <>
                            <div className={styles.lineVertical}></div>
                            <div className={styles.buttonAction}>
                                {!isExpired && !isHighestPackage && (
                                    <ButtonCustom
                                        type="outline"
                                        onClick={e => handleClick(e, PAYMENT_TYPE.UPGRADE)}
                                    >
                                        {t("upgrade")}
                                    </ButtonCustom>
                                )}
                                {dataPlan?.code !== PACKAGES.TRIAL.CODE && (
                                    <ButtonCustom
                                        onClick={handleClick}
                                    >
                                        {isExpired ? t("buy-now") : t("extend")}
                                    </ButtonCustom>
                                )}
                            </div>
                        </>
                    :
                        <>
                            <div className={styles.lineVertical}></div>
                            <div className={styles.buttonAction}>
                                <ButtonCustom className={classNames(styles.btn, styles.upgrade)}
                                              onClick={e => handleClick(e, PAYMENT_TYPE.RENEW)}>
                                    {t(`${t("package")["title-combo-unit-test"]}`)}
                                </ButtonCustom>
                            </div>
                        </>
                }
            </div>
        </div>
    )
}

export default CardInfoPackage
