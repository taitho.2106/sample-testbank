import React from "react";

import classNames from "classnames";
import dayjs from "dayjs";
import isBetween from 'dayjs/plugin/isBetween';
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'

import Container from '@/componentsV2/Common/Container'
import Button from '@/componentsV2/Common/Controls/Button'
import { paths } from '@/interfaces/constants'

import useTranslation from '../../hooks/useTranslation'
import { USER_ROLES } from "../../interfaces/constants";
import { eventConfig } from "../../utils";
import CustomCarousel from '../Common/CustomCarousel'
import styles from './Banner.module.scss'
dayjs.extend(isBetween)

const Banner = () => {
    const { t } = useTranslation()
    const [session] = useSession();
    const user = session?.user
    const { push } = useRouter();
    const { startDate, endDate } = eventConfig;
    const isRunning = dayjs().isBetween(startDate, endDate, 'minute', '[]')
    const path = () => {
        switch (user.user_role_id) {
            case USER_ROLES.Student: return paths.assignedUnitTest
            case USER_ROLES.Teacher: return paths.unitTest
            default: return paths.unitTest
        }
    }
    const handleSignUp = () => {
        push(user ? path() : paths.signUp)
    }
    
    return (
        <div className={styles.background}>
            <Container>
                <section id="referral" className={styles.bannerSection}>
                    <CustomCarousel
                        renderCenterLeftControls={null}
                        renderCenterRightControls={null}
                        slidesToShow={1}
                        cellSpacing={20}
                    >
                        {isRunning &&
                            <div className={styles.bannerWrapper}>
                                <div className={styles.bannerLeft}>
                                    <div className={styles.featureTitle}>
                                        <div className={styles.label}>{t("back-to-school-banner")['program']}</div>
                                        <div className={styles.name}>Back To School <br />{t("back-to-school-banner")["cool-offer"]}</div>
                                        <div className={styles.labelAvailable}><label>{t("back-to-school-banner")["apply-period"]}</label></div>
                                    </div>
                                    <div className={classNames(styles.description, styles.flashSale)}>
                                        <span>
                                            {t("back-to-school-banner")["desc-1"]}
                                        </span>
                                        <ul>
                                            <li>{t("back-to-school-banner")["offer-1"]}</li>
                                            <li>{t("back-to-school-banner")["offer-2"]}</li>
                                        </ul>
                                        <span>
                                            {t("back-to-school-banner")["desc-2"]}
                                        </span>
                                    </div>
                                </div>
                                <div className={styles.bannerRight}>
                                    <img src={"/images/home/banner/banner-image-1410.png"} alt={""} />
                                </div>
                            </div>
                        }
                        <div className={styles.bannerWrapper}>
                            <div className={styles.bannerLeft}>
                                <div className={styles.featureTitle}>
                                    <div className={styles.label}>{t("feature-header")}</div>
                                    <div className={styles.name}>Learning Management System (LMS)</div>
                                    <div className={styles.labelAvailable}><label>{t("banner-intro")}</label> <span className={styles.iTest}>i-Test</span></div>
                                </div>
                                <div className={styles.description}><b>i-Test</b>&nbsp;{t("i-test-description")}</div>
                                <Button
                                    onClick={handleSignUp}
                                    className={styles.btnSignUp}
                                    type='primary'>{user ? t('explore-now') : t('sign-up-now')}</Button>
                            </div>
                            <div className={styles.bannerRight}>
                                <img src={'/images/home/banner/banner-right-2.png'} alt={''} />
                            </div>
                        </div>
                        <div className={styles.bannerWrapper}>
                            <div className={styles.bannerLeft}>
                                <div className={styles.title}>
                                    <h3>{t("banner-title1")}</h3>
                                    <h1>{t("banner-title2")} {t("banner-title3")}</h1>
                                    <h4>{t("banner-title4")}</h4>
                                </div>
                                <h4>{t("teacher")} – {t("student")} – {t("parent")} – {t("school")}</h4>
                                <p className={styles.description}>
                                    <b>i-Test</b>&nbsp;
                                    {t('i-test-description')}
                                </p>
                                <Button 
                                    onClick={handleSignUp}
                                    className={styles.btnSignUp}
                                    type='primary'>{user ? t('explore-now') : t('sign-up-now')}</Button>
                            </div>
                            <div className={styles.bannerRight}>
                                <img src={'/images/home/banner/banner-right.png'} alt={''} />
                            </div>
                        </div>
                    </CustomCarousel>
                </section>
            </Container>
        </div>
    )
}

export default Banner