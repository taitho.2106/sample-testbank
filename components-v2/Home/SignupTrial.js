import React from 'react'

import Button from '@/componentsV2/Common/Controls/Button'
import Container from '@/componentsV2/Common/Container'
import useTranslation from '../../hooks/useTranslation'

import ITestIcon from '../../assets/icons/i-test.svg'

import styles from './SignupTrial.module.scss'

const SignupTrial = () => {
    const { t } = useTranslation()
    return (
        <Container>
            <div className={styles.signupTrialWrapper}>
                <h3>{t('free-experience')}</h3>
                <ITestIcon />

                <div className={styles.submitForm}>
                    <input placeholder={t('enter-email-to-try-free')} />
                    <Button type='primary'>{t('try-now')}</Button>
                </div>
            </div>
        </Container>
    )
}

export default SignupTrial