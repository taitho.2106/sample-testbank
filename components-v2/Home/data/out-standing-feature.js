import { TABS } from '@/interfaces/constants'

export const outstandingFeatures = [
    {
        tabName: TABS.OVERVIEW,
        bgImage: '/images/home/outstanding-feature/overview.png',
        startIndex: 1,
        descriptions: ['overview-des1', 'overview-des2', 'overview-des3', 'overview-des4', 'overview-des5', 'overview-des6'],
    },
    {
        tabName: TABS.STUDENT,
        bgImage: '/images/home/outstanding-feature/student.png',
        startIndex: 7,
        descriptions: ['student-des1', 'student-des2', 'student-des3', 'student-des4', 'student-des5', 'student-des6'],
    },
    {
        tabName: TABS.PARENTS,
        bgImage: '/images/home/outstanding-feature/parent.png',
        startIndex: 13,
        descriptions: ['parent-des1', 'parent-des2', 'parent-des3', 'parent-des4'],
    },
    {
        tabName: TABS.TEACHER,
        bgImage: '/images/home/outstanding-feature/teacher.png',
        startIndex: 17,
        descriptions: ['teacher-des-1', 'teacher-des-2', 'teacher-des-3', 'teacher-des-4', 'teacher-des-5', 'teacher-des-6'],
    },
    {
        tabName: TABS.SCHOOL,
        bgImage: '/images/home/outstanding-feature/school.png',
        startIndex: 23,
        descriptions: ['school-des-1', 'school-des-2', 'school-des-3', 'school-des-4', 'school-des-5', 'school-des-6'],
    },
]