import BestChoiceIcom from '@/assets/icons/best-choice.svg'
import IconPackageAdvance from '@/assets/icons/ic-package-advance.svg'
import IconPackageBasic from '@/assets/icons/ic-package-basic.svg'
import IconPackageInternational from '@/assets/icons/ic-package-international.svg'
import IconPackage199 from '@/assets/icons/student-plan-199.svg'
import IconPackage299 from '@/assets/icons/student-plan-299.svg'
import IconPackage99 from '@/assets/icons/student-plan-99.svg'
import IconPackageFree from '@/assets/icons/student-plan-free.svg'
import { PACKAGES, PACKAGES_FOR_STUDENT } from "@/interfaces/constants"

export const studentPricings = {
    student: [
        {
            code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE,
            label: 'student-basic-plan',
            price: 'free',
            backgroundImage: '/images/home/subscribe/basic-pack.png',
            icon: <IconPackageFree />,
            isFree: true,
            descriptions: [
                {
                    label: "BEAT_FREE_EXAM-plan-label",
                    labelTranslateOption: ["BEAT_FREE_EXAM-total-unitTest"],
                    lists: [
                        {
                            label: "lists-label-1",
                            labelTranslateOption: ["TOEIC"],
                        },
                        {
                            label: "lists-label-2",
                        },
                        {
                            label: "lists-label-3",
                        }
                    ]
                },
                {
                    label: "general-student-label-1",
                },
                {
                    label: "general-student-label-2",
                },
                {
                    label: "general-student-label-3",
                },
                {
                    label: "general-student-label-4",
                },
            ],
        },
        {
            code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE,
            label: 'student-medium-plan',
            price: '99,000',
            unit: 'year',
            backgroundImage: '/images/home/subscribe/advance-pack.png',
            icon: <IconPackage99 />,
            isFree: false,
            descriptions: [
                {
                    label: "PREMIUM_PACKAGE-label-1",
                    labelTranslateOption: ["PREMIUM_PACKAGE-total-unitTest"]
                },
                {
                    label: "general-student-label-1",
                },
                {
                    label: "general-student-label-2",
                },
                {
                    label: "general-student-label-3",
                },
                {
                    label: "general-student-label-4",
                },
                {
                    label: "general-student-label-5",
                    labelTranslateOption: ["PREMIUM_PACKAGE-total-unitTest"]
                }
            ],
            isTrial: true,
        },
        {
            code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE,
            label: 'student-advanced-plan',
            price: '299,000',
            unit: 'year',
            isFree: false,
            backgroundImage: '/images/home/subscribe/international-pack.png',
            icon: <IconPackage299 />,
            descriptions: [
                {
                    label: "SUPER_INTELLIGENCE_CHALLENGE-label-1",
                    labelTranslateOption: ["SUPER_INTELLIGENCE_CHALLENGE-total-unitTest"]
                },
                {
                    label: "general-student-label-1",
                },
                {
                    label: "general-student-label-2",
                },
                {
                    label: "general-student-label-3",
                },
                {
                    label: "general-student-label-4",
                },
                {
                    label: "general-student-label-5",
                    labelTranslateOption: ["SUPER_INTELLIGENCE_CHALLENGE-total-unitTest"]
                }
            ],
            isTrial: true,
        },
        {
            code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE,
            label: 'student-international-plan',
            price: '199,000',
            unit: 'year',
            isFree: false,
            backgroundImage: '/images/home/subscribe/student-plan-advance.png',
            icon: <IconPackage199 />,
            descriptions: [
                {
                    label: "CERTIFICATE_MASTER-label-1",
                    labelTranslateOption: ["CERTIFICATE_MASTER-total-unitTest"]
                },
                {
                    label: "CERTIFICATE_MASTER-label-2",
                },
                {
                    label: "general-student-label-1",
                },
                {
                    label: "general-student-label-2",
                },
                {
                    label: "general-student-label-4",
                },
                {
                    label: "general-student-label-5",
                    labelTranslateOption: ["CERTIFICATE_MASTER-total-unitTest"]
                }
            ],
            isTrial: true,
        },
    ],
    parent: [
        {
            code: 'basic',
            label: 'basic-plan',
            price: 'free',
            descriptions: [
                {
                    label: 'parent-plan-label1',
                    option: 'parent-plan-option1',
                },
                {
                    label: 'parent-plan-label2',
                },
                {
                    label: 'parent-plan-label3',
                },
                {
                    label: 'parent-plan-label4',
                },
            ],
        },
        {
            code: PACKAGES.ADVANCE.CODE,
            label: 'advanced-plan',
            price: '999,000đ',
            unit: 'year',
            descriptions: [
                {
                    label: 'parent-plan-label5',
                },
                {
                    label: 'parent-plan-label2',
                },
                {
                    label: 'parent-plan-label3',
                },
                {
                    label: 'parent-plan-label4',
                },
            ],
        },
    ],
    teacher: [
        {
            code: PACKAGES.BASIC.CODE,
            label: 'basic-plan',
            price: '18,000đ',
            unit: 'month',
            isFree: false,
            isStart: true,
            icon: <IconPackageBasic />,
            backgroundImage: '/images/home/subscribe/basic-pack.png',
            descriptions: [
                {
                    label: 'teacher-plan-label15',
                    labelTranslateOption: ['500'],
                },
                {
                    label: 'teacher-plan-label6',
                    labelTranslateOption: ['limited'],
                },
                {
                    label: 'teacher-plan-label16',
                },
                {
                    label: 'teacher-plan-label3',
                },
                {
                    label: 'teacher-plan-label4',
                },
            ],
        },
        {
            code: PACKAGES.ADVANCE.CODE,
            label: 'medium-plan',
            price: '27,000đ',
            unit: 'month',
            isFree: false,
            isStart: true,
            icon: <IconPackageAdvance />,
            backgroundImage: '/images/home/subscribe/advance-pack.png',
            descriptions: [
                {
                    label: 'teacher-plan-label14',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label6',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label7',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label16',
                },
                {
                    label: 'teacher-plan-label3',
                },
                {
                    label: 'teacher-plan-label4',
                },
            ],
        },
        {
            code: PACKAGES.COMBO.CODE,
            label: 'Gói Hot Deal Back to school',
            lableTemp: '<b>4 gói quốc tế</b> chỉ với',
            price: '999,999đ',
            discountPrice: '2,400,000đ',
            icon: <BestChoiceIcom />,
            startTime: process.env.NEXT_PUBLIC_TIME_START_COMBO,
            endTime: process.env.NEXT_PUBLIC_TIME_END_FLASH_SALE,
            timeLeftDescription: '(Áp dụng từ 25/08 - 06/09/2023)',
            timeLineFlashSale: '(Áp dụng từ 01/9 – 06/9/2023)',
            note: 'Sở hữu ngay <b>4 Gói quốc tế cùng nhóm đồng nghiệp</b> chỉ với 999.999đ/năm',
            backgroundImage: '/images/home/subscribe/student-plan-advance.png',
            unit: 'year',
            descriptions: [
                {
                    label: 'teacher-plan-label15',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label6',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label7',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label17',
                },
                {
                    label: 'teacher-plan-label16',
                },
                {
                    label: 'teacher-plan-label3',
                },
                {
                    label: 'teacher-plan-label4',
                },
            ],
        },
        {
            code: PACKAGES.INTERNATIONAL.CODE,
            label: 'advanced-plan',
            price: '50,000đ',
            unit: 'month',
            isFree: false,
            isStart: true,
            icon: <IconPackageInternational />,
            backgroundImage: '/images/home/subscribe/international-pack.png',
            descriptions: [
                {
                    label: 'teacher-plan-label15',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label6',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label7',
                    labelTranslateOption: ['unlimited'],
                },
                {
                    label: 'teacher-plan-label17',
                },
                {
                    label: 'teacher-plan-label16',
                },
                {
                    label: 'teacher-plan-label3',
                },
                {
                    label: 'teacher-plan-label4',
                },
            ],
            isTrial: true,
        },
    ],
    school: [
        {
            code: 'basic',
            label: 'basic-plan',
            price: 'free',
            descriptions: [
                {
                    label: 'school-plan-label1',
                    option: 'school-plan-option1',
                },
                {
                    label: 'school-plan-label2',
                    lists: [
                        'school-plan-list-class',
                        'school-plan-list-teacher',
                        'school-plan-list-student',
                    ],
                },
            ],
        },
        {
            code: 'medium',
            label: 'medium-plan',
            price: '999,000đ',
            unit: 'year',
            descriptions: [
                {
                    label: 'school-plan-label1',
                    option: 'school-plan-option1',
                },
                {
                    label: 'school-plan-label3',
                },
                {
                    label: 'school-plan-label4',
                },
                {
                    label: 'school-plan-label5',
                },
            ],
            isTrial: true,
        },
        {
            code: PACKAGES.ADVANCE.CODE,
            label: 'advanced-plan',
            price: '999,000đ',
            unit: 'year',
            descriptions: [
                {
                    label: 'school-plan-label7',
                },
                {
                    label: 'school-plan-label3',
                },
                {
                    label: 'school-plan-label4',
                },
                {
                    label: 'school-plan-label5',
                },
                {
                    label: 'school-plan-label6',
                },
            ],
            isTrial: true,
        },
        // {
        //     code: 'plan-custom',
        //     label: 'plan-custom',
        //     price: 'plan-custom',
        //     descriptions: [
        //         {
        //             label: 'custom-plan-label1',
        //         },
        //         {
        //             label: 'custom-plan-label2',
        //         },
        //     ],
        // },
    ],
}