import { LIMIT_CLASS_PACKAGE_BASIC, LIMIT_CLASS_STUDENT_PACKAGE_BASIC } from "../../../constants";
import { PACKAGES, PACKAGES_FOR_STUDENT } from "../../../interfaces/constants";
import { formatNumber } from "../../../utils";

export const tableDatas = [
    {
        name: 'expiry-date',
        descriptions: [
            [
                {
                    package: "Thời hạn sử dụng",
                    value: 'expiry-date',
                },
                {
                    package: 'Phổ thông',
                    value: 'expiry-date-value',
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: 'expiry-date-value',
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: 'expiry-date-value',
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
        ],
        isNotPayment: true,
    },
    {
        name: 'general-header-feature-1',
        descriptions: [
            [
                {
                    value: 'teacher-feature-1-1',
                },
                {
                    package: 'Phổ thông',
                    value: "unlimited-cell",
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: "unlimited-cell",
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: "unlimited-cell",
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-1-2',
                },
                {
                    package: 'Phổ thông',
                    value: "limited-used",
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: "unlimited-used",
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: "unlimited-used",
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-1-3',
                },
                {
                    package: 'Phổ thông',
                    value: false,
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: true,
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: true,
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-1-4',
                },
                {
                    package: 'Phổ thông',
                    value: false,
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: false,
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: true,
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-1-5',
                },
                {
                    package: 'Phổ thông',
                    value: true,
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: true,
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: true,
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-1-6',
                },
                {
                    package: 'Phổ thông',
                    value: true,
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: true,
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: true,
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
        ],
    },
    {
        name: 'teacher-header-feature-2',
        isNotPayment: false,
        descriptions: [
            [
                {
                    value: 'teacher-feature-2-1',
                },
                {
                    package: 'Phổ thông',
                    value: `limit-managing-classes-students`,
                    optional: [`${formatNumber(LIMIT_CLASS_PACKAGE_BASIC, 0)}`, `${formatNumber(LIMIT_CLASS_STUDENT_PACKAGE_BASIC, 0)}`],
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: "unlimited-managing-classes-students",
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: "unlimited-managing-classes-students",
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-2-2',
                },
                {
                    package: 'Phổ thông',
                    value: `limit-classes`,
                    optional: [`${formatNumber(LIMIT_CLASS_PACKAGE_BASIC, 0)}`],
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: "unlimited-cell",
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: "unlimited-cell",
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-2-3',
                },
                {
                    package: 'Phổ thông',
                    value: `limit-learners`,
                    optional: [`${formatNumber(LIMIT_CLASS_STUDENT_PACKAGE_BASIC, 0)}`],
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: 'unlimited-cell',
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: 'unlimited-cell',
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-2-4',
                },
                {
                    package: 'Phổ thông',
                    value: "view-score",
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: "view-score-and-statistic",
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: "view-score-and-statistic",
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
        ],
    },
    {
        name: 'teacher-header-feature-3',
        isNotPayment: true,
        descriptions: [
            [
                {
                    value: 'teacher-feature-3-1',
                },
                {
                    package: 'Phổ thông',
                    value: true,
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: true,
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: true,
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-3-2',
                },
                {
                    package: 'Phổ thông',
                    value: false,
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: true,
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: true,
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-3-3',
                },
                {
                    package: 'Phổ thông',
                    value: "within-limited-unitTest",
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: "unlimited-cell",
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: "unlimited-cell",
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-3-4',
                },
                {
                    package: 'Phổ thông',
                    value: false,
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: true,
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: true,
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-3-5',
                },
                {
                    package: 'Phổ thông',
                    value: "within-limited-question",
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: "unlimited-cell",
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: "unlimited-cell",
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            [
                {
                    value: 'teacher-feature-3-6',
                },
                {
                    package: 'Phổ thông',
                    value: true,
                    code: PACKAGES.BASIC.CODE,
                },
                {
                    package: 'Nâng cao',
                    value: true,
                    code: PACKAGES.ADVANCE.CODE,
                },
                {
                    package: 'Quốc tế',
                    value: true,
                    code: PACKAGES.INTERNATIONAL.CODE,
                },
            ],
            // [
            //     {
            //         value: 'Chia sẻ & thu phí đề thi tự tạo',
            //     },
            //     {
            //         package: 'Phổ thông',
            //         value: false,
            //         code: PACKAGES.BASIC.CODE,
            //     },
            //     {
            //         package: 'Nâng cao',
            //         value: true,
            //         code: PACKAGES.ADVANCE.CODE,
            //     },
            //     {
            //         package: 'Quốc tế',
            //         value: true,
            //         code: PACKAGES.INTERNATIONAL.CODE,
            //     },
            // ],
        ],
    },
]

export const tableDatasStudent = [
    {
        name: "expiry-date",
        isNotPayment: true,
        descriptions: [
            [
                {
                    package: "Thời hạn sử dụng",
                    value: "expiry-date"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: "",
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: "expiry-date-value",
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: "expiry-date-value",
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: "expiry-date-value",
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ]
        ]
    },
    {
        name: "general-header-feature-1",
        isNotPayment: false,
        descriptions: [
            [
                {
                    value: "student-feature-1-1"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: "tests-count",
                    optional: [10],
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: "tests-count",
                    optional: [20],
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: "tests-count",
                    optional: [88],
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: "tests-count",
                    optional: [20],
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ],
            [
                {
                    value: "student-feature-1-2"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: "unitTest-from-textbook-supplement",
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: "unitTest-from-textbook-supplement",
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: "unitTest-from-textbook-supplement",
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: "unitTest-from-textbook-supplement-international",
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ],
            [
                {
                    value: "student-feature-1-3"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: false,
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ],
            [
                {
                    value: "student-feature-1-4"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: false,
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: false,
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: false,
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ],
            [
                {
                    value: "student-feature-1-5"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ],
            [
                {
                    value: "student-feature-1-6"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ]
        ]
    },
    {
        name: "student-header-feature",
        isNotPayment: false,
        descriptions: [
            [
                {
                    value: "student-feature-2-1"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ],
            [
                {
                    value: "student-feature-2-2"
                },
                {
                    package: "Phá đảo đề thi miễn phí",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                },
                {
                    package: "Gói xịn hời",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE
                },
                {
                    package: "Thử thách siêu trí tuệ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE
                },
                {
                    package: "Bá chủ mọi chứng chỉ",
                    value: true,
                    code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
                }
            ]
        ]
    }
];
