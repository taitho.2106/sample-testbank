export const dataFeedback = [
    {   
        name:'Cô Nguyễn Thị A',
        avatar:'/images/home/customer-service/image-avt.png',
        role:'Giáo viên Trường Tiểu học 1',
        contentFeedback:"i-Test có giao diện rất đẹp và dễ dùng. Có rất nhiều mẫu đề thi đạt chuẩn trong nước và quốc tế cho tôi lựa chọn và các bước soạn đề cũng rất dễ thực hiện. Từ khi sử dụng i-Test, công việc soạn đề thi, quản lý lịch thi lẫn theo dõi quá trình học tập của các em học sinh với tôi trở nên “dễ thở” hơn rất nhiều."

    },
    {   
        name:'Thầy Trần Minh B',
        avatar:'/images/home/customer-service/image-avt1.png',
        role:'Giáo viên Trung tâm Anh ngữ 2',
        contentFeedback:"Là một giáo viên Tiếng Anh thời 4.0, tôi luôn muốn cập nhật các công cụ mới hiệu quả nhất để nâng cao chất lượng giảng dạy, giúp học sinh học tốt, tương tác vui mà giáo viên không mất nhiều công sức hay thời gian. Và ứng dụng tạo và quản lý đề thi i-Test hoàn toàn đáp ứng được yêu cầu này của tôi, hỗ trợ tôi tối đa trong quá trình công tác."

    },
    {   
        name:'Em Phạm Thị C',
        avatar:'/images/home/customer-service/image-avt.png',
        role:'Học sinh lớp 6A Trường THCS C',
        contentFeedback:"Con rất thích sử dụng chương trình này để làm bài tập và kiểm tra cô giao. Vì con có thể sử dụng máy tính, điện thoại làm bài rất nhanh và biết điểm luôn ngay sau khi bấm nộp bài. Các bài luyện tập cũng rất nhiều mẫu thú vị. Nhờ đó mà con làm bài không thấy chán, không thấy sợ nữa."

    },
    {   
        name:'Em Phan Thanh D',
        avatar:'/images/home/customer-service/image-avt1.png',
        role:'Học sinh lớp 11D Trường THPT D',
        contentFeedback:"Em biết đến i-Test là nhờ cô chủ nhiệm giới thiệu từ đầu năm học. i-Test có rất nhiều tính năng phù hợp để em và các bạn trong lớp có thể làm bài thi thử Tiếng Anh. Sau thời gian sử dụng, em thấy hệ thống câu hỏi rất phong phú, độc đáo, có kho dữ liệu đề thi đa dạng và chất lượng. Nhờ i-Test mà em thấy tự tin hơn khi tham gia các bài kiểm tra hay kỳ thi quan trọng tại trường."

    },
    {   
        name:'Chị Nguyễn Ngọc E',
        avatar:'/images/home/customer-service/image-avt.png',
        role:'Phụ huynh học sinh',
        contentFeedback:"Tôi thấy i-Test là một chương trình rất thú vị, phù hợp cho nhiều đối tượng. Cả giáo viên, học sinh và phụ huynh như tôi đều có thể theo dõi mỗi ngày và nắm rõ lộ trình học tập trên lớp lẫn kết quả học tập của con mình. Nhờ vậy mà tôi có thể theo sát cháu hơn, dễ dàng trao đổi với giáo viên lẫn nhà trường mà không phải mất quá nhiều thời gian hay công sức."

    },
    {   
        name:'Anh Đỗ Tuấn F',
        avatar:'/images/home/customer-service/image-avt1.png',
        role:'Phụ huynh học sinh',
        contentFeedback:"Vì quá bận công việc nên trước đây tôi rất khó nắm được thông tin về lớp học, các kỳ kiểm tra hay kết quả học tập của con mình. Nhưng từ khi nhà trường và giáo viên ứng dụng sản phẩm i-Test và phổ biến đến phụ huynh thì tôi cũng học cách sử dụng. Khi nào con thi hay con làm bài xong có kết quả là tôi biết ngay. Thấy con học tốt hơn, vui hơn, nên tôi cũng thấy yên tâm hơn."

    },
    
]