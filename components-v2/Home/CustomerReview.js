import React, {useState} from "react";
import classNames from "classnames";

import {Desktop, Mobile} from "@/componentsV2/Common/Media";
import CustomCarousel from "@/componentsV2/Common/CustomCarousel";
import useDevices from "@/hooks/useDevices";
import useTranslation from '@/hooks/useTranslation'

import Icon from "../../assets/icons/double.svg";

import styles from "./CustomerReview.module.scss";
import {dataFeedback} from './data/dataFeedback';

const CustomerReview = () => {
    const {isDesktop} = useDevices();
    const {t}= useTranslation()

    const [activeReview, setActiveReview] = useState('/images/home/customer-service/image-avt.png');
    const [indexSelected, setIndexSelected] = useState(0)
    const renderBottomCenterControl = (slideControl) => {
        return (
            <div className={styles.dotWrapper}>
                {[...Array(slideControl.slideCount).keys()].map((item, index) => (
                    <div key={index}
                         className={classNames(styles.dotItem, {
                             [styles.active]: index === slideControl.currentSlide
                         })}
                         onClick={() => slideControl.goToSlide(index)}/>
                ))}
            </div>
        )
    }

    const renderText = (index) => dataFeedback[index].contentFeedback
    return (
        <div className={styles.background}>
            <div className={classNames(styles.customerReviewWrapper, {
                "container": isDesktop
            })}>
                <h2>{t('customer-say-about-us')}</h2>
                <Desktop>
                    <div className={styles.content}>
                        {dataFeedback.map((item,index)=>(
                                <>
                                    <div className={classNames(styles.reviewContent,{
                                        [styles.activeContent]: index == indexSelected
                                    })}>
                                        <Icon/>
                                        <p>
                                            {renderText(index)}
                                        </p>
                                        <div className={styles.currentAvatar}>
                                            <div></div>
                                            <p className={styles.name}>{item.name}</p>
                                            <p>{item.role}</p>
                                        </div>
                                    </div>
                                </>
                            ))}
                        
                        <Desktop>
                            <div className={styles.boxItemReview}>
                                {dataFeedback.map((item,index)=>{
                                    return (
                                        <>
                                            <img className={classNames(styles[`review${index+1}`], {
                                                [styles.active]: index== indexSelected
                                            })}
                                                src={item.avatar}
                                                alt={""}
                                                onClick={()=>setIndexSelected(index)}/>
                                        </>
                                    )
                                })}
                            </div>
                        </Desktop>
                    </div>
                </Desktop>
                <Mobile>
                    <CustomCarousel
                        renderCenterLeftControls={null}
                        renderCenterRightControls={null}
                        renderBottomCenterControls={renderBottomCenterControl}>
                        {dataFeedback.map((item)=>{
                            return (
                                <>
                                    <div className={styles.content}>
                                        <div className={styles.reviewContent}>
                                            <Icon/>
                                            <p>{item.contentFeedback}</p>
                                            <div className={styles.currentAvatar}>
                                                <img src={item.avatar} alt={""}/>
                                                <p className={styles.name}>{item.name}</p>
                                                <p>{item.role}</p>
                                            </div>
                                        </div>

                                    </div>
                                </>
                            )
                        })}
                    </CustomCarousel>
                </Mobile>
            </div>
        </div>
    )
}

export default CustomerReview;