import React, { useEffect, useMemo, useRef } from 'react';

import CustomCarousel from '@/componentsV2/Common/CustomCarousel';
import { useWindowSize } from '@/utils/hook';

import useTranslation from "../../../hooks/useTranslation";
import styles from './index.module.scss';

const Product = () => {
    const [windowWith] = useWindowSize();
    const { t } = useTranslation()
    const carouselRef = useRef();

    const slidesToShow = useMemo(() => {
        if (windowWith >= 1202) {
            return 4;
        }

        if (windowWith >= 904) {
            return 3;
        }

        if (windowWith >= 606) {
            return 2;
        }

        return 1;
    }, [windowWith]);

    useEffect(() => {
        carouselRef.current?.goToSlide(0);
    }, [slidesToShow]);

    const disableDragging = slidesToShow === 4;
    const hideDotProp = disableDragging ? { renderBottomCenterControls: null } : {};

    return (
        <section className={styles.product} id="product">
            <div className={styles.list}>
                <div className={styles.title}>
                    <img src="/images-v2/home/product-title.png" alt="" />
                    <span className={styles.textTitle}>{t('product-us-header')}</span>
                </div>
                <div className={styles.listContent}>
                    <CustomCarousel
                        renderCenterLeftControls={null}
                        renderCenterRightControls={null}
                        slidesToShow={slidesToShow}
                        cellSpacing={20}
                        dragging={!disableDragging}
                        carouselRef={carouselRef}
                        {...hideDotProp}
                    >
                        <div className={styles.item}>
                            <img src="/images-v2/home/prod-skill.png" alt="" />
                            <h3 className={styles.name} dangerouslySetInnerHTML={{ __html: t('product-skills') }}></h3>
                        </div>
                        <div className={styles.item}>
                            <img src="/images-v2/home/prod-book.png" alt="" />
                            <h3 className={styles.name} dangerouslySetInnerHTML={{ __html: t('product-books') }}></h3>
                        </div>
                        <div className={styles.item}>
                            <img src="/images-v2/home/prod-improve.png" alt="" />
                            <h3 className={styles.name} dangerouslySetInnerHTML={{ __html: t('product-improve') }}></h3>
                        </div>
                        <div className={styles.item}>
                            <img src="/images-v2/home/prod-cert.png" alt="" />
                            <h3 className={styles.name} dangerouslySetInnerHTML={{ __html: t('product-cerf') }}></h3>
                        </div>
                    </CustomCarousel>
                </div>
            </div>
            <div className={styles.label}>
                <div className={styles.box}>
                    <h3>{t('product-describe-text-1')}</h3>
                    <h3>{t('product-describe-text-2')}​</h3>
                </div>
            </div>
        </section>
    );
};

export default Product;