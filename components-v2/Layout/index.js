import React from "react";

import Footer from "@/componentsV2/Layout/Footer";
import Header from "@/componentsV2/Layout/Header";

import styles from "./Layout.module.scss";

const MasterLayout = ({children, contentStyle, footerStyle = {}}) => {
    return (
        <main className={styles.masterLayout}>
            <Header/>
            <div className={styles.content} style={contentStyle}>
                {children}
            </div>
            <Footer style={footerStyle} />
        </main>
    )
}

export default MasterLayout;