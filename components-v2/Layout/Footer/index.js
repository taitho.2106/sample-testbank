import classNames from "classnames";
import Link from "next/link";

import Container from "@/componentsV2/Common/Container";
import useTranslation from '../../../hooks/useTranslation';
import { paths } from "../../../interfaces/constants";

import TaxIcon from "assets/icons/tax.svg";
import FacebookIcon from "../../../assets/icons/fb.svg";
import LocationIcon from "../../../assets/icons/location.svg";
import MailIcon from "../../../assets/icons/mail.svg";
import PhoneIcon from "../../../assets/icons/phone.svg";
import YoutubeIcon from "../../../assets/icons/yt.svg";

import { useRouter } from "next/router";
import { EMAIL_COMPANY } from "../../../constants";
import styles from "./Footer.module.scss";

const Footer = ({ style }) => {
    const {t} = useTranslation()
    const {pathname} = useRouter();

    return (
        <footer className={styles.footerWrapper} style={style}>
            <Container>
                <div className={styles.footerTop}>
                    <img src={"/images/layout/footer-logo.png"} alt=""/>
                    <div className={styles.content}>
                        <div className={styles.leftSide}>
                            <h3>{t('company-name-new')}</h3>
                            <p className="break-line"><LocationIcon/>{t('company-address-new')}</p>
                            <p><PhoneIcon/>1800 6242</p>
                            <p><MailIcon/>{EMAIL_COMPANY}</p>
                            <p><TaxIcon/>MST: {t('MST')}</p>
                        </div>
                        <div className={styles.center}>
                            <h3>{t("support-information")}</h3>
                            <Link href={paths.aboutUs}>
                                <p className={classNames("cursor-pointer", {
                                    [styles.highlight]: pathname === paths.aboutUs
                                })}>{t("about-us-footer")}</p>
                            </Link>
                            <Link href={paths.shoppingGuide}>
                                <p className={classNames("cursor-pointer", {
                                    [styles.highlight]: pathname === paths.shoppingGuide
                                })}>{t("shopping-guide")["title"]}</p>
                            </Link>
                            <Link href={paths.paymentPolicy}>
                                <p className={classNames("cursor-pointer", {
                                    [styles.highlight]: pathname === paths.paymentPolicy
                                })}>{t("payment-policy")["title"]}</p>
                            </Link>
                            <Link href={paths.feedbackPolicy}>
                                <p className={classNames("cursor-pointer", {
                                    [styles.highlight]: pathname === paths.feedbackPolicy
                                })}>{t("feedback-policy")["title"]}</p>
                            </Link>
                            <Link href={paths.policySecurity}>
                                <p className={classNames("cursor-pointer", {
                                    [styles.highlight]: pathname === paths.policySecurity
                                })}>{t("security-policy")["title"]}</p>
                            </Link>
                        </div>
                        <div className={styles.rightSide}>
                            <p>{t('see-more')}</p>
                            <div className={styles.iconWrapper}>
                                <span className={classNames(styles.icon, styles.facebook)}>
                                    <FacebookIcon/>
                                </span>
                                    <span className={classNames(styles.icon, styles.youtube)}>
                                    <YoutubeIcon/>
                                </span>
                            </div>
                            <Link href="http://online.gov.vn/Home/WebDetails/103701">
                                <a target="_blank">
                                    <img src="/images/layout/bo-cong-thuong.png" alt="" className="cursor-pointer"/>
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </Container>
            <div className={styles.separate}/>
            <Container>
                <div className={styles.footerBottom}>
                    Copyright (c) EDUCATION SOFTWARE VIET NAM.,LTD. All Rights Reserved
                </div>
            </Container>
        </footer>
    )
}

export default Footer;