import React, { useEffect, useRef, useState } from 'react'

import classNames from 'classnames'
import { useSession } from 'next-auth/client'
import Link from 'next/link'
import { useRouter } from 'next/router'

import Chevron from '@/assets/icons/chevron-down.svg'
import LogoMobile from '@/assets/icons/logo-mobile.svg'
import Logo from '@/assets/icons/logo.svg'
import MenuIcon from '@/assets/icons/menu.svg'
import Profile from '@/assets/icons/profile.svg'
import LogoIcon from '@/assets/icons/logo-icon.svg'
import LogoTextIcon from '@/assets/icons/logo-text.svg'
import Container from '@/componentsV2/Common/Container'
import Button from '@/componentsV2/Common/Controls/Button'
import { Desktop, Mobile } from '@/componentsV2/Common/Media'
import { headerItems } from '@/componentsV2/Layout/Header/data'
import DropdownLoginMobile from '@/componentsV2/Layout/Header/DropdownLoginMobile'
import MenuMobile from '@/componentsV2/Layout/Header/MenuMobile'
import useDevices from '@/hooks/useDevices'
import { USER_ROLES, paths, ssrMode } from '@/interfaces/constants'
import { generateUniqueId, userInRight } from '@/utils/index'
import { useUserInfo } from 'lib/swr-hook'

import useTranslation from '../../../hooks/useTranslation'
import Switch from '../../Common/Controls/Switch'
import ButtonHeader from "../../Dashboard/Header-v2/components/ButtonHeaderDropdown/ButtonHeader";
import LanguageSelector from '../../Dashboard/Header-v2/components/LanguageSelector/LanguageSelector'
import UserManual from '../../Dashboard/Header-v2/components/UserManual/UserManual'
import DropdownUserInfo from './DropdownUserInfo'
// import MobileSideBarMenu from "@/componentsV2/Layout/Header/MobileSideBarMenu";
import styles from './Header.module.scss'

const Header = () => {
    const { isDesktop } = useDevices()
    const { t, switchLocale } = useTranslation()
    const { user, mutateUserInfo, userDisplayName } = useUserInfo()
    const headerRef = useRef()
    const [session] = useSession()
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const { push, pathname, query } = useRouter()

    const [isShowMenu, setIsShowMenu] = useState(false)
    const [isShowProfile, setIsShowProfile] = useState(false)
    const [isShowUserInfo, setIsShowUserInfo] = useState(false)
    const [isShow, setIsShow] = useState(false);
    const [isShowUserManual, setIsShowUserManual] = useState(false)
    const [isScroll, setScroll] = useState(false);
    const [activeHeaderItem, setActiveHeaderItem] = useState('referral');
    const [languageClassName, setLanguageClassName] = useState('highlight')
    const [isShowLanguage, setIsShowLanguage] = useState(false)

    const hideMenuInfoPaths = [ paths.payment, paths.paymentResults ];
    const isHideMenuInfo = hideMenuInfoPaths.includes(pathname);

    const navHighlighter = () => {
      const sections = document.querySelectorAll('section[id]')
      // Get current scroll position
      let scrollY = window.pageYOffset

      // Now we loop through sections to get height, top and ID values for each
      sections.forEach((current) => {
        const sectionHeight = current.offsetHeight
        const sectionTop = current.offsetTop - (isDesktop ? 110 : 87)
        let sectionId = current.getAttribute('id')

        /*
            - If our current scroll position enters the space where current section on screen is, add .active class to corresponding navigation link, else remove it
            - To know which link needs an active class, we use sectionId variable we are getting while looping through sections as an selector
            */
        if (scrollY > sectionTop && scrollY <= sectionTop + sectionHeight) {
          setActiveHeaderItem(sectionId)
        }
      })
    }

    const isShowHeaderItem = ![paths.policySecurity, paths.paymentPolicy, paths.feedbackPolicy, paths.aboutUs, paths.shoppingGuide].includes(pathname)

    const scrollToElement = (elementId) => {
        if(!isShowHeaderItem){
            push({pathname: paths.home, query: {el: elementId}})
        }else{
            const element = document.getElementById(elementId)
            //add offset to scroll
            if (element) {
                element.style.scrollMargin = isDesktop ? '100px' : '77px'
                element.scrollIntoView({ behavior: 'smooth' })
            }
        }
    }

    const onSwitchLanguage = (e) => {
        if (e.target.checked) {
            switchLocale('en')
        } else {
            switchLocale('vi')
        }
    }

    const trackScroll = () => {
        if (headerRef.current) {
            if(window.scrollY > 10 ){
                headerRef.current.dataset.scroll = 'scroll-out'
                setScroll(true);
                setLanguageClassName('')
            } else {
                headerRef.current.dataset.scroll = 'scroll-in'
                setScroll(false);
                setLanguageClassName('highlight')
            }
        }
        isShowHeaderItem ? navHighlighter() : () => {
            //
        };
    }

    useEffect(() => {
        document.addEventListener('scroll', trackScroll)
        if(query.el){
            setTimeout(()=> {
                scrollToElement(query?.el)
            }, 500)
        }
        return () => {
            document.removeEventListener('scroll', trackScroll)
        }
    }, [query])

    const userAvatar = user?.avatar ?? session?.user?.avatar

    const iTestLogo = (
        <div className={styles.logoContainer}>
            <div className={styles.logoBox}>
                <Link href={paths.home} passHref>
                    <div className={classNames(styles.logoContent, 'cursor-pointer')}>
                        <LogoIcon className={styles.logoIcon} />
                        <LogoTextIcon className={styles.logoText} />
                    </div>
                </Link>
            </div>
        </div>
    );
    
    useEffect(() => {
        const liveChatElement = document.getElementById('cs-live-chat')
        if (liveChatElement) {
            // liveChatElement.style.bottom = isShowProfile && !session && !isDesktop ? "180px" : "0px";
            // liveChatElement.style.right = isShowMenu && !isDesktop ? "260px" : "0px";
            // liveChatElement.style.transitionProperty = "bottom, transform, right"
            // liveChatElement.style.transitionDuration = "250ms"
            
            if (isShowProfile && !session && !isDesktop){
                liveChatElement.classList.add('show-login-mobile')
            }else {
                liveChatElement.classList.remove('show-login-mobile')
            }
            
            if (isShowMenu && !isDesktop) {
                liveChatElement.classList.add('show-menu-mobile')
            } else {
                liveChatElement.classList.remove('show-menu-mobile')
            }
        }
    } , [session, isShowProfile, isShowMenu, isDesktop])

    return (
        <header className={classNames(styles.header)} ref={headerRef} id='header-landingpage'>
            <div className={classNames(styles.fixedHeader)}>
                <Container>
                    <Desktop>
                        <div className={styles.leftSide}>
                            {iTestLogo}
                            {!isHideMenuInfo && headerItems.map(item => {
                                    return (
                                        <a key={generateUniqueId('header')} className={classNames(styles.navItem, {
                                            [styles.active]: activeHeaderItem === item.referenceId && isShowHeaderItem
                                        })}
                                           onClick={() => scrollToElement(item.referenceId)}>
                                            {t(item.name)}
                                        </a>
                                    )
                                })
                            }
                        </div>
                        <div className={styles.rightSide}>
                            {!isHideMenuInfo && (
                                <>
                                    {session?.user
                                        ?
                                        <>
                                            {!isSystem && <ButtonHeader session={session} 
                                                        id='header-landingpage'
                                                        isShow={isShow} 
                                                        setIsShow={setIsShow} 
                                                        setIsShowUser={setIsShowUserInfo} 
                                                        setIsShowUserManual={setIsShowUserManual}
                                                        setIsShowLanguage={setIsShowLanguage}
                                                        />}
                                            {isSystem &&
                                                <Button className={styles.btnExplore}
                                                                 onClick={() => push(paths.unitTest)}
                                                                 type='yellow'>
                                                {t('explore-now')}
                                                </Button>}
                                            <div style={{ position: 'relative' }}>
                                                <div className={classNames(styles.userGroup, 'cursor-pointer')} onClick={() => setIsShowUserInfo(!isShowUserInfo)}>
                                                    {userAvatar ? <img src={`/upload/${userAvatar}`} alt='' /> : <div className={styles.avatarDefault}><LogoIcon height={40} width={40}/></div>}
                                                    <div>
                                                        <p className={styles.userName}>
                                                            {/* {session?.user?.user_name}  */}
                                                            <Chevron />
                                                        </p>
                                                        {/* <p className={styles.userRole}> {session?.user?.user_role_name}</p> */}
                                                    </div>
                                                </div>
                                                {isShowUserInfo && (
                                                    <DropdownUserInfo
                                                        session={session}
                                                        onClose={() => setIsShowUserInfo(false)}
                                                        userInfo={user}
                                                        mutateUserInfo={mutateUserInfo}
                                                        userDisplayName={userDisplayName}
                                                    />
                                                )}
                                            </div>
                                        </>
                                        :
                                        <>
                                            <Button className={styles.btnLogin}
                                                    onClick={() => push('/login')}
                                                    type='outline'>
                                                {t('sign-in')}
                                            </Button>
                                            <Button className={styles.btnTrial}
                                                    onClick={() => scrollToElement('trial')}
                                                    type='yellow'>
                                                {t('trial-now')}
                                            </Button>
                                        </>
                                    }
                                    <UserManual
                                        id='header-landingpage'
                                        isShowUserManual={isShowUserManual}
                                        setIsShowUserManual={setIsShowUserManual}
                                        onClick={() => {
                                            setIsShow(false)
                                            setIsShowUserInfo(false)
                                        }}
                                    />
                                    <LanguageSelector
                                        className={languageClassName}
                                        isShowLanguage={isShowLanguage}
                                        setIsShowLanguage={setIsShowLanguage}
                                        onClick={() => {
                                            setIsShow(false)
                                            setIsShowUserInfo(false)
                                            setIsShowUserManual(false)
                                        }}
                                        shouldCLose={isShow || isShowUserInfo || isShowUserManual}
                                    />
                                </>
                            )}

                            <Link href="https://dtp-education.com/">
                                <a target='_blank'>
                                    <div className={styles.rightLogo}>
                                        <img src={!isScroll ? "/images/layout/logo-dtp-header.png": "/images/layout/logo-header.png"} alt=''/>
                                    </div>
                                </a>
                            </Link>
                        </div>
                    </Desktop>
                    <Mobile>
                        <div className={styles.leftSide}>
                            {iTestLogo}
                        </div>
                        <div className={styles.rightSide}>
                            <UserManual
                                id='header-landingpage'
                                isShowUserManual={isShowUserManual}
                                setIsShowUserManual={setIsShowUserManual}                                
                                onClick={() => {
                                    setIsShowProfile(false)
                                    setIsShowMenu(false)
                                }}
                            />
                            <div className={styles.profileGr}>
                                <Profile className={classNames(styles.profileIcon, 'cursor-pointer', {
                                    [styles.profileActive]: isShowProfile,
                                })}
                                    onClick={() => {
                                        setIsShowProfile(!isShowProfile)
                                        setIsShowUserManual(false)
                                    }}
                                />
                                {
                                    isShowProfile &&
                                    <>
                                        {!session
                                            ? <DropdownLoginMobile onClose={setIsShowProfile} scrollToElement={scrollToElement}/>
                                            : <DropdownUserInfo session={session}
                                                                onClose={() => setIsShowProfile(false)} 
                                                                userInfo={user}
                                                                mutateUserInfo={mutateUserInfo}
                                                                isHideMenuInfo={isHideMenuInfo}
                                                                userDisplayName={userDisplayName}
                                                                />
                                        }
                                    </>
                                }
                            </div>
                            {!isHideMenuInfo && (
                                <div className={styles.menuGr}>
                                    <MenuIcon
                                        height={26}
                                        width={26}
                                        className={classNames(styles.hamburgerIcon, 'cursor-pointer', {
                                            [styles.hamburgerActive]: isShowMenu,
                                        })}
                                        onClick={() => setIsShowMenu(!isShowMenu)} />
                                    {isShowMenu && <MenuMobile scrollToElement={scrollToElement}
                                                            setIsShowMenu={setIsShowMenu}
                                                            activeHeaderItem={activeHeaderItem}
                                                            isShowHeaderItem={isShowHeaderItem}
                                                            onSwitchLanguage={onSwitchLanguage} />}
                                </div>
                            )}
                        </div>
                        {/* <MobileSideBarMenu isShow={isShowSidebar} onClose={() => setIsShowSidebar(false)}/> */}
                    </Mobile>
                </Container>
            </div>
        </header>
    )
}

export default Header