export const headerItems = [
    {
        name: 'about-us-header',
        referenceId: 'referral',
    },
    {
        name: 'product-us-header',
        referenceId: 'product',
    },
    {
        name: 'feature-header',
        referenceId: 'feature',
    },
    {
        name: 'pricing-header',
        referenceId: 'pricing',
    },
    {
        name: 'contact-header',
        referenceId: 'contact',
    },
]