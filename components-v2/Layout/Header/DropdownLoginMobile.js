import React from 'react'
import { useRouter } from 'next/router'

import BaseDropdownMenu from '@/componentsV2/Common/Controls/BaseDropdownMenu'
import Button from '@/componentsV2/Common/Controls/Button'
import useTranslation from '@/hooks/useTranslation'
import useDevices from "../../../hooks/useDevices";

import CloseIcon from "assets/icons/close.svg";

import styles from './DropdownLoginMobile.module.scss'

const DropdownLoginMobile = React.forwardRef(({ onClose, scrollToElement }, ref) => {
    const { t } = useTranslation()
    const { push } = useRouter()
    const {isDesktop} = useDevices();

    return (
        <div className={styles.dropdownProfileMobileWrapper} ref={ref}>
            <BaseDropdownMenu className={styles.profileDropdown}
                              onClickOutside={onClose}>
                <div className={styles.btnClose} ><CloseIcon onClick={() => onClose()}/></div>
                <div className={styles.item}>
                    <Button type={'outline'} onClick={() => {
                        push("/login");
                        onClose(false);
                    }}>{t('sign-in')}</Button>
                </div>
                <div className={styles.item}>
                    <Button onClick={() => {
                        scrollToElement('trial');
                        onClose();
                    }}>{t('trial-now')}</Button>
                </div>
            </BaseDropdownMenu>
        </div>
    )
})

DropdownLoginMobile.displayName = "DropdownLoginMobile"

export default DropdownLoginMobile