import React, { useState } from 'react'

import classNames from 'classnames'

import BaseDropdownMenu from '@/componentsV2/Common/Controls/BaseDropdownMenu'
import LanguageSelector from '@/componentsV2/Dashboard/Header-v2/components/LanguageSelector/LanguageSelector'
import { headerItems } from '@/componentsV2/Layout/Header/data'
import { generateUniqueId } from '@/utils/index'

import MailIcon from '../../../assets/icons/mail.svg'
import CallIcon from '../../../assets/icons/phone.svg'
import { EMAIL_COMPANY } from "../../../constants";
import useTranslation from '../../../hooks/useTranslation'
import Switch from '../../Common/Controls/Switch'
import styles from './MenuMobile.module.scss'

const MenuMobile = ({ scrollToElement, setIsShowMenu, onSwitchLanguage, activeHeaderItem, isShowHeaderItem }) => {
    const { t, locale } = useTranslation()
    const onClickItem = (value) => {
        setIsShowMenu(false)
        scrollToElement(value)
    }
    const [isShowLanguage, setIsShowLanguage] = useState(false)

    return (
        <div className={styles.menuMobileWrapper}>
            <BaseDropdownMenu className={styles.menuDropdown} onClickOutside={setIsShowMenu}>
                <div>
                    <div className={styles.switchLanguage}>
                       <p>{t("language")}</p>
                        <LanguageSelector
                            isShowLanguage={isShowLanguage}
                            setIsShowLanguage={setIsShowLanguage}
                            onClick={() => {
                                //
                            }}
                        />
                    {/*    <Switch onChange={onSwitchLanguage} checked={locale === 'en'} />*/}
                    </div>
                    {headerItems.map((item, index) => (
                        <div key={generateUniqueId('header')}
                             className={classNames(styles.item, {
                                [styles.active]: activeHeaderItem === item.referenceId && isShowHeaderItem
                             })}
                             onClick={() => onClickItem(item.referenceId)}>
                            {t(item.name)}
                        </div>
                    ))}
                </div>
                <div className={styles.contactInfo}>
                    <p><MailIcon /> {EMAIL_COMPANY}</p>
                    <p><CallIcon /> 1800 6242</p>
                </div>
            </BaseDropdownMenu>
        </div>
    )
}

export default MenuMobile