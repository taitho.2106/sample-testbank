import { useState } from 'react'

import classNames from 'classnames'
import { signOut } from 'next-auth/client'
import Link from 'next/link'

import { paths as p } from 'api/paths'
import { callApi } from 'api/utils'

import { userInRight } from '@/utils/index'
import { useRouter } from 'next/router'
import CallIcon from '../../../assets/icons/call.svg'
import Camera from '../../../assets/icons/camera.svg'
import Overview from '../../../assets/icons/logo-icon.svg'
import LogoutIcon from '../../../assets/icons/logout.svg'
import MailIcon from '../../../assets/icons/mail.svg'
import useInfo from "../../../hooks/useInfo"
import useTranslation from '../../../hooks/useTranslation'
import { USER_ROLES, paths } from '../../../interfaces/constants'
import BaseDropdownMenu from '../../Common/Controls/BaseDropdownMenu'
import styles from './DropdownUserInfo.module.scss'

const DropdownUserInfo = ({ session, onClose, userInfo, mutateUserInfo, isHideMenuInfo, userDisplayName }) => {
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const { t } = useTranslation()
    const [error, setError] = useState(false)
    const user = session?.user;
    const { getRoleUser } = useInfo(user)
    const userAvatar = userInfo?.avatar ?? (user?.avatar || '');
    const { pathname, push } = useRouter();

    const handleFileChange = async (e) => {
        e.preventDefault()
        const file = e.target.files[0]
        if(!file) {
            setError(false)
            return
        }

        if (!file.type.startsWith("image/")) {
            setError(true)
            return;
        }
        const fileSizeLimit = 2 * 1024 * 1024;
        const allowedTypes = ["image/jpeg", "image/jpg", "image/png"];
        if (file.size > fileSizeLimit) {
            setError(true)
            return
        }
        if (!allowedTypes.includes(file.type)) {
            setError(true)
            return
        }
        const formData = new FormData();
        formData.append("file_avatar", file);
        const res = await callApi(p.api_users_avatar, 'post', 'token', formData)
        if(res?.isSuccess){
            setError(false)
            mutateUserInfo({ ...user, ...userInfo, avatar: res.avatar }, { revalidate: false })
        }

    }
    const role = getRoleUser()
    const handlerSingout = () => {
        sessionStorage.removeItem('visitCount')
        window.localStorage.removeItem('nextTimeRun')
        signOut({ callbackUrl: paths.signIn, redirect: false }).then(() => {
            if (pathname !== paths.home) {
                onClose()
                push(paths.signIn)
            }
        })
    }
    return (
        <div className={styles.dropdownUserInfoWrapper}>
            <BaseDropdownMenu onClickOutside={onClose} className={classNames(styles.dropdownUser, isHideMenuInfo && styles.straitRight)}>
                <div className={styles.userGroup}>
                    <div className={classNames(styles.avatar,{
                            [styles.error]:error
                        })}>
                        <div className={classNames(styles['wrapper-img'],'cursor-pointer')}>
                            <label htmlFor='file-input' className='cursor-pointer'>
                            {userAvatar ? 
                            <img 
                                src={`/upload/${userAvatar}`}
                                alt=''
                            /> :
                            <Overview height={80} width={80}/>
                            }
                            <Camera className={styles.camera} />
                            </label>
                        </div>
                        <input type='file' id='file-input' onChange={handleFileChange}/>
                        {error && (
                                <div className={styles.tooltipError}><span dangerouslySetInnerHTML={{__html: t('avatar-error')}}></span></div>
                            )}
                    </div>
                    <div className={styles.userInfo}>
                        <p className={styles.username}>{userDisplayName}</p>
                        <p className={styles.userRole}>{role}</p>
                        {user.email ? <p><MailIcon/> {user.email}</p> : null}
                        {user.phone ? <p><CallIcon/> {user.phone}</p> : null}
                    </div>
                </div>
                <div className={styles.menuList}>
                    <Link href={paths.userInfo}>
                        <div>{t('personal-info')}</div>
                    </Link>
                    {!isSystem && <>
                        {!isStudent ?
                            <Link href={paths.payment}>
                                <div>{t('payment-service')}</div>
                            </Link>
                            : null
                        }
                        <Link href={paths.transactionHistory}>
                            <div>{t('transaction-history')}</div>
                        </Link>
                    </>}
                    {/* <div>{t('change-password')}</div> */}
                </div>
                <div className={styles.logout}>
                    <LogoutIcon onClick={handlerSingout} />
                </div>
            </BaseDropdownMenu>
        </div>
    )
}

export default DropdownUserInfo