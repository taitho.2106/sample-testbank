import React from 'react'

import VietNamFlag from '../../../assets/icons/vn-flag.svg'
import EnglishFlag from '../../../assets/icons/en-flag.svg'

import styles from './DropdownLanguage.module.scss'
import BaseDropdownMenu from '@/componentsV2/Common/Controls/BaseDropdownMenu'
import useTranslation from '../../../hooks/useTranslation'

const DropdownLanguage = ({ onClose }) => {
    const { switchLocale, t } = useTranslation()
    return (
        <div className={styles.dropdownLanguageWrapper}>
            <BaseDropdownMenu
                textHeader={t('change-language')}
                className={styles.dropdownMenu}
                onClickOutside={onClose}>
                <div className={styles.item}
                     onClick={() => switchLocale('vi')}>
                    <VietNamFlag height={16} width={16} />
                    {t('vietnamese')}
                </div>
                <div className={styles.item}
                     onClick={() => switchLocale('en')}>
                    <EnglishFlag />
                    {t('english')}
                </div>
            </BaseDropdownMenu>
        </div>
    )
}

export default DropdownLanguage