import React, { Fragment, useContext, useEffect, useMemo, useRef, useState } from 'react'

import classNames from 'classnames';
import { useSession } from 'next-auth/client';
import { useRouter } from 'next/router';
import { Popover, Whisper } from 'rsuite';
import Link from 'next/link';

import PlusIcon from '@/assets/icons/transparent-plus.svg';
import BookIcon from '@/assets/icons/book-icon.svg';
import IconDisable from '@/assets/icons/lock-icon-disable.svg';
import CustomCarousel from '@/componentsV2/Common/CustomCarousel';
import SkeletonLoading from '@/componentsV2/Common/SkeletonLoading';
import Tabs from '@/componentsV2/Common/Tabs';
import { studentPricings } from '@/componentsV2/Home/data/price';
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage';
import useGrade from '@/hooks/useGrade';
import useTranslation from '@/hooks/useTranslation';
import { PACKAGES, paths, USER_ROLES } from '@/interfaces/constants';
import { AppContext, UnitTestWrapper } from '@/interfaces/contexts';
import { userInRight } from '@/utils/index';
import PopoverUpgradeContent from 'components/molecules/unitTestTable/PopoverUpgradeContent';

import ChevronIcon from '../../../assets/icons/arrow.svg'
import { DataBook, bookTag, dataTag } from './data/constantData';
import styles from './WrapperUnitTestV2.module.scss';
import { dataWithPackage } from './data/dataWithPackage';

  type Props = {
    childrens?:any
    className?:string
  }
export default function WrapperUnitTestV2({childrens , className = ''}: Props) {
  const {t} = useTranslation()
  const [session] = useSession()
  const { listPlans } = useContext(AppContext)
  const isSystem = userInRight([USER_ROLES.Operator],session)
  const isStudent = userInRight([-1, USER_ROLES.Student],session)
  const router = useRouter()
  const { query, push} = router
  const { namePackage } = useCurrentUserPackage()
  const dataPackage = studentPricings.teacher.find(item => item.code === PACKAGES.INTERNATIONAL.CODE)?.descriptions || []
  const price = listPlans.find((item:any) => item.code === PACKAGES.INTERNATIONAL.CODE)?.price || 0
  const itemId = listPlans.find((item:any) => item.code === PACKAGES.INTERNATIONAL.CODE)?.id || ''
  const tab = Number(`${query.tab ?? 0}`) 
  const groupCurrent =  Number(`${query.group ?? 1}`) 
  const isActiveTagInternational:boolean = isSystem || namePackage?.code === PACKAGES.INTERNATIONAL.CODE || isStudent
  const handlerClickTag = (value: any) => {
    if(value.tab == tab) return

    if(value.tab === 0){
      push({
        pathname:'/unit-test',
        query: {
          tab: value.tab,
          group: value?.groups[0].toString()
        }
      })
    }else{
      push({
        pathname: '/unit-test',
        query: {
          tab: value.tab,
          group: value.groups.toString(),
        },
      })
    }
    
  }

  return (
    <UnitTestWrapper.Provider
      value={{
        isActiveTagInternational: isActiveTagInternational,
      }}
    >
      <div className={classNames(styles.contentUnitTestWrapper, className)}>
        <div className={styles.unitTestTag}>
            <div className={styles.topActions}>
                <div className={styles.tagPage}>
                    {dataTag.map((item: any) => {
                    const isShowToolTip = !isActiveTagInternational && item.tab !== 0
                    const Compo = !isShowToolTip
                        ? ({ children }: any) => <>{children}</>
                        : ({ children }: any) => (
                            <Whisper
                            placement="rightStart"
                            trigger="hover"
                            enterable
                            speaker={
                                <Popover className={styles.poperverModal} arrow={false}>
                                <PopoverUpgradeContent
                                    dataPackage={dataPackage}
                                    price={price}
                                    itemId={itemId}
                                />
                                </Popover>
                            }
                            >
                            {children}
                            </Whisper>
                        )
                    return (
                        <Compo key={item.tab}>
                        <div
                            className={classNames(styles.itemTag, {
                            [styles.active]: tab == item.tab,
                            })}
                            onClick={() => handlerClickTag(item)}
                        >
                            <div className={styles.iconBook}>
                            <BookIcon />
                            </div>                            
                            <span>{t(item.name)}</span>
                            {isShowToolTip && (
                            <div className={styles.iconDisable}>
                                {dataWithPackage[PACKAGES.INTERNATIONAL.CODE].icon}
                            </div>
                            )}
                        </div>
                        </Compo>
                    )
                    })}
                </div>
                {isSystem && (
                    <Link href={paths.createUnitTest}>
                        <button className={styles.btnCreate}><PlusIcon />{t("common")["create-new-unit-test"]}</button>
                    </Link>
                )}
            </div>
          <div className={styles['box-filter-wrapper']}>
            {tab == 0 && <BoxStick tab={tab ?? 0} group={groupCurrent} />}
          </div>
        </div>
        {<div className={styles.boxContentUnitTest}>{childrens[tab]}</div>}
      </div>
    </UnitTestWrapper.Provider>
  )
}

const BoxStick = ({group}:any) => {
    const { t } = useTranslation();
    const { getGrade } = useGrade()
    const {push, query , isReady} = useRouter()
    const carouselRef = useRef(null)
    const currentGrade = query?.grade || "-1"
    const showBtn = 6
    const gradeList = getGrade.filter(item => /^G[0-9]{1,2}$/.test(`${item.code}`)) || []
    const defaultActiveIndex = useMemo(() => {
      const index = gradeList.findIndex((item) => item.code === currentGrade)
        if(index !== -1 && index > 5){
          return index - 5 
        }
        return 0
    } ,[gradeList.length, isReady])

    const handleGradeChange = (value:string) => {
        if(value === "-1"){
          push({
            pathname:'/unit-test',
            query:{
                tab: 0,
                group: group
            }
          })
          if(carouselRef.current){
            carouselRef.current?.goToSlide(0)
          }
        }else{
          push({
            pathname:'/unit-test',
              query:{
                  grade: value,
                  group: group
              }
          })
        }
    }
    const handleTagClick = (index:number) => {
        if(group === index) return
        push({
          pathname:'/unit-test',
          query: {
            tab: 0,
            group:index
          }
        })
        if(carouselRef.current){
          carouselRef.current?.goToSlide(0)
        }
    }
    return (
            <>
                <div className={styles['box-filter-action']}>
                  <Tabs 
                    data={bookTag}
                    onClickStep={handleTagClick}
                    step={group}
                  />
                    {gradeList.length > 0 ? 
                    <div className={styles['wrapper-carousel']}>
                        <button 
                            onClick={()=>handleGradeChange("-1")}
                            className={classNames(styles['btn-all'],{
                            [styles["active"]]: currentGrade === '-1'
                        })}>{t('common')['all']}</button>
                        <div className={styles['diriver']}></div>
                        <div className={styles['temp']}></div>
                        <div className={styles['box-carosel']}>
                            <CustomCarousel
                                dragging={null}
                                className={styles['carousel']}
                                slideIndex={defaultActiveIndex}
                                carouselRef={carouselRef}
                                adaptiveHeight={true}
                                autoPlay={false}
                                cellSpacing={4}
                                disableEdgeSwiping={true}
                                slidesToShow={showBtn}
                                slidesToScroll={6}
                                withoutControls={false}
                                isInfinity={true}
                                swiping={false}
                                wrapAround={false}
                                renderTopLeftControls={null}
                                renderBottomCenterControls={null}
                                renderCenterRightControls={({ nextDisabled, nextSlide, currentSlide }:any) => {
                                    currentSlide === gradeList.length - showBtn ? nextDisabled = true : nextDisabled = false
                                    return (
                                      <button onClick={nextSlide} disabled={nextDisabled}>
                                        <ChevronIcon />
                                      </button>
                                )}}
                                renderCenterLeftControls={({ previousDisabled, previousSlide, currentSlide }:any) => {
                                    currentSlide === 0 ? previousDisabled = true : previousDisabled = false
                                    return (
                                      <button onClick={previousSlide} disabled={previousDisabled}>
                                        <ChevronIcon />
                                      </button>
                                )}}
                            > 
                            {gradeList.map((item,index:number)=>{
                                return (
                                    <button key={index} 
                                    className={classNames(styles['carousel-item'],{
                                        [styles["active"]]:currentGrade === item.code
                                    })}
                                    onClick={()=> handleGradeChange(`${item.code}`)}
                                    >
                                        {item.display}
                                    </button>
                                )
                            })}  
                            </CustomCarousel>
                        </div>
                        <div className={styles['temp']}></div>
                    </div> 
                    : 
                    <div className={styles['wrapper-carousel']}>
                      <SkeletonLoading 
                          height='41px'
                          width='78px'
                          borderRadius='12px'
                      />
                        <div className={styles['diriver']}></div>
                        <div className={styles['temp']}></div>
                        <div className={styles['box-carosel']} style={{
                          display:'flex',
                          gap:'4px'
                        }}>
                          {Array.from(Array(6),(_,index:number)=>{
                            return (
                              <Fragment key={index}>
                                  <SkeletonLoading 
                                    height='41px'
                                    width='60px'
                                    borderRadius='12px'
                                  />
                              </Fragment>
                            )
                          })}
                        </div>
                        <div className={styles['temp']}></div>
                    </div>
                    }
                </div>
            </>
    )
}
