import { UNIT_TEST_GROUP } from "@/constants/index"

export const dataTag: any = [
    {
      name: "books",
      tab: 0,
      groups:[UNIT_TEST_GROUP.TEXTBOOK, UNIT_TEST_GROUP.TEXTBOOK_SUPPLEMENT]
    },
    {
      name: "international-exam",
      tab: 1,
      groups:[UNIT_TEST_GROUP.INTERNATIONAL_EXAM]
    },
  ]

export type DataBook = {
  groupName?:string,
  code?:number
}
export const bookTag: DataBook[] = [
  {
    groupName: "textbook",
    code: UNIT_TEST_GROUP.TEXTBOOK
  },
  {
    groupName: "supplement-book",
    code: UNIT_TEST_GROUP.TEXTBOOK_SUPPLEMENT
  }
]