import BasicIcon from '@/assets/icons/book.svg';
import InternationalIcon from '@/assets/icons/ic-package-international-color.svg';
import AdvanceIcon from '@/assets/icons/magic-star.svg';
import { PACKAGES } from "@/interfaces/constants";

export const dataWithPackage = {
  [`${PACKAGES.BASIC.CODE}`]: {
    icon: <BasicIcon />,
  },
  [`${PACKAGES.ADVANCE.CODE}`]: {
    icon: <AdvanceIcon />,
  },
  [`${PACKAGES.INTERNATIONAL.CODE}`]: {
    icon: <InternationalIcon />,
  },
}