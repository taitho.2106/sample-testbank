import React from 'react'
import InternationalUnitTest from '../components/InternationalUnitTest';
import SeriesList from '../components/SeriesList';
import styles from './UnitTestPage.module.scss';
import WrapperUnitTestV2 from './WrapperUnitTestV2';

export default function UnitTestPage() {
  const data = [
    <SeriesList key={1} />,
    <InternationalUnitTest key={2} />,
  ]
  
  return (
    <WrapperUnitTestV2 className={styles["unit-test-wrapper-v2"]} childrens={data} />
  )
}