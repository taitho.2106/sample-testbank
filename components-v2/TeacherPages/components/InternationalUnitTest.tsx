import React, { useContext } from 'react'

import classNames from 'classnames';
import { useRouter } from 'next/router';
import { Popover, Whisper } from 'rsuite';

import LockIcon from '@/assets/icons/lock.svg'
import BackgroundBlurBottom from '@/componentsV2/Common/BackgroundBlurBottom';
import Tabs from '@/componentsV2/Common/Tabs';
import { unitTestTypeInternationalOptions } from '@/constants/index';
import { AppContext, UnitTestWrapper } from '@/interfaces/contexts';
import { PACKAGES } from '@/interfaces/constants';

import PopoverUpgradeContent from "../../../components/molecules/unitTestTable/PopoverUpgradeContent";
import { studentPricings } from "@/componentsV2/Home/data/price";
import { dataWithPackage } from '../UnitTestPage/data/dataWithPackage';
import styles from './InternationalUnitTest.module.scss';

const InternationalUnitTest = () => {
  const { listPlans } = useContext(AppContext)
  const { isActiveTagInternational } = useContext(UnitTestWrapper)
  const isDisable = !isActiveTagInternational
  const icon = dataWithPackage[PACKAGES.INTERNATIONAL.CODE].icon
  const {push, query} = useRouter()
  const step = isNaN(Number(query?.step)) ? 1 : Number(query?.step)
  const dataPackageIE = studentPricings.teacher.find(item => item.code === PACKAGES.INTERNATIONAL.CODE)?.descriptions || []
  const data  = listPlans.find((item: any) => item.code === PACKAGES.INTERNATIONAL.CODE) || {}
  const price = data?.price || 0
  const id = data?.id || ''
  const handleStepChange = (step:number) => {
    push(`/unit-test?tab=1&group=3&step=${step}`)
  }
  const handleItemClick = (item: any) => {
    if(isDisable) return
    push({
      pathname:'/unit-test/series-detail',
      query:{
        page:1,
        limit:10,
        display: item?.label,
        idI: item?.value
      }
    })
  }


  const speaker = (
      <Popover className={styles.poperverModal} arrow={false}>
          <PopoverUpgradeContent
              dataPackage={dataPackageIE}
              price={price}
              itemId={id}
          />
      </Popover>
  )
  return (
    <div className={styles['wrapper-unit-test-internal']}>
      <Tabs
        data={unitTestTypeInternationalOptions}
        onClickStep={handleStepChange}
        step={step}
      />
      <div className={styles['wrapper-content']}>
        <div className={styles['box-content']} id="unit-test-box-content">
          {unitTestTypeInternationalOptions[step-1] && (
            <>{unitTestTypeInternationalOptions[step-1].childrens ? 
              (<>
                {unitTestTypeInternationalOptions[step-1].childrens.map((itemChild, cIndex:number) => {
                  return (
                    <div key={cIndex} className={styles['box-child']}>
                      <span className={styles['text-child']}>{itemChild.groupName}</span>
                      <div className={styles['box-items-child']}>
                      {itemChild.options.map((itemOps, oIndex:number)=>{
                        return (
                          <div
                            key={oIndex}
                            id={`container-${oIndex}`}
                            className={classNames(styles['item-child'], {
                              [styles.disabled]: isDisable,
                            })}
                            onClick={() => handleItemClick(itemOps)}
                          >
                            <img src={itemOps.image} alt="" />
                            <div className={styles['text-ops']}>
                              {itemOps.label}
                            </div>
                            {isDisable && (
                              <Whisper
                                placement="autoHorizontal"
                                trigger="hover"
                                preventOverflow
                                enterable
                                // container={() => document.getElementById(`unit-test-box-content`)}
                                speaker={speaker}
                              >
                                <div className={styles.iconLock}>{icon}</div>
                              </Whisper>
                            )}
                          </div>
                        )
                      })}
                      </div>
                    </div>
                  )
                })}
              </>) 
              : 
              (<div className={styles['box-child']}>
                <div className={styles['box-items-child']}>
                {unitTestTypeInternationalOptions[step-1].options.map((itemOps, oIndex:number)=>{
                  return (
                    <div
                      key={oIndex}
                      id={`container-s-${oIndex}`}
                      className={classNames(styles['item-child'], {
                        [styles.disabled]: isDisable,
                      })}
                      onClick={() => handleItemClick(itemOps)}
                    >
                      <img src={itemOps.image} alt="" />
                      <div className={styles['text-ops']}>{itemOps.label}</div>

                      {isDisable && (
                        <Whisper
                          placement="autoHorizontal"
                          preventOverflow
                          trigger="hover"
                          enterable
                          // container={() => document.getElementById(`unit-test-box-content`)}
                          speaker={speaker}
                        >
                          <div className={styles.iconLock}>
                            {icon}
                          </div>
                        </Whisper>
                      )}
                    </div>
                  )
                })}
                </div>
              </div>)
              }</>
          )}
        </div>
        <BackgroundBlurBottom paddingBottom={30}/>
      </div>
    </div>
  )
}

export default InternationalUnitTest