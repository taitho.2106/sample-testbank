import React, { useEffect, useMemo, useRef, useState } from 'react'

import classNames from 'classnames';
import { useRouter } from 'next/router';

import ResetIcon from '@/assets/icons/reset-icon.svg'
import SearchIcon from '@/assets/icons/search.svg'
import { InputWithLabel } from '@/componentsV2/Common/InputWithLabel/InputWithLabel';
import { MultiSelectPicker } from '@/componentsV2/Common/MultiSelectPicker/MultiSelectPicker';
import { SingleSelectPicker } from '@/componentsV2/Common/SingleSelectPicker/SingleSelectPicker';
import { SKILLS_SELECTIONS, UNIT_TYPE } from '@/interfaces/struct';

import styles from './FilterBox.module.scss';

export default function FilterBox() {
	const router = useRouter()
	const { isReady, query, push } = router;
    
	// const [title, setTitle] = useState('')
	const [triggerReset, setTriggerReset] = useState(false)
	const [filterName, setFilterName] = useState(query["name"] ?? '')
	const [filterTime, setFilterTime] = useState(query["time"] ?? '')
  	const [filterTotalQuestion, setFilterTotalQuestion] = useState(query["total_question"] ?? '')
	const [filterSkill, setFilterSkill] = useState(query["skills"]?.toString()?.split('-') ?? [] as any[])
	const [filterUnitType, setFilterUnitType] = useState(query['unit-type'] ?? '')
	// const [currentPage, setCurrentPage] = useState(1)
	const page = query?.page ?? 1
	const series_id = query?.series_id
	const tab = query?.tab
	const limit = 10
	const display = query?.display
	const [filterBagde, setFilterBagde] = useState(
		[
		  filterName ? true : false,
		  filterTime ? true : false,
		  filterTotalQuestion ? true : false,
		  filterSkill.length > 0,
		  filterUnitType.length > 0,
		].filter((item) => item === true).length,
	)
    const handleNameChange = (val: string) => {
		setTimeout(()=>{
			setFilterName(val)
		},500)
	}
	const handleSkillChange = (val: any[]) => setFilterSkill([...val])
	
	const handleTimeChange = (val: string) => setFilterTime(!isNaN(Number(val)) ? val : '')
	
	const handleTotalQuestionChange = (val: string) => {
		setFilterTotalQuestion(!isNaN(Number(val)) ? val : '')
	}
	
	const handleUnitTypeChange = (val:string) => setFilterUnitType(val)
	const resetFilterState = () => {
		setFilterTime('')
		setFilterTotalQuestion('')
		setFilterSkill([])
		setFilterName('')
		setFilterUnitType('')
		query["page"] = "1"
	}
	const checkFilterBadge = () => {
		const length = [
		  filterName ? true : false,
		  filterTime ? true : false,
		  filterTotalQuestion ? true : false,
		  filterSkill.length > 0,
		  filterUnitType.length > 0,
		].filter((item) => item === true).length
		setFilterBagde(length)
	}
	useEffect(
		() => checkFilterBadge(),
		[filterName, filterUnitType, filterTime, filterTotalQuestion, filterSkill],
	)
	const onResetForm = () => {
		setTriggerReset(!triggerReset)
		resetFilterState()
	}
    
    useEffect(() => {
		if (isReady) {
			const query:any = {
				tab,
				page,
				limit,
				series_id,
				display,
			}
			if (filterName) {
				query['name'] = filterName
				query["page"] = "1"
			}
			
			if(filterTime) {
				query['time'] = filterTime
				query["page"] = "1"
			}
			if (filterTotalQuestion){
				query['total_question'] = +filterTotalQuestion
				query["page"] = "1"
			}
			
			if ((filterSkill && filterSkill.length)){
				query['skills'] = filterSkill.join('-')
				query["page"] = "1"
			}
			if (filterUnitType) {
				query['unit-type'] = filterUnitType
				query["page"] = "1"
			}

            const timeout = setTimeout(() => {
			    handleFilterChange(query)
                
            }, 300);
            return () => clearTimeout(timeout)
		}
	}, [isReady, filterName, filterTime, filterTotalQuestion , filterSkill, filterUnitType ])

	const handleFilterChange = (filter:any) => {
		push({
			pathname:'/unit-test/series-detail',
			query: filter
		})
	}
  return (
    <div className={styles.boxFillter}>
        <div className={styles.itemFillter}>
            <div className={styles.boxSearch}>
                <InputWithLabel
                    className="__filter-box --search"
                    label="Tìm kiếm đề thi"
                    placeholder="Tìm kiếm đề thi"
                    triggerReset={triggerReset}
                    type="text"
                    onChange={handleNameChange}
                    icon={<SearchIcon />}
					defaultValue={filterName}
                />
                
            </div>
            <button className={classNames(styles.resetBtn,{
                [styles.active]: filterBagde > 0
            })} onClick={onResetForm}>
                <span>Đặt lại</span>
                <div><ResetIcon /></div>
            </button>
        </div>
        <div className={styles.itemFillter}>
            <InputWithLabel
                className={styles["filter-box"]}
                decimal={0}
                label="Thời lượng"
                min={0}
                triggerReset={triggerReset}
                type="number"
                onChange={handleTimeChange}
				defaultValue={filterTime}
            />
            <InputWithLabel
                className={styles["filter-box"]}
                decimal={0}
                label="Số câu hỏi"
                min={0}
                triggerReset={triggerReset}
                type="number"
                onChange={handleTotalQuestionChange}
				defaultValue={filterTotalQuestion}
            />
            <MultiSelectPicker
                className={styles["filter-box"]}
                data={SKILLS_SELECTIONS}
                label="Kỹ năng"
                triggerReset={triggerReset}
                onChange={handleSkillChange}
				defaultValue={SKILLS_SELECTIONS?.filter((item:any)=> item.code == filterSkill)}
            />
            <SingleSelectPicker 
                className={styles["filter-box"]}
                data={UNIT_TYPE}
                label="Loại đề"
                triggerReset={triggerReset}
                onChange={handleUnitTypeChange}
				defaultValue={UNIT_TYPE.filter((item) => item.code == filterUnitType)[0]}
            />
        </div>
        {/* <div className={styles.boxAction}>
            <span>Đã chọn 3 đề thi</span>
            <input placeholder='Chọn thao tác'/>
        </div> */}
    </div>
    )
}
