import React from 'react'

import classNames from 'classnames';

import styles from './UnitTestSGK.module.scss';

type Props = {
    classname?: string
    children?: any
}
function UnitTestSGK({ classname, children }: Props) {
    return (
        <div className={classNames(styles.unitTestSgk, classname)}>
                {children}
            </div>
    )
}

export default UnitTestSGK