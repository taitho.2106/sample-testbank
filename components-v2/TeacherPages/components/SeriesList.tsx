import React, { Fragment, memo, useEffect, useMemo, useRef, useState } from 'react'

import { useRouter } from 'next/router';

import SkeletonLoading from '@/componentsV2/Common/SkeletonLoading';
import NoFieldData from '@/componentsV2/NoDataSection/NoFieldData';
import useGrade from '@/hooks/useGrade';
import { paths } from 'api/paths';
import { callApi } from 'api/utils';

import styles from './SeriesList.module.scss';
import UnitTestSGK from './UnitTestSGK';
import { useWindowSize } from '@/utils/hook';
import classNames from 'classnames';
import { delayResult } from '@/utils/index';
import BackgroundBlurBottom from '@/componentsV2/Common/BackgroundBlurBottom';

type data = {
    grade?: string
}

const SeriesList = () => {
    const {isReady, query, push, asPath} = useRouter()
    const currentGrade = useRef(query.grade)
    const contentRef = useRef<HTMLDivElement>(null)
    const [getSeries, setGetSeries] = useState([])
    const [isLoading, setIsLoading] = useState(0)
    const { getGrade } = useGrade()
    const currentTab = (query?.tab ?? 0)
    const currentGroup = query?.group ?? 1
    const dataColection = async (body: data) => {
        setIsLoading(prev => prev + 1)
        const param = !body.grade || body.grade === '-1' ?`${paths.api_series}?groups=${currentGroup}` : `${paths.api_series}?groups=${currentGroup}&grade=${body.grade}`
        const response: any = await delayResult(callApi(`${param}`, 'get', 'token'), 400);
        if (response?.data) {
            const result = response?.data.map((item: any) => {
                return {
                    code: item.id,
                    display: item.name,
                    image: item.image,
                    type: item.type,
                    grade_eduhome_id: item.grade_eduhome_id,
                    grade_id: item.grade_id
                }
            })
            setGetSeries(result)
            scrollToTop()
        }
        setIsLoading(prev => Math.max(prev - 1, 0))

    }
    useEffect(()=> {
        currentGrade.current = query.grade
    },[query])

    useEffect(()=>{
        if(isReady){
            dataColection(query)
        }
    },[asPath])


    const scrollToTop = () => {
        contentRef?.current?.scrollTo({
            top: 0,
            behavior: 'smooth',
        })
    }
    const onClickSeries = (series: any) => {
        push({
            pathname:'/unit-test/series-detail',
            query:{
                tab:currentTab,
                page:1,
                limit:10,
                series_id:series?.code,
                display:series?.display
            }
        })
    }
    const [width] = useWindowSize()
    const itemSkeleton = width <= 1820 ? 8 : width <= 1920 ? 10 : 12
      
    return (
        <UnitTestSGK classname={styles.wrapper}>
            {(<div className={styles.content} ref={contentRef}>
                {!isLoading ? (
                    getSeries.length > 0 
                    ? 
                    <div className={styles.boxSeries}>
                        {getSeries.map((item, index: number) => {
                            return (
                                <div key={index} className={classNames(styles.itemSeries,styles.hover)} onClick={() => onClickSeries(item)}>
                                    <img
                                        src={item.image ?? '/images-v2/default-img.jpg'}
                                        alt=''
                                    />
                                    <span className={styles.textItem} dangerouslySetInnerHTML={{
                                        __html:item?.display?.replace('-','&#8209;')
                                    }}>
                                    </span>
                                </div>
                            )
                        })}
                    </div> 
                    : 
                    <NoFieldData
                        field='series_list'
                        hasAction={false}
                    />
                )
                :
                <div className={styles.boxSeries}>
                    {Array.from(Array(itemSkeleton), (_,index:number)=> {
                        return (
                            <Fragment key={index}>
                                <SkeletonLoading
                                    className={styles.itemSeries}
                                >
                                <div className={styles.boxImageSkeleton}></div>
                                <span className={styles.textItem} style={{
                                    width:'50%'
                                }}>
                                </span>
                                <span className={styles.textItem}>
                                </span>
                                </SkeletonLoading>
                            </Fragment>
                        )
                    })}
                </div>}
            </div>)}
            {getSeries.length > 0  && <BackgroundBlurBottom paddingBottom={30}/>}
        </UnitTestSGK>
    )
}
export default SeriesList