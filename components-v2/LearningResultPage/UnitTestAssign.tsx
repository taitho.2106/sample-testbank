import React, { useEffect, useMemo, useRef, useState } from "react";

import SearchIcon from "@rsuite/icons/Search";
import classNames from "classnames";
import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
import { useRouter } from "next/router";
import { Button, Loader } from "rsuite";

import StudentIcon from "@/assets/icons/avt-default.svg";
import IconReset from "@/assets/icons/reset-icon.svg";
import { CODE_ASSIGN_OPTION, paths as pathsURL, STATUS_CODE_EXAM } from "@/interfaces/constants";
import { paths as pathsUrl } from "@/interfaces/constants";

import { paths } from "../../api/paths";
import { callApi } from "../../api/utils";
import { Wrapper } from "../../components/templates/wrapper";
import NoFieldData from "../NoDataSection/NoFieldData";
import styles from "./UnitTestAssign.module.scss";
import SkeletonLoading from "@/componentsV2/Common/SkeletonLoading";
import { cleanObject, delayResult } from "@/utils/index";
import qs from "qs"
import { InputWithLabel } from "../../components/atoms/inputWithLabel";
import { SelectPicker } from "../../components/atoms/selectPicker";
import { toFixedNumber } from "@/utils/number";
import useTranslation from "@/hooks/useTranslation";
dayjs.extend(isBetween);


const UnitTestAssign = () => {
    const { t } = useTranslation()
    const STATUS_OPTION_FILTER = [
        {
            code: "1",
            display: t("status")['submitted']
        },
        {
            code: "2",
            display: t("status")['no-submitted']
        }
    ];
    const { push, query } = useRouter();
    const refInput = useRef<NodeJS.Timeout>()

    const assignId = +query?.learningSlug;
    const initValueInput = query?.name || ''

    const [valueInput, setValueInput] = useState(initValueInput)
    const [dataAssignResults, setDataAssignResults] = useState([]);
    const [isLoadPage, setIsLoadPage] = useState(true)
    const [isLoadAssign, setIsLoadAssign] = useState(0);
    const [dataDetailUnitTestAssign, setDataDetailUnitTestAssign] = useState(null);
    const [severTime, setSeverTime] = useState("");
    const [searching, setSearching] = useState(false)
    const [isFirstRender, setIsFirstRender] = useState(true)

    const initExamStatus = STATUS_OPTION_FILTER.find(item => item.code === query?.status?.toString())

    const objQuery = useMemo(() => {
        const obj = cleanObject({
            name: query?.name,
            status: query?.status,
        })
        return obj
    }, [query])

    const fetcherDataAssignResults = async () => {
        try {
            const queryString = qs.stringify(objQuery)
            const response: any = await  delayResult(callApi(`${paths.api_unit_test_assigned}/${assignId}/results?${queryString}`), 400);
            setIsLoadAssign(prev => Math.max(prev - 1, 0));
            if(response.code === 200){
                const dataResult = response?.data?.filter((item:any) => {
                    if(+query?.status === 1){
                        return item.unit_test_result
                    }
                    if(+query?.status === 2) {
                        return item.unit_test_result === null;
                    }
                    return item
                })
                setDataAssignResults(dataResult);
                setIsFirstRender(false)
            }
        } catch (e: any) {
            setIsLoadAssign(prev => Math.max(prev - 1, 0));
            push("/_error").finally();
        }

    };

    const fetchDetailUnitTestAssign = async () => {
        try {
            const response:any = await  delayResult(callApi(`${paths.api_unit_test_assigned}/${assignId}`), 400)
            setIsLoadPage(false);
            if(response.code === 200){
                setDataDetailUnitTestAssign(response?.data);
                setSeverTime(response?.serverTime);
            }
        } catch (e) {
            setIsLoadPage(false);
        }

    }
    useEffect(() => {
        fetchDetailUnitTestAssign().finally()
    }, [])

    useEffect(() => {
        setIsLoadAssign(prev => prev + 1)
        fetcherDataAssignResults().finally()
    }, [objQuery]);

    const onClickItem = (dataAssignId: string) => {
        push({
            pathname: `${pathsUrl.learningResult}/${query.learningSlug}`,
            query: {
                studentId: dataAssignId
            }
        }).finally();
    };
    const checkStatusAssign = ({ today, startDate, endDate }: any) => {
        const isBetween = dayjs(today).isBetween(startDate, endDate, "millisecond", "[]");
        if (isBetween) return STATUS_CODE_EXAM.ONGOING;
        return compareTwoDate({ dateOne: today, dateTwo: startDate });
    };
    const compareTwoDate = ({ dateOne, dateTwo }: any) => {
        const isAfter = dayjs(dateOne).isAfter(dateTwo, "millisecond");
        if (isAfter) return STATUS_CODE_EXAM.ENDED;
        return STATUS_CODE_EXAM.UPCOMING;
    };
    const unitTestDetail = useMemo(() => {
        const titleGroup = dataDetailUnitTestAssign?.group ? dataDetailUnitTestAssign?.group?.name : t('learning-result-page')['all-learner'];
        const boxTitle = `${dataDetailUnitTestAssign?.class?.name} - ${titleGroup}` || "";
        const statusAssign = checkStatusAssign({
            today: severTime,
            startDate: dataDetailUnitTestAssign?.start_date,
            endDate: dataDetailUnitTestAssign?.end_date
        });
        return {
            pageTitle: dataDetailUnitTestAssign?.unitTest?.name || "",
            boxTitle: boxTitle,
            statusAssign: statusAssign
        };
    }, [dataDetailUnitTestAssign, severTime]);
    const onChangeStatus = (value:string|number) => {
        push({
            pathname: `${pathsUrl.learningResult}/${query.learningSlug}`,
            query: {
                ...objQuery,
                status: value
            }
        }).finally()
    }

    const onChangeName = (value:string) => {
        if (refInput.current) {
            clearTimeout(refInput.current);
        }
        if (value === ''){
            setIsFirstRender(true)
        }
        setValueInput(value);
        refInput.current = setTimeout(() => {
            push({
                pathname: `${pathsUrl.learningResult}/${query.learningSlug}`,
                query: {
                    ...objQuery,
                    name: value
                }
            }).finally()
        }, 300);
    }
    const disabledReset = useMemo(()=> {
        return !query.name && !query.status
    }, [query])

    const onClickReset = () => {
        setValueInput("")
        push(`${pathsUrl.learningResult}/${query.learningSlug}`).finally()
        setIsFirstRender(true)
    }

    useEffect(() => {
        if (query?.name || query?.status) setSearching(true)
        else setSearching(false)
    }, [
        query?.name,
        query?.status
    ])

    const isSearchEmpty = useMemo(() => {
        if (!dataAssignResults || dataAssignResults.length === 0 && !searching) {
            return false
        }
        return true
    }, [
        searching, 
        dataAssignResults
    ])

    return (
        <Wrapper pageTitle={unitTestDetail.pageTitle} hasStickyFooter={false} className={styles.wrapperAssign}>
            {isLoadPage && (
                    <div className={styles.containerBox}>
                        <div className={styles.header}>
                            <SkeletonLoading height="30px" width="100%" borderRadius="8px"/>
                        </div>
                        <div className={styles.filterBox}>
                            <SkeletonLoading height="36px" width="100%" borderRadius="8px"/>
                        </div>
                        <div className={styles.contentData}>
                            <Loader backdrop center content="loading..." vertical/>
                        </div>
                    </div>
                ) 
            }
            {!isLoadPage && (
                <div className={styles.containerBox}>
                    <div className={styles.header}>
                        <span className={styles.infoUnitTest}>{unitTestDetail.boxTitle}</span>
                        <span className={classNames(styles.statusUnitTest, {
                            [styles.ongoing]: unitTestDetail.statusAssign === STATUS_CODE_EXAM.ONGOING,
                            [styles.ended]: unitTestDetail.statusAssign === STATUS_CODE_EXAM.ENDED
                        })}>{t('status')[CODE_ASSIGN_OPTION[unitTestDetail.statusAssign - 1].label]}</span>
                    </div>
                    {(isSearchEmpty || isFirstRender) && (
                        <div className={styles.filterBox}>
                            <InputWithLabel
                                type="text"
                                style={{ width: 336 }}
                                icon={<SearchIcon />}
                                label={t('learning-result-page')['search-learner']} onChange={onChangeName}
                                defaultValue={valueInput}
                                shouldUpdateDefaultValue={true}
                            />
                            <SelectPicker
                                label={t('assign-exam-filter')['status-exam']}
                                data={STATUS_OPTION_FILTER}
                                className={styles.selectPicker}
                                style={{ width: 161 }}
                                onChange={onChangeStatus}
                                shouldUpdateDefaultValue={true}
                                defaultValue={initExamStatus}
                            />
                            <Button
                                disabled={disabledReset}
                                appearance="ghost"
                                className={styles.btnReset}
                                onClick={onClickReset}
                            >
                                {t('common')['reset']}
                                <IconReset />
                            </Button>
                            {/*<Button*/}
                            {/*    appearance="primary"*/}
                            {/*    className={styles.exportResult}*/}
                            {/*>*/}
                            {/*    Xuất kết quả*/}
                            {/*</Button>*/}
                        </div>
                    )}
                    <div className={styles.contentData}>
                        {isLoadAssign ? (
                                <Loader backdrop center content="loading..." vertical />
                            ) :
                            <div className={classNames(styles.gridData, {
                                [styles.noData]: !dataAssignResults.length
                            })}>
                                {!!dataAssignResults.length ?
                                    Array.from(dataAssignResults, (dataAssign: any, index) => {
                                        return (
                                            <div className={classNames(styles.itemUserAssign, {
                                                [styles.noResultAssign]: !dataAssign?.unit_test_result
                                            })}
                                                 key={dataAssign.id}
                                                 onClick={() => onClickItem(dataAssign.id)}
                                            >
                                                <div className={styles.avatarDefault}>
                                                    <StudentIcon />
                                                </div>
                                                <div className={styles.userResultAssign}>
                                                    <span title={dataAssign?.full_name} className={styles.userName}>{dataAssign?.full_name}</span>
                                                    {dataAssign?.unit_test_result ?
                                                        <>
                                                            <span className={styles.statusLearning}>{t('status')['submitted']}</span>
                                                            <span
                                                                className={styles.resultLearning}>{t('learning-result-page')['point']}: {toFixedNumber(dataAssign?.unit_test_result?.point)}/{dataAssign?.unit_test_result?.max_point}</span>
                                                        </> :
                                                        <>
                                                            <span className={styles.statusLearning}>{t('status')['no-submitted']}</span>
                                                        </>
                                                    }
                                                </div>
                                            </div>
                                        );
                                    }) : (                                        
                                        <NoFieldData
                                            field='unit_test_assign_results'
                                            hasAction={false}
                                            isSearchEmpty={isSearchEmpty}
                                        />
                                    )}
                            </div>}
                    </div>
                </div>
            )}            
        </Wrapper>
    );
};

export default UnitTestAssign;