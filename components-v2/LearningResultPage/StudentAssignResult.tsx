import React, { useEffect, useMemo, useState } from "react";

import dayjs from "dayjs";
import { useSession } from "next-auth/client";
import { useRouter } from "next/router";
import { Button, Loader } from "rsuite";

import ChevronIcon from '@/assets/icons/arrow.svg'
import { paths as pathsUrl } from "@/interfaces/constants";
import { delayResult } from "@/utils/index";

import { paths } from "../../api/paths";
import { callApi } from "../../api/utils";
import { Wrapper } from "../../components/templates/wrapper";
import styles from "./StudentAssignResult.module.scss"
import { toFixedNumber } from "@/utils/number";
import useTranslation from "@/hooks/useTranslation";
const StudentAssignResult = () => {
    const { t, locale, subPath } = useTranslation();
    const { push, back, query } = useRouter();
    const [ session ] = useSession()
    const user:any = session?.user
    const assignId = query?.learningSlug
    const studentId = query?.studentId
    const [dataUserResult, setDataUserResult] = useState(null)
    const [isLoad, setIsLoad] = useState(0)

    const fetcherDataUserResult = async () => {
        try {
            const response:any = await delayResult(callApi(`${paths.api_unit_test_assigned}/${assignId}/result-detail?studentId=${studentId}`),400)
            setIsLoad(prev => Math.max(prev - 1, 0))
            if(response.code === 200){
                setDataUserResult(response?.data)
            }
        }catch (e) {
            setIsLoad(prev => Math.max(prev - 1, 0))
            push("/_error").finally()
        }

    }
    useEffect(() => {
        setIsLoad(prev => prev + 1)
        fetcherDataUserResult().finally()
    }, [])

    const pageTitle = useMemo(() => {
        return dataUserResult?.student || {}
    }, [dataUserResult])
    const onClickBack = () => {
        back()
    }
    const renderTextWithIndex = (index: number) => {
        if (locale === 'vi') {
            return `Lần thi ${index}`;
        }
        switch (index) {
            case 1:
                return `${index}st times`;
            case 2:
                return `${index}nd times`;
            case 3:
                return `${index}rd times`;
            default:
                return `${index}th times`;
        }
    }
    return (
        <Wrapper pageTitle={pageTitle?.full_name || ''} hasStickyFooter={false}>
            {!!dataUserResult?.content.length &&
				<div className={styles.back} onClick={() => back()}>
                    <ChevronIcon />
					<span>{t('common')['back']}</span>
				</div>}
            <div className={styles.studentAssignResult}>
                { isLoad ?
                    <Loader backdrop center content="loading..." vertical/>
                    :
                    !!dataUserResult?.content.length ?
                        Array.from(dataUserResult?.content,(item:any,index)=>{
                            const count = dataUserResult.content.length
                            const pathHistory = `${subPath}/practice-test/${user?.id}===${item.unit_test_id}===${item.id}`
                            return (
                                <div className={styles.resultItem}>
                                    <div className={styles.title}>
                                        <span>{renderTextWithIndex(count - index)}: {dataUserResult.unitTest.name}</span>
                                    </div>
                                    <div className={styles.content}>
                                        <div className={styles.itemLeft}>
                                            <span>{t("learning-result-page")["point"]}:</span>
                                            <span>{t("learning-result-page")["deadline-time"]}:</span>
                                        </div>
                                        <div className={styles.itemRight}>
                                            <span>{toFixedNumber(item.point)}/{item.max_point}</span>
                                            <span>{dayjs(item.created_date).format('DD/MM/YYYY HH:mm')}</span>
                                        </div>
                                    </div>
                                    <Button className={styles.button} as={"a"} target={"_blank"} href={pathHistory} appearance="primary">{t('learning-result-page')['exam-review']}</Button>
                                </div>
                            )
                        }) : (
                            <div className={styles.noResult}>
                                <img src={"/images-v2/no-result.png"} alt=""/>
                                <span>{t('assign-exam')['no-result']}</span>
                                <Button onClick={onClickBack} appearance="ghost">{t('common')['back']}</Button>
                            </div>
                        )
                }
            </div>
        </Wrapper>
    );
};

export default StudentAssignResult;