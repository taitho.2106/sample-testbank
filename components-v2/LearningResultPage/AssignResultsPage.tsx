import React, { useMemo } from "react";

import { useRouter } from "next/router";

import StudentAssignResult from "@/componentsV2/LearningResultPage/StudentAssignResult";
import UnitTestAssign from "@/componentsV2/LearningResultPage/UnitTestAssign";
const AssignResultsPage = ({ query }: any) => {
    const { push } = useRouter();
    return useMemo(() => {
        if (!isNaN(Number(query?.learningSlug)) && !query?.studentId) {
            return <UnitTestAssign />;
        }else if (query?.studentId) {
            return <StudentAssignResult />;
        }else{
            push("/_error").finally();
            return  null
        }

    }, [query]);
};
export default AssignResultsPage;