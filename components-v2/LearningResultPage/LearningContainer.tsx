import React, { useEffect, useMemo, useRef, useState } from "react";

import SearchIcon from '@rsuite/icons/Search';
import classNames from "classnames";
import dayjs from "dayjs";
import isBetween from 'dayjs/plugin/isBetween'
import { useSession } from "next-auth/client";
import Link from "next/link";
import { useRouter } from "next/router";
import qs from "qs"
import { AiOutlineEye } from 'react-icons/ai'
import { BsBookmark } from 'react-icons/bs'
import { GoKebabVertical } from 'react-icons/go'
import { Button, Loader, Pagination, Popover, Whisper } from "rsuite";

import IconExport from "@/assets/icons/icon-export.svg"
import IconCopyLink from "@/assets/icons/link-url.svg";
import IconReset from '@/assets/icons/reset-icon.svg';
import SkeletonLoading from "@/componentsV2/Common/SkeletonLoading";
import useGrade from "@/hooks/useGrade";
import useNoti from "@/hooks/useNoti";
import { CODE_ASSIGN_OPTION, paths as pathsURL, STATUS_CODE_EXAM } from "@/interfaces/constants";
import { cleanObject, delayResult, onCopyUrl } from "@/utils/index";
import { StickyFooter } from 'components/organisms/stickyFooter'

import { paths } from "../../api/paths";
import { callApi } from "../../api/utils";
import { InputWithLabel } from "../../components/atoms/inputWithLabel";
import { SelectPicker } from "../../components/atoms/selectPicker";
import NoFieldData from "../NoDataSection/NoFieldData";
import styles from './LearningContainer.module.scss'
import useTranslation from "@/hooks/useTranslation";

dayjs.extend(isBetween)
export default function LearningContainer() {
    const { t } = useTranslation();
    const { push, query, isReady } = useRouter()
    const { getNoti } = useNoti();
    const { getGrade } = useGrade()
    const [ session ] = useSession();
    const user:any = session?.user

    const limitPage = 10;
    const currentPage = +query?.page || 1
    const dataOption = CODE_ASSIGN_OPTION.map(item => {
        return {
            code: item.value.toString(),
            display: t('status')[item.label]
        };
    });
    const GradesOptions = getGrade
        .filter(item => /^G[0-9]{1,2}$/.test(`${item.code}`))
    const initInputValue = query?.name?.toString() || ''
    const initExamStatus = dataOption.find(item => item.code === query?.examStatus?.toString())

    const [dataUnitTestAssign, setDataUnitTestAssign] = useState([])
    const [totalDataAssign, setTotalDataAssign] = useState(0)
    const [isLoading, setIsLoading] = useState(0)
    const [severTime, setSeverTime] = useState('')
    const [valInput, setValInput] = useState(initInputValue)
    const [dataOptionClasses, setDataOptionClasses] = useState([])
    const [isFirstRender, setIsFirstRender] = useState(true)

    const refInput = useRef<NodeJS.Timeout>()

    const fetcherClassIdByGrade = async ({objQuery}:any) => {
        const response:any = await callApi(`${paths.api_classes}?${objQuery}`)
        if(response.code === 200){
            setSeverTime(response?.serverTime)
            const dataOption = response?.data?.content?.map((item: any) => {
                return {
                    code: item.id.toString(),
                    display: item.name,
                }
            })
            setDataOptionClasses(dataOption)
        }
    }
    const fetcherDataAssigned = async () => {
        try {
            setIsLoading(prev => prev + 1)
            onScrollToTop()
            const objQueryString = qs.stringify({ ...objQuery, size: limitPage, page: currentPage - 1 });
            const response: any = await delayResult(callApi(`${paths.api_unit_test_assigned}?${objQueryString}`),400);
            setIsLoading(prev => Math.max(prev - 1, 0));
            if (response.code === 200) {
                setDataUnitTestAssign(response?.data?.content);
                setTotalDataAssign(response?.data?.total);
                setIsFirstRender(false)
            }
        } catch (e: any) {
            setIsLoading(prev => Math.max(prev - 1, 0));
        }
    }

    const objQuery = useMemo(() => {
        const obj = cleanObject({
            size: limitPage,
            name: query.name,
            grade: query?.grade,
            classId: query?.classId,
            examStatus: query?.examStatus
        })
        return obj
    }, [query])

    useEffect(() => {
        const objQueryString = qs.stringify({ ...objQuery, size: 100 })
        fetcherClassIdByGrade({objQuery:objQueryString}).finally()

    }, [objQuery.grade])

    useEffect(() => {
        if(isReady){
            fetcherDataAssigned().finally()
        }
    }, [objQuery])

    const handleSearchChange = (value: string) => {
        if (refInput.current) {
            clearTimeout(refInput.current);
        }
        setValInput(value);
        if (value === ''){
            setIsFirstRender(true)
        }
        refInput.current = setTimeout(() => {
            push({
                pathname: `${pathsURL.learningResult}`,
                query: cleanObject({
                    ...objQuery,
                    name: value
                })
            }).finally()
        }, 300);
    }

    const onChangeGrade = (value: string| number) => {
        push({
            pathname: pathsURL.learningResult,
            query: cleanObject({
                ...objQuery,
                grade:value,
                classId: undefined
            })
        }).finally();
    }
    const onChangeClasses = (value:string|number) => {
        push({
            pathname: pathsURL.learningResult,
            query: cleanObject({
                ...objQuery,
                classId:value
            })
        }).finally();
    }

    const onChangeStatus = (value:string|number) => {
        push({
            pathname: pathsURL.learningResult,
            query: cleanObject({
                ...objQuery,
                examStatus: value
            })
        }).finally();

    }

    const onClickReset = () => {
        setIsFirstRender(true)
        push({
            pathname: pathsURL.learningResult,
        }).finally();
        setValInput('')
    }

    const onScrollToTop = () => {
        const content = document.getElementById("container-body")
        content?.scrollTo({
            top: 0,
            behavior: 'smooth',
        })
    }
    const onChangePage = (page: number) => {
        push({
            pathname: pathsURL.learningResult,
            query: {
                ...objQuery,
                page
            }
        }).finally();
    }

    const disableFilter = useMemo(()=>{
        if(query){
            return !query?.name && !query?.classId && !query?.grade && !query?.examStatus
        }
        return true
    }, [query])

    const onCopy = (unitTestAssign:any) => {
        const path = `practice-test/${user?.id}===${unitTestAssign.unitTest.id}?idAssign=${unitTestAssign.id}`
        onCopyUrl(path)
        getNoti('success', 'topCenter', t('common-get-noti')['copy-url-succeed'])
    }

    const getPathUrl = (unitTestAssign:any) => {
        if(unitTestAssign.student)
            return `${pathsURL.learningResult}/${unitTestAssign.id}?studentId=${unitTestAssign.student.id}`
        return `${pathsURL.learningResult}/${unitTestAssign.id}`
    }

    const isSearchEmpty = useMemo(() => {
        if (!dataUnitTestAssign || dataUnitTestAssign.length === 0 && !query?.name && !query?.classId && !query?.grade && !query?.examStatus) {
            return false
        }
        return true
    }, [query, dataUnitTestAssign])

    return (
        <>
        {(isSearchEmpty || isFirstRender) && (
        <div className={styles.container}>
            <div className={styles.filterBox}>
                <InputWithLabel type="text" style={{ width: 336 }} icon={<SearchIcon />}
                                label={t('assign-exam-filter')['search-name']} onChange={handleSearchChange}
                                defaultValue={valInput}
                                shouldUpdateDefaultValue={true}
                />
                {GradesOptions ? <SelectPicker
                    label={t('common')['grade']}
                    data={GradesOptions}
                    className={styles.selectPicker}
                    style={{ width: 161 }}
                    onChange={onChangeGrade}
                    shouldUpdateDefaultValue={true}
                    defaultValue={GradesOptions.find(item => item.code === query?.grade?.toString())}
                /> : <SkeletonLoading height="36px" width="161px" borderRadius="6px"/>}
                {dataOptionClasses && <SelectPicker
                    label={t('learring-result')['class']}
                    data={dataOptionClasses}
                    className={styles.selectPicker}
                    style={{ width: 161 }}
                    onChange={onChangeClasses}
                    shouldUpdateDefaultValue={true}
                    defaultValue={dataOptionClasses.find(item => item.code === query?.classId?.toString())}
                />}
                <SelectPicker
                    label={t('learring-result')['status']}
                    data={dataOption}
                    className={styles.selectPicker}
                    style={{ width: 161 }}
                    onChange={onChangeStatus}
                    shouldUpdateDefaultValue={true}
                    defaultValue={initExamStatus}
                />
                <Button
                    disabled={disableFilter}
                    appearance="ghost"
                    className={styles.btnReset}
                    onClick={onClickReset}
                >
                    {t('common')['reset']}
                    <IconReset/>
                </Button>
            </div>
            <div className={styles.tableData}>
                <div className={styles.header}>
                    <div>{t('learring-result')['unit-test-name']}</div>
                    <div>{t('learring-result')['class']}</div>
                    <div>{t('learring-result')['assign-subject']}</div>
                    <div>{t('learring-result')['to-do-time']}</div>
                    <div>{t('learring-result')['status']}</div>
                    <div></div>
                </div>
                {!!isLoading ?
                    (
                        <div className={styles.body}>
                            <Loader backdrop center content="loading..." vertical />
                        </div>

                    )   :
                    <div className={styles.body} id="container-body">
                        {!!dataUnitTestAssign.length && Array.from(dataUnitTestAssign, (unitTestAssign: any, index: number) => {
                            const assignObject = unitTestAssign?.student ?
                                unitTestAssign?.student?.full_name :
                                unitTestAssign?.group ?
                                    unitTestAssign?.group?.name : t('assigned-subject')['class'];
                            const Com = unitTestAssign.examStatus !== STATUS_CODE_EXAM.UPCOMING ?
                                ({ children }:any) => <Link href={getPathUrl(unitTestAssign)}>{children}</Link> :
                                ({ children }:any) => <span>{children}</span>
                            return (
                                <div className={styles.item} key={index}>
                                    <div className={styles.itemInfo}>
                                        <div title={unitTestAssign.unitTest.name}><Com>{unitTestAssign.unitTest.name}</Com></div>
                                        <div title={unitTestAssign.class.name}><span>{unitTestAssign.class.name}</span>
                                        </div>
                                        <div title={assignObject}><span>{assignObject}</span></div>
                                        <div className={classNames({
                                            [styles.ongoing]: unitTestAssign.examStatus === STATUS_CODE_EXAM.ONGOING,
                                            [styles.ended]: unitTestAssign.examStatus === STATUS_CODE_EXAM.ENDED
                                            })}>
                                            <div className={styles.title}>
                                                <span>{t('unit-test-assigned-page')['from']}</span>
                                                <span>{t('unit-test-assigned-page')['to']}</span>
                                            </div>
                                            <div className={styles.timer}>
                                                <span>{dayjs(unitTestAssign.start_date).format("HH:mm")}</span>
                                                <span>{dayjs(unitTestAssign.end_date).format("HH:mm")}</span>
                                            </div>
                                            <div className={styles.date}>
                                                <span>{dayjs(unitTestAssign.start_date).format("DD/MM/YYYY")}</span>
                                                <span>{dayjs(unitTestAssign.end_date).format("DD/MM/YYYY")}</span>
                                            </div>
                                        </div>
                                        <div>
                                            <span className={classNames({
                                                [styles.ongoing]: unitTestAssign.examStatus === STATUS_CODE_EXAM.ONGOING,
                                                [styles.ended]: unitTestAssign.examStatus === STATUS_CODE_EXAM.ENDED
                                            })}>{t('status')[CODE_ASSIGN_OPTION[unitTestAssign.examStatus-1].label]}</span>
                                        </div>
                                        <Whisper
                                            placement={"auto"}
                                            trigger={"click"}
                                            container={() => document.getElementById("container-body")}
                                            speaker={
                                                <Popover arrow={false} className={styles.popoverWrapper}>
                                                    <Link
                                                        href={`${pathsURL.unitTest}/${unitTestAssign.unitTest.id}?mode=view&m=mine`}>
                                                        <a target="_blank"><AiOutlineEye /><span>{t('assign-exam')['content-view']}</span></a>
                                                    </Link>
                                                    {unitTestAssign.examStatus !== STATUS_CODE_EXAM.ENDED &&
														<div onClick={() => onCopy(unitTestAssign)}>
															<IconCopyLink /><span>{t('assign-exam')['copy-url']}</span>
														</div>}
                                                    {unitTestAssign.examStatus !== STATUS_CODE_EXAM.UPCOMING &&
                                                        <>
                                                            <Link href={getPathUrl(unitTestAssign)}>
                                                                <a><BsBookmark /><span>{t('assign-exam')['learing-result-view']}</span></a>
                                                            </Link>
															{/*<div><IconExport /><span>Xuất kết quả thi</span></div>*/}
                                                        </>
                                                    }
                                                </Popover>
                                            }
                                        >
                                            <div className={styles.action}>
                                                <button
                                                    onClick={e => e.preventDefault()}
                                                ><GoKebabVertical /></button>
                                            </div>
                                        </Whisper>
                                    </div>
                                </div>
                            );
                        })}
                    {!dataUnitTestAssign.length && (
                        // hiện khi tìm kiếm không có dữ liệu
                        <NoFieldData
                            field='learning_container'
                            hasAction={false}
                            isSearchEmpty={isSearchEmpty}
                        />
                    )}
                </div>}
            </div>
            {!!dataUnitTestAssign.length && <StickyFooter>
                <Pagination
                    prev
                    next
                    size="sm"
                    total={totalDataAssign}
                    limit={10}
                    activePage={currentPage}
                    onChangePage={onChangePage}
                    className={styles.pagination}
                />
            </StickyFooter>}
        </div>
        )}            
        {!isLoading && !isFirstRender && !isSearchEmpty && (
            // hiện khi chưa có dữ liệu
            <div className={styles.container}>
                <NoFieldData
                    field='learning_container'
                    hasAction={false}
                    isSearchEmpty={isSearchEmpty}
                />
            </div>
        )}   
        </>
    )
}