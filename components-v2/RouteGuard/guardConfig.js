import { PACKAGES, paths, USER_ROLES } from "../../interfaces/constants";

const roleConfig = Object.freeze({
    admin: -1, // custom
    operator: USER_ROLES.Operator,
    teacher: USER_ROLES.Teacher,
    student: USER_ROLES.Student,
});

// packageRequired example
// packageRequired: 1 -> not apply for admin + operator
// packageRequired: { -1: 'ADVANCE', 1: 'BASIC' }

// accessRole undefined will ignore check role
// accessRole [] will ignore all role

const makeGroupConfig = (groups, config) => {
    return groups.reduce((rs, cur) => ({ ...rs, [cur]: config }), {});
}

const guardConfig = {
    [paths.unitTestSlug]: {
        authRequired: true,
        pakacgeRequired: PACKAGES.BASIC.CODE,
        matchRouteWithPackage: (route, userRole ,currentPackage) => matchConfig( route, userRole ,currentPackage, paths.createUnitTest),
        accessRole: [
            roleConfig.admin,
            roleConfig.operator,
            roleConfig.teacher,
        ],
    },
    [paths.templateSlug]: {
        authRequired: true,
        packageRequired: PACKAGES.BASIC.CODE,
        matchRouteWithPackage: (route, userRole ,currentPackage) => matchConfig( route, userRole ,currentPackage, paths.createTemplate),
        accessRole: [
            roleConfig.admin,
            roleConfig.operator,
            roleConfig.teacher,
        ],
    },
    ...makeGroupConfig([paths.dashboard, paths.questions, paths.questionSlug, paths.templates], {
        authRequired: true,
        packageRequired: PACKAGES.BASIC.CODE,
        accessRole: [
            roleConfig.teacher,
            roleConfig.admin,
            roleConfig.operator,
        ]
    }),
    [paths.dashboardClassRanking]: {
        authRequired: true,
        packageRequired: PACKAGES.BASIC.CODE,
        accessRole: [
            roleConfig.teacher,
        ],
    },
    ...makeGroupConfig([paths.unitTest, paths.detailSeries, paths.classes, paths.classeSlug], {
        authRequired: true,
        packageRequired: PACKAGES.BASIC.CODE,
        accessRole:[roleConfig.admin, roleConfig.operator, roleConfig.teacher, roleConfig.student],
    }),
    ...makeGroupConfig([paths.classes, paths.classeSlug, paths.studentSlug], {
        authRequired: true,
        packageRequired: PACKAGES.BASIC.CODE,
        accessRole: [roleConfig.teacher],
    }),
    ...makeGroupConfig([paths.integrateExam, paths.integrateExamSlug, paths.overview, ],{
        authRequired: true,
        accessRole:[roleConfig.admin, roleConfig.operator],
    }),
    [paths.userInfo]:{
        authRequired: true,
    },
    ...makeGroupConfig([paths.transactionHistory, paths.testSchedule],{
        authRequired: true,
        packageRequired: PACKAGES.BASIC.CODE,
        accessRole:[roleConfig.teacher, roleConfig.student],
    }),
    ...makeGroupConfig([paths.dashboardUser, paths.dashboardTurnover, paths.dashboardServicePackage], {
        authRequired: true,
        accessRole:[roleConfig.admin],
    }),
    ...makeGroupConfig([paths.payment, paths.paymentResults], {
        authRequired: true,
    }),
    ...makeGroupConfig([paths.assignedUnitTest, paths.assignedUnitTestSlug], {
        authRequired: true,
        accessRole:[roleConfig.student],
    })
};

const matchConfig = ( route, userRole ,currentPackage, slug) => {
    
    const { asPath, query } = route
    const pathWithoutQuery = asPathWithoutQuery(asPath)

    if(!currentPackage || !userRole) return false

    if (currentPackage?.code === PACKAGES.BASIC.CODE && pathWithoutQuery === slug) return false

    if ((userRole === roleConfig.operator || userRole === roleConfig.admin) && query?.m === 'mine') return false

    if(userRole === roleConfig.teacher && pathWithoutQuery === slug && (!query?.m || query?.m !== 'mine')) return false

    return true
}

const asPathWithoutQuery = asPath => asPath?.split('?')[0]

export { roleConfig };

export default guardConfig;