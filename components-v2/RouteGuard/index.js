import { useSession } from 'next-auth/client';
import { useRouter } from 'next/router';
import React, { useEffect, useState } from 'react';

import useCurrentUserPackage from '../../hooks/useCurrentUserPackage';
import { paths, USER_ROLES } from '../../interfaces/constants';
import { userInRight } from '../../utils';
import { checkAccessPackageLevel } from '../../utils/user';
import guardConfig from './guardConfig';

import { CircleLoading } from '../Common/Controls/Loading';

import styles from './index.module.scss';

const RouteGuard = ({
    children,
}) => {
    const [showChildren, setShowChildren] = useState(false);
    const [ session, loadingUser ] = useSession();
    const userPackage = useCurrentUserPackage();
    const route = useRouter();
    const { pathname, push } = route;
    const currentPackage = userPackage?.dataPlan;
    const isLoadingCurrentPackage = userPackage?.isLoading;
    const isPackageExpired = userPackage?.isExpired;
    const isOperator = userInRight([USER_ROLES.Operator], session)
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const user = session?.user;
    const userRole = user?.is_admin ? -1 : user?.user_role_id;

    const checkAccessRole = (accessConfig) => {
        return accessConfig.some(access => {
            if (typeof access === 'function') {
                return access(route, userRole);
            }

            return access === userRole;
        });
    }

    const checkAccessPackage = (accessConfig) => {
        let packageRequired;
        if (typeof accessConfig !== 'object') {
            if (isOperator) {
                return true;
            }

            packageRequired = accessConfig;
        } else {
            packageRequired = accessConfig[userRole];
        }

        if (!packageRequired) {
            return true;
        }

        if (!currentPackage || isPackageExpired) {
            return false;
        }

        return checkAccessPackageLevel(currentPackage.code, packageRequired);
    }

    useEffect(() => {
        const checkAccess = () => {
            const currentPathConfig = guardConfig[pathname];

            if (!currentPathConfig || !currentPathConfig.authRequired) {
                return true;
            }

            if (loadingUser) {
                return false;
            }

            if (!session) {
                push(paths.signIn);
                return false;
            }

            if (currentPathConfig.accessRole && !checkAccessRole(currentPathConfig.accessRole)) {
                push(paths.home); // need update
                return false;
            }

            if (isLoadingCurrentPackage) {
                return false;
            }

            if (currentPathConfig.packageRequired?.length) {
                if(isStudent){
                    return true;
                }

                if (isLoadingCurrentPackage) {
                    return false;
                }

                if (!checkAccessPackage(currentPathConfig.packageRequired)) {
                    push(paths.payment); // need update
                    return false;
                }
            }

            if (currentPathConfig?.matchRouteWithPackage) {

                const checkRouteWithPackage = currentPathConfig?.matchRouteWithPackage(route, userRole ,currentPackage)

                if (!checkRouteWithPackage) {
                    push(paths.home);
                    return false;
                }

            }
            return true;
        }

        setShowChildren(checkAccess());
    }, [ loadingUser, isLoadingCurrentPackage, pathname ]);


    if (!showChildren) {
        return (
            <div className={styles.initLoading}>
                <CircleLoading />
            </div>
        );
    }

    return children;
};

export default RouteGuard;