import React from 'react'

import classNames from "classnames";
import { Button, Popover, Whisper } from "rsuite";

import { studentPricings } from "@/componentsV2/Home/data/price";
import { dataWithPackage } from "@/componentsV2/TeacherPages/UnitTestPage/data/dataWithPackage";
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import useTranslation from '@/hooks/useTranslation'
import { PACKAGES } from "@/interfaces/constants";

import PopoverUpgradeContent from "../../components/molecules/unitTestTable/PopoverUpgradeContent";
import { NoDataContent } from './data'
import styles from './NoFieldData.module.scss'

const NoFieldData = ({
    field = '',
    isSearchEmpty = false,
    hasAction = true,
    handleCreate = () => undefined,
    renderElement = () => <></>
}) => {
    const { t } = useTranslation()
    const content = NoDataContent.find(
        (item) => item.field === field,
    )
    const { dataPlan, listPlans } = useCurrentUserPackage();
    const isNotCreate = field === 'teacher_unit_test' && dataPlan?.code === PACKAGES.BASIC.CODE
    const dataDescription = studentPricings.teacher.find(item => item.code === PACKAGES.ADVANCE.CODE)?.descriptions || []
    const dataPackage = listPlans.find((item) => item.code === PACKAGES.ADVANCE.CODE)
    return (
        <div className={styles.emptyWrapper}>
            <img
                className={styles.banner}
                src='/images-v2/no-data.png'
                alt="banner"
            />
            <div className={styles.description}>
                {isSearchEmpty ? (
                    <>
                        <span className={styles.title}>
                            {t(`${t('no-field-data')[`${content.search_title}`]}`)}
                        </span>
                        <span className={styles.textContent}>
                            {t(`${t('no-field-data')[`${content.search_text}`]}`)}
                        </span>
                    </>
                ) : (
                    <>
                        <span className={styles.title}>
                            {t(`${t('no-field-data')[`${content.empty_title}`]}`)}
                        </span>
                        <span
                            className={styles.textContent}
                            dangerouslySetInnerHTML={{
                                __html: t(`${t('no-field-data')[`${content.empty_text}`]}`)
                            }}
                        />
                    </>
                )}
            </div>
            {!isSearchEmpty && hasAction && (
                isNotCreate ? (
                    <Whisper
                        trigger="hover"
                        placement="auto"
                        enterable
                        speaker={
                            <Popover className={styles.poperverModal} arrow={false}>
                                <PopoverUpgradeContent
                                    dataPackage={dataDescription}
                                    price={dataPackage?.exchange_price}
                                    itemId={dataPackage?.item_id}
                                />
                            </Popover>}
                    >
                        <button
                            disabled={true}
                            className={classNames(styles.createField, styles.block)}
                            onClick={handleCreate}
                        >
                            <span>{t('no-field-data')[content.btn_action]}</span>
                            <div className={styles.icon}>{dataWithPackage[PACKAGES.ADVANCE.CODE].icon}</div>
                        </button>
                    </Whisper>
                ) : (
                        <button className={styles.createField} onClick={handleCreate}>
                            <span>{t(`${t('no-field-data')[`${content.btn_action}`]}`)}</span>
                        </button>
                    )
            )}
            {renderElement()}
        </div>
    )
}

export default NoFieldData