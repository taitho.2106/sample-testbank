import { useContext } from "react";

import classNames from "classnames";
import Link from "next/link";
import { useRouter } from "next/router";

import ModalNotiReceivedComboUnitTest from './ModalNotiReceivedComboUnitTest';

import { useSession } from "next-auth/client";
import CallIcon from "../../../assets/icons/call.svg";
import FailedIcon from "../../../assets/icons/failed.svg";
import MailIcon from "../../../assets/icons/mail.svg";
import RestoreIcon from "../../../assets/icons/restore.svg";
import SuccessIcon from "../../../assets/icons/success.svg";
import { EMAIL_COMPANY } from "../../../constants";
import useTranslation from "../../../hooks/useTranslation";
import {
	ORDER_STATUS,
	USER_ASSET_TYPE,
	USER_ROLES,
	VNPAY_STATUS_CODE,
	paths as pathUrls
} from "../../../interfaces/constants";
import { AppContext } from "../../../interfaces/contexts";
import { formatNumber, renderNameByCode, userInRight } from "../../../utils";
import Button from "../../Common/Controls/Button";
import styles from "./index.module.scss";

const PaymentResults = () => {
	const { t } = useTranslation();
	const [session] = useSession();
	const isStudent = userInRight([-1, USER_ROLES.Student],session)
	const { query } = useRouter();
	const { vnp_ResponseCode, vnp_TransactionStatus } = query;
	const { orderData } = useContext(AppContext);

	const isBuyExam = orderData?.saleItem?.item_type && orderData?.saleItem?.item_type === USER_ASSET_TYPE.UNIT_TEST;
	const isBuyComboServicePackage = orderData?.saleItem?.item_type && orderData?.saleItem?.item_type === USER_ASSET_TYPE.COMBO_SERVICE_PACKAGE;
	const successApi = !orderData?.vnp_response_code && !orderData?.vnp_transaction_status;
	
	const isSuccess = vnp_ResponseCode === "00" && vnp_TransactionStatus === "00";
	const { saleItem, actual_price, quantity, status } = orderData || {};
	
	const renderIcon = () => {
		if (isSuccess && orderData?.status === ORDER_STATUS.NEW) {
			return (
				<RestoreIcon
					className={classNames(styles.iconWrapper, {
						[styles.processing]: orderData?.status === ORDER_STATUS.NEW
					})}
				/>
			);
		}
		
		if (isSuccess) {
			return <SuccessIcon className={styles.iconWrapper} fill="#21C274" />;
		}
		
		return (
			<FailedIcon
				className={classNames(styles.iconWrapper, {
					[styles.failed]: !isSuccess
				})}
			/>
		);
	};
	
	const renderMessage = () => {
		if (isSuccess && orderData?.status === ORDER_STATUS.NEW) {
			return <div dangerouslySetInnerHTML={{
				__html: t('result-payment')['process-payment']
			}}></div>;
		}
		
		if (isSuccess) {
			return <div
					dangerouslySetInnerHTML={{
						__html: t('result-payment')['success-payment']
					}}
				></div>;
		}
		
		return <div dangerouslySetInnerHTML={{ __html: t('message-vnpay')[VNPAY_STATUS_CODE[vnp_ResponseCode]] || t('result-payment')['error-payment'] }}></div>;
	};

    const btnBackHome = (
        <Link href={pathUrls.home}>
            <Button type={"outline"}>{t('result-payment')['action-fail']}</Button>
        </Link>
    );

    const btnStartUse = (
        <Link href={pathUrls.unitTest}>
            <Button>{t('result-payment')['action-success']}</Button>
        </Link>
    );

	const btnStartUseExam = (
		<Link href={`${pathUrls.practiceTest}/${session.user?.id}===${saleItem?.item?.id}`}>
			<a target="_blank">
				<Button style={{minWidth: 308}}>{t('result-payment')['action-success-buy-unittest']}</Button>
			</a>
		</Link>
	)

	const btnSeeListExam = (
		<Link href={`${pathUrls.unitTest}?m=mine`}>
			<Button type={"outline"} style={{minWidth: 308}}>{t('result-payment')['action-fail-buy-unittest']}</Button>
		</Link>
	)

	const renderActionBtn = () => {
		if(isBuyExam){
			return (
				isSuccess && !successApi ? (
						<>
							{btnStartUseExam}
							{btnSeeListExam}
						</>
				) : btnSeeListExam
			)
		}
		
		if (isBuyComboServicePackage) {
			return btnBackHome
		}

		return (
			isSuccess && status === ORDER_STATUS.DONE && !successApi ? (
				<>
					<div className={styles.desk}>{btnStartUse}</div>
					<div className={styles.mobile}>{btnBackHome}</div>
				</>
			) : btnBackHome
		)
	}

	return (
		<div className={styles.paymentResultsPage}>
            <ModalNotiReceivedComboUnitTest orderData={orderData}/>
			<div className={styles.mainContent}>
				<div className={styles.info}>
					<div className={styles.leftTop}>
						<div className={classNames(styles.orderStatus, {
							[styles.failed]: !isSuccess,
							[styles.processing]: isSuccess && status === ORDER_STATUS.NEW
						})}>
							{renderIcon()}
							{isSuccess && status === ORDER_STATUS.NEW ?
								t('result-payment')['progress']
								:
								isSuccess && status === ORDER_STATUS.DONE ? t('result-payment')['success'] : t('result-payment')['fail']}
						</div>
						<div className={styles.infoPlan}>
							<div className={classNames(styles.packageName, {
								[styles.notSuccess]: successApi || !isSuccess,
								[styles.isBuyExam]: isBuyExam,
							})}>
								<div>{isBuyExam ? t('result-payment')['unit-test'] : t('result-payment')['package']}:</div>
								<span className={classNames( {
										[styles.ellipsis]: isBuyExam
									})}>
									{renderNameByCode(saleItem?.item, t)}
								</span>
							</div>
							<div className={classNames(styles.packageName, {
								[styles.notSuccess]: successApi || !isSuccess,
								[styles.isBuyExam]: isBuyExam,
							})}>
								<div>{t('result-payment')['price']}: </div>
								<span className="fw-700">
									{formatNumber(actual_price || "")}đ{!isBuyExam ? <span> / {(quantity / isStudent ? 1 : 12 || "")} {t('year')}</span> : ''}
								</span>
							</div>
						</div>
					</div>
					<div className={styles.message}>
						{renderMessage()}
					</div>
				</div>
				<div className={styles.separate}>
					<div className={styles.line} />
				</div>
				<div className={styles.price}>
					<div className={styles.top}>
                        {renderActionBtn()}
					</div>
					<div className={styles.bottom}>
						{t('result-payment')['assistance']}
						<p><CallIcon />Hotline: 1800 6242</p>
						<p><MailIcon />{EMAIL_COMPANY}</p>
					</div>
				</div>
			</div>
		</div>
	);
};

export default PaymentResults;