import { useEffect, useMemo, useState } from 'react';
import { Modal } from 'rsuite';

import Button from '../../Common/Controls/Button';

import { SALE_ITEM_TYPE } from '@/constants/index';
import { comboLevel, ORDER_STATUS, PACKAGES_FOR_STUDENT } from "@/interfaces/constants";
import { useUserInfo } from 'lib/swr-hook';

import IconCongratulations from '@/assets/icons/congratulations.svg';

import useTranslation from '@/hooks/useTranslation';
import styles from './ModalNotiReceivedComboUnitTest.module.scss';

const ModalNotiReceivedComboUnitTest = ({ orderData }) => {
    const {t} = useTranslation()
    const [showModalNoti, setShowModalNoti] = useState(false);
    const { userDisplayName } = useUserInfo();
    const { extra_data, status, vnp_response_code, vnp_transaction_status } = orderData || {};
    const isSuccess = status === ORDER_STATUS.DONE && vnp_response_code === '00' && vnp_transaction_status === '00';

    const {
        saleItemType,
        oldCombo,
        newCombo,
        addDayExpire,
        addQuantity,
    } = useMemo(() => {
        if (extra_data) {
            try {
                return JSON.parse(extra_data);
            } catch (error) {
                console.log({ error });
            }
        }

        return {};
    }, [extra_data]);

    useEffect(() => {
        if (saleItemType === SALE_ITEM_TYPE.UNIT_TEST && isSuccess) {
            setShowModalNoti(true);
        }
    }, [saleItemType, isSuccess]);

    const { title, lbQuantity, lbTime } = useMemo(() => {
        const comboName = (PACKAGES_FOR_STUDENT[newCombo]?.NAME || '').toLowerCase().replace(/^gói/i, '').trim();

        const oldComboLevel = comboLevel.findIndex(code => code === oldCombo);
        const newComboLevel = comboLevel.findIndex(code => code === newCombo);

        if (oldComboLevel === newComboLevel) {
            return {
                title: t(t('modal-noti-received-combo-unittest')['title-1'],[`${comboName}`]),
                lbQuantity: t(t('modal-noti-received-combo-unittest')['quality'],[`${addQuantity}`]),
                lbTime: t(t('modal-noti-received-combo-unittest')['time'],[`${addDayExpire}`]),
            }
        }

        if (newComboLevel > oldComboLevel) {
            return {
                title: t(t('modal-noti-received-combo-unittest')['title-2'],[`${comboName}`]),
                lbQuantity: oldCombo === PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                    ? t(t('modal-noti-received-combo-unittest')['quality-1'],[`${addQuantity}`])
                    : t(t('modal-noti-received-combo-unittest')['quality-2'],[`${addQuantity}`]),
                lbTime: t(t('modal-noti-received-combo-unittest')['time-1'],[`${addDayExpire}`]),
            }
        }

        return {
            title: t(t('modal-noti-received-combo-unittest')['title-3'],[`${comboName}`]),
            lbQuantity: t(t('modal-noti-received-combo-unittest')['quality-3'],[`${addQuantity}`]),
            lbTime: t(t('modal-noti-received-combo-unittest')['time-2'],[`${addDayExpire}`]),
        };
    }, [ oldCombo, newCombo, addDayExpire, addQuantity ]);

    return (
        <Modal
            className={styles.wrapperModal}
            open={showModalNoti}
            backdrop={false}
        >
            <div className={styles.head}>
                <div className={styles.icon}>
                    <IconCongratulations />
                </div>
                <div className={styles.title}>
                    {(t('modal-noti-received-combo-unittest')['title'])}
                    <div className={styles.user}>{userDisplayName}</div>
                </div>
            </div>
            <div className={styles.content}>
                <div className={styles.reward}>
                    <div dangerouslySetInnerHTML={{
                        __html: title
                    }}></div>
                    <ul>
                        <li dangerouslySetInnerHTML={{
                            __html: lbQuantity
                        }}></li>
                        {!!addDayExpire && <li dangerouslySetInnerHTML={{
                            __html: lbTime
                        }}></li>}
                        <li>{t('modal-noti-received-combo-unittest')['desc-1']}</li>
                    </ul>
                    <div className={styles.tagLine}>{t('modal-noti-received-combo-unittest')['desc']}</div>
                </div>
                <Button className={styles.btnClaim} onClick={() => setShowModalNoti(false)}>
                    {t('common')['close']}
                </Button>
            </div>
        </Modal>
    );
};

export default ModalNotiReceivedComboUnitTest;