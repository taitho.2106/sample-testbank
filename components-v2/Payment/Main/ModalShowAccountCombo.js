import React from "react";

import CloseIcon from 'assets/icons/close.svg';

import Button from "../../Common/Controls/Button";
import ModalWrapper from "../../Common/ModalWrapper";
import styles from "./ModalShowAccountCombo.module.scss"
const ModalShowAccountCombo = ({dataModal}) => {
	return (
		<ModalWrapper isOpen={dataModal.open} className={styles.modalContainer}>
			<div className={styles.contentModal}>
				<div className={styles.titleModal}>
					<span>Xác nhận danh sách tài khoản</span>
					<div>
						<CloseIcon onClick={() => dataModal.onCancel?.()}/>
					</div>
				</div>
				<span>Hãy kiểm tra lại lần nữa thông tin các tài khoản trước khi thực hiện thanh toán nhé</span>
				<div className={styles.tableAccount}>
					<table>
						<thead>
							<tr>
								<th align="center">STT</th>
								<th align="left">Họ và tên</th>
								<th align="left">Số điện thoại</th>
								<th align="left">Email</th>
							</tr>
						</thead>
						<tbody>
						{dataModal.dataAccount.map((account, index) => (
							<tr key={index + 1}>
								<td align="center"><span>{index + 1}</span></td>
								<td align="left" title={account.full_name}><span>{account.full_name}</span></td>
								<td align="left"><span>{account.phone}</span></td>
								<td align="left" title={account.email}><span>{account.email}</span></td>
							</tr>
							))}
						</tbody>
					</table>
				</div>
				<div className={styles.actionModal}>
					<Button className={styles.cancel} onClick={() => dataModal.onCancel?.()}>Hủy</Button>
					<Button className={styles.submit} onClick={() => dataModal.onSubmit?.()}>Xác nhận</Button>
				</div>
			</div>
		</ModalWrapper>
	);
};

export default ModalShowAccountCombo;