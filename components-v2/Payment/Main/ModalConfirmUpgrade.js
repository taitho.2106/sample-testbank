import BasicModal from '../../Common/Controls/BasicModal';
import Button from '../../Common/Controls/Button';

import CloseIcon from 'assets/icons/close.svg';

import styles from './ModalConfirmUpgrade.module.scss';
import { useSession } from "next-auth/client";
import { userInRight } from "../../../utils";
import { USER_ROLES } from "../../../interfaces/constants";
import useTranslation from "../../../hooks/useTranslation";

const ModalConfirmUpgrade = ({ isShowModal, onCancel, onConfirm }) => {
    const { t } = useTranslation()
    const [session] = useSession();
    const isStudent = userInRight([USER_ROLES.Student], session)
    return (
        <BasicModal isOpen={isShowModal}
            contentClassName={styles.contentModal}>
            <div className={styles.header}>
                <p>{isStudent ? t('modal-change-package')['title'] : t('modal-change-package')['title-upgrade']}</p>
                <Button type='none' onClick={onCancel}>
                    <CloseIcon />
                </Button>
            </div>
            <div className={styles.body}>
                <p dangerouslySetInnerHTML={{__html: `${isStudent ?
                        t('modal-change-package')['description'] :
                        t('modal-change-package')['description-upgrade']}`}}></p>
            </div>
            <div className={styles.footer}>
                <Button type='outline' onClick={onCancel}>{t('common')['cancel']}</Button>
                <Button onClick={onConfirm}>{t('common')['submit-btn']}</Button>
            </div>
        </BasicModal>
    );
};

export default ModalConfirmUpgrade;