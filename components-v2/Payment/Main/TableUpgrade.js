import classNames from 'classnames'
import { useFormikContext } from 'formik'
import { Fragment, useCallback, useMemo } from 'react'

import { tableDatas, tableDatasStudent } from '@/componentsV2/Home/data/detailFeatureAndPrice'
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'
import { PACKAGES } from '@/interfaces/constants'
import { getPackageLevel } from "@/utils/user"
import { formatNumber, renderNameByCode, userInRight } from "utils";

import BasicModal from '@/componentsV2/Common/Controls/BasicModal'

import BasicIcon from "assets/icons/book.svg"
import CloseIcon from 'assets/icons/close.svg'
import InternationalIcon from "assets/icons/ic-package-international-color.svg"
import AdvancedIcon from "assets/icons/magic-star.svg"
import TickFailedIcon from 'assets/icons/tick-failed.svg'
import TickIcon from 'assets/icons/tick.svg'
import Icon99 from 'assets/icons/student_99.svg'
import Icon199 from 'assets/icons/student_199.svg'
import Icon299 from 'assets/icons/student_299.svg'
import styles from './TableUpgrade.module.scss'
import { PACKAGES_FOR_STUDENT, USER_ROLES } from "../../../interfaces/constants";
import { useSession } from "next-auth/client";
import useTranslation from "../../../hooks/useTranslation";

const TableUpgrade = ({ isOpen, setIsOpen }) => {
    const { t } = useTranslation();
    const { values } = useFormikContext()
    const { dataPlan, listPlans, listPlansStudent } = useCurrentUserPackage();
    const [session] = useSession();
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const exampleData = [
        {
            code: PACKAGES.ADVANCE.CODE,
            icon: <AdvancedIcon />,
        },
        {
            code: PACKAGES.INTERNATIONAL.CODE,
            icon: <InternationalIcon />,
        },
    ]
    
    const addIconData = [
        {
            code: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE,
            icon: <Icon99 />
        },
        {
            code: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE,
            icon: <Icon299 />
        },
        {
            code: PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE,
            icon: <Icon199 />
        },
    ]

    const dataTeacher = listPlans?.map(item => ({ ...item, ...exampleData.find(x => x.code === item.code) }))
    const dataStudent = listPlansStudent?.filter(item => item.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE).map(item => ({ ...item, ...addIconData.find(x => x.code === item.code) }))
    const data = isStudent ? dataStudent : dataTeacher
    const servicePackages = useMemo(() => {
        return data.sort((a, b) => getPackageLevel(a.code) - getPackageLevel(b.code));
    }, [data])

    const selectedPlan = useMemo(() => {
        return servicePackages.find(item => item.id === values.plan) || {}
    }, [values.plan]);
    const renderValueWithDefault = (value, unit = '', defaultVal = '---') => {
        if (value) {
            return `${formatNumber(value)}đ${unit ? ` / ${unit}` : ''}`
        }

        return defaultVal
    }

    const ColumnContent = useCallback(({ children, className, dot, isSelected, style }) => {
        const content = dot ? <span>{children}</span> : children;
        return (
            <div style={style} className={classNames(styles.columnContent, className, {
                [styles.dot]: dot,
                [styles.selected]: isSelected
            })}>
                {content}
            </div>
        )
    }, [])

    const RowContent = useCallback(({ children, className, style }) => {
        return (
            <div style={style} className={classNames(styles.rowContent, className)}>
                {children}
            </div>
        )
    }, [])
    
    const dataFeatures = isStudent ? tableDatasStudent : tableDatas
    const dataTable = dataFeatures.filter(item => !item.isNotPayment);

    return (
        <BasicModal isOpen={isOpen}
            contentClassName={styles.modalContent}
            onRequestClose={() => setIsOpen(false)}>
            <div className={styles.title}>
                <h1>{t('modal-pack-info')['title']}</h1>
                <span onClick={() => setIsOpen(false)}>
                    <CloseIcon />
                </span>
            </div>
            <div style={{ overflow: 'auto', height: 'calc(100% - 51px)', padding: '20px' }}>
                <div className={styles.tableUpgradeWrapper}>
                    <div className={styles.section}>
                        <RowContent className={styles.rowContent}>
                            <ColumnContent />
                            {servicePackages.map((item, index) => {
                                const highlight = (selectedPlan.code || dataPlan?.code) === item.code;
                                return (
                                    <ColumnContent
                                        style={
                                            highlight
                                                ? { borderRadius: '12px 12px 0 0' }
                                                : { borderRadius: index === 0 ? '12px 0 0 0' : index === 2 ? '0 12px 0 0' : '' }
                                        }
                                        isSelected={highlight}
                                        key={index}
                                    >
                                        {highlight && (
                                            <h4>{values.isRenew ? t('modal-pack-info')['pack-usage'] : t('modal-pack-info')['pack-selected']}</h4>
                                        )}
                                        <div>
                                            {item.icon || <BasicIcon />}
                                            <p className={styles.packageName}>{renderNameByCode(item, t)}</p>
                                            <p className={styles.price}>{renderValueWithDefault(item.price || '0')}<span>{isStudent ? "" : `/${t('month')}`}</span></p>
                                            <p className={styles.apply}>({t('application-time')})</p>
                                        </div>
                                    </ColumnContent>
                                );
                            })}
                        </RowContent>
                        {dataTable.map((item, index) => (
                            <Fragment key={index}>
                                <div className={styles.rowHeader} style={ index === 0 ? { borderRadius: '12px 0 0 0' } : {}}>
                                    <div className={styles.columnHeader}>{t(item.name)}</div>
                                    {servicePackages.map((item, colIndex) => (
                                        <div
                                            className={classNames(styles.columnHeader, {
                                                [styles.selected]: (selectedPlan.code || dataPlan?.code) === item.code
                                            })}
                                            key={colIndex}
                                        />
                                    ))}
                                </div>
                                {item.descriptions.map((des, desIndex) => {
                                    const isLastRow = index === dataTable.length - 1 && desIndex === item.descriptions.length - 1
                                    return (
                                        <RowContent className={styles.rowContent} key={desIndex} style={isLastRow ? {borderRadius: "0 0 12px 12px"} : {}}>
                                            {des.filter(item => item.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE).map((col, colIndex) => {
                                                const highlight = (selectedPlan.code || dataPlan?.code) === col.code;
                                                return (
                                                    <ColumnContent
                                                        dot={!col.package}
                                                        style={highlight && isLastRow ?
                                                            {
                                                                   borderBottom: "1px solid #7C68EE",
                                                                   borderRadius: "0 0 12px 12px",
                                                                   padding: '0px 5px'
                                                            }
                                                            : { padding: '0px 5px' }}
                                                       isSelected={(selectedPlan.code || dataPlan?.code) === col.code}
                                                       key={colIndex}
                                                    >
                                                        {typeof col.value === "string" ?
                                                            <span
                                                                dangerouslySetInnerHTML={{ __html: t(col.value, col?.optional) }}></span> :
                                                            col.value ? <TickIcon /> : <TickFailedIcon />}
                                                    </ColumnContent>
                                                );
                                            })}
                                        </RowContent>
                                    );
                                })}
                            </Fragment>
                        ))}
                    </div>
                </div>
            </div>
        </BasicModal>
    )
}

export default TableUpgrade