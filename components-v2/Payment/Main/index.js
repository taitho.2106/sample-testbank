import { useField } from 'formik';
import { useRouter } from 'next/router';
import { useContext, useEffect, useMemo, useRef, useState } from "react";
import * as yup from 'yup';

import BasicForm from "@/componentsV2/Common/Controls/BasicForm";
import Button from '@/componentsV2/Common/Controls/Button';
import Switch from '@/componentsV2/Common/Controls/Switch';
import useTranslation from '@/hooks/useTranslation';

import ChoosePackage from './ChoosePackage';
import ModalConfirmUpgrade from './ModalConfirmUpgrade';
import PaymentMethod from './PaymentMethod';

import useCurrentUserPackage from '@/hooks/useCurrentUserPackage';
import { NUMBER_MONTH_PAYMENT, PACKAGES, paths, PAYMENT_TYPE } from '@/interfaces/constants';
import { AppContext } from '@/interfaces/contexts';
import { paths as apiPaths } from 'api/paths';
import { callApi } from 'api/utils';
import {
    emailRegExp,
    PACKAGES_FOR_STUDENT, phoneRegExp1,
    USER_ASSET_TYPE,
    USER_ROLES,
    whiteSpaceRegex
} from "../../../interfaces/constants";
import { getUpgradeAblePackageLevels } from "../../../utils/user";
import ExamDetail from "./ExamDetail";
import ShowModalExpired from "./ShowModalExpired";

import dayjs from 'dayjs';
import isBetween from 'dayjs/plugin/isBetween';
import { useSession } from "next-auth/client";
import { Loader } from "rsuite";
import { useDateNow } from "../../../lib/swr-hook";
import { userInRight } from "../../../utils";
import BuyCombo from "./BuyCombo";
import styles from './index.module.scss';
import ModalFlashSaleExpired from "./ModalFlashSaleExpired";
import ModalShowAccountCombo from "./ModalShowAccountCombo";
dayjs.extend(isBetween)

const SwitchWrapper = ({ onChange, disabled }) => {
    const [field, _, helpers] = useField('isRenew');
    const {t} = useTranslation()
    const handleChange = e => {
        const { checked } = e.target;
        helpers.setValue(checked);
        onChange?.(checked);
    }

    return (
        <Switch
            onChange={handleChange}
            labelRight={t("extend")}
            labelLeft={t("upgrade")}
            width="100%"
            checked={field.value}
            height={50}
            disabled={disabled}
        />
    );
}

const Payment = () => {
    const { query, push, back, replace } = useRouter();
    const [session] = useSession();
    const { t, locale } = useTranslation()
    const { type, itemId } = query;
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
    const [isLoading, setIsLoading] = useState(true)
    const { fullScreenLoading } = useContext(AppContext)
    const [detailData, setDetailData] = useState({});
    const [voucherError, setVoucherError] = useState("");
    const returnPath = locale === 'en' ? `/en${paths.paymentResults}` : paths.paymentResults
    const { data } = useDateNow();
    const setVoucherErrorFunc = (value) => {
        setVoucherError(value)
    }
    const [isBuyExam, setIsBuyExam] = useState(false)
    // const isBuyExam = detailData.item_type && detailData.item_type === USER_ASSET_TYPE.UNIT_TEST;

    const [isShowConfirm, setIsShowConfirm] = useState(false);
    const [isEndFlashSale, setIsEndFlashSale] = useState(false)
    const {
        isExpired,
        dataPlan,
        getPackageByCode,
        listPackage,
    } = useCurrentUserPackage();
    const formRef = useRef();
    const [dataModal, setDataModal] = useState({
        open: false,
        dataAccount: [],
        onCancel: () => {
            //
        },
        onSubmit: () => {
            //
        },
    })
    
    const onCloseModal = () => {
        setDataModal(prevState => ({...prevState, open: false}))
    }
    
    const onSubmitCombo = async (requestData) => {
        try {
            fullScreenLoading.setState(true);
            const data = await callApi(apiPaths.api_order_create, "POST", "Token", requestData);
            if (!data) {
                return fullScreenLoading.setState(false);
            }
            
            if (data.error_code) {
                fullScreenLoading.setState(false);
                setVoucherError(t('voucher-mess')[`${data.error_code}`]);
                return;
            }
            
            push(data.vnpUrl);
            
        } catch (e) {
            fullScreenLoading.setState(false);
        }
    }

    const handleSubmit = async values => {
        if (isEndFlashSale) return
        if (type === PAYMENT_TYPE.BUYCOMBO) {
            const dataAccount = values.accounts.map(item => ({
                full_name: !!item.full_name.trim().length ? item.full_name.trim() : `${t('role')['teacher']} ${item.phone}`,
                phone: item.phone.trim(),
                email: item.email.trim()
            }))
            const requestDataCombo = {
                itemId: Number(process.env.NEXT_PUBLIC_ID_COMBO_SERVICE_PACKAGE),
                quantity: 1,
                bankCode: values.paymentMethodCode,
                returnPath: paths.paymentResults,
                accounts: dataAccount
            }
            setDataModal({
                open: true,
                dataAccount,
                onSubmit: () => onSubmitCombo(requestDataCombo),
                onCancel: onCloseModal
            })
            return
        }
        const currentPackageNotFree = dataPlan && getPackageByCode(dataPlan.code)
        const conditionSubmit = isBuyExam || isExpired || values.plan === currentPackageNotFree?.id || (dataPlan.code === PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE) || isShowConfirm
        if (conditionSubmit) {
            try {
                setIsShowConfirm(false);
                fullScreenLoading.setState(true);
                const requestData = {
                    itemId: parseInt(values.plan || itemId),
                    quantity: isStudent ? 1 : parseInt(values.period || '0'),
                    bankCode: values.paymentMethodCode,
                    returnPath,
                    voucher_code: values.voucher_code
                }
                const data = await callApi(apiPaths.api_order_create, 'POST', 'Token', requestData);
                if (!data) {
                    return fullScreenLoading.setState(false);
                }

                if (data.error_code) {
                    fullScreenLoading.setState(false);
                    setVoucherError(t('voucher-mess')[`${data.error_code}`]);
                    return
                }

                push(data.vnpUrl);
            } catch (error) {
                fullScreenLoading.setState(false);
            }
        } else {
            setIsShowConfirm(true);
        }
    }

    const handleBuyTypeChange = isRenew => {
        let plan = '', period = '';
        if (isRenew) {
            plan = dataPlan?.id;
            period = NUMBER_MONTH_PAYMENT;
        } else {
            const initData = getInitData(PAYMENT_TYPE.UPGRADE, itemId, detailData, listPackage);
            plan = initData.plan;
            period = initData.period;
        }

        formRef.current.setErrors({});
        formRef.current.setTouched({
            isRenew: false,
            plan: false,
            paymentMethodCode: false,
            period: false,
        }, true);

        formRef.current.setFieldValue('plan', plan);
        formRef.current.setFieldValue('period', period);
    }

    const getPackageById = (id) => {
        return listPackage?.find(item => item.id === id) || {};
    }

    const getUpgradeAblePackages = () => {
        const upgradeAblePackages = getUpgradeAblePackageLevels(dataPlan?.code)
        return  isStudent ? listPackage : listPackage?.filter(item => upgradeAblePackages.includes(item?.code))
    };

    const getInitData = (type, itemId, detailData, listPackage) => {
        let checkType = type;
        if (!checkType) {
            checkType = isExpired ? PAYMENT_TYPE.UPGRADE : PAYMENT_TYPE.RENEW;
        }
        
        if (checkType === PAYMENT_TYPE.BUYCOMBO) {
            const initAccount = [
                {
                    full_name: '',
                    phone: '',
                    email: '',
                },
                {
                    full_name: '',
                    phone: '',
                    email: '',
                },
                {
                    full_name: '',
                    phone: '',
                    email: '',
                },
                {
                    full_name: '',
                    phone: '',
                    email: '',
                },
            ]
            return {
                period: 1,
                paymentMethodCode: "",
                accounts: initAccount,
                price: 999999,
            }
        }

        if (isBuyExam) {
            return {
                voucher_code: "",
                isRenew: false,
                plan: '',
                paymentMethodCode: '',
                period: 1,
            }
        }

        if (checkType === PAYMENT_TYPE.RENEW) {
            return {
                isRenew: !isStudent,
                voucher_code: "",
                plan: detailData?.id,
                paymentMethodCode: '',
                period: NUMBER_MONTH_PAYMENT,
            }
        } else {
            let upgradePackage = getUpgradeAblePackages();
            if (isExpired || !dataPlan) {
                upgradePackage = listPackage;
            }
            let packageSelect = getPackageById(+itemId);
            if (!packageSelect.id && upgradePackage?.length === 1) {
                packageSelect = upgradePackage[0];
            }

            if (packageSelect.id && upgradePackage.find(el => el.id === packageSelect.id)) {
                return {
                    voucher_code: "",
                    isRenew: false,
                    plan: packageSelect.id,
                    paymentMethodCode: '',
                    period: NUMBER_MONTH_PAYMENT,
                }
            }
        }

        return {
            voucher_code: "",
            isRenew: false,
            plan: '',
            paymentMethodCode: '',
            period: '',
        };
    }

    const onCancel = () => {
        isStudent ? back() : push(paths.home);
    }
    
    useEffect(() => {
        if (data && type === PAYMENT_TYPE.BUYCOMBO) {
            const today = dayjs(data)
            const startTimeCombo = process.env.NEXT_PUBLIC_TIME_START_COMBO;
            const endTimeCombo = process.env.NEXT_PUBLIC_TIME_END_COMBO;
            const startTimeFlashSale = process.env.NEXT_PUBLIC_TIME_START_FLASH_SALE;
            const endTimeFlashSale = process.env.NEXT_PUBLIC_TIME_END_FLASH_SALE;
            const isActiveCombo = today.isBetween(startTimeCombo, endTimeCombo, 'day', '[]')
            const isActiveFlashSale = today.isBetween(startTimeFlashSale, endTimeFlashSale, 'day', '[]')
            const listTimeFlashSale = {
                '2023-09-01':{
                    start: '2023-09-01 08:00',
                    end: '2023-09-01 12:30'
                },
                '2023-09-02':{
                    start: '2023-09-02 08:00',
                    end: '2023-09-02 12:30'
                },
                '2023-09-03':{
                    start: '2023-09-03 08:00',
                    end: '2023-09-03 12:30'
                },
                '2023-09-04':{
                    start: '2023-09-04 08:00',
                    end: '2023-09-04 12:30'
                },
                '2023-09-05':{
                    start: '2023-09-05 08:00',
                    end: '2023-09-05 12:30'
                },
                '2023-09-06':{
                    start: '2023-09-06 06:00',
                    end: '2023-09-06 23:59'
                },
            }
            setIsEndFlashSale(false);
            let timeout;
            if (!isTeacher || (!isActiveCombo && !isActiveFlashSale)) {
                replace(paths.payment);
            }
            if (isActiveFlashSale && !isActiveCombo) {
                const time = listTimeFlashSale[today?.format("YYYY-MM-DD")];
                const startTime = dayjs(time?.start);
                const endTime = dayjs(time?.end);
                if (!today.isBetween(startTime, endTime, "minute", "[]")) {
                    setIsEndFlashSale(true);
                } else {
                    setIsEndFlashSale(false);
                }
            }
            if (today.isBetween("2023-09-07 00:00", "2023-09-07 00:05", "minute", "[]")) {
                const delay = dayjs("2023-09-07 00:05").diff(dayjs("2023-09-07 00:00"), 'millisecond')
                timeout = setTimeout(() => {
                    setIsEndFlashSale(true);
                }, delay)
            }
            
            return () => {
                if (timeout) {
                    clearTimeout(timeout)
                }
            }
        }
    }, [data])
    
    useEffect(() => {
        if (data && type === PAYMENT_TYPE.BUYCOMBO) {
            const today = dayjs(data)
            let timeout;
            let delay = 0;
            if (dayjs(data).isBetween(process.env.NEXT_PUBLIC_TIME_END_COMBO, process.env.NEXT_PUBLIC_TIME_START_FLASH_SALE, 'millisecond','[]')){
                delay = dayjs('2023-09-01 00:00').diff(today, 'millisecond')
            } else {
                delay = dayjs('2023-09-07 00:00').diff(today, 'millisecond')
            }
            timeout = setTimeout(() => {
                setIsEndFlashSale(true);
            }, delay)
            return () => clearTimeout(timeout)
        }
    } , [data])

    useEffect(() => {
        if(dataPlan && listPackage?.length && (itemId || dataPlan?.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE)){
            const currentPackage = getPackageByCode(dataPlan?.code)
            callApi(`${apiPaths.api_sale_item}/${itemId || currentPackage.id}`, "GET", "Token")
                .then(res => {
                    if(res.result){
                        setDetailData({ ...res.data.item, ...res.data } || {});
                        setIsBuyExam(res.data?.item_type === USER_ASSET_TYPE.UNIT_TEST)
                    }
                    setIsLoading(false)
                })
                .catch(err => {
                    console.log(err)
                    setIsLoading(false)
                })
        } else {
            setIsLoading(false)
        }
    }, [itemId, dataPlan?.code, listPackage?.length]);

    useEffect(() => {
        if (detailData.id) {
            const initData = getInitData(type, itemId, detailData, listPackage)
            formRef.current?.setValues(initData, true)
        }
    }, [type, itemId, detailData.id]);

    //disable bf-cache when back from other website
    useEffect(() => {
        function unHandle() { };
        window.addEventListener('unload', unHandle);
        window.addEventListener('beforeunload', unHandle);

        window.addEventListener('pageshow', (event) => {
            if (event.persisted) {
                location.reload();
            }
        });

        return () => {
            window.addEventListener('unload', unHandle);
            window.addEventListener('beforeunload', unHandle);
        }
    }, [])
    
    const validationSchema = useMemo(() => {
        if (type === PAYMENT_TYPE.BUYCOMBO) {
            const accountSchema = yup.object().shape({
                full_name: yup.string()
                    .matches(whiteSpaceRegex, t(`${t("formikForm")["white-space-regex"]}`, [`${t("common")["full-name"]}`]))
                    .max(45, t(t("full-name-max"), [45])),
                phone: yup.string()
                    .required(t(`${t("formikForm")["required"]}`, [t("phone-number-alt")]))
                    .length(10, t(`${t("formikForm")["phone-length"]}`, [10]))
                    .matches(phoneRegExp1, t(`${t("formikForm")["incorrect"]}`, [t("phone-number-alt")])),
                email: yup.string()
                    .required(t(`${t("formikForm")["required"]}`, ['Email']))
                    .max(64, t(`${t("formikForm")["max-validate"]}`, ['Email', 64]))
                    .matches(emailRegExp, t(`${t("formikForm")["wrong-format"]}`, ['Email'])),
            });
            return yup.object().shape({
                paymentMethodCode: yup.string().required(t("select-a-payment-method")),
                accounts: yup.array().of(accountSchema)
            })
        }
        return yup.object().shape({
            plan: !isBuyExam ? yup.number().required(t("select-test-package")) : undefined,
            paymentMethodCode: yup.string().required(t("select-a-payment-method"))
        })
    }, [type, isBuyExam])

    return (
        <div className={styles.paymentPage}>
            {isEndFlashSale && <ModalFlashSaleExpired />}
            {!isStudent && <ShowModalExpired />}
            {dataModal?.open && <ModalShowAccountCombo dataModal={dataModal}/>}
            {dataPlan && dataPlan.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE &&
                <ModalConfirmUpgrade
                    isShowModal={isShowConfirm}
                    onCancel={() => setIsShowConfirm(false)}
                    onConfirm={() => formRef.current.submitForm()}
                />}
            <div className={styles.container}>
                {isLoading ? (
                        <Loader vertical backdrop content="loading..."/>
                    ) :
                    (<BasicForm
                        initialValues={getInitData(type, itemId, detailData, listPackage)}
                        id="payment-package"
                        onSubmit={handleSubmit}
                        validationSchema={validationSchema}
                        formikRef={formRef}
                    >
                        <div className={styles.head}>
                            <div className={styles.headActions}>
                                {type === PAYMENT_TYPE.BUYCOMBO ?
                                    t(`${t('package')["title-teacher-combo"]}`) :
                                    isBuyExam ? t(`${t('package')["title-buy-exam"]}`) :
                                        isStudent ? t(`${t('package')["title-combo-unit-test"]}`) :
                                            isExpired ? t(`payment-service`) :
                                                t(`${t('package')["title-service-package"]}`)
                                }
                            </div>
                            
                            {!isExpired && dataPlan && !isBuyExam && !isStudent && type !== PAYMENT_TYPE.BUYCOMBO && (
                                <div className={styles.buyType}>
                                    <SwitchWrapper
                                        onChange={handleBuyTypeChange}
                                        disabled={dataPlan.code === PACKAGES.INTERNATIONAL.CODE}
                                    />
                                </div>
                            )}
                        </div>
                        <main>
                            <div className={styles.choosePackage}>
                                {(type === PAYMENT_TYPE.BUYCOMBO && !isStudent) ? (
                                    <BuyCombo />
                                ) : isBuyExam ?
                                    <ExamDetail voucherError={voucherError} setVoucherError={setVoucherErrorFunc}
                                                data={detailData} /> :
                                    <ChoosePackage voucherError={voucherError} setVoucherError={setVoucherErrorFunc}
                                                   listPlans={listPackage}
                                                   getUpgradeAblePackages={getUpgradeAblePackages} />}
                            </div>
                            <div className={styles.paymentMethods}>
                                <PaymentMethod />
                            </div>
                        </main>
                    </BasicForm>)}
                
                {!isLoading &&
                <div className={styles.actions}>
                    <Button className={styles.btnCancel} onClick={onCancel}>{t(`${t('common')['cancel']}`)}</Button>
                    <Button
                        type="success"
                        buttonType="submit"
                        form="payment-package"
                        disable={!!voucherError || isEndFlashSale}
                    >
                        {t(`${t('common')['pay']}`)}
                    </Button>
                </div>}
            </div>
        </div>
    );
};

export default Payment;

