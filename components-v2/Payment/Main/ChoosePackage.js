import dayjs from 'dayjs';
import { useFormikContext } from 'formik';
import { useCallback, useEffect, useMemo, useState } from "react";

import useCurrentUserPackage from '@/hooks/useCurrentUserPackage';
import { FORMAT_DATE, NUMBER_MONTH_PAYMENT } from "@/interfaces/constants";
import { formatNumber } from "@/utils/index";

import SelectField from '@/componentsV2/Common/Form/SelectField';
import ExternalLinkIcon from "assets/icons/external-link.svg";
import QuestionCircleIcon from "assets/icons/question-circle.svg";
import VoucherValid from "assets/icons/voucher-valid.svg";
import VoucherValidate from "assets/icons/voucher-validate.svg";
import TableUpgrade from './TableUpgrade';

import InputTextField from '@/componentsV2/Common/Form/InputTextField';
import useTranslation from '@/hooks/useTranslation';
import { callApi } from 'api/utils';
import classNames from 'classnames';
import { useSession } from "next-auth/client";
import { PACKAGES_FOR_STUDENT, USER_ROLES } from "../../../interfaces/constants";
import { renderNameByCode, userInRight } from "../../../utils";
import styles from './ChoosePackage.module.scss';

const ChoosePackage = ({listPlans, getUpgradeAblePackages, voucherError, setVoucherError}) => {
    const {t} = useTranslation()
    const [session] = useSession()
    const {
        dataPlan,
        isExpired,
    } = useCurrentUserPackage();
    const [isShow, setIsShow] = useState(false);
    const [isShowTable, setIsShowTable] = useState(false);
    const [discount, setDiscount] = useState(0);
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const { values, setFieldValue } = useFormikContext();
    const isRenew = values.isRenew;
    const period = isStudent ? 1 : values.period
    const selectedPlan = useMemo(() => {
        if (values.plan || !isRenew) {
            return listPlans?.find(item => item.id === +values.plan);
        }
        return dataPlan
    }, [values.plan]);
    const totalPrice = (selectedPlan?.price || 0) * (period || 0);
    const unit = t('year')

    const renderExpectedTime = useCallback((date) => {
        const startDate = dayjs().format(FORMAT_DATE.ISO_DATE);
        let endDate = '';
        if ((isRenew && dataPlan && !dataPlan.isExpired) || (isStudent && dataPlan && dataPlan.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE)) {
            if (selectedPlan.code === dataPlan.code) {
                endDate = dayjs(date).add(values.period, 'months').format(FORMAT_DATE.ISO_DATE)
            } else {
                endDate = dayjs().add(values.period, 'months').format(FORMAT_DATE.ISO_DATE)
            }
        } else {
            endDate = dayjs().add(values.period, 'months').format(FORMAT_DATE.ISO_DATE)
        }

        return `${startDate} - ${endDate}`
    }, [selectedPlan])

    const upgradeAblePackages = useMemo(() => {
        let upgradePackage = getUpgradeAblePackages();
        if (isRenew || isExpired || !dataPlan) {
            upgradePackage = listPlans;
        }

        return upgradePackage.map((pack) => ({
            label: renderNameByCode(pack, t),
            value: pack.id,
            code: pack.code
        }));
    }, [listPlans, dataPlan, isRenew])

    const periods = [...Array(1).keys()].map(x => {
        const month = (x + 1) * NUMBER_MONTH_PAYMENT;
        return {
            label: `${x + 1} ${t('year')}`,
            value: month
        }
    });

    const renderValueWithDefault = (value, unit = '', defaultVal = '---') => {
        if (value) {
            return `${formatNumber(value)}đ${unit ? ` / ${t(unit)}` : ''}`
        }

        return defaultVal
    }

    const onChangePackage = () => {
        setFieldValue('period', NUMBER_MONTH_PAYMENT);
        setVoucherError("");
    }

    const handleValidateVoucher = async () => {
        if (!values.plan) {
            setVoucherError(t("package")["voucher-error"])
            return
        }
        if (!voucherError) {
            const resultValidateVoucher = await callApi("/api/voucher/validate", "post", "token", {
                voucher_code: values.voucher_code,
                order_price: totalPrice
            });

            if (resultValidateVoucher.error_code) {
                setVoucherError(t("voucher-mess")[`${resultValidateVoucher.error_code}`]);
                return
            }

            setDiscount(resultValidateVoucher?.data?.discount_price);
        }
    }

    useEffect(() => {
        if (discount) {
            handleValidateVoucher();
        }
    }, [values.plan])
    const isCheckShowIcon = useMemo(() => {
        if (!dataPlan?.code) return false
        if (isStudent)
            return dataPlan?.code === selectedPlan?.code && dataPlan?.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
        return dataPlan?.code === selectedPlan?.code && (!dataPlan?.isExpired || isRenew)
    }, [dataPlan])
    return (
        <div className={styles.choosePackageWrapper}>
            <h5>{t('package')['package-card']}</h5>
            <div className={styles.content}>
                <div className={styles.choosePackage}>
                    <div className={styles.selectPlan}>
                        <SelectField
                            name="plan"
                            placeHolder={t("package")["package-card"]}
                            titleDropdown={t("package")["package-card"]}
                            options={upgradeAblePackages}
                            menuClassName={styles.selectMenu}
                            onChange={onChangePackage}
                            disable={isRenew}
                        />
                        {!!values.plan && (
                            <span
                                className={styles.seePackInfo}
                                onClick={() => setIsShowTable(true)}
                            >
                                <ExternalLinkIcon />{t("package")["info-service"]}
                            </span>
                        )}
                    </div>
                    <div className={styles.period}>
                        <SelectField
                            name="period"
                            placeHolder={t("common")["period"]}
                            titleDropdown={t("common")["period"]}
                            options={periods}
                            disable={true}
                        />

                        {!!values.period && (
                            <div className={styles.description} id='tooltip12'>
                                {t("package")["intend-time"]}:
                                {isCheckShowIcon && (
                                    <span style={{ position: 'relative' }}>
                                        <QuestionCircleIcon
                                            className="cursor-pointer"
                                            onMouseEnter={() => setIsShow(true)}
                                            onMouseLeave={() => setIsShow(false)}
                                        />
                                        {isShow &&
                                            <div className={styles.tooltip} dangerouslySetInnerHTML={{
                                                __html: t("payment")["time-line-hover"]
                                            }}>
                                            </div>
                                        }
                                    </span>
                                )}
                                <br />
                                <span>{!!values.period ? renderExpectedTime(dataPlan?.end_date) : 'dd/mm/yyyy - dd/mm/yyyy'}</span>
                            </div>
                        )}
                    </div>
                </div>
                <div className={styles.voucherValidate}>
                    <InputTextField
                        name='voucher_code'
                        className={styles.inputField}
                        placeholder={t("package")["discount-code"]}
                        onChange={() => {setVoucherError(false); setDiscount(0)}}
                        autoComplete='off'
                        error={!!voucherError}
                        disabled={discount}
                        clearButton={discount}
                        errorFeedback={voucherError}
                        iconRight={discount ? <VoucherValid className={styles.voucherValidateIcon} /> : <VoucherValidate onClick={() => {handleValidateVoucher()}} className={classNames({
                            [styles.voucherValidateIcon]: true,
                            [styles.disable]: !!voucherError
                        })} />}
                    />
                </div>
                <div className={styles.separate}>
                    <div className={styles.line} />
                </div>
                <div className={styles.price}>
                    <div className={styles.line}>
                        <p>{t("package")["price"]}</p>
                        <span>{isStudent ? renderValueWithDefault(selectedPlan?.price, t('year'), `--- / ${t('year')}`) : renderValueWithDefault(selectedPlan?.price, t('month'), `--- / ${t('month')}`)}</span>
                    </div>
                    {isStudent && selectedPlan && selectedPlan.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE && (
                        <div className={styles.line}>
                            <p>{t("package")["usage"]}</p>
                            <span>{selectedPlan.limit} {t("package")["turn-of-use"]}</span>
                        </div>
                    )}
                    {discount ? <div className={styles.line}>
                        <p>{t("package")["discount"]}</p>
                        <span className={styles.discount}><b>{renderValueWithDefault(discount)}</b></span>
                    </div> : <></>}
                    <div className={styles.line}>
                        <p>{t("package")["total-price"]}</p>
                        {!!totalPrice && values.plan
                            ? <span className={styles.totalPrice}><b>{renderValueWithDefault(totalPrice - discount)}</b> / {values.period / 12} {unit}</span>
                            : <span className={styles.default}>---</span>}
                    </div>
                </div>
            </div>
            <TableUpgrade
                renderExpectedTime={renderExpectedTime}
                isOpen={isShowTable}
                setIsOpen={setIsShowTable}
            />
        </div>
    )
}

export default ChoosePackage
