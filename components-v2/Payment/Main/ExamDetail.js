import { useEffect, useState } from "react";

import classNames from "classnames";
import { useFormikContext } from "formik";

import InputTextField from "@/componentsV2/Common/Form/InputTextField";
import { callApi } from "api/utils";
import VoucherValid from "assets/icons/voucher-valid.svg";
import VoucherValidate from "assets/icons/voucher-validate.svg";

import useTranslation from "../../../hooks/useTranslation";
import { formatNumber } from "../../../utils";
import styles from "./ExamDetail.module.scss";

const ExamDetail = ({data, voucherError, setVoucherError}) => {
    const { t } = useTranslation()
    const [discount, setDiscount] = useState(0);
    const { values, setFieldValue } = useFormikContext();

    const handleValidateVoucher = async () => {
        if (!voucherError) {
            const resultValidateVoucher = await callApi("/api/voucher/validate", "post", "token", {
                voucher_code: values.voucher_code,
                order_price: data.exchange_price
            });

            if (resultValidateVoucher.error_code) {
                setVoucherError(t('voucher-mess')[`${resultValidateVoucher.error_code}`]);
                return
            }

            setDiscount(resultValidateVoucher?.data?.discount_price);
        }
    }
    
    useEffect(() => {
        setFieldValue('name', data.item?.name)
    }, [])

    return (
        <div className={styles.examDetailWrapper}>
            <h5>{t('package')['buy-exam-card']}</h5>
            <div className={styles.content}>
                <div className={styles.nameUnitTest}>
                    <div className={styles.unitTestInfo}>
                        <span className={styles.title}>{t('package')['unit-test']}</span>
                        <span className={styles.name} title={data.item?.name}>{data.item?.name}</span>
                    </div>
                </div>
                <div className={styles.voucherValidate}>
                    <InputTextField
                        name='voucher_code'
                        className={styles.inputField}
                        placeholder={<>{t('package')['discount-code']}</>}
                        onChange={() => {setVoucherError(false); setDiscount(0)}}
                        autoComplete='off'
                        disabled={discount}
                        error={!!voucherError}
                        clearButton={discount}
                        errorFeedback={voucherError}
                        iconRight={discount ? <VoucherValid className={styles.voucherValidateIcon} /> : <VoucherValidate onClick={() => {handleValidateVoucher()}} className={classNames({
                            [styles.voucherValidateIcon]: true,
                            [styles.disable]: !!voucherError
                        })} />}
                    />
                </div>
                <div className={styles.line}>
                    <p>{t('package')['price']}</p>
                    <span>{formatNumber(data.exchange_price, 0)}đ</span>
                </div>
                {discount ? <div className={styles.line}>
                    <p>{t('package')['discount']}</p>
                    <span className={styles.discount}><b>{formatNumber(discount)}đ</b></span>
                </div> : <></>}
                <div className={styles.line}>
                    <p>{t('package')['total-price']}</p>
                    <span className={styles.totalPrice} >{formatNumber(data.exchange_price - discount, 0)}đ</span>
                </div>
            </div>
        </div>
    )
}

export default ExamDetail;