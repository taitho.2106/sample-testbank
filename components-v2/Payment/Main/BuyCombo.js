import { useEffect } from "react";

import { useField, useFormikContext } from "formik";

import TickIcon from '@/assets/icons/tick.svg';
import { formatNumber } from "@/utils/index";

import useTranslation from "@/hooks/useTranslation";
import InputTextField from "../../Common/Form/InputTextField";
import styles from "./BuyCombo.module.scss";

const BuyCombo = () => {
	const {t} = useTranslation()
	const [fields, meta, helpers] = useField('accounts');
	const formRef = useFormikContext()
	const listTouched = formRef.touched?.accounts || []
	const listErrors = formRef.errors?.accounts ||  [];
	const listIndexError = listErrors.map((item, index) => {
		if (item) return index;
		return -1;
	}).filter(i => i !== -1);
	// console.log({fields});
	const onChangeFullName = (value, index) => {
		fields.value[index]["full_name"] = value || ''
		helpers.setValue(fields.value)
	}
	const onChangePhone = (value, index) => {
		fields.value[index]["phone"] = value || ''
		helpers.setValue(fields.value)
	}
	const onChangeEmail = (value, index) => {
		fields.value[index]["email"] = value || ''
		helpers.setValue(fields.value)
	}
	const renderIconCheck = (index, error) => {
		return !!fields.value[index].phone.length &&
			!!fields.value[index].email.length &&
			(error?.phone && !listErrors[index]?.phone) &&
			(error?.email && !listErrors[index]?.email)
	}
	useEffect(() => {
		if (!!listTouched.length && !!listErrors.length) {
			const key = Object?.keys(listErrors[listIndexError[0]])[0] || ''
			const attr = `accounts[${listIndexError[0]}.${key}]` || ''
			const element = document.querySelector(`[name="${attr}"]`)
			if (element) {
				element.focus({ preventScroll: true })
				element.scrollIntoView({ behavior: 'smooth', block: 'center' })
			}
		}
	}, [formRef.submitCount]);
	return (
		<div className={styles.buyComboContainer}>
			<span className={styles.title}>{t('personal')['account-info']}</span>
			<div className={styles.comboBox}>
				{fields.value?.map((_, index) => {
					const error = !!listTouched.length ?
						listTouched[index] :
						{ full_name: false, phone: false, email: false };
					
					return (
						<div className={styles.formData} key={index}>
							<div className={styles.itemData}>
								<div className={styles.accountData}>
									<div className={styles.accountTitle}>
										<span>{t('account')} {index + 1}</span>
									</div>
									{renderIconCheck(index, error) && <TickIcon />}
								</div>
								{<div className={styles.dataFields}>
									<InputTextField
										name={`accounts[${index}.full_name]`}
										className={styles.inputField}
										placeholder={t('common')['full-name']}
										onChange={(e) => onChangeFullName(e, index)}
										autoComplete="fff"
										spellcheck={false}
										error={error?.full_name && !fields.value[index]?.full_name?.length && listErrors[index]?.full_name}
										errorFeedback={listErrors[index]?.full_name}
									/>
									<div className={styles.col2}>
										<InputTextField
											name={`accounts[${index}.phone]`}
											className={styles.inputField}
											placeholder={t('phone')}
											type="tel"
											onChange={(e) => onChangePhone(e, index)}
											autoComplete="fff"
											spellcheck={false}
											onlyNumber={true}
											error={error?.phone && !fields.value[index]?.phone?.length && listErrors[index]?.phone}
											errorFeedback={listErrors[index]?.phone}
										/>
										<InputTextField
											name={`accounts[${index}.email]`}
											className={styles.inputField}
											placeholder="Email"
											type="email"
											spellcheck={false}
											onChange={(e) => onChangeEmail(e, index)}
											autoComplete="fff"
											error={error?.email && !fields.value[index]?.email?.length && listErrors[index]?.email}
											errorFeedback={listErrors[index]?.email}
										/>
									</div>
								</div>}
							</div>
						</div>
					);
				})}
			</div>
			<div className={styles.lineDash}>
				<div/>
			</div>
			<div className={styles.paymentDetail}>
				<div className={styles.detailsInfo}>
					<span>{t("package")["price"]}</span>
					<span>{t("package")["total-price"]}</span>
				</div>
				<div className={styles.detailsInfo}>
					<span>{formatNumber(formRef?.values?.price)}đ / 4 {t('account')} / {t('year')}</span>
					<span className={styles.price}>{formatNumber(formRef?.values?.price)}đ /{t('year')}</span>
				</div>
			</div>
		</div>
	);
};

export default BuyCombo;