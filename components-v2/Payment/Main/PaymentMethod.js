import classNames from 'classnames';
import { useField } from 'formik';

import useTranslation from '@/hooks/useTranslation'
import IconCard from 'assets/icons/cards.svg';
import IconScan from 'assets/icons/scan.svg';
import IconVisa from 'assets/icons/visa.svg';

import { BANK_CODES } from '../../../interfaces/constants';
import styles from './PaymentMethod.module.scss';

const paymentMethods = [
    {
        name: "QR-code",
        description: "QR-method",
        code: BANK_CODES.VNPAYQR,
        icon: <IconScan />,
    },
    {
        name: "bank-card",
        description: "bank-method",
        code: BANK_CODES.VNBANK,
        icon: <IconCard />,
    },
    {
        name: "international-card",
        description: "international-method",
        code: BANK_CODES.INTCARD,
        icon: <IconVisa />,
    },
]

const PaymentMethod = () => {
    const [_, meta, helpers] = useField('paymentMethodCode');
    const isError = meta.error && meta.touched;
    const { t } = useTranslation()

    return (
        <div
            className={classNames(styles.paymentMethod, isError && styles.error)}
        >
            <h5>{t('banking-method')['title']}</h5>
            <div className={styles.list}>
                {paymentMethods.map(({ name, code, description, icon }) => (
                    <div
                        className={classNames(styles.item, meta.value === code && styles.active)}
                        key={code}
                        onClick={() => helpers.setValue(code)}
                    >
                        <div className={styles.select} />
                        <div className={styles.info}>
                            <h3 className={styles.name}>{t('banking-method')[name]}</h3>
                            <div className={styles.description}>{t('banking-method')[description]}</div>
                        </div>
                        <div className={styles.icon}>
                            {icon}
                        </div>
                    </div>
                ))}
            </div>
            {isError && (
                <div className={styles.feedback}>{meta.error}</div>
            )}
        </div>
    );
};

export default PaymentMethod;