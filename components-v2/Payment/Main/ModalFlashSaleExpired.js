import React, { useEffect, useMemo, useState } from "react";

import ModalWrapper from "../../Common/ModalWrapper";
import dayjs from "dayjs";
import { useDateNow } from "../../../lib/swr-hook";
import { useRouter } from "next/router";
import { paths } from "../../../interfaces/constants";
import styles from "./ModalFlashSaleExpired.module.scss"
const ModalFlashSaleExpired = () => {
	const { data } = useDateNow()
	// const [isEndFlashSale, setIsEndFlashSale] = useState(false)
	const { push } = useRouter()
	
	const listTimeFlashSale = {
		'2023-09-01':{
			start: '2023-09-01 08:00',
			end: '2023-09-01 12:30'
		},
		'2023-09-02':{
			start: '2023-09-02 08:00',
			end: '2023-09-02 12:30'
		},
		'2023-09-03':{
			start: '2023-09-03 08:00',
			end: '2023-09-03 12:30'
		},
		'2023-09-04':{
			start: '2023-09-04 08:00',
			end: '2023-09-04 12:30'
		},
		'2023-09-05':{
			start: '2023-09-05 08:00',
			end: '2023-09-05 12:30'
		},
		'2023-09-06':{
			start: '2023-09-06 06:00',
			end: '2023-09-06 23:59'
		},
	}
	
	const isEndFlashSale = useMemo(() => {
		const today = dayjs(data).format("YYYY-MM-DD")
		const timeLineFlashSale = listTimeFlashSale[today]
		
		if(!timeLineFlashSale) return true

		if (dayjs(data).isBefore(timeLineFlashSale?.start)) {
			return false
		}
		if (dayjs(data).isAfter(timeLineFlashSale?.start)) {
			return true
		}
		
	}, [data]);
	return (
		<ModalWrapper isOpen={true} className={styles.modalFlashSale}>
			<div className={styles.contentModal}>
				<span className={styles.title}>{isEndFlashSale ? "KẾT THÚC CHƯƠNG TRÌNH fLASH SALE" : "CHƯA BẮT ĐẦU CHƯƠNG TRÌNH fLASH SALE"}</span>
				<span className={styles.description}>{isEndFlashSale ? "Chương trình Flash Sale đã kết thúc, cảm ơn bạn đã tham gia": "Chương trình Flash Sale chưa diễn ra, vui lòng quay lại sau."}</span>
				<div className={styles.action}>
					<button onClick={() => push(paths.home)}>Đóng</button>
				</div>
			</div>
		</ModalWrapper>
	);
};

export default ModalFlashSaleExpired;