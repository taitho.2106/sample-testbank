import { useEffect, useState } from "react";

import { useRouter } from "next/router";

import useCurrentUserPackage from "../../../hooks/useCurrentUserPackage";
import ModalExpiredPackage from "../../Common/ModalExpiredPackage";

const ShowModalExpired = () => {
	const { isExpired, isLoading} = useCurrentUserPackage()
	const [isShowModal, setIsShowModal] = useState(false)
	const router = useRouter()
	useEffect(()=> {
		const count = sessionStorage.getItem("visitCount") || "0";
		if(!isLoading && router.isReady){
			const isShow = isExpired && router.pathname === '/payment' && parseInt(count) < 1
			if(isShow){
				setIsShowModal(true)
				sessionStorage.setItem("visitCount", `${parseInt(count) + 1}`);
			}
		}
	},[isLoading, isExpired])
	const onCloseModal = () => setIsShowModal(false)
	return (<>
		<ModalExpiredPackage isActive={isShowModal} onCloseModal={onCloseModal}/>
	</>)
}

export default ShowModalExpired;