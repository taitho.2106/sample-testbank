import React, { useEffect, useMemo, useRef, useState } from 'react'

import classNames from 'classnames'
import dayjs from 'dayjs'
import Link from 'next/dist/client/link'
import { useRouter } from 'next/router'
import { Loader } from 'rsuite'

import ChevronIcon from '@/assets/icons/arrow.svg'
import ToggleActionIcon from '@/assets/icons/toggle-action.svg'
import { DATE_DISPLAY_FORMAT } from '@/constants/index'
import useTranslation from '@/hooks/useTranslation'
import { STATUS_CODE_EXAM, CODE_ASSIGN_OPTION, paths as pathsUrl } from "@/interfaces/constants";
import { delayResult } from '@/utils/index'
import { toFixedNumber } from "@/utils/number";
import { paths } from 'api/paths';
import { callApiConfigError } from 'api/utils'

import CustomCarousel from '../Common/CustomCarousel'
import SkeletonLoading from '../Common/SkeletonLoading'
import styles from './AssignPage.module.scss'

const newCodeAssignOption = [
    CODE_ASSIGN_OPTION[1],
    CODE_ASSIGN_OPTION[0],
    CODE_ASSIGN_OPTION[2],
]

const AssignPage = () => {
    const {t} = useTranslation()
    const { push, query } = useRouter()
    const [classData, setClassData] = useState([])
    const [examFromClass, setExamFromClass] = useState([])

    const [loadingClass, setLoadingClass] = useState(true)
    const [loadingTable, setLoadingTable] = useState(true)
    const [toggle, setToggle] = useState<{ [index: number]: boolean }>({
        0: true,
        1: true,
        2: true,
    })

    const sortByField = (array: any[], field: string) => {
        return array.sort((a, b) => {
            const valueA: any = new Date(a[field])
            const valueB: any = new Date(b[field])
            return valueB.getTime() - valueA.getTime();
        })
    }

    const carouselRef = useRef(null)
    const showBtn = 3
    const [currentClass, setCurrentClass] = useState(0)
    const [currentClassId, setCurrentClassId] = useState(0)

    const scrollToClass = (index: number) => {
        if (carouselRef.current) {
            if (index === 0) {
                carouselRef.current.goToSlide(0);
            } else if (index === classData.length - 1) {
                // scroll to last
                carouselRef.current.goToSlide(index - 2);
            } else {
                // scroll to index
                carouselRef.current.goToSlide(index - 1);
            }
        }
    }

    const fetcher = async () => {
        setLoadingClass(true)
        const response: any = await delayResult(
            callApiConfigError(`${paths.api_student_classes}`), 400
        )
        setLoadingClass(false)
        if (response.result && response.data) {
            setClassData(response.data)
        }
    }

    useEffect(() => {
        fetcher().finally()
    }, [])

    useEffect(() => {
        if (classData.length > 0 && currentClassId == 0) {
            push({
                pathname: `${pathsUrl.assignedUnitTest}`,
                query: {
                    classId: classData[0].id
                }
            })
            setCurrentClassId(classData[0].id)
        } else if (classData.length == 0) {
            setLoadingTable(false)
        }
    }, [classData])

    useMemo(() => {
        if (query.classId) {
            setCurrentClassId(Number(query.classId))
        }
    }, [query])

    useEffect(() => {
        const fetcherBaseOnId = async () => {
            setLoadingTable(true);
            const response: any = await delayResult(
                callApiConfigError(
                    `${paths.api_student_classes}/${currentClassId}/unit-test`
                ), 400
            )
            setLoadingTable(false);
            if (response.result && response.data) {
                setExamFromClass(sortByField(response.data, "created_date"))
            }
        };

        if (classData.length > 0 && currentClassId != 0) {
            fetcherBaseOnId().finally()
            const indexQueryClass = classData.findIndex((classItem) => classItem.id === currentClassId)
            setCurrentClass(indexQueryClass)
            scrollToClass(indexQueryClass)
        }
    }, [classData, currentClassId])

    const toggleContent = (index: number) => {
        setToggle((prevState) => ({ ...prevState, [index]: !prevState[index] }));
    }

    const mapStatusToClass = {
        [STATUS_CODE_EXAM.UPCOMING]: styles.upcoming,
        [STATUS_CODE_EXAM.ONGOING]: styles.ongoing,
        [STATUS_CODE_EXAM.ENDED]: styles.ended,
    }

    const handleClassChange = async (value: number, index: number) => {
        setCurrentClassId(value)
        setCurrentClass(index)
        setToggle({
            0: true,
            1: true,
            2: true,
        })
        push({
            pathname: `${pathsUrl.assignedUnitTest}`,
            query: {
                classId: value
            }
        })
    }

    return (
        <div className={styles.wrapper}>
            <div className={styles.container}>
                {loadingClass && (
                    <div className={styles.teacherClassInfo}>
                        <div className={classNames(styles.boxListSkeleton, styles.boxCarousel)}>
                            {Array.from(Array(3), (val, sIndex: number) => {
                                return (
                                    <SkeletonLoading className={styles.btnSkeleton} key={sIndex} />
                                )
                            })}
                        </div>
                    </div>
                )}
                {!loadingClass && classData.length > 3 && (
                    <div className={styles.teacherClassInfo}>
                        <div className={styles.boxCarousel}>
                            <CustomCarousel
                                dragging={null}
                                className={styles.carousel}
                                slideIndex={0}
                                carouselRef={carouselRef}
                                adaptiveHeight={true}
                                autoPlay={false}
                                cellSpacing={20}
                                disableEdgeSwiping={true}
                                slidesToScroll={1}
                                slidesToShow={showBtn}
                                withoutControls={false}
                                isInfinity={true}
                                swiping={false}
                                wrapAround={false}
                                renderTopLeftControls={null}
                                renderBottomCenterControls={null}
                                renderCenterLeftControls={({ previousDisabled, previousSlide, currentSlide }: any) => {
                                    const isFirstSlide = currentSlide === 0
                                    return (
                                        <button onClick={previousSlide} disabled={previousDisabled || isFirstSlide}>
                                            <ChevronIcon />
                                        </button>
                                    )
                                }}
                                renderCenterRightControls={({ nextDisabled, nextSlide, currentSlide }: any) => {
                                    const isLastSlide = currentSlide >= classData.length - showBtn
                                    return (
                                        <button onClick={nextSlide} disabled={nextDisabled || isLastSlide}>
                                            <ChevronIcon />
                                        </button>
                                    )
                                }}
                            >
                                {classData.map((item: any, index: number) => {
                                    return (
                                        <button
                                            key={item.id}
                                            className={classNames(styles.carouselItem, {
                                                [styles.active]: currentClass === index,
                                            })}
                                            onClick={() => handleClassChange(item.id, index)}
                                        >
                                            <div className={styles.avatarContent}>
                                                <div className={styles.avatar}>
                                                    <img src='/images-v2/sample-avatar.png' alt='' />
                                                </div>
                                            </div>
                                            <div className={styles.textInfo}>
                                                <div className={styles.teacher}>
                                                    <span>{t("role")["teacher"]}{' '}
                                                        <span
                                                            title={item.teacher.full_name ? item.teacher.full_name : ''}
                                                            style={{ color: '#000000' }}
                                                        >
                                                            {item.teacher.full_name}
                                                        </span>
                                                    </span>
                                                </div>
                                                <div className={styles.classes}>
                                                    <span title={item.name} className={styles.classLine}>{item.name}</span>
                                                </div>
                                            </div>
                                        </button>
                                    )
                                })}
                            </CustomCarousel>
                        </div>
                    </div>
                )}
                {!loadingClass && classData.length < 4 && (
                    <div className={styles.teacherClassInfo}>
                        {classData.map((classItem: any, classIndex: number) => {
                            return (
                                <button
                                    className={classNames(styles.classBtn, {
                                        [styles.active]: currentClass === classIndex
                                    })}
                                    key={classItem.id}
                                    onClick={() => handleClassChange(classItem.id, classIndex)}
                                >
                                    <div className={styles.avatarContent}>
                                        <div className={styles.avatar}>
                                            <img src='/images-v2/sample-avatar.png' alt='' />
                                        </div>
                                    </div>
                                    <div className={styles.textInfo}>
                                        <div className={styles.teacher}>
                                            <span>{t("role")["teacher"]}{' '}
                                                <span
                                                    title={classItem.teacher.full_name ? classItem.teacher.full_name : ''}
                                                    style={{ color: '#000000' }}
                                                >
                                                    {classItem.teacher.full_name}
                                                </span>
                                            </span>
                                        </div>
                                        <div className={styles.classes}>
                                            <span title={classItem.name}>{classItem.name}</span>
                                        </div>
                                    </div>
                                </button>
                            )
                        })}
                    </div>
                )}
                {!loadingClass && classData.length == 0 && (
                    <div className={styles.emptyZone}>
                        <img
                            src='/images-v2/empty-exam-result.png'
                            alt="/"
                        />
                        <span>{t("unit-test-assigned-page")["no-unit-test"]}</span>
                    </div>
                )}
                <div className={styles.examInfo}>
                    {loadingTable && (
                        <div className={styles.loadingZone}>
                            <Loader content="loading..." vertical />
                        </div>
                    )}
                    {!loadingTable &&
                        classData.length > 0 &&
                        newCodeAssignOption.map((item, index) => {
                            const shouldToggle = !toggle[index];
                            const filteredExams = examFromClass.filter((exam) => exam.examStatus === item.value);
                            return (
                                <div className={styles.examCategory} key={index}>
                                    <div
                                        className={classNames(styles.header, {
                                            [styles.toggle]: shouldToggle
                                        })}
                                        onClick={() => toggleContent(index)}
                                    >
                                        <ToggleActionIcon />
                                        <span
                                            dangerouslySetInnerHTML={{
                                                __html: t('status-test',[t("exam"), t('status')[`${item.label}`]])
                                            }}
                                        >
                                            {/*{t('status-test',[t("exam"), t('status')[`${item.label}`]])}*/}
                                            {/*{t("exam")} <span style={{ textTransform: 'lowercase' }}>{t('status')[`${item.label}`]}</span>*/}
                                        </span>
                                    </div>
                                    {toggle[index] && (
                                        <div className={styles.body}>
                                            {(filteredExams.length === 0) && (
                                                <div className={styles.rowNoData}>
                                                    <span>{t("unit-test-assigned-page")["no-unit-test"]}</span>
                                                </div>
                                            )}
                                            {filteredExams.map((exam) => {
                                                const statusClassName = mapStatusToClass[exam.examStatus]
                                                const statusLabel = CODE_ASSIGN_OPTION.find(option => option.value === exam.examStatus)?.label;

                                                return (
                                                    <div className={styles.rowContent} key={exam.id}>
                                                        <div className={styles.nameDisplay} title={exam.unitTest.name}>
                                                            <Link href={`${pathsUrl.assignedUnitTest}/${exam.id}`}>
                                                                <a>
                                                                    <span>{exam.unitTest.name}</span>
                                                                </a>
                                                            </Link>
                                                        </div>
                                                        <div className={styles.dateTimeDisplay}>
                                                            <div className={classNames(styles.textDisplay, statusClassName)}>
                                                                <span>{t("unit-test-assigned-page")["from"]}</span>
                                                                <span>{t("unit-test-assigned-page")["to"]}</span>
                                                            </div>
                                                            <div className={classNames(styles.timeDisplay, statusClassName)}>
                                                                <span>{dayjs(exam.start_date).format("HH:mm")}</span>
                                                                <span>{dayjs(exam.end_date).format("HH:mm")}</span>
                                                            </div>
                                                            <div className={classNames(styles.dateDisplay, statusClassName)}>
                                                                <span>{dayjs(exam.start_date).format(DATE_DISPLAY_FORMAT)}</span>
                                                                <span>{dayjs(exam.end_date).format(DATE_DISPLAY_FORMAT)}</span>
                                                            </div>
                                                        </div>
                                                        <div className={classNames(styles.labelDisplay, statusClassName)}>
                                                            <span className={statusClassName}>{t('status')[statusLabel]}</span>
                                                        </div>
                                                        <div className={styles.moreInfoDisplay}>
                                                            {exam.unit_test_result ? (
                                                                <div className={styles.result}>
                                                                    <span>{t("unit-test-assigned-page")["submit-time"]}:{' '}
                                                                        <span style={{ color: '#202226' }}>
                                                                            {dayjs(exam.unit_test_result.created_date).format("DD/MM/YYYY HH:mm")}
                                                                        </span>
                                                                    </span>
                                                                    <span>{t("unit-test-assigned-page")["lasts-point"]}:{' '}
                                                                        <span style={{ color: '#202226' }}>{toFixedNumber(exam.unit_test_result.point)}/{exam.unit_test_result.max_point}</span>
                                                                    </span>
                                                                </div>
                                                            ) : (
                                                                <div className={styles.result}>
                                                                    <span style={{ color: '#202226' }}>N/A</span>
                                                                    <span>---</span>
                                                                </div>
                                                            )}
                                                        </div>
                                                    </div>
                                                );
                                            })}
                                        </div>
                                    )}
                                </div>
                            );
                        })
                    }
                </div>
            </div>
        </div>
    )
}

export default AssignPage