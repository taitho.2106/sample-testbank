import React, { useEffect, useState } from "react";

import classNames from "classnames";
import dayjs from "dayjs";
import { useSession } from "next-auth/client";
import { useRouter } from "next/router";
import { Button, Loader } from "rsuite";

import IconBack from "@/assets/icons/back-arrow.svg";
import { commonDeleteStatus, commonStatus } from "@/constants/index";
import { CODE_ASSIGN_OPTION, paths as pathsUrl, STATUS_CODE_EXAM } from "@/interfaces/constants";

import { paths } from "../../api/paths";
import { callApi } from "../../api/utils";
import { Wrapper } from "../../components/templates/wrapper";
import styles from "./AssignSlugContainer.module.scss";
import { toFixedNumber } from "@/utils/number";
import useTranslation from "@/hooks/useTranslation";

const AssignSlugContainer = () => {
    const RUN_TIME_OUT = 60 * 60 * 1000
    const [session] = useSession();
    const { t, locale, subPath } = useTranslation();
    const user: any = session?.user
    const { isReady, push, query, back } = useRouter();
    const [dataAssign, setDataAssign] = useState([]);
    const [dataUnitTest, setDataUnitTest] = useState(null);
    const [dataClassStudent, setDataClassStudent] = useState(null)
    const [classID, setClassId] = useState(0)
    const [isLoad, setIsLoad] = useState(true);
    const [timeout, setTimeout] = useState(0)
    const fetchApi = async () => {
        try {
            setIsLoad(true);
            const response: any = await callApi(`${paths.api_unit_test_assigned_result}/${query?.assignSlug}/results`);
            setIsLoad(false);
            if (response.code === 200) {
                const data = response?.data;
                setDataAssign(data?.content);
                setDataClassStudent(data?.classStudent)
                setDataUnitTest(data?.classUnitTest);
                setClassId(data?.classUnitTest?.class_id)
                setTimeout(data?.classUnitTest?.timeout)
            }
        } catch (e) {
            push(pathsUrl.assignedUnitTest).finally()
        }

    };
    useEffect(() => {
        if (isReady) {
            fetchApi().finally();
        }
    }, []);
    
    useEffect(() => {
        if ( dataUnitTest?.examStatus !== STATUS_CODE_EXAM.ENDED ) {
            let intervalId: NodeJS.Timeout
            if ( timeout <= RUN_TIME_OUT && timeout > 1000 ) {
                intervalId = setInterval(() => {
                    setTimeout(timeout - 1000)
                }, 1000)

                return () => clearInterval(intervalId)
            }
            if ( timeout <= 1000 && timeout > 0 ) {
                clearInterval(intervalId)
                fetchApi().finally()
            }
        }
    }, [ timeout, dataUnitTest ])
    const onBack = () => {
        push({
            pathname: `${pathsUrl.assignedUnitTest}`,
            query: {
                classId: classID,
            }
        })
    }

    const getClassnameStatus = (code: number) => {
        switch (code) {
            case STATUS_CODE_EXAM.ONGOING: return 'ongoing';
            case STATUS_CODE_EXAM.ENDED: return 'ended';
            default: return ''
        }
    }

    const getDisplayTimeOut = (timeout: number) => {
        const milliseconds = timeout;
        const seconds = Math.floor((milliseconds / 1000) % 60);
        const minutes = Math.floor((milliseconds / (1000 * 60)) % 60);
        const hours = Math.floor((milliseconds / (1000 * 60 * 60)) % 24);
        const days = Math.floor(milliseconds / (1000 * 60 * 60 * 24));

        return [
            days && `${days} ${t('time')['days']}`,
            hours && `${hours} ${t('time')['hours']}`,
            minutes && `${minutes} ${t('time')['minutes']}`,
            seconds && `${seconds} ${t('time')['seconds']}`,
        ].filter(Boolean).slice(0, 2).join(' ');
    }

    const getPropButtonByStatus = (unitTestInfo: any) => {
        const timer = getDisplayTimeOut(timeout)
        const urlPracticeTest = `${subPath}/practice-test/${dataClassStudent?.created_by}===${unitTestInfo?.unitTest?.id}?idAssign=${query?.assignSlug}`
        const isNotAllowPracticeTest = (!!dataAssign.length && unitTestInfo?.unit_type === 1) ||
            (dataClassStudent?.status !== commonStatus.ACTIVE) ||
            unitTestInfo?.unitTest?.deleted === commonDeleteStatus.DELETED ||
            (unitTestInfo?.class_group_id && unitTestInfo?.class_group_id !== dataClassStudent?.class_group_id);

        switch (unitTestInfo?.examStatus) {
            case STATUS_CODE_EXAM.ONGOING:
                return {
                    label: unitTestInfo?.unitTest?.deleted === commonDeleteStatus.DELETED ? t("learning-result-page")["deleted-exam"] : t("learning-result-page")["start-exam"],
                    disabled: isNotAllowPracticeTest,
                    className: "ongoing",
                    url: urlPracticeTest,

                };
            case STATUS_CODE_EXAM.ENDED:
                return {
                    label: t("learning-result-page")["start-exam"],
                    disabled: true,
                    className: "ended",
                    url: "",
                };
            default:
                return {
                    label: t(t("learning-result-page")["time-line"],[timer]),
                    disabled: true,
                    className: "upcoming",
                    url: urlPracticeTest,
                };
        }
    }

    const renderTextWithIndex = (index: number) => {
        if (locale === 'vi') {
            return `Lần thi ${index}`;
        }
        switch (index) {
            case 1:
                return `${index}st times`;
            case 2:
                return `${index}nd times`;
            case 3:
                return `${index}rd times`;
            default:
                return `${index}th times`;
        }
    }
    return (
        <Wrapper pageTitle={t("learring-result")['title-1']} hasStickyFooter={false}>
            <div className={styles.containerAssign}>
                <div className={styles.headerBack} onClick={back}><IconBack /><span>{t("common")["back"]}</span></div>
                <div className={styles.contentAssign}>
                    {isLoad ? (
                        <Loader backdrop vertical content="Loading..." />
                    ) : (
                        <>
                            <div className={styles.assignItem}>
                                <span className={styles.title}>{dataUnitTest?.unitTest?.name}</span>
                                <div className={styles.contentData}>
                                    <div className={classNames(styles.itemData, styles.taskName)}>
                                        <span>{t("common")["unit-type"]}:</span>
                                        <span>{t("learning-result-page")["submit-time"]}:</span>
                                        <span>{t("learning-result-page")["total-question"]}:</span>
                                        <span>{t("learning-result-page")["start-time"]}:</span>
                                        <span>{t("learning-result-page")["deadline"]}:</span>
                                        <span className={styles.tabStatus}>{t("learning-result-page")["status"]}:</span>
                                    </div>
                                    <div className={styles.itemData}>
                                        <span>{dataUnitTest?.unit_type === 0 ? t('unit-type')['practice'] : t('unit-type')['exam']}</span>
                                        <span>{dataUnitTest?.unitTest?.time} {t('time')['minutes']}</span>
                                        <span>{dataUnitTest?.unitTest?.total_question} {t("learning-result-page")["questions"]}</span>
                                        <span>{dayjs(dataUnitTest?.start_date).format("DD/MM/YYYY HH:mm")}</span>
                                        <span>{dayjs(dataUnitTest?.end_date).format("DD/MM/YYYY HH:mm")}</span>
                                        <span className={classNames(styles.tabStatus, styles[getClassnameStatus(dataUnitTest?.examStatus)])}>
                                            {t("status")[CODE_ASSIGN_OPTION[dataUnitTest?.examStatus - 1]?.label]}
                                        </span>
                                    </div>
                                </div>
                                <Button as={getPropButtonByStatus(dataUnitTest).disabled ? "button" : "a"}
                                        target="_blank"
                                        href={getPropButtonByStatus(dataUnitTest).url}
                                        appearance="primary"
                                        className={classNames(styles.buttonAction,
                                            styles[getPropButtonByStatus(dataUnitTest).className])}
                                        disabled={getPropButtonByStatus(dataUnitTest).disabled}
                                >
                                    {getPropButtonByStatus(dataUnitTest).label}
                                </Button>
                            </div>
                            {!!dataAssign.length &&
                                dataAssign.map((unitAssign: any, index: number) => {
                                    const href = `${subPath}/practice-test/${user?.id}===${unitAssign?.unit_test_id}===${unitAssign?.id}`
                                    return (
                                        <div className={styles.assignItem} key={unitAssign?.id}>
                                            <span className={classNames(styles.title, styles.ended)}>{renderTextWithIndex(dataAssign.length - index)}: {dataUnitTest?.unitTest?.name}</span>
                                            <div className={classNames(styles.contentData, styles.ended)}>
                                                <div className={classNames(styles.itemData, styles.taskName)}>
                                                    <span>{t("learning-result-page")["point"]}:</span>
                                                    <span>{t("learning-result-page")["deadline-time"]}:</span>
                                                </div>
                                                <div className={styles.itemData}>
                                                    <span>{toFixedNumber(unitAssign?.point)}/{unitAssign?.max_point}</span>
                                                    <span>{dayjs(unitAssign?.created_date).format("DD/MM/YYYY HH:mm")}</span>
                                                </div>
                                            </div>
                                            <Button as={"a"} target="_blank" href={href} appearance="primary" className={classNames(styles.buttonAction, styles.ended)}>{t("learning-result-page")["exam-review"]}</Button>
                                        </div>
                                    );
                                })
                            }
                        </>
                    )}
                </div>
            </div>
        </Wrapper>
    );
};

export default AssignSlugContainer;
