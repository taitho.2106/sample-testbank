import { emailRegExp, phoneRegExp1, whiteSpaceRegex } from "@/interfaces/constants";
import { convertDateStringToDate } from "@/utils/date";
import dayjs from "dayjs";
import * as yup from "yup";
import { DATE_DISPLAY_FORMAT, genderEnum } from "../constants";

const studentCode = (t: any) => yup.string()
    .nullable()
    .max(10, t(t('formikForm')['max-length'], [t('common')['student-code'],'10']))
    .matches(/^[a-zA-Z0-9\.\-_]+$/, {
        message: t(t('formikForm')['space-validate'], [t('common')['student-code']]),
        excludeEmptyString: true,
    });
const phoneValidate = (t:any) => yup.string()
    .required(t('phone-required'))
    .length(10, t('phone-validation'))
    .matches(phoneRegExp1, t('phone-incorrect'));
const addStudentSchema = (t:any) => yup.object().shape({
    phone: phoneValidate(t),
    fullName: yup.string()
        .required(t(t('formikForm')['required'], [t('common')['full-name']]))
        .matches(whiteSpaceRegex, t(t('formikForm')['white-space-regex'], [t('common')['full-name']]),)
        .max(45, t(t('formikForm')['max-length'], [t('common')['full-name'], '45'])),
    email: yup.string()
        .required(t(t('formikForm')['required'], ['Email']))
        .max(64, t(t('formikForm')['max-length'], ['Email', '64']))
        .matches(emailRegExp, t(t('formikForm')['wrong-format'], ['Email'])),
    studentCode: studentCode(t),
    gender: yup.number()
        .transform(value => (isNaN(value) ? 0 : value))
        .oneOf(Object.values(genderEnum), t('formikForm')['gender-format']),
    birthday: yup.string().test({
        name: 'date-string',
        skipAbsent: true,
        test: (value, ctx) => {
            if (value) {
                const date = convertDateStringToDate(value)

                if (!date.isValid()) {
                    return ctx.createError({ message: t('formikForm')['birthday-format'] });
                }

                if (date.isAfter(dayjs().endOf('D'))) {
                    return ctx.createError({ message: t(t('formikForm')['before-day'],[`${dayjs().format(DATE_DISPLAY_FORMAT)}`])});
                }
            }

            return true;
        }
    })
});

const validStudentCode = (t: any) => yup.object().shape({ studentCode: studentCode(t) });
const validPhone = (t:any) => yup.object().shape({ phone: phoneValidate(t) });

const paginationQuerySchema = yup.object().shape({
    page: yup
        .number()
        .transform(value => (isNaN(value) ? undefined : value))
        .min(0),
    size: yup
        .number()
        .transform(value => (isNaN(value) ? undefined : value))
        .min(1)
        .max(200),
});

export { addStudentSchema, paginationQuerySchema, validPhone, validStudentCode };

