import { generateUsername } from '@/utils/user';
import bcrypt from 'bcrypt';

import prisma from "lib/prisma";
import { commonDeleteStatus } from '../constants';
import { randomString } from "../utils";

const findAllUserExisted = ({ phone, email }: { phone: string[] | string, email: string[] | string }, select: any) => {
    const args: any = {
        where: {
            OR: [
                { user_name: { in: phone } },
                { phone: { in: phone } },
                { email: { in: email } },
            ],
            deleted: commonDeleteStatus.ACTIVE,
        }
    }

    if (select) {
        args.select = select;
    }

    return prisma.user.findMany(args);
}

const createListUser = async (users: any, roleId: number) => {
    const userSaveData = users.map((user: any) => {
        const { phone, email, fullName, birthday, gender } = user;
        const randomPassword = randomString(8);
        const salt = bcrypt.genSaltSync(10)
        const password = bcrypt.hashSync(randomPassword, salt);

        return {
            phone,
            user_name: generateUsername(phone),
            email: email ? email.toLowerCase() : email,
            full_name: fullName,
            user_role_id: roleId,
            password,
            salt,
            birthday,
            gender,
            created_date: new Date(),
            textPassword: randomPassword,
        }
    });

    const result: any = await prisma.$transaction(async tx => {
        const userCount = await tx.user.createMany({ data: userSaveData.map(({ textPassword, ...user }: any) => user) });

        if (userCount.count !== userSaveData.length) {
            throw new Error('Insert list user failed!');
        }

        const users = await tx.user.findMany({
            where: {
                phone: {
                    in: userSaveData.map((el: any) => el.phone)
                },
                deleted: commonDeleteStatus.ACTIVE,
            }
        })

        return users;
    });

    return result.map((userSaved: any, index: number) => {
        const textPassword = userSaveData.find((user: any) => user.phone === userSaved.phone)?.textPassword;
        return { ...userSaved, textPassword };
    })
}

export { findAllUserExisted, createListUser }