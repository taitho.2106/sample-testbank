import prisma from "lib/prisma";
import { commonDeleteStatus, commonStatus } from "../constants";

const getActiveClass = (classId: number, ownerId: string) => {
    return prisma.classes.findFirst({
        where: {
            id: classId,
            status: commonStatus.ACTIVE,
            deleted: commonDeleteStatus.ACTIVE,
            owner_id: ownerId,
        }
    });
}

const countClassActiveStudent = (classId: number) => {
    return prisma.classStudent.count({
        where: {
            class_id: classId,
            status: commonStatus.ACTIVE,
        }
    });
}

export { getActiveClass, countClassActiveStudent }