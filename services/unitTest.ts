import { shortClassUnitTestSelect } from "dto/entitiesSelect";
import prisma from "lib/prisma";
import { commonDeleteStatus, commonStatus } from "../constants";

const checkStudentAccessUnitTestAssigned = async (studentId: string, unitTestAssigned: any) => {
    const classStudent = await prisma.classStudent.findFirst({
        where: {
            class_id: unitTestAssigned.class_id,
            student_id: studentId,
            status: commonStatus.ACTIVE,
            classes: {
                status: commonStatus.ACTIVE,
                deleted: commonDeleteStatus.ACTIVE,
            }
        }
    });

    if (
        !classStudent ||
        (unitTestAssigned.student_id && studentId !== unitTestAssigned.student_id) ||
        (unitTestAssigned.class_group_id && classStudent.class_group_id !== unitTestAssigned.class_group_id)
    ) {
        return false;
    }

    return true;
}

const getStudentUnitTestAvailable = async (classId: number, studentId: string, dateCondition?: any) => {
    const classStudent = await prisma.classStudent.findFirst({
        where: {
            class_id: classId,
            student_id: studentId,
        }
    })

    if (!classStudent) {
        return null;
    }

    const classUnitTestIds = (await prisma.unit_test_result.findMany({
        where: {
            unitTestAssign: {
                class_id: classId,
            },
            user_id: studentId,
        },
        select: {
            class_unit_test_id: true,
        }
    })).map(el => el.class_unit_test_id)

    const unitTest = await prisma.classUnitTest.findMany({
        where: {
            AND: [
                {
                    OR: [
                        {
                            class_id: classStudent.class_id,
                            OR: [
                                {
                                    class_group_id: null,
                                    student_id: null,
                                },
                                {
                                    class_group_id: null,
                                    student_id: classStudent.student_id,
                                },
                                {
                                    class_group_id: classStudent.class_group_id,
                                    student_id: null,
                                },
                            ]
                        },
                        {
                            id: {
                                in: classUnitTestIds,
                            }
                        }
                    ],
                },
                {
                    OR: dateCondition,
                }
            ]
        },
        select: {
            ...shortClassUnitTestSelect,
            unit_test_result: {
                where: {
                    user_id: studentId,
                },
                orderBy: {
                    created_date: 'desc',
                },
                take: 1,
            },
            created_date: true,
        },
        orderBy: {
            created_date: 'desc',
        }
    })

    let unitTestResponse = unitTest;
    if (classStudent?.status !== commonStatus.ACTIVE) {
        unitTestResponse = unitTest.filter(el => !!el.unit_test_result[0]);
    }

    return unitTestResponse;
}

export { checkStudentAccessUnitTestAssigned, getStudentUnitTestAvailable };