import { SMTPClient, Message } from 'emailjs'
import { EMAIL_CONFIG } from '@/interfaces/constants'

interface MessageHeaders {
    text?: string,
    to: string | string[],
    cc?: string,
    subject: string,
    content?: string,
    attachment?: any | any[],
}

const sendEmail = ({
    text = '',
    to,
    cc = '',
    subject,
    content,
    attachment,
}: MessageHeaders) => {
    const smtpClient = new SMTPClient(EMAIL_CONFIG);

    const messageSend = new Message({
        text,
        from: EMAIL_CONFIG.user,
        to,
        cc,
        subject,
        attachment: attachment ?? [{ data: content, alternative: true }],
    });

    return new Promise(res => {
        smtpClient.send(messageSend, (error, message) => {
            res({ error, message });
        })
    });
}

export { sendEmail }