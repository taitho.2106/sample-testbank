import dayjs from 'dayjs';
import prisma from 'lib/prisma';
import { SALE_ITEM_TYPE } from '../constants';
import { query } from '../lib/db';

export const saveUserAssets = async (asset: any) => {
    const results: any = await query<any>(
        `INSERT INTO \`user_asset\`(
            user_id, 
            asset_id, 
            asset_type, 
            created_date, 
            updated_date, 
            start_date, 
            end_date, 
            quantity
            ) VALUES(?,?,?,?,?,?,?,?)`,
        [
            asset.userId,
            asset.assetId,
            asset.assetType,
            asset.createdDate,
            asset.createdDate,
            asset.createdDate,
            asset.endDate,
            asset.quantity,
        ],
    )

    if (results.affectedRows === 1) {
        // success
        return results;
    }

    // failed
    return null;
}

export const updateUserAssets = async (asset: any) => {
    const results:any = await query<any>(
        `UPDATE \`user_asset\` 
            SET 
                asset_id = ?, 
                updated_date = ?, 
                start_date = ?, 
                end_date = ?, 
                quantity = ?
            WHERE id = ?`,
        [
            asset.assetId,
            new Date(),
            asset.startDate,
            asset.endDate,
            asset.quantity,
            asset.id,
        ]
    )

    if (results.affectedRows === 1) {
        // success
        return results;
    }

    // failed
    return null;
}

const addUnitTestAssetForUser = (unitTestId: number, userId: string) => {
    return prisma.user_asset.create({
        data: {
            user_id: userId,
            asset_id: unitTestId + '',
            asset_type: SALE_ITEM_TYPE.UNIT_TEST,
            created_date: new Date(),
            updated_date: new Date(),
            start_date: new Date(),
            quantity: 1,
        }
    })
}

export { addUnitTestAssetForUser }