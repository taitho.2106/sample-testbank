
import { getContentEmailAccountCreated, titleAccountCreated } from "@/constants/email-template";
import { ClassError } from "@/constants/errorCodes";
import { USER_ROLES } from "@/interfaces/constants";
import { getAuditData, getYupError } from "lib/apiHandle";
import prisma from "lib/prisma";
import { sendEmail } from "services/email";
import { createListUser, findAllUserExisted } from "services/user";
import { addStudentSchema, validStudentCode } from "validations";
import { commonStatus } from "../constants";

const verifyStudentData = async (studentData: any, t:any) => {
    const listPhone = [], listEmail = [];
    for (const row of studentData) {
        if (row.phone) {
            listPhone.push(row.phone);
        }
        if (row.email) {
            listEmail.push(row.email);
        }
    }

    const existedStudents = await findAllUserExisted({ phone: listPhone, email: listEmail }, {
        user_name: true,
        email: true,
        phone: true,
        id: true,
        user_role_id: true,
    });

    const prev: any = {}
    const checkStudent = async (stu: any) => {
        if (prev[stu.phone]) {
            return { status: ClassError.IMPORT_STUDENT_DUPLICATE_PHONE }
        }
        if (prev[stu.email]) {
            return { status: ClassError.IMPORT_STUDENT_DUPLICATE_EMAIL }
        }
        if (prev[stu.studentCode]) {
            return { status: ClassError.IMPORT_STUDENT_DUPLICATE_STUDENT_CODE }
        }

        for (const existedStu of existedStudents) {
            if (existedStu.phone === stu.phone) {

                if (existedStu.user_role_id !== USER_ROLES.Student) {
                    return { status: ClassError.USER_NOT_STUDENT }
                }

                try {
                    await validStudentCode(t).validate(stu, { abortEarly: false })
                } catch (error) {
                    return { status: ClassError.IMPORT_STUDENT_INVALID, errors: getYupError(error) }
                }

                return { id: existedStu.id }
            }
        }

        try {
            await addStudentSchema(t).validate(stu, { abortEarly: false })
        } catch (error) {
            return { status: ClassError.IMPORT_STUDENT_INVALID, errors: getYupError(error) }
        }

        if (existedStudents.some(existedStu => existedStu.email === stu.email)) {
            return { status: ClassError.IMPORT_STUDENT_EXISTED }
        }
    }

    const result: any = [];
    for (const stu of studentData) {
        const rawEmailData = stu.email;
        stu.email = typeof stu.email === 'string' ? stu.email.toLowerCase() : stu.email;
        result.push({ ...stu, email: rawEmailData, ...(await checkStudent(stu)) });

        stu.phone && (prev[stu.phone] = true);
        stu.email && (prev[stu.email] = true);
        stu.studentCode && (prev[stu.studentCode] = true);
    }

    return result;
}

const createUsersAndSendMail = async (users: any) => {
    let studentCreateResult = [];
    if (users.length) {
        studentCreateResult = await createListUser(users, USER_ROLES.Student);
    }

    for (const newStu of studentCreateResult) {
        try {
            if (newStu.email) {
                sendEmail({
                    to: newStu.email,
                    subject: titleAccountCreated,
                    content: getContentEmailAccountCreated({ username: newStu.phone, password: newStu.textPassword }),
                });
            }
        } catch (error) {
        }
    }

    return studentCreateResult;
}

const addStudentsToTeacherIfNotExist = async (students: any, teacherId: string, session: any) => {
    const alreadyAdded: any = await prisma.teacherStudent.findMany({
        where: {
            teacher_id: teacherId,
            student_id: {
                in: students.map((el: any) => el.id),
            }
        }
    });

    const studentsAdded: any = [];
    const studentsWillAdd: any = [];
    const studentWillActive: any = [];

    for (const stu of students) {
        const studentAdded = alreadyAdded.find((added: any) => added.student_id === stu.id)
        if (studentAdded) {
            if (studentAdded.status === commonStatus.ACTIVE) {
                studentsAdded.push({
                    ...stu,
                    status: ClassError.IMPORT_STUDENT_ALREADY_ADDED,
                });
            } else {
                studentWillActive.push(stu);
            }
        } else {
            studentsWillAdd.push(stu);
        }
    }

    if (studentWillActive) {
        await prisma.teacherStudent.updateMany({
            where: {
                student_id: {
                    in: studentWillActive.map((el: any) => el.id),
                }
            },
            data: {
                status: commonStatus.ACTIVE,
            }
        })
    }

    if (studentsWillAdd.length) {
        await prisma.teacherStudent.createMany({
            data: studentsWillAdd.map((stu: any) => ({
                student_id: stu.id,
                teacher_id: teacherId,
                ...getAuditData(session, true),
            })),
        });
    }

    return { errors: studentsAdded, success: [ ...studentsWillAdd, ...studentWillActive ] };
}

const addStudentsToClassIfNotExist = async (students: any, classId: number, session: any) => {
    const alreadyAddedClass: any = await prisma.classStudent.findMany({
        where: {
            class_id: classId,
            OR: [
                {
                    student_id: {
                        in: students.map((stu: any) => stu.id),
                    }
                },
                {
                    student_code: {
                        in: students.map((stu: any) => stu.studentCode).filter(Boolean),
                    }
                }
            ]
        }
    });

    const studentsAdded: any = [];
    const studentsWillAdd: any = [];
    const studentWillActive: any = [];

    for (const stu of students) {
        const existed = alreadyAddedClass.find((exStu: any) => exStu.student_code === stu.studentCode || exStu.student_id === stu.id);

        if (existed) {
            if (existed.student_id === stu.id && existed.status === commonStatus.INACTIVE) {
                studentWillActive.push(stu);
            } else {
                studentsAdded.push({
                    ...stu,
                    status: existed.student_id === stu.id ? ClassError.IMPORT_STUDENT_ALREADY_ADDED : ClassError.IMPORT_STUDENT_DUPLICATE_STUDENT_CODE,
                });
            }
        } else {
            studentsWillAdd.push(stu);
        }
    }

    if (studentWillActive) {
        await prisma.$transaction(studentWillActive.map((stu: any) => prisma.classStudent.updateMany({
            where: {
                student_id: stu.id,
            },
            data: {
                status: commonStatus.ACTIVE,
                student_code: stu.studentCode
            }
        })))
    }

    if (studentsWillAdd.length) {
        await prisma.classStudent.createMany({
            data: studentsWillAdd.map((stu: any) => ({
                class_id: classId,
                student_id: stu.id,
                student_code: stu.studentCode,
                ...getAuditData(session, true),
            })),
        });
    }

    return { errors: studentsAdded, success: [ ...studentsWillAdd, ...studentWillActive ] };
}

export { addStudentsToClassIfNotExist, addStudentsToTeacherIfNotExist, createUsersAndSendMail, verifyStudentData };
