import dayjs from "dayjs";
import prisma from "lib/prisma";
import { commonStatus, SALE_ITEM_TYPE } from "../constants";

export const getStudentComboUnitTest = async (studentId: string) => {
    const currentCombo: any = await prisma.user_asset.findFirst({
        where: {
            user_id: studentId,
            asset_type: SALE_ITEM_TYPE.COMBO_UNIT_TEST,
            status: commonStatus.ACTIVE,
        }
    })

    if (currentCombo) {
        currentCombo.isExpired = dayjs().isAfter(currentCombo.end_date);
        currentCombo.item = await prisma.comboUnitTest.findUnique({ where: { id: +currentCombo.asset_id } });

        if (currentCombo.item) {
            currentCombo.item.unit_test_types = currentCombo.item.unit_test_types.split(',').map(Number);
        }
    }

    return currentCombo;
}