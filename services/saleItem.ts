import { saleItem } from "@prisma/client";
import prisma, { prismaRaw } from "lib/prisma";
import { SALE_ITEM_TYPE } from "../constants";

const getItemInfoByType = (id: number, type: string) => {
    const typeMap = {
        [SALE_ITEM_TYPE.UNIT_TEST]: prisma.unit_test,
        [SALE_ITEM_TYPE.SERVICE_PACKAGE]: prisma.service_package,
        [SALE_ITEM_TYPE.COMBO_UNIT_TEST]: prisma.comboUnitTest,
        [SALE_ITEM_TYPE.COMBO_SERVICE_PACKAGE]: prisma.service_package,
    }

    const schema: any = typeMap[type as keyof Object] || prisma.service_package;

    return schema.findUnique({ where: { id } });
}

const getSaleItemInfo = async (id: number) => {
    const saleItemData: saleItem & { item?: any } = await prisma.saleItem.findUnique({
        where: { id },
    })

    if (saleItemData) {
        const item = await getItemInfoByType(saleItemData.item_id, saleItemData.item_type);
        saleItemData.item = item;
    }

    return saleItemData;
}

const mapTypeToTable = {
    [SALE_ITEM_TYPE.UNIT_TEST]: 'unit_test',
    [SALE_ITEM_TYPE.SERVICE_PACKAGE]: 'service_package',
    [SALE_ITEM_TYPE.COMBO_UNIT_TEST]: 'combo_unit_test',
    [SALE_ITEM_TYPE.COMBO_SERVICE_PACKAGE]: 'service_package',
}
const getListSaleItemByType = async (type: string, skip?: number, take?: number) => {
    const result: any = await prisma.$queryRaw`SELECT 
        si.id as 'sale_item_id', 
        si.name as 'sale_item_name', 
        si.exchange_price as 'sale_item_exchange_price', 
        si.discount_price as 'sale_item_discount_price', 
        si.day_expire as 'sale_item_day_expire', 
        si.item_id as 'sale_item_item_id', 
        si.item_type as 'sale_item_item_type', 
        it.* FROM sale_item si 
        JOIN ${prismaRaw(mapTypeToTable[type as keyof Object] + '')} it 
            ON si.item_id = it.id 
        WHERE si.item_type = ${type} 
        LIMIT ${take} OFFSET ${skip}`;

    return result.map(({
        sale_item_id,
        sale_item_name,
        sale_item_exchange_price,
        sale_item_discount_price,
        sale_item_day_expire,
        sale_item_item_id,
        sale_item_item_type,
        ...rest
    }: any) => ({
        id: sale_item_id,
        name: sale_item_name,
        exchange_price: sale_item_exchange_price,
        discount_price: sale_item_discount_price,
        day_expire: sale_item_day_expire,
        item_id: sale_item_item_id,
        item_type: sale_item_item_type,
        item: rest,
    }));
}

export { getSaleItemInfo, getListSaleItemByType };