import { query } from '../lib/db';

export const saveOrder = async (order: any) => {
    const results: any = await query<any>(
        `INSERT INTO \`order\`(
            user_id, 
            item_id, 
            created_date, 
            updated_date, 
            quantity, 
            total, 
            status
            ) VALUES(?,?,?,?,?,?,?)`,
        [order.userId, order.item_id, order.createdDate, order.updatedDate, order.quantity, order.total, order.status],
    )

    if (results.affectedRows === 1) {
        // success
        return results;
    }

    // failed
    return null;
}

export const getOrder = async (orderId: string) => {
    const results: any = await query<any>(
        `SELECT od.*, sp.name as name, sp.description as description, sp.price as price 
            FROM \`order\` od 
            JOIN \`service_package\` sp 
            ON od.package_id = sp.id 
            WHERE od.id = ?`,
        [orderId],
    );

    const result = results[0];

    if (!result) {
        return result;
    }

    const { name, description, price, vnpay_params, ...restOrder } = result;
    restOrder.servicePackage = {
        id: restOrder.package_id,
        name,
        description,
        price,
    }

    return restOrder;
}

export const updateOrderPayment = async (orderId: any, orderPayment: any) => {
    const results:any = await query<any>(
        `UPDATE \`order\` 
            SET 
                status = ?, 
                vnp_transaction_no = ?, 
                vnp_transaction_status = ?, 
                vnp_response_code = ?, 
                updated_date = ?, 
                note = ?, 
                vnpay_params = ? 
            WHERE id = ?`,
        [
            orderPayment.status,
            orderPayment.trxNo,
            orderPayment.trxStatus,
            orderPayment.rspCode,
            new Date(),
            orderPayment.note,
            orderPayment.vnpParams,
            orderId
        ]
    )

    if (results.affectedRows === 1) {
        // success
        return results;
    }

    // failed
    return null;
}