import dayjs from 'dayjs';
import { SALE_ITEM_TYPE } from '../constants';
import { query } from '../lib/db';

export const getPackageById = async (id: keyof Object) => {
    const results: any = await query<any>(
        `SELECT * FROM \`service_package\` where id = ?`,
        [id],
    );

    return results[0];
}

export const getPackageByCode = async (code: keyof Object) => {
    const results: any = await query<any>(
        `SELECT * FROM \`service_package\` where code = ?`,
        [code],
    );

    return results[0];
}

export const getLastPackage = async (userId: string) => {
    const results: any = await query<any>(
        `SELECT sp.*, 
                us.start_date AS start_date, us.end_date AS end_date, 
                us.created_date AS created_date, us.id as asset_id 
            FROM \`service_package\` sp 
            JOIN \`user_asset\` us 
                ON sp.id = us.asset_id 
            WHERE sp.status = 1 
                AND us.asset_type = ? 
                AND us.user_id = ?`,
        [
            SALE_ITEM_TYPE.SERVICE_PACKAGE,
            userId,
        ],
    );

    if (results[0]) {
        results[0].isExpired = dayjs().isAfter(results[0].end_date);
    }

    return results[0];
}