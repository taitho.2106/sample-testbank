import { ClassError } from "@/constants/errorCodes";
type Props = {
    isImportIntoClass?: boolean
    t?:any
}
export const mapMessageError:any = ({isImportIntoClass = false, t}:Props) => {
    return {
        //add-new-student
        [ClassError.CLASS_NOT_FOUND] : t('message-error')[ClassError.CLASS_NOT_FOUND],
        [ClassError.STUDENT_NOT_FOUND] : t('message-error')[ClassError.STUDENT_NOT_FOUND],
        [ClassError.STUDENT_EXISTED] : t('message-error')[ClassError.STUDENT_EXISTED],
        [ClassError.STUDENT_EXISTED_CODE] : t('message-error')[ClassError.STUDENT_EXISTED_CODE],
    
        //[import student to classes, import student to teacher]
        [ClassError.IMPORT_STUDENT_DUPLICATE_PHONE] : t('message-error')[ClassError.IMPORT_STUDENT_DUPLICATE_PHONE],
        [ClassError.IMPORT_STUDENT_DUPLICATE_EMAIL] : t('message-error')[ClassError.IMPORT_STUDENT_DUPLICATE_EMAIL],
        [ClassError.IMPORT_STUDENT_DUPLICATE_STUDENT_CODE] : t('message-error')[ClassError.IMPORT_STUDENT_DUPLICATE_STUDENT_CODE],
        [ClassError.IMPORT_STUDENT_OLD] : t('message-error')[ClassError.IMPORT_STUDENT_OLD],
        [ClassError.USER_NOT_STUDENT]: t('message-error')[ClassError.USER_NOT_STUDENT],
        [ClassError.IMPORT_STUDENT_INVALID]: t('message-error')[ClassError.IMPORT_STUDENT_INVALID],
        [ClassError.IMPORT_STUDENT_EXISTED]: t('message-error')[ClassError.IMPORT_STUDENT_EXISTED],
        [ClassError.IMPORT_STUDENT_ALREADY_ADDED] : t(t('message-error')[ClassError.IMPORT_STUDENT_ALREADY_ADDED],[`${isImportIntoClass ?  t('message-error')["in-class"] : ''}`]),
    }
}