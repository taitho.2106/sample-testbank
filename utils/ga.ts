import { googleTagManagerId } from "../constants";

declare const window: Window & { gtag: any; };

const noop = () => { };

const checkTracking = () => {
    if (!googleTagManagerId || typeof window === 'undefined' || !window.gtag) {
        return false;
    }

    return true;
}

const pageView = (url: string) => {
    if (!checkTracking()) {
        return;
    }

    window.gtag('event', 'page_view', {
        page_path: url,
    })
}

const unitTestUse = (unitTest: any, eventCallback = noop) => {
    if (!checkTracking()) {
        return;
    }

    window.gtag('event', 'unit_test_use', {
        event_category: 'unit_test_use',
        event_label: 'Unit test use',
        unit_test: {
            id: unitTest.id,
            name: unitTest.name,
        }
    })
}

const ga = {
    unitTestUse,
    pageView,
}

export default ga;