export default function convertTextCell(str:any){
    if(Array.isArray(str)){
        let strResult = ""
        for(let i=0; i<str.length;i++){
            if(str[i]){
                strResult+=str[i].text
            }
        }
        return strResult;
    }else{
        return str;
    }
}