const arrStyle = ["bold", "italic", "underline"];
export default function convertFontStyle(arr: any) {
    if(!arr) return arr;
    let str = ''
    const length = arr.length
    for (let i = 0; i < length; i++) {
      const item = arr[i]
      if(typeof item == "object"){
        let strItem =item.text
        if(item.bold){
          strItem = `%b%${strItem}%b%`
        }
        if(item.italic){
          strItem = `%i%${strItem}%i%`
        }
        
        if(item.underline){
          strItem = `%u%${strItem}%u%`
        }
        str+= strItem;
      }else{
        str+= item;
      }
      
    }
    return str
  }
  