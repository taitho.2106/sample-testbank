export default function handlerUnderline(arr: any) {
  let underline = false
  let str = ''
  const length = arr.length
  for (let i = 0; i < length; i++) {
    const item = arr[i]
    if (item.underline == true) {
      if (underline == true) {
        str += item.text
      } else {
        str += '%u%' + item.text
      }
      if (i == length - 1) {
        str += '%u%'
      }
    } else {
      if (underline == true) {
        str += '%u%' + item.text
      } else {
        str += item.text
      }
    }
    underline = item.underline
  }
  return str
}
