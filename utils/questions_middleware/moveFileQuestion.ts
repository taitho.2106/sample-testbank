import { uploadDir } from "@/constants/index";
import fs from "fs";
import path from "path";

export default function moveFileQuestion(oldFolder:string,newFolder:any, arrFileName:any[]){
  try{
    for(let i=0; i<arrFileName.length;i++){
      const fileName = arrFileName[i];
      const newPathName = `${uploadDir}/${newFolder}/${fileName}`;
      const des = path.dirname(newPathName)
      if (!fs.existsSync(des)) {
        fs.mkdirSync(des, { recursive: true })
      }
      if(fileName){
       const result =  fs.renameSync(`${oldFolder}/${fileName}`, newPathName)
      }
  }
  } catch(ex){
    console.log("ex=====================",ex)
  }
}