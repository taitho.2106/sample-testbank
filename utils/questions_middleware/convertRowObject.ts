import { cellArrayObject } from './cellDefine'
import convertTextCell from './convertTextCell'
import trimText from './trimText'

//chuyển objec row có index thành k có index
//ex old={A2:"123", B2:"abc", C2:{text:"c1111", bold:true}} => new ={question_type:"123",level:"abc", cefr:{text:"c1111", bold:true}}
export default function convertRowObject(dataRow: any, index: number) {
  const len = cellArrayObject.length
  const result: any = {}
  if (!dataRow) return {}
  for (let i = 0; i < len; i++) {
    const cellObject = cellArrayObject[i]
    const oldKey = `${cellObject.code}${index}`
    const newKey = `${cellObject.key}`
    if (dataRow[oldKey]) {
      let cellData = dataRow[oldKey]

      //if not answer => replace special character.
      if(!newKey.startsWith("answer")){
        cellData = replaceSpecialCharacter(cellData)
      }
      result[newKey] = cellData
      if (typeof cellData == 'string') {
        result[newKey] = cellData.trim()
      } else {
        if (
          !cellObject.disableStyle &&
          Array.isArray(cellData) &&
          cellData.length > 0
        ) {
          ///enable style and cell data  is array
          if (cellData.length == 1) {
            cellData[0].text = cellData[0].text.trim()
            if (!cellData[0].text) {
              cellData[0] = null
            }
          } else {

            //trim first item  and remove if no data
            while (cellData.length > 0) {
              const cellMiniData = miniObjectText(cellData[0], 1)
              if (!cellMiniData) {
                cellData.shift()
              } else {
                cellData[0] = cellMiniData
                break
              }
            }
             //trim end item  and remove if no data
            while (cellData.length > 0) {
              const lenArr = cellData.length
              const cellMiniData = miniObjectText(cellData[lenArr - 1], 2)
              if (!cellMiniData) {
                cellData.pop()
              } else {
                cellData[lenArr - 1] = cellMiniData
                break
              }
            }
          }

          //if length = 0, set data = null;
          if(cellData.length == 0){
            result[newKey] = null;
          }else{
            result[newKey] = cellData;
          }
        } else {
          result[newKey] = trimText(convertTextCell(cellData), 3)
        }
      }
    }
  }
  return result
}
function miniObjectText(obj: any, modeTrim: 1 | 2 | 3) {
  obj.text = trimText(obj.text, modeTrim)
  if (!obj.text) {
    obj = ''
  }
  return obj
}
function replaceSpecialCharacter(value:any){
  if(typeof value == "string"){
    return value.replace(/(\*|\#|\[])/g, '')
  }else if(Array.isArray(value)){
    const arr = [...value];
    for(let i=0; i<value.length; i++){
      arr[i].text = arr[i].text.replace(/(\*|\#|\[])/g, '')
    }
    return arr;
  }else if( value.text){
    return value.text.replace(/(\*|\#|\[])/g, '')
  }
}

