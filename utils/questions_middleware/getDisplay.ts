const handleGetDisplay = (constant: any[], code: string) => {
  if(!code){
    return "";
  }
  for (const i in constant) {
    if (constant[i].code == code) {
      return constant[i].display
    }
  }
}
const handleGetDisplayCodeNumber = (constant: any[], code: number) => {
  if(!code){
    return "";
  }
const item = constant.find(m=>m.code == code);
if(item) return item.display;
return ""
}
const getDisplay = {handleGetDisplay,handleGetDisplayCodeNumber}
export default getDisplay
