export default function handlerSpaceWord(str: string) {
  if (!str) return str
  let result = ''
  let i = 0
  while (i < str.length) {
    if (str[i] === '_') {
      if (str[i + 1] === '_') {
        // i++;
      } else {
        result += '%s%'
      }
    } else {
      result += str[i]
    }

    i++
  }
  return result
}
