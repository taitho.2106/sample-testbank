export default function handlerBoldUnderline(arr: any) {
  if(!arr) return arr;
  let underline = false
  let bold = false
  let str = ''
  const length = arr.length
  for (let i = 0; i < length; i++) {
    const item = arr[i]
    if (item.bold === true) {
      if (bold === true) {
        str += item.text
      } else {
        if (underline === true) {
          str += '%u%%' + item.text
          underline = false
        } else {
          if (i === length - 1) {
            str += '%' + item.text + '%'
          } else {
            str += '%' + item.text
          }
        }
      }
      bold = item.bold
    } else if (item.underline === true) {
      if (underline === true) {
        str += item.text
      } else {
        if (bold === true) {
          str += '%%u%' + item.text
          bold = false
        } else {
          if (i === length - 1) {
            str += '%u%' + item.text + '%u%'
          } else {
            str += '%u%' + item.text
          }
        }
      }
      underline = item.underline
    } else {
      if (underline === true) {
        str += '%u%' + item.text
        underline = false
      } else {
        if (bold === true) {
          str += '%' + item.text
          bold = false
        } else str += item.text
      }
    }
  }
  return str
}
