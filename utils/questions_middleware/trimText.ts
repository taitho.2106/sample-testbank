//mode: 1 start, 2 end, 3: start and end
export default function trimText(str: string, mode: 1 | 2 | 3) {
  try {
    const arrVerify = [1, 2, 3]
    if (!str || typeof str != 'string') return str
    if (arrVerify.indexOf(mode) == -1) return str
    switch (mode) {
      case 1:
        let textResult1 = str
        const i = 0
        while (i < textResult1.length) {
          if (textResult1[0] == ' ') {
            textResult1 = textResult1.substring(1)
          } else {
            break
          }
        }
        return textResult1
      case 2:
        let textResult2 = str
        while (textResult2.length > 0) {
          const i = textResult2.length - 1
          if (textResult2[i] == ' ') {
            textResult2 = textResult2.slice(0, -1)
          } else {
            break
          }
        }
        return textResult2
      case 3:
        return str.trim()
      default:
        return str
    }
  } catch (e) {
    console.log('trimText--------', e)
    return str;
  }
}
