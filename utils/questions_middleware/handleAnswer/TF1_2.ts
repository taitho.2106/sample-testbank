import { cellAnswerArray } from '../cellDefine'
import convertTextCell from '../convertTextCell'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
import answerToArray from './answerToArray'
import checkTFNi from './checkTFNi'

function handlerAnswerTF12(objectRow: any) {
  const arr_answer: any[] = []
  const arr_corret_answer: any[] = []
  const length = cellAnswerArray.length
  const arrAnswerRaw = answerToArray(objectRow)
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 2)

  if (verifyCell.code === 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < length; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {
      const text = convertTextCell(objectRow[cellKey])?.replace(/(\*|\#|\[])/g, '')
      const arr = text.split('|')
      if(arr.length !== 2){
        return {code:0, message:'Vui lòng nhập |True, |False cho từng đáp án'}
      }else{
        if(arr[0] == "" || arr[1]== ""){
          return {code:0, message:'Đáp án không hợp lệ'}
        }
        arr_answer.push(arr[0])
        arr_corret_answer.push(arr[1])
      }
      
    } else {
      break
    }
  }
  for (const i in arr_corret_answer) {
    if (arr_corret_answer[i].toLowerCase() === 'true') {
      arr_corret_answer[i] = 'T'
    }
    if (arr_corret_answer[i].toLowerCase() === 'false') {
      arr_corret_answer[i] = 'F'
    }
  }
  const constant = ['T', 'F']
  const ans = checkTFNi(constant, arr_corret_answer)
  if (ans.code === 0) {
    return { code: 0, message: ans.message }
  }
  return {
    code: 1,
    answer: arr_answer.join('#'),
    correct_answer: arr_corret_answer.join('#'),
    length_answer: arr_answer.length,
    length_corret_answer: arr_corret_answer.length,
    message: '',
  }
}

export default handlerAnswerTF12
