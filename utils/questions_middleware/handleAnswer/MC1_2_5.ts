import { cellAnswerArray } from '../cellDefine'
import convertFontStyle from '../convertFontStyle'
import convertTextCell from '../convertTextCell'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
import answerToArray from './answerToArray'

export default function handleAnswerMC1_2_5(objectRow: any) {
  const arrAnswer = []
  const arrCorrectAnswer = []
  const len = cellAnswerArray.length
  const arrAnswerRaw = answerToArray(objectRow)
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 2)

  if (verifyCell.code == 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < len; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {
    let text = convertTextCell(objectRow[cellKey]);
      let textHtmlFormat = convertFontStyle(objectRow[cellKey]);
      if (text.startsWith('*')) {
        textHtmlFormat = textHtmlFormat?.replace(/(\*|\#|\[])/g, '')
        const textCorrect = text?.replace(/(\*|\#|\[])/g, '')
        arrCorrectAnswer.push(textCorrect)
      }else{
        text = text?.replace(/(\*|\#|\[])/g, '')
        textHtmlFormat = textHtmlFormat?.replace(/(\*|\#|\[])/g, '')
      }
      // remove *, # in answer
     
      if(text == "" || textHtmlFormat == ""){
        return { code: 0, message: 'Đáp án không hợp lệ' }
      }
      arrAnswer.push(textHtmlFormat)
    } else {
      break
    }
  }
  if (arrCorrectAnswer.length > 1) {
    return { code: 0, message: 'Vui lòng chỉ đánh dấu 1 đáp án đúng' }
  } else if (arrCorrectAnswer.length == 0) {
    return {
      code: 0,
      message: 'Vui lòng nhập dấu * trước 1 cột Answer được cho là đáp án đúng',
    }
  }
  const strAnswer = arrAnswer.join('*')
  const strCorrectAnswer = arrCorrectAnswer.join('%/%')
  return {
    code: 1,
    message: '',
    data: {
      answers: strAnswer,
      correctAnswers: strCorrectAnswer,
      countAnswer: arrAnswer.length,
      countCorrectAnswer : arrCorrectAnswer.length
    },
  }
}
