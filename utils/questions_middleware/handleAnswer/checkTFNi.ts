export default function checkTFNi(constant: any[], arrCheck: any[]) {
  for (const i in arrCheck) {
    if (constant.indexOf(arrCheck[i]) === -1) {
      if(constant.includes('NI')){
        return { code: 0, message: 'Vui lòng nhập |True, |False, |Ni cho từng đáp án' }
      }else{
        return { code: 0, message: 'Vui lòng nhập |True, |False cho từng đáp án' }
      }
    }
  }
  return { code: 1, message: '' }
}
