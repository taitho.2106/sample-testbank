import { cellAnswerArray } from '../cellDefine'
import convertFontStyle from '../convertFontStyle'
import convertTextCell from '../convertTextCell'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
import answerToArray from './answerToArray'

export default function handleAnswerMC4(objectRow: any) {
  const arrAnswer = []
  const arrCorrectAnswer = []
  const len = cellAnswerArray.length
  const arrAnswerRaw =answerToArray(objectRow)
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 2)

  if (verifyCell.code == 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < len; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {
      let textBold = convertFontStyle(objectRow[cellKey])
      let text = convertTextCell(objectRow[cellKey])
      if (text.startsWith('*')) {
        text = text?.replace(/(\*|\#|\[])/g, '')
        textBold = textBold?.replace(/(\*|\#|\[])/g, '')
        arrCorrectAnswer.push(text)
      }else{
        textBold = textBold?.replace(/(\*|\#|\[])/g, '')
      }
      console.log("text-bold-------",textBold)
      if(textBold == ""){
        return { code: 0, message: 'Đáp án không hợp lệ' }
      }
      arrAnswer.push(textBold)
    } else {
      break
    }
  }
  if(arrCorrectAnswer.length >1){
      return { code: 0, message: "Vui lòng chỉ đánh dấu 1 đáp án đúng" }
  }else
  if(arrCorrectAnswer.length ==0){
    return { code: 0, message: "Vui lòng nhập dấu * trước 1 cột Answer được cho là đáp án đúng" }
    }
  const strAnswer = arrAnswer.join('*')
  const strCorrectAnswer = arrCorrectAnswer.join('%/%')
  return {code :1, message:"", data:{answers: strAnswer, correctAnswers:strCorrectAnswer}}
}
