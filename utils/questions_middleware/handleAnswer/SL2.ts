import { cellAnswerArray } from '../cellDefine'
import convertTextCell from '../convertTextCell'
import answerToArray from '../handleAnswer/answerToArray'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
function handlerAnswerSL2(objectRow: any) {
  const arr_answer:any[] = []
  const correct_answers = []
  const length = cellAnswerArray.length
  const arrAnswerRaw = answerToArray(objectRow)
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 1)

  if (verifyCell.code == 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < length; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {

      ///chỉ được nhập 1 answer.
      if(arr_answer.length>=1){
        return {
          code: 0,
          message:
            'Vui lòng chỉ nhập 1 ô đáp án',
        }
      }
      const text = convertTextCell(objectRow[cellKey])
      const arr = text.split('|')

      if(arr.length<=1){
        return {
          code: 0,
          message:
            'Vui lòng nhập ít nhất 2 đáp án cách nhau bởi dấu |',
        }
      }
      const arrCorrect = arr.filter((item: any) => item.startsWith('*'));
      if ( arrCorrect.length != 1) {
        return {
          code: 0,
          message:
            'Vui lòng nhập dấu * trước 1 giá trị được cho là đáp án đúng',
        }
      }
      for (const j in arr) {
        if (arr[j].startsWith('*')) {
          arr[j] = arr[j]?.replace(/(\*|\#|\[])/g, '')
          correct_answers.push(arr[j])
        }else{
          arr[j] = arr[j]?.replace(/(\*|\#|\[])/g, '')
        }
      }
      const findEmpty = arr_answer.find(m=> m =="");
      if(findEmpty){
        return {
          code: 0,
          message:
            'Đáp án không hợp lệ',
        }
      }
      arr_answer.push(arr.join('*'))
    } else {
      break
    }
  }
  if (arr_answer.length >1) {
    return {
      code: 0,
      message:
        'Vui lòng chỉ nhập duy nhất 1 đáp án',
    }
  }
  return {
    code: 1,
    arr_answer: arr_answer.join('#'),
    correct_answers: correct_answers.join('#'),
    lengthAnswer: arr_answer.length,
    message: '',
  }
}

export default handlerAnswerSL2
