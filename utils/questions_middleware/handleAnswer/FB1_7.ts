import { cellAnswerArray } from '../cellDefine'
import convertTextCell from '../convertTextCell'
import answerToArray from '../handleAnswer/answerToArray'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
export default function handlerAnswerFB17(objectAnswer: any) {
  // console.log('objectAnswer', objectAnswer)
  const correctAnswers = []
  const length = cellAnswerArray.length
  const arrAnswerRaw = answerToArray(objectAnswer)
  for (let i = 0; i < length; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectAnswer[cellKey]) {
      const text = convertTextCell(objectAnswer[cellKey])?.replace(/(%\/%|\*|\#|\[])/g, '')
      if(!text){
        return {code:0, message:'Đáp án không hợp lệ'}
      }
      correctAnswers.push(text)
    } else {
      break
    }
  }
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 1)
  if (verifyCell.code === 0) {
    return { code: 0, message: verifyCell.message }
  }
  if(correctAnswers.length < 1){
    return {code:0, message:'Vui lòng có ít nhất 1 đáp án, nhập đáp án vào ô Answer 1'}
  }
  for (const i in correctAnswers) {
    if (correctAnswers[i].includes('|')) {
      correctAnswers[i] = correctAnswers[i].replaceAll('|', '%/%')
    }
  }
  // const str = correctAnswers.join('#')
  return {
    code: 1,
    data: {
      lengthAnswer: correctAnswers.length,
      stringAnswer: correctAnswers.join('#'),
    },
    message: '',
  }
}
