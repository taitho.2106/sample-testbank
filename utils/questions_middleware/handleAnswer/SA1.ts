import { cellAnswerArray } from '../cellDefine'
import convertTextCell from '../convertTextCell'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
import answerToArray from './answerToArray'

function handlerAnswerSA1(objectRow: any) {
  const arr_answer: any[] = []
  const length = cellAnswerArray.length
  const arrAnswerRaw = answerToArray(objectRow)
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 1)

  if (verifyCell.code == 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < length; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {
      const text = convertTextCell(objectRow[cellKey])?.replace(/(%\/%|\*|\#|\[])/g, '')
      // const arr = text.split('|')
      if(!text){
        return {code:0, message:'Đáp án không hợp lệ'}
      }
      arr_answer.push(text)
    } else {
      break
    }
  }
  if (arr_answer.length < 1) {
    return {
      code: 0,
      message: 'Vui lòng có ít nhất 1 đáp án, nhập đáp án vào ô Answer',
    }
  }
  return {
    code: 1,
    correct_answer: arr_answer.join('%/%'),
    length_answer: arr_answer.length,
    message: '',
  }
}

export default handlerAnswerSA1
