import { cellAnswerArray } from '../cellDefine'
import convertTextCell from '../convertTextCell'
import shuffleArray from '../shuffleArray'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
import answerToArray from './answerToArray'
export default function handleAnswerMG1_2_3(objectRow: any) {
  const arrAnswer = []
  const arrCorrectAnswer = []
  const len = cellAnswerArray.length
  const arrAnswerRaw = answerToArray(objectRow)
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 2)

  if (verifyCell.code == 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < len; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {
      const text = convertTextCell(objectRow[cellKey])?.replace(/(\*|\#|\[])/g, '')
      const arrText = text.split("|");
      if(!arrText || arrText.length!==2){
        return { code: 0, message: 'Vui lòng nhập 1 cặp đáp án đúng cho mỗi ô Answer, mỗi đáp án phân cách bằng dấu |' }
      }
      if(arrText[0] == "" || arrText[1]== ""){
        return {code:0, message:'Đáp án không hợp lệ'}
      }
      arrAnswer.push(arrText[0])
      arrCorrectAnswer.push(arrText[1])
    } else {
      break
    }
  }
 
  const strCorrectAnswer = arrCorrectAnswer.join('#')
  const strAnswer = `${arrAnswer.join('*')}#${shuffleArray(arrCorrectAnswer).join('*')}`
  return {
    code: 1,
    message: '',
    data: {
      answers: strAnswer,
      correctAnswers: strCorrectAnswer,
      countAnswer: arrAnswer.length,
      countCorrectAnswer : arrCorrectAnswer.length
    },
  }
}
