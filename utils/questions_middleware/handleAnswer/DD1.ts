import { cellAnswerArray } from '../cellDefine'
import convertTextCell from '../convertTextCell'
import shuffleArray from '../shuffleArray'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
import answerToArray from './answerToArray'

export default function handleAnswerDD1(objectRow: any) {
  const arrCorrectAnswer = []
  const len = cellAnswerArray.length
  const arrAnswerRaw =answerToArray(objectRow)
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 1)
  const arrFirst:any = []
let strAnswer = ""
  if (verifyCell.code == 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < len; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {
      const text = convertTextCell(objectRow[cellKey])?.replace(/(%\/%|\*|\#|\[])/g, '')
      if(!text){
        return { code: 0, message: "Vui lòng nhập ít nhất 2 từ đã sắp xếp hợp lệ vào ô đáp án. Mỗi từ phân cách bằng dấu |" }
      }
      let arrTextCell:string[] = text.split("|")||[];

      //check length
      if(arrTextCell.length <=1){
        return { code: 0, message: "Vui lòng nhập ít nhất 2 từ đã sắp xếp hợp lệ vào ô đáp án. Mỗi từ phân cách bằng dấu |" }
      }
      //check space
      const arrSpace = arrTextCell.filter((item: any) => item.trim().length==0);
      if(arrSpace.length>0){
        return { code: 0, message: "Vui lòng chỉ nhập đáp án bao gồm các từ đã được sắp xếp hợp lệ vào ô đáp án. Mỗi từ phân cách bằng dấu |" }
      }
      arrTextCell = arrTextCell.map(m=> m.trim());
      if(arrFirst.length>0){
        //các đáp án khác chiều dài.
          if(arrFirst.length != arrTextCell.length){
            return { code: 0, message: "Các phần tử của các đáp án không được khác nhau" }
          }
        for(let j = 0; j< arrTextCell.length; j++){
          if(arrFirst.indexOf(arrTextCell[j]) ==-1){
            return { code: 0, message: "Các phần tử của các đáp án không được khác nhau" }
          }
        }

      }else{
        arrFirst.push(...arrTextCell)
      }
      
      arrCorrectAnswer.push(arrTextCell.join("*"))
      if(strAnswer==""){
        strAnswer = shuffleArray(arrTextCell).join("*")
      }
      
    } else {
      break
    }
  }
  if(arrCorrectAnswer.length <=0){
      return { code: 0, message: "Vui lòng có ít nhất 1 đáp án, nhập đáp án vào ô Answer" }
  }

  const strCorrectAnswer = arrCorrectAnswer.join('%/%')
  return {code :1, message:"", data:{answers: strAnswer, correctAnswers:strCorrectAnswer}}
}
