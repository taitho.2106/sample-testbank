import { cellAnswerArray } from '../cellDefine'
import convertTextCell from '../convertTextCell'
import answerToArray from './answerToArray'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
function handlerAnswerSL1(objectRow: any, numSpace: any) {
  const arr_answer = []
  const correct_answers = []
  const length = cellAnswerArray.length
  const arrAnswerRaw = answerToArray(objectRow)

  if (numSpace !== arrAnswerRaw.filter((item: any) => Boolean(item)).length) {
    return {
      code: 0,
      message:
        'Vui lòng đảm bảo số lượng chỗ trống ____ trong Question Text phải đúng bằng số lượng cột Answer',
    }
  }
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 1)

  if (verifyCell.code == 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < length; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {
      const text = convertTextCell(objectRow[cellKey])
      const arr = text.split('|')
      let count = 0;
      if (arr.length < 2) {
        return {
          code: 0,
          message:
            'Vui lòng có ít nhất 2 đáp án, mỗi ô answer chứa các đáp án của 1 chỗ trống',
        }
      }
      for (const j in arr) {
        if (arr[j].startsWith('*')) {
          count++;
          arr[j] = arr[j]?.replace(/(\*|\#|\[])/g, '')
          correct_answers.push(arr[j])
        }else{
          arr[j] = arr[j]?.replace(/(\*|\#|\[])/g, '')
        }
        if(!arr[j]){
          return {code:0, message:'Đáp án không hợp lệ'}
        }
      }
      arr_answer.push(arr.join('*'))
      if (count != 1) {
        return {
          code: 0,
          message:
            'Vui lòng nhập dấu * trước 1 giá trị được cho là đáp án đúng',
        }
      }
      
    } else {
      break
    }
  }
  return {
    code: 1,
    arr_answer: arr_answer.join('#'),
    correct_answers: correct_answers.join('#'),
    lengthAnswer: arr_answer.length,
    lengthCorrectAnswer: correct_answers.length,
    message: '',
  }
}

export default handlerAnswerSL1
