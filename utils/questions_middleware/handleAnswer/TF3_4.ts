import { cellAnswerArray } from '../cellDefine'
import convertTextCell from '../convertTextCell'
import { verifyAnswerCellCount } from '../verifyRow/checkAnswer'
import answerToArray from './answerToArray'
import checkTFNi from './checkTFNi'

function handlerAnswerTF3_4(objectRow: any) {
  let arr_answer: any[] = []
  let arr_corret_answer: any[] = []
  const arr_answer_ex: any[] = []
  const arr_corret_answer_ex: any[] = []
  let total_question = 0
  let total_question_ex = 0
  const arr_index_ex = []
  let index_ans = -1
  const length = cellAnswerArray.length
  const arrAnswerRaw = answerToArray(objectRow)
  const verifyCell = verifyAnswerCellCount(arrAnswerRaw, 0)
  if (verifyCell.code === 0) {
    return { code: 0, message: verifyCell.message }
  }
  for (let i = 0; i < length; i++) {
    const cellKey = `${cellAnswerArray[i].key}`
    if (objectRow[cellKey]) {
      const text = convertTextCell(objectRow[cellKey])?.replace(
        /(\*|\#|\[])/g,
        '',
      )

      const arr = text.split('|')
      const length = arr.length
      if (length < 2) {
        return {
          code: 0,
          message: 'Vui lòng nhập |True, |False cho từng đáp án',
        }
      } else if (length > 3) {
        return { code: 0, message: 'Đáp án không đúng định dạng' }
      } else {
        let modeEx = false
        if (length == 3) {
          if (arr[2] && arr[2].trim().toLowerCase() == 'ex') {
            modeEx = true
          } else {
            return { code: 0, message: 'Nhập |ex sau đáp án của câu ví dụ' }
          }
        }
        arr[0] = arr[0].trim()
        arr[1] = arr[1].trim()
        if (arr[0] == '' || arr[1] == '') {
          return { code: 0, message: 'Đáp án không hợp lệ' }
        }

        //parse True/False
        if (arr[1].toLowerCase() === 'true') {
          arr[1] = 'T'
        }
        if (arr[1].toLowerCase() === 'false') {
          arr[1] = 'F'
        }

        index_ans += 1
        //example
        if (modeEx) {
          arr_answer_ex.push(`*${arr[0]}`) // * is start example
          arr_corret_answer_ex.push(arr[1])
          arr_index_ex.push(index_ans)
        } else {
          arr_answer.push(arr[0])
          arr_corret_answer.push(arr[1])
        }
      }
    } else {
      break
    }
  }

  // only 3 example
  if (arr_answer_ex.length > 3) {
    return { code: 0, message: 'Vui lòng chỉ nhập tối đa 3 ví dụ' }
  }
  // at least 1 question
  if (arr_answer.length == 0) {
    return { code: 0, message: 'Vui lòng nhập ít nhất 1 câu hỏi' }
  }
  total_question = arr_answer.length
  total_question_ex = arr_answer_ex.length
  arr_answer = [...arr_answer_ex, ...arr_answer]
  arr_corret_answer = [...arr_corret_answer_ex, ...arr_corret_answer]

  // verify true/false
  const constant = ['T', 'F']
  const ans = checkTFNi(constant, arr_corret_answer)
  if (ans.code === 0) {
    return { code: 0, message: ans.message }
  }
  return {
    code: 1,
    answer: arr_answer.join('#'),
    correct_answer: arr_corret_answer.join('#'),
    total_question: total_question,
    total_question_ex: total_question_ex,
    arr_index_ex: arr_index_ex,
    message: '',
  }
}

export default handlerAnswerTF3_4
