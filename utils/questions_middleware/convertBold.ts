export default function handlerBold(arr: any) {
  if(Array.isArray(arr)){
    let bold = false
    let str = ''
    const length = arr.length
    for (let i = 0; i < length; i++) {
      const item = arr[i]
      if (item.bold === true) {
        if (bold === true) {
          str += item.text
        } else {
          str += '%' + item.text
        }
        if (i === length - 1) {
          str += '%'
        }
      } else {
        if (bold === true) {
          str += '%' + item.text
        } else {
          str += item.text
        }
      }
      bold = item.bold
    }
    return str
  }else{
    return arr;
  }
}
