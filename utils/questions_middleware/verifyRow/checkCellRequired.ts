import {
  CERF_SELECTIONS,
  LEVEL_SELECTIONS,
  SKILLS_SELECTIONS,
  TYPES_SELECTIONS,
  FORMAT_SELECTIONS,
  PUBLISHER_SELECTIONS,
  TEST_TYPE_SELECTIONS,
} from '@/interfaces/struct'
import { query } from 'lib/db'

import { cellArrayObject } from '../cellDefine'
//kiểm tra các cell bắt buộc.
//vd: dataObject={A:"123", M:"abc"},  arr["A","B","C"]
export function requireCellQuestions(dataObject: any, arr: any[string]) {
  let strMessage = 'Vui lòng nhập đầy đủ thông tin cột: '
  const arrMessage: any = []
  const arrCellHasData: any = []
  const len = arr.length
  for (let i = 0; i < len; i++) {
    const key: string = arr[i]
    const cellObject = cellArrayObject.find((m) => m.key == key)
    if (!dataObject[key] || dataObject[key] == '') {
      arrMessage.push(cellObject.display)
    } else {
      arrCellHasData.push(key)
    }
  }
  if (arrMessage.length > 0) {
    strMessage = `${strMessage}${arrMessage.join(', ')}`
    return { code: 0, message: strMessage, arrCellHasData: arrCellHasData }
  }
  return { code: 1, message: '', arrCellHasData: arrCellHasData }
}
//Check verify value Level, CEFR, Skills có trùng với constant hay không
//arrData: arr được lấy từ constant, value: value được get từ file
//true: thỏa mãn, false: không thỏa
export async function verifyValueLCS(objectKey: string, value: string) {
  if (!value || value == '') return { code: 1, message: 'message' }
  let arrData: any = []
  let message = ''
  const listGrade = await query<any[]>(
    'SELECT id as code, name as display FROM grade where deleted =0 order by priority',
    [],
  )
  const listSeries = await query<any[]>(
    'SELECT id as code, name as display FROM series where deleted =0 order by priority',
    [],
  )
  switch (objectKey) {
    case 'cefr':
      arrData = CERF_SELECTIONS
      message = 'Thông tin chứng chỉ không đúng.'
      break
    case 'skill':
      arrData = SKILLS_SELECTIONS
      message = 'Thông tin kỹ năng không đúng.'
      break
    case 'level':
      arrData = LEVEL_SELECTIONS
      message = 'Thông tin độ khó không đúng.'
      break
    case 'publisher':
      arrData = PUBLISHER_SELECTIONS
      message = 'Thông tin nhà xuất bản không đúng.'
      break
    case 'test_type':
      arrData = TEST_TYPE_SELECTIONS
      message = 'Thông tin môn học không đúng.'
      break
    case 'series':
      arrData = listSeries
      message = 'Thông tin chương trình không đúng.'
      break
    case 'grade':
      arrData = listGrade
      message = 'Thông tin khối lớp không đúng.'
      break
    case 'format':
      arrData = FORMAT_SELECTIONS
      message = 'Thông tin tiêu chuẩn không đúng.'
      break
    case 'types':
      arrData = TYPES_SELECTIONS
      message = 'Thông tin kỳ thi không đúng.'
      break
    default:
      return { code: 0, message: 'key error' }
  }
  const index = arrData.findIndex(
    (item: any) => item?.code === value.toUpperCase(),
  )
  if (index !== -1) {
    return { code: 1, message: 'message' }
  } else {
    return { code: 0, message: message }
  }
}
