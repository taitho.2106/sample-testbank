import fs from 'fs'

//Kiểm tra chuổi các string của file có thỏa mãn đuôi file
//ex: arrextension = ['jpg', 'png'], format: Image
export function checkExistFile(folder:string, strArr:any[], format:string, arrExtension:any[string]){
    const strAllowExtension = arrExtension.join("| ")
    for(let i=0;i<strArr.length;i++){
        const fileName = strArr[i];
        const ext =
          fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length) ||
          fileName
          if (arrExtension.indexOf(ext) < 0) {
            return{code : 0, message:`Vui lòng nhập ${format} theo cấu trúc: Tên file + đuôi file (${strAllowExtension})`}
          }
    }
    for(let i=0;i<strArr.length;i++){
      const fileName = strArr[i];
      if (!fs.existsSync(`${folder}/${fileName}`)) {
        return{code : 0, message:`Không tìm thấy tập tin ${fileName}`}
      }
       
  }
    return {code :1, message:""}
}