

//kiểm tra thứ tự các answer và số câu trả lời tối thiểu.
export function verifyAnswerCellCount(arrAnswers:any[], minAnswer?:number){
    const len=arrAnswers.length;
    let i = len-1;
    let checkHasData = false;
    let countAnswer = 0;
    while(i>=0){
        if(arrAnswers[i]){
            checkHasData = true;
            countAnswer+=1;
        }else if(checkHasData == true){
           return {code: 0, message:"Vui lòng không để trống đáp án ô bên trái."}
        }
        i--;
    }
    if(minAnswer){
        if(countAnswer<minAnswer){
            return {code: 0, message:`Vui lòng có ít nhất ${minAnswer} đáp án, mỗi ô answer là 1 đáp án`}
        }
    }
    return {code: 1, message:""}
}

//kiểm tra điều kiện 1 ô đáp án chứa số lượng đáp án num
export function coupleAnswer(arrAnswers:any[],divisionCharacter:string,num:number){
    const len=arrAnswers.length;
    let i = 0;
    while(i<len){
        if(arrAnswers[i]){
            const itemArr =arrAnswers[i].split(divisionCharacter);
            if(itemArr.length<=num){
                return {code: 0, message:`Vui lòng nhập ${num} đáp án đúng cho mỗi ô Answer, mỗi đáp án phân cách bằng dấu ${divisionCharacter}`}
            }
        }
        i++;
    }
    
    return {code: 1, message:""}
}

