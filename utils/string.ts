import { useCallback } from "react";

export const formatDate = (date: string | number | Date, isGetTime = false) => {
  const d = new Date(date)
  let month = '' + (d.getMonth() + 1)
  let day = '' + d.getDate()
  const year = d.getFullYear()

  const hour = d.getHours()
  const minute = d.getMinutes()

  if (month.length < 2) month = '0' + month
  if (day.length < 2) day = '0' + day

  return (
    [day, month, year].join('/') +
    `${
      isGetTime
        ? ` ${hour < 10 ? `0${hour}` : hour}:${
            minute < 10 ? `0${minute}` : minute
          }`
        : ''
    }`
  )
}

export const yyyymmdd = (d: string | number | Date) => {
  const date = new Date(d)
  let mm: any = date.getMonth() + 1; // Months start at 0!
  let dd: any = date.getDate();
  const yyyy: any = date.getFullYear();
  if (dd < 10) dd = '0' + dd;
  if (mm < 10) mm = '0' + mm;

  return `${yyyy}-${mm}-${dd}`;
}

export const getNDayFromNow = (n: number) => {
  const today = new Date();
  today.setDate(today.getDate() - n);
  return today;
}

export const isEqualDate = (d1: any, d2: any) => {
  if (!d1 || !d2) {
      return false;
  }
  return (d1.getDate() === d2.getDate() && d1.getFullYear() === d2.getFullYear() && d1.getMonth() === d2.getMonth())
}




export const replaceTyphoStr = (str: string, typho: string) => {
  const tag = typho.replace(/\%/g, '')
  const splitArr = str.split(typho)
  let returnStr = ''
  splitArr.forEach((item: string, i: number) => {
    returnStr += i % 2 === 0 ? item : `<${tag}>${item}</${tag}>`
  })
  return returnStr
}
export const formatBytes=(bytes:number, decimals = 2) =>{
  if (bytes === 0) return '0 B';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

// %b%hello%b% => hello
export const getTextWithoutFontStyle = (str:any)=>{
  if(!str) return str;
 const text =  str.replace(/\%b\%/g, '')
            .replace(/\%u\%/g, '')
            .replace(/\%i\%/g, '')
            return text;
}


// %b%hello%b% => <b>hello</b>
export const formatHtmlText = (str:string) => {
  if(!str) return str
  const typho = ['%b%', '%i%', '%u%']
  let typhoData = str
  typho.forEach((item) => {
    typhoData = replaceTyphoStr(typhoData, item)
  })
  return typhoData
}

const convertHtmlToObject= ( text: string,
  bold?: boolean,
  underline?: boolean,
  italic?: boolean)=>{
    try{
      if (typeof text != "string") return text;
      if (!text) return [{ text, bold, underline, italic }];
      const leni = text.length;
      if (leni <= 3) return [{ text, bold, underline, italic }];
      let i = 0;
      const list: any = [];
      const typho = ["<b>", "<i>", "<u>"];
      const firstText: any = { bold, underline, italic };
      const secondText: any = { bold, underline, italic };
      const thirdText: any = { bold, underline, italic };
      while (i < leni - 2) {
        const textKey = `${text[i]}${text[i + 1]}${text[i + 2]}`;
        if (typho.indexOf(textKey) >= 0) {
            let indexEnd = i;
          if (textKey == "<b>") {
            secondText.bold = true;
           indexEnd = text.indexOf("</b>");
           
          }
          if (textKey == "<u>") {
            secondText.underline = true;
            indexEnd = text.indexOf("</u>");
          }
          if (textKey == "<i>") {
            secondText.italic = true;
             indexEnd = text.indexOf("</i>");
          }
          firstText.text = text.slice(0, i);
          const temp = text.slice(i + 3, leni);
          if (indexEnd >= 0) {
            secondText.text = text.slice(i+3, indexEnd);
            thirdText.text = text.slice(indexEnd + 4, leni);
          } else {
            secondText.text = temp;
          }
          if (firstText?.text) {
            list.push(
              ...convertHtmlToObject(firstText.text, bold, underline, italic)
            );
          }
          if (secondText?.text) {
            list.push(
              ...convertHtmlToObject(
                secondText.text,
                secondText.bold,
                secondText.underline,
                secondText.italic
              )
            );
          }
          if (thirdText?.text) {
            list.push(
              ...convertHtmlToObject(thirdText.text, bold, underline, italic)
            );
          }
          break;
        }
        i++;
      }
    
      if (i >= leni - 2) {
        return [{ text, bold, underline, italic }];
      } else {
        return list;
      }
    }catch{
      return text;
    }
  }
// text font to object
export const formatTextStyleObject = (
  text: string
) => {
  const html = convertStrToHtml(text)
  return convertHtmlToObject(html)
};
/*ex "nguyen %b%%u%van%u%%b% %i%a%i%" = > [
    {
      "text": "nguyen "
  },
  {
      "text": "van",
      "bold": true,
      "underline": true
  },
  {
      "text": " "
  },
  {
      "text": "a",
      "italic": true
  }
]
*/

//has dot => ex: abc.png => .png
export const getFileExtension = (fileName: string) => {
  if (fileName) {
    return (
      fileName.substring(fileName.lastIndexOf('.'), fileName.length) ||
      fileName
    )
  } else {
    return ''
  }
}

export const convertStrToHtml = (dataD: string) => {
  const matches = [...dataD?.matchAll(/(%b%|%i%|%u%)/g)]
  let result = dataD
  const styleIndex: any = {}
  for (const match of matches) {
    const sIndex = styleIndex[match[0]] ?? 0
      const tag = match[0].replaceAll('%', '')
      result = result.replace(
        match[0],
        sIndex % 2 === 0 ? `<${tag}>` : `</${tag}>`,
      )
      styleIndex[match[0]] = sIndex + 1
  }
  return result
}

export const removeVNCharacter = (str: string) => {
  if(!str) return str
  // return data.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/[đ]/g, 'd').replace(/[Đ]/g, 'D')
  str = str.normalize("NFC")
  str = str.toLowerCase();
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  return str;
}

export const addSpacingForString = (text: string, action: string | 'before' | 'after') => {
  text = text.trim()
  switch(action){
    case 'before':
      text = ` ${text}`
      break
    case 'after':
      text = `${text} `
      break
    default:
      text = ` ${text} `
  }
  return text
}

export const compareStringIgnoreCase = (a: string, b: string) => a.toLowerCase() === b.toLowerCase();
export const convertHtmlToStr = (dataD: ChildNode[]) => {
  let str = "";
  for (let i = 0, len = dataD.length; i < len; i++) {
    const child = dataD[i] as Node;
    if (child.nodeName === "#text") {
      str += child.textContent;
    } else if (child.nodeName === "SPAN") {
      str += child.firstChild.textContent;
    } else if (child.nodeName === "BR") {
      // str += '\n'
    } else if (child.nodeName === "INPUT") {
      const inputEl = child as HTMLInputElement;
      str += `#${inputEl.value}#`;
    } else {
      const el = child as Element;
      const tag = child.nodeName.toLowerCase();
      const content = convertHtmlToStr(Array.from(el.childNodes));
      if (content !== "") {
        str += `%${tag}%${content}%${tag}%`;
      }
    }
  }
  return str;
};

export const onPaste = (event: any) => {
  event.preventDefault();
  let data = event.clipboardData.getData("text").replace(/(\#|\*)/g, "");
  data = data?.replace(/\r?\n|\r/g, "");
  const selection = window.getSelection();
  const range = selection.getRangeAt(0);
  range.deleteContents();
  const node = document.createTextNode(data);
  range.insertNode(node);
  selection.removeAllRanges();
  const newRange = document.createRange();
  newRange.setStart(node, data.length);
  selection.addRange(newRange);
};

type PropType = {
  classElement: string
  type: "Bold" | "Underline" | "Italic"
  groupKey?: number
  action?: (position:number, index: number, text: string) => void
  tablePosition?: number
}
export const onType = ({ classElement = "div-input", type, groupKey, action, tablePosition = 0}: PropType) => {
  const selection = window.getSelection();
  if (selection.rangeCount === 0) return;
  const range = selection.getRangeAt(0);
  const parent = range.commonAncestorContainer;
  let el: HTMLElement = null;
  let parentEl =
      parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement);
  while (parentEl.parentElement) {
    if (parentEl.classList.contains(classElement)) {
      el = parentEl;
      break;
    }
    parentEl = parentEl.parentElement;
  }
  if (el) {
    const position = Number(el.getAttribute('aria-colindex')) || 0
    if (tablePosition === position) {
      document.execCommand(type, false, null);
    }

    if (action) {
      const index = parseInt(el.id.replace(`${groupKey}_`, ""));
      let text = "";
      for (let i = 0, len = el.childNodes.length; i < len; i++) {
        const childNode = el.childNodes[i];
        text += convertHtmlToStr([childNode]);
      }
      action(position, index, text);
    }
  }
};