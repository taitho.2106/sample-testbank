import log4js from 'log4js';
import { logDir } from '@/constants/index'

log4js.configure({
    appenders: {
        daily: {
            type: 'dateFile',
            filename: `${logDir}/vnpay_ipn`,
            pattern: 'yyyy-MM-dd.log',
            alwaysIncludePattern: true,
        }
    },
    categories: {
        default: { appenders: ['daily'], level: 'INFO' },
        vnpay_ipn: { appenders: ['daily'], level: 'INFO' },
    }
});

export { log4js };