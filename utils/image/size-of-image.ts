import fs from 'fs'

import sizeOf from 'image-size'

export const sizeOfImage = (imgUrl: string) =>{
    try{
      return sizeOf(fs.readFileSync(imgUrl))
    }catch(err){
      throw err
    }
}