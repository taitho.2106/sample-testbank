export default function sortExampleQuestionMC2(formData: any) {
  try {
    if (!formData) return formData
    const data = { ...formData }
    const listQuestionText = data?.question_text?.split('#')
    const listAnswer = data?.answers.split('#')
    const listCorrect = data?.correct_answers.split('#')
    const questionsExample = []
    const answersExample: any = []
    const answerCorrectsExample: any = []
    const questions = []
    const answers: any = []
    const answerCorrects: any = []
    for (let i = 0; i < listQuestionText.length; i++) {
      if (listQuestionText[i].startsWith('*')) {
        questionsExample.push(listQuestionText[i])
        answersExample.push(listAnswer[i])
        answerCorrectsExample.push(listCorrect[i])
      } else {
        questions.push(listQuestionText[i])
        answers.push(listAnswer[i])
        answerCorrects.push(listCorrect[i])
      }
    }
    data.answers = [...answersExample, ...answers].join('#')
    data.correct_answers = [...answerCorrectsExample, ...answerCorrects].join(
      '#',
    )
    data.question_text = [...questionsExample, ...questions].join('#')
    return data
  } catch(e) {
    console.log("sortExampleQuestionMC2---",e)
    return formData
  }
}
