export default function sortExampleQuestionTF5(formData: any) {
    try {
        if (!formData) return formData
        const data = { ...formData }
        const isNotExamArr = data.isNotExam.split('#') ?? []
        const answersArr = data?.answers.split('#') ?? []
        const imageArr = data.image.split('#') ?? []
        const correctArr = data.correct_answers.split('#') ?? []
        const listFileImage = data.image_files ?? []

        const answersEx:any = []
        const imageEx:any = []
        const correct_answersEx:any = []
        const listFileEx:any = []

        const answersAns:any = []
        const imageAns:any = []
        const correct_answersAns:any = []
        const listFileAns:any = []

        isNotExamArr.forEach((item:string, mIndex:number) => {
            if(item === 'false'){
                answersEx.push(answersArr[mIndex])
                imageEx.push(imageArr[mIndex])
                correct_answersEx.push(correctArr[mIndex])
                listFileEx.push(listFileImage[mIndex])
            }else{
                answersAns.push(answersArr[mIndex])
                imageAns.push(imageArr[mIndex])
                correct_answersAns.push(correctArr[mIndex])
                listFileAns.push(listFileImage[mIndex])
            }
        });
        data.answers = [...answersEx,...answersAns].join('#')
        data.image = [...imageEx,...imageAns].join('#')
        data.correct_answers = [...correct_answersEx,...correct_answersAns].join('#')
        data.image_files = [...listFileEx,...listFileAns]
        // console.log("data=====be===",data)
        return data
      } catch {
        return formData
      }
}


  