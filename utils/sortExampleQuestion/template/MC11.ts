export default function sortExampleQuestionMC11(data: any) {
    try {
        const answerGroup = data.answers.split('#')
        const correctGroup = data.correct_answers.split('#')
        const filesGroup = data.mc11_image_files
        const exampleLength = answerGroup.filter((item:string) => item.startsWith('ex'))?.length ?? 0
        if( exampleLength < 1 || answerGroup.length != correctGroup.length ) return data

        const exampleContainer: any = { answers: [], correct_answers: [], files: [] }
        const questionContainer: any = { answers: [], correct_answers: [], files: [] }

        answerGroup.forEach((value: string, index: number) => {
            if( value.startsWith('ex') ){
                exampleContainer.answers.push(answerGroup[index])
                exampleContainer.correct_answers.push(correctGroup[index])
                exampleContainer.files.push(filesGroup[index])
            }else{
                questionContainer.answers.push(answerGroup[index])
                questionContainer.correct_answers.push(correctGroup[index])
                questionContainer.files.push(filesGroup[index])
            }
        })

        data.answers = [ ...exampleContainer.answers, ...questionContainer.answers].join('#')
        data.correct_answers = [ ...exampleContainer.correct_answers, ...questionContainer.correct_answers].join('#')
        data.mc11_image_files = [ ...exampleContainer.files, ...questionContainer.files]

        return data
    } catch(error){
        console.log('Error comes from MG11 Sorting', error)
        return data
    }
}
  