export default function sortExampleQuestionMC7(formData: any) {
    try {
        if (!formData) return formData
        const data = { ...formData }
        console.log("data==MC7==",data);
        const listQuestionText = data?.question_text?.split('#')
        const listAnswer = data?.answers.split('#')
        const listCorrect = data?.correct_answers.split('#')
        const listImageFile = data?.mc7_image_files || []
        const questionsExample = []
        const answersExample: any = []
        const answerCorrectsExample: any = []
        const listFileImageExample: any[] = []
        const questions = []
        const answers: any = []
        const answerCorrects: any = []
        const listFileImage: any[] = []
        for (let i = 0; i < listQuestionText.length; i++) {
          if (listQuestionText[i].startsWith('*')) {
            questionsExample.push(listQuestionText[i])
            answersExample.push(listAnswer[i])
            answerCorrectsExample.push(listCorrect[i])
            listFileImageExample.push(listImageFile[i])
          } else {
            questions.push(listQuestionText[i])
            answers.push(listAnswer[i])
            answerCorrects.push(listCorrect[i])
            listFileImage.push(listImageFile[i])
          }
        }
        data.answers = [...answersExample, ...answers].join('#')
        data.correct_answers = [...answerCorrectsExample, ...answerCorrects].join('#')
        data.question_text = [...questionsExample, ...questions].join('#')
        data.mc7_image_files = [...listFileImageExample,...listFileImage]
        return data
      } catch {
        return formData
      }
  }
  