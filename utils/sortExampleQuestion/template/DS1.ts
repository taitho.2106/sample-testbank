export default function sortExampleQuestionDS1(formData: any) {
    try {
      if (!formData) return formData
      const data = { ...formData }
      const listAnswer = data?.answers.split('#')
      const indexExample = listAnswer.findIndex((m:any) => m.startsWith("ex|"))
      if (indexExample != -1) {
        const itemExample = listAnswer[indexExample]
        listAnswer.splice(indexExample, 1)
        listAnswer.splice(0, 0, itemExample)
      }
      data.answers =listAnswer.join('#')
      return data
    } catch(e) {
      console.log("sortExampleQuestionDS1---",e)
      return formData
    }
  }
  