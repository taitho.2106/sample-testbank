export default function sortExampleQuestionMG6(formData: any) {
    try {
        const question = formData;
        const leftRights = question.answers?.split('#') || [] // tách left và right hiển thị
        const corrects = question.correct_answers.split('#') || [] // thứ tự đáp án đúng theo left
        const left = leftRights[0]?.split('*')|| [] //
        const listImageFileLeft = question?.mg6_images_left || []
        const listImageFileRight = question?.mg6_images_right || []
        const modeQuestion = leftRights[2]?.split("*") || []
        if(modeQuestion.length === 0) return question; 
        const right = leftRights[1]?.split('*') || []
        const example:any = {
            leftR:[],
            rightR: [],
            corrects:[],
            fileLeft:[],
            index:[],
            modeQuestion:[]
        }
        const mainQuestion:any = {
            leftR:[],
            rightR: [],
            corrects:[],
            fileLeft:[],
            index:[],
            modeQuestion:[]
        }
        const maxLength = Math.max(left.length,right.length, corrects.length)
        // const randomIndexs = Array.from({ length:  }, (v, i) => i)
        for (let index = 0; index < maxLength;index++) {
            if(!modeQuestion || !modeQuestion[index] || modeQuestion[index] != "ex"){
                if(right.length > index){
                    mainQuestion.rightR.push(right[index])
                }
                if(left.length > index){
                    mainQuestion.modeQuestion.push("")
                    mainQuestion.leftR.push(left[index])
                }
                if(corrects.length > index){
                    mainQuestion.corrects.push(corrects[index])
                }
                if(listImageFileLeft.length > index){
                    mainQuestion.fileLeft.push(listImageFileLeft[index])
                }
                if(listImageFileRight.length > index){
                    mainQuestion.index.push(index)
                }
            } else{
                if(right.length > index){
                    example.rightR.push(right[index])
                }
                if(left.length > index){
                    example.modeQuestion.push("ex")
                    example.leftR.push(left[index])
                }
                if(corrects.length > index){
                    example.corrects.push(corrects[index])
                }
                if(listImageFileLeft.length > index){
                    example.fileLeft.push(listImageFileLeft[index])
                }
                if(listImageFileRight.length > index){
                    example.index.push(index)
                }
            }
        }
        
        let arrRightR:any[] = [];
        let arrImageRight:any[] = []
        example.index.forEach((indexLeft:number) => {
            const indexRight = right.findIndex((m:string)=>m == corrects[indexLeft])
            arrRightR.push(right[indexRight])
            right.splice(indexRight,1)
            arrImageRight.push(listImageFileRight[indexRight])
            listImageFileRight.splice(indexRight, 1)
        });
        arrImageRight= [...arrImageRight,...listImageFileRight]
        arrRightR= [...arrRightR,...right]
        const leftR = `${[...example.leftR,...mainQuestion.leftR].join("*")}`
        const rightR = `${arrRightR.join("*")}`
        const modeQuestionR = example.leftR.length==0?"":`${[...example.modeQuestion,...mainQuestion.modeQuestion].join("*")}`
        const strAnswer = `${leftR}#${rightR}${modeQuestionR?`#${modeQuestionR}`:""}`
        const strCorrect = `${[...example.corrects,...mainQuestion.corrects].join("#")}`
        const arrImageLeft = (example.fileLeft.length >0 || mainQuestion.fileLeft.length>0)?[...example.fileLeft,... mainQuestion.fileLeft]:[]
        return {
            ...question,
            answers: strAnswer,
            correct_answers: strCorrect,
            mg6_images_left:arrImageLeft.length>0?[...arrImageLeft]:null,
            mg6_images_right:arrImageRight.length>0?[...arrImageRight]:null
        }
    } catch (err) {
        console.log('sortExampleQuestionMG6--', err)
        return formData
    }
}  