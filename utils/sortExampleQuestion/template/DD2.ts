export default function sortExampleQuestionDD2(formData: any) {
    try {
        if (!formData) return formData
        const data = { ...formData }
        const isNotExamArr = data.isNotExam.split('#') ?? []
        const imageArr = data.image.split('#') ?? []
        const correctArr = data.correct_answers.split('#') ?? []
        const listFileImage = data.image_files ?? []

        const imageEx:any = []
        const correct_answersEx:any = []
        const listFileEx:any = []

        const imageAns:any = []
        const correct_answersAns:any = []
        const listFileAns:any = []

        isNotExamArr.forEach((item:string, mIndex:number) => {
            if(item === 'false'){
                imageEx.push(imageArr[mIndex])
                correct_answersEx.push(correctArr[mIndex])
                listFileEx.push(listFileImage[mIndex])
            } else{
                imageAns.push(imageArr[mIndex])
                correct_answersAns.push(correctArr[mIndex])
                listFileAns.push(listFileImage[mIndex])
            }
        });
        data.image = [...imageEx,...imageAns].join('#')
        data.correct_answers = [...correct_answersEx,...correct_answersAns].join('#')
        data.image_files = [...listFileEx,...listFileAns]
        return data
    } catch {
        return formData
    }
}