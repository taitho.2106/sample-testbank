export default function sortExampleQuestionMG5(data: any) {
  try {
    const question = data;
    const leftRights = question.answers?.split('#') ?? [] //tách left và right hiển thị
    const corrects = question.correct_answers.split('#') ?? [] // thứ tự đáp án đúng theo left
    const left = leftRights[0]?.split('*') ?? [] 
    const right = leftRights[1]?.split('*') ?? []
    const dataImage = question?.image?.split('#') ?? [] 
    const dataImageFiles = question?.image_files ?? [] 
    const modeQuestion = leftRights[2]?.split("*") ?? []
    if(modeQuestion.length === 0) return question;
    const example:any = {
        leftR:{
          text:[],
          image:[],
          file_image:[]
        },
        rightR: [],
        corrects:[],
        modeQuestion:[],
    }
    const mainQuestion:any = {
        leftR:{
          text:[],
          image:[],
          file_image:[]
        },
        rightR: [],
        corrects:[],
        modeQuestion:[],
    }
    const maxLength = Math.max(left.length,right.length, corrects.length)
    
    for (let index = 0; index < maxLength;index++) {
        if(!modeQuestion || !modeQuestion[index] || modeQuestion[index] !="ex"){
            if (right.length > index) {
                mainQuestion.rightR.push(right[index])
              }
              if (left.length > index) {
                mainQuestion.modeQuestion.push("")
                mainQuestion.leftR.text.push(left[index])
                mainQuestion.leftR.image.push(dataImage[index])
                mainQuestion.leftR.file_image.push(dataImageFiles[index])
              }
              if(corrects.length> index){
                mainQuestion.corrects.push(corrects[index])
              }
        }else{
            if (right.length > index) {
                example.rightR.push(right[index])
              }
              if (left.length > index) {
                example.modeQuestion.push("ex")
                example.leftR.text.push(left[index])
                example.leftR.image.push(dataImage[index])
                example.leftR.file_image.push(dataImageFiles[index])
              }
              if(corrects.length> index){
                example.corrects.push(corrects[index])
              }
        }
    }
    let arrRightR = [...example.rightR,...mainQuestion.rightR];
    
    if(example.modeQuestion && example.modeQuestion.length>0 ){
      for(let i=0; i<example.corrects.length; i++){
        const indexRight = arrRightR.indexOf(example.corrects[i]);
        if(indexRight != -1){
          arrRightR.splice(indexRight, 1)
        }
      }
      arrRightR = [...example.corrects,...arrRightR]
    }

    const leftR = `${[...example.leftR.text,...mainQuestion.leftR.text].join("*")}`
    const rightR = `${arrRightR.join("*")}`
    const modeQuestionR = example.leftR.length==0?"":`${[...example.modeQuestion,...mainQuestion.modeQuestion].join("*")}`
    const strAnswer = `${leftR}#${rightR}${modeQuestionR?`#${modeQuestionR}`:""}`
    const strCorrect = `${[...example.corrects,...mainQuestion.corrects].join("#")}`
    const imageArr = [...example.leftR.image,...mainQuestion.leftR.image]
    const image_files = [...example.leftR.file_image,...mainQuestion.leftR.file_image]
    return {
      ...question,
      answers: strAnswer,
      correct_answers: strCorrect,
      image: imageArr.join('#'),
      image_files: image_files

    }
  } catch (err) {
    console.log('sortExampleQuestionMG5--', err)
    return data
  }
}
  