export default function sortExampleQuestionDD1(formData: any) {
    try {
      if (!formData) return formData
      const data = { ...formData }
      const listQuestionText = data?.question_text?.split('#')
      const listAnswer = data?.answers.split('#')
      const listCorrect = data?.correct_answers.split('#')
      const listImage = data?.image.split('#')
      const listFile = data?.image_files
      const questionsExample = []
      const answersExample: any = []
      const answerCorrectsExample: any = []
      const imageExample:any = []
      const listFileExample:any = []
      const questions = []
      const answers: any = []
      const answerCorrects: any = []
      const image:any = []
      const listFileQuestion : any = []
      for (let i = 0; i < listQuestionText.length; i++) {
        if (listQuestionText[i] === '*') {
          questionsExample.push(listQuestionText[i])
          answersExample.push(listAnswer[i])
          answerCorrectsExample.push(listCorrect[i])
          imageExample.push(listImage[i])
          listFileExample.push(listFile[i])
        } else {
          questions.push(listQuestionText[i])
          answers.push(listAnswer[i])
          answerCorrects.push(listCorrect[i])
          image.push(listImage[i])
          listFileQuestion.push(listFile[i])
        }
      }
      data.answers = [...answersExample, ...answers].join('#')
      data.correct_answers = [...answerCorrectsExample, ...answerCorrects].join('#')
      data.question_text = [...questionsExample, ...questions].join('#')
      data.image = [...imageExample,...image].join('#')
      data.image_files = [...listFileExample,...listFileQuestion]
      return data;
    } catch(e) {
      return formData
    }
  }
  