export default function sortExampleQuestionMG4(formData: any) {
    try {
        const question = formData;
        const leftRights = question.answers?.split('#')||[] //tách left và right hiển thị
        const corrects = question.correct_answers.split('#')||[] // thứ tự đáp án đúng theo left
        const left = leftRights[0]?.split('*')||[] //
        const modeQuestion = leftRights[2]?.split("*")||[]
        if(modeQuestion.length==0) return question;
        const right = leftRights[1]?.split('*')||[]
        const example:any = {
            leftR:[],
            rightR: [],
            corrects:[],
            modeQuestion:[]
        }
        const mainQuestion:any = {
            leftR:[],
            rightR: [],
            corrects:[],
            modeQuestion:[]
        }
        const maxLength = Math.max(left.length,right.length, corrects.length)
        // const randomIndexs = Array.from({ length:  }, (v, i) => i)
        for (let index = 0; index < maxLength;index++) {
            if(!modeQuestion || !modeQuestion[index] || modeQuestion[index] !="ex"){
                if (right.length > index) {
                    mainQuestion.rightR.push(right[index])
                  }
                  if (left.length > index) {
                    mainQuestion.modeQuestion.push("")
                    mainQuestion.leftR.push(left[index])
                  }
                  if(corrects.length> index){
                    mainQuestion.corrects.push(corrects[index])
                  }
            }else{
                if (right.length > index) {
                    example.rightR.push(right[index])
                  }
                  if (left.length > index) {
                    example.modeQuestion.push("ex")
                    example.leftR.push(left[index])
                  }
                  if(corrects.length> index){
                    example.corrects.push(corrects[index])
                  }
            }
        }
        let arrRightR = [...example.rightR,...mainQuestion.rightR];
        
        if(example.modeQuestion && example.modeQuestion.length>0 ){
          for(let i=0; i<example.corrects.length; i++){
            const indexRight = arrRightR.indexOf(example.corrects[i]);
            if(indexRight != -1){
              arrRightR.splice(indexRight, 1)
            }
          }
          arrRightR = [...example.corrects,...arrRightR]
        }

        const leftR = `${[...example.leftR,...mainQuestion.leftR].join("*")}`
        const rightR = `${arrRightR.join("*")}`
        const modeQuestionR = example.leftR.length==0?"":`${[...example.modeQuestion,...mainQuestion.modeQuestion].join("*")}`
        const strAnswer = `${leftR}#${rightR}${modeQuestionR?`#${modeQuestionR}`:""}`
        const strCorrect = `${[...example.corrects,...mainQuestion.corrects].join("#")}`
        return {
          ...question,
          answers: strAnswer,
          correct_answers: strCorrect,
        }
      } catch (err) {
        console.log('sortExampleQuestionMG4--', err)
        return formData
      }
  }
  