export default function sortExampleQuestionMC9(formData: any) {
  try {
    if (!formData) return formData
    const data = { ...formData }
    const listQuestionText = data?.question_text?.split('#')
    const listAnswer = data?.answers.split('#')
    const listCorrect = data?.correct_answers.split('#')
    const listImages = data.image.split("#")||[]
    const listImageFiles = data.image_files?[...data.image_files]:[]


    const questionsExample: any[] = []
    const answersExample: any[] = []
    const answerCorrectsExample: any[] = []
    const imagesExample: any[] = []
    const imageFilesExample: any[] = []

    const questions = []
    const answers: any[] = []
    const answerCorrects: any[] = []
    const images: any[] = []
    const imageFiles: any[] = []

    for (let i = 0; i < listQuestionText.length; i++) {
      if (listQuestionText[i].startsWith('*')) {
        questionsExample.push(listQuestionText[i])
        answersExample.push(listAnswer[i])
        answerCorrectsExample.push(listCorrect[i])
        imagesExample.push(listImages[i])
        if(listImageFiles.length>i){
          imageFilesExample.push(listImageFiles[i])
        }
      } else {
        questions.push(listQuestionText[i])
        answers.push(listAnswer[i])
        answerCorrects.push(listCorrect[i])
        images.push(listImages[i])
        if(listImageFiles.length>i){
          imageFiles.push(listImageFiles[i])
        }
      }
    }
    data.answers = [...answersExample, ...answers].join('#')
    data.correct_answers = [...answerCorrectsExample, ...answerCorrects].join(
      '#',
    )
    data.question_text = [...questionsExample, ...questions].join('#')
    data.image = [...imagesExample, ...images].join('#')
    data.image_files = [...imageFilesExample,...imageFiles]
    return data
  } catch(e) {
    console.log("sortExampleQuestionMC9---",e)
    return formData
  }
}
