import sortExampleQuestionDD1 from './template/DD1'
import sortExampleQuestionDD2 from './template/DD2'
import sortExampleQuestionDS1 from './template/DS1'
import sortExampleQuestionMC1 from './template/MC1'
import sortExampleQuestionMC10 from './template/MC10'
import sortExampleQuestionMC2 from './template/MC2'
import sortExampleQuestionMC7 from './template/MC7'
import sortExampleQuestionMC8 from './template/MC8'
import sortExampleQuestionMC9 from './template/MC9'
import sortExampleQuestionMC11 from './template/MC11'
import sortExampleQuestionMG4 from './template/MG4'
import sortExampleQuestionMG5 from './template/MG5'
import sortExampleQuestionMG6 from './template/MG6'
import sortExampleQuestionTF5 from './template/TF5'
const SortExampleQuestion = {
    sortExampleQuestionMC1,
    sortExampleQuestionMC2,
    sortExampleQuestionDD1,
    sortExampleQuestionDD2,
    sortExampleQuestionMC7,
    sortExampleQuestionMC8,
    sortExampleQuestionMC9,
    sortExampleQuestionMC10,
    sortExampleQuestionMG4,
    sortExampleQuestionTF5,
    sortExampleQuestionMG5,
    sortExampleQuestionDS1,
    sortExampleQuestionMG6,
    sortExampleQuestionMC11
}
export default SortExampleQuestion