import fs from 'fs'
import unzipper from 'unzipper'
import Excel, { ValueType } from 'exceljs';
import dayjs from 'dayjs';

const exist = function (path: fs.PathLike): Promise<fs.Stats> {
  return new Promise((resolve, reject) => {
    fs.stat(path, (err, stat) => {
      if (err) reject(err)
      else resolve(stat)
    })
  })
}

const createDir = function (
  path: fs.PathLike,
  options?: fs.MakeDirectoryOptions & { recursive: true },
): Promise<string> {
  return new Promise((resolve, reject) => {
    fs.mkdir(path, options, (err, pathDir) => {
      if (err) reject(err)
      else resolve(pathDir)
    })
  })
}

const remove = function (path: fs.PathLike): Promise<boolean> {
  return new Promise((resolve, reject) => {
    fs.unlink(path, (err) => {
      if (err) reject(err)
      else resolve(true)
    })
  })
}

const readFile = function (
  path: fs.PathLike | number,
  options?:
    | { encoding?: null | undefined; flag?: string | undefined }
    | undefined
    | null,
): Promise<Buffer> {
  return new Promise((resolve, reject) => {
    fs.readFile(path, options, (err, data) => {
      if (err) reject(err)
      else resolve(data)
    })
  })
}

const writeFile = function (
  path: fs.PathLike | number,
  data: string | NodeJS.ArrayBufferView,
): Promise<boolean> {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data, (err) => {
      if (err) reject(err)
      else resolve(true)
    })
  })
}
const writeFileBase64 = function (
  path: fs.PathLike | number,
  data: string,
): Promise<boolean> {
  return new Promise((resolve, reject) => {
    fs.writeFile(path, data,'base64', (err) => {
      if (err) reject(err)
      else resolve(true)
    })
  })
}
const unzip = function (path: string, destination: string): Promise<boolean> {
  return new Promise((resolve, reject) => {
    fs.createReadStream(path)
      .pipe(unzipper.Extract({ path: destination }))
      .on('error', reject)
      .promise()
      .then(()=>{
        resolve(true)
      })
  })
}

const readExcelFile = async (file: any, keys: string[], excludeFirstRow: boolean = true) => {
    const workbook = new Excel.Workbook();
    await workbook.xlsx.readFile(file.filepath);

    const data: any = [];
    const keyLength = keys.length;
    workbook.getWorksheet(1).eachRow((row, index) => {
        if (index > +excludeFirstRow) {
            const obj: any = {};
            let invalidCelValueCount = 0;

            for (let i = 0; i < keyLength; i++) {
                const cel = row.getCell(i + 1);
                let celValue;
                if (cel.type === ValueType.Date) {
                    celValue = dayjs(cel.value as Date).format('DD/MM/YYYY');
                } else {
                    celValue = cel.text;
                }

                if (!!celValue) {
                    obj[keys[i]] = celValue;
                } else {
                    invalidCelValueCount++;
                }
            }

            if (invalidCelValueCount !== keyLength) {
                data.push(obj);
            }
        }
    });

    return data;
}

export default { exist, createDir, remove, readFile, writeFile, unzip, writeFileBase64, readExcelFile }
