import { unitTestTypeInternationalOptions } from '../../constants'

export const filterCertificate = (parentCode?: any) => {
    const filteredOptions = unitTestTypeInternationalOptions.find((option: any) => option.code === parentCode)

    const results: any[] = []
    if (filteredOptions) {
        if (filteredOptions?.childrens) {
            filteredOptions.childrens.forEach((group) => {
                const groupName = group.groupName
                const groupOrder = group.groupOrder
                group.options.forEach((option) => {
                    results.push({
                        code: option.value,
                        display: option.label,
                        // imageUrl: option.image,
                        groupName: groupName,
                        groupOrder: groupOrder,
                    })
                })
            })
        }
        else {
            filteredOptions.options.forEach((option) => {
                results.push({
                    code: option.value,
                    display: option.label,
                    // imageUrl: option.image,
                });
            })
        }
    }
    return results
}

export const filterInternationalType = () => {
    const results: any[] = []
    unitTestTypeInternationalOptions.forEach((group) => {
        results.push({
            code: group.code,
            display: group.groupName,
        })
    })
    return results
}

export const getDefaultCertificate = (inputCode?: any) => {
    if (!inputCode) {
        return
    }
    const option = unitTestTypeInternationalOptions.find((group) =>
        (group.childrens || []).some((child) =>
            (child.options || []).some((opt) => opt.value === inputCode)
        ) || (group.options || []).some((opt) => opt.value === inputCode)
    )
    return option?.code
}