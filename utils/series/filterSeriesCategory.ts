import { unitTestTypeOptions } from '../../constants'

export const filterSeriesCategory = (t?:any) => {
    const results: any[] = []
    unitTestTypeOptions.forEach((group) => {
        const groupName = group.groupName
        const groupOrder = group.groupOrder
        group.options.forEach((option) => {
            results.push({
                code: option.value,
                display: t('unit-test-group')[option.label],
                groupName: t('unit-test-group-name')[groupName],
                groupOrder: groupOrder,
            })
        })
    })
    return results
}