import { GradeStructType, SeriesStructType } from '@/interfaces/types'

export const filterSeriesByGradeAndType = (series: SeriesStructType[], grades: GradeStructType[], gradeId: any, typeGroupId?: any) => {
    try {
        if (!series || series.length == 0) return []
        let results: any[] = []
        const grade = grades.find((m) => m.code == gradeId)
        if (!grade) {
            results = series
                .filter((m) => m.grade_id && m.code != 1)
                .map((m) => {
                    return {
                        code: m.code,
                        display: m.display,
                        imageUrl: m.image,
                    }
                })
        } else {
            results = series
                .filter((m) => m.grade_id == gradeId && m.code != 1 && m.group === typeGroupId)
                .map((m) => {
                    return {
                        code: m.code,
                        display: m.display,
                        imageUrl: m.image,
                    }
                })
        }
        return results
    } catch (err) {
        console.log('filterSeriesByGradeAndType---', err)
    }
}

export const getDefaultUnitTestGroup = (seriesId: any, series: SeriesStructType[]) => {
    if (series && series.length > 0) {
        const seriesSelect = series.find(s => s.code === seriesId);
        if (seriesSelect) {
            return seriesSelect.group
        }
    }
    return null
}