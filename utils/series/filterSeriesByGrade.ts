import { GradeStructType, SeriesStructType } from '@/interfaces/types'

export const filterSeriesByGrade = (series: SeriesStructType[], grades: GradeStructType[], gradeId: any) => {
  try {
    if (!series || series.length == 0) return []
    const resultDefault = [{ code: 1, groupName:'others', display: 'undefined', groupOrder: 4 }]
    let results: any[] = []
    const grade = grades.find((m) => m.code == gradeId)
    if (!grade) {
      results = series
        .filter((m) => m.grade_id && m.code != 1)
        .map((m) => {
          // const isTextBook = m.group == 1
          const nameTextBook = m.group && m.group === 1 ? 'textbook' : m.group === 2 ? 'supplement-book' : 'international-exam'
          return {
            code: m.code,
            display: m.display,
            groupName: nameTextBook,
            imageUrl: m.image,
            groupOrder: m.group,
          }
        })
    } else {
      results = series
        .filter((m) => m.grade_id == gradeId && m.code != 1)
        .map((m) => {
           const nameTextBook = m.group && m.group === 1 ? 'textbook' : m.group === 2 ? 'supplement-book' : 'international-exam'
          return {
            code: m.code,
            display: m.display,
            groupName: nameTextBook,
            imageUrl: m.image,
            groupOrder: m.group,
          }
        })
    }
    if (results.length > 1) {
      results.sort((s1, s2) => sortSeries(s1, s2))
    }
    if (resultDefault.length > 0) {
      results.push(...resultDefault)
    }
    return results
  } catch (err) {
    console.log('filterSeriesByGrade---', err)
  }
}
const sortSeries = (s1: any, s2: any) => {
  if (s1.groupOrder != s2.groupOrder) {
    return s1.groupOrder - s2.groupOrder
  }
 return s1.display?.localeCompare(s2.display)
}
