import { NextApiRequest } from "next";
import formidable from "formidable";
import dayjs from "dayjs";

export const getFormDataFromRequest = (req: NextApiRequest, multiples: boolean = true) => {
    const form = formidable({ multiples });
    const formData = new Promise((resolve, reject) => {
        form.parse(req, async (err, fields, files) => {
            if (err) {
                reject("error");
            }
            resolve({ fields, files });
        });
    });

    return formData;
}

export const fillEmptyData = (sourceData: any[], startDate: Date, endDate: Date, fillData: any, key = 'date') => {
    const sourceObj = sourceData.reduce((rs, cur) => ({ ...rs, [cur[key]]: cur }), {});

    let start = dayjs(startDate);
    const end = dayjs(endDate);

    while (!start.isAfter(end)) {
        const dayString = start.format('DD/MM/YYYY')

        if (!sourceObj[dayString]) {
            sourceObj[dayString] = {
                ...fillData,
                [key]: dayString,
            }
        }

        start = start.add(1, 'day');
    }

    return Object.values(sourceObj);
}