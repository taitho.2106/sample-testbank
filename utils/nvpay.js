import crypto from 'crypto';

export const createSign = (signData) => {
    let hmac = crypto.createHmac("sha512", process.env.NEXT_PUBLIC_VNP_SECRET_KEY);
    let signed = hmac.update(new Buffer(signData, 'utf-8')).digest("hex");

    return signed;
}

export const sortObject = (obj) => {
    let sorted = {};
    let str = [];
    let key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) {
            str.push(encodeURIComponent(key));
        }
    }
    str.sort();
    for (key = 0; key < str.length; key++) {
        sorted[str[key]] = encodeURIComponent(obj[str[key]]).replace(/%20/g, "+");
    }
    return sorted;
}