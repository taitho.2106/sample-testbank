import dayjs from "dayjs";
import isBetween from "dayjs/plugin/isBetween";
dayjs.extend(isBetween)

import { PACKAGES, PACKAGES_FOR_STUDENT, STATUS_CODE_EXAM, USER_ROLES } from "@/interfaces/constants";

import { UNIT_TEST_TYPE } from "../constants";

export const checkArray = (arr: any, isCheckArrWithLength = false) => {
    if (arr && Array.isArray(arr)) {
        if (isCheckArrWithLength) return arr.length > 0 ? true : false
        return true
    }
    return false
}

export const randomIntFromInterval = (min: number, max: number) => {
    // min and max included
    return Math.floor(Math.random() * (max - min + 1) + min) as number
}

export const copyText = (str: string) => {
    /* Copy the text inside the text field */
    navigator.clipboard.writeText(str)

    /* Alert the copied text */
    alert('Copied the text: ' + str)
}

export const userInRight = (access_roles: number[], session: any) => {
    return (
        !access_roles ||
        (access_roles[0] !== -1 && session?.user?.is_admin === 1) ||
        access_roles.includes(session?.user?.user_role_id)
    )
}

export const textOverflowEllipsis = (str: string, length: number) => {
    if (!str) return ''
    return str.substr(0, length) + (str.length > length ? '...' : '')
}

export const cleanObject = (obj: any) => {
    const result: any = {};
    if (obj) {
        Object.keys(obj).forEach((key) => {
            if ((!Array.isArray(obj[key]) && obj[key]) || obj[key]?.length)
                result[key] = obj[key];
        });
    }
    return result;
};

export const generateUniqueId = (pre: string) => {
    return `${pre || ''}-${new Date().getTime() * Math.random()}`;
}

export const formatNumber = (value: any, defaultValue: any) => {
    if (value) {
        const decimalPosition = value.toString().indexOf(".");
        if (decimalPosition > 0) {
            const intVal = value.toString().substring(0, decimalPosition);
            const decimalVal = value.toString().substring(decimalPosition + 1);
            return `${intVal.replace(/\B(?=(\d{3})+(?!\d))/g, ",")}.${decimalVal}`;
        }
        return value.toString().padStart(2,'0').replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }
    return defaultValue ?? "";
};

export const sleep = (time: number) => new Promise((res) => setTimeout(res, time));

export const delayResult = async (func: any, time: number) => new Promise(async (res, reject) => {
    try {
        const result = await Promise.all([func, sleep(time)])
        res(result[0]);
    } catch (error) {
        reject(error);
    }
});

const defaultSourceString = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
export const randomString = (length: number, sourceString: string = defaultSourceString) => {
    let result = '', counter = 0;
    const sourceLength = sourceString.length;
    while (counter++ < length) {
        result += sourceString.charAt(Math.floor(Math.random() * sourceLength));
    }
    return result;
}

export const toNum = (value: string, defaultValue: any) => isNaN(+value) ? defaultValue : +value;

export const getFullUrl = (path: string) => {
  const protocol = window.location.protocol
  const hostname = window.location.hostname
  const port = hostname === 'localhost' ? `:${window.location.port}` : ''
  const url = `${protocol}//${hostname}${port}/${path}`
  return url
}
export const onCopyUrl = (path: string) => {
  const urlCopy = getFullUrl(path)
  navigator.clipboard.writeText(urlCopy)
}
export const checkStatusAssign = ({ today, startDate, endDate }: any) => {
    const isBetween = dayjs(today).isBetween(startDate, endDate, "millisecond", "[]");
    if (isBetween) return STATUS_CODE_EXAM.ONGOING;
    return compareTwoDate({ dateOne: today, dateTwo: startDate });
};
export const compareTwoDate = ({ dateOne, dateTwo }: any) => {
    const isAfter = dayjs(dateOne).isAfter(dateTwo, "millisecond");
    if (isAfter) return STATUS_CODE_EXAM.ENDED;
    return STATUS_CODE_EXAM.UPCOMING;
};

const transformTypes = {
    String: 'string',
    Number: 'number',
    Array: 'array',
    ArrayNumber: 'array-number',
}

const transformByType = (value: any, targetType = 'string', ignoreUndefined = true) => {
    if (value === undefined && ignoreUndefined) {
        return value;
    }
    switch (targetType) {
        case transformTypes.String: {
            return String(value);
        }
        case transformTypes.Number: {
            return Number(value);
        }
        case transformTypes.Array: {
            return Array.isArray(value) ? value : [value];
        }
        case transformTypes.ArrayNumber: {
            return (Array.isArray(value) ? value : [value]).map(Number);
        }
        default: {
            return value;
        }
    }
}

const transformObject = (data: any, targetTransform: any) => {
    const newObject: any = {};
    for (const key in targetTransform) {
        newObject[key] = transformByType(data[key], targetTransform[key]);
    }

    return newObject;
}

transformObject.types = transformTypes;

export { transformObject };

export const enabledExportUnitTest = (user: any, unitTestData: any) => {
    const isAdmin = user?.is_admin === 1
    const checkUserRole = user.user_role_id
    if (checkUserRole === USER_ROLES.Operator || isAdmin) {
        //ko thuoc nhom de quoc te'
        return unitTestData.is_international_exam != 1
    }
    else if (checkUserRole === USER_ROLES.Teacher) {
        // acc giao vien chỉ bao gồm các đề unit_test_type nhóm Thực hành
        return (
            unitTestData.unit_test_type === UNIT_TEST_TYPE.PRACTICE_UNIT_TEST ||
            unitTestData.unit_test_type === UNIT_TEST_TYPE.PRACTICE_LANGUAGE ||
            unitTestData.unit_test_type === UNIT_TEST_TYPE.PRACTICE_SKILL
        )
    }
}
export const eventConfig = {
    startDate: '2023-09-14 00:00:00',
    endDate: '2023-10-14 23:59:59',
    gifts: {
        // 9 lượt cũ và bao gồm lượt thành công hiện tại -> 10 lượt
        9: { addQuantity: 20, addDayExpire: 365, comboCode: PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE },
        24: { addQuantity: 88, addDayExpire: 0, comboCode: PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE },
    }
}

export const renderNameByCode = (item: any, t:any) => {
    switch(item?.code){
        case PACKAGES.BASIC.CODE:
            return t('basic');
        case PACKAGES.ADVANCE.CODE:
            return t('medium');
        case PACKAGES.INTERNATIONAL.CODE:
            return t('advanced');
        case PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE:
            return t('student-basic-plan');
        case PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE:
            return t('student-medium-plan');
        case PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE:
            return t('student-advanced-plan');
        case PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE:
            return t('student-international-plan');
        default:
            return item?.name || '';
    }
}