import TripleDES from 'crypto-js/tripledes' // always import this first
import Utf8 from 'crypto-js/enc-utf8'
import MD5 from 'crypto-js/md5'
import ECB from 'crypto-js/mode-ecb'
import Pkcs7 from 'crypto-js/pad-pkcs7'

export const cryptoDecrypt = (message) => {
  let base64 = Utf8.parse(process.env.NEXT_PUBLIC_APP_SECRET_KEY)
  let decrypt = TripleDES.decrypt(message, MD5(base64), {
    mode: ECB,
    padding: Pkcs7
  })
  return JSON.parse(decrypt.toString(Utf8))
}

//giải mã với secret key
export const  cryptoDecrypt_key = (secret_key, message)=>{
  try{
    let base64 = Utf8.parse(secret_key)
    let decrypt = TripleDES.decrypt(message, MD5(base64), {
      mode: ECB,
      padding: Pkcs7
    })
    
    return JSON.parse(decrypt.toString(Utf8))
  }catch(e){
    console.log("eee============err",e)
  return {}
  }
  
}
