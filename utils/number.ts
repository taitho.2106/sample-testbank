//check positive integer
export function isPositiveInteger(n:any) {
    return n >>> 0 === parseFloat(n);
}

export const toFixedNumber = (value: number, fractionDigits = 2) => {
    return Number(value.toFixed(fractionDigits));
}