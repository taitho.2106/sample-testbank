import { type } from "os"

type PosType = {
    x: number
    y: number
    c: string
  }
function getMaxMinXY  (data: PosType[]) {
    if(data.length == 0) return{ minX:0, minY:0, maxX:0, maxY:0 }
    let minX = data[0].x
    let maxX = data[0].x
    let minY = data[0].y
    let maxY = data[0].y
    const len = data.length
    for (let i = 0; i < len; i++) {
      const item = data[i]
      if (item.x > maxX) maxX = item.x
      if (item.x < minX) minX = item.x
      if (item.y > maxY) maxY = item.y
      if (item.y < minY) minY = item.y
    }
    return { minX, minY, maxX, maxY }
  }
  const getRoundLine = (data: PosType[]) => {
    if (data.length == 0) return data
    let maxX = 0;
    let maxY = 0;
    data.forEach((ele:any) => {
      if(ele.x > maxX){
        maxX = ele.x;
      }
      if(ele.y > maxY){
        maxY = ele.y;
      }
    });

    const list1: PosType[] = []
    for (let y = 0; y < maxY; y++) {
      const listY = data.filter((m) => m.y == y).sort((a, b) => a.x - b.x)
      const lenSubItem = listY.length
      if (listY && lenSubItem > 0) {
        list1.push(listY[0])
        if (lenSubItem > 1) {
          list1.push(listY[lenSubItem - 1])
          for (let j = 1; j < lenSubItem - 1; j++) {
            if (
              listY[j + 1].x - listY[j].x > 1 ||
              listY[j].x - listY[j - 1].x > 1
            ) {
              list1.push(listY[j])
            }
          }
        }
      }
    }
    for (let x = 0; x < maxX; x++) {
      const listX = data.filter((m) => m.x == x).sort((a, b) => a.y - b.y)
      const lenSubItem = listX.length
      if (listX && lenSubItem > 0) {
        list1.push(listX[0])
        if (lenSubItem > 1) {
          for (let j = 1; j < lenSubItem - 1; j++) {
            if (
              listX[j + 1].y - listX[j].y > 1 ||
              listX[j].y - listX[j - 1].y > 1
            ) {
              list1.push(listX[j])
            }
          }
          list1.push(listX[lenSubItem - 1])
        }
      }
    }
    return list1
  }
type DirectionType = 'top'|'topLeft'|'topRight'|'bottomRight'|'bottom'|'bottomLeft'
const getPointPosition = (data: PosType[], direction:DirectionType)=>{
  const dataAround = getRoundLine(data)
  const maxMin = getMaxMinXY (data);
  const check = (a:number, b:number)=>{
    if(dataAround.findIndex(m=>m.x == a && m.y == b)!=-1){
      return true
    }
    if(dataAround.findIndex(m=>m.x == a+1 && m.y == b)!=-1){
      return true
    }
    if(dataAround.findIndex(m=>m.x == a && m.y == b+1)!=-1){
      return true
    }
    if(dataAround.findIndex(m=>m.x == a+1 && m.y == b+1)!=-1){
      return true
    }
    if(dataAround.findIndex(m=>m.x == a-1 && m.y == b-1)!=-1){
      return true
    }
    return false;
  }
  switch(direction){
    case 'top':
      const x1t = Math.ceil(maxMin.maxX/2);
      let y1t = maxMin.minY;
      while(y1t <maxMin.maxY){
        if(dataAround.findIndex(m=>m.x == x1t && m.y == y1t)!=-1){
          return {x:x1t, y:y1t};
        }
        y1t+=1;
      }
      return {x:0, y:0};
    case 'topLeft':
      let x1 = maxMin.minX;
      let y1 = maxMin.minY;
      while(x1 <= maxMin.maxX || y1 <= maxMin.maxY){
        if(check(x1,y1)){
          return {x:x1, y:y1}
        }
        x1+=1;
        y1+=1;
      }
      return {x:0, y:0};
      case 'topRight':
      let x2 = maxMin.maxX;
      let y2 = maxMin.minY;
      while(x2 >= maxMin.minX || y2 <= maxMin.maxY){
        if(check(x2,y2)){
          return {x:x2, y:y2}
        }
        x2-=1;
        y2+=1;
      }
      return {x:0, y:0};
      case 'bottomLeft':
        let x3 = maxMin.minX;
        let y3 = maxMin.maxY;
        while(x3 <= maxMin.maxX || y3 >= maxMin.minY){
          if(check(x3,y3)){
            return {x:x3, y:y3}
          }
          x3+=1;
          y3-=1;
        }
        return {x:0, y:0};
        case 'bottomRight':
          let x4 = maxMin.maxX;
          let y4 = maxMin.maxY;
          while(x4 <= maxMin.maxX || y4 >= maxMin.minY){
            if(check(x4,y4)){
              return {x:x4, y:y4}
            }
            x4-=1;
            y4-=1;
          }
          return {x:0, y:0};
  }
  
}
const coordinates ={getMaxMinXY,getRoundLine,getPointPosition}
export default coordinates;