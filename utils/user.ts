import { PACKAGES, PACKAGE_LEVEL_CONFIG } from "@/interfaces/constants";
import { UserDataType } from "@/interfaces/types";
import { randomString } from ".";

const checkTrialUser = (user: UserDataType) => {
    if (!user?.is_trial_mode) {
        return false;
    }

    const currentTime = new Date().getTime();

    return +(user.end_date ?? 0) * 1000 - currentTime > 0;
}

const getUserPackageCode = (userSession: any, userPackageCode: any) => {
    if (userPackageCode) {
        return userPackageCode;
    }

    return PACKAGES.BASIC.CODE;
}

const getPackageLevel = (packageCode: string, arrConfig = PACKAGE_LEVEL_CONFIG) => arrConfig.findIndex(el => el === packageCode);


const getUpgradeAblePackageLevels = (currentPackageCode: string, arrConfig = PACKAGE_LEVEL_CONFIG) => {
    const indexCurrentPackage = getPackageLevel(currentPackageCode, arrConfig);

    return arrConfig.slice(indexCurrentPackage + 1,arrConfig.length).filter(item => item !== PACKAGES.TRIAL.CODE);
}
const checkAccessPackageLevel = (currentPackage: string, requirePackage: string, arrConfig = PACKAGE_LEVEL_CONFIG) => {
    if (!currentPackage) {
        return false;
    }

    const currentPackageLevel = getPackageLevel(currentPackage, arrConfig);
    const requirePackageLevel = getPackageLevel(requirePackage, arrConfig);

    return currentPackageLevel >= requirePackageLevel;
}

const generateUsername = (phone: string, randomStringLength = 3) => {
    return `${phone}${randomString(randomStringLength, 'abcdefghijklmnopqrstuvwxyz')}`
}

export {
    checkTrialUser,
    getUserPackageCode,
    getPackageLevel,
    checkAccessPackageLevel,
    getUpgradeAblePackageLevels,
    generateUsername,
};