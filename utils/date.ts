import dayjs from 'dayjs';
import utc from 'dayjs/plugin/utc';
import customParseFormat from 'dayjs/plugin/customParseFormat';
import isBetween from 'dayjs/plugin/isBetween';
import { STATUS_CODE_EXAM } from '@/interfaces/constants';
import { DATE_DISPLAY_FORMAT } from "@/constants/index";
import useTranslation from "@/hooks/useTranslation";

dayjs.extend(isBetween)
dayjs.extend(customParseFormat);
dayjs.extend(utc)

const dayjsUTC = dayjs.utc;

// utc = true => format should be string
const convertDateStringToDate = (dateString: string, format: string | any[] = DATE_DISPLAY_FORMAT, utc = false) => {
    if (utc) {
        return dayjsUTC(dateString, format as string, true);
    }
    return dayjs(dateString, format, true);
};

const compareDateString = (dateA: string, dateB: string) => {
    return convertDateStringToDate(dateA).unix() - convertDateStringToDate(dateB).unix();
}

const getDateRangeConditionByStatus = (status: number | string, startDateKey = 'start_date', endDateKey = 'end_date') => {
    const curDate = dayjs();
    const mapDateRangeByStatus = {
        [STATUS_CODE_EXAM.UPCOMING]: {
            [startDateKey]: {
                gte: curDate.toDate(),
            },
        },
        [STATUS_CODE_EXAM.ONGOING]: {
            [startDateKey]: {
                lte: curDate.toDate(),
            },
            [endDateKey]: {
                gte: curDate.toDate(),
            }
        },
        [STATUS_CODE_EXAM.ENDED]: {
            [endDateKey]: {
                lte: curDate.toDate(),
            }
        },
    }

    return mapDateRangeByStatus[status as number];
}

const getDateStatusFromStartEnd = (startDate: any, endDate: any) => {
    const startDayjs = dayjs(startDate), endDayjs = dayjs(endDate);
    const currentTime = dayjs();

    if (currentTime.isAfter(endDayjs)) {
        return STATUS_CODE_EXAM.ENDED;
    }

    if (currentTime.isBefore(startDayjs)) {
        return STATUS_CODE_EXAM.UPCOMING;
    }

    return STATUS_CODE_EXAM.ONGOING;
}

const getDisplayTimeOut = (timeout: number, t?:any) => {
    const milliseconds = timeout;
    const seconds = Math.floor((milliseconds / 1000) % 60);
    const minutes = Math.floor((milliseconds / (1000 * 60)) % 60);
    const hours = Math.floor((milliseconds / (1000 * 60 * 60)) % 24);
    const days = Math.floor(milliseconds / (1000 * 60 * 60 * 24));

    return [
        days && `${days} ${t('time')['days'] || "ngày"}`,
        hours && `${hours} ${t('time')['hours'] || "giờ"} `,
        minutes && `${minutes} ${t('time')['minutes'] || "phút"}`,
        seconds && `${seconds} ${t('time')['seconds'] || "giây"}`,
    ].filter(Boolean).slice(0, 2).join(' ');

}

export { convertDateStringToDate, compareDateString, dayjsUTC, dayjs, getDateRangeConditionByStatus, getDateStatusFromStartEnd, getDisplayTimeOut };
