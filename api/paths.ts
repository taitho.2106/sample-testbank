export const paths = {
  api_questions: '/api/questions',
  api_questions_search: '/api/questions/search',
  api_questions_import: '/api/questions/import',
  api_questions_upload: '/api/questions/upload',
  api_templates: '/api/templates',
  api_templates_origin_create: '/api/templates/origin-create-template',
  api_templates_search: '/api/templates/search',
  api_users: '/api/users',
  api_users_register: '/api/users/register',
  api_users_user_exist: '/api/users/user-exist',
  api_users_forgot_password: '/api/users/forgot-password',
  api_users_otp: '/api/users/otp',
  api_users_roles: '/api/users/roles',
  api_users_change_password: '/api/users/change-password',
  api_users_info: '/api/users/info',
  api_users_save: '/api/users/save',
  api_users_avatar: '/api/users/avatar',
  api_unit_test: '/api/unit-test',
  api_eduhome_user:'/api/users/user-eduhome',
  api_date_now:'/api/common/now',
  api_user_guide: '/api/user-guide',

  //api import questions;
  api_import_questions_v2:'/api/questions/import-questions',

  //api integration:
  api_data_integration: '/api/integrate-exam/data',
  api_child_app_id_integration: '/api/integrate-exam/child-app-id',

  //api unit_test_result:
  api_unit_test_result:'/api/unit-test-result',
  api_unit_test_search: '/api/unit-test/search',


  //api send email result:
  api_send_email_result:"/api/unit-question/send-email",

  //api get list third party:
  api_get_third_party: "/api/common/third-party",

  //eduhome:
  api_eduhome_get_theme_info:"/api/getthemebyid",
  api_eduhome_get_account_info:"/api/getinfoaccount",
  api_eduhome_get_series:"/api/v2/store/series",
  //export - unit_test:
  api_unit_test_export:"/api/unit-test/export-zip",

  api_unit_test_create_from_teacher:"/api/unit-test/from-teacher/create",
  api_unit_test_sync_from_teacher:"/api/unit-test/from-teacher/sync",
  api_unit_test_questions:"/api/unit-test/questions",

  api_unit_test_update_privacy:"/api/unit-test/privacy",

  //question
  api_question_update_privacy:"/api/questions/privacy",

  //order
  api_order_create: '/api/order/create',
  api_order_detail: '/api/order',
  api_user_package: '/api/users/package',
  api_service_package: '/api/service-package',

  api_series: '/api/common/series',
  api_user_feedback: '/api/users/question',

  api_send_email_student : '/api/unit-test/send-email-to-student',

  api_teacher_student:'/api/teacher/students',
  
  api_teacher_unit_tests: '/api/teacher/unit-tests',

  api_teacher_classes: '/api/classes',

  // new unit test result
  api_unit_test_submit: '/api/unit-test/submit',

  //unit test assign for student
  api_unit_test_assign_student: '/api/students/unit-test',

  api_classes: '/api/classes',

  api_unit_test_assigned: '/api/unit-test/assigned',

  api_unit_test_assigned_result: '/api/students/unit-test-assigned',

  api_unit_test_assigned_info: "/api/students/unit-test-assigned",

  api_student_classes: "/api/students/classes",

  api_sale_item: "/api/sale-item",

  api_get_student_unit_test_result: "/api/students/unit-test-asset/results",

  api_student_use_unit_test: "/api/unit-test/use",

  api_get_province_list: `${process.env.NEXT_PUBLIC_INTEGRATION_API_HOST}/integration/areas`,

  //combo-unit-test
  api_current_combo: "/api/students/current-combo",

  //calendar
  api_calendar_student: "/api/students/unit-tests/calendar",
  api_calendar_teacher: "/api/teacher/unit-tests/calendar",
}
