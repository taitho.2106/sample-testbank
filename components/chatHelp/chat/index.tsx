import { useEffect } from 'react'

import Script from 'next/script'
const Chat = () => {
  const handleEvents = (event: any) => {
    try {
      const msg: any = JSON.parse(event.data)
      if (msg.topic == 'initialized') {
        console.log('initialized')
        fillingInfo(event.source)
      }
    } catch (error) {
      console.log(error)
    }
  }
  const fillingInfo = (source: any) => {
    // const info = {
    //   // [topic] must be "fill-info"
    //   topic: 'fill-info',
    //   // [data] contains keys, which created when add form setting, and values to be filled
    //   data: {
    //     name: userInfo?.user_name,
    //     email: '',
    //     phone: '',
    //   },
    // }
    // console.log('user-chat', userInfo?.user_name)
    // source.postMessage(JSON.stringify(info), '*')
    const frame = document.getElementById('mde-iframe')
    if (isIFrame(frame) && frame.contentWindow) {
      frame.contentWindow.postMessage(
        JSON.stringify({ topic: 'popup-open' }),
        '*',
      )
      frame.style.setProperty('display', 'none')
    }
  }

  const HiddenChatPopup = () => {
    document
      .getElementById('btn-close-box-chat')
      .style.setProperty('display', 'none')
    document.getElementById('mde-iframe').style.setProperty('display', 'none')
  }

  useEffect(() => {
    if (window.addEventListener) {
      window.addEventListener('message', handleEvents, false)
    } else if ((window as any)['attachEvent']) {
      ;(window as any)['attachEvent']('onmessage', handleEvents)
    }
  }, [])
  return (
    <>
      <Script src="https://dtp.izimessage.com/api/script/widget"></Script>
      <div id="btn-close-box-chat">
        <img
          src="/images/icons/ic-close-circle.png"
          alt="close"
          onClick={HiddenChatPopup}
        />
        <div className="cover-top-chat"></div>
        <div className="cover-left-chat"></div>
        <div className="circle-left"></div>
        <div className="circle-right"></div>
      </div>
    </>
  )
}

//check is iframe
const isIFrame = (input: HTMLElement | null): input is HTMLIFrameElement =>
  input !== null && input.tagName === 'IFRAME'

//show chat popup
const OpenChatPopup = () => {
  const frame = document.getElementById('mde-iframe')
  if (isIFrame(frame) && frame.contentWindow) {
    frame.contentWindow.postMessage(
      JSON.stringify({ topic: 'popup-open' }),
      '*',
    )
    frame.style.setProperty('display', 'block')
    document
      .getElementById('btn-close-box-chat')
      .style.setProperty('display', 'block')
  }
}
export { Chat, OpenChatPopup }
