import { useContext, useEffect, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import { Button } from 'rsuite'

import HeaderV2 from '@/componentsV2/Dashboard/Header-v2/Header'
import SideBarDashboard from '@/componentsV2/Dashboard/SideBar/SideBarDashboard'
import { paths } from 'api/paths'
import { ComingSoon } from 'components/organisms/comingSoon'
import { userInRight } from 'utils'

import SideBarMenu from "../../../components-v2/Admin/SideBarMenu"
import { AppContext, WrapperContext } from '../../../interfaces/contexts'
import { DefaultPropsType, GlobalModalType } from '../../../interfaces/types'
import { Header } from '../../organisms/header'
import { Sidebar } from '../../organisms/sidebar'
import { ModalPicker } from '../modalPicker'
import classNames from 'classnames'
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'
import { USER_ROLES } from '@/interfaces/constants'
import useTranslation from '@/hooks/useTranslation'


interface PropsType extends DefaultPropsType {
  access_role?: number[]
  isComingSoon?: boolean
  pageTitle?: string
  subTitle?: string
  templateDetailOrigin?: any
  userInfo?: any,
  hasStickyFooter?: boolean,
  hiddenOverflow?: boolean
}

export const Wrapper = ({
  className = '',
  access_role,
  isComingSoon = false,
  pageTitle = '',
  subTitle = '',
  templateDetailOrigin = null,
  style,
  children,
  userInfo,
  hasStickyFooter = true,
  hiddenOverflow = false
}: PropsType) => {
  const router = useRouter()

  const { isExpandSidebar } = useContext(AppContext)
  const [globalModal, setGlobalModal] = useState(null as GlobalModalType | null)
  const [dataImport, setDataImport] = useState([])
  const [countRowData, setCountRowData] = useState(0)
  const [templateDetail, setTemplateDetail] = useState(
    templateDetailOrigin as any,
  )
  
  const [session] = useSession()
  useEffect(() => {
    if (session === null) {
      console.log("section-Wrapper null")
      router.push('/login')
    }
    // else if (session) {
    //   const userSession: any = session.user
    //   const lastDay:any = userSession.end_date && userSession.end_date>0 ? new Date(userSession.end_date*1000) : null
    //   if (lastDay !== null && userSession.is_trial_mode === 1){
    //     //check expiry date
    //     const checkDate=async()=>{
    //       console.log("lastDay", userSession.end_date)
    //       const res = await fetch(paths.api_date_now)
    //       const json = await res.json();
    //       // console.log("json",json);
    //       const currentDay:any = new Date(json)
    //       const subTime = (lastDay - currentDay) /(1000*60*60*24);
    //       if(subTime <= 0){
    //         console.log("expiration")
    //         router.push(userSession.redirect)
    //       }
    //     }
    //     checkDate();
    //   }

    //   if (!userInRight(access_role, session)) {
    //     router.push('/error/auth')
    //   }
    // }
  }, [session])



  if (!session || !userInRight(access_role, session)) return null


  return (
    <>
    <WrapperContext.Provider
      value={{
        pageTitle,
        subTitle,
        globalModal: { state: globalModal, setState: setGlobalModal },
        templateDetail: { state: templateDetail, setState: setTemplateDetail },
        userInfo: userInfo,
        dataImportError : {state: dataImport,setState: setDataImport},
        countRowData : {state: countRowData,setState: setCountRowData}
      }}
    >
      <div
        className={classNames('t-wrapper', className, hasStickyFooter && 'has-sticky-footer')}
        data-expand={isExpandSidebar.state}
        style={style}
      >
          <div className="t-wrapper__sidebar">

            {/*<Sidebar*/}
            {/*  isExpand={isExpandSidebar.state}*/}
            {/*  onToggle={() => isExpandSidebar.setState(!isExpandSidebar.state)}*/}
            {/*/>*/}
            <SideBarDashboard
              isExpand={isExpandSidebar.state}
              onToggle={() => isExpandSidebar.setState(!isExpandSidebar.state)}
            />
            {/*  onToggle={() => isExpandSidebar.setState(!isExpandSidebar.state)}
          />
          {/* <div className="t-wrapper__sidebar__instruction">
            <Button
              className="submit-btn"
              onClick={() => setGlobalModal({id: 'instruction'})}
            >
              <CreativeIcon />
              <span className={`${isExpandSidebar.state ? 'instruction-text' : 'instruction-text-none'}`}>Hướng dẫn</span>
            </Button>
          </div> */}
          </div>

          <div className='t-wrapper__right'>
          <div className="t-wrapper__header">
            <HeaderV2 papeTitle={pageTitle} subTitle={subTitle}/>
          </div>
            <div className="t-wrapper__main" style={hiddenOverflow ? { overflow: 'hidden' } : {}}>
                {isComingSoon ? (
                  <ComingSoon />
                ) : (<>{children}</>)}
            </div>
        </div>
      </div>
      <div className='background-print-pdf' id='background-print-pdf'/>
      {/* Thêm props cho modal để phân quyền UI */}
      <ModalPicker userInfo={session?.user}/>
      {/* <div className="t-contact">
        <Button className="t-contact__btn-chat">
          <img src="/images/icons/ic-contact-chat.png" alt="chat-button"/>
          <span>Hỗ trợ trực tuyến</span>
        </Button> 
        <Button 
          className="t-contact__btn-help" 
        onClick={(e) => { 
          
          setGlobalModal({
            id: 'help',
            data: guideData,
          })
          //{guideData: [{ListHelp}]}
          // setGlobalModal({
          //   id:"help",
          //   // data: {ListHelp:[{title:"t1", type:"video"},{title:"t2",type:"document"}]}
    
          // })
        }}>

          <img src="/images/icons/ic-contact-help.png" alt="help-button"/>
          <span>Hướng dẫn sử dụng</span>
        </Button>        
      </div> */}
      </WrapperContext.Provider>
    </>
  )
}

