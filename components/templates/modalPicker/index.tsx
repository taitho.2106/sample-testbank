import { useContext } from 'react'

import CopyUnitTestShortStep from '@/componentsV2/Common/CopyUnitTestShortStepModal'
import ModalImportFail from '@/componentsV2/Common/ImportFailModal'
import ModalActionConfirm from '@/componentsV2/Common/ModalActionConfirm'
import ModalDelete from '@/componentsV2/Common/ModalDelete'
import ModalExpiredPackage from '@/componentsV2/Common/ModalExpiredPackage'
import ModalSendEmail from '@/componentsV2/Common/ModalSendEmail'
import ModalSendUnitTest from '@/componentsV2/Common/ModalSendUnitTest'
import { BtnGroupModal } from 'components/organisms/modals/btnGroupModal'
import { ChangeDateModal } from 'components/organisms/modals/changeDateModal'
import { ConfirmModal } from 'components/organisms/modals/confirmModal'
import ConfirmModalIntegrateExam from 'components/organisms/modals/confirmModalIntegrateExam'
import { InstructionModal } from 'components/organisms/modals/instructionModal'
import LoadingModal from 'components/organisms/modals/loadingModal'
import { PreviewUnitQuestion } from 'components/organisms/modals/previewUnitQuestion'
import { QuestionDrawer } from 'components/organisms/modals/questionDrawer'
import ModalResultIntegrateExam from 'components/organisms/modals/resultModalIntegrateExam'
import { ShareLinkModal } from 'components/organisms/modals/shareLinkModal'
import UploadModal from 'components/organisms/modals/uploadModal'
import { UploadResultModal } from 'components/organisms/modals/uploadResultModal'
import { WrapperContext } from 'interfaces/contexts'
import ModalConfirmUseUnitTest from "../../molecules/unitTestTable/ModalConfirmUseUnitTest";

export const ModalPicker = ({ userInfo }: any) => {
  const { globalModal } = useContext(WrapperContext)
  return (
    <>
      <ConfirmModal isActive={globalModal.state?.id === 'confirm-modal'} />
      {globalModal.state?.id === 'question-drawer' && (
        <QuestionDrawer isActive={true} />
      )}
      {globalModal.state?.id === 'share-link' && (
        <ShareLinkModal isActive={true} />
      )}
      {globalModal.state?.id === 'change-date' && (
        <ChangeDateModal isActive={true} />
      )}
      {globalModal.state?.id === 'instruction' && (
        <InstructionModal isActive={true} />
      )}
      {globalModal.state?.id === 'btn-group' && (
        <BtnGroupModal isActive={true} />
      )}
      {/* Thêm 2 modal mới cho hệ thống với mô tả mới */}
      {globalModal.state?.id === 'btn-uploadfile' && (
        <UploadModal isActive={true} userInfo={userInfo} />
      )}
      {globalModal.state?.id === 'loading-modal' && (
        <LoadingModal isActive={true} />
      )}
      {/* Modal Thông báo kết quả upload chung */}
      {globalModal.state?.id === 'upload-question-result' && (
        <UploadResultModal isActive={true} />
      )}
      {/* Modal confirm integrate exam */}
      <ConfirmModalIntegrateExam
        isActive={globalModal.state?.id === 'confirm-modal-integrate-exam'}
      />
      {/* Modal result integrate exam */}
      <ModalResultIntegrateExam
        isActive={globalModal.state?.id === 'result-modal-integrate-exam'}
      />
      {globalModal.state?.id === 'preview-question-modal' && (
        <PreviewUnitQuestion isActive={true} />
      )}
      {globalModal.state?.id === 'copy-unit-test-short-step' && (
        <CopyUnitTestShortStep isActive={true} />
      )}
      {globalModal.state?.id === 'send-email' && (
        <ModalSendEmail isActive={true} />
      )}
      {globalModal.state?.id === 'service-package' && (
        <ModalExpiredPackage isActive={true} />
      )}
      {globalModal.state?.id === 'send-unit-test' && (
        <ModalSendUnitTest isActive={true} />
      )}
      {globalModal.state?.id === 'action-confirm' && (
        <ModalActionConfirm isActive={true} />
      )}
      {globalModal.state?.id === 'delete-student-to-classes' && (
        <ModalDelete isActive={true} />
      )}
      {globalModal.state?.id === 'import-fail' && (
        <ModalImportFail isActive={true} />
      )}
      {globalModal.state?.id === 'confirm-use-unit-test' && (
        <ModalConfirmUseUnitTest isActive={true} />
      )}
    </>
  )
}
