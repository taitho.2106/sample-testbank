import { Dropdown } from 'rsuite'

import { DefaultPropsType } from '../../../interfaces/types'
interface PropsType extends DefaultPropsType {
  data?: any
  selected: string
}

export const SLAnswersDropdown = ({
  className = '',
  data,
  selected,
  style,
}: PropsType) => {
  return (
    <div className={`a-sl-dropdown ${className}`} style={style}>
      <Dropdown title={selected} activeKey ={selected} >
      {data.map((item: string, i: number) => (
          <Dropdown.Item
            key={i}
            eventKey={item}
            className={item.trim() === selected.trim() ? 'answer' : ''}
            value={item.trim()}
          >
            {item.trim()}
          </Dropdown.Item>
        ))}
      </Dropdown>
    </div>
  )
}
