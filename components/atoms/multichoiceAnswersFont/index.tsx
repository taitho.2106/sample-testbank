import { ReactNode } from 'react'
import { convertStrToHtml, replaceTyphoStr } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  checked?: boolean
  data?: ReactNode
  radioName?: string
}
const typho = ['%b%', '%i%', '%u%']
export const MultiChoicesAnwersFont = ({
  className = '',
  checked,
  data,
  radioName,
  style,
}: PropsType) => {
  const stringAns:any = data;
  return (
    <div className={`a-multichoiceanswers-font ${className}`} style={style}>
      <div className="a-multichoiceanswers-font__answer">
        <ul className="a-multichoiceanswers-font__input ">
          <li>
            <input
              type="checkbox"
              name={radioName}
              className="option-input checkbox"
              defaultChecked={checked}
              style={{ pointerEvents: 'none' }}
            />
            <label dangerouslySetInnerHTML={{
            __html: convertStrToHtml(stringAns),
          }}></label>
          </li>
        </ul>
      </div>
    </div>
  )
}
