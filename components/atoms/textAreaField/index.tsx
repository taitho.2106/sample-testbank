import { CSSProperties, useEffect, useState, useCallback, KeyboardEvent, useRef } from 'react'

import { UseFormRegisterReturn, UseFormSetValue } from 'react-hook-form'

import Guid from 'utils/guid'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  defaultValue?: any
  label: string
  name?: string
  reverse?: boolean
  register?: UseFormRegisterReturn
  setValue?: UseFormSetValue<any>
  disabled?: boolean
  rows?: number
  onBlur?: (data: any) => void
  inputStyle?: CSSProperties
  viewMode?: boolean,
  id?:string
  specialChar?:string[]
}

export const TextAreaField = ({
  className = '',
  defaultValue = '',
  label,
  name,
  reverse = false,
  style,
  register,
  setValue,
  disabled,
  onBlur,
  inputStyle,
  viewMode = false,
  id,
  specialChar = []
}: PropsType) => {
  const key = useRef(`${Guid.newGuid()}`)
  const [valueStr, setValueStr] = useState(defaultValue ?? '')
  useEffect(()=>{
    const elementId = id?id:key.current;
    const el = document.getElementById(elementId)
    if(el){
      el.setAttribute("ondragenter","return false")
    el.setAttribute("ondragleave","return false")
    el.setAttribute("ondragover","return false")
    el.setAttribute("ondrop","return false")
    }
  },[])
  useEffect(()=>{
    const elementId = id?id:key.current;
    const el = document.getElementById(elementId)
    if(el){
      const removeFocus = () => {
        el.style.borderColor = '#d5dee8'
      }
      if(viewMode){
        el.setAttribute("readonly",'readonly')
        el.style.pointerEvents = 'auto'
        el.addEventListener('focus',removeFocus)
        return ()=>{
          el.removeEventListener('focus',removeFocus)
        }
      }else{
        el.removeAttribute("readonly")
        el.removeAttribute("style")
       
      }
    }
    
  },[viewMode])
  
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLTextAreaElement>) => {
    if (specialChar.includes(event.key)) {
      event.preventDefault()
      event.stopPropagation()
    }
    if(event.key == "Enter"){
      event.preventDefault();
    }
  }, [])
  const replaceRange = (str:string, start:number, end:number, substitute:string) => {
    return str.substring(0, start) + substitute + str.substring(end);
  }
  const onPaste = useCallback((event: any) => {
    event.preventDefault()
    try{
      let data = event.clipboardData.getData('text').split('\n').map((item: any) => item.trim()).join(' ')
      if (specialChar.length > 0) {
        for (let i = 0; i < specialChar.length; i++) {
          data = data.replaceAll(specialChar[i], '')
        }
      }
      const txtarea = event.currentTarget;
      const start = txtarea.selectionStart;
      const finish = txtarea.selectionEnd;
      const value = txtarea.value;
      const newValue = replaceRange(value,start,finish,data)
      txtarea.value = newValue;
      const newIndexSelect = start+(data.length)
      txtarea?.setSelectionRange(newIndexSelect, newIndexSelect)
      setValueStr(newValue)
    }catch{
      return;
    }
    
  }, [])
  
  return (
    <div
      className={`a-textarea-with-label ${className}`}
      data-exist={valueStr ? true : false}
      data-reverse={reverse}
      style={style}
    >
      <textarea
      id={id||key.current}
        rows={4}
        style={inputStyle}
        value={valueStr}
        name={name}
        disabled={disabled}
        {...register}
        onChange={(e) => {
          setValueStr(e.target.value)
          if(!setValue){
            register?.onChange(e)
          }
        }}
        onBlur={(e) => {
          onBlur && onBlur(e.target.value)
          setValueStr(e.target.value.trim())
          if(setValue){
            setValue(register.name, e.target.value.trim())
          }else{
            register?.onBlur(e)
          }
        }}
        onKeyDown={(e=>{
          onKeyDown && onKeyDown(e)
        })}
        onPaste={onPaste}
      />
      <label>
        <span>{label}</span>
      </label>
    </div>
  )
}
