import { convertStrToHtml } from '@/utils/string'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  data?: any[]
  correctData: any[]
}

export const MultipleResponsesAnswers = ({
  className = '',
  data,
  correctData,
  style,
}: PropsType) => {
  const chunkArray = Array.from(
    Array(2),
    () => [],
  ) as any[]
  data.forEach((item: string, i: number) => {
    if(i%2===0){
      chunkArray[0].push(item)
    }else{
      chunkArray[1].push(item)
    }
  })
  return (
    <div className={`a-multiple-responses-answers ${className}`} style={style}>
      <div className="a-multiple-responses-answers__answer">
        {chunkArray.map((arr: string[], i: number) => {
          return (
          <ul key={i} className="a-multiple-responses-answers__input ">
            {arr.map((item, j) => (
              <li key={j}>
                <input
                  type="checkbox"
                  className="option-input checkbox"
                  defaultChecked={correctData.includes(item)}
                  disabled={true}
                />
                <span
                  dangerouslySetInnerHTML={{
                    __html: convertStrToHtml(item).replace(/%s%/g, '__________'),
                  }}
                >
                </span>
              </li>
            ))}
          </ul>
        )})}
      </div>
    </div>
  )
}
