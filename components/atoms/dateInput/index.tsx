import { useEffect, useState } from 'react'

import { DatePicker, Popover, Tooltip, Whisper } from 'rsuite'

import { formatDate } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'

interface Props extends DefaultPropsType {
  defaultValue?: string | Date | null
  disabled?: boolean
  label: string
  name?: string
  placement?: any
  time?: boolean
  triggerReset?: boolean
  triggerSetValue?: Date | null
  onChange?: (val: number) => void
  toolTip?: string
  isShowErr?: boolean
  errorText?: string
}

export const DateInput = ({
  className = '',
  defaultValue = '',
  disabled = false,
  label,
  name = '',
  placement,
  time = false,
  triggerReset,
  triggerSetValue,
  style,
  onChange,
  toolTip,
  isShowErr = false,
  errorText = ''
}: Props) => {
  const [isFirstRender, setIsFirstRender] = useState(true)
  const [isFocus, setIsFocus] = useState(false)
  const [value, setValue] = useState(
    defaultValue ? formatDate(defaultValue, time) : '',
  )
  const [tmpValue, setTmpValue] = useState(
    defaultValue ? new Date(`${defaultValue}`) : new Date(),
  )

  const HandleChange = (date: Date) => {
    const d = new Date(date)
    const timestamp = d.getTime()
    setTmpValue(date)
    setValue(formatDate(date, time))
    if (onChange) onChange(timestamp)
  }

  // use for case reset trigger
  useEffect(() => {
    if (isFirstRender) {
      setIsFirstRender(false)
      return
    }
    if (onChange) onChange(null)
    setValue('')
    setTmpValue(new Date())
  }, [triggerReset])

  useEffect(() => {
    if (triggerSetValue) HandleChange(triggerSetValue)
  }, [triggerSetValue])

  return (
    <Whisper
      placement="topStart"
      trigger="hover"
      speaker={
        <Tooltip style={{ visibility: toolTip ? 'visible' : 'hidden' }}>
          {toolTip}
        </Tooltip>
      }
    >
      <div className={`a-date-input ${className}`} style={style}>
        <div
          className={`a-date-input__cover ${isShowErr && !value.length ? 'error' : ''}`}
          data-active={value ? true : false}
          data-disabled={disabled}
          data-focus={isFocus}
        >
          <label>
            <span>{label}</span>
          </label>
          {value && (
            <div className="__value" data-disabled={disabled}>
              {value}
            </div>
          )}
          <img
            className="__toggle"
            src="/images/icons/ic-calendar-dark.png"
            alt="calendar"
          />
        </div>
        <DatePicker
          className="a-date-input__picker"
          cleanable={false}
          disabled={disabled}
          format={time ? 'yyyy-MM-dd HH:mm' : 'yyyy-MM-dd'}
          name={name}
          value={tmpValue}
          placement={placement || 'bottomEnd'}
          onEnter={() => setIsFocus(true)}
          onExit={() => setIsFocus(false)}
          onSelect={(date: Date) => HandleChange(date)}
          onNextMonth={(date: Date) => HandleChange(date)}
          onPrevMonth={(date: Date) => HandleChange(date)}
        />
        {isShowErr && !value.length && (
          <div className="a-date-input__error-text">
            <label>{errorText}</label>
          </div>
        )}
      </div>
    </Whisper>
  )
}
