import { convertStrToHtml, formatHtmlText, formatTextStyleObject, replaceTyphoStr } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  data: string
}

export const MultiChoicesQuestion = ({
  className = '',
  data,
  style,
}: PropsType) => {
  const formatData = () => {
    const dataStr = data.replaceAll("%s%","###")
    const newData = formatTextStyleObject(dataStr) ||[]
    const result:any[] = []
    newData.forEach((obj:any, indexObj:number) => {
      const textArr = obj.text.split('###')
      const len =  textArr.length
      textArr.map((item:any, aIndex:number) => {
        result.push({
          ...obj,
          text: item,
        })
        if(aIndex < len - 1){
          result.push({
            text: '%s%'
          })
        }
      })
    })
    
    let returnStr = ''
    result.forEach((item, i) => {
      if(item.text === '%s%'){
        returnStr += "__________";
      } else{
        let tempText = item.text;
        if(item.underline){
          tempText = `<u>${tempText}</u>`
        }
        if(item.bold){
          tempText = `<b>${tempText}</b>`
        }
        if(item.italic){
          tempText = `<i>${tempText}</i>`
        }
        returnStr += tempText;
      }
    })
    return returnStr
  }
  return (
    <div className={`a-multichoicequestion ${className}`} style={style}>
      <div className="a-multichoicequestion__title">
        <span
          dangerouslySetInnerHTML={{
            __html:formatData(),
          }}
        ></span>
      </div>
    </div>
  )
}
