import { useEffect, useState } from 'react'

import { Dropdown, Tooltip, Whisper } from 'rsuite'

import { DefaultPropsType, StructType } from 'interfaces/types'

interface PropsType extends DefaultPropsType {
  checkValue?: boolean
  data: StructType[]
  disabled?: boolean
  defaultValue?: StructType
  hiddenValue?: boolean
  isMultiChoice?: boolean
  label?: string
  name?: string
  placeholder?: string
  reverseData?: boolean
  triggerReset?: boolean
  toolTip?:string
  placement?: any
  trigger?: any
  onChange?: (val: any) => void
  shouldUpdateDefaultValue?: boolean
}

export const SelectPicker = ({
  className = '',
  checkValue = true,
  data,
  disabled = false,
  defaultValue = { code: '', display: '' },
  hiddenValue = false,
  isMultiChoice = false,
  label = '',
  name = '',
  placeholder = '',
  reverseData,
  triggerReset = false,
  style,
  toolTip = '',
  placement = 'topStart',
  trigger = 'hover',
  shouldUpdateDefaultValue = false,
  onChange = () => null,
}: PropsType) => {
  const [isFirstRender, setIsFirstRender] = useState(true)
  const [isFocus, setIsFocus] = useState(false)

  const [code, setCode] = useState(defaultValue.code as string | number)
  const [display, setDisplay] = useState(defaultValue.display)
  const [tmpCode, setTmpCode] = useState(defaultValue.code as string | number)
  const [tmpDisplay, setTmpDisplay] = useState(defaultValue.display)

  useEffect(() => {
    if (shouldUpdateDefaultValue) {
      setCode(defaultValue.code)
      setDisplay(defaultValue.display)
    }
  }, [shouldUpdateDefaultValue, defaultValue?.code])

  const handleItemSelect = (e: MouseEvent, item: StructType) => {
    if (isMultiChoice) e.stopPropagation()
    setTmpCode(code)
    setTmpDisplay(display)
    setTimeout(() => {
      onChange(item.code)
      setCode(item.code)
      setDisplay(item.display)
    }, 0)
  }

  useEffect(() => {
    setCode(tmpCode)
    setDisplay(tmpDisplay)
  }, [reverseData])

  // use for case reset trigger
  useEffect(() => {
    if (isFirstRender) {
      setIsFirstRender(false)
      return
    }
    setCode('')
    setDisplay('')
  }, [triggerReset])

  // console.log('defaultValue-------', defaultValue);
  return (
    <Whisper
      placement={placement}
      trigger={trigger}
      speaker={
        <Tooltip style={{ visibility: toolTip ? 'visible' : 'hidden' }}>
          {toolTip}
        </Tooltip>
      }
    >
    <div
      className={`a-select-picker ${className}`}
      data-focus={isFocus}
      style={style}
    >
      <input name={name} type="hidden" value={code} />
      {label && (
        <label className={isFocus || (checkValue && display) ? '--active' : ''}>
          <span>
            {isFocus || (checkValue && display) ? label : placeholder || label}
          </span>
        </label>
      )}

      <img
        className="a-select-picker__caret"
        src="/images/icons/ic-chevron-down-dark.png"
        alt="toggle"
      />
      <Dropdown
        className={`a-select-picker__dropdown ${
          data.length > 0 ? '' : '--hidden'
        }`}
        disabled={disabled}
        title={hiddenValue ? '' : display}
        onClose={() => setIsFocus(false)}
        onOpen={() => setIsFocus(true)}
        onSelect={(eventKey: any, e: any) => handleItemSelect(e, eventKey)}
      >
        {data.map((item) => (
          <Dropdown.Item key={item.code} eventKey={item} title={item.display}>
            {item.display}
          </Dropdown.Item>
        ))}
      </Dropdown>
    </div>
    </Whisper>
  )
}
