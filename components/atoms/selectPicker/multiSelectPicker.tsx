import { useEffect, useState } from 'react'

import { Dropdown, Popover, Tooltip, Whisper } from 'rsuite'

import { DefaultPropsType, StructType } from 'interfaces/types'

import { CheckBox } from '../checkbox'
import useTranslation from "@/hooks/useTranslation";

interface PropsType extends DefaultPropsType {
  checkValue?: boolean
  data: StructType[]
  defaultValue?: StructType[]
  disabled?: boolean
  displayAll?: boolean
  hiddenValue?: boolean
  isMultiChoice?: boolean
  label?: string
  menuSize?: 'md' | 'lg' | 'xl'
  name?: string
  placeholder?: string
  placement?:
    | 'bottomStart'
    | 'bottomEnd'
    | 'topStart'
    | 'topEnd'
    | 'leftStart'
    | 'leftEnd'
    | 'rightStart'
    | 'rightEnd'
  reverseData?: boolean
  triggerReset?: boolean
  onChange?: (val: any[]) => void
  toolTip?: string
  allIsEmpty?:boolean
  isError?:boolean
}

export const MultiSelectPicker = ({
  className = '',
  checkValue = true,
  data,
  defaultValue = [],
  disabled = false,
  displayAll = true,
  hiddenValue = false,
  label = '',
  menuSize = 'md',
  name = '',
  placeholder = '',
  placement = 'bottomStart',
  reverseData,
  triggerReset = false,
  style,
  onChange = () => null,
  toolTip,
  allIsEmpty = true,
  isError = false
}: PropsType) => {
  const { t } = useTranslation();
  const [isFocus, setIsFocus] = useState(false)
  const [isFirstRender, setIsFirstRender] = useState(true)
  const codeValue = defaultValue.map((item) => item.code) as any[]
  const displayValue = defaultValue.map((item) => item.display) as any[]
  
  const [code, setCode] = useState(
    codeValue.length==0?[]:codeValue,
  )
  const [display, setDisplay] = useState(
    displayValue.length==0?[]:displayValue,
  )
  const [tmpCode, setTmpCode] = useState(codeValue.length==0?[]:codeValue)
  const [tmpDisplay, setTmpDisplay] = useState(displayValue.length==0?[]:displayValue)
  const handleItemSelect = (
    e: MouseEvent,
    item: StructType,
    isAdd: boolean,
  ) => {
    e.stopPropagation()
    
    const newCode = isAdd
      ? [...code, item.code]
      : code.filter((codeItem) => codeItem !== item.code)
      if((code.length==0 || newCode.length == data.length)){
        setDisplay([])
      }
    const newDisplay = isAdd
      ? [...display, item.display]
      : display.filter((displayItem) => displayItem !== item.display)
  
      setTmpCode([...code])
      setTmpDisplay([...display])
      
      // if select all code
      if(newCode.length != data.length || !allIsEmpty){
        onChange([...newCode])
        setCode([...newCode])
        setDisplay([...newDisplay])
      }else{
        onChange([])
        setCode([])
        setDisplay([])
      }
  }

  const handleAllToggle = (e: any) => {
    e.stopPropagation()

    setTmpCode([...code])
    setTmpDisplay([...display])

    if(allIsEmpty){
      if (code.length > 0) {
        setCode([])
        setDisplay([])
      }
      onChange([])
    }else{
      const newCode = data.map(m=>m.code)
      const newDisplay = data.map(m=>m.display)
      setCode([...newCode])
      setDisplay([...newDisplay])
      onChange([...newCode])
    }
    
  }
  useEffect(() => {
    setCode(tmpCode)
    setDisplay(tmpDisplay)
  }, [reverseData])
  
  // use for case reset trigger
  useEffect(() => {
    if (isFirstRender) {
      setIsFirstRender(false)
      return
    }
    setCode([])
    setDisplay([])
  }, [triggerReset])

  return (
    <Whisper
      placement="topStart"
      trigger="hover"
      speaker={
        <Tooltip style={{ visibility: toolTip ? 'visible' : 'hidden' }}>
          {toolTip}
        </Tooltip>
      }
    >
      <div
        className={`a-select-picker ${className} ${isError ? 'error' : '' }`}
        title={display.join(', ')}
        style={disabled ? { ...style, cursor: 'not-allowed' } : style}
      >
        <input name={name} type="hidden" value={code.join(',')} />
        <label
          className={
            isFocus || (checkValue && display.length > 0) ? '--active' : ''
          }
        >
          <span>
            {isFocus || (checkValue && display) ? label : placeholder || label}
          </span>
        </label>
        <img
          className="a-select-picker__caret"
          src="/images/icons/ic-chevron-down-dark.png"
          alt="toggle"
        />
        <Dropdown
          style={{ pointerEvents: disabled ? 'none' : 'auto' }}
          className={`a-select-picker__dropdown ${
            menuSize !== 'md' ? `dropdown-menu-${menuSize}` : ''
          } ${data.length > 0 ? '' : '--hidden'} ${isFocus ? '--focus' : ''}`}
          disabled={disabled}
          placement={placement}
          title={hiddenValue && display.length <= 0 ? '' : display.join(', ')}
          onClose={() => setIsFocus(false)}
          onOpen={() => setIsFocus(true)}
          onSelect={(eventKey: any, e: any) =>
            eventKey.code === 'all'
              ? handleAllToggle(e)
              : handleItemSelect(e, eventKey, !code.includes(eventKey.code))
          }
        >
          {displayAll && data.length > 0 && (
            <Dropdown.Item
              key="all"
              className="a-select-picker__dropdown-item"
              eventKey={{ code: 'all', display: '' }}
              style={
                code.length <= 0
                  ? { background: '#fff', cursor: 'no-drop' }
                  : null
              }
            >
              <CheckBox
                className="dropdown-item__checkbox"
                checked={code.length <= 0 || code.length == data.length}
                disabled={code.length <= 0 || code.length == data.length}
                onChange={handleAllToggle}
              />
              <span>{t('common')['all']}</span>
            </Dropdown.Item>
          )}
          {data.map((item) => (
            <Dropdown.Item
              key={item.code}
              className="a-select-picker__dropdown-item"
              eventKey={item}
            >
              <CheckBox
                className="dropdown-item__checkbox"
                checked={(code.length == 0 && allIsEmpty) ||  code.includes(item.code)}
                onChange={(e: any) =>
                  handleItemSelect(e, item, !code.includes(item.code))
                }
              />
              <span>{item.display}</span>
            </Dropdown.Item>
          ))}
        </Dropdown>
      </div>
    </Whisper>
  )
}
