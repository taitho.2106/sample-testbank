import { useContext, useEffect, useRef, useState } from 'react'

import { useSession } from 'next-auth/client'
import { Tooltip, Whisper } from 'rsuite'

import { filterCertificate, filterInternationalType, getDefaultCertificate } from '@/utils/certificate/filterCertificate'
import { filterSeriesCategory } from '@/utils/series/filterSeriesCategory'
import { paths } from 'api/paths'
import { callApi } from 'api/utils'
import useGrade from 'hooks/useGrade'
import { SingleTemplateContext, WrapperContext } from 'interfaces/contexts'
import { UNIT_TYPE } from 'interfaces/struct'
import { getDefaultUnitTestGroup } from 'utils/series/filterSeriesByGradeAndType'

import { UNIT_TEST_GROUP } from '../../../constants'
import useNoti from '../../../hooks/useNoti'
import { DefaultPropsType, StructType } from '../../../interfaces/types'
import { CheckBox } from '../checkbox'
import { DateInput } from '../dateInput'
import { InputWithLabel } from '../inputWithLabel'
import { SelectFilterPicker } from '../selectFilterPicker'
import { SelectPicker } from '../selectPicker'
import useTranslation from "@/hooks/useTranslation";

type PropType = DefaultPropsType & {
  isCopy?: boolean,
  isChangeData?: boolean
}
export const UnittestInfoCard = ({ className = '', style, isCopy = false , isChangeData}: PropType) => {
  const { t, subPath } = useTranslation()
  const [getSeries, setGetSeries] = useState([])
  const [seriesGroup, setSeriesGroup] = useState(0)
  const [internationalType, setInternationalType] = useState(0)
  const { getNoti } = useNoti()
  const { getGrade } = useGrade()
  const { globalModal } = useContext(WrapperContext)
  const { chosenTemplate, mode } = useContext(SingleTemplateContext)
  const [session] = useSession()
  const user: any = session?.user;
  const isShowPrivacy = user.is_admin == 1 && chosenTemplate.state?.scope == 0;
  const defaultId = chosenTemplate.state?.id || null
  const defaultUserId = chosenTemplate.state?.author || null
  const defaultName = chosenTemplate.state?.name || ''
  const defaultTime = chosenTemplate.state?.time || ''
  const defaultTotalQuestion = chosenTemplate.state?.totalQuestion || ''
  const defaultPoint = chosenTemplate.state?.totalPoints || ''
  const defaultUnitType = UNIT_TYPE.find((item) => item.code === chosenTemplate.state?.unitType) || {
    code: '0',
    display: t('unit-type')['practice'],
    key: t('unit-type')['practice'],
  }

  const defaultDate = chosenTemplate.state?.date || [null, null]

  const dataCollection = async () => {
    const param = chosenTemplate.state?.templateLevelId && seriesGroup != 0 ? `${paths.api_series}?groups=${seriesGroup}&grade=${chosenTemplate.state?.templateLevelId}` : `${paths.api_series}?groups=1,2`
    const response: any = await callApi(`${param}`, 'get', 'token');
    if (response?.data) {
      const result = response?.data.map((item: any) => {
        return {
          code: item.id,
          display: item.name,
          imageUrl: item.image,
          type: item.type,
          grade_eduhome_id: item.grade_eduhome_id,
          grade_id: item.grade_id,
          group: item.group
        }
      })
      setGetSeries(result)
    }
  }

  useEffect(() => {
    dataCollection()
  }, [seriesGroup, chosenTemplate.state?.templateLevelId])

  const [defaultUnitTestGroup, setDefaultUnitTestGroup] = useState({ code: 0, display: '' })
  const defaultCertificate: any = filterInternationalType().find((item) => item.code === getDefaultCertificate(chosenTemplate.state?.unit_test_type))
  // console.log('defaultUnitTestGroup-----', defaultUnitTestGroup)
  // console.log('defaultCertificate-----', defaultCertificate)

  const infoCardDiv = document.querySelector(".a-template-info-card")?.getBoundingClientRect()?.right || 0


  useEffect(() => {
    if (getSeries.length > 0 && !defaultUnitTestGroup.code) {
      const unitGroup: any = seriesGroupType.find((item) => item.code === getDefaultUnitTestGroup(chosenTemplate.state?.series_id, getSeries))
      if (unitGroup) {
        setSeriesGroup(unitGroup.code);
        setDefaultUnitTestGroup(() => ({ ...unitGroup,display: t(unitGroup?.display) }))
      }
    }
    if (chosenTemplate.state?.is_international_exam === 1) {
      setDefaultUnitTestGroup({
        code: 3,
        display: t('international-exam'),
      })
      if (defaultCertificate) {
        setInternationalType(defaultCertificate.code)
      }
    }
  }, [getSeries, chosenTemplate.state?.series_id, defaultUnitTestGroup.code]);

  const defaultScope = chosenTemplate.state?.scope || 0
  const [triggerEndDate, setTriggerEndDate] = useState(null)
  const seriesFilterRef = useRef(null)
  

  const getTotalData = () => {
    let currentTotalQuestion = 0
    let currentPoint = 0
    if (chosenTemplate.state?.sections) {
      chosenTemplate.state.sections.forEach((item: any) => {
        const countTotal = item.parts[0]?.totalQuestion || 0
        const countPoint = item.parts[0]?.points || 0
        currentTotalQuestion += countTotal
        currentPoint += countPoint
      })
    }
    return {
      currentTotalQuestion,
      currentPoint
    }
  }

  const total = getTotalData()

  const getShareUrl = () => {
    if (!defaultId || !defaultUserId) return ''
    const protocol = window.location.protocol
    const hostname = window.location.hostname
    const port = hostname === 'localhost' ? `:${window.location.port}` : ''
    return `${protocol}//${hostname}${port}${subPath}/practice-test/${defaultUserId}===${defaultId}`
  }

  const handleNameChange = (str: string) => {
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    detail.name = str
    chosenTemplate.setState({ ...detail })
  }

  const handleTimeChange = (str: string) => {
    const parseValue = parseInt(str || '0')
    if (parseValue <= 0) getNoti('error', 'topCenter', t('common-get-noti')['time-change-validate'])
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    detail.time = parseValue
    chosenTemplate.setState({ ...detail })
    if (chosenTemplate.state?.unitType === '1')
      if (parseValue && chosenTemplate.state?.date[0]) {
        handleEndDateChange(new Date(chosenTemplate.state.date[0]).getTime() + parseValue * 60 * 1000)
        setTriggerEndDate(new Date(new Date(chosenTemplate.state.date[0]).getTime() + parseValue * 60 * 1000))
      }
  }

  const handleTotalQuestionChange = (value: any) => {
    const parseValue = parseInt(value || '0')
    if (parseValue <= 0) getNoti('error', 'topCenter', t('common-get-noti')['total-question-validate'])
    const detail = chosenTemplate.state || {}
    detail.totalQuestion = parseValue
    chosenTemplate.setState({ ...detail })
  }
  const handlePrivacyChange = () => {
    const detail = chosenTemplate.state || {}
    detail.privacy = detail.privacy == 1 ? null : 1
    chosenTemplate.setState({ ...detail })
  }

  const handleGradeChange = (value: any) => {
    // if (chosenTemplate.state?.templateLevelId && chosenTemplate.state.templateLevelId !== value) {
    //   globalModal.setState({
    //     id: 'confirm-modal',
    //     type: 'change-grade',
    //     content: {
    //       closeText: 'Giữ lại đề thi',
    //       submitText: 'Đổi khối lớp',
    //       onClose: () => {
    //         let detail = chosenTemplate.state
    //         if (!detail) detail = {}
    //         chosenTemplate.setState({ ...detail })
    //       },
    //       onSubmit: () => {
    //         let detail = chosenTemplate.state
    //         if (!detail) detail = {}
    //         detail.templateLevelId = value
    //         detail.sections.forEach((section: any) => {
    //           section.parts[0].questions = []
    //         })
    //         chosenTemplate.setState({ ...detail })
    //       },
    //     },
    //   })
    //   return
    // } else {
    //   let detail = chosenTemplate.state
    //   if (!detail) detail = {}
    //   detail.templateLevelId = value
    //   chosenTemplate.setState({ ...detail })
    // }
    if (chosenTemplate.state?.templateLevelId && chosenTemplate.state.templateLevelId !== value) {
      let detail = chosenTemplate.state
      if (!detail) detail = {}
      detail.templateLevelId = value
      detail.sections.forEach((section: any) => {
        section.parts[0].questions = []
      })
      chosenTemplate.setState({ ...detail })
    } else {
      let detail = chosenTemplate.state
      if (!detail) detail = {}
      detail.templateLevelId = value
      chosenTemplate.setState({ ...detail })
    }
  }

  const handleInternationalType = (value: any) => {
    setInternationalType(value)
  }

  const handleUnitTypeChange = (value: string | number | null) => {
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    detail.unitType = value
    chosenTemplate.setState({ ...detail })
    if (value === '1') {
      if (chosenTemplate.state?.time) {
        const parseTime = parseInt(chosenTemplate.state.time)
        if (chosenTemplate.state?.date[0]) {
          handleEndDateChange(new Date(chosenTemplate.state.date[0]).getTime() + parseTime * 60 * 1000)
          setTriggerEndDate(new Date(new Date(chosenTemplate.state.date[0]).getTime() + parseTime * 60 * 1000))
        }
      }
    }
  }

  const handleUnitTestType = (value: any) => {
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    if (value != detail.unit_test_type) {
      detail.unit_test_type = value
      chosenTemplate.setState({ ...detail })
    }
  }

  const handleSeriesGroupChange = (value: any) => {
    setSeriesGroup(value)
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    if (value === 1 || value === 2) {
      detail.is_international_exam = 0
      chosenTemplate.setState({ ...detail })
    } else {
      detail.is_international_exam = 1
      chosenTemplate.setState({ ...detail })
    }
  }

  const handleSeriesChange = (value: any) => {
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    if (value != detail.series_id) {
      detail.series_id = value
      chosenTemplate.setState({ ...detail })
    }
  }

  const handlePointChange = (str: string) => {
    const parseValue = parseFloat(str || '0')
    if (parseValue <= 0) getNoti('error', 'topCenter', t('common-get-noti')['point-validate'])
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    detail.point = parseValue
    detail.totalPoints = parseValue
    chosenTemplate.setState({ ...detail })
  }

  const handleStartDateChange = (timestamp: number) => {
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    const currentDateValue: any[] = detail.date || [null, null]
    currentDateValue[0] = new Date(timestamp).toString();
    detail.date = currentDateValue
    chosenTemplate.setState({ ...detail })

    if (chosenTemplate.state?.unitType === '1') {
      if (chosenTemplate.state.time && timestamp) {
        handleEndDateChange(timestamp + chosenTemplate.state.time * 60 * 1000)
        setTriggerEndDate(new Date(timestamp + chosenTemplate.state.time * 60 * 1000))
      }
    } else {
      if (currentDateValue[1]) {
        if (new Date(currentDateValue[0]).getTime() > new Date(currentDateValue[1]).getTime()) {
          getNoti('error', 'topCenter', t('common-get-noti')['date-time'])
        }
      }
    }
  }

  const handleEndDateChange = (timestamp: number) => {
    let detail = chosenTemplate.state
    if (!detail) detail = {}
    const currentDateValue: any[] = detail.date || [null, null]
    currentDateValue[1] = new Date(timestamp).toString();
    detail.date = currentDateValue
    if (currentDateValue[0]) {
      if (new Date(currentDateValue[0]).getTime() > new Date(currentDateValue[1]).getTime()) {
        getNoti('error', 'topCenter', t('common-get-noti')['date-time'])
      }
    }
    chosenTemplate.setState({ ...detail })
  }

  const checkActive = () => {
    const detail = chosenTemplate.state
    if (!detail?.date || detail.date.length < 1) return false
    const startDate = detail.date[0]
    const endDate = detail.date[1]
    if (!startDate) return false
    const d = new Date()
    const currentTime = d.getTime()
    const startStamp = new Date(startDate).getTime()
    if (currentTime < startStamp) return false
    if (endDate) {
      const endStamp = new Date(endDate).getTime()
      if (currentTime > endStamp) return false
    }
    return true
  }

  if (['detail', 'update'].includes(mode) && !chosenTemplate.state) return <></>

  // console.log('chosenTemplate=============', chosenTemplate.state)

  let defaultValueEnd = null
  if (defaultUnitType?.code === '1') {
    if (defaultDate[0]) {
      defaultValueEnd = new Date(new Date(defaultDate[0]).getTime() + defaultTime * 60 * 1000)
    } else {
      defaultValueEnd = null
    }
  } else {
    defaultValueEnd = defaultDate[1] ? new Date(defaultDate[1]) : null
  }

  const onConfirmChangeGrade = async (value: any) => {
    return await new Promise<boolean>((resolve) => {
      if (chosenTemplate.state?.templateLevelId && chosenTemplate.state.templateLevelId !== value) {
        globalModal.setState({
          id: 'confirm-modal',
          type: 'change-grade',
          content: {
            closeText: t("modal-change-grade")['cancel-action'],
            submitText: t("modal-change-grade")['submit-action'],
            onClose: () => {
              resolve(false)
            },
            onSubmit: () => {
              resolve(true)
            },
          },
        })
      } else {
        resolve(true)
      }
    });
  }

  const onKeyDownInputNumber = (e: any, val: any) => {
    const key = e?.key?.toLowerCase();
    if (["e", "+", "-"].includes(key)) {
      e.preventDefault();
      return;
    }
    if (!val && key == "0") {
      e.preventDefault();
      return;
    }
  }

  const isDisabled = mode === 'detail' || !isChangeData

  const isActive = checkActive()

  return (
    <div className={`a-template-info-card ${className}`} style={style}>
      <div className="a-template-info-card__container">
        <InputWithLabel
          className="mainbox"
          defaultValue={defaultName}
          disabled={mode === 'detail'}
          label={t('common')['unit-test-name']}
          type="text"
          maxLength={100}
          onBlur={handleNameChange}
          style={{ zIndex: 7 }}
          status={
            !!chosenTemplate.state?.name.length ? 'default'
              : 'danger'
          }
        />
        {/* {getGrade.length > 0 && (<SelectPicker
          className="midbox"
          data={getGrade}
          defaultValue={defaultGrade}
          disabled={mode === 'detail'}
          label="Khối lớp"
          reverseData={gradeReverse}
          onChange={handleGradeChange}
          style={{ zIndex: 6 }}
        />)} */}

        <InputWithLabel
          className="subbox"
          decimal={0}
          defaultValue={defaultTime}
          disabled={isDisabled}
          label={t('common')['time']}
          max={32767}
          min={1}
          type="number"
          onBlur={handleTimeChange}
          style={{ zIndex: 6 }}
          toolTip={t('common-whisper')['time-required']}
          status={
            chosenTemplate.state?.time != 0 ? 'default'
              : 'danger'
          }
          onkeyDown={onKeyDownInputNumber}
        />
        <InputWithLabel
          className="midbox"
          defaultValue={defaultTotalQuestion}
          disabled={isDisabled}
          min={0}
          placeholder={t("common")['total-question']}
          toolTip={t("common-whisper")['total-question-required']}
          status={
            Math.round(chosenTemplate.state?.totalQuestion) === Math.round(total.currentTotalQuestion) ? 'default' : 'danger'
          }
          type="number"
          onkeyDown={onKeyDownInputNumber}
          style={{ zIndex: 6 }}
          onBlur={handleTotalQuestionChange}
          customLabel={
            <span>
              {t("common")['total-question']}{' '}
              <TooltipNumberBadge
                tooltip={
                  Math.round(chosenTemplate.state?.totalQuestion) === Math.round(total.currentTotalQuestion)
                    ? t("common-whisper")['total-question-validate']
                    : t("common-whisper")['total-question-error']
                }
                type={
                  Math.round(chosenTemplate.state?.totalQuestion) === Math.round(total.currentTotalQuestion)
                    ? 'success'
                    : 'danger'
                }
                value={Math.round(total.currentTotalQuestion)}
              />
            </span>
          }
        />
        <InputWithLabel
          className="midbox"
          decimal={2}
          defaultValue={defaultPoint}
          disabled={isDisabled}
          min={0}
          placeholder={t('common')['total-point']}
          toolTip={t('common-whisper')['point-required']}
          status={
            [
              Math.round(chosenTemplate.state?.point * 100) / 100,
              Math.round(chosenTemplate.state?.totalPoints * 100) / 100,
            ].includes(Math.round(total.currentPoint * 100) / 100)
              ? 'default'
              : 'danger'
          }
          type="number"
          onkeyDown={onKeyDownInputNumber}
          // style={{ zIndex: 3 }}
          onBlur={handlePointChange}
          customLabel={
            <span>
              {t('common')['total-point']}{' '}
              <TooltipNumberBadge
                tooltip={
                  [
                    Math.round(chosenTemplate.state?.point * 100) / 100,
                    Math.round(chosenTemplate.state?.totalPoints * 100) / 100,
                  ].includes(Math.round(total.currentPoint * 100) / 100)
                    ?  t("common-whisper")['point-validate']
                    : t("common-whisper")['point-error']
                }
                type={
                  [
                    Math.round(chosenTemplate.state?.point * 100) / 100,
                    Math.round(chosenTemplate.state?.totalPoints * 100) / 100,
                  ].includes(Math.round(total.currentPoint * 100) / 100)
                    ? 'success'
                    : 'danger'
                }
                value={Math.round(total.currentPoint * 100) / 100}
              />
            </span>
          }
        />
        <SelectPicker
          className="midbox"
          data={UNIT_TYPE.map(item => ({...item, display: t('unit-type')[item.key]}))}
          defaultValue={{ ...defaultUnitType, display: t('unit-type')[defaultUnitType.key] }}
          disabled={mode === 'detail'}
          label={t('common')['unit-type']}
          onChange={handleUnitTypeChange}
          style={{ zIndex: 6 }}
        />
        <DateInput
          className="midbox time"
          defaultValue={defaultDate[0] ? new Date(defaultDate[0]) : null}
          disabled={mode === 'detail'}
          label={t('common')['start-date']}
          time={true}
          style={{ zIndex: 5 }}
          onChange={handleStartDateChange}
          toolTip={t('common-whisper')["start-date-required"]}
          isShowErr={true}
          errorText='Vui lòng chọn thời gian'
        />
        <DateInput
          className="midbox time"
          defaultValue={defaultValueEnd || null}
          disabled={mode === 'detail' || chosenTemplate.state?.unitType === '1' ? true : false}
          label={t('common')['end-date']}
          toolTip={
            mode === 'view' || chosenTemplate.state?.unitType === '0'
              ? t('common-whisper')["end-date-required"]
              : t('common-whisper')["end-date-error"]
          }
          time={true}
          triggerSetValue={triggerEndDate}
          style={{ zIndex: 5 }}
          onChange={(timestamp) =>
            !(mode === 'detail' || chosenTemplate.state?.unitType === '1') && handleEndDateChange(timestamp)
          }
          isShowErr={true}
          errorText='Vui lòng chọn thời gian'
        />

        {!isCopy && (
          <>
            {getGrade.length > 0 && chosenTemplate.state?.is_international_exam === 0 && (
              <SelectFilterPicker
                data={getGrade}
                isMultiChoice={false}
                inputSearchShow={false}
                isShowSelectAll={false}
                onChange={handleGradeChange}
                className="midbox"
                label={t('common')['grade']}
                disabled={mode === 'detail' || isCopy}
                defaultValue={chosenTemplate.state?.templateLevelId}
                onConfirmChange={onConfirmChangeGrade}
                style={{ zIndex: 4 }}
              />
            )}
            <SelectPicker
              className="midbox group"
              data={seriesGroupType.map((item)=> ({...item, display: t(item.display)}))}
              onChange={handleSeriesGroupChange}
              label={t('common')['series-group-type']}
              disabled={mode === 'detail' || chosenTemplate.state?.is_international_exam === 1}
              style={{ zIndex: 4 }}
              defaultValue={defaultUnitTestGroup}
              shouldUpdateDefaultValue={true}
            />
            {getSeries && chosenTemplate.state?.is_international_exam != 1 && seriesGroup != 0 && (
              <SelectFilterPicker
                data={getSeries}
                isMultiChoice={false}
                inputSearchShow={true}
                isShowSelectAll={false}
                onChange={handleSeriesChange}
                className="midbox series"
                label={t('back-to-school-banner')['program']}
                menuSize='xxl'
                disabled={mode === 'detail'}
                defaultValue={chosenTemplate.state?.series_id}
                style={{ zIndex: 3 }}
              />
            )}
            {chosenTemplate.state?.is_international_exam != 1 && seriesGroup != 0 && (
              <SelectFilterPicker
                className="midbox group"
                data={filterSeriesCategory(t)}
                isMultiChoice={false}
                inputSearchShow={false}
                isShowSelectAll={false}
                onChange={handleUnitTestType}
                label={t('common')['unit-test-type']}
                disabled={mode === 'detail' || !chosenTemplate.state?.series_id}
                defaultValue={chosenTemplate.state?.unit_test_type}
                style={{ zIndex: 3 }}
                parentPosition={infoCardDiv}
              />
            )}

            {chosenTemplate.state?.is_international_exam === 1 && (
              <>
                <SelectPicker
                  className="midbox"
                  data={filterInternationalType()}
                  onChange={handleInternationalType}
                  label={t('table')['cefr']}
                  style={{ zIndex: 3 }}
                  disabled={mode === 'detail'}
                  defaultValue={defaultCertificate}
                />
                <SelectFilterPicker
                  className="midbox group"
                  data={filterCertificate(internationalType)}
                  isMultiChoice={false}
                  inputSearchShow={false}
                  isShowSelectAll={false}
                  onChange={handleUnitTestType}
                  label={t('common')['format']}
                  disabled={mode === 'detail'}
                  defaultValue={chosenTemplate.state?.unit_test_type}
                  style={{ zIndex: 3 }}
                  parentPosition={infoCardDiv}
                />
              </>
            )}
          </>
        )}

        <Whisper
          placement="top"
          trigger="hover"
          speaker={
            <Tooltip>
              {t('common-whisper')['check-active']}
            </Tooltip>
          }
        >
          <div className="__checkbox">
            <CheckBox checked={isActive} disabled={true} />
            <span>{t('common')['opening']}</span>
          </div>
        </Whisper>
        {isShowPrivacy && <Whisper
          placement="top"
          trigger="hover"
          speaker={
            <Tooltip>
              {t('common-whisper')['privacy-unit-test']}
            </Tooltip>
          }
        >
          <div className="__checkbox" style={{ marginLeft: "3rem" }}>
            <CheckBox checked={chosenTemplate?.state?.privacy == 1}
              disabled={mode === 'detail'}
              onChange={handlePrivacyChange} />
            <span>{t('common')['private']}</span>
          </div>
        </Whisper>}
      </div>
      {isActive && ['detail', 'update'].includes(mode) && (
        <InputWithLabel
          className="mainbox --cursor-pointer"
          defaultValue={getShareUrl()}
          disabled={false}
          iconPlacement="end"
          label="URL"
          type="text"
          readOnly={true}
          style={{ maxWidth: '50rem', marginBottom: '1.6rem' }}
          onClick={() => {
            navigator.clipboard.writeText(getShareUrl())
            getNoti('success', 'topCenter', t("common-get-noti")['copy-url'])
          }}
          icon={<img src="/images/icons/ic-copy-active.png" alt="url" />}
        />
      )}
    </div>
  )
}

interface TooltipNumberBadgeProps {
  tooltip: string
  type: 'danger' | 'success'
  value: number
}

const TooltipNumberBadge = ({ tooltip, type, value }: TooltipNumberBadgeProps) => {
  return (
    <Whisper
      controlId="control-id-hover"
      placement="topEnd"
      trigger="hover"
      speaker={
        <Tooltip className="tooltip-number-badge-input" data-type={type}>
          {tooltip}
        </Tooltip>
      }
    >
      <span
        style={{
          marginLeft: '-1rem',
          padding: '1rem 0 1rem 1rem',
          color: type === 'success' ? '#568745' : '#c96951',
          cursor: 'pointer',
          pointerEvents: 'all',
        }}
      >
        {(value || value == 0) && <>({value})</>}
      </span>
    </Whisper>
  )
}

export const seriesGroupType: StructType[] = [
  {
    display: 'textbook',
    code: UNIT_TEST_GROUP.TEXTBOOK
  },
  {
    display: 'supplement-book',
    code: UNIT_TEST_GROUP.TEXTBOOK_SUPPLEMENT
  },
]