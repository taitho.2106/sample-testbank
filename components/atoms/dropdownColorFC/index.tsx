import { Popover, Whisper } from 'rsuite'

import { DefaultPropsType } from '../../../interfaces/types'
type DataColorType = {
  value:string,
  label: JSX.Element
}
interface DroupDownColorPropsType extends DefaultPropsType {
  data: DataColorType[]
  value?: string
  onChangeColor: (value: string) => void
  disable?:boolean
}
export const DropdownColorFC = ({ data = [], value = '', onChangeColor,disable, children, style={} }: DroupDownColorPropsType) =>{
  return (
    <Whisper
      placement="bottom"
      controlId="dropdown-color-fc-id"
      trigger="click"
      speaker={
        <Popover>
          <div className="dropdown-color" style={style}>
            {data.map((m) => {
              const isSelected = m.value == value
              return (
                <div className={`dropdown-color__item-color${isSelected?" --selected ": ""} ${disable?" --disable ":""}`} onClick={() => !disable && onChangeColor(m.value)} key={m.value}>
                  {m.label}
                </div>
              )
            })}
          </div>
        </Popover>
      }
    >
      <div>{children}</div>
    </Whisper>
  )
}
