import { DefaultPropsType } from 'interfaces/types'

interface Props extends DefaultPropsType {
  data?: string
}

export const TrueFalseImage = ({ className = '', data, style }: Props) => {
  return (
    <div className={`a-true-false-image ${className}`} style={style}>
      <div className="a-true-false-image__thumbnail">
        <img src={data} alt="banner" />
      </div>
    </div>
  )
}