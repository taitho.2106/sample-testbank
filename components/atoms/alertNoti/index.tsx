import { forwardRef } from 'react'

import { Message } from 'rsuite'

interface PropsType {
  message: string
  type: 'success' | 'warning' | 'error' | 'info'
  duration?: number
}

export const AlertNoti = forwardRef((props: PropsType,ref:any) => {
  const {message,type, duration = 2000}= props
  return (
      <Message
        className="a-alert-noti"
        duration={duration}
        showIcon
        type={type}
        style={{ minWidth: 320, padding: 10 }}
      >
        {message}
      </Message>
  )
})
AlertNoti.displayName = ''
