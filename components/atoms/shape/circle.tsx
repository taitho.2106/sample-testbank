import { DefaultPropsType } from '../../../interfaces/types'
interface Props extends DefaultPropsType {
  fill?: string
  height?: number
  stroke?: string
  strokeWidth?: number
  strokeDasharray?: string
}

export const CircleShape = ({
  className = '',
  fill,
  stroke,
  height,
  style,
  strokeWidth,
  strokeDasharray,
}: Props) => {
  height = height || 16
  const width = height
  strokeWidth = strokeWidth || 0
  return (
    <svg
      pointerEvents='none'
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={`${width}px`}
      height={`${height}px`}
      viewBox="0 0 16 16"
      style={
        style ? { ...style, overflow: 'visible' } : { overflow: 'visible' }
      }
    >
      <path
        width={width}
        height={height}
        fill={fill || '#ff0000'}
        strokeWidth={strokeWidth}
        strokeLinecap="round"
        strokeDasharray={strokeDasharray || '0,0'}
        stroke={stroke || '#000000'}
        d="M16,8c0,4.4-3.6,8-8,8s-8-3.6-8-8s3.6-8,8-8S16,3.6,16,8z"
      />
    </svg>
  )
}
