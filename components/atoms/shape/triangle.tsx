import { DefaultPropsType } from '../../../interfaces/types'
interface Props extends DefaultPropsType {
  fill?: string
  height?: number
  stroke?: string
  strokeWidth?:number
  strokeDasharray?: string
}

export const TriangleShape = ({
  className = '',
  fill,
  stroke,
  strokeWidth,
  height,
  style,
  strokeDasharray
}: Props) => {
  height = height || 16
  const width = height *(18.5/16) 
  strokeWidth = strokeWidth||0
  return (
    <svg
      pointerEvents='none'
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={`${width}px`}
      height={`${height}px`}
      viewBox="0 0 18.5 16"
      style={style?{...style, overflow:'visible'}:{overflow:'visible'}}
    >
      <defs>
</defs>
      <path
      width={width}
      height={height}
      fill={fill||"#ff0000"}
      strokeWidth={strokeWidth}
      stroke={stroke||"#000000"}
      strokeLinecap="round"
      strokeDasharray={strokeDasharray || '0,0'}
      d="M18.5,16H0L9.2,0L18.5,16z"
      />
    </svg>
  )
}
