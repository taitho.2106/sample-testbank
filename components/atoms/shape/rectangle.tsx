import { DefaultPropsType } from '../../../interfaces/types'
interface Props extends DefaultPropsType {
  fill?: string
  height?: number
  stroke?: string
  strokeWidth?:number
  strokeDasharray?:string
}

export const RectangleShape = ({
  className = '',
  fill,
  stroke,
  strokeWidth,
  height,
  strokeDasharray,
  style,
}: Props) => {
  height = height || 16
  const width = height *(21.3/16)
  strokeWidth = strokeWidth||0
  return (
    <svg
      pointerEvents='none'
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={`${width}px`}
      height={`${height}px`}
      viewBox="0 0 21.3 16"
      style={style?{...style, overflow:'visible'}:{overflow:'visible'}}
    >
      <defs>
</defs>
      <path
      width={width}
      height={height}
      fill={fill||"#ff0000"}
      strokeWidth={strokeWidth}
      strokeLinecap="round"
      strokeDasharray={strokeDasharray || '0,0'}
      stroke={stroke||"#000000"}
      d="M21.3,16H0V0h21.3V16z"
      />
    </svg>
  )
}
