import { DefaultPropsType } from '../../../interfaces/types'
interface Props extends DefaultPropsType {
  fill?: string
  height?: number
  stroke?: string
  strokeWidth?:number
}

export const DefaultShape = ({
  className = '',
  fill,
  stroke,
  height,
  style,
  strokeWidth
}: Props) => {
    height = height || 16
    const width = height *(22.3/16.5)
  strokeWidth = strokeWidth||0
  return (
    <svg
      pointerEvents='none'
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={`${width}px`}
      height={`${height}px`}
      viewBox="0 0 22.3 16.5"
      style={style?{...style, overflow:'visible'}:{overflow:'visible'}}
    >
      <g>
	<rect fill='#D4DDE7' width={height/2+2} height={height/2+2}/>
	<circle stroke='#FDFDFF' fill='#D4DDE7' cx={width/2} cy={height/2} r={(height-3)/2 -2}/>
</g>
    </svg>
  )
}
