import { DefaultPropsType } from '../../../interfaces/types'
interface Props extends DefaultPropsType {
  fill?: string
  height?: number
  stroke?: string
  strokeWidth?:number
}

export const IcFillShape = ({
  className = '',
  fill,
  stroke,
  strokeWidth,
  height,
  style,
}: Props) => {
  height = height || 16
  const width = height *(12/16)
  strokeWidth = strokeWidth||0
  return (
    <svg
      pointerEvents='none'
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={`${width}px`}
      height={`${height}px`}
      viewBox="0 0 12 16"
      style={style?{...style, overflow:'visible'}:{overflow:'visible'}}
    >
      <defs>
</defs>
      <path
      fill={fill||"#d4dde7"}
      strokeWidth={strokeWidth}
      stroke={stroke||"#000000"}
      d="M6,2L5,0H4L3,2L2,0H1.5C0.7,0,0,0.7,0,1.5V8h12V1.5C12,0.7,11.3,0,10.5,0H7L6,2z"
      />
      <path fill={`${fill?"#738396":"#d4dde7"}`}  d="M0,9v1c0,1.1,0.9,2,2,2h2v2c0,1.1,0.9,2,2,2s2-0.9,2-2v-2h2c1.1,0,2-0.9,2-2V9H0z"/>
      {/* <circle cx={width/2} cy="14" r="0.5"/> */}
    </svg>
  )
}
