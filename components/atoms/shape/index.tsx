import { CircleShape } from "./circle"
import { OvalShape } from "./oval"
import { RectangleShape } from "./rectangle"
import { SquareShape } from "./square"
import { StarShape } from "./star"
import { TriangleShape } from "./triangle"
import { DefaultShape } from "./default"
import { IcFillShape } from "./icFill"

const ShapeComponent = {CircleShape,OvalShape,SquareShape,RectangleShape,TriangleShape,StarShape,DefaultShape,IcFillShape}

export default ShapeComponent;
