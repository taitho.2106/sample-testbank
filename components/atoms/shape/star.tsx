import { DefaultPropsType } from '../../../interfaces/types'
interface Props extends DefaultPropsType {
  fill?: string
  height?: number
  stroke?: string
  strokeWidth?:number
  strokeDasharray?: string
}

export const StarShape = ({
  className = '',
  fill,
  stroke,
  strokeWidth,
  height,
  style,
  strokeDasharray
}: Props) => {
  height = height || 16
  const width = height *(16.8/16)
  strokeWidth = strokeWidth||0
  return (
    <svg
      pointerEvents='none'
      version="1.1"
      xmlns="http://www.w3.org/2000/svg"
      x="0px"
      y="0px"
      width={`${width}px`}
      height={`${height}px`}
      viewBox="0 0 16.8 16"
      style={style?{...style, overflow:'visible'}:{overflow:'visible'}}
    >
      <defs>
</defs>
      <path
      width={width}
      height={height}
      fill={fill||"#ff0000"}
      strokeWidth={strokeWidth}
      stroke={stroke||"#000000"}
      strokeLinecap="round"
      strokeDasharray={strokeDasharray || '0,0'}
      d="M8.4,0L11,5.3l5.8,0.8l-4.2,4.1l1,5.8l-5.2-2.7L3.2,16l1-5.8L0,6.1l5.8-0.8L8.4,0z"
      />
    </svg>
  )
}
