import { Tooltip, Whisper } from 'rsuite'
import { DefaultPropsType } from '../../../interfaces/types'

interface Props extends DefaultPropsType {
  checked?: boolean
  disabled?: boolean
  name?: string
  onChange?: (e: any) => void
  toolTip?: string
}

export const CheckBox = ({
  className = '',
  checked = false,
  disabled = false,
  name = '',
  style,
  onChange = () => null,
  toolTip,
}: Props) => {
  return (
    <Whisper
      placement="topStart"
      trigger="hover"
      speaker={
        <Tooltip style={{visibility: toolTip?"visible":"hidden"}}>{toolTip}</Tooltip>
      }
    >
      <div
        className={`a-checkbox ${className}`}
        data-checked={checked}
        data-disabled={disabled}
        style={style}
      >
        <input
          type="checkbox"
          name={name}
          checked={checked}
          disabled={disabled}
          onChange={(e: any) => onChange(e)}
        />
      </div>
    </Whisper>
  )
}
