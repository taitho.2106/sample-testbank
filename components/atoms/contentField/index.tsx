import {
  useRef,
  forwardRef,
  useImperativeHandle,
  MouseEvent,
  FocusEvent,
} from 'react'
import { useState } from 'react'
import { useEffect } from 'react'
import { useCallback } from 'react'
import { CSSProperties, KeyboardEvent } from 'react'

import ContentEditable, { ContentEditableEvent } from 'react-contenteditable'
import { UseFormRegisterReturn } from 'react-hook-form'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  className?: string
  inputStyle?: CSSProperties
  strValue?: any
  label: string
  name?: string
  reverse?: boolean
  register?: UseFormRegisterReturn
  disabled?: boolean
  autoFocus?: boolean
  isMultiTab?: boolean
  disableTab?: boolean
  disableTabInput?: boolean
  styleAction?: ('u' | 'i' | 'b')[]
  onChange?: (data: string) => void
  onTabClick?: (e: MouseEvent<HTMLDivElement>, index: number) => void
  onTabCreated?: (id: string, index: number) => void
  onTabsDeleted?: (indexes: number[]) => void
  onBlur?: (data: string) => void
  children?: any
  viewMode?: boolean
  specialChar?: string[]
  disableStyleText?: string[]
}

export const ContentField = forwardRef(
  (
    {
      className = '',
      inputStyle,
      strValue = '',
      label,
      style,
      disabled,
      isMultiTab = false,
      disableTab = false,
      disableTabInput = false,
      styleAction = [],
      onChange,
      onTabClick,
      onTabCreated,
      onTabsDeleted,
      onBlur,
      children = '',
      viewMode = false,
      specialChar = [],
      disableStyleText = []
    }: PropsType,
    ref,
  ) => {
    useImperativeHandle(ref, () => ({
      pressTab: handleTab,
      getBoundingClientRect: () => {
        return inputRef.current.getBoundingClientRect()
      },
    }))

    const inputMinWidth = 70
    const [isExist, setIsExist] = useState(strValue ? true : false)
    const htmlFormat = useRef(strValue ?? '')

    const inputRef = useRef<HTMLDivElement>(null)
    const hiddenRef = useRef<HTMLSpanElement>(null)

    useEffect(() => {
      inputRef.current.setAttribute("ondragenter","return false")
      inputRef.current.setAttribute("ondragleave","return false")
      inputRef.current.setAttribute("ondragover","return false")
      inputRef.current.setAttribute("ondrop","return false")
      inputRef.current.addEventListener('input', onInputChange)
      inputRef.current.addEventListener('keydown', onPreventTabKey)
      setTimeout(() => {
        for (let i = 0, len = listInput.current.length; i < len; i++) {
          const childN = document.getElementById(
            `${listInput.current[i].id}`,
          ) as HTMLInputElement
         
          if (childN && childN.nodeName === 'INPUT') {
            hiddenRef.current.textContent = childN.value
            if (hiddenRef.current.offsetWidth > inputMinWidth) {
              childN.style.width = `${hiddenRef.current.offsetWidth}px`
              listInput.current[i].width = hiddenRef.current.offsetWidth
            } else {
              childN.style.width = `${inputMinWidth}px`
              listInput.current[i].width = inputMinWidth
            }
          }
        }
      }, 0)

      return () => {
        inputRef?.current?.removeEventListener('input', onInputChange)
        inputRef?.current?.removeEventListener('keydown', onPreventTabKey)
      }
    }, [])

    useEffect(() => {
      if (viewMode) {
        inputRef.current.contentEditable = 'false'
      } else {
        inputRef.current.contentEditable = 'true'
      }
    }, [viewMode])

    const convertListInput = useCallback((strV: any) => {
      const list = []
      const matches = strV?.matchAll(/#[^#]*#/g)
      let index = 0
      for (const match of matches) {
        list.push({
          id: `${Math.random()}${index}`,
          value: match[0].replaceAll('#', ''),
          width: inputMinWidth,
        })
        index++
      }
      return list
    }, [])

    const listInput = useRef(convertListInput(strValue ?? ''))

    const onInputChange = useCallback((e: Event) => {
      const el = e.target as HTMLInputElement
      if (el.nodeName !== 'INPUT') return
      const itemInput = listInput.current.find((m) => m.id === el.id)
      itemInput.value = el.value
      htmlFormat.current = convertHtmlToStr(
        Array.from(inputRef.current.childNodes),
      )
      onChange && onChange(htmlFormat.current)
      setTimeout(() => {
        hiddenRef.current.textContent = el.value
        if (hiddenRef.current.offsetWidth > 70) {
          el.style.width = `${hiddenRef.current.offsetWidth}px`
          itemInput.width = hiddenRef.current.offsetWidth
        } else {
          el.style.width = `${inputMinWidth}px`
          itemInput.width = inputMinWidth
        }
      }, 0)
    }, [])

    const onPreventTabKey = useCallback((e: any) => {
      const el = e.target as HTMLInputElement
      if (el.nodeName === 'INPUT' && (e.code === 'Tab' || e.code === 'Enter')) {
        e.preventDefault()
        e.stopPropagation()
      }
    }, [])

    const onChangeText = (event: ContentEditableEvent) => {
      const listNode:any[] = []
      const checkHasInput=(nodeList:any[])=>{
        for(let i=0; i< nodeList.length;i++){
          if(nodeList[i].nodeName === 'INPUT'){
            listNode.push(nodeList[i])
          }else if( nodeList[i].nodeName ==="#text"){

          }else{
           if(nodeList[i].childNodes && nodeList[i].childNodes.length>0){
            checkHasInput(nodeList[i].childNodes)
           } 
          }
        }
      }
      checkHasInput(Array.from(event.currentTarget.childNodes))
      
      if (listNode.length != listInput.current.length) {
        const listInputTemp: any[] = []
        const listRemain: any[] = [...listInput.current]
        listNode.forEach((el: any) => {
          const index = listRemain.findIndex((m) => m.id === el.id)
          listInputTemp.push(listRemain.splice(index, 1)[0])
        })
        onTabsDeleted &&
          onTabsDeleted(
            listRemain.map((m) =>
              listInput.current.findIndex((g) => g.id === m.id),
            ),
          )
        listInput.current = listInputTemp
      }
      const questionT = convertHtmlToStr(event.currentTarget.childNodes)
      htmlFormat.current = questionT?.replaceAll('\u200B','')
      setIsExist(questionT ? true : false)
      onChange && onChange(htmlFormat.current)
    }

    const handleTab = useCallback(() => {
      let nodeEl: any = null
      const selection = window.getSelection()
      if (selection.rangeCount === 0) return
      const range = selection.getRangeAt(0)
      if (
        !isElement(range.commonAncestorContainer as Element, inputRef.current)
      ) {
        return
      }
      const newId = `${Math.random()}${listInput.current.length}`
      const parentEl = range.startContainer
      if (parentEl.nodeName === 'INPUT') return
      if (!isMultiTab && listInput.current.length > 0) return
      nodeEl = document.createElement('input')
      nodeEl.id = newId
      nodeEl.autocomplete = 'off'
      nodeEl.style.width = `${inputMinWidth}px`
      if(disableTabInput){
        nodeEl.readOnly = disableTabInput
      }
      range.insertNode(nodeEl)

      let inpIndex = -1
      const checkHasInput=(nodeList:any[], isRecursive = false) => {
        for(let i = 0; i < nodeList.length; i++){
          if (nodeList[i].childNodes && nodeList[i].childNodes.length > 0) {
            if(checkHasInput(nodeList[i].childNodes, true)){
              return true
            }
          }
          if(nodeList[i].nodeName === 'INPUT'){
            inpIndex++
          }else if( nodeList[i].nodeName ==="#text"){

          }
          if (nodeList[i] === nodeEl) {
            return true
          }
        }
        return false
      }
      checkHasInput(Array.from(inputRef.current.childNodes))
      listInput.current.splice(inpIndex, 0, {
        id: newId,
        value: '',
        width: inputMinWidth,
      })
      htmlFormat.current = convertHtmlToStr(
        Array.from(inputRef.current.childNodes),
      )
      setIsExist(htmlFormat.current ? true : false)

      onTabCreated && onTabCreated(newId, inpIndex)
      onChange && onChange(htmlFormat.current)

      setTimeout(() => {
        if (nodeEl) {
          if (nodeEl.nodeName === 'INPUT') {
            if (!disableTabInput) {
              nodeEl.focus()
            } else {
              if (nodeEl.nextSibling) {
                const range1 = document.createRange()
                range1.setStartBefore(nodeEl.nextSibling)
                selection.removeAllRanges()
                selection.addRange(range1)
              }
            }
          }
        }
      }, 0)
    }, [])

    const handleEnter = useCallback(() => {
      let nodeEl: any = null
      const selection = window.getSelection()
      if (selection.rangeCount === 0) return
      const range = selection.getRangeAt(0)
      nodeEl = document.createTextNode('\n\u200B')
      range.insertNode(nodeEl)

      htmlFormat.current = convertHtmlToStr(
        Array.from(inputRef.current.childNodes),
      )
      setIsExist(htmlFormat.current ? true : false)

      onChange && onChange(htmlFormat.current)

      setTimeout(() => {
        if (nodeEl) {
          const range1 = document.createRange()
          range1.setStart(nodeEl, 1)
          selection.removeAllRanges()
          selection.addRange(range1)
        }
      }, 0)
    }, [])

    const onKeyDown = useCallback((event: KeyboardEvent<HTMLDivElement>) => {
      specialChar.map((item:string)=>{
        if(event.key === item){
          event.preventDefault()
          return
        }
      })
      if(event.ctrlKey && disableStyleText.some(item=> event.key.toLowerCase() === item)){
        event.preventDefault()
        return
      }
      let ctrlDown = false
      if (event.ctrlKey) ctrlDown = true
      if (ctrlDown && (event.key.toLowerCase() === 'z')) {
        event.preventDefault()
        return
      }
      const isTab = !disableTab && event.code === 'Tab'
      const isEnter = event.code === 'Enter'
      if (isTab || isEnter) {
        event.preventDefault()
        if (isTab) handleTab()
        else if (isEnter) handleEnter()
      }
    }, [])

    const onPaste = useCallback((event: any) => {
      event.preventDefault()
      const data = event.clipboardData.getData('text').replace(/(\#|\*)/g, '')
      const selection = window.getSelection()
      const range = selection.getRangeAt(0)
      range.deleteContents()
      const node = document.createTextNode(data)
      range.insertNode(node)
      selection.removeAllRanges()
      const newRange = document.createRange()
      newRange.setStart(node, data.length)
      selection.addRange(newRange)
    }, [])

    const onClickStyle = useCallback((style: string) => {
      console.log("style",style)
      switch (style) {
        case "u":
          onUnderline()
          break;
        case "i":
          onItalic()
          break;
        case "b":
          onBold()
          break;
            
        default:
          break;
      }
      // const selection = window.getSelection()
      // console.log("selection===",selection)
      // const range = selection.getRangeAt(0)
      // console.log("range===",range)

      // if (IsAncestorTag(range.commonAncestorContainer, style.toUpperCase())) {
      //   return
      // }

      // const frag = range.cloneContents()
      // range.deleteContents()
      // const node = document.createElement(style)
      // removeSameNodeType(node, Array.from(frag.childNodes), style.toUpperCase())
      // range.insertNode(node)
      // selection.removeRange(range)
      // htmlFormat.current = convertHtmlToStr(
      //   Array.from(inputRef.current.childNodes),
      // )
      // onChange && onChange(htmlFormat.current)
    }, [])
    const convertStrToHtml = useCallback((dataD: any) => {
      const matches = dataD?.matchAll(/(#[^#]*#|%u%|%b%|%i%)/g)
      let result = dataD
      let index = 0
      const styleIndex: any = {}
        for (const match of matches) {
            if (match[0].startsWith('#')) {
              if(listInput.current?.length > 0){
                const itemInput = listInput.current[index]
                result = result.replace(
                  match[0],
                  `<input id="${itemInput?.id}" autocomplete="off" value="${match[0].replaceAll(
                    '#',
                    '',
                  )}" ${viewMode?"readonly":""} style="width:${
                    itemInput?.width === 0 ? inputMinWidth : itemInput?.width
                  }px"/>`,
                )
                index++
              }
            } else {
              const sIndex = styleIndex[match[0]] ?? 0
              const tag = match[0].replaceAll('%', '')
              result = result.replace(
                match[0],
                sIndex % 2 === 0 ? `<${tag}>` : `</${tag}>`,
              )
              styleIndex[match[0]] = sIndex + 1
            }
          }
      return result
    }, [])

    const convertHtmlToStr = useCallback((dataD: ChildNode[]) => {
      let str = ''
      for (let i = 0, len = dataD.length; i < len; i++) {
        const child = dataD[i] as Node
        if (child.nodeName === '#text') {
          str += child.textContent
        } else if (child.nodeName === 'SPAN') {
          str += child.firstChild.textContent
        } else if (child.nodeName === 'BR') {
          // str += '\n'
        } else if (child.nodeName === 'INPUT') {
          const inputEl = child as HTMLInputElement
          str += `#${inputEl.value}#`
        } else {
          const el = child as Element
          const tag = child.nodeName.toLowerCase()
          const content = convertHtmlToStr(Array.from(el.childNodes))
          if (content !== '') {
            str += `%${tag}%${content}%${tag}%`
          }
        }
      }
      return str
    }, [])

    const onClickInput = (e: MouseEvent<HTMLDivElement>) => {
      const input = e.target as HTMLInputElement
      if (input.tagName === 'INPUT') {
        e.preventDefault()
        onTabClick &&
          onTabClick(
            e,
            listInput.current.findIndex((m) => m.id === input.id),
          )
      }
    }

    const onInputBlur = (e: FocusEvent<HTMLDivElement>) => {
      onBlur && onBlur(htmlFormat.current.trim())
      const el:HTMLElement = e.currentTarget 
      el.innerHTML = convertStrToHtml(htmlFormat.current.trim())
    }

    return (
      <div
        className={`a-content-with-label ${className}`}
        data-exist={isExist}
        data-style={styleAction.length > 0}
        style={style}
      >
        {children}
        <ContentEditable
          innerRef={inputRef}
          spellCheck={false}
          style={inputStyle}
          disabled={disabled}
          html={convertStrToHtml(htmlFormat.current.trim())}
          contentEditable="true"
          onChange={onChangeText}
          onKeyDown={onKeyDown}
          onPaste={onPaste}
         //  onClick={onClickInput}
         onMouseDown={onClickInput}
          onBlur={onInputBlur}
        />
        <label>
          <span>{label}</span>
        </label>
        <div className="style-action">
          {styleAction.map((m) => (
            <div
              key={m}
              className={`style-${m}`}
              onMouseDown={onClickStyle.bind(null, m)}
            />
          ))}
        </div>
        <span className="hidden-text" ref={hiddenRef}></span>
      </div>
    )
  },
)

ContentField.displayName = ''

const removeSameNodeType = (
  parent: HTMLElement,
  nodes: ChildNode[],
  style: string,
) => {
  for (const node of nodes) {
    if (node.nodeName === style) {
      removeSameNodeType(parent, Array.from(node.childNodes), style)
    } else {
      parent.appendChild(node)
    }
  }
}

const IsAncestorTag = (node: Node, style: string): boolean => {
  if (node.nodeName === style) {
    return true
  }
  if (node.parentNode) {
    return IsAncestorTag(node.parentNode, style)
  }
  return false
}

const isElement = (el: Element, target: Element): boolean => {
  if (el === target) return true
  if (el.parentElement) return isElement(el.parentElement, target)
  return false
}
const onBold = () => {
  const selection = window.getSelection()
  if (selection.rangeCount === 0) return
  document.execCommand('Bold', false, null)

}
const onItalic = () => {
  const selection = window.getSelection()
  if (selection.rangeCount === 0) return
  document.execCommand('Italic', false, null)
}
const onUnderline = () => {
  const selection = window.getSelection()
  if (selection.rangeCount === 0) return
  document.execCommand('Underline', false, null)
}
