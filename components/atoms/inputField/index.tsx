import { ChangeEvent, CSSProperties, ReactNode, useState, useEffect } from 'react'

import { UseFormRegisterReturn, UseFormSetValue } from 'react-hook-form'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  inputStyle?: CSSProperties
  defaultValue?: any
  label: string
  name?: string
  reverse?: boolean
  type: string
  register?: UseFormRegisterReturn
  setValue?: UseFormSetValue<any>
  disabled?: boolean
  autoFocus?: boolean
  icon?: ReactNode
  iconPos?: 'start' | 'end'
  onBlur?: () => null
  onChange?: (e: ChangeEvent<HTMLInputElement>) => void
  preventKey?: string[]
}

export const InputField = ({
  className = '',
  inputStyle,
  defaultValue = '',
  label,
  name,
  type,
  style,
  register,
  setValue,
  disabled,
  autoFocus = false,
  icon,
  iconPos,
  onBlur,
  onChange,
  preventKey = [],
}: PropsType) => {
  const [valueStr, setValueStr] = useState(defaultValue ?? '')

  useEffect(() => {
    setValueStr(defaultValue)
  } ,[defaultValue])

  useEffect(()=>{
    const el = document.querySelector('#input-field')
    el?.setAttribute("ondragenter","return false")
    el?.setAttribute("ondragleave","return false")
    el?.setAttribute("ondragover","return false")
    el?.setAttribute("ondrop","return false")
  },[])
  return (
    <div
      className={`a-input-with-label ${className}`}
      data-exist={!!valueStr}
      data-icon={!!icon}
      data-icon-placement={iconPos}
      style={style}
    >
      <input
        style={inputStyle}
        id='input-field'
        type={type}
        value={valueStr}
        name={name}
        disabled={disabled}
        {...register}
        onChange={(e) => {
          setValueStr(e.target.value)
          if(!setValue){
            register?.onChange(e)
          }
          onChange?.(e)
        }}
        onBlur={(e) => {
          onBlur?.()
          setValueStr(e.target.value.trim())
          if(setValue){
            setValue(register.name,e.target.value.trim())
          }else{
            register?.onBlur(e)
          }
        }}
        autoFocus={autoFocus}
        autoComplete="off"
        onKeyDown={(e) => {
          if (preventKey.includes(e.key) || e.code === 'Enter') {
            e.preventDefault()
            e.stopPropagation()
          }
        }}
      />
      <label>
        <span>{label}</span>
      </label>
      {icon && <div className={'a-input-with-label__icon'}>{icon}</div>}
    </div>
  )
}
