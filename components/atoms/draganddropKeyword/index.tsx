import useTranslation from '@/hooks/useTranslation'
import { DefaultPropsType } from '../../../interfaces/types'
import { DDAnswers } from '../draganddropAnswers'

interface PropsType extends DefaultPropsType {
  answers: string[]
  correctAnswer: string
  labelDefault?: string
}

export const DDKeyword = ({
  className = '',
  answers,
  correctAnswer,
  style,
  labelDefault,
}: PropsType) => {
  return (
    <div className={`a-dd-keyword ${className}`} style={style}>
      <div className="a-dd-keyword__item">
        <span className="a-dd-keyword__title">{labelDefault}:</span>
        {answers.map((item: string, i: number) => (
          <div key={`word_${i}`}>
            <span>{item}</span>
          </div>
        ))}
      </div>
      <DDAnswers data={correctAnswer} />
    </div>
  )
}
