import useTranslation from '@/hooks/useTranslation'
import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  answer?: string
  hint?: string
}

export const FBSuggest = ({
  className = '',
  answer,
  hint,
  style,
}: PropsType) => {
  const {t} = useTranslation()
  return (
    <div className={`a-fb-suggest ${className}`} style={style}>
      <div className="a-fb-suggest__title">
        <span>{t('create-question')['clues']}: {hint}</span>
        <div className="a-fb-suggest__correct-answer">
        {t('create-question')['correct-answer']}: {answer}
        </div>
      </div>
    </div>
  )
}
