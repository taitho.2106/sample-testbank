import { ChangeEvent, CSSProperties, forwardRef, ReactNode, useEffect, useImperativeHandle, useRef, useState } from 'react'

import { UseFormRegisterReturn } from 'react-hook-form'
import { Dropdown, Popover, Tooltip, Whisper } from 'rsuite'

import { DefaultPropsType, StructType } from '@/interfaces/types'

import { CheckBox } from '../checkbox'
import useTranslation from "@/hooks/useTranslation";

interface PropsType extends DefaultPropsType {
  name?: string
  data: StructTypeGroup[]
  defaultValue?: any

  label?: string
  toolTip?: string
  placeholder?: string

  placement?: 'bottomStart' | 'bottomEnd' | 'topStart' | 'topEnd' | 'leftStart' | 'leftEnd' | 'rightStart' | 'rightEnd'
  menuSize?: 'md' | 'lg' | 'xl' | 'xxl'
  menuMaxHeight?: string,
  onConfirmChange?: (value: any) => Promise<boolean>;
  isMultiChoice?: boolean
  groupNameShow?: boolean
  inputSearchShow?: boolean
  triggerReset?: boolean
  register?: UseFormRegisterReturn
  disabled?: boolean
  isShowSelectAll?: boolean
  labelSearch?: string
  menuStyle?: CSSProperties
  onChange?: (value: any[]) => void
  parentPosition?: any
}

interface StructTypeGroup extends StructType {
  imageUrl?: string
  groupName?: string
  groupOrder?: number
}

interface StructRender extends StructTypeGroup {
  status?: boolean
  largeHeight?: boolean
}

export const SelectFilterPicker = forwardRef(({
  name = '',
  data,
  defaultValue = [],
  className = '',
  label = '',
  toolTip = '',
  placeholder = '',
  placement = 'bottomStart',
  menuSize = 'md',
  isMultiChoice = true,
  isShowSelectAll = true,
  groupNameShow = true,
  inputSearchShow = true,
  labelSearch = "search",
  register,
  onChange = () => null,
  triggerReset = false,
  disabled = false,
  style,
  menuStyle = {},
  onConfirmChange = null,
  parentPosition,
}: PropsType, ref) => {
  const { t } = useTranslation();
  const initialData = data ?? []
  const allData = useRef(initialData)
  const inputRef = useRef(null)
  const [stateFocus, setStateFocus] = useState<boolean>(false)
  const [stateTitle, setStateTitle] = useState<ReactNode>('')
  const defaultValueRef = useRef(defaultValue ? (Array.isArray(defaultValue) ? defaultValue : [defaultValue]) : [])
  const [stateCode, setStateCode] = useState<any[]>(defaultValueRef.current)
  const [stateData, setStateData] = useState<StructTypeGroup[]>(initialData)
  const beforeCode = useRef(defaultValueRef.current)
  const largeHeight = useRef(data.some(m => m.imageUrl))

  const [placementDefault, setPlacementDefault] = useState(placement)

  useEffect(() => {
    const selectFilterDiv = document.querySelector(".select_filter_picker")
    if(selectFilterDiv && stateFocus) {
      const selectFilterOffsetRight = selectFilterDiv.getBoundingClientRect().right
      if(parentPosition && parentPosition - selectFilterOffsetRight < 50){
        setPlacementDefault('bottomEnd')
      } else {
        setPlacementDefault('bottomStart')
      }
    }
  }, [stateFocus])

  useEffect(() => {
    allData.current = data;
    setStateData([...data])

    if (data.length == 0 && !isShowSelectAll) return;
    beforeCode.current = stateCode;
    if (stateCode.findIndex((x) => data.findIndex((m) => m.code == x) == -1) != -1) {
      setStateCode([])
      handleStateCode([])
    } else {
      setStateCode([...stateCode])
      handleStateCode([...stateCode])
    }
    largeHeight.current = data.some(m => m.imageUrl)
  }, [data])
  useEffect(() => {
    let titleDropDown = ''
    if (stateCode.length > 0) {
      titleDropDown = renderTitleDropDown(stateData.filter((item) => stateCode.includes(item.code)).map((item) => item), t)
    }
    setStateTitle(titleDropDown)
  }, [stateCode, data])

  useEffect(() => {
    if (defaultValueRef.current !== stateCode) {
      beforeCode.current = stateCode;
      setStateCode([])
      handleStateCode([])
      setStateTitle('')
    }
  }, [triggerReset])
  const handleStateCode = (codes: any[]) => {
    if (codes.length == 0 && beforeCode.current.length == 0) return;
    const value = getValueCallback(codes);
    onChange(value)
  }
  const handleOnOpen = () => setStateFocus(true)
  const handleOnClose = () => {
    setStateFocus(false)
    if (inputRef.current) {
      inputRef.current.value = ""
      setStateData([...initialData])
    }
  }
  const getValueCallback = (codes: any[]) => {
    if (isMultiChoice) {
      return codes
    } else {
      return codes[0] || null
    }
  }
  const handleOnSelect = async (eventKey: any, event: any) => {
    event.stopPropagation()

    const currentStateCode = [...stateCode]

    if (eventKey == 0) {
      //rỗng
      if (currentStateCode.length == 0) return;
      currentStateCode.splice(0)
    } else {
      const indexCode = currentStateCode.findIndex((item) => item == eventKey)
      if (indexCode != -1) {
        if (isMultiChoice || currentStateCode.length > 1) {
          currentStateCode.splice(indexCode, 1)
        }
      } else {
        if (isMultiChoice) currentStateCode.push(eventKey)
        else currentStateCode[0] = eventKey
      }
    }
    if (onConfirmChange && typeof (onConfirmChange) == "function") {
      const confirmChange = await onConfirmChange(getValueCallback(currentStateCode))
      if (!confirmChange) return;
    }
    beforeCode.current = [...stateCode];
    setStateCode([...currentStateCode])
    handleStateCode([...currentStateCode])
  }

  const handleOnSearch = (event: ChangeEvent<HTMLInputElement>) => {
    let filterByDisplay: StructTypeGroup[] = initialData
    if (event.target.value && event.target.value.length > 0) {
      filterByDisplay = initialData.filter((item) => {
        const stringDisplay = cleanString(item.display.toLowerCase())
        const stringSearch = cleanString(event.target.value.toLowerCase())
        return stringDisplay.includes(stringSearch)
      })
    }
    setStateData(filterByDisplay)
  }
  const itemGroup = stateData.reduce((group: any, { groupOrder: order, groupName: name, ...item }: any) => {
    group[order] = group[order] ?? []
    
    if(group[order]['groupOrder']) {
      group[order]['groupName'] = t(name)
    } else {
      group[order]['groupName'] = ''
    }
    if (!group[order]['items']) {
      group[order] = { groupName: t(name) ?? '', groupOrder: order, items: [] }
    }
    group[order]['items'].push({...item, display: t(item.display)})
    return group
  }, {})
  return (
    <Whisper
      placement="topStart"
      trigger="hover"
      speaker={renderTooltip(toolTip, t)}
    >
      <div
        className={`select_filter_picker ${className}`}
        style={disabled ? { cursor:'not-allowed',...style } : style}
        data-length={stateData.length}
      >
        <input
          name={name || ''}
          type="hidden"
          value={
            stateCode.length == 0
              ? ''
              : isMultiChoice
              ? stateCode
              : stateCode[0]
          }
          {...register}
        />
        {/* <input name={name} type="hidden" value={stateCode.join(',')} /> */}
        <label
          className={`__label ${
            stateFocus || stateTitle.toString().length > 0 ? '__active' : ''
          }`}
        >
          <span className={`__text ${disabled ? 'disabled' : ''}`}>{label ? label : placeholder}</span>
        </label>
        <img
          className="__caret"
          src="/images/icons/ic-chevron-down-dark.png"
          alt="image-toggle"
        />
        <Dropdown
          style={disabled ? { pointerEvents: 'none', ...menuStyle } : menuStyle}
          placement={placement != 'bottomStart' ? placement : placementDefault}
          className={`wrapper dropdown-menu-${menuSize}`}
          onOpen={handleOnOpen}
          onClose={handleOnClose}
          title={stateTitle}
          onSelect={handleOnSelect}
          disabled={disabled}
        >
          {inputSearchShow ? (
            <div className={`searching`}>
              <img
                className={'__image'}
                src="/images/icons/ic-search-dark.png"
                alt="search"
              />
              <input
                ref={inputRef}
                type={'text'}
                onChange={handleOnSearch}
                className={'__input_search'}
                placeholder={t('common')[labelSearch] + '...'}
              />
            </div>
          ) : (
            ''
          )}
          <div className="__dropdown-content">
            {Object.values(itemGroup).length > 0 && isShowSelectAll && (
              <div className={`group --all`}>
                <Dropdown.Item
                  key={0}
                  eventKey={0}
                  className={`item ${stateCode.length == 0 ? '--checked' : ''}`}
                >
                  <CheckBox
                    className={'__checkbox'}
                    checked={stateCode.length == 0 ? true : false}
                  />
                  <span className={'__text'}>{t('common')['all']}</span>
                </Dropdown.Item>
              </div>
            )}
            <div className="__group-list">
              {Object.values(itemGroup).map((item: any, index: number) => (
                <div key={index} className={`group`}>
                  {groupNameShow ? renderTitleGroup(item.groupName) : ''}
                  {item.items.map((i: StructTypeGroup) =>
                    renderItem({
                      code: i.code,
                      display: i.display,
                      imageUrl: i.imageUrl,
                      status:
                        (stateCode.length == 0 && isShowSelectAll) ||
                        stateCode.includes(i.code)
                          ? true
                          : false,
                      largeHeight: largeHeight.current,
                    }, t),
                  )}
                </div>
              ))}
              {Object.values(itemGroup).length <= 0 && (
                <div className='empty-data'>{t('no-field-data')['search-title']}</div>
              )}
            </div>
          </div>
        </Dropdown>
      </div>
    </Whisper>
  )
})
SelectFilterPicker.displayName = "s"
const renderItem = ({ code, display, imageUrl, status, largeHeight }: StructRender, t:any) => {
  return (
    <Dropdown.Item key={code} eventKey={code} className={`item ${largeHeight ? "--large" : ""} ${status ? "--checked" : ""}`}>
      <CheckBox className={'__checkbox'} checked={status} key={code} />
      {imageUrl && renderImage(imageUrl)}
      <span className={'__text'}>{t(display)}</span>
    </Dropdown.Item>
  )
}

const renderTooltip = (text: string, t:any) => {
  return <Tooltip style={{ visibility: text ? 'visible' : 'hidden' }}>{t(text)}</Tooltip>
}

const renderTitleGroup = (title: string) => {
  if (!title || title.length === 0) return
  return (
    <div className={`__title`}>
      <span className={'__text'}>{title}</span>
    </div>
  )
}

const renderTitleDropDown = (bunchOfItem: any, t:any) => {
  return bunchOfItem.map((item: any, index: number) => (
    <Whisper
      key={index}
      trigger={'hover'}
      placement={'auto'}
      speaker={
        <Popover className={`__popover_select_filter_picker${item.imageUrl ? "__image" : "__text"}`}>
          {item.imageUrl && renderImage(item.imageUrl)}
          {!item.imageUrl && <span>{t(item.display)}</span>}
        </Popover>
      }
    >
      <div className={`__box`}>
        {item.imageUrl && renderImage(item.imageUrl)}
        <span className={'__text'}>{t(item.display)}</span>
        {/* <span className={'__deleted'}>&#215;</span> */}
      </div>
    </Whisper>
  ))
}

const renderImage = (path: string) => {
  return (
    <div className={'__frame'}>
      {path.length > 0 ? (
        <img className={'__image'} src={path} loading="lazy" alt={'img'} />
      ) : (
        <img className={'__image'} src="/images/icons/ic-image.png" alt="image-default" />
      )}
    </div>
  )
}

const cleanString = (text: string) => {
  text = text.normalize('NFC')
  text = text.toLowerCase()
  text = text.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
  text = text.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
  text = text.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
  text = text.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
  text = text.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
  text = text.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
  text = text.replace(/đ/g, 'd')
  return text
}