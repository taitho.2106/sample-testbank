import { Fragment } from 'react'

import { convertStrToHtml } from '@/utils/string'

import { DefaultPropsType } from '../../../interfaces/types'
import { SLAnswersDropdown } from '../selectfromlistAnswersDropdown'

interface PropsType extends DefaultPropsType {
  correctOptions: string[]
  data: string
  options: any[]
}

export const SLParagraph = ({
  className = '',
  correctOptions,
  data,
  options,
  style,
}: PropsType) => {
  const transformData = data.split('%s%')

  return (
    <div className={`a-sl-paragraph ${className}`} style={style}>
      <div className="a-sl-paragraph__title">
        <p>
          {transformData.map((item: string, i: number) => {
            const isExample = correctOptions[i]?.startsWith('*')
            return (
              <Fragment key={i}>
                <span
                  dangerouslySetInnerHTML={{
                    __html: convertStrToHtml(item).replace(/%s%/g, '[ ]'),
                  }}
                />
                {i !== transformData.length - 1 && !isExample && (
                  <SLAnswersDropdown
                    data={options[i] || []}
                    selected={correctOptions[i] || ''}
                  />
                )}
                {i !== transformData.length - 1 && isExample && (
                  <span className="--example">
                    {correctOptions[i].substring(1)}
                  </span>
                )}
              </Fragment>
            )
          })}
        </p>
      </div>
    </div>
  )
}
