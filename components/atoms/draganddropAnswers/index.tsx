import useTranslation from '@/hooks/useTranslation'
import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  data: string
}

export const DDAnswers = ({ className = '', data, style }: PropsType) => {
  const {t} = useTranslation()
  return (
    <div className={`a-dd-answers ${className}`} style={style}>
      <div className="a-dd-answers__text">
        <span>{t('create-question')['correct-answer']}: </span>
        <span>{data}</span>
      </div>
    </div>
  )
}
