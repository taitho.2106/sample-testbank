/* eslint-disable @next/next/no-img-element */

import {
  useContext,
  useRef,
  useState,
  useCallback,
  useEffect,
  MutableRefObject,
  useImperativeHandle,
  forwardRef,
} from 'react'

import { ContentItemPosType, DefaultPropsType } from '@/interfaces/types'
import coordinates from 'utils/coordinates'
import Guid from 'utils/guid'
import useTranslation from '@/hooks/useTranslation'
//arr item.
type DataImageType = {
  data: string
  key: string
  color:string
}
type DataPosType = {
  minX: number
  minY: number
  maxX: number
  maxY: number
  data: string
}
type JsonDataType = {
  imageUrl:string,
  positionArr:ContentItemPosType[]
}
type ImageCurrentInfoType={
  src:string,
  width:number,
  height:number
}
const charAlpha = '80'
interface PropsType extends DefaultPropsType {
  jsonData: JsonDataType
  className?: string
  errorStr?: string
  onChangeItem: (data: any) => void
  onChangeItemIndexSelected: (index:number)=> void
  onChangeImageData?:(data:ImageCurrentInfoType)=> void
  onClickChangeImage:()=> void
  viewMode?: boolean
}
const colorHighLightSelected = '#ff0'
const pi = Math.PI
const maxHeight = 338
const maxWidth = 900
const lineWidth = 20;
export const SplitImage = forwardRef(
  ({ 
    jsonData,
     onChangeItem, 
    onChangeItemIndexSelected,
    onChangeImageData,
    onClickChangeImage,
    style,
    className,
    viewMode
  }: PropsType, ref) => {
    const {t} = useTranslation()
    const viewModeRef = useRef(viewMode)
    const myCanvas = useRef() as MutableRefObject<HTMLCanvasElement>
    const myCtx = useRef() as MutableRefObject<CanvasRenderingContext2D>

    const canvas = useRef() as MutableRefObject<HTMLCanvasElement>
    const ctxCanvas = useRef() as MutableRefObject<CanvasRenderingContext2D>
    const ctxImageData = useRef() as MutableRefObject<Uint8ClampedArray>

    const canvasDraw = useRef() as MutableRefObject<HTMLCanvasElement>
    const ctxCanvasDraw = useRef() as MutableRefObject<CanvasRenderingContext2D>

    const listHighLightPos = useRef<ContentItemPosType[]>([])
    const listData = useRef<DataPosType[]>([])
    const isDown = useRef(false)
    const isMouseMove = useRef(false)
    const imgElement = useRef() as MutableRefObject<HTMLImageElement>
    const index = useRef<number>(-1)

    const listImageRef = useRef<DataImageType[]>([])
    const [indexSelected, setIndexSelected] = useState(index.current)
    const [showImageDefault, setShowImageDefault] = useState(true)
    useImperativeHandle(ref, () => ({
      addNewItem: (color:string = null) => {
       return addNewItem(color)
      },
      removeItem: (indexItem: number) => {
        return removeItem(indexItem)
      },
      selectItem:(indexItem: number) => {
        return selectItem(indexItem)
      },
      changeColorItem:(indexItem: number, color:string = null) => {
        return changeColorItem(indexItem, color)
      },
      changeImageUrl:(url:string)=>{
        if(url){
          return changeImageUrl(url)
        }else{
          return clearImageInput();
        }
      },
      clearImage:()=>{
        return clearImageInput();
      },
      getImagePostions:()=>{
        return listHighLightPos.current;
      }
    }))
    function drawImageFromWebUrl(sourceurl: any, isOld=false) {
      if(canvas.current && canvasDraw.current && myCanvas.current){
        imgElement.current = new Image()
        imgElement.current.addEventListener('load', function (e) {
          setShowImageDefault(false)
          const container = document.getElementById(
            'image-split-content-image-draw-id',
          ) as HTMLElement
          let height = imgElement.current.height
          let width = imgElement.current.width
          if (height > maxHeight) {
            height = maxHeight
            width =
            maxHeight * (imgElement.current.width / imgElement.current.height)
          }
          if(!isOld){
            if(width >maxWidth){
              width = maxWidth
              height =
              width * (imgElement.current.height / imgElement.current.width)
            }
          }
          container.style.height = `${height}px`
          container.style.width = `${width}px`
          // The image can be drawn from any source
          canvas.current.setAttribute('height', `${height}`)
          canvas.current.setAttribute('width', `${width}`)
  
          canvasDraw.current.setAttribute('height', `${height}`)
          canvasDraw.current.setAttribute('width', `${width}`)
  
          myCanvas.current.setAttribute('height', `${height}`)
          myCanvas.current.setAttribute('width', `${width}`)
          ctxCanvas.current.imageSmoothingQuality = "high";
          ctxCanvas.current.imageSmoothingEnabled = true;
          ctxCanvas.current.drawImage(
            imgElement.current,
            0,
            0,
            imgElement.current.width,
            imgElement.current.height,
            0,
            0,
            canvas.current.width,
            canvas.current.height,
          )
          ctxImageData.current = ctxCanvas.current.getImageData(
            0,
            0,
            canvas.current.width,
            canvas.current.height,
          ).data
          ctxCanvasDraw.current.drawImage(
            imgElement.current,
            0,
            0,
            imgElement.current.width,
            imgElement.current.height,
            0,
            0,
            canvas.current.width,
            canvas.current.height,
          )
          //callback image.
          if(onChangeImageData){
            const dataImage = canvas.current.toDataURL("img/png")
            onChangeImageData({src:dataImage,height, width })
          }
          renderImageFromData()
          drawHighLightCanvas()
        })
        imgElement.current.addEventListener('onerror', function (e){
          console.log("errror",e)
        })
        imgElement.current.src = sourceurl
      }
    }
    useEffect(() => {
     viewModeRef.current = viewMode;
    }, [viewMode])
    useEffect(() => {
      if (canvas.current) {
        if (indexSelected >= 0) {
          if (!canvas.current.classList.contains('cursor-pen')) {
            canvas.current.classList.add('cursor-pen')
          }
        } else {
          canvas.current.classList.remove('cursor-pen')
        }
      }
      onChangeItemIndexSelected(indexSelected)
    }, [indexSelected])

    useEffect(() => {
      myCtx.current = myCanvas.current.getContext(
        '2d',
      ) as CanvasRenderingContext2D
      ctxCanvas.current = canvas.current.getContext(
        '2d',
      ) as CanvasRenderingContext2D
      ctxCanvasDraw.current = canvasDraw.current.getContext(
        '2d',
      ) as CanvasRenderingContext2D
      if(jsonData){
        listHighLightPos.current = jsonData.positionArr.map(
          (item: any) => {
            const keyData:any = {}
            const colorData:any = {}
            item.listPos.forEach((m:any)=> {
              keyData[`${m.x}:${m.y}:${m.c}`] = 1;
              const keyColor =m.c.substring(1)
              if(!colorData[`${keyColor}`]){
                colorData[`${keyColor}`] = []
              }
              colorData[`${keyColor}`].push(`${m.x}:${m.y}`);
            })
            return { ...item,colorData:colorData, keyCheckData:keyData}
          },
        )
      }
      if(jsonData?.imageUrl){
        drawImageFromWebUrl(jsonData.imageUrl, true)
      }
    }, [jsonData])


    useEffect(() => {
      if (!canvas.current) return
      function getElementPosition(obj: any) {
        let curleft = 0,
          curtop = 0
        if (obj.offsetParent) {
          do {
            curleft += obj.offsetLeft
            curtop += obj.offsetTop
          } while ((obj = obj.offsetParent))
          return { x: curleft, y: curtop }
        }
        return undefined
      }

      function getEventLocation(element: any, event: any) {
        // const pos: any = getElementPosition(element)
        const pos: any = element.getBoundingClientRect();

        return {
          x: event.pageX - pos.x,
          y: event.pageY - pos.y,
        }
      }

      function rgbToHex(r: any, g: any, b: any) {
        if (r > 255 || g > 255 || b > 255) throw 'Invalid color component'
        return ((r << 16) | (g << 8) | b).toString(16)
      }
      canvas.current.addEventListener('mousemove', canvasMouseMove, false)

      canvas.current.addEventListener('mousedown', canvasMouseDown, false)
      canvas.current.addEventListener('mouseup', canvasMouseUp, false)
      canvas.current.addEventListener('mouseleave', canvasMouseLeave, false)
      function canvasMouseMove(e: any) {
        if (!isDown.current) return
        isMouseMove.current = true;
        const eventLocation = getEventLocation(canvas.current, e)
        const minX = Math.floor(eventLocation.x - lineWidth / 2)
        const minY = Math.floor(eventLocation.y - lineWidth / 2)
        const maxX = Math.ceil(eventLocation.x + lineWidth / 2)
        const maxY = Math.ceil(eventLocation.y + lineWidth / 2)
        const widthCanvas =   canvas.current.width;
        const itemHighLightPos = listHighLightPos.current[index.current]
        for (let i = minX; i <= maxX; i++) {
          for (let j = minY; j <= maxY; j++) {
            if (
              (i - eventLocation.x) * (i - eventLocation.x) +
                (j - eventLocation.y) * (j - eventLocation.y) <
              (lineWidth * lineWidth) / 4
            ) {
              // const pixelDataCurrent = ctxCanvas.current.getImageData(
              //   i,
              //   j,
              //   1,
              //   1,
              // ).data
              const pixelPosition = (i*4) + ((j*widthCanvas*4));
              const pixelDataCurrent = [ctxImageData.current[(pixelPosition)],
              ctxImageData.current[(pixelPosition + 1)],
              ctxImageData.current[(pixelPosition + 2)],
              ctxImageData.current[(pixelPosition + 3)]
            ]
              if(pixelDataCurrent[0] == 0 && pixelDataCurrent[1]==0 && pixelDataCurrent[2] == 0 && pixelDataCurrent[3] ==0){
                continue
              }
              const hexCurrent =
                '#' +
                (
                  '000000' +
                  rgbToHex(
                    pixelDataCurrent[0],
                    pixelDataCurrent[1],
                    pixelDataCurrent[2],
                  )
                ).slice(-6)
                const keyCheck = `${i}:${j}:${hexCurrent}`
                if(!itemHighLightPos.keyCheckData[keyCheck]){
                  itemHighLightPos.listPos.push({
                    x: i,
                    y: j,
                    c: hexCurrent,
                  })
                  itemHighLightPos.keyCheckData[keyCheck] = 1;
                  const keyColor = hexCurrent.substring(1);
                  if(!itemHighLightPos.colorData[keyColor]){
                    itemHighLightPos.colorData[keyColor] = []
                  }
                  itemHighLightPos.colorData[keyColor].push(`${i}:${j}`)
                }
                // const checkExist = itemHighLightPos.listPos.find(m=> m.x == i && m.y == j && m.c == hexCurrent)
                // if(!checkExist){ //does not exist
                //   itemHighLightPos.listPos.push({
                //     x: i,
                //     y: j,
                //     c: hexCurrent,
                //   })
                // }
              myCtx.current.beginPath()
              myCtx.current.fillStyle = hexCurrent
              // myCtx.current.arc(i, j, 0.5, 0, pi)
              myCtx.current.fillRect(i, j, 1, 1)
              myCtx.current.fill()
              // ctxCanvasDraw.current.arc(i, j, 0.5, 0, pi)
              ctxCanvasDraw.current.clearRect(i, j, 1, 1)
              ctxCanvasDraw.current.fill()

              //color hightlight
              if(itemHighLightPos.color){
                ctxCanvasDraw.current.beginPath()
                ctxCanvasDraw.current.lineJoin = 'round'
                ctxCanvasDraw.current.globalCompositeOperation = 'source-over'
  
                ctxCanvasDraw.current.fillStyle = `#${itemHighLightPos.color}${charAlpha}`
                // ctxCanvasDraw.current.arc(i, j, 0.5, 0, pi)
                ctxCanvasDraw.current.fillRect(i, j, 1, 1)
                ctxCanvasDraw.current.fill()
              }
              
              if (
                !listData.current[index.current].minX ||
                i < listData.current[index.current].minX
              ) {
                listData.current[index.current].minX = i
              }
              if (
                !listData.current[index.current].maxX ||
                i > listData.current[index.current].maxX
              ) {
                listData.current[index.current].maxX = i
              }

              if (
                !listData.current[index.current].minY ||
                j < listData.current[index.current].minY
              ) {
                listData.current[index.current].minY = j
              }

              if (
                !listData.current[index.current].maxY ||
                j > listData.current[index.current].maxY
              ) {
                listData.current[index.current].maxY = j
              }
            }
          }
        }
      }
     
      function canvasDrawItem (e:any){
        //get image item
        const w = listData.current[index.current].maxX -
        listData.current[index.current].minX;
        const h = listData.current[index.current].maxY -
        listData.current[index.current].minY
        //no width, height
        if(w <= 0 || h <=0) return;
        const imgData = myCtx.current.getImageData(
          listData.current[index.current].minX,
          listData.current[index.current].minY,
          w,
          h,
        )
        const canvasDownload = document.createElement('canvas')
        const ctxDownload: any = canvasDownload.getContext('2d')
        canvasDownload.width = imgData.width
        canvasDownload.height = imgData.height
        ctxDownload.imageSmoothingQuality = 'high';
        ctxDownload.imageSmoothingEnabled = true;
        ctxDownload.putImageData(imgData, 0, 0)
        let url = ''
        url = canvasDownload.toDataURL('image/png', 1)
        canvasDownload.remove();
        listData.current[index.current].data = url
        listImageRef.current[index.current] = { ...listImageRef.current[index.current],color:listHighLightPos.current[index.current]?.color, data: url }

        // console.log("canvasDrawItem-----image",listImage)
        onChangeItem(listImageRef.current)
      }
      function canvasMouseLeave(e:any){
        const isDownBefore = isDown?.current;
        const isMouseMoveBefore = isMouseMove?.current;
        if(isDownBefore){
          isDown.current = false
        }
        if(isMouseMoveBefore){
          isMouseMove.current = false;
        }
        if(viewModeRef.current == true) return
        if (!isDownBefore || !isMouseMoveBefore) return
        canvasDrawItem(e);
      }
      function canvasMouseUp(e: any) {
        const isDownBefore = isDown?.current;
        const isMouseMoveBefore = isMouseMove?.current;
        if(isDownBefore){
          isDown.current = false
        }
        if(isMouseMoveBefore){
          isMouseMove.current = false;
        }
        if(viewModeRef.current == true) return
        if (!isDownBefore || !isMouseMoveBefore) return
        canvasDrawItem(e);
      }
      function canvasMouseDown(e: any) {
        if(viewModeRef.current == true) return
        if (index.current >= 0 && !isDown.current && imgElement.current) {
          isDown.current = true
        } else {
          isDown.current = false
          isMouseMove.current = false;
          const eventLocation = getEventLocation(canvas.current, e)
          const indexItem = listHighLightPos.current.findIndex(m=>m.listPos.findIndex(n=> n.x == eventLocation.x && n.y == eventLocation.y)!=-1)
          
          if(indexItem !=-1){
            selectItem(indexItem)
          }
          e.preventDefault()
          e.stopPropagation()
        }
      }

      //callback
      // onChangeItem([...listImage])
      return () => {
        if(canvas.current){
          canvas.current.removeEventListener('mouseup', canvasMouseUp)
          canvas.current.removeEventListener('mousedown', canvasMouseDown)
          canvas.current.removeEventListener('mousemove', canvasMouseMove)
          canvas.current.removeEventListener('mouseleave', canvasMouseMove)
        }
      }
    }, [listImageRef.current])

    const renderImageFromData = () => {
      const list: DataImageType[] = []
      listHighLightPos.current.forEach((item, indexItem) => {
        myCtx.current.clearRect(
          0,
          0,
          myCanvas.current.width,
          myCanvas.current.height,
        )
        const len = item.listPos.length
        for (let i = 0; i < len; i++) {
          const element = item.listPos[i]
          myCtx.current.beginPath()
          myCtx.current.fillStyle = element.c
          // myCtx.current.arc(element.x, element.y, 0.5, 0, pi)
          myCtx.current.fillRect(element.x, element.y, 1, 1)
          myCtx.current.fill()
        }
        const minMax = coordinates.getMaxMinXY(item.listPos)
        const imgData = myCtx.current.getImageData(
          minMax.minX,
          minMax.minY,
          minMax.maxX - minMax.minX,
          minMax.maxY - minMax.minY,
        )
        // canvas.style.display = "block"
        const canvasDownload = document.createElement('canvas')
        const ctxDownload: any = canvasDownload.getContext('2d')
        canvasDownload.width = imgData.width
        canvasDownload.height = imgData.height
        ctxDownload.imageSmoothingQuality = 'high';
        ctxDownload.msImageSmoothingEnabled  = true;
        ctxDownload.mozImageSmoothingEnabled = true;
        ctxDownload.webkitImageSmoothingEnabled  = true;
        ctxDownload.imageSmoothingEnabled = true;
        ctxDownload.putImageData(imgData, 0, 0)
        const url = canvasDownload.toDataURL('image/png', 1)
        canvasDownload.remove();
        list.push({ data: url,key: item.key, color:item.color})
        listData.current.push({ data: url, ...minMax })
      })
      // setListImage(list)
      listImageRef.current = list;
      onChangeItem(list)
      myCtx.current.clearRect(
        0,
        0,
        myCanvas.current.width,
        myCanvas.current.height,
      )
    }
    const drawItemCanvas = () => {
      // const myCanvas: any = document.getElementById("myCanvas");
      // const myCtx = myCanvas.getContext("2d");

      myCtx.current.clearRect(
        0,
        0,
        myCanvas.current.width,
        myCanvas.current.height,
      )
      
      listHighLightPos.current.forEach((item, indexItem) => {
        if (indexItem == index.current) {
          const list = item.listPos;
          const len = list.length
          for (let i = 0; i < len; i++) {
            const element = list[i]
            // myCtx.current.beginPath()
            // myCtx.current.arc(element.x, element.y, 0.5, 0, pi)
            myCtx.current.fillStyle = element.c
            myCtx.current.fillRect(element.x, element.y, 1, 1)
            myCtx.current.fill()
            
          }
        }
      })
    }
    const drawHighLightCanvas = () => {
      ctxCanvasDraw.current.clearRect(
        0,
        0,
        canvasDraw.current.width,
        canvasDraw.current.height,
      )
      ctxCanvasDraw.current.drawImage(
        imgElement.current,
        0,
        0,
        imgElement.current.width,
        imgElement.current.height,
        0,
        0,
        canvasDraw.current.width,
        canvasDraw.current.height,
      )
      if (index.current >= 0) {
        ctxCanvasDraw.current.beginPath()
        ctxCanvasDraw.current.globalCompositeOperation = 'source-over'
        ctxCanvasDraw.current.lineJoin = 'round'
        ctxCanvasDraw.current.fillStyle = '#00000077'
        ctxCanvasDraw.current.fillRect(
          0,
          0,
          canvasDraw.current.width,
          canvasDraw.current.height,
        )
        ctxCanvasDraw.current.fill()
      }
      listHighLightPos.current.forEach((item, indexItem) => {
        if (
          indexItem != index.current &&
          item.listPos.length > 0
        ) {
          ctxCanvasDraw.current.beginPath()
          ctxCanvasDraw.current.globalCompositeOperation = 'source-over'
          ctxCanvasDraw.current.fillStyle = '#000'
          if(item.color){
            ctxCanvasDraw.current.fillStyle = `#${item.color}${charAlpha}`
            const list = item.listPos
            const len =list.length
            for (let i = 0; i < len; i++) {
              const element = list[i]
              ctxCanvasDraw.current.fillRect(element.x, element.y, 1, 1)
            }
            ctxCanvasDraw.current.fill()
            
          }
            setTimeout(() => {
              ctxCanvasDraw.current.fillStyle = item.color?'#fff':"#000"
              const list = coordinates.getRoundLine(item.listPos)
            const len =list.length
            for (let i = 0; i < len; i++) {
              const element = list[i]
              ctxCanvasDraw.current.fillRect(element.x, element.y, 1, 1)
            }
            ctxCanvasDraw.current.fill()
            }, 0);
          
        }
      })
      if (index.current >= 0) {
        const itemHighLightPos = listHighLightPos.current[index.current];
        const list = itemHighLightPos?.listPos || []
        const len = list.length
        if (len > 0) {
          for (let i = 0; i < len; i++) {
            const element = list[i]
            ctxCanvasDraw.current.clearRect(
              element.x, element.y, 1, 1,
            )
          }
          ctxCanvasDraw.current.fill()
          if(itemHighLightPos.color){
            ctxCanvasDraw.current.globalCompositeOperation = 'source-over'
            ctxCanvasDraw.current.lineJoin = 'round'
            ctxCanvasDraw.current.fillStyle = `#${itemHighLightPos.color}${charAlpha}`
            for (let i = 0; i < len; i++) {
              const element = list[i]
              ctxCanvasDraw.current.fillRect(element.x, element.y, 1, 1)
            }
            // ctxCanvasDraw.current.fill()
          }
        }
      }
    }


    const removeItem = (indexItem: number) => {
      if(listImageRef.current.length <= indexItem) return;
      index.current = - 1
      listImageRef.current.splice(indexItem, 1)
      listHighLightPos.current.splice(indexItem, 1)
      listData.current.splice(indexItem,1)
      setTimeout(() => {
        drawHighLightCanvas()
      }, 0);
      setIndexSelected(index.current)
      onChangeItem(listImageRef.current)
      
    }
    const addNewItem = (color:string) => {
      if(!imgElement.current) return;
      index.current = listHighLightPos.current.length
      setIndexSelected(index.current)
      const key = Guid.newGuid()
      listHighLightPos.current.push({
        listPos: [],
        key:key,
        color:color,
        keyCheckData:{},
        colorData:{}
      })
      listData.current.push({
        minX: canvasDraw.current.width,
        minY: canvasDraw.current.height,
        maxX: 0,
        maxY: 0,
        data: '',
      })
      listImageRef.current.push({ data: '', key: key, color:color })
      onChangeItem(listImageRef.current)
      myCtx.current.clearRect(
        0,
        0,
        myCanvas.current.width,
        myCanvas.current.height,
      )
      setTimeout(() => {
        drawHighLightCanvas()
      }, 0);
    }
    const changeImageUrl = (url: string)=>{
      index.current = -1;
      setIndexSelected(-1);
      listHighLightPos.current = []
      listData.current =[]
      clearAllCanvas();
      setShowImageDefault(true)
      drawImageFromWebUrl(url)
    }
    const clearImageInput = ()=>{
      index.current = -1;
      setIndexSelected(-1);
      listHighLightPos.current = []
      listData.current =[]
      clearAllCanvas();
      setShowImageDefault(true)
      // setListImage([])
      listImageRef.current=[]
      onChangeItem([])
      onChangeImageData && onChangeImageData({src:"", width:0, height:0})
    }
    const selectItem = (indexItem:number)=>{
      if(indexItem >= listHighLightPos.current.length) return false;
      index.current = indexItem
      setIndexSelected(index.current)
      drawItemCanvas()
      setTimeout(() => {
        drawHighLightCanvas()
      }, 0);
      return true;
    }
    const changeColorItem = (indexItem:number, color:string)=>{
      if(indexItem >= listHighLightPos.current.length) return false;
      index.current = indexItem
      setIndexSelected(index.current)
      listHighLightPos.current[indexItem].color = color
      drawItemCanvas()
      setTimeout(() => {
        drawHighLightCanvas()
      }, 0);
      listImageRef.current[indexItem].color = color;
      onChangeItem(listImageRef.current)
      return true;
    }
    const clearAllCanvas = ()=>{
      myCtx.current.clearRect(
        0,
        0,
        myCanvas.current.width,
        myCanvas.current.height,
      )
      ctxCanvasDraw.current.clearRect(
        0,
        0,
        myCanvas.current.width,
        myCanvas.current.height,
      )
      ctxCanvas.current.clearRect(
        0,
        0,
        myCanvas.current.width,
        myCanvas.current.height,
      )
    }
    return (
      <div className={`image-split-container-tool ${className?className:""}` } style={style}>
        <div
          id="image-split-content-image-draw-id"
          className={`image-split-content-image-draw ${showImageDefault?"no-data":""}`}
          style={{ position: 'relative', width: '0', height: '0' }}
        >
          <div
            style={{
              display: 'block',
              position: 'absolute',
              left: '100%',
            }}
          >
            <canvas ref={myCanvas} id="myCanvas" style={{display:"none", imageRendering:"pixelated"}} width="0" height="0"></canvas>
           
          </div>
          <canvas
          className={`${!viewMode?"cursor-pointer":""}`}
            ref={canvas}
            style={{ position: 'absolute' }}
            id="canvas"
            width="0"
            height="0"
          ></canvas>
          <canvas
            ref={canvasDraw}
            style={{ position: 'absolute', pointerEvents: 'none' }}
            id="canvasDraw"
            width="0"
            height="0"
          ></canvas>
           
        </div>
        {showImageDefault && (<div className='image-split-container-tool__default-content' style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  cursor:'pointer'
                }} onClick={onClickChangeImage}>
                <img
                  className="icon"
                  src="/images/icons/ic-image.png"
                  
                  alt=''
                />
                <span>{t('create-question')['image']}</span>
                </div>)}
        {!showImageDefault && !viewMode && <img
                  className="image-delete-icon"
                  src="/images/icons/ic-remove-opacity.png"
                  alt=""
                  onClick={() => {
                    event.preventDefault()
                    event.stopPropagation()
                    clearImageInput();
                  }}
                />}
      </div>
    )
  },
)
SplitImage.displayName = ' split image'
