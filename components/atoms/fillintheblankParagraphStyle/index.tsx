import { convertStrToHtml, formatTextStyleObject } from 'utils/string'
import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  answer?: string
  data?: string
}

export const FBParagraphStyle = ({
  className = '',
  answer,
  data,
  style,
}: PropsType) => {
  const formatData = () => {
    const answerList = answer.split('#')
    // const dataList = data.replace(/\n/g, '<br />').split('%s%')
    const dataStr = data.replace(/\n/g, '<br />').replaceAll("%s%","###")
    const newData = formatTextStyleObject(dataStr)
    // console.log("newData-----", newData);
    const result:any[] = []
    newData.forEach((obj:any, indexObj:number) => {
      const textArr = obj.text.split('###')
      const len =  textArr.length
      textArr.map((item:any, aIndex:number) => {
        result.push({
          ...obj,
          text: item,
        })
        if(aIndex < len - 1){
          result.push({
            text: '%s%',
            bold: undefined,
            underline: undefined,
            italic: undefined,
          })
        }
      })
    })
    
    let returnStr = ''
    let index = 0
    result.forEach((item, i) => {
      if(item.text === '%s%'){
        if(answerList[index]?.startsWith("*")){
          returnStr += ` <span class="answers-ex">${answerList[index]?.replace(
            /\%\/\%/g,
            '/',
          ).replace("*","")} </span>`
        } else{
          returnStr += `${
            answerList[index]
              ? `____<span class="answers">${answerList[index]?.replace(
                  /\%\/\%/g,
                  '/',
                ).replace("*","")}</span>____ `
              : ''
          }` 
        }
        index++
      } else{
        let tempText = item.text;
        if(item.underline){
          tempText = `<u>${tempText}</u>`
        }
        if(item.bold){
          tempText = `<b>${tempText}</b>`
        }
        if(item.italic){
          tempText = `<i>${tempText}</i>`
        }
        returnStr += tempText;
      }
    })
    const textHtml = convertStrToHtml(returnStr)
    return textHtml
  }

  return (
    <div className={`a-fb-paragraph-style ${className}`} style={style}>
      <div className="a-fb-paragraph-style__title">
        <p
          dangerouslySetInnerHTML={{
            __html: formatData(),
          }}
        ></p>
      </div>
    </div>
  )
}
