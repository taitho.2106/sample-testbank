import { useContext, useEffect, useState } from 'react'

import { Button, Popover, Tooltip, Whisper } from 'rsuite'

import { SingleTemplateContext, WrapperContext } from '../../../interfaces/contexts'
import { DefaultPropsType, StructType } from '../../../interfaces/types'
import { InputWithLabel } from '../inputWithLabel'
import { MultiSelectPicker } from '../selectPicker/multiSelectPicker'
import { useSession } from 'next-auth/client'
import { userInRight } from '@/utils/index'
import { PACKAGES, USER_ROLES } from '@/interfaces/constants'
import useTranslation from "@/hooks/useTranslation";
import PopoverUpgradeContent from 'components/molecules/unitTestTable/PopoverUpgradeContent'
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'
import { studentPricings } from '@/componentsV2/Home/data/price'
import { dataWithPackage } from '@/componentsV2/TeacherPages/UnitTestPage/data/dataWithPackage'

interface PropsType extends DefaultPropsType {
  id: number
  index?: number
  feature?: 'unittest' | 'template'
  isActive?: boolean
  isDisabled?: boolean
  name?: string
  points?: number
  questionList?: any[]
  skill?: StructType[]
  skillList?: StructType[]
  type?: 'default' | 'add'
  totalQuestion?: number
  onAdd?: () => void
  onChooseQuestion?: () => void
  onDelete?: () => void
  onNameChange?: (id: number, val: string) => void
  onPointsChange?: (id: number, val: string) => void
  onSkillChange?: (
    id: number,
    val: any[],
    reverse: boolean,
    reverseFunc: (boo: boolean) => void,
  ) => void
  onTotalQuestionChange?: (id: number, val: string) => void
  isReady?: boolean
}

const skillThumbnails: any = {
  GR: '/images/collections/clt-grammar.png',
  LI: '/images/collections/clt-listening.png',
  PR: '/images/collections/clt-pronunciation.png',
  RE: '/images/collections/clt-reading.png',
  SP: '/images/collections/clt-speaking.png',
  US: '/images/collections/clt-use-of-english.png',
  VO: '/images/collections/clt-vocab.png',
  WR: '/images/collections/clt-writing.png',
}

export const SectionToggle = ({
  className = '',
  id,
  index,
  feature = 'unittest',
  isActive = false,
  isDisabled = false,
  name = '',
  points,
  questionList = [],
  skill = [],
  skillList = [],
  type = 'default',
  totalQuestion,
  style,
  onAdd = () => null,
  onChooseQuestion = () => null,
  onDelete = () => null,
  onNameChange = () => null,
  onPointsChange = () => null,
  onSkillChange = () => null,
  onTotalQuestionChange = () => null,
  isReady = false
}: PropsType) => {
  const { t } = useTranslation()
  const { globalModal } = useContext(WrapperContext)
  const { chosenTemplate, mode } = useContext(SingleTemplateContext)
  const { listPlans, dataPlan } = useCurrentUserPackage()
  const [ session ] = useSession()
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
  const unitTestEI = chosenTemplate?.state?.is_international_exam === 1
  const isShowPlan = (dataPlan?.code !== PACKAGES.INTERNATIONAL.CODE && unitTestEI && isTeacher)
  const dataDescription = studentPricings.teacher.find(item => item.code === PACKAGES.INTERNATIONAL.CODE)?.descriptions || []
  const dataPackage = listPlans.find((item: any) => item.code === PACKAGES.INTERNATIONAL.CODE)
  const [errorText, setErrorText] = useState<any>({
    isError: false,
    name: '',
    skill: '',
    totalQuestion: '',
    points: '',
    questionList: ''
  })

  const [skillReverse, setSkillReverse] = useState(false)
  const [currentTotalQuestion, setCurrentTotalQuestion] = useState(totalQuestion)

  const getBadgeCount = (
    qArr: any[],
    sArr: any[],
    code: any,
    index: number,
  ) => {
    let count = 0
    qArr.forEach((q) => {
      if (q?.total_question)
        if (sArr.length >= 4 && index >= 3) {
          if (
            code &&
            ![sArr[0].code, sArr[1].code, sArr[2].code].includes(q?.skills)
          )
            count += q.total_question
        } else {
          if (code && q?.skills === code) count += q.total_question
        }
    })
    return count
  }

  const getTotalSentence = (arr: any[]) => {
    let totalChosenSentence = 0
    if (arr) {
      arr.forEach((item) => {
        if (item?.total_question) totalChosenSentence += item.total_question
      })
    }
    return totalChosenSentence
  }
  const totalCurrentQuestion = getTotalSentence(questionList)
  useEffect(() => {
    if(feature === 'unittest'){
      setErrorText((prev: any) => {
        return ({
          isError: mode !== 'detail' && (!name?.length || !points || !totalQuestion || !questionList.length || !skill.length || totalCurrentQuestion !== totalQuestion),
          name: !name?.length ? "Tên đề thi không được để trống" : "",
          skill: !skill.length ? "Vui lòng chọn kỹ năng cho phần thi" : "",
          totalQuestion: !totalQuestion ? "Số câu hỏi không được để trống" : "",
          points: !points ? "Điểm/Phần thi không được để trống" : "",
          questionList: (!questionList.length || totalCurrentQuestion !== totalQuestion) ? "Vui lòng chọn câu hỏi cho phần thi" : "",
        })
      })
    }
  }, [name, points, totalQuestion, questionList.length, skill.length, mode, totalCurrentQuestion, feature])

  const handleNameChange = (val: string) =>
    onNameChange && onNameChange(id, val)

  const handleTotalQuestionType = (val: string) =>
    setCurrentTotalQuestion(parseInt(val))

  const handleTotalQuestionChange = (val: string) =>
    onTotalQuestionChange && onTotalQuestionChange(id, val)

  const handleSkillChange = (val: any[]) => {
    onSkillChange && onSkillChange(id, val, skillReverse, setSkillReverse)
  }

  const handlePointsChange = (val: string) =>
    onPointsChange && onPointsChange(id, val)
  const onKeyDownInputNumber = (e:any,val:any)=>{
    const key = e?.key?.toLowerCase();
    if(["e","+","-"].includes(key)){
      e.preventDefault();
      return;
    }
    if(!val && key == "0"){
      e.preventDefault();
      return;
    }
  }
  const renderTextError = () => {
    let text = ''
    if (errorText.isError) {
      for(const key of Object.keys(errorText)){
        if(key !== 'isError' && !!errorText[key]?.length){
          text = errorText[key]
          break;
        }
      }
    }
    return text
  }
  const textError = renderTextError()
  return (
    <>
      <div
        className={`a-section-toggle ${className} ${errorText.isError && type === 'default' && isReady ? 'error-section' : ''}`}
        data-index={index && `${t('unit-test')['part']} ${index}`}
        data-status={
          skill && skill.length > 0
            ? isActive
              ? 'active'
              : 'default'
            : 'unknown'
        }
        data-type={type}
        style={style}
        onClick={() => type === 'add' && onAdd && onAdd()}
      >
        {type === 'add' && (
          <div className="a-section-toggle__add" data-feature={feature}>
            <img src="/images/icons/ic-plus-sub.png" alt="plus" />
            <span>{t('common')['add-new-section']}</span>
          </div>
        )}
        {type === 'default' && (
          <div className="a-section-toggle__container">
            <div className="a-section-toggle__top">
              <InputWithLabel
                className="info-input"
                defaultValue={name}
                disabled={isDisabled}
                label={t('common')['unit-test-name']}
                type="text"
                onChange={handleNameChange}
                toolTip={t('common-whisper')['part-info']}
                isError={errorText.isError && !!errorText.name?.length}
              />
            </div>
            <div className="a-section-toggle__mid">
              <div
                className="a-section-toggle__icons"
                data-amount={
                  skill.length >= 4 ? 4 : skill.length <= 0 ? 1 : skill.length
                }
              >
                {skill.filter((item: StructType, i) => i < 4).length > 0 ? (
                  skill
                    .filter((item: StructType, i) => i < 4)
                    .map((item: StructType, i) => (
                      <div key={i} className="__icon">
                        {feature === 'unittest' && (
                          <div className="__badge">
                            {getBadgeCount(questionList, skill, item?.code, i)}
                          </div>
                        )}
                        <div className="__icon-container">
                          {skill.length <= 4 || i < 3 ? (
                            <img
                              src={
                                skillThumbnails[item?.code || ''] ||
                                '/images/collections/clt-unknown.png'
                              }
                              alt={item?.display || 'unknown'}
                            />
                          ) : (
                            <span className="tmp-icon">+{skill.length - 3}</span>
                          )}
                        </div>
                      </div>
                    ))
                ) : (
                  <div className="__icon">
                    <div className="__icon-container">
                      <img
                        src="/images/collections/clt-unknown.png"
                        alt="unknown"
                      />
                    </div>
                  </div>
                )}
              </div>
              <div className="a-section-toggle__inputs">
                <MultiSelectPicker
                  className="info-select"
                  data={skillList}
                  defaultValue={skill}
                  disabled={isDisabled}
                  displayAll={false}
                  label={t('common')['skill']}
                  placeholder={t('unit-test')['chosen-skill']}
                  reverseData={skillReverse}
                  onChange={handleSkillChange}
                  toolTip={t('common-whisper')['part-skills']}
                  allIsEmpty={false}
                  isError={errorText.isError && !!errorText.skill?.length}
                />
                <InputWithLabel
                  className="info-input"
                  defaultValue={totalQuestion}
                  disabled={isDisabled}
                  label={t('common')['total-question']}
                  min={0}
                  type="number"
                  onkeyDown={onKeyDownInputNumber}
                  onBlur={handleTotalQuestionChange}
                  onChange={handleTotalQuestionType}
                  toolTip={t('common-whisper')['part-total-question']}
                  isError={errorText.isError && !!errorText.totalQuestion?.length}
                />
                <InputWithLabel
                  className="info-input"
                  decimal={2}
                  defaultValue={points}
                  disabled={isDisabled}
                  label={t('unit-test')['point/part']}
                  min={0}
                  type="number"
                  onkeyDown={onKeyDownInputNumber}
                  onBlur={handlePointsChange}
                  toolTip={t('common-whisper')['part-points']}
                  isError={errorText.isError && !!errorText.points?.length}
                />
              </div>
            </div>
            {feature === 'unittest' && !isShowPlan && (
              <div className="a-section-toggle__bottom">
                <Button
                  className={`popup-toggle ${errorText.isError && !!errorText.questionList?.length ? 'error' : '' }`}
                  disabled={
                    skill.length <= 0 ||
                    !currentTotalQuestion ||
                    currentTotalQuestion <= 0
                  }
                  onClick={onChooseQuestion}
                >
                  <img
                    src={
                      isActive
                        ? '/images/icons/ic-show-white.png'
                        : '/images/icons/ic-question-white.png'
                    }
                    alt="questions"
                  />
                  <span>{isActive ? t('common')['view-questions'] : t('common')['select-questions']}</span>
                </Button>
              </div>
            )}
            {feature === 'unittest' && isShowPlan && (
              <div className="a-section-toggle__bottom">
                <Whisper
                  trigger="hover"
                  placement='auto'
                  enterable
                      speaker={
                        <Popover arrow={false}>
                          <PopoverUpgradeContent
                              dataPackage={dataDescription}
                              price={dataPackage?.exchange_price}
                              itemId={dataPackage?.item_id}
                          />
                        </Popover>}
                >
                  <Button
                    className="popup-toggle block"
                    disabled={true}
                  >
                    <img
                      src='/images/icons/ic-show-white.png'
                      alt="questions"
                    />
                    <span>{t('common')['view-questions']}</span>
                    <div className="__icon-package">{dataWithPackage[PACKAGES.INTERNATIONAL.CODE].icon}</div>
                  </Button>
                </Whisper>
              </div>
            )}
          </div>
        )}
        {type === 'default' && !isDisabled && (
          <Button
            className="delete-btn"
            appearance="link"
            onClick={() =>
              globalModal.setState({
                id: 'confirm-modal',
                type: 'delete-skill',
                content: {
                  closeText: t('modal')['cancel'],
                  submitText: t('modal')['submit-1'],
                  onSubmit: onDelete,
                },
              })
            }
          >
            <img src="/images/icons/ic-close-dark.png" alt="delete" />
          </Button>
        )}
      </div>
      {!!textError.length && type === 'default' && feature === 'unittest' && (
        <div className='a-section-toggle__error-text'>
          <span>{textError}</span>
        </div>
      )}
    </>
  )
}
