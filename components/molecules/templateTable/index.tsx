import { useContext } from 'react'

import { useSession } from 'next-auth/client'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FaEye, FaPencilAlt, FaTrashAlt } from 'react-icons/fa'
import { GoKebabVertical } from 'react-icons/go'
import { Whisper, Popover, Button, Toggle, Loader } from 'rsuite'

import useGrade from 'hooks/useGrade'
import { PACKAGES, USER_ROLES } from 'interfaces/constants'
import { WrapperContext } from 'interfaces/contexts'
import { SKILLS_SELECTIONS } from 'interfaces/struct'
import { DefaultPropsType, StructType } from 'interfaces/types'
import { userInRight } from 'utils'
import useTranslation from "@/hooks/useTranslation";
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'

interface Props extends DefaultPropsType {
  currentPage: number
  data: any[]
  isLoading?: boolean
  onDelete?: (id: number) => void
  onStatusToggle?: (id: number, boo: boolean) => void
}

const skillThumbnails: any = {
  GR: '/images/collections/clt-grammar-green.png',
  LI: '/images/collections/clt-listening-green.png',
  PR: '/images/collections/clt-pronunciation-green.png',
  RE: '/images/collections/clt-reading-green.png',
  SP: '/images/collections/clt-speaking-green.png',
  US: '/images/collections/clt-use-of-english-green.png',
  VO: '/images/collections/clt-vocab-green.png',
  WR: '/images/collections/clt-writing-green.png',
}

export const TemplateTable = ({
  className = '',
  currentPage,
  data,
  isLoading = false,
  style,
  onDelete,
  onStatusToggle,
}: Props) => {
  const { t } = useTranslation()
  const router = useRouter()

  const [session] = useSession()
  const {getGrade} = useGrade()
  const user: any = session?.user
  const isMTeacher = router.query?.m === 'teacher' && user?.is_admin === 1

  const { globalModal } = useContext(WrapperContext)

  const { dataPlan } = useCurrentUserPackage()
  const isEdit = dataPlan?.code !== PACKAGES.BASIC.CODE

  const checkCustomRight = () => {
    let check: any = null
    switch (router.query?.m) {
      case 'mine':
        check = [USER_ROLES.Teacher]
        break
      case 'teacher':
        check = []
        break
      default:
        check = [USER_ROLES.Operator]
        break
    }
    return userInRight(check, session)
  }

  const getSkills = (arr: any[]) => {
    const skillArr: any[] = []
    arr.forEach((parents) => {
      if (Array.isArray(parents) && parents.length > 0) {
        parents.forEach((children) => {
          if (!skillArr.includes(children)) skillArr.push(children)
        })
      }
    })
    return skillArr
  }

  return (
    <div className={`m-template-table ${className}`} style={style}>
      {isLoading && <Loader backdrop content="loading..." vertical />}
      <div className="__table" style={{ opacity: isLoading ? 0 : 1 }}>
        <table>
          <thead>
            <tr>
              <th>{t("STT")}</th>
              <th>{t('table')['template-name']}</th>
              <th>{t('common')['grade']}</th>
              <th>{t('common')['time']}</th>
              <th>{t('common')['total-question']}</th>
              <th>{t('table')['template-in-used']}</th>
              <th>{t('common')['total-skill']}</th>
              <th>{t('learring-result')['status']}</th>
              <th style={isMTeacher ? {} : { display: 'none' }}>{t('table')['created-by']}</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.map((item, i) => (
                <tr key={item.id}>
                  <td>{i + 1 + (currentPage - 1) * 10}</td>
                  <td title={item?.name}>
                    <div className="__elipsis">{item?.name || '---'}</div>
                  </td>
                  <td
                    title={
                      getGrade.length > 0 && getGrade.find(
                        (find: StructType) =>
                          find?.code === item?.templateLevelId,
                      )?.display
                    }
                  >
                    <div className="__elipsis">
                      {getGrade.length > 0 && getGrade.find(
                        (find: StructType) =>
                          find?.code === item?.templateLevelId,
                      )?.display ||
                        '---'}
                    </div>
                  </td>
                  <td title={item?.time}>
                    <div className="__elipsis">{item?.time || '---'}</div>
                  </td>
                  <td title={item?.totalQuestions}>
                    <div className="__elipsis">
                      {item?.totalQuestions || '---'}
                    </div>
                  </td>
                  <td title={item?.totalUnitTests}>
                    <div className="__elipsis">
                      {item?.totalUnitTests || item?.totalUnitTests === 0
                        ? item.totalUnitTests
                        : '---'}
                    </div>
                  </td>
                  <td
                    title={getSkills(item.sections)
                      .map(
                        (item) =>
                          SKILLS_SELECTIONS.find((skill:any) => skill.code === item)
                            ?.display || item,
                      )
                      .join(', ')}
                  >
                    <Whisper
                      placement="auto"
                      trigger="hover"
                      speaker={
                        <Popover>
                          <div className="m-unittest-template-table-data__section-popover">
                            {getSkills(item.sections).map(
                              (item: any, i: number) => (
                                <div key={i} className="section-item">
                                  <img src={skillThumbnails[item]} alt={item} />
                                  <span>
                                    {SKILLS_SELECTIONS.find(
                                      (skill:any) => skill.code === item,
                                    )?.display || item}
                                  </span>
                                </div>
                              ),
                            )}
                          </div>
                        </Popover>
                      }
                    >
                      <span onClick={(e) => e.stopPropagation()}>
                        <span
                          style={{
                            color: '#6868AC',
                            textDecoration: 'underline',
                            cursor: 'pointer',
                          }}
                        >
                          {getSkills(item.sections).length || 0}
                        </span>
                        <sup style={{ color: 'red' }}>*</sup>
                      </span>
                    </Whisper>
                  </td>
                  <td>
                    {checkCustomRight() ? (
                      <Whisper
                        placement="top"
                        trigger="hover"
                        speaker={
                          <Popover>
                            {t('unit-test')['tooltip-status']}
                          </Popover>
                        }
                      >
                        <Toggle
                          checked={item?.status || false}
                          size="md"
                          onChange={() =>
                            checkCustomRight() &&
                            onStatusToggle &&
                            onStatusToggle(
                              item?.id || null,
                              !(item?.status || false),
                            )
                          }
                        />
                      </Whisper>
                    ) : (
                      <img
                        src={
                          item?.status
                            ? '/images/icons/ic-check-circle.png'
                            : '/images/icons/ic-close-circle.png'
                        }
                        alt={item?.status ? 'active' : 'inactive'}
                        style={{
                          width: 24,
                          height: 24,
                          objectFit: 'contain',
                          objectPosition: 'center',
                        }}
                      />
                    )}
                  </td>
                  <td
                    title={item?.userName}
                    style={isMTeacher ? {} : { display: 'none' }}
                  >
                    <div className="__elipsis">{item?.userName || '---'}</div>
                  </td>
                  <td>
                    <div className="__action-group">
                      <Whisper
                        placement="auto"
                        trigger="click"
                        preventOverflow={true}
                        container={() => document.querySelector('.m-template-table')}
                        speaker={
                          <Popover>
                            <div className="table-action-popover">
                              {checkCustomRight() && isEdit && (
                                <Link
                                  as={`/templates/${item.id}?mode=edit${
                                    router.query.m ? `&m=${router.query.m}` : ''
                                  }`}
                                  href={`/templates/[templateSlug]?mode=edit${
                                    router.query.m ? `&m=${router.query.m}` : ''
                                  }`}
                                >
                                  <a className="__item">
                                    <FaPencilAlt />
                                    <span>{t('common')['edit']}</span>
                                  </a>
                                </Link>
                              )}
                              <Link
                                as={`/templates/${item.id}?mode=view${
                                  router.query.m ? `&m=${router.query.m}` : ''
                                }`}
                                href={`/templates/[templateSlug]?mode=view${
                                  router.query.m ? `&m=${router.query.m}` : ''
                                }`}
                              >
                                <a className="__item">
                                  <FaEye />
                                  <span>{t('common')['view']}</span>
                                </a>
                              </Link>
                              {checkCustomRight() && (
                                <div
                                  className="__item"
                                  onClick={() =>
                                    globalModal.setState({
                                      id: 'confirm-modal',
                                      type: 'delete-template',
                                      content: {
                                        closeText: t('modal')['cancel'],
                                        submitText: t('modal')['submit-2'],
                                        onSubmit: () =>
                                          onDelete(item?.id || null),
                                      },
                                    })
                                  }
                                >
                                  <FaTrashAlt />
                                  <span>{t('common')['deleted']}</span>
                                </div>
                              )}
                            </div>
                          </Popover>
                        }
                      >
                        <Button className="__action-btn">
                          <GoKebabVertical color="#6262BC" />
                        </Button>
                      </Whisper>
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </table>
      </div>
    </div>
  )
}
