import Link from 'next/link'
import { useRouter } from 'next/router'

import { DefaultPropsType, BreadcrumbItemType } from '../../../interfaces/types'

interface Props extends DefaultPropsType {
  data: BreadcrumbItemType[]
}

export const BreadCrumbNav = ({ className = '', data, style }: Props) => {
  const router = useRouter()
  return (
    <div className={`m-breadcrumb-nav ${className}`} style={style}>
      {data.map((item, i: number) => {
        let appendQuery = !item.ignoreQueryCheck;
        if(!router?.query?.m){
          appendQuery = false;
        }
        return (
          <div key={i} className="m-breadcrumb-nav__item">
            {item.url ? (
              <Link href={`${item.url}${appendQuery?`?m=${router.query.m}`:''}`}>
                <a>{item.name}</a>
              </Link>
            ) : (
              <span>{item.name}</span>
            )}
          </div>
        )
      })}
    </div>
  )
}
