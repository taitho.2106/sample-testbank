import { useEffect, useState } from "react"
import { convertStrToHtml } from "@/utils/string";
import useTranslation from "@/hooks/useTranslation";

function TemplateTrueFalseType4({ data, mode, className, style }: any) {
    // console.log("data=====tf4", data)
    // console.log("mode=====tf4", mode)
    const {t} = useTranslation()
    const imageArr = data.image.split('#')
    const correctArr = data.correct_answers.split('#')
    const answerArr = data.answers.split('#')
   
    const [examArr, setExamArr] = useState([])
    const [answerList, setAnswerList] = useState([])
    useEffect(() => {
        const answerList = answerArr.map((item:string, index:number)=>{
            if(item.startsWith('*')){
              item = item.replace('*','')
            }
            return item
        })
        setAnswerList(answerList)
        if (data.isExam) {
            setExamArr(data.isExam.split('#'))
        } else {
            const arr: any[] = [];
            for (let i = 0; i < answerArr.length; i++) {
                if (answerArr[i].startsWith('*')) {
                    arr[i] = 'false'
                } else {
                    arr[i] = 'true'
                }
            }
            console.log("new new ", arr)
            setExamArr(arr)
        }

    }, [])
    
    return (
        <div className={`m-template-true-false-type4 ${className}`} style={style}>
            <div className={"m-template-true-false-type4__header"}>
                <span>{data.question_description}</span>
            </div>
            <div className="m-template-true-false-type4__question">
                <table>
                    <thead>
                        <tr>
                            <th align="center">{t('common')['question']}/ {t('common')['example']}</th>
                            <th align="left">{t('create-question')['list-questions-examples']}</th>
                            <th></th>
                            <th align="center">True/ False</th>
                        </tr>
                    </thead>
                    <tbody id="scroll-bar-body">
                        {answerList.map((answer: string, answerIndex: number) => {
                            const src = imageArr[answerIndex].startsWith('blob') ? imageArr[answerIndex] : `/upload/${imageArr[answerIndex]}`
                            return (
                                <tr key={answerIndex + 1} id={`${(answerIndex + 1).toString()}`}>
                                    <td align="center" className={`${examArr[answerIndex] === 'false' ? 'exam_answer' : 'answer'}`}>
                                        {examArr[answerIndex] === 'false' ? t('common')['example'] : t('common')['question']}
                                    </td>
                                    <td className="__image">
                                        <img src={src} alt={answer} />
                                    </td>
                                    <td className="__content">
                                        <span dangerouslySetInnerHTML={{__html: convertStrToHtml(answer) }}></span>
                                    </td>
                                    <td align="center">
                                        <img
                                            src={
                                                correctArr[answerIndex] === 'T'
                                                    ? '/images/icons/ic-true.png'
                                                    : '/images/icons/ic-false.png'
                                            }
                                            alt={answer}
                                        />
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default TemplateTrueFalseType4;