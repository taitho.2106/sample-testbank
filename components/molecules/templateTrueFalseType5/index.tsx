import useTranslation from "@/hooks/useTranslation";
import { convertStrToHtml } from "@/utils/string";

function TemplateTrueFalseType5({ data, mode, className, style }: any) {
    const {t} = useTranslation()
    const imageArr = data.image.split('#') ?? []
    const correctArr = data.correct_answers.split('#') ?? []
    const answerArr = data.answers.split('#') ?? []
   
    const exampleData: data[] = []
    const answersData: data[] = []
    answerArr.forEach((item:string, mIndex:number) => {
        if(item.startsWith('*')){
            exampleData.push({
                answer:item.substring(1),
                image: imageArr[mIndex],
                correctAnswer: correctArr[mIndex],
                isNotExam: false
            })
        }else{
            answersData.push({
                answer:item,
                image: imageArr[mIndex],
                correctAnswer: correctArr[mIndex],
                isNotExam: true
            }) 
        }
    });
    
    return (
        <div className={`m-template-true-false-type5 ${className}`} style={style}>
            <div className={"m-template-true-false-type5__header"}>
                <span>{data.question_description}</span>
            </div>
            <div className="m-template-true-false-type5__audio">
                <audio controls>
                    <source 
                        src={data.audio.startsWith('blob') ? data.audio : `/upload/${data.audio}`}
                        type="audio/mpeg" 
                    />
                </audio>
            </div>
            <div className="m-template-true-false-type5__question">
                <table>
                    <thead>
                        <tr>
                            <th align="center">{t('common')['question']}/ {t('common')['example']}</th>
                            <th align="left">{t('create-question')['list-questions-examples']}</th>
                            <th></th>
                            <th align="center">True/ False</th>
                        </tr>
                    </thead>
                    <tbody id="scroll-bar-body">
                        {exampleData.map((item: data, answerIndex: number) => {
                            const src = item.image.startsWith('blob') ? item.image : `/upload/${item.image}`
                            return (
                                <tr key={answerIndex + 1} id={`${(answerIndex + 1).toString()}`}>
                                    <td align="center" className={`${item.isNotExam === false ? 'exam_answer' : 'answer'}`}>
                                        {item.isNotExam === false ? t('common')['example'] : t('common')['question']}
                                    </td>
                                    <td className="__image">
                                        <img src={src} alt={''} />
                                    </td>
                                    <td className="__content">
                                        <span
                                            dangerouslySetInnerHTML={{
                                                __html: convertStrToHtml(item.answer),
                                            }}
                                        ></span>
                                        {/*<span>{item.answer}</span>*/}
                                    </td>
                                    <td align="center">
                                        <img
                                            src={
                                                item.correctAnswer === 'T'
                                                    ? '/images/icons/ic-true.png'
                                                    : '/images/icons/ic-false.png'
                                            }
                                            alt={''}
                                        />
                                    </td>
                                </tr>
                            )
                        })}
                        {answersData.map((item: data, answerIndex: number) => {
                            const src = item.image.startsWith('blob') ? item.image : `/upload/${item.image}`
                            return (
                                <tr key={answerIndex + 1} id={`${(answerIndex + 1).toString()}`}>
                                    <td align="center" className={`${item.isNotExam === false ? 'exam_answer' : 'answer'}`}>
                                        {item.isNotExam === false ? t('common')['example'] : t('common')['question']}
                                    </td>
                                    <td className="__image">
                                        <img src={src} alt={''} />
                                    </td>
                                    <td className="__content">
                                        <span
                                            dangerouslySetInnerHTML={{
                                                __html: convertStrToHtml(item.answer),
                                            }}
                                        ></span>
                                    </td>
                                    <td align="center">
                                        <img
                                            src={
                                                item.correctAnswer === 'T'
                                                    ? '/images/icons/ic-true.png'
                                                    : '/images/icons/ic-false.png'
                                            }
                                            alt={''}
                                        />
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
type data = {
    answer : string,
    image : string,
    correctAnswer : string,
    isNotExam : boolean
}
export default TemplateTrueFalseType5;