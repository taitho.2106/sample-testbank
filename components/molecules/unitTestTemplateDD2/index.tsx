import useTranslation from "@/hooks/useTranslation"

export default function UnitTestTemplateDD2({ data, className, style }: any) {
    const {t} = useTranslation()
    const imageArr = data.image.split('#') ?? []
    const correctArr = data.correct_answers.split('#') ?? []
    const exampleData: data[] = []
    const answersData: data[] = []
    correctArr.forEach((item:string, mIndex:number) => {
        if(item.startsWith('*')){
            exampleData.push({
                correct_answer: item.substring(1),
                image: imageArr[mIndex],
                isNotExam: false
            })
        }else{
            answersData.push({
                correct_answer: item,
                image: imageArr[mIndex],
                isNotExam: true
            }) 
        }
    });
    
    return (
        <div className={`m-unittest-dd2 ${className}`} style={style}>
            <div className={"m-unittest-dd2__header"}>
                <span>{data.question_description}</span>
            </div>
            <div className="m-unittest-dd2__audio">
                <audio controls>
                    <source 
                        src={data.audio.startsWith('blob') ? data.audio : `/upload/${data.audio}`}
                        type="audio/mpeg" 
                    />
                </audio>
            </div>
            <div className="m-unittest-dd2__question">
                <table>
                    <thead>
                        <tr>
                            <th align="center">{t('common')['question']}/ {t('common')['example']}</th>
                            <th align="center">{t('create-question')['list-questions-examples']}</th>
                            <th align="center">{t('create-question')['correct-answer']}</th>
                        </tr>
                    </thead>
                    <tbody id="scroll-bar-body">
                        {exampleData.map((item: data, answerIndex: number) => {
                            const src = item.image.startsWith('blob') ? item.image : `/upload/${item.image}`
                            return (
                                <tr key={answerIndex + 1} id={`${(answerIndex + 1).toString()}`}>
                                    <td align="center" className={`${item.isNotExam === false ? 'exam_answer' : 'answer'}`}>
                                        {item.isNotExam === false ? t('common')['example'] : t('common')['question']}
                                    </td>
                                    <td className="__image">
                                        <img src={src} alt={''} />
                                    </td>
                                    <td className="__content">
                                    <input
                                        defaultValue={item.correct_answer}
                                        readOnly={true}
                                    />
                                    </td>
                                </tr>
                            )
                        })}
                        {answersData.map((item: data, answerIndex: number) => {
                            const src = item.image.startsWith('blob') ? item.image : `/upload/${item.image}`
                            return (
                                <tr key={answerIndex + 1} id={`${(answerIndex + 1).toString()}`}>
                                    <td align="center" className={`${item.isNotExam === false ? 'exam_answer' : 'answer'}`}>
                                        {item.isNotExam === false ? t('common')['example'] : t('common')['question']}
                                    </td>
                                    <td className="__image">
                                        <img src={src} alt={''} />
                                    </td>
                                    <td className="__content">
                                    <input
                                        defaultValue={item.correct_answer}
                                        readOnly={true}
                                    />
                                    </td>
                                </tr>
                            )
                        })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}
type data = {
    correct_answer: string,
    image: string,
    isNotExam: boolean
}