import { useEffect, useRef, useState } from 'react';

import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import Guid from 'utils/guid';

import { DefaultPropsType } from '../../../interfaces/types'

interface Props extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateSelectObject1 = ({
  className = '',
  data,
  style,
}: Props) => {
  const sizeWordExport = 680
  const elementRef = useRef<HTMLDivElement>(null)
  const [loading, setLoading] = useState(false)
  const [ratio, setRatio] = useState(1)
  const countImageLoad = useRef(0)
  let answerList:any[] = [];
  let urlImage = data.image_question_base64
  if(data.list_answer_item){
    answerList = JSON.parse(data.list_answer_item);
  }else{
    answerList = data.correct_answers.split("#").map((m:any)=> {
      const position = m.split("*")[1].split(':')
      const dataTemp = m.split("*")[0].replace("ex|",'')
      const isSelectedTemp = m.split("*")[2] === 'True' ? true : false
      return {
        isExample: m.startsWith("ex|"),
        name:m.split("*")[2], 
        data:`/upload/${dataTemp}`, 
        minX:position[0], 
        minY:position[1], 
        maxX:position[2], 
        maxY:position[3],
        isSelected : isSelectedTemp
    }
    })
    urlImage = `/upload/${data.image.split("*")[0]}`
  }
  
  const onLoadImage = () => {
    if (countImageLoad.current < answerList.length + 1) {
      return
    }
      if(!elementRef.current) return;
      const imageInstruction :HTMLImageElement = elementRef.current.querySelector('.image-instruction')
      if(imageInstruction){
        const imageItems :any = elementRef.current.querySelectorAll('.item-image-content')
        const divItems :NodeListOf<HTMLDivElement> = elementRef.current.querySelectorAll('.item-image')
        if (!imageItems) return
        const canvas = elementRef.current.querySelector(
          '#canvas-image-SO1',
        ) as HTMLCanvasElement
        canvas.setAttribute('height', `${imageInstruction.offsetHeight}`)
        canvas.setAttribute('width', `${imageInstruction.offsetWidth}`)
        canvas.style.position = 'absolute'
        canvas.style.top = '0'
        canvas.style.left = '0'
        const iconTrue :NodeListOf<HTMLImageElement> = elementRef.current.querySelectorAll('.icon-success-image')
        const iconFalse :NodeListOf<HTMLImageElement> = elementRef.current.querySelectorAll('.icon-danger-image')
        imageItems.forEach((item:HTMLImageElement,mIndex:number)=>{
          if(!answerList[mIndex].isExample){
            if(answerList[mIndex].isSelected){
              divItems[mIndex].classList.remove('--default')
              setTimeout(() => {
                divItems[mIndex].classList.add('--success')
              }, 0);
            }else{
              divItems[mIndex].classList.remove('--default')
              setTimeout(() => {
                divItems[mIndex].classList.add('--danger')
              }, 0);
            }
          }
        })
      
      }

      
  }

  useEffect(() => {
    //tr-expand in view 'choose question for unit_test'
    let interval:any;
    const trExpand = elementRef.current.closest('.tr-expand')
    if (trExpand) {
      let isDisplayNone = window.getComputedStyle(trExpand).display == 'none'
      if (isDisplayNone) {
        interval = setInterval(() => {
          isDisplayNone = window.getComputedStyle(trExpand).display == 'none'
          if (!isDisplayNone) {
            clearInterval(interval)
            setTimeout(() => {
              onLoadImage()
            }, 300)
          }
        }, 100)
        return
      }else{
        interval = setInterval(() => {
          clearInterval(interval)
          setTimeout(() => {
            onLoadImage()
          }, 300)
        }, 100)
        return
      }
    }else{
      setTimeout(() => {
        onLoadImage()
      }, 300)
    }
    return()=>{
      if(interval){
        clearInterval(interval)
      }
    }
  })
  
  const getPointDraw = (element: HTMLImageElement) => {
    const canvas = document.createElement('canvas')
    const ctx = canvas.getContext('2d')
    const imgHeight = element.offsetHeight
    const imgWidth = element.offsetWidth
    canvas.height = imgHeight
    canvas.width = imgWidth
    canvas.style.visibility = 'hidden'
    ctx.drawImage(element, 0, 0, imgWidth, imgHeight)
    
    for(let i = 0; i < imgHeight; i++){
      const pixelData = ctx.getImageData(imgWidth / 2, i, 1, 1).data
      if (
        pixelData[0] !== 0 ||
        pixelData[1] !== 0 ||
        pixelData[2] !== 0 ||
        pixelData[3] !== 0
      ) {
        return { x: imgWidth / 2, y: i - 7}
      }
    }
  }

  return (
    <div className={`m-unittest-select-object-1 ${className}`} style={style} ref={elementRef}>
      <div className='m-unittest-select-object-1__header'>
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.question_description || ''}
        />
      </div>
      <div className="m-unittest-select-object-1__audio">
        <audio controls>
            <source 
                src={data.audio.startsWith('blob') ? data.audio : `/upload/${data.audio}`}
                type="audio/mpeg" 
            />
        </audio>
      </div>
      <div className="m-unittest-select-object-1__container">
        <div className='m-unittest-select-object-1__container__answers'  style={{height:'100%', position:'relative'}}>
          <img 
            alt='' 
            style={{ maxWidth: `${sizeWordExport}px` }}
            src={urlImage}
            ref={(value)=>{
              if(!value) return
              
              const img = value
              let ratioCurrent = sizeWordExport / value.naturalWidth
              if (ratioCurrent > 1) ratioCurrent = 1
              setRatio(ratioCurrent)
              const updateFunc = () => {
                setLoading(true)
                countImageLoad.current += 1
                onLoadImage()
              }
              img.onload = updateFunc
              if(img.complete){
                updateFunc()
              }
            }}
            className='image-instruction'
            onLoad={() => onLoadImage()}
          />
          {answerList.map((item:any,mIndex:number)=>{
            const width = item.maxX - item.minX
            const height = item.maxY - item.minY
            const left = item.minX
            const top = item.minY
            return (
              <div key={Guid.newGuid()}
                className={`item-image ${item.isExample ? '--example-div' : '--default'}`}
                style={{
                  position:'absolute',
                  height:`${height}px`,
                  width:`${width}px`,
                  left:`${left*ratio}px`,
                  top:`${top*ratio}px`,
                  transform:`scale(${ratio})`,
                  pointerEvents:'none',
                  transformOrigin:"top left"
                }}
              >
                <img 
                  src={item.data}
                  alt=''
                  ref={(value)=>{
                    if(!value) return
                    const img = value
                    const functionLoading = () => {
                      countImageLoad.current += 1
                      const iconTrue: HTMLImageElement = img.parentNode.querySelector('.icon-success-image')
                      const iconFalse: HTMLImageElement = img.parentNode.querySelector('.icon-danger-image')
                      const pointAnswer = getPointDraw(img)
                      if(pointAnswer){
                        const xAnswer = pointAnswer.x
                        const yAnswer = pointAnswer.y
                        iconTrue.style.left = `${xAnswer}px`
                        iconFalse.style.left = `${xAnswer}px`
                        iconTrue.style.top = `${yAnswer}px`
                        iconFalse.style.top = `${yAnswer}px`
                      }
                    }
                    img.onload = functionLoading
                    if(img.complete){
                      functionLoading()
                    }
                  }}
                  className='item-image-content'
                />
                <img 
                  src='/images/icons/ic-true-so.png'
                  alt=''
                  className='icon-success-image'
                />
                <img 
                  src='/images/icons/ic-false-so.png'
                  alt=''
                  className='icon-danger-image'
                />
              </div>
            )
          })}
          <canvas id={`canvas-image-SO1`}></canvas>
        </div>
      </div>

    </div>
  )
}

