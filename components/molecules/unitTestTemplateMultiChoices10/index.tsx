import { MultiChoicesAnwersFont } from 'components/atoms/multichoiceAnswersFont'
import { MultiChoicesQuestion } from 'components/atoms/multichoiceQuestion'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import { getTextWithoutFontStyle } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from '@/hooks/useTranslation'

interface Props extends DefaultPropsType {
    data?: any
}

export const UnitTestTemplateMultiChoices10 = ({
    className = '',
    data,
    style,
}: Props) => {
    const {t} = useTranslation()
    const listQuestionText = data?.question_text?.split('#')
    const listAnswer = data?.answers.split('#')
    const listCorrect = data?.correct_answers.split('#')
    const listImages = data.image.split('#') || []

    const questionsExample: any[] = []
    const answersExample: any[] = []
    const answerCorrectsExample: any[] = []
    const imagesExample: any[] = []

    const questions = []
    const answers: any[] = []
    const answerCorrects: any[] = []
    const images: any[] = []

    for (let i = 0; i < listQuestionText.length; i++) {
        if (listQuestionText[i].startsWith('*')) {
            questionsExample.push(listQuestionText[i].substring(1))
            answersExample.push(listAnswer[i])
            answerCorrectsExample.push(listCorrect[i])
            imagesExample.push(listImages[i])
        } else {
            questions.push(listQuestionText[i])
            answers.push(listAnswer[i])
            answerCorrects.push(listCorrect[i])
            images.push(listImages[i])
        }
    }
    return (
        <div className={`m-unittest-multi-choice-10 ${className}`} style={style}>
            <div>
                <MultiChoicesTutorial
                    className="tutorial"
                    data={data?.parent_question_description || ''}
                />
            </div>
            <div
                style={{
                    display: 'flex',
                    flexDirection:'column'
                }} 
            >
                <div className="m-unittest-multi-choice-10__audio">
                <audio controls>
                        <source 
                            src={data.audio.startsWith('blob') ? data.audio : `/upload/${data.audio}`}
                            type="audio/mpeg" 
                        />
                    </audio>
                </div>
                <div className="m-unittest-multi-choice-10__list-question">
                    <div className="m-unittest-multi-choice-10__right">
                    {questionsExample.length > 0 && (
                        <div className="content-block-question">
                            <div>
                                <span className="__title-example">{t('common')['example']}:</span>
                            </div>
                            <div>
                                {questionsExample.map((question: string, index: number) => {
                                const imageUrl = imagesExample[index].startsWith('blob')
                                    ? imagesExample[index]
                                    : `/upload/${imagesExample[index] || ''}`
                                return (
                                    <div key={`example-${index}`}>
                                    <MultiChoicesQuestion
                                        className="question"
                                        data={question || ''}
                                    />
                                        <div style={{ display: 'flex', margin: '1rem 0' }}>
                                            <div style={{ width: '18rem', marginRight: '3rem' }}>
                                            <img
                                                alt="image"
                                                style={{ width: '18rem' }}
                                                src={imageUrl}
                                            ></img>
                                            </div>
                                            <div className="m-unittest-multi-choice-10__tutorial">
                                            {answersExample[index]
                                                ?.split('*')
                                                .map((item: string, i: number) => {
                                                return (
                                                    <MultiChoicesAnwersFont
                                                    key={`example-ans-${i}`}
                                                    radioName={`mc10-${data.id}`}
                                                    checked={
                                                        answerCorrectsExample[index] ===
                                                        getTextWithoutFontStyle(item.trim())
                                                    }
                                                    className="answers"
                                                    data={item.trim()}
                                                    />
                                                )
                                                })}
                                            </div>
                                        </div>
                                    </div>
                                )
                                })}
                            </div>
                        </div>
                    )}
                    {questions.length > 0 && (
                        <div className="content-block-question">
                            <div>
                                <span className="__title-question">{t('common')['question']}:</span>
                            </div>
                            <div>
                                {questions.map((question: string, index: number) => {
                                const imageUrl = images[index].startsWith('blob')
                                    ? images[index]
                                    : `/upload/${images[index] || ''}`
                                return (
                                    <div key={`question-${index}`}>
                                    {index > 0 && <hr></hr>}
                                    <MultiChoicesQuestion
                                        key={`question-${index}`}
                                        className="question"
                                        data={`${question ?? ''}`}
                                    />
                                    <div style={{ display: 'flex', margin: '1rem 0' }}>
                                        <div style={{ width: '18rem', marginRight: '3rem' }}>
                                        <img
                                            alt="image"
                                            style={{ width: '18rem' }}
                                            src={imageUrl}
                                        ></img>
                                        </div>
                                        <div className="m-unittest-multi-choice-10__tutorial">
                                        {answers[index]
                                            ?.split('*')
                                            .map((item: string, i: number) => (
                                            <MultiChoicesAnwersFont
                                                key={`question-ans-${i}`}
                                                radioName={`mc10-${data.id}`}
                                                checked={
                                                answerCorrects[index] ===
                                                getTextWithoutFontStyle(item)
                                                }
                                                className="answers"
                                                data={item}
                                            />
                                            ))}
                                        </div>
                                    </div>
                                    </div>
                                )
                                })}
                            </div>
                        </div>
                    )}
                    </div>
                </div>
            </div>
        </div>
    )
}
