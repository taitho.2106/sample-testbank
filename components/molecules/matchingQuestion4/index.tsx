import { CSSProperties } from 'react'
import sortExampleQuestionMG4 from 'utils/sortExampleQuestion/template/MG4'
import { convertStrToHtml } from "@/utils/string";
import useTranslation from '@/hooks/useTranslation';

type PropsType = {
  data?: any
  className?: string
  style?: CSSProperties
}

export const MatchingQuestion4 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  const {t} = useTranslation()
  const dataSort = sortExampleQuestionMG4(data)
  // console.log("data--------",dataSort)
  const colData = dataSort?.answers
    .split('#')
    .map((item: string) => item.split('*')) || [[], []]
  // console.log("colData--------",colData)
  const loopLength =
    colData[0].length > colData[1].length
      ? colData[0].length
      : colData[1].length

  const correctList = dataSort?.correct_answers.split('#')
  const posArr = Array.from(Array(loopLength), (e, i) =>
    correctList.findIndex((item: string) => item === colData[1][i]),
  )
  // console.log("posArr-----",posArr)

  //first is example
  if (colData[2] && colData[2][0] && colData[2][0] == 'ex') {
    const indexExample = posArr.indexOf(0)
    if (indexExample > 0) {
      const indexTempP = posArr[0]
      posArr[0] = 0
      posArr[indexExample] = indexTempP
      if (colData[1].length > indexExample) {
        const valueCol1 = colData[1][indexExample]
        colData[1][indexExample] = colData[1][0]
        colData[1][0] = valueCol1
      }
    }
  }
  return (
    <div className={`matching-question4 ${className}`} style={style}>
      <div className="matching-question4__des">
        {dataSort?.question_description || ''}
      </div>
      <div className="matching-question4__content">
        {data?.image && (
          <div className="left-data">
            <div className="__content-image">
              <img
                alt=""
                src={
                  dataSort?.image?.startsWith('blob')
                    ? dataSort?.image
                    : `/upload/${dataSort?.image || ''}`
                }
              ></img>
              <span>{dataSort.audio_script}</span>
            </div>
          </div>
        )}
        <div className={`right-data ${!data?.image?"__100":""}`}>
          <div className="row-data-header">
            <div className="__left-column">
              {t('common')['question']}
              <br />
              /{t('common')['example']}
            </div>
            <div className="__center-column">
              <div>
                <span>{t('create-question')['list-questions-examples']}</span>
              </div>
              <div>
                <span>{t('create-question')['answer-key']}</span>
              </div>
            </div>
            <div className="__right-column">{t('create-question')['position']}</div>
          </div>
          {Array.from(Array(loopLength), (e, i) => {
            const isExample = colData[2] && colData[2][i] == 'ex'
            return (
              <div key={`review-mg4_${i}`} className={`row-data ${isExample ? 'row-example' : ''}`}>
                <div className="__left-column">
                  {colData[0][i]? isExample ? (
                    <span className="title-example">{t('common')['example']}</span>
                  ) : (
                    <span className="title-question">{t('common')['question']}</span>
                  ):"" }
                </div>
                <div className="__center-column">
                  <div className="__left-item" style={!colData[0][i]?{visibility:"hidden"}:{}}>
                    {colData[0][i] && <><span className="__left-item__text"
                    dangerouslySetInnerHTML={{
                      __html: convertStrToHtml(colData[0][i] || '')
                    }}>
                    </span>
                    <div className="__left-item__linked">
                      <div></div>
                      <div></div>
                    </div></>}
                    
                  </div>
                  <div className="__right-item">
                    <div className="__right-item__linked">
                      <div></div>
                      <div></div>
                    </div>
                    <span className="__right-item__text"
                          dangerouslySetInnerHTML={{
                            __html: convertStrToHtml(colData[1][i] || '')
                          }}
                    >
                    </span>
                  </div>
                </div>
                <div className="__right-column">
                  <div className="__index">
                    <span>
                      {posArr[i] === -1 || posArr[i] + 1 > colData[0].length
                        ? 'N/A'
                        : posArr[i] + 1}
                    </span>
                  </div>
                </div>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}
