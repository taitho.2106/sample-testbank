import { MultiChoicesAnwersFont } from 'components/atoms/multichoiceAnswersFont'
import { MultiChoicesQuestion } from 'components/atoms/multichoiceQuestion'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import Guid from 'utils/guid'
import { getTextWithoutFontStyle } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from '@/hooks/useTranslation'

interface Props extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateMultiChoices1 = ({
  className = '',
  data,
  style,
}: Props) => {
  const {t} = useTranslation()
  const listQuestionText:string[] = data?.question_text?.split('#') ?? []
  const listExample:Answer[] = []
  const listQuestions:Answer[] = []
  const listAnswer:string[] = data?.answers.split('#') ?? []
  const listCorrect:string[] = data?.correct_answers.split('#') ?? []
  for(let i = 0; i < listQuestionText.length; i++){
    const item:string = listQuestionText[i]
    if(item.startsWith('*')){
      listExample.push({
        questionText: item.replaceAll('*',''),
        answer: listAnswer[i],
        correctAnswer: listCorrect[i]
      })
    }else{
      listQuestions.push({
        questionText: item,
        answer: listAnswer[i],
        correctAnswer: listCorrect[i]
      })
    }
  }
  return (
    <div className={`m-unittest-multi-choice-1 ${className}`} style={style}>
      <div className="m-unittest-multi-choice-1__turorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={
            data?.question_description ||
            data?.parent_question_description ||
            ''
          }
        />
      </div>
      <div className="m-unittest-multi-choice-1__listquestion" style={{
        marginBottom:'2rem'
      }}>
        {listExample.length > 0 && (
          <>
            <span className='title-example'>{t('common')['example']}</span>
          {listExample.map((item: Answer, index: number) => (
            <div key={`${Guid.newGuid()}_ex`} 
            className='m-unittest-multi-choice-1__group_questions' 
            style={{
              paddingLeft: '3rem'
            }}>
              <div className="m-unittest-multi-choice-1__left">
                <div className="m-unittest-multi-choice-1__question">
                  <MultiChoicesQuestion className="question" data={item.questionText || ''} />
                </div>
              </div>
              <div className="m-unittest-multi-choice-1__right">
                <div className="m-unittest-multi-choice-1__answers">
                  {item.answer
                    ?.split('*')
                    .map((itemAnswer: string, i: number) => {
                      return (
                        <MultiChoicesAnwersFont
                          key={i}
                          radioName={`mc1-${data.id}`}
                          checked={item.correctAnswer === getTextWithoutFontStyle(itemAnswer)}
                          className="answers"
                          data={itemAnswer}
                        />
                      )
                    })}
                </div>
              </div>
            </div>
          ))}
          </>
        )}
        
      </div>
      <div className="m-unittest-multi-choice-1__listquestion">
        <span className='title-question'>{t('common')['question']}</span>
        <div style={{
          paddingLeft: '3rem'
        }}>
        {listQuestions.map((item: Answer, index: number) => (
          <div key={`${Guid.newGuid()}`} className='m-unittest-multi-choice-1__group_questions'>
            <div className="m-unittest-multi-choice-1__left">
              <div className="m-unittest-multi-choice-1__question">
                <MultiChoicesQuestion className="question" data={item.questionText || ''} />
              </div>
            </div>
            <div className="m-unittest-multi-choice-1__right">
              <div className="m-unittest-multi-choice-1__answers">
                {item.answer
                  ?.split('*')
                  .map((itemAnswer: string, i: number) => {
                    return (
                      <MultiChoicesAnwersFont
                        key={i}
                        radioName={`mc1-${data.id}`}
                        checked={item.correctAnswer === getTextWithoutFontStyle(itemAnswer)}
                        className="answers"
                        data={itemAnswer.trim()}
                      />
                    )
                  })}
              </div>
            </div>
          </div>
        ))}
        </div>
        
      </div>
    </div>
  )
}
type Answer = {
  questionText:string,
  answer:string,
  correctAnswer:string
}
