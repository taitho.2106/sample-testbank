import shuffleArray from '@/questions_middleware/shuffleArray'
import { MultiChoicesAnwersFont } from 'components/atoms/multichoiceAnswersFont'
import { MultiChoicesImage } from 'components/atoms/multichoiceImage'
import { MultiChoicesQuestion } from 'components/atoms/multichoiceQuestion'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import ShapeComponent from 'components/atoms/shape'
import { useEffect, useRef } from 'react'
import Guid from 'utils/guid'
import { getTextWithoutFontStyle } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from '@/hooks/useTranslation'

interface Props extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateDrawShape1 = ({
  className = '',
  data,
  style,
}: Props) => {
  const {t} = useTranslation()
  const listAnswer = data?.answers.split('#')
      const indexExample = listAnswer.findIndex((m:any) => m.startsWith("ex|"))
      if (indexExample != -1) {
        const itemExample = listAnswer[indexExample]
        listAnswer.splice(indexExample, 1)
        listAnswer.splice(0, 0, itemExample)
      }
      const renderShapeSelected = (shape: string, color: string) => {
        const height = 56
        switch (shape) {
          case 'circle':
            return <ShapeComponent.CircleShape fill={color} height={height} />
          case 'triangle':
            return <ShapeComponent.TriangleShape fill={color} height={height} />
          case 'square':
            return <ShapeComponent.SquareShape fill={color} height={height} />
          case 'star':
            return <ShapeComponent.StarShape fill={color} height={height} />
          case 'rectangle':
            return <ShapeComponent.RectangleShape fill={color} height={height} />
          case 'oval':
            return <ShapeComponent.OvalShape fill={color} height={height} />
        }
      }
      let indexAnswer = -1;
  return (
    <div
      className={`m-unittest-draw-shape-1 ${className}`}
      style={style}
    >
      <div>
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.question_description || ''}
        />
      </div>
      <div className="m-unittest-draw-shape-1__audio">
                <audio controls>
                    <source 
                        src={data.audio.startsWith('blob') ? data.audio : `/upload/${data.audio}`}
                        type="audio/mpeg" 
                    />
                </audio>
            </div>
      <div className="m-unittest-draw-shape-1__container">
        <div
          className="m-unittest-draw-shape-1__container__answers"
        >
         {listAnswer.map((item:string)=>{
          const items = item.split("*");
          if(!items[0].startsWith("ex|")){
            indexAnswer +=1
          }
          return(
            <div key={Guid.newGuid()} className={`card-answer`}>
             <div className={`card-answer__image ${items[0].startsWith("ex|")?"--example":""}`}>
             {renderShapeSelected(items[1], `#${items[2]}`)}
             </div>
             <span className={`card-answer__text ${items[0].startsWith("ex|")?"--example":""}`}>{items[0].startsWith("ex|")? t('common')['example'] :indexAnswer + 1}</span>
            </div>
          )
         })}
        </div>
      </div>
    </div>
  )
}
