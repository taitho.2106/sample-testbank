import { FBParagraphStyle } from 'components/atoms/fillintheblankParagraphStyle'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateFB2 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  return (
    <div className={`m-unittest-fb2 ${className}`} style={style}>
      <div className="m-unittest-fb2__turorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.question_description || ''}
        />
      </div>
      <FBParagraphStyle
        answer={data?.correct_answers || ''}
        data={data?.question_text || ''}
      />
    </div>
  )
}
