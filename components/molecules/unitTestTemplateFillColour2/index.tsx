import { useEffect, useRef, useState } from 'react';

import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import Guid from 'utils/guid';

import { DefaultPropsType } from '../../../interfaces/types'

interface Props extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateFillColour2 = ({
  className = '',
  data,
  style,
}: Props) => {
  const sizeWordExport = 680
  const elementRef = useRef<HTMLDivElement>(null)
  const [loading, setLoading] = useState(false)
  const [ratio, setRatio] = useState(1)
  let answerList:any[] = [];
  let urlImage = data.image_question_base64
  if(data.list_answer_item){
    answerList = JSON.parse(data.list_answer_item);
  }else{
    answerList = data.correct_answers?.split("#").map((m:any)=> {
      const position = m.split("*")[1]?.split(':')
      const dataTemp = m.split("*")[0]?.replace("ex|",'')
      return {
        isExample: m.startsWith("ex|"),
        color :m.split("*")[2], 
        data:`/upload/${dataTemp}`, 
        minX:position[0], 
        minY:position[1], 
        maxX:position[2], 
        maxY:position[3],
    }
    })||[]
    urlImage = `/upload/${data.image?.split("*")[0]}`
  }
  if(answerList.length==0) return <></>
  const onLoadImage = () => {
      if(!elementRef.current) return;
      const imageInstruction :HTMLImageElement = elementRef.current.querySelector('.image-instruction')
      if(imageInstruction){
        const imageItems :any = elementRef.current.querySelectorAll('.item-image-content')
        if (!imageItems) return
        const canvas = elementRef.current.querySelector(
          '#canvas-image-FC2',
        ) as HTMLCanvasElement
        canvas.setAttribute('height', `${imageInstruction.offsetHeight}`)
        canvas.setAttribute('width', `${imageInstruction.offsetWidth}`)
        canvas.style.position = 'absolute'
        canvas.style.top = '0'
        canvas.style.left = '0'
        imageItems.forEach((item:HTMLImageElement,mIndex:number)=>{
          const heightImage = answerList[mIndex].maxY - answerList[mIndex].minY
          const widthImage = answerList[mIndex].maxX - answerList[mIndex].minX
          const canvasItem:HTMLCanvasElement = elementRef.current.querySelector(`#canvas-image-item-${mIndex}`)
          canvasItem.setAttribute('height', `${heightImage}`)
          canvasItem.setAttribute('width', `${widthImage}`)
          const temp = answerList[mIndex]
          drawColour(canvasItem,item,temp.color)
        })
      }

      
  }
  const drawColour = (canvas:HTMLCanvasElement,img:HTMLImageElement, color:string) =>{
    const ctx = canvas.getContext("2d")
    ctx.drawImage(img, 0, 0, canvas.width, canvas.height)
    ctx.beginPath()
    ctx.globalCompositeOperation = 'source-in'
    ctx.fillStyle = `#${color}80`
    ctx.fillRect(0, 0, canvas.width, canvas.height)
    img.style.display = 'none'
    // ctx.fill()
  }
  useEffect(() => {
    //tr-expand in view 'choose question for unit_test'
    let interval:any;
    const trExpand = elementRef.current.closest('.tr-expand')
    if (trExpand) {
      let isDisplayNone = window.getComputedStyle(trExpand).display == 'none'
      if (isDisplayNone) {
        interval = setInterval(() => {
          isDisplayNone = window.getComputedStyle(trExpand).display == 'none'
          if (!isDisplayNone) {
            clearInterval(interval)
            setTimeout(() => {
              onLoadImage()
            },300)
          }
        }, 100)
        return
      }
    }else{
      setTimeout(() => {
        onLoadImage()
      },300)
    }
    return()=>{
      if(interval){
        clearInterval(interval)
      }
    }
  })
  
  return (
    <div className={`m-unittest-fill-colour-2 ${className}`} style={style} ref={elementRef}>
      <div className='m-unittest-fill-colour-2__header'>
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.question_description || ''}
        />
      </div>
      <div className="m-unittest-fill-colour-2__container">
        <div className='m-unittest-fill-colour-2__container__answers'  
        style={{height:'100%', position:'relative'}}>
          <img 
            alt=''
            style={{ maxWidth: `${sizeWordExport}px` }}
            ref={(value)=>{
              if(!value) return
              const img = value
              let ratioCurrent = sizeWordExport / value.naturalWidth
              if (ratioCurrent > 1) ratioCurrent = 1
              setRatio(ratioCurrent)
              const updateFunc = () => {
                setLoading(true)
                onLoadImage()
              }
              img.onload = updateFunc
              if(img.complete){
                updateFunc()
              }
            }} 
            src={urlImage}
            className='image-instruction'
            onLoad={() => onLoadImage()}
          />
          {answerList.map((item:any,mIndex:number)=>{
            const width = item.maxX - item.minX
            const height = item.maxY - item.minY
            const left = item.minX
            const top = item.minY
            return (
              <div key={Guid.newGuid()}
                className={`item-image ${item.isExample ? '--example-div' : ''}`}
                style={{
                  position:'absolute',
                  height:`${height}px`,
                  width:`${width}px`,
                  left:`${left*ratio}px`,
                  top:`${top*ratio}px`,
                  transform:`scale(${ratio})`,
                  pointerEvents:'none',
                  transformOrigin:"top left"
                }}
              >
                <img 
                  src={item.data}
                  alt=''
                  className='item-image-content'
                />
                <canvas id={`canvas-image-item-${mIndex}`} 
                ></canvas>
              </div>
            )
          })}
          <canvas id={`canvas-image-FC2`}></canvas>
        </div>
      </div>

    </div>
  )
}

