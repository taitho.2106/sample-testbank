import { useEffect } from 'react'

import { MultiChoicesAnwersFont } from 'components/atoms/multichoiceAnswersFont'
import { MultiChoicesQuestion } from 'components/atoms/multichoiceQuestion'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from '@/hooks/useTranslation'
interface PropsType extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateMultiChoices8 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  const {t} = useTranslation()
  const listQuestionText = data?.question_text?.split('#')
  const listAnswer = data?.answers.split('#')
  const listCorrect = data?.correct_answers.split('#')
  const listImageFile = data?.mc8_image_files || []
  const questionsExample: answer[] = []
  const questions: answer[] = []
  for (let i = 0; i < listQuestionText.length; i++) {
    if (listQuestionText[i].startsWith('*')) {
      questionsExample.push({
        answer: listAnswer[i],
        correct_answers: listCorrect[i],
        question_text: listQuestionText[i].replaceAll('*', ''),
        image_files: listImageFile[i]
      })
    } else {
      questions.push({
        answer: listAnswer[i],
        correct_answers: listCorrect[i],
        question_text: listQuestionText[i],
        image_files: listImageFile[i]
      })
    }
  }

  return (
    <div className={`m-unittest-multi-choice-8 ${className}`} style={style}>
      <div className="m-unittest-multi-choice-8__tutorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={
            data?.parent_question_description || ''
          }
        />
      </div>
      <div className="m-unittest-multi-choice-8__list-question">
        <div className="m-unittest-multi-choice-8__image-list-data">
          {questionsExample.length > 0 && (
            <div className="content-block-question">
              <span className='__title-example'>{t('common')['example']}:</span>
              <div className='__content-example'>
                {questionsExample.map((questionE: answer, index: number) => {
                  const answersList = questionE.answer.split('*') || []
                  return (
                    <div key={`example-${index}`}>
                      {index !== 0 && <hr />}
                      <MultiChoicesQuestion
                        className="example-question-text"
                        data={questionE.question_text ? `${questionE.question_text}` : ''}
                      />
                      <div className="m-unittest-multi-choice-8__tutorial">
                      <div className='m-unittest-multi-choice-8__item'>
                          {answersList.map((mAnswer: string, mIndex: number) => {
                            const src = mAnswer.startsWith('blob') ? mAnswer : `/upload/${mAnswer}`
                            if (answersList.length === 4) {
                              return (
                                <div key={`answer-example-${mIndex}`} style={{ flex: 'none', width: 'calc(100% / 2)', marginBottom: '3rem' }}>
                                  <div className='list-example-image'>
                                    <img src={src} alt='answer-example' />
                                    <div
                                      className='checkbox-example length-4'>
                                      {questionE.correct_answers === mIndex.toString() && (
                                        <MultiChoicesAnwersFont
                                          radioName={`mc8-radio`}
                                          checked={true}
                                          className="examples"
                                          data={''}
                                        />
                                      )}
                                    </div>
                                  </div>
                                </div>
                              )
                            } else {
                              return (
                                <div key={`answer-example-${mIndex}`} style={{ flex: 'none', width: 'calc(100% / 3)', marginBottom: '3rem' }}>
                                  <div className='list-example-image'>
                                    <img src={src} alt='answer-example' />
                                    <div
                                      className='checkbox-example'>
                                      {questionE.correct_answers === mIndex.toString() && (
                                        <MultiChoicesAnwersFont
                                          radioName={`mc8-radio`}
                                          checked={true}
                                          className="examples"
                                          data={''}
                                        />
                                      )}
                                    </div>
                                  </div>
                                </div>
                              )
                            }
                          })}
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          )}
          {questions.length > 0 && (
            <div className="content-block-question">
              <span className='__title-answers'>{t('common')['question']}:</span>
              <div className='__content-answers'>
                {questions.map((questionA: answer, index: number) => {
                  const answerList = questionA.answer.split('*') || []
                  return (
                    <div key={`answers-${index}`}>
                      {index !== 0 && <hr />}
                      <MultiChoicesQuestion
                        className="answers-question-text"
                        data={questionA.question_text ?? ''}
                      />
                      <div className="m-unittest-multi-choice-8__tutorial">
                        <div className='m-unittest-multi-choice-8__item'>
                          {answerList.map((mAnswer: string, mIndex: number) => {
                            const src = mAnswer.startsWith('blob') ? mAnswer : `/upload/${mAnswer}`
                            if (answerList.length === 4) {
                              return (
                                <div key={`answer-answers-${mIndex}`} style={{ flex: 'none', width: 'calc(100% / 2)', marginBottom: '3rem' }}>
                                  <div className='list-answers-image'>
                                    <img src={src} alt='answers-example' />
                                    <div
                                      className='checkbox-answers length-4'>
                                      {questionA.correct_answers === mIndex.toString() && (
                                        <MultiChoicesAnwersFont
                                          radioName={`mc8-radio`}
                                          checked={true}
                                          className="answers"
                                          data={''}
                                        />
                                      )}
                                    </div>
                                  </div>
                                </div>
                              )
                            } else {
                              return (
                                <div key={`answer-answers-${mIndex}`} style={{ flex: 'none', width: 'calc(100% / 3)', marginBottom: '3rem' }}>
                                  <div className='list-answers-image'>
                                    <img src={src} alt='answers-example' />
                                    <div
                                      className='checkbox-answers'>
                                      {questionA.correct_answers === mIndex.toString() && (
                                        <MultiChoicesAnwersFont
                                          radioName={`mc8-radio`}
                                          checked={true}
                                          className="answers"
                                          data={''}
                                        />
                                      )}
                                    </div>
                                  </div>
                                </div>
                              )
                            }
                          })}
                        </div>
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
type answer = {
  answer?: string
  correct_answers?: string
  image_files?: any
  question_text?: any
}