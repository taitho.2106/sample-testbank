import { Fragment } from 'react'

import convertFontStyle from '@/questions_middleware/convertFontStyle'
import { MultiChoicesAnwers } from 'components/atoms/multichoiceAnswers'
import { MultiChoicesGeneralQuestion } from 'components/atoms/multichoiceGeneralQuestion'
import { MultiChoicesQuestion } from 'components/atoms/multichoiceQuestion'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import { convertStrToHtml, formatHtmlText, getTextWithoutFontStyle, replaceTyphoStr } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from '@/hooks/useTranslation'

interface PropsType extends DefaultPropsType {
  data?: any
}
type Answers = {
  questionText: string
  answers: string
  correctAnswers: string
}
export const UnitTestTemplateMultiChoices5 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  console.log(data)
  const {t} = useTranslation()
  const questionList: string[] = data.question_text
    .split('#') ?? []

  const answerList: string[] = data.answers.split('#') ?? []

  const correctAnswerList: string[] = data.correct_answers.split('#') ?? []
  const listExampleQuestions: Answers[] = []
  const listAnswersQuestions: Answers[] = []
  for (let i = 0; i < questionList.length; i++) {
    const item: string = questionList[i]
    if (item.startsWith('*')) {
      listExampleQuestions.push({
        questionText: item.replaceAll('*', ''),
        answers: answerList[i],
        correctAnswers: correctAnswerList[i]
      })
    } else {
      listAnswersQuestions.push({
        questionText: item,
        answers: answerList[i],
        correctAnswers: correctAnswerList[i]
      })
    }
  }
  return (
    <div className={`m-unittest-multi-choice-5 ${className}`} style={style}>
      <div className="m-unittest-multi-choice-5__turorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={
            data?.parent_question_description
              ? data.parent_question_description
              : data?.question_description || ''
          }
        />
      </div>
      <div className="m-unittest-multi-choice-5__listquestion">
        <div className="m-unittest-multi-choice-5__left">
          <div className="m-unittest-multi-choice-5__general-question">
            <MultiChoicesGeneralQuestion
              className="question"
              data={data?.parent_question_text || ''}
            />
          </div>
        </div>
        <div className="m-unittest-multi-choice-5__right">
          {listExampleQuestions.length > 0 && (
            <div className='__content_group'>
              <div className='__content_header --example'>
                <span>{t('common')['example']}:</span>
              </div>
              {listExampleQuestions.map((item: Answers, i: number) => (
                <div key={i} className='__group_answer'>
                  {i !== 0 && <hr />}
                  <div className="m-unittest-multi-choice-5__question">
                    <MultiChoicesQuestion
                      className="question"
                      data={item.questionText}
                    />
                  </div>
                  <div className="m-unittest-multi-choice-5__answers">
                    {item.answers.length > 0 &&
                      item.answers.split('*').map((answer: any) => (
                        <MultiChoicesAnwers
                          key={i}
                          radioName={`mc1-${data.id}`}
                          checked={item.correctAnswers.trim() === getTextWithoutFontStyle(answer.trim())}
                          className="answers"
                          data={
                            <label
                              dangerouslySetInnerHTML={{
                                __html: convertStrToHtml(answer.trim()),
                              }}
                            ></label>
                          }
                        />
                      ))}
                  </div>
                </div>
              ))}
            </div>
          )}
          {listAnswersQuestions.length > 0 && (
            <div className='__content_group'>
              <div className='__content_header'>
                <span>{t('common')['question']}:</span>
              </div>
              <div className='__group_answer'>
                {listAnswersQuestions.map((item: Answers, i: number) => (
                  <Fragment key={i}>
                    {i !== 0 && <hr />}
                    <div className="m-unittest-multi-choice-5__question">
                      <MultiChoicesQuestion
                        className="question"
                        data={item.questionText ? `${i + 1}. ${item.questionText}` : ''}
                      />
                    </div>
                    <div className="m-unittest-multi-choice-5__answers">
                      {item.answers.length > 0 &&
                        item.answers.split('*').map((answer: any) => (
                          <MultiChoicesAnwers
                            key={i}
                            radioName={`mc1-${data.id}`}
                            checked={item.correctAnswers.trim() === getTextWithoutFontStyle(answer.trim())}
                            className="answers"
                            data={
                              <label
                                dangerouslySetInnerHTML={{
                                  __html: convertStrToHtml(answer.trim()),
                                }}
                              ></label>
                            }
                          />
                        ))}
                    </div>
                  </Fragment>
                ))}
              </div>

            </div>
          )}

        </div>
      </div>
    </div>
  )
}
