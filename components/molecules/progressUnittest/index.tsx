import { CSSProperties, useContext } from 'react'

import { SingleTemplateContext, WrapperContext } from 'interfaces/contexts'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from "@/hooks/useTranslation";



interface PropsType extends DefaultPropsType {
  max: number
  value: number
  setValue: (val: number) => void
  setMax: (val: number) => void
  onClickStep: (val: number) => void
}

export const ProgressUnittest = ({
  className = '',
  max,
  setMax,
  value,
  setValue,
  onClickStep,
}: PropsType) => {
  const { t } = useTranslation()
  const steps = [
    { id: 1, value: 0, name: t('unit-test')['chosen-template-2'] },
    { id: 2, value: 1, name: t('unit-test')['title-page'] },
    { id: 3, value: 2, name: t('common')['completed'] },
  ]
  const { globalModal }: any = useContext(WrapperContext)
  const { chosenTemplate }: any = useContext(SingleTemplateContext)
  const handleFirstStepClick = () => {
    globalModal.setState({
      id: 'confirm-modal',
      type: 'move-to-first-step',
      content: {
        closeText: t('modal-change-grade')['cancel-action'],
        submitText: t('modal')['submit-action'],
        onSubmit: () => {
          chosenTemplate.setState(null)
          setMax(0)
          setValue(0)
        },
      },
    })
  }
  return (
    <div
      className={`m-progress-unittest ${className}`}
      data-value={(50 * 100) / steps.length}
      style={
        { '--value': `${(max * 100) / (steps.length - 1)}%` } as CSSProperties
      }
    >
      {steps.map((item,index:number) => (
        <div
          key={item.id}
          className="m-progress-unittest__item"
          data-active={item.value === value}
          data-disabled={item.value > max}
          onClick={() =>
            item.value === 0
              ? handleFirstStepClick()
              : item.value !== value &&
                item.value <= max &&
                onClickStep(item.value)
          }
        >
          <div className="badge">{index + 1}</div>
          <span className="title">{item.name}</span>
        </div>
      ))}
    </div>
  )
}
