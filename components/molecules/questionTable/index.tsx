import React, { useContext, useEffect, useMemo, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import Link from 'next/link'
import { FaExchangeAlt, FaEye, FaPencilAlt, FaTrashAlt } from 'react-icons/fa'
import { GoKebabVertical } from 'react-icons/go'
import { Button, toaster, Message, Pagination, Loader, Whisper, Popover } from 'rsuite'

import { paths } from 'api/paths'
import { StickyFooter } from 'components/organisms/stickyFooter'
import useGrade from 'hooks/useGrade'
import { PACKAGES, USER_ROLES } from "interfaces/constants";
import { QuestionContext, WrapperContext } from 'interfaces/contexts'
import { LEVEL_SELECTIONS, SKILLS_SELECTIONS, TASK_SELECTIONS } from 'interfaces/struct'
import { useQuestions } from 'lib/swr-hook'
import { userInRight } from 'utils'

import { DefaultPropsType, QuestionContextType, QuestionDataType } from '../../../interfaces/types'
import { CheckBox } from 'components/atoms/checkbox'
import { AlertNoti } from 'components/atoms/alertNoti'
import api from 'lib/api'
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import styles from "@/componentsV2/TeacherPages/UnitTestPage/WrapperUnitTestV2.module.scss";
import PopoverUpgradeContent from "../unitTestTable/PopoverUpgradeContent";
import { dataWithPackage } from "@/componentsV2/TeacherPages/UnitTestPage/data/dataWithPackage";
import { studentPricings } from "@/componentsV2/Home/data/price";
import useTranslation from "@/hooks/useTranslation";

interface Props extends DefaultPropsType {
  searching: boolean
}
const imagePrivacy: any = {
  private: '/images/icons/ic-private.png',
  public: '/images/icons/ic-public.png',
}
export const QuestionTable = ({ className = '', style, searching }: Props) => {
  const { t } = useTranslation()
  const [session] = useSession()
  const { getGrade } = useGrade()
  const user: any = session.user
  const router = useRouter()
  const { dataPlan, listPlans } = useCurrentUserPackage();
  const pageSize = 10
  const { search, setSearch } = useContext<QuestionContextType>(QuestionContext)
  const { globalModal } = useContext(WrapperContext)
  const isOperator = userInRight([USER_ROLES.Operator], session)
  const isSystem = !router.query.m
  const isCreatable = isSystem === isOperator
  const isShowPrivacy = user?.is_admin == 1 && !router.query.m
  const isAllowAccept = dataPlan?.code === PACKAGES.INTERNATIONAL.CODE || isOperator

  const dataDescription = studentPricings.teacher.find(item => item.code === PACKAGES.INTERNATIONAL.CODE)?.descriptions || []
  const dataPackage = listPlans.find((item: any) => item.code === PACKAGES.INTERNATIONAL.CODE)

  const { questions, totalRecords, isLoading, mutateQuestions } = useQuestions(
    search.page_index,
    pageSize,
    search,
    router.query.m === 'mine' || router.query.m === 'teacher',
  )

  const isShowCreateBy = router.query.m === 'teacher' && user.is_admin === 1

  const isSearchEmpty = useMemo(() => {
    if (!questions || questions.length === 0) {
      return searching
    }
    return true
  }, [searching])

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    })
  }

  const onPreviewQuestion = async (question: QuestionDataType) => {
    const response = await fetch(`/api/questions/review-question/${question.id}`)
    const data = await response.json()
    const dataQuestion: any[] = []
    let count = 0

    if (data) {
      if (data?.totalQuestion === data.listContent.length) {
        data.listContent.forEach((item: any) => {
          item.id = data.id
          item.activityType = data.activityType
          item.audioInstruction = data.audioInstruction
          item.audioScript = data.audioScript
          item.imageInstruction = data.imageInstruction
          item.index = count
          item.instruction = data.instruction
          item.group = 1
          // item.point = parent.totalPoint / arr[index].list.length
          item.questionInstruction = data.questionInstruction
          dataQuestion.push(item)
          count++
        })
      } else {
        Array.from(Array(data.totalQuestion), () => {
          const cloneItem = data.listContent[0]
          cloneItem.id = data.id
          if (cloneItem?.questionsGroup && cloneItem.questionsGroup.length > 0) {
            cloneItem.questionsGroup.forEach((item: any, i: number) => {
              cloneItem.activityType = item.activityType
              cloneItem.audioInstruction = data.audioInstruction
              cloneItem.audioScript = data.audioScript
              cloneItem.imageInstruction = data.imageInstruction
              cloneItem.index = count
              cloneItem.instruction = data.instruction
              cloneItem.group = data.totalQuestion
              cloneItem.questionInstruction = data.questionInstruction
              if (i === 0) return
            })
            dataQuestion.push(cloneItem)
            count++
          } else {
            //group question from many items
            cloneItem.audioInstruction = data.audioInstruction
            cloneItem.audioScript = data.audioScript
            cloneItem.imageInstruction = data.imageInstruction
            cloneItem.index = count
            cloneItem.instruction = data.instruction
            cloneItem.group = data.totalQuestion
            cloneItem.questionInstruction = data.questionInstruction
            dataQuestion.push(cloneItem)
            count++
          }
        })
      }
    }
    globalModal.setState({
      id: 'preview-question-modal',
      questionType: question.question_type,
      data: dataQuestion[0],
    })
  }
  const onDelete = async (question: QuestionDataType) => {
    globalModal.setState({
      id: 'confirm-modal',
      type: 'delete-question',
      content: {
        closeText: t('common')['back'],
        submitText: t('question-page')['deleted-question'],
        onSubmit: async () => {
          const res = await fetch(`${paths.api_questions}/${question.id}`, {
            method: 'DELETE',
          })
          const json = await res.json()
          if (json) {
            toaster.push(
              <Message showIcon type="success">
                {t("question-page")["deleted-question-true-mess"]}
              </Message>,
            )
            if (questions.length > 1) {
              mutateQuestions()
            } else {
              mutateQuestions()
              search.page_index > 0 && setSearch({ ...search, page_index: search.page_index - 1 })
            }
          }
        },
      },
    })
  }
  const updatePrivacy = async (id: any, privacyBefore: any) => {
    const formData: any = { id: id }
    let privacy = null
    if (privacyBefore != 1) {
      // public
      formData.privacy = 1 //private
      privacy = 1
    }
    const response = await api.post(`${paths.api_question_update_privacy}`, formData)
    if (response.status == 200) {
      const key = toaster.push(<AlertNoti type={'success'} message={t('common-get-noti')['update-privacy-succeed']} />, {
        placement: 'topEnd',
      })
      setTimeout(() => toaster.remove(key), 2000)
      if (questions.length >= 1) {
        mutateQuestions()
      } else {
        search.page_index > 0 && setSearch({ ...search, page_index: search.page_index - 1 })
      }
    } else {
      const key = toaster.push(<AlertNoti type={'error'} message={t('common-get-noti')['update-privacy-failed']} />, {
        placement: 'topEnd',
      })
      setTimeout(() => toaster.remove(key), 2000)
    }
  }
  return (
    <div>
      <div
        className={`m-question-table ${className}`}
        style={{
          ...style,
          ...(!isLoading && questions && questions.length > 0
            ? {}
            : { backgroundColor: 'transparent', boxShadow: 'unset' }),
        }}
      >
        {isLoading && <Loader backdrop content="loading..." vertical />}
        <div className="__table" style={{ opacity: isLoading ? 0 : 1 }}>
          {!isLoading &&
            (questions && questions.length > 0 ? (
              <table>
                <thead>
                  <tr>
                    <th>{isSystem ? 'ID' : t('STT')}</th>
                    <th style={isOperator ? {} : { display: 'none' }}>{t('common')['question-name']}</th>
                    <th>{t('common')['grade']}</th>
                    <th>{t('common')['skill']}</th>
                    <th>{t('common')['question-type']}</th>
                    <th>{t('common')['level']}</th>
                    <th>{t('question-update-container')['question-description']}</th>
                    <th>{t('common')['question']}</th>
                    <th style={isShowCreateBy ? {} : { display: 'none' }}>{t('table')['created-by']}</th>
                    <th style={isShowPrivacy ? {} : { display: 'none' }}>{t('common')['private']}</th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                  {questions &&
                    questions?.map((question, index) => {
                      const isEditable = isOperator || question.created_by === user.id
                      const question_description = (
                        question?.parent_question_description || question?.question_description
                      )?.replaceAll(/\%s\%/g, ' ')
                      const question_text = question?.question_text
                        ?.replaceAll(/\%(u|b|i|s)\%/g, '')
                        .replaceAll(/(#|\*)/g, '')
                        .replaceAll(/\[\]/g, '')
                        const lockInternationalQuestion = question.grade === 'IE' && question.created_by === user.id || isAllowAccept || question.grade !== 'IE'
                      return (
                        <tr key={`question_${index}`} className={`${lockInternationalQuestion ? "" : "__not-allow"}`}>
                          <td>{isSystem ? question.id :index + 1}</td>
                          <td title={question?.name} style={isOperator ? {} : { display: 'none' }}>
                            <div className="__elipsis">
                              {question?.name || '---'}
                            </div>
                          </td>
                          <td title={getGrade.length > 0 ? getGrade.find((m) => m.code === question.grade)?.display : ''}>
                            <div className="__elipsis">
                              {(getGrade.length > 0 && getGrade.find((m) => m.code === question.grade)?.display) ||
                                '---'}
                            </div>
                          </td>
                          <td title={SKILLS_SELECTIONS.find((m: any) => m.code === question.skills?.toString())?.display}>
                            <div className="__elipsis" style={{ lineBreak: 'anywhere' }}>
                              {SKILLS_SELECTIONS.find((m: any) => m.code === question.skills?.toString())?.display || '---'}
                            </div>
                          </td>
                          <td title={TASK_SELECTIONS.find((m) => m.code === question.question_type)?.display}>
                            <div className="__elipsis">
                              {TASK_SELECTIONS.find((m) => m.code === question.question_type)?.display || '---'}
                            </div>
                          </td>
                          <td title={t('contant')[question.level]}>
                            <div className="__elipsis">{t('contant')[question.level] || '---'}</div>
                          </td>
                          <td title={question_description}>
                            <div className="__elipsis">{question_description || '---'}</div>
                          </td>
                          <td title={question_text}>
                            <div className="__elipsis">{question_text && question_text.length > 0 ? question_text : '---'}</div>
                          </td>
                          <td style={isShowCreateBy ? {} : { display: 'none' }} title={question.user_name}>
                            <div className="__elipsis">{question.user_name || '---'}</div>
                          </td>
                          <td style={isShowPrivacy ? {} : { display: 'none' }}>
                            <img
                              height={18}
                              alt="img"
                              src={question?.privacy == 1 ? imagePrivacy.private : imagePrivacy.public}
                            ></img>
                          </td>
                          <td>
                            <div className="__action-group">
                              {lockInternationalQuestion ? (
                                <Whisper
                                    placement="auto"
                                    trigger="click"
                                    container={() => document.querySelector('.m-question-table')}
                                    speaker={
                                      <Popover>
                                        <div className="table-action-popover">
                                          {isEditable && (
                                              <Link
                                                  href={`questions/${question.id}${
                                                      router.query.m ? `?m=${router.query.m}` : ''
                                                  }`}
                                              >
                                                <a
                                                    className="__item"
                                                    onClick={(e) => {
                                                      e.preventDefault()
                                                      router.push({
                                                        pathname: '/questions/[questionSlug]',
                                                        query: {
                                                          ...router.query,
                                                          questionSlug: question.id,
                                                        },
                                                      })
                                                    }}
                                                >
                                                  <FaPencilAlt />
                                                  <span>{t('common')['edit']}</span>
                                                </a>
                                              </Link>
                                          )}
                                          <Link
                                              href={`questions/${question.id}?mode=view${
                                                  router.query.m ? `&m=${router.query.m}` : ''
                                              }`}
                                          >
                                            <a
                                                className="__item"
                                                onClick={(e) => {
                                                  e.preventDefault()
                                                  router.push({
                                                    pathname: '/questions/[questionSlug]',
                                                    query: {
                                                      ...router.query,
                                                      questionSlug: question.id,
                                                      mode: 'view',
                                                    },
                                                  })
                                                }}
                                            >
                                              <FaEye />
                                              <span>{t('common')['view']}</span>
                                            </a>
                                          </Link>
                                          <div className="__item" onClick={onPreviewQuestion.bind(null, question)}>
                                            <FaEye />
                                            <span>{t('table-action-popover')['format-preview']}</span>
                                          </div>
                                          {isShowPrivacy && (
                                              <div
                                                  className="__item"
                                                  onClick={() => updatePrivacy(question.id, question.privacy)}
                                              >
                                                <FaExchangeAlt />
                                                <span style={{ lineHeight: '1.2' }}>{t('table-action-popover')['change-privacy']}</span>
                                              </div>
                                          )}
                                          {isEditable && (
                                              <div className="__item" onClick={onDelete.bind(null, question)}>
                                                <FaTrashAlt />
                                                <span>{t('common')['delete']}</span>
                                              </div>
                                          )}
                                        </div>
                                      </Popover>
                                    }
                                >
                                  <Button className="__action-btn">
                                    <GoKebabVertical color="#6262BC" />
                                  </Button>
                                </Whisper>
                              ) : (
                                <Whisper
                                    container={() => document.querySelector('.t-wrapper__main')}
                                    trigger="hover"
                                    placement="auto"
                                    preventOverflow={true}
                                    enterable
                                    speaker={
                                      <Popover className={styles.poperverModal} arrow={false}>
                                        <PopoverUpgradeContent
                                            dataPackage={dataDescription}
                                            price={dataPackage?.exchange_price}
                                            itemId={dataPackage?.item_id}
                                        />
                                      </Popover>}
                                >
                                  <div className="__icon">
                                    {dataWithPackage[PACKAGES.INTERNATIONAL.CODE].icon}
                                  </div>
                                </Whisper>
                              ) }
                            </div>
                          </td>
                        </tr>
                      )
                    })}
                  {questions && questions.length === 0 && (
                    <tr>
                      <td colSpan={8}>{t('no-data')}</td>
                    </tr>
                  )}
                </tbody>
              </table>
            ) : (
              <div
                className="o-question-drawer__empty"
                style={{ minHeight: '50vh', paddingTop: 32, paddingBottom: 32 }}
              >
                <img
                  className="__banner"
                  src={
                    isSearchEmpty
                      ? '/images/collections/clt-emty-result.png'
                      : '/images/collections/clt-emty-question.png'
                  }
                  alt="banner"
                />
                <p className="__description">
                  {isSearchEmpty ? (
                    <>{t('no-field-data')['search-empty']}</>
                  ) : (
                    <>
                      {t('question-page')['no-question']}
                      <br />
                      {t('unit-test')['pls-create']}
                    </>
                  )}
                </p>
                {isCreatable && !search?.question_text && !searching &&(
                  <Button
                    className="__submit"
                    onClick={() =>
                      router.push({
                        pathname: '/questions/[questionSlug]',
                        query: { ...router.query, questionSlug: -1 },
                      })
                    }
                  >
                    {t('common')['create']}
                  </Button>
                )}
              </div>
            ))}
        </div>
      </div>
      {questions?.length > 0 && (
        <StickyFooter>
          <div className="__pagination">
            <Pagination
              prev
              next
              ellipsis
              size="md"
              total={totalRecords}
              maxButtons={10}
              limit={pageSize}
              activePage={search.page_index + 1}
              onChangePage={(page) => {
                scrollToTop()
                setSearch({ ...search, page_index: page - 1 })
              }}
            />
          </div>
        </StickyFooter>
      )}
    </div>
  )
}
