import { useEffect, useState } from 'react'

import { TrueFalseImage } from 'components/atoms/truefalseImage'

import { DefaultPropsType } from '../../../interfaces/types'
import { convertStrToHtml } from "@/utils/string";
import useTranslation from '@/hooks/useTranslation';
interface PropsType extends DefaultPropsType {
  data: any
}

export const TemplateTrueFalseType3 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  const {t} = useTranslation()
  const correctArr = data.correct_answers.split('#')
  const answerArr = data.answers.split('#')

  console.log('answerArr', answerArr)
  const [examArr, setExamArr] = useState([])
  const [answerList, setAnswerList] = useState([])

  useEffect(() => {
    const answerList = answerArr.map((item: string, index: number) => {
      if (item.startsWith('*')) {
        item = item.replace('*', '')
      }
      return item
    })
    setAnswerList(answerList)
    if (data.isExam) {
      setExamArr(data.isExam.split('#'))
    } else {
      const arr: any[] = []
      for (let i = 0; i < answerArr.length; i++) {
        if (answerArr[i].startsWith('*')) {
          arr[i] = 'false'
        } else {
          arr[i] = 'true'
        }
      }
      setExamArr(arr)
    }
  }, [])

  return (
    <div className={`m-template-true-false-type3 ${className}`} style={style}>
      <p className={`m-template-true-false-type3__instruction`}>
        {data?.question_description || ''}
      </p>
      <div className={`m-template-true-false-type3__question-container`}>
        <div className={`m-template-true-false-type3__question-container__img`}>
          <TrueFalseImage
            data={
              data?.image.startsWith('blob')
                ? data.image
                : `/upload/${data?.image || ''}`
            }
          />
        </div>
        <div
          className="m-template-true-false-type3__question-container__answer">
          <table>
            <thead>
              <tr>
                <th>
                  <span>{t('common')['question']}/ {t('common')['example']}</span>
                </th>
                <th>
                  <span>{t('create-question')['list-questions-examples']}</span>
                </th>
                <th>
                  <span>True/ False</span>
                </th>
              </tr>
            </thead>
            <tbody>
              {answerList.map((answer: string, answerIndex: number) => (
                  <tr key={answerIndex + 1}>
                    <td 
                      align="center" 
                      className={`${examArr[answerIndex] === 'false' ? 'exam_answer' : 'answer'}`}
                    >
                      {examArr[answerIndex] === 'false' ? t('common')['example'] : t('common')['question']}
                    </td>
                    <td>
                      <span
                          dangerouslySetInnerHTML={{
                            __html: convertStrToHtml(answer),
                          }}
                      ></span>
                    </td>
                    <td align="center">
                      <img
                        src={
                          correctArr[answerIndex] === 'T'
                            ? '/images/icons/ic-true.png'
                            : '/images/icons/ic-false.png'
                        }
                        alt={answer}
                      />
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  )
}
