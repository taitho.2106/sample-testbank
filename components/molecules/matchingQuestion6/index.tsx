import useTranslation from '@/hooks/useTranslation'
import { CSSProperties } from 'react'
import sortExampleQuestionMG6 from 'utils/sortExampleQuestion/template/MG6'

type PropsType = {
  data?: any
  className?: string
  style?: CSSProperties
}

function MatchingQuestion6( {className = '', data, style }: PropsType){
  const {t} = useTranslation()
  const dataSort = sortExampleQuestionMG6(data)
  const correctArray: string[] = dataSort?.correct_answers?.split('#') ?? []
  const columnData: string[][] = dataSort?.answers?.split('#').map((item: string) => item.split('*')) ?? [[],[],[]]
  const maxLength = Math.max(columnData[0].length, columnData[1].length, correctArray.length)
  const handleURL = (value: string) => {
    return value.startsWith('blob') ? value : `${location.origin}/upload/${value}`
  }
  if(columnData.length <1 ) return <></>
  return (
    <div className={`matching-question6 ${className}`} style={style}>
      <div className="matching-question6__des">
        {data?.question_description || ''}
      </div>
      <div className="matching-question6__content">
        <div className={`right-data ${!data?.image?"__100":""}`}>
          <div className="row-data-header">
            <div className="__left-column">
              {t('common')['question']}
              <br />
              /{t('common')['example']}
            </div>
            <div className="__center-column">
              <div>
                <span>{t('create-question')['list-questions-examples']}</span>
              </div>
              <div>
                <span>{t('create-question')['answer-key']}</span>
              </div>
            </div>
            <div className="__right-column">{t('create-question')['position']}</div>
          </div>
          
          {Array.from(Array(maxLength), (item, index) => 
           {
            const isExample =columnData[2] && columnData[2][index]?.includes('ex') 
            return  <div key={`mg6_preview_${index}`} 
            className={`row-data ${ isExample ? 'row-example' : ''}`}>
          <div className="__left-column">
            {isExample
            ? <span className="title-example">{t('common')['example']}</span>
            : columnData[0][index] 
              ? <span className="title-question">{t('common')['question']}</span> : '' }
          </div>
          <div className="__center-column">
            <div className={`__left-item ${ !columnData[0][index] ? '__empty' : '' }`}>
              { columnData[0][index]
              ? <><div className="__left-item__image">
                <img src={handleURL(columnData[0][index])} alt="image-item" />
              </div>
              <div className="__left-item__linked">
                <div></div>
                <div></div>
              </div></> : ''}
            </div>
            <div className="__right-item">
              <div className="__right-item__image">
                <img src={handleURL(columnData[1][index])} alt="image-item" />
              </div>
              <div className="__right-item__linked">
                <div></div>
                <div></div>
              </div>
            </div>
          </div>
          <div className="__right-column">
            <div className="__index">
              <span>{correctArray.includes(columnData[1][index]) 
                ? correctArray.findIndex((value) => value.includes(columnData[1][index])) + 1 : 'N/A'}</span>
            </div>
          </div>
        </div>
           }
          )}
          
        </div>
      </div>
    </div>
  )
}

export { MatchingQuestion6 }
