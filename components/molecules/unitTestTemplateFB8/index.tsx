import { FBParagraphStyle } from 'components/atoms/fillintheblankParagraphStyle'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateFB8 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  // console.log('go here', data)
  const generalListImage = data?.image?.split('*') ?? []
  const lengthCA = data?.correct_answers?.split('#')?.length
  const listArrImageCA = generalListImage[1]?.split('#').slice(0,lengthCA)
  ?.map((item:string)=> item?.split('|'))
  ?.reduce((a:any,b:any)=>[...a,...b],[])

  const listArrImageSG = generalListImage[1]?.split('#').slice(lengthCA)?? []

  const listCorrectAnswers = (data?.correct_answers ?? '').split('#')
  .map((item:string)=> item?.split('%/%'))
  .reduce((a:any,b:any)=> [...a,...b],[])

  const listSuggestion = (data?.answers ?? '')?.split('#')

  const swapArr = (arr:any[]) => {
    const arrTemp:any[] = []
    for(const i in arr){
      if(arr[i]?.startsWith('*')){
          arrTemp.splice(0,0,arr[i])
      }else{
        arrTemp.push(arr[i])
      }
    }
    return arrTemp
  }

  
  const listAnswers = swapArr([...listCorrectAnswers, ...listSuggestion])
  const indexEx =  [...listCorrectAnswers, ...listSuggestion].findIndex((item:string) => item.startsWith('*'))
  
  const swapListImg = (arr:any[]) => {
    const arrTemp:any[] = []
    for(const i in arr){
      if(parseInt(i) == indexEx){
        arrTemp.splice(0,0,arr[i])
      }else{
        arrTemp.push(arr[i])
      }
    }
    return arrTemp
  }

  const listImage = swapListImg([...listArrImageCA,...listArrImageSG])

  return (
    <div className={`m-unittest-fb8 ${className}`} style={style}>
      <div className="m-unittest-fb8__turorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.question_description || ''}
        />
      </div>

      <div className="m-unittest-fb8__listquestion">
        <div className="m-unittest-fb8__left">
          <div className='image__banner'>
              <img 
                src={generalListImage[0].startsWith('blob') ? generalListImage[0] : `/upload/${generalListImage[0]}`}
                alt='image-banner'
                width={150}
              />
              <span>{data.audio_script}</span>
          </div>
          <div className={`list_image_item ${listImage?.length === 4 ? 'column_2' : 'column_3'}`}>
              {listImage?.length > 0 && listImage?.map((item:string, index:number)=>(
                <div key={`item-image-${index}`} className={`item_image`}>
                  <>
                    <img 
                      src={item.startsWith('blob') ? item : `/upload/${item}`}
                      alt='item-image'
                      width={80}
                    />
                    <span 
                      className={`${listAnswers[index].startsWith('*') ? 'example_text' : ''}`}
                      >
                        {listAnswers[index].replaceAll('*','')}
                    </span>
                  </>
                </div>
              ))}
          </div>
        </div>
        <div className="m-unittest-fb8__right">
          <div className="m-unittest-multi-choice-1__answers">
            <FBParagraphStyle
              answer={data?.correct_answers || ''}
              data={data?.question_text || ''}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
