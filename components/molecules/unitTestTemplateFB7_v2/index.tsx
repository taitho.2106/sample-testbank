import { FBAudio } from 'components/atoms/fillintheblankAudio'
import { FBParagraph } from 'components/atoms/fillintheblankParagraph'
import { FBParagraphStyle } from 'components/atoms/fillintheblankParagraphStyle'
import { MultiChoicesImage } from 'components/atoms/multichoiceImage'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateFB7_v2 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  return (
    <div className={`m-unittest-fb4 ${className}`} style={style}>
      <div className="m-unittest-fb4__turorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.question_description || ''}
        />
      </div>

      <div className="m-unittest-fb4__listquestion">
        <div
          className="m-unittest-fb4__left"
          style={{ height: 'unset', padding: '10px' }}
        >
          <div className="m-unittest-fb4__image">
            <MultiChoicesImage
              data={
                data?.image.startsWith('blob')
                  ? data.image
                  : `/upload/${data?.image || ''}`
              }
            />
          </div>
        </div>
        <div className="m-unittest-fb4__right">
          <div className="m-unittest-multi-choice-1__answers">
            <FBParagraphStyle
              answer={data?.correct_answers || ''}
              data={data?.question_text || ''}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
