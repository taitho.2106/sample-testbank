import { ContentField } from 'components/atoms/contentField'
import { CSSProperties, useRef, useState } from 'react'
import { Button, Modal } from 'rsuite'

type PropsType = {
  style?: CSSProperties
  isError?: boolean
  url: string
  onChangeFile?: (files: FileList) => void
}

export default function ImageInput({
  style,
  isError,
  url,
  onChangeFile,
}: PropsType) {
  const [imageUrl, setImageUrl] = useState(url ?? '')
  const fileRef = useRef(null)

  return (
    <div
      style={style}
      className={`box-image-input ${imageUrl !== '' ? 'has-data' : ''} ${
        isError ? 'error' : ''
      }`}
      onClick={() => fileRef.current.dispatchEvent(new MouseEvent('click'))}
    >
      {imageUrl !== '' ? (
        <img
          className="image-upload"
          src={imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`}
        />
      ) : (
        <>
          <img className="icon" src="/images/icons/ic-image.png" />
          Hình ảnh
        </>
      )}
      <input
        ref={fileRef}
        type="file"
        style={{ display: 'none' }}
        accept=".png,.jpeg,.jpg"
        onChange={(e) => {
          if (fileRef.current.files.length > 0) {
            setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
            onChangeFile && onChangeFile(e.target.files)
          }
        }}
      />
    </div>
  )
}
