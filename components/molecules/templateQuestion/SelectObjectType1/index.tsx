import React, { useEffect, useState, useRef } from 'react'

import { Toggle } from 'rsuite'

import { SelectField } from 'components/atoms/selectField'
import { SplitImage } from 'components/atoms/SplitImage/Index'
import { TextAreaField } from 'components/atoms/textAreaField'
import { ContentItemPosType, PosType, QuestionPropsType } from 'interfaces/types'
import coordinates from 'utils/coordinates'
import Guid from 'utils/guid'

import AudioInput from '../AudioInput'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation'

type DataImageType = {
  data: string
  deleted: boolean
  key: string
  name?: string
  isExample?: boolean
  isSelected?: boolean
}
type ImageCurrentInfoType = {
  src: string
  width: number
  height: number
}
const sizeWordExport = 680
export default function SelectObjectType1({
  question,
  register,
  setValue,
  isAudio = false,
  isImage = false,
  isReading = false,
  viewMode = false,
  errors = {},
}: QuestionPropsType) {
  const {t} = useTranslation()
  const getFirstDataImage = (correct_answers: string) => {
    const correct_answersArr = correct_answers?.split('#') || []
    const tempArr: DataImageType[] = correct_answersArr.map((item: any, i: number) => {
      const key = Guid.newGuid()
      const itemCorrectArr = item?.split('*') || []
      return {
        name: itemCorrectArr[2] === 'True' ? 'T' : 'F',
        key: key,
        isExample: itemCorrectArr[0].startsWith('ex|'),
        data: '',
        deleted: false,
        isSelected: itemCorrectArr[2] === 'True' ? true : false,
      }
    })
    return tempArr
  }
  const splitImageRef = useRef(null)
  const imageRef = useRef(null)
  const [jsonData, setJsonData] = useState(null)
  const df = getFirstDataImage(question?.correct_answers)
  const listImageRef = useRef(df)
  const [listImage, setListImage] = useState<DataImageType[]>(listImageRef.current)
  const timeOutImageWord = useRef(null)
  const [indexSelected, setIndexSelected] = useState(-1)
  const imgCurrentInfoRef = useRef<ImageCurrentInfoType>({
    src: '',
    width: 0,
    height: 0,
  })
  useEffect(() => {
    const getData = async () => {
      try {
        if (!question?.answers) return
        const res = await fetch(`/${question.answers}`, {
          method: 'GET',
        })
        const json = await res.json()
        if (json && Array.isArray(json)) {
          const imageArr = question?.image.split('*') || []
          if (json[0]?.listPos) {
            //old:
            const positionArr = json.map((item: any, i: number) => {
              const key = listImageRef.current[i]?.key
              return { ...item, key: key }
            })
            setJsonData({
              imageUrl: imageArr.length > 0 ? `/upload/${imageArr[0]}` : '',
              positionArr,
            })
          } else {
            //new
            const positionArr = handleStringPositionToLoad(json)
            setJsonData({
              imageUrl: imageArr.length > 0 ? `/upload/${imageArr[0]}` : '',
              positionArr,
            })
          }
        }
      } catch (ex) {
        console.log('ex======-----------------------', ex)
      }
    }
    getData()
  }, [])
  useEffect(() => {
    if (splitImageRef.current) {
      const listPos = splitImageRef?.current?.getImagePostions()
      const errorImageArr: any[] = []
      let imageData: any[] = []
      let countExample = 0
      if (listPos && listPos.length > 0) {
        imageData = listImage.map((m, index) => {
          if (m.isExample) countExample += 1
          const imageErr = !m.data
          const exampleErr = m.isExample && countExample > 1
          const isErr = imageErr || exampleErr

          errorImageArr.push({
            imageErr,
            exampleErr,
            isErr,
            key: m.key,
          })
          let coorMaxMin = {
            minX: 0,
            minY: 0,
            maxX: 0,
            maxY: 0,
          }
          if (listPos[index]?.listPos) {
            coorMaxMin = coordinates.getMaxMinXY(listPos[index].listPos)
          }
          return { ...m, ...coorMaxMin }
        })
      }
      setValue('list_answer_item', JSON.stringify(imageData))
      setValue('list_answer_item_error', errorImageArr)
      const listAnswer = listImage.filter((item) => !item.isExample)
      const listDropdown = listAnswer.map((item) => item.isSelected)
      console.log('listDropdown====', listDropdown)
      setValue('checkSelected', listDropdown.join('#'))
    }
    let countQuestion = 0
    let countExample = 0
    listImage.forEach((m) => {
      if (m.isExample) {
        countExample += 1
      } else {
        countQuestion += 1
      }
    })
    if (countExample > 1 || countQuestion == 0) {
      setValue('passData', false)
    } else {
      setValue('passData', true)
    }

    if (timeOutImageWord.current) {
      clearTimeout(timeOutImageWord.current)
    }
    timeOutImageWord.current = setTimeout(() => {
      drawImageExportQuestion()
    }, 150)

    //check style btn add new answer
    setTimeout(() => {
      const scrollElement = document.getElementById('class-image-list-content-id')
      if (scrollElement) {
        if (scrollElement.scrollWidth > scrollElement.offsetWidth) {
          const addBtnElement = document.getElementById('add-new-answer-id')
          if (addBtnElement) {
            if (!addBtnElement.classList.contains('scroll')) {
              addBtnElement.classList.add('scroll')
            }
          }
        } else {
          const addBtnElement = document.getElementById('add-new-answer-id')
          if (addBtnElement) {
            if (addBtnElement.classList.contains('scroll')) {
              addBtnElement.classList.remove('scroll')
            }
          }
        }
      }
    }, 10)
  }, [listImage])
  useEffect(() => {
    register('passData', {
      validate: (value: string) => {
        return value
      },
    })
    register('image_question_base64', {
      required: t('question-validation-msg')['image_question_base64'],
    })
    register('image_question_export_base64', {
      required: t('question-validation-msg')['image_question_export_base64'],
    })
    register('list_answer_item', {
      validate: (value) => {
        if (!value) return t('question-validation-msg')['canvas-answers']
        try {
          const arr = JSON.parse(value)
          if (Array.isArray(arr) && arr.length == 0) return t('question-validation-msg')['canvas-answers']
        } catch {}
        return true
      },
      required: t('question-validation-msg')['canvas-answers'],
    })
    register('list_answer_item_error', {
      validate: (value) => {
        if (!value) return true
        // const value = JSON.parse(value)
        const indexErr = value.findIndex((m: any) => m.isErr)
        if (indexErr == -1) {
          return true
        } else {
          return JSON.stringify(value)
        }
      },
    })
    register('checkSelected', {
      validate: (value: string) => {
        console.log('value===', value)
        if (!value) return true
        const listValue = (value ?? '').split('#')
        const indexT = listValue.findIndex((item: string) => item === 'true')
        return indexT === -1 ? t(t('question-validation-msg')['true-answer-num'], ['1']) : true
      },
    })
    register('audio_file', {
      validate: {
        acceptedFormats: async (file) => {
          // check format if has file
          if (!file) {
            return true
          }
          if (file.size == 0) {
            return t('question-validation-msg')['audio-link-error']
          }
          if (!['audio/mp3', 'audio/mpeg'].includes(file?.type)) {
            return t('question-validation-msg')['audio-type']
          }
          if (!(await browserFile.checkFileExist(file))) {
            return t('question-validation-msg')['audio-link-error']
          }
          return true
        },
      },
    })
    register('audio', {
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['audio-max-length'], ['10000']),
      },
      required: t('question-validation-msg')['audio-required'],
      value: question?.audio,
    })
    register('audio_script', {
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['audio-script-max-length'], ['10000']),
      },
      value: question?.audio_script,
    })
  }, [])
  const [defaultValue, setDefaultValue] = useState('T')
  const onToggleExample = (key: string) => {
    const tempArr: DataImageType[] = listImageRef.current.map((m) => {
      if (m.key == key) {
        if (m.isSelected) {
          setDefaultValue('T')
        } else {
          setDefaultValue('F')
        }
        return { ...m, isExample: !m.isExample, isSelected: m.isSelected }
      } else {
        return m
      }
    })
    listImageRef.current = tempArr
    setListImage([...tempArr])
  }

  const onChangeAudioScript = (data: string) => {
    setValue('audio_script', data)
  }
  const onChangeAudioFile = (files: FileList) => {
    setValue('audio_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('audio', urlAudio, {
      shouldValidate: true,
    })
  }
  const itemImageErrArr: any[] = errors['list_answer_item_error']?.message
    ? JSON.parse(errors['list_answer_item_error']?.message)
    : []
  const exampleErr = listImage.filter((m) => m.isExample).length > 1
  const answerErr = listImage.filter((m) => !m.isExample).length == 0
  const listAnswerErr = errors['list_answer_item']
  const imageErr = errors['image_question_base64']

  const drawImageExportQuestion = () => {
    setValue('image_question_export_base64', '')
    if(!imgCurrentInfoRef.current || !imgCurrentInfoRef.current.src ){
      console.debug("noimage---")
      return
    }
    const container = document.getElementById('content-draw-image-export-id') as HTMLElement
    if (!container || !splitImageRef?.current) return
    const canvas = document.getElementById('canvas-export-id') as HTMLCanvasElement
    canvas.setAttribute('height', `${container.offsetHeight}`)
    canvas.setAttribute('width', `${container.offsetWidth}`)
    const imageTarget = document.getElementById('image-export-question-id') as HTMLCanvasElement

    const indexExample = listImage.findIndex((m) => m?.isExample)
    let ratio = 1
    if (imageTarget.offsetWidth < imgCurrentInfoRef.current.width) {
      ratio = imageTarget.offsetWidth / imgCurrentInfoRef.current.width
    }
    function point(x: number, y: number, ctx: any) {
      ctx.beginPath()
      ctx.moveTo(x, y)
      ctx.lineJoin = 'round'
      ctx.fillStyle = '#ffffff'
      ctx.lineTo(x + 1, y + 1)
      ctx.stroke()
    }

    const ctxCanvasDraw: any = canvas.getContext('2d')

    ctxCanvasDraw.drawImage(
      imageTarget,
      canvas.width / 2 - imageTarget.offsetWidth / 2,
      canvas.height / 2 - imageTarget.offsetHeight / 2,
      imageTarget.offsetWidth,
      imageTarget.offsetHeight,
    )

    if (indexExample != -1) {
      const listPos = splitImageRef?.current?.getImagePostions()
      const posExample = listPos[indexExample]
      const leftImage = imageTarget.offsetLeft
      const topImage = imageTarget.offsetTop
      if (posExample && posExample.listPos) {
        const positionRoundLine = coordinates.getRoundLine(posExample.listPos)
        for (let i = 0; i < positionRoundLine.length; i++) {
          point(positionRoundLine[i].x * ratio + leftImage, positionRoundLine[i].y * ratio + topImage, ctxCanvasDraw)
        }
      }
    }
    const imageData = canvas.toDataURL('image/png', 1)
    setValue('image_question_export_base64', imageData)
  }

  const onChangeItem = (dataImage: any) => {
    if (dataImage) {
      const tempArr: DataImageType[] = dataImage.map((m: DataImageType, index: number) => {
        const item = listImageRef.current.find((n) => n.key == m?.key)

        return {
          key: m.key,
          data: m.data,
          name: item?.name || 'T',
          isExample: item?.isExample,
          isSelected: item?.isSelected ?? true,
        }
      })
      setDefaultValue('T')
      listImageRef.current = tempArr
      setListImage(tempArr)
      const listPos: ContentItemPosType[] = splitImageRef?.current?.getImagePostions()
      const listDataPos = handleStringPositionToSave(listPos)
      setValue('list_image_item_position', JSON.stringify(listDataPos))
    } else {
      setListImage([])
      setValue('list_image_item_position', JSON.stringify([]))
    }
  }
  const handleStringPositionToSave = (listPos: ContentItemPosType[]) => {
    try {
      const listDataPos: any[] = []
      listPos.forEach((item) => {
        const dataColor = item.colorData
        const dataColorStr: any = {}
        const listTemp: any[] = []
        Object.keys(dataColor).forEach(function (key) {
          dataColorStr[key] = dataColor[key].join('*')
          listTemp.push(`${key}|${dataColor[key].join('*')}`)
        })
        listDataPos.push({ strPos: listTemp.join('#'), key: item.key })
      })
      return listDataPos
    } catch {
      return [] as any[]
    }
  }
  const handleStringPositionToLoad = (json: any[]) => {
    try {
      const positionArr = json.map((item: any, i: number) => {
        const itemsWithColor = item.strPos.split('#') || []
        const key = listImageRef.current[i]?.key
        const listPos: PosType[] = []
        itemsWithColor.forEach((m: string) => {
          const items = m.split('|')
          const color = items[0]
          items[1].split('*').forEach((m) => {
            const xy = m.split(':')
            const poss: PosType = { x: parseInt(xy[0]), y: parseInt(xy[1]), c: `#${color}` }
            listPos.push(poss)
          })
        })
        return { listPos: listPos, key: key }
      })
      return positionArr
    } catch {
      return []
    }
  }
  const onChangeImageData = (data: any) => {
    const boxImage = document.querySelector('.box-image-handle-answer') as HTMLElement
    if (data.src) {
      if (boxImage.classList.contains('no-image')) {
        boxImage.classList.remove('no-image')
      }
    } else {
      if (!boxImage.classList.contains('no-image')) {
        boxImage.classList.add('no-image')
      }
      if (imageRef.current) imageRef.current.value = null
    }
    const imgExportElement = document.getElementById('image-export-question-id') as HTMLImageElement
    if (imgExportElement) {
      imgExportElement.src = data.src
    }
    imgCurrentInfoRef.current = data
    setValue('image_question_base64', data.src)
  }

  return (
    <div className={`m-select-object-template-1`}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          flex: '1',
          width: '100%',
        }}
      >
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            label={t('create-question')['question-description']}
            setValue={setValue}
            defaultValue={question?.question_description}
            register={register('question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['description-max-length'], ['10000']),
              },
            })}
            style={{ height: '4rem', marginLeft: '0', width: '100%' }}
            className={`form-input-stretch ${errors['question_description'] ? 'error' : ''}`}
            viewMode={viewMode}
          />

          <div className={`box-image-handle-answer ${jsonData && jsonData.imageUrl ? '' : 'no-image'}`}>
            {
              <SplitImage
                className={`${imageErr ? 'error' : ''}`}
                ref={splitImageRef}
                jsonData={jsonData}
                viewMode={viewMode}
                onChangeItemIndexSelected={(indexSelected) => {
                  setIndexSelected(indexSelected)
                }}
                onChangeItem={(dataImage) => onChangeItem(dataImage)}
                onChangeImageData={(data) => onChangeImageData(data)}
                onClickChangeImage={() => {
                  imageRef.current.dispatchEvent(new MouseEvent('click'))
                }}
              ></SplitImage>
            }
            <input
              ref={imageRef}
              type="file"
              style={{ display: 'none' }}
              accept=".png,.jpeg,.jpg"
              onChange={(e) => {
                if (imageRef.current.files.length > 0) {
                  const file = imageRef.current.files[0]
                  if (['image/jpeg', 'image/png'].includes(file?.type)) {
                    const url = URL.createObjectURL(imageRef.current.files[0])
                    splitImageRef.current && splitImageRef.current.changeImageUrl(url)
                  }
                }
              }}
            />
            <div className="text-user-guide">
              <img alt="" src="/images/icons/ic-info-blue.png"></img>
              <span>{t('create-question')['chosen-image']}</span>
            </div>
            <div className="class-image-list" style={!listImage || listImage.length == 0 ? { height: '13.2rem' } : {}}>
              {listImage && (
                <div
                  id="class-image-list-content-id"
                  className="class-image-list__content"
                  style={listImage.length == 10 ? { maxWidth: '100%' } : {}}
                >
                  {listImage.map((item: DataImageType, indexItem: number) => {
                    const itemErr = itemImageErrArr.find((m) => m.key == item.key && m.isErr)

                    return (
                      item && (
                        <div
                          data-id={`key_${item.key}`}
                          className={`class-image-list__content__card-image ${viewMode ? 'view-mode' : ''}`}
                          key={item.key}
                        >
                          <div
                            className={`box-image ${indexItem == indexSelected ? 'selected' : ''} ${
                              item.isExample ? '--example' : ''
                            }`}
                          >
                            {!viewMode && (
                              <img
                                className="image-delete-icon"
                                src="/images/icons/ic-remove-opacity.png"
                                alt=""
                                onClick={() => {
                                  event.preventDefault()
                                  event.stopPropagation()
                                  splitImageRef.current && splitImageRef.current.removeItem(indexItem)
                                }}
                              />
                            )}

                            <div
                              className={`__content-image ${itemErr?.imageErr ? 'error' : ''}`}
                              onClick={() => {
                                splitImageRef.current && splitImageRef.current.selectItem(indexItem)
                              }}
                            >
                              <img
                                alt="img"
                                className={`${item.data ? '' : 'img-default'}`}
                                src={item.data || '/images/icons/ic-image.png'}
                              />
                            </div>
                            {item.isExample ? (
                              <div className="toggle-example-text">
                                <div>
                                  <span>{t('create-question')['true']}</span>
                                </div>
                              </div>
                            ) : (
                              <>
                                <SelectField
                                  className={`${item.isSelected ? 'true' : 'false'} ${
                                    errors['checkSelected']?.message ? 'error' : ''
                                  }`}
                                  defaultValue={item.name ?? defaultValue}
                                  disabled={viewMode ? true : false}
                                  data={[
                                    {
                                      code: 'T',
                                      display: t('create-question')['true'],
                                    },
                                    {
                                      code: 'F',
                                      display: t('create-question')['false'],
                                    },
                                  ]}
                                  onChange={(e) => {
                                    if (e === 'T') {
                                      listImageRef.current[indexItem].name = 'T'
                                      listImageRef.current[indexItem].isSelected = true
                                      setListImage([...listImageRef.current])
                                    } else {
                                      listImageRef.current[indexItem].name = 'F'
                                      listImageRef.current[indexItem].isSelected = false
                                      setListImage([...listImageRef.current])
                                    }
                                  }}
                                />
                              </>
                            )}
                          </div>
                          <div className={`__content-toggle ${exampleErr && item.isExample ? 'error' : ''}`}>
                            <Toggle
                              size="md"
                              className="__switcher"
                              disabled={viewMode}
                              checked={!item.isExample}
                              checkedChildren={t('common')['question']}
                              unCheckedChildren={t('common')['example']}
                              onChange={(e) => onToggleExample(item.key)}
                            />
                          </div>
                        </div>
                      )
                    )
                  })}
                </div>
              )}
              {listImage.length < 10 && (
                <div
                  className={`add-new-answer`}
                  id="add-new-answer-id"
                  style={!listImage || listImage.length == 0 ? { borderRadius: '0.8rem', alignItems: 'center' } : {}}
                >
                  <div
                    className={`add-new-answer__btn ${
                      (!listImage || listImage.length == 0) && listAnswerErr ? 'error' : ''
                    }`}
                    onClick={() => {
                      imgCurrentInfoRef.current &&
                        imgCurrentInfoRef.current.src &&
                        splitImageRef.current &&
                        splitImageRef.current.addNewItem()
                      setTimeout(() => {
                        const scrollElement = document.getElementById('class-image-list-content-id')
                        if (scrollElement) {
                          if (scrollElement.scrollWidth > scrollElement.offsetWidth) {
                            scrollElement.scrollTo(scrollElement.scrollWidth, 0)
                            const addBtnElement = document.getElementById('add-new-answer-id')
                            if (addBtnElement) {
                              if (!addBtnElement.classList.contains('scroll')) {
                                addBtnElement.classList.add('scroll')
                              }
                            }
                          } else {
                            const addBtnElement = document.getElementById('add-new-answer-id')
                            if (addBtnElement) {
                              if (addBtnElement.classList.contains('scroll')) {
                                addBtnElement.classList.remove('scroll')
                              }
                            }
                          }
                        }
                      }, 50)
                    }}
                  >
                    <img alt="" src="/images/icons/ic-plus-sub.png"></img>
                    <span>{t('create-question')['add-answer']}</span>
                  </div>
                </div>
              )}
            </div>
            <div className="text-user-guide">
              <img alt="" src="/images/icons/ic-info-blue.png"></img>
              <span>
                {' '}{t('create-question')['chosen-answer-des']}
              </span>
            </div>
          </div>
          <AudioInput
            url={question?.audio}
            script={question?.audio_script}
            isError={errors['audio'] || errors['audio_file']}
            onChangeFile={onChangeAudioFile}
            onChangeScript={onChangeAudioScript}
            style={{ height: '200px', margin: 0 }}
            viewMode={viewMode}
          />
        </div>
        {(answerErr || exampleErr || errors['checkSelected']?.message) && (
          <div className="form-error-message">
            {answerErr && <span>{`* ${t(t('question-validation-msg')['add-answer-with-num'],['1'])}`}</span>}
            {exampleErr && <span>{`* ${t(t('question-validation-msg')['example-allow-total'],['1'])}`}</span>}
            {errors['checkSelected']?.message && <span>{`* ${errors['checkSelected']?.message}`}</span>}
          </div>
        )}
        <div style={{ width: `${sizeWordExport}px`, position: 'fixed', zIndex: '-11', visibility: 'hidden' }}>
          <div
            id="content-draw-image-export-id"
            style={{
              width: '100%',
              padding: '1rem 8rem',
              display: 'flex',
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <img
              id="image-export-question-id"
              alt=""
              style={{
                maxWidth: `${sizeWordExport}px`,
                visibility: 'hidden',
                maxHeight: '338px',
              }}
              onLoad={(e) => {
                const container = document.getElementById('content-draw-image-export-id') as HTMLElement
                const canvas = document.getElementById('canvas-export-id') as HTMLCanvasElement
                canvas.setAttribute('height', `${container.offsetHeight}`)
                canvas.setAttribute('width', `${container.offsetWidth}`)
              }}
            ></img>
          </div>
          <canvas id="canvas-export-id" style={{ position: 'absolute', top: 0, left: 0 }}></canvas>
        </div>
      </div>
    </div>
  )
}
