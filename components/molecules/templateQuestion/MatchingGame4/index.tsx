import { useEffect, useRef, useState } from 'react'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import MatchingGameTable from './table'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation'

export default function MatchingGame4({
  question,
  register,
  setValue,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const [imgUrl, setImgUrl] = useState(question?.image || '')
  useEffect(() => {
    register('image_file', {
      validate: {
        acceptedFormats: async(file) => {
          // check format if has file
          if (!file) {
            return true
          }
          if(file.size ==0){
            return t('question-validation-msg')['image-deleted']
          }
          if(!['image/jpeg', 'image/png'].includes(file?.type)){
            return t('question-validation-msg')['image-file']
          }
          if(!await browserFile.checkFileExist(file)){
            return t('question-validation-msg')['image-deleted']
          }
          return true;
        },
      },
    })
    register('image', {
      maxLength: {
        value: 2000,
        message: t(t('question-validation-msg')['image-max-length'], ['2000']),
      },
      // required: 'Hình ảnh không được để trống',
      value: question?.image,
    })
    register('answers', {
      validate: (value: string) => {
        
        if (!value) {
          setValue('errDataQuestion', t(t('question-validation-msg')['input-required'], ['1']))
        } else {
          setValue('errDataQuestion', "")
          const leftRight = value.split('#')
          if(!leftRight[0]){
            setValue('errDataQuestion',t(t('question-validation-msg')['input-required'], ['1']))
          }
          if (leftRight[2]) {
            const checkModeArr = leftRight[2].split('*')||[]
            if(checkModeArr.length>0){
              if (checkModeArr.filter((m: string) => m == 'ex').length > 1) {
                setValue('passDataExample',false)
              }else{
                setValue('passDataExample',true)
              }
              if (checkModeArr.filter((m: string) => m == '').length == 0) {
                  setValue('passDataQuestion',false)
              }else{
                setValue('passDataQuestion',true)
              }
            }
          }
        }
        const errs = (value ?? '#').split('#').map((m, indexSplit) =>
          m
            .split('*')
            .map((g, gIndex) => {
              if (g === '' && indexSplit < 2) return gIndex
              return null
            })
            .filter((m) => m !== null),
        )
        return errs.some((m) => m.length > 0)
          ? errs.map((m) => m.join()).join('#')
          : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['total-question-answer'], ['10000']),
      },
      value: question?.answers ?? '***#***',
    })
    register('correct_answers', {
      required: t('question-validation-msg')['canvas-answers'],
      // maxLength: {
      //   value: 10000,
      //   message: 'Tổng ký tự của các đáp án không được quá 10000 ký tự',
      // },
      validate: (item: string) => { return item.length <=10000},
      value: question?.correct_answers,
    })
    register('passDataQuestion', {
      validate: (value: string) => {
        return value
      },
    })
    register('passDataExample', {
      validate: (value: string) => {
        return value
      },
    })
    register('audio_script', {
      maxLength: {
        value: 100,
        message: t(t('question-validation-msg')['image-caption-max-length'], ['100']),
      },
      value: question?.audio_script,
    })
  }, [])
  const onChange = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers)
  }

  const onChangeFile = (files: FileList) => {
    if (files && files[0]) {
      setValue('image_file', files[0])
      const urlImage = URL.createObjectURL(files[0])
      setValue('image', urlImage)
      setImgUrl(urlImage)
    } else {
      setValue('image_file', null)
      setValue('image', '')
      setImgUrl('')
    }
  }
  // console.log("err--", errors)
  return (
    <div className="m-matching-game-4" style={{ display: 'flex' }}>
      <div className="m-matching-game-4__left">
        <div className="form-input-stretch">
          <ImageInputMG4
            url={imgUrl}
            isError={errors['image'] || errors['image_file']}
            onChangeFile={onChangeFile}
            viewMode={viewMode}
          />
        </div>
        <TextAreaField
          label={t('create-question')['image-caption']}
          defaultValue={question?.audio_script}
          setValue={setValue}
          register={register('audio_script', {
            maxLength: {
              value: 100,
              message: t(t('question-validation-msg')['image-caption-max-length'], ['100']),
            },
            value: question?.audio_script,
          })}
          className={`form-input-stretch ${
            errors['audio_script'] ? 'error' : ''
          }`}
          style={{ height: '4rem' }}
          viewMode={viewMode}
          specialChar={['#','*']}
        />
      </div>
      <div className="m-matching-game-4__right">
        <TextAreaField
          label={t('create-question')['question-description']}
          setValue={setValue}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          style={{ height: '4rem' }}
          viewMode={viewMode}
          specialChar={['#','*']}
        />
        <MatchingGameTable
          className={`form-input-stretch`}
          errExample={errors['passDataExample']}
          answerStr={question?.answers ?? '***#***'}
          correctStr={question?.correct_answers}
          onChange={onChange}
          errorStr={errors['answers']?.message ?? ''}
          errorCorrectStr={errors['correct_answers']?'error':""}
          register={register}
          setValue={setValue}
          errors={errors}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}
type PropsType = {
  labelDefault?: string
  isError?: any
  url: string
  onChangeFile?: (file: FileList) => void
  viewMode?: boolean
}
function ImageInputMG4({
  url,
  onChangeFile,
  isError,
  viewMode = false,
}: PropsType) {
  const {t} = useTranslation()
  const [imageUrl, setImageUrl] = useState(url ?? '')
  const fileRef = useRef(null)
  const src = imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`
  return (
    <div
      className={`box-image-input-MG4 ${imageUrl !== '' ? 'has-data' : ''} ${
        isError ? 'error' : ''
      }`}
    >
      <div className="image-section">
        <div className="image-input-origin">
          {src && url ? (
            <>
              <img
                className="image-upload"
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                src={src}
                alt="image-upload"
                style={{ cursor: 'pointer' }}
              />
              {!viewMode && src && url && (
                <img
                  className="image-delete-icon"
                  src="/images/icons/ic-remove-opacity.png"
                  alt=""
                  onClick={() => {
                    fileRef.current.value = ''
                    event.preventDefault()
                    event.stopPropagation()
                    onChangeFile(null)
                  }}
                />
              )}
            </>
          ) : (
            <>
              <div
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  cursor: 'pointer',
                }}
              >
                <img
                  className="icon"
                  src="/images/icons/ic-image.png"
                  alt="delete-img-icon"
                />
                {t('create-question')['image']}
              </div>
            </>
          )}
          <input
            ref={fileRef}
            type="file"
            style={{ display: 'none' }}
            accept=".png,.jpeg,.jpg"
            onChange={(e) => {
              if (fileRef.current.files.length > 0) {
                setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                onChangeFile && onChangeFile(e.target.files)
              }
            }}
          />
        </div>
      </div>
    </div>
  )
}
