import React, { useEffect, useRef, useState } from 'react'

import { SelectPicker, Toggle } from 'rsuite'
import { MyCustomCSS } from "@/interfaces/types";
import { convertHtmlToStr, convertStrToHtml, onPaste, onType } from "@/utils/string";
import ContentEditable from "react-contenteditable";
import useTranslation from '@/hooks/useTranslation';

type PropType = {
  answerStr: string
  correctStr: string
  styleBold?: boolean
  className?: string
  errorStr?: string
  errorCorrectStr?:string
  onChange: (data: any) => void
  register?:any
  setValue?:any
  errors?:any,
  viewMode?:boolean,
  errExample?:any
}

export default function MatchingGameTable({
  answerStr,
  correctStr,
  className = '',
  errorStr = '',
  onChange,
  register,
  setValue,
  errors,
  viewMode,
  errExample,
  errorCorrectStr
}: PropType) {
  const {t} = useTranslation()
  const groupKey = useRef(Math.random()).current
  const correctList = correctStr?.split('#') ?? []
  const leftRight = answerStr?.split('#') ?? []
  const modeQuestion = leftRight[2]?leftRight[2].split("*"):Array.from(Array(leftRight[0].split("*").length), () => '');
  if(modeQuestion.length < leftRight[0].split("*").length){
    modeQuestion.push(...Array.from(Array(leftRight[0].split("*").length - modeQuestion.length), () => ''))
  }
  const [modeQuestionsState, setModeQuestionState ] = useState(modeQuestion)
  const [strErrExample, setStrErrExample ] = useState("")
  const [strErrQuestion, setStrErrQuestion ] = useState("")
  const [left, setLeft] = useState<Answer[]>(
    leftRight[0]?.split('*')?.map((m, mIndex) => ({
      id: `${groupKey}_${mIndex}`,
      text: m,
    })) ?? [],
  )
  const [right, setRight] = useState<Answer[]>(
    leftRight[1]?.split('*')?.map((m, mIndex) => ({
      id: `${groupKey}_${mIndex}`,
      text: m,
      order: correctList.findIndex((g) => g === m) + 1,
    })) ?? [],
  )

  const isInit = useRef(true)

  useEffect(() => {
     setValue('order',right.map((item:any)=>item.order).join('#'))
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        const rigtTemp = right.filter((m) => m.order > 0)
        onChange({
          answers: `${left.map((m) => (m.text ?? '').trim()).join('*')}#${right.map((m) => (m.text ?? '').trim()).join('*')}${modeQuestionsState && modeQuestionsState.filter(m=>m == "ex").length>0?`#${modeQuestionsState.join("*")}`:""}`,
          correct_answers: rigtTemp
            .sort((a, b) => a.order - b.order)
            .map((m) => (m.text ?? '').trim())
            .join('#')
        })
      }
    }
  }, [left, right,modeQuestionsState])
  useEffect(() => {
    if(modeQuestionsState.filter(m=> m == "ex").length >1){
      setStrErrExample(t(t('question-validation-msg')['example-allow-total'], ['1']))
    }else{
      setStrErrExample("")
    }
    if(modeQuestionsState.filter(m=> m == "").length ==0){
      setStrErrQuestion(t(t('question-validation-msg')['question-fb-required'], ['1']))
    }else{
      setStrErrQuestion("")
    }
  }, [modeQuestionsState])
  useEffect(()=>{
    register('order', {
      validate: (value: string) => {
        const lengthLeft = left.length;
        const arrLeft = Array.from(Array(lengthLeft),(item,i)=>"err");
        const err = value.split('#').map((item:any, i:number)=>{
          if(item >0 && item<=lengthLeft){
            arrLeft[parseInt(item)-1] = ""
          }
          if(i>=lengthLeft || item > 0) return true
          return false
        })
        if(arrLeft.find(m=> m!="") || err.find(m=>!m)){
          return `${arrLeft.join()}#${err.map(m=> m?"":"err").join(",")}`
        }else{
          return true
        }
      },
      value:right.map((item:any)=>item.order).join('#')
    })
  })
  const onAddAnswer = (index: number) => {
    if (index === 0) {
      left.push({ id: `${groupKey}_0`, text: '' })
      modeQuestionsState.push("")
      setLeft([...left])
      setModeQuestionState([...modeQuestionsState])
    } else {
      right.push({ id: `${groupKey}_0`, text: '', order: 0 })
      setRight([...right])
    }

    const posIndex = index === 0 ? left?.length : right?.length
    setTimeout(() => {
      const el = document.querySelector(
        `table:nth-child(${index + 1}) tbody tr:nth-child(${
          posIndex ?? 1
        }) td input`,
      ) as HTMLInputElement
      el?.focus()
    }, 0)
  }

  const onChangeText = (index: number, answerIndex: number, event: any) => {
    const el: HTMLDivElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    if (index === 0) {
      left[answerIndex].text = text.replace(/(\*|\#)/g, '')
      setLeft([...left])
    } else {
      right[answerIndex].text = text.replace(/(\*|\#)/g, '')
      setRight([...right])
    }
  }
  const onBlurText = (index: number, answerIndex: number, event: any) => {
    const el: HTMLDivElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    if (index === 0) {
      left[answerIndex].text = text.replace(/(\*|\#)/g, '').trim()
      // setLeft([...left])
    } else {
      right[answerIndex].text = text.replace(/(\*|\#)/g, '').trim()
      // setRight([...right])
    }
  }

  const onChangeOrder = (value: any, answerIndex: any) => {
    if (value != NaN) {
      const rightTemp = [...right];
      const item = rightTemp.find(m=> m.order== value);
      if(item){
        item.order = 0;
      }
      rightTemp[answerIndex].order = value
      setRight([...rightTemp])
      setValue('order',rightTemp.filter((m:any) => m.order))
    }
  }

  const onDeleteAnswer = (index: number, answerIndex: number) => {
    if (index === 0) {
      left.splice(answerIndex, 1)
      if(modeQuestionsState&& modeQuestionsState.length>answerIndex ){
        modeQuestionsState.splice(answerIndex, 1)
      }
      const rightClone = right.map((item:any)=> {
        const newOrder = item.order == answerIndex+1?0:item.order>left.length?0:item.order
        return {
          ...item,
          order:newOrder,
        }
      })
      setModeQuestionState([...modeQuestionsState])
      setLeft([...left])
      setRight([...rightClone])
    } else {
      
      right.splice(answerIndex, 1)
      setRight([...right])
    }
  }
  const onToggleExample = (index:number)=>{
    //only 1 example
    if(modeQuestionsState[index]== "ex"){
      modeQuestionsState[index]=""
    }else{
      modeQuestionsState[index] = "ex"
    }
    setModeQuestionState([...modeQuestionsState])
  }
  const listError = errorStr.split('#')
  const leftError = listError[0] ? listError[0].split(',') : []
  const rightError = listError[1] ? listError[1].split(',') : []

  const errOrderMessage = errors['order']?.message?.split("#")||[];
  const errOrderLeft = errOrderMessage[0]?.split(",")||[]
  const errOrderRight = errOrderMessage[1]?.split(",")||[]
  let indexQ = -1
  let indexExample = -1
  const onTypeClick = (type : "Bold" | "Italic" | "Underline", tablePosition: number) => {
    onType({
      classElement: "div-input",
      groupKey,
      type,
      tablePosition,
      action: (position: number, index, value) => {
        if (position === 0) {
          left[index].text = value.replace(/(\*|\#)/g, "")
          setLeft([...left])
        } else {
          right[index].text = value.replace(/(\*|\#)/g, "")
          setRight([...right])
        }
      }
    })
  }
  return (
    <div>
 <div className={`box-table ${className}`} style={{ position: 'relative' }}>
      <div
        style={{
          width: 'calc(50% - 0.5rem)',
          marginRight: "1rem",
          border: leftError.length > 0 ? '1px solid #dd3a09' : 'none',
          borderRadius:'8px'
        }}
      >
        <table className={`question-table left`}>
          <thead>
            <tr>
              <th align="left"
                dangerouslySetInnerHTML={{
                  __html: t('create-question')['select-question-example']
              }}
              ></th>
              <th align="left">{t('create-question')['question-example']}</th>
              <th>
                <div style={{ display: 'flex' }}>
                  <div
                      className="icon-mask icon-action-style"
                      onMouseDown={() => onTypeClick("Bold", 0)}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-bold.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                  <div
                      className="icon-mask icon-action-style"
                      onMouseDown={() => onTypeClick("Italic", 0)}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-italic.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                  <div
                      className="icon-mask icon-action-style"
                      onMouseDown={() => onTypeClick("Underline", 0)}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-underline.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            {left?.map((answer: Answer, answerIndex: number) => {
              const showErrTr = !leftError.includes(answerIndex.toString()) && errOrderLeft[answerIndex];
              const isExample = modeQuestionsState && modeQuestionsState[answerIndex]=="ex";
              if(isExample){
                indexExample +=1;
              }else{
                indexQ+=1;
              }
              return (
                <tr key={`left_${answer.id}`} className={`${showErrTr? 'error' : ''} ${isExample?"tr-example":""}`} >
                  <td>
                  <div
                className={`__content-toggle ${errExample && isExample && modeQuestionsState.filter(m=>m =="ex").length!=1?"toggle-error":""}`}
              >
                <Toggle
                  size="md"
                  className="__switcher"
                  disabled={viewMode}
                  checkedChildren={t('common')['question']}
                  unCheckedChildren={t('common')['example']}
                  onChange={()=>onToggleExample(answerIndex)}
                  defaultChecked={!isExample}
                />
              </div>
                  </td>
                  <td
                    className={`input-placeholder ${
                      leftError.includes(answerIndex.toString()) ? 'error' : ''
                    }`}
                  >
                    <ContentEditable
                        aria-colindex={0}
                        id={`${groupKey}_${answerIndex}`}
                        spellCheck={false}
                        className={`div-input ${groupKey}`}
                        html={convertStrToHtml(answer.text)}
                        contentEditable="true"
                        onChange={(e) => onChangeText(0, answerIndex, e)}
                        onBlur={(e) => onBlurText(0, answerIndex, e)}
                        onPaste={onPaste}
                        onKeyDown={(event)=>{
                          if (event.key === 'Enter') {
                            event.preventDefault()
                          }
                        }}
                    />
                    <span className="placeholder">{isExample?`${t('common')['example']} ${indexExample+1}`:`${t('common')['question']} ${indexQ+1}`}</span>
                  </td>
                  <td align="right">
                    <img alt=''
                      className="ic-action"
                      src="/images/icons/ic-trash.png"
                      onClick={onDeleteAnswer.bind(null, 0, answerIndex)}
                    />
                  </td>
                </tr>
              )
            })}
            {!viewMode && (
            <tr className="action tr-action-add">
              <td align="left" colSpan={3}>
                <label
                  className={`add-new-answer ${left.length == 0 ? 'error' : ''}`}
                  onClick={onAddAnswer.bind(null, 0)}
                >
                  <img alt=''
                    src="/images/icons/ic-plus-sub.png"
                    width={17}
                    height={17}
                  />{' '}
                  {t('create-question')['add-question']}
                </label>
              </td>
            </tr>
            )}
          </tbody>
        </table>
      </div>
      <div
      style={{
        width: 'calc(50% - 1rem)',
        border: rightError.length > 0  || errorCorrectStr? '1px solid #dd3a09' : 'none',
        borderRadius:'8px'
      }}
      >
      <table className={`question-table right`}>
        <thead>
          <tr>
            <th align="left">
              <span>{t('create-question')['answer-key']}</span>
              <div style={{ display: 'flex' }}>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={() => onTypeClick("Bold", 1)}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                ></div>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={() => onTypeClick("Italic", 1)}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-italic.png)',
                      } as MyCustomCSS
                    }
                ></div>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={() => onTypeClick("Underline", 1)}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-underline.png)',
                      } as MyCustomCSS
                    }
                ></div>
              </div>
            </th>
            <th align="left">{t('create-question')['position']}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {right?.map((answer: Answer, answerIndex: number) => {
            const pickerData = left.map((item: Answer, index:number) => ({
              "label": `${index + 1}`,
              "value": index + 1,
            }))
            pickerData.unshift({
              "label": '_',
              "value": 0,
            })
            const isExample = modeQuestionsState && modeQuestionsState[answer.order - 1]=="ex";
            return (
            <tr key={answer.id} className={`${isExample?"tr-example":""}`}>
              <td
                className={`input-placeholder ${
                  rightError.includes(answerIndex.toString()) ? 'error' : ''
                }`}
              >
                <ContentEditable
                    aria-colindex={1}
                    id={`${groupKey}_${answerIndex}`}
                    spellCheck={false}
                    className={`div-input ${groupKey}`}
                    html={convertStrToHtml(answer.text)}
                    contentEditable="true"
                    onChange={(e) => onChangeText(1, answerIndex, e)}
                    onBlur={(e) => onBlurText(1, answerIndex, e)}
                    onPaste={onPaste}
                    onKeyDown={(event)=>{
                      if (event.key === 'Enter') {
                        event.preventDefault()
                      }
                    }}
                />
                <span className="placeholder">{`${t('create-question')['answer']} ${
                  answerIndex + 1
                }`}</span>
              </td>
              <td
                align='center'
                className={`input-placeholder ${errOrderRight[answerIndex] ? 'error' : ''} ${answer.order==0?"order_default":""}`}
                style={{padding:0}}
              >
                <SelectPicker
                key={`${new Date().getMilliseconds()}`}
                  onChange={(value, event) => onChangeOrder(value, answerIndex)}
                  data={pickerData}
                  appearance="default"
                  placeholder=" "
                  value={answer.order}
                  searchable={false}
                  cleanable={false}
                  size="xs"
                />
              </td>
              <td>
                <img alt=''
                  className="ic-action"
                  src="/images/icons/ic-trash.png"
                  onClick={onDeleteAnswer.bind(null, 1, answerIndex)}
                />
              </td>
            </tr>
          )})}
          {!viewMode && (
          <tr className="action tr-action-add">
            <td align="left" colSpan={3}>
              <label
                className={`add-new-answer ${right.length === 0 ? 'error' : ''}`}
                onClick={onAddAnswer.bind(null, 1)}
              >
                <img alt=''
                  src="/images/icons/ic-plus-sub.png"
                  width={17}
                  height={17}
                />{' '}
                {t('create-question')['add-answer']}
              </label>
            </td>
          </tr>
          )}
        </tbody>
      </table>
      </div>
    </div>
    {(strErrExample || strErrQuestion) &&
    <div className='form-error-message'>
      {strErrExample &&<span>{`* ${strErrExample}`}</span> }
      {strErrQuestion &&<span>{`* ${strErrQuestion}`}</span> }
      </div>}
    </div>
  )
}

type Answer = {
  id: string
  text: string
  order?: number
}
