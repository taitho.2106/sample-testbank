import { useEffect, useState, useRef } from 'react'

import { Button, Toggle } from 'rsuite'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import AudioInput from '../AudioInput'
import QuestionMC10 from './question'
import browserFile from 'lib/browserFile'
import useTranslation from "@/hooks/useTranslation";

export default function MultiChoiceGroupMC10({
    question,
    register,
    setValue,
    errors = {},
    viewMode,
}: QuestionPropsType) {
    const { t } = useTranslation()
    const listImage = question?.image?.split('#') ?? []
    const listAnswer = question?.answers?.split('#') ?? []
    const listCorrect = question?.correct_answers?.split('#') ?? []
    const questionList = (question?.question_text || '')?.split('#') ?? []
    const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
        questionList.map((questionStr: string, mIndex: number) => {
            return {
                key: new Date().getTime().toString() + mIndex,
                imageStr: listImage[mIndex] || '',
                answerStr: listAnswer[mIndex] || '***',
                correctStr: listCorrect[mIndex] || null,
                questionText: questionStr?.replace('*', ''),
                isQuestion: !questionStr?.startsWith('*'),
                toggleErr: false,
            }
        }),
    )

    const answerGroupRef = useRef(answerGroup)
    const imageFiles = useRef(answerGroup.map((m) => null))

    useEffect(() => {
        register('passData', {
            validate: (value: string) => {
                return value
            },
        })
        register('image_files', {
            validate: async (files: any) => {
                if (!files) {
                  return true
                }
                let fileErr: number[] = []
                let mIndex = -1
                for await (const file of files) {
                  mIndex += 1
                  if (file !== null) {
                    if (!['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)) {
                      fileErr.push(mIndex)
                      continue
                    } else if (!(await browserFile.checkFileExist(file))) {
                      fileErr.push(mIndex)
                      continue
                    } else {
                      fileErr.push(null)
                      continue
                    }
                  }
                  fileErr.push(null)
                }
                fileErr= fileErr.filter(m=>m!=null)
                return fileErr.length > 0 ? fileErr.join() : true
              },
        })
        register('image', {
            maxLength: {
                value: 2000,
                message: t(t('question-validation-msg')['image-max-length'], ['2000']),
            },
            value: question?.image,
            validate: (value: string) => {
                const errs = (value ?? '')
                    .split('#')
                    .map((m, mIndex) => {
                        if (m === '') return mIndex
                        return null
                    })
                    .filter((m) => m !== null)
                return errs.length > 0 ? errs.join() : true
            },
        })
        register('answers', {
            validate: (value: string) => {
                const errs = (value ?? '').split('#').map((m) =>
                m
                    .split('*')
                    .map((g, gIndex) => {
                    if (g === '') return gIndex
                    return null
                    })
                    .filter((m) => m !== null),
                )
                const re = errs.some((m) => m.length > 0)
                ? errs.map((m) => m.join()).join('#')
                : true
                return re
            },
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['answers-mc-max-length'], ['10000']),
            },
            value: question?.answers ?? '***',
        })
        register('correct_answers', {
            validate: (value: string) => {
                const errs = (value ?? '')
                .split('#')
                .map((m, gIndex) => {
                    if (m === '') return gIndex
                    return null
                })
                .filter((m) => m !== null)
                const re = errs.length > 0 ? errs.join() : true
                return re
            },
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
            },
            value: question?.correct_answers,
        })
        register('question_text', {
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['question-example-max-length'], ['10000']),
            },
            value: question?.question_text,
        })
        register('audio_file',{
            validate: {
                acceptedFormats: async(file) => {
                    if (!file) {
                        return true
                      }
                      if(file.size ==0){
                        return t('question-validation-msg')['audio-link-error']
                      }
                      if(!['audio/mp3','audio/mpeg'].includes(file?.type)){
                        return t('question-validation-msg')['audio-type']
                      }
                      if(!await browserFile.checkFileExist(file)){
                        return t('question-validation-msg')['audio-link-error']
                      }
                      return true;
                },
            }
        })
        register('audio', {
            maxLength: {
                value: 2000,
                message: t(t('question-validation-msg')['audio-max-length'],['2000']),
            },
            required: t('question-validation-msg')['audio-required'],
            value: question?.audio,
        })
        register('audio_script', {
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
            },
            value: question?.audio_script,
        })
    }, [])
    const onAddQuestion = () => {
        const newQuestion: AnswerGroup = {
            key: new Date().getTime().toString() + (answerGroup?.length ?? 0),
            imageStr: '',
            answerStr: '***',
            correctStr: null,
            isQuestion: true,
            questionText: '',
            toggleErr: false,
        }
        let data = [newQuestion]
        if (answerGroup) {
            data = [...answerGroup, newQuestion]
        }
        if (data.filter((m) => !m.isQuestion).length == 1) {
            if (data.length > 1) {
                data = data.map((m) => {
                    return { ...m, toggleErr: false }
                })
            } else {
                data = data.map((m) => {
                    return { ...m, toggleErr: true }
                })
            }
        }
        setAnswerGroup([...data])
        imageFiles.current.push(null)
    }

    useEffect(() => {
        answerGroupRef.current = answerGroup
        const example = []
        const ans = []
        const arrIndexEx = []
        const arrIndexAns = []
        for (let i = 0; i < answerGroup.length; i++) {
            if (answerGroup[i].isQuestion) {
                ans.push(answerGroup[i])
                arrIndexAns.push(i)
            } else {
                example.push(answerGroup[i])
                arrIndexEx.push(i)
            }
        }

        if (imageFiles?.current) {
            setValue('image_files', imageFiles?.current)
        }

        if (answerGroup) {
            setValue('answers', answerGroup.map((m) => m.answerStr).join('#'))
            setValue(
                'correct_answers',
                answerGroup.map((m) => m.correctStr).join('#'),
            )
            setValue('image', answerGroup.map((m) => m.imageStr).join('#'))
            setValue(
                'question_text',
                answerGroup
                .map((m) => {
                    if (m.isQuestion) {
                        let question = m.questionText
                        while (question?.startsWith('*')) {
                            question = question.substring(1)
                        }
                        return question
                    } else {
                    //example
                        let question = m.questionText
                        while (question?.startsWith('*')) {
                            question = question.substring(1)
                        }
                        return '*' + question ?? ""
                    }
                })
                .join('#'),
            )
        }
        renderError()
    }, [answerGroup])

    const onChangeImage = (index: number, files: FileList) => {
        if (files && files[0]) {
            const urlImage = URL.createObjectURL(files[0])
            answerGroup[index].imageStr = urlImage
            imageFiles.current[index] = files[0]
            setAnswerGroup([...answerGroup])
        } else {
            answerGroup[index].imageStr = ''
            imageFiles.current[index] = null
            setAnswerGroup([...answerGroup])
        }
    }

    const onDeleteAnswer = (index: number) => {
        let data: any[] = answerGroup.filter(
            (m: any, mIndex: number) => mIndex !== index,
        )
        imageFiles?.current.splice(index, 1)
        //only 1 example
        if (data.filter((m) => !m.isQuestion).length == 1) {
            if (data.length > 1) {
                data = data.map((m) => {
                    return { ...m, toggleErr: false }
                })
            } else {
                data = data.map((m) => {
                    return { ...m, toggleErr: true }
                })
            }
        }
        setAnswerGroup([...data])
    }

    const handleToggle = (mIndex: number) => {
        let data = [...answerGroup]
        data[mIndex].toggleErr = false
        data[mIndex].isQuestion = !data[mIndex].isQuestion

        //only 1 example
        if (
            !data[mIndex].isQuestion &&
            data.filter((m) => !m.isQuestion).length > 1
        ) {
            data[mIndex].toggleErr = true
        }

        if (data.filter((m) => !m.isQuestion).length == 1) {
            data = data.map((m) => {
                return { ...m, toggleErr: false }
            })
        }
        if (data.length == 1 && !data[mIndex].isQuestion) {
            data[mIndex].toggleErr = true
        }
        setAnswerGroup(data)
    }

    const [errorExample, setErrorExample] = useState('')
    const [errorQuestion, setErrorQuestion] = useState('')
    const [errorImageFile, setErrorImageFile] = useState('')
    const listImageError = (errors['image']?.message ?? '').split(',')
    const listImageFileError = (errors['image_files']?.message ?? '').split(',')
    const renderError = () => {
        let passData = true
        if (answerGroup.filter((m) => !m.isQuestion).length > 1) {
            passData = false
            setErrorExample(t(t('question-validation-msg')['example-allow-total'], ['1']))
        } else {
            setErrorExample('')
        }
        if (answerGroup.filter((m) => m.isQuestion).length == 0) {
            passData = false
            setErrorQuestion(t(t('question-validation-msg')['question-fb-required'], ['1']))
        } else {
            setErrorQuestion('')
        }
        setValue('passData', passData)
    }
    useEffect(() => {
        if (errors['image_files']) {
            setErrorImageFile(t('question-validation-msg')['list-images'])
        } else {
            setErrorImageFile('')
        }
    }, errors['image_files'])

    let indexE = -1
    let indexA = -1
    const onMCChange = (dataV: any, index: number) => {
        answerGroupRef.current[index].answerStr = dataV.answers
        answerGroupRef.current[index].correctStr = dataV.correctStrs
        answerGroupRef.current[index].questionText = dataV.questionText
        setValue(
            'answers',
            answerGroupRef.current.map((m) => m.answerStr).join('#'),
        )
        setValue(
            'correct_answers',
            answerGroupRef.current.map((m) => m.correctStr).join('#'),
        )
        setValue(
            'question_text',
            answerGroupRef.current
                .map((m) => (m.isQuestion ? m.questionText : `*${m.questionText}`))
                .join('#'),
        )
        setAnswerGroup([...answerGroupRef.current])
    }

    const onChangeAudioScript = (data: any) => {
        setValue('audio_script', data)
    }
    const onChangeFile = (files: FileList) => {
        if (files.length > 0) {
            setValue('audio_file', files[0])
            const urlAudio = URL.createObjectURL(files[0])
            setValue('audio', urlAudio)
        }
    }

    const arrErrAnswerMessage = errors['answers']?.message?.split('#') || []
    const arrErrCorrectMessage =
        errors['correct_answers']?.message?.split(',') || []
    const arrErrQuestionTextMessage =
        errors['question_text']?.message?.split(',') || []

    return (
        <div 
            className="m-multi-choice-mc10" 
            style={{ display: 'flex', width:'100%' }}
        >
            <AudioInput
                url={question?.audio}
                script={question?.audio_script}
                isError={errors['audio'] || errors['audio_file']}
                onChangeFile={onChangeFile}
                onChangeScript={onChangeAudioScript}
                style={{ 
                    height: '280px',
                    width:'280px',
                    // height: '468px',
                }}
                viewMode={viewMode}
            />
            <div style={{ 
                flex: 3, 
                marginBottom:'2rem' 
                }}
            >
                <TextAreaField
                    label={t('create-question')['question-description']}
                    setValue={setValue}
                    defaultValue={question?.parent_question_description}
                    register={register('parent_question_description', {
                        required: t('question-validation-msg')['description-required'],
                        maxLength: {
                            value: 2000,
                            message: t(t('question-validation-msg')['description-max-length'], ['2000']),
                        },
                    })}
                    className={`form-input-stretch ${
                        errors['parent_question_description'] ? 'error' : ''
                    }`}
                    style={{ height: '6rem' }}
                    viewMode={viewMode}
                    specialChar={['#','*']}
                />    
                <div className="question-table-mc10" style={{ position: 'relative' }}>
                    <table className="question-table-mc10-table">
                        <thead className="table-thead">
                            <tr className="tr-mc10">
                                <th className="th-mc10"
                                    dangerouslySetInnerHTML={{
                                        __html: t('create-question')['select-question-example']}}
                                >
                                </th>
                                <th className="th-mc10" align="left">
                                    {t('create-question')['list-questions-examples']}
                                </th>
                                <th className="th-mc10"></th>
                            </tr>
                        </thead>
                        <tbody className="table-tbody">
                            {answerGroup?.map((answer: AnswerGroup, answerIndex: number) => {
                            if (answer.isQuestion) {
                                indexA += 1
                            } else {
                                indexE += 1
                            }
                            return (
                                <tr key={answer.key} id={answer.key} className="tr-mc10">
                                <td align="center" className={`td-mc10`}>
                                    <div
                                        className={`toggle-switch ${
                                            answer.toggleErr ? 'toggle-error' : ''
                                        }`}
                                    >
                                    <Toggle
                                        size="md"
                                        checked={answer.isQuestion}
                                        checkedChildren={t("common")["question"]}
                                        unCheckedChildren={t('common')['example']}
                                        onChange={() => handleToggle(answerIndex)}
                                    />
                                    </div>
                                </td>
                                <td className="td-mc10">
                                    <div className="image-input-mc10">
                                    <ImageInputMC10
                                        url={answer.imageStr}
                                        onChangeFile={onChangeImage.bind(null, answerIndex)}
                                        labelDefault={
                                            answer.isQuestion
                                            ? `${t(t("create-question")["question-image"], [`${indexA + 1}`])}`
                                            : `${t(t('create-question')['image-example-with-num'], [`${indexE + 1}`])}`
                                        }
                                        viewMode={viewMode}
                                        isError={listImageError?.includes(
                                            answerIndex.toString()) || listImageFileError?.includes(
                                                answerIndex.toString())}
                                    />
                                    </div>
                                </td>
                                <td className="td-mc10">
                                    <QuestionMC10
                                        questions={answer?.questionText}
                                        errorStr={arrErrAnswerMessage[answerIndex] || ''}
                                        correctErrorStr={arrErrCorrectMessage.includes(
                                            answerIndex.toString(),
                                        )}
                                        questionErrorStr={arrErrQuestionTextMessage.includes(
                                            answerIndex.toString(),
                                        )}
                                        onQuestionChange={(data) => onMCChange(data, answerIndex)}
                                        answers={answer?.answerStr}
                                        corrects={answer?.correctStr}
                                        viewMode={viewMode}
                                        isQuestion={answer.isQuestion}
                                    />
                                    <img
                                        onClick={onDeleteAnswer.bind(null, answerIndex)}
                                        className="ic-action"
                                        src="/images/icons/ic-trash.png"
                                        alt="delete-icon"
                                    />
                                </td>
                                </tr>
                            )
                            })}
                        </tbody>
                        </table>
                    </div>

                    <div className="form-input-stretch">
                        {!viewMode && (
                        <Button
                            appearance={'primary'}
                            type="button"
                            style={{
                                backgroundColor: 'white',
                                width: '20rem',
                                color: '#5CB9D8',
                                fontSize: '1.6rem',
                                fontWeight: 600,
                            }}
                            onClick={onAddQuestion}
                            className={`btn-add-new-question ${answerGroup.length==0 ? "error" : ""}`}
                        >
                            <label className="add-new-answer">
                            <img
                                src="/images/icons/ic-plus-sub.png"
                                width={17}
                                height={17}
                                alt=""
                            />{' '}
                                {t('create-question')['add-question']}
                            </label>
                        </Button>
                        )}
                    </div>
                    {(errorExample || errorQuestion || errorImageFile) && (
                        <div className="render_error">
                        {errorExample && <span>* {errorExample}</span>}
                        {errorQuestion && <span>* {errorQuestion}</span>}
                        {errorImageFile && <span>* {errorImageFile}</span>}
                        </div>
                    )}
                
            </div>
        </div>
    )
}

type PropsType = {
    labelDefault?: string
    isError?: boolean
    url: string
    onChangeFile?: (files: FileList) => void
    viewMode?: boolean
}

function ImageInputMC10({
    isError,
    url,
    onChangeFile,
    viewMode,
    labelDefault,
}: PropsType) {
    const {t} = useTranslation()
    const [imageUrl, setImageUrl] = useState(url ?? '')
    const fileRef = useRef(null)
    const src = imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`
    return (
        <div
            className={`box-image-input-mc10 ${imageUrl !== '' ? 'has-data' : ''} ${
                isError ? 'error' : ''
            }`}
        >
            <div className="image-section">
                <div
                    className="image-input-origin"
                    onClick={() => fileRef.current.dispatchEvent(new MouseEvent('click'))}
                >
                {src && url ? (
                    <>
                    <img className="image-upload" src={src} alt="image-upload" />
                    {!viewMode && src && url && (
                        <img
                            className="image-delete-icon"
                            src="/images/icons/ic-remove-opacity.png"
                            alt=""
                            onClick={(event) => {
                                fileRef.current.value = ''
                                event.preventDefault()
                                event.stopPropagation()
                                onChangeFile(null)
                            }}
                        />
                    )}
                    </>
                ) : (
                    <>
                    <div
                        style={{
                            display: 'flex',
                            flexDirection: 'column',
                            justifyContent: 'center',
                            alignItems: 'center',
                            height: '100%',
                        }}
                    >
                        <img
                            className="icon"
                            src="/images/icons/ic-image.png"
                            alt="delete-img-icon"
                        />
                        {labelDefault}
                    </div>
                    </>
                )}
                <input
                    ref={fileRef}
                    type="file"
                    style={{ display: 'none' }}
                    accept=".png,.jpeg,.jpg"
                    onChange={(e) => {
                        if (fileRef.current.files.length > 0) {
                            setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                            onChangeFile && onChangeFile(e.target.files)
                        }
                    }}
                />
                </div>
            </div>
        </div>
    )
}

type AnswerGroup = {
    key: string
    imageStr: string
    answerStr: string
    correctStr: string
    questionText: string
    isQuestion: boolean
    toggleErr?: boolean
}
