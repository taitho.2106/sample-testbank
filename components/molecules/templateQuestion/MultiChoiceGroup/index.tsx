import { useEffect, useRef } from 'react'

import { Button, Tooltip, Whisper } from 'rsuite'

import convertFontStyle from '@/questions_middleware/convertFontStyle'
import convertSpaceWord from '@/questions_middleware/convertSpaceWord'
import { ContentField } from 'components/atoms/contentField'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionPropsType } from 'interfaces/types'

import AudioInput from '../AudioInput'
import MultiChoiceGroupQuestion from './question'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation'

export default function MultiChoiceGroup({
  question,
  register,
  setValue,
  isAudio = false,
  errors = {},
  viewMode,
  questionType,
}: QuestionPropsType) {
  const {t} = useTranslation()
  useEffect(() => {
    register('question_text', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
    },
      value: question?.question_text,
    })
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '').split('#').map((m) =>
          m
            .split('*')
            .map((g, gIndex) => {
              if (g === '') return gIndex
              return null
            })
            .filter((m) => m !== null),
        )
        return errs.some((m) => m.length > 0)
          ? errs.map((m) => m.join()).join('#')
          : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: 'Đáp án không được lớn hơn 10000 ký tự',
      },
      value: question?.correct_answers,
    })
    if (isAudio) {
      register('audio_file',{
        validate: {
          acceptedFormats: async(file) => {
            // check format if has file
            if (!file) {
              return true
            }
            if(file.size ==0){
              return "Âm thanh bị lỗi hoặc đã bị xóa, vui lòng kiểm tra lại"
            }
            if(!['audio/mp3','audio/mpeg'].includes(file?.type)){
              return 'Chỉ hổ trợ định dạng âm thanh mp3'
            }
            if(!await browserFile.checkFileExist(file)){
              return "Âm thanh bị lỗi hoặc đã bị xóa, vui lòng kiểm tra lại"
            }
            return true;
          },
        }
      })
      register('audio', {
        maxLength: {
          value: 2000,
          message: 'Audio không được lớn hơn 2000 ký tự',
        },
        required: 'Audio không được để trống',
        value: question?.audio,
      })
      register('audio_script', {
        maxLength: {
          value: 10000,
          message: 'Nội dung bài nghe không được lớn hơn 10000 ký tự',
        },
        value: question?.audio_script,
      })
    } else {
      register('parent_question_text', {
        maxLength: {
          value: 10000,
          message: 'Câu hỏi chung không được lớn hơn 10000 ký tự',
        },
        required: 'Câu hỏi chung không được để trống',
        value: question?.parent_question_text,
      })
    }
  }, [])
  const inputRef = useRef(null)

  const onMCChange = (dataV: any) => {
    setValue('answers', dataV.answers)
    setValue('correct_answers', dataV.corrects)
    setValue('question_text', dataV.questions)
  }

  const onChangeAudioScript = (data: any) => {
    setValue('audio_script', data)
  }
  const onChangeParentQuestionText = (data: any) => {
    setValue('parent_question_text', data)
  }
  const onChangeFile = (files: FileList) => {
    setValue('audio_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('audio', urlAudio)
  }
  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)

  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }
  return (
    <div className="m-multi-choice" style={{ display: 'flex' }}>
      {!isAudio ? (
        <div
          className={`form-input-stretch`}
          style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            position: 'relative'
          }}
        >
          <ContentField
            ref={inputRef}
            label="Nội dung bài đọc"
            strValue={question?.parent_question_text?.replace(/\%s\%/g, '##')}
            disableTabInput={true}
            isMultiTab={true}
            onChange={onChangeParentQuestionText}
            // onBlur={onChangeParentQuestionText.bind(null,)}
            style={{ flex: 1 }}
            className={`${errors['parent_question_text'] ? 'error' : ''}`}
            inputStyle={{ paddingRight: '3rem', height: '100%' }}
            viewMode={viewMode}
            specialChar={['#','*']}
          >
            <div className="box-action-info">
              <div style={{ display: 'flex', alignItems: "flex-end" }}>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onBold}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-bold.png)',
                    } as MyCustomCSS
                  }
                ></div>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onItalic}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-italic.png)',
                    } as MyCustomCSS
                  }
                ></div>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onUnderline}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-underline.png)',
                    } as MyCustomCSS
                  }
                ></div>
              </div>
              <div className="clear-outline" style={{}}></div>
            </div>
          </ContentField>
        </div>
      ) : (
        <AudioInput
          url={question?.audio}
          script={question?.audio_script}
          isError={errors['audio'] || errors['audio_file']}
          onChangeFile={onChangeFile}
          onChangeScript={onChangeAudioScript}
          style={{ height: '468px' }}
          viewMode={viewMode}
        />
      )}
      <div style={{ flex: 1 }}>
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            label="Yêu cầu đề bài"
            register={register('parent_question_description', {
              required: 'Yêu cầu đề bài không được để trống',
              maxLength: {
                value: 2000,
                message: 'Yêu cầu đề bài chung không được lớn hơn 2000 ký tự',
              },
            })}
            defaultValue={question?.parent_question_description}
            style={{ height: '9rem' }}
            inputStyle={isAudio ? { paddingBottom: '4.5rem' } : {}}
            className={errors['parent_question_description'] ? 'error' : ''}
            viewMode={viewMode}
            specialChar={['#','*']}
          />
        </div>
        <MultiChoiceGroupQuestion
          questionType={questionType}
          questions={question?.question_text}
          answers={question?.answers}
          corrects={question?.correct_answers}
          onQuestionChange={onMCChange}
          errorStr={errors['answers']?.message ?? ''}
          correctErrorStr={errors['correct_answers']?.message ?? ''}
          questionErrorStr={errors['question_text']?.message ?? ''}
          // clearInputOutlineStyle={{backgroundColor: '#f5f8fc'}}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}
