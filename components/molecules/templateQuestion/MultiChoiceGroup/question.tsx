import { MyCustomCSS } from '@/interfaces/types'
import { ContentField } from 'components/atoms/contentField'
import { Fragment, useEffect, useRef, useState } from 'react'
import { Button, Tooltip, Whisper } from 'rsuite'
import MultiChoiceTable from '../MultiChoice/table'
import MultiChoiceTableMC5 from './table'
import useTranslation from '@/hooks/useTranslation'

type PropsType = {
  corrects: string
  answers: string
  questions: string
  errorStr?: string
  correctErrorStr?: string
  questionErrorStr?: string
  onQuestionChange?: (data: any) => void
  clearInputOutlineStyle?: any
  questionType?: string
  viewMode?: boolean
}
export default function MultiChoiceGroupQuestion({
  corrects,
  answers,
  questions,
  errorStr = '',
  correctErrorStr = '',
  questionErrorStr = '',
  onQuestionChange,
  clearInputOutlineStyle = {},
  questionType,
  viewMode,
}: PropsType) {
  const {t} = useTranslation()
  const correctList = corrects?.split('#') ?? []
  const answerList = answers?.split('#') ?? ['***']
  const questionList = questions?.split('#') ?? []
  const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
    answerList.map((g: string, gIndex: number) => {
      return {
        key: new Date().getTime().toString() + gIndex,
        answerStrs: g,
        correctStrs: correctList[gIndex],
        questionText: questionList[gIndex],
      }
    }),
  )
  const answerGroupRef = useRef(answerGroup)
  const isInit = useRef(true)

  useEffect(() => {
    answerGroupRef.current = answerGroup
    if (!isInit.current) {
      onQuestionChange &&
        onQuestionChange({
          questions: answerGroup.map((m) => m.questionText).join('#'),
          answers: answerGroup.map((m) => m.answerStrs).join('#'),
          corrects: answerGroup.map((m) => m.correctStrs).join('#'),
        })
    }
    isInit.current = false
  }, [answerGroup])

  const onAddQuestion = () => {
    const newQuestion: any = {
      key: new Date().getTime().toString() + (answerGroup?.length ?? 0),
      questionText: '',
      answerStrs: '***',
      correctStrs: null,
    }
    if (answerGroup) {
      setAnswerGroup([...answerGroup, newQuestion])
    } else {
      setAnswerGroup([newQuestion])
    }
  }

  const onChange = (key: string, data: any) => {
    const answer = answerGroupRef.current.find((m) => m.key === key)
    answer.answerStrs = data.answers
    answer.correctStrs = data.correct_answers
    setAnswerGroup([...answerGroupRef.current])
  }

  const onChangeQuestionText = (key: string, data: string) => {
    const answer = answerGroupRef.current.find((m) => m.key === key)
    answer.questionText = data.replace(/##/g, '%s%')
    setAnswerGroup([...answerGroupRef.current])
  }

  const onRemoveQuestion = (key: string) => {
    const ans = answerGroup.filter((m: any) => m.key !== key)
    setAnswerGroup([...ans])
  }

  const listErrors = errorStr.split('#')
  const listQError = questionErrorStr.split(',')
  const listCError = correctErrorStr.split(',')

  return (
    <Fragment>
      {answerGroup?.map((m, mIndex) => (
        <GroupItem
          key={m.key}
          questionText={m.questionText}
          answerStrs={m.answerStrs}
          correctStrs={m.correctStrs}
          onChangeQuestionText={onChangeQuestionText.bind(null, m.key)}
          onChange={onChange.bind(null, m.key)}
          onRemoveQuestion={onRemoveQuestion.bind(null, m.key)}
          error={listCError.includes(mIndex.toString())}
          errorStr={listErrors[mIndex]}
          question_error={listQError.includes(mIndex.toString())}
          clearInputOutlineStyle={clearInputOutlineStyle}
          questionType={questionType}
          viewMode={viewMode}
        />
      ))}
      <div className="form-input-stretch">
        {!viewMode && (
          <Button
            appearance={'primary'}
            type="button"
            style={{
              backgroundColor: 'white',
              width: '20rem',
              color: '#5CB9D8',
              fontSize: '1.6rem',
              fontWeight: 600,
              border:answerGroup.length == 0?"1px solid #dd3a09":"none"
            }}
            onClick={onAddQuestion}
          >
            <label className="add-new-answer">
              <img
                src="/images/icons/ic-plus-sub.png"
                width={17}
                height={17}
                alt=""
              />{' '}
              {t('create-question')['add-question']}
            </label>
          </Button>
        )}
      </div>
    </Fragment>
  )
}

type GroupItemProps = {
  questionText: string
  question_error: boolean
  onChangeQuestionText: (data: string) => void
  answerStrs: string
  correctStrs: string
  onChange: (data: string) => void
  error: boolean
  errorStr: string
  onRemoveQuestion: () => void
  clearInputOutlineStyle?: any
  questionType?: string
  viewMode?: boolean
}

function GroupItem({
  questionText,
  question_error,
  onChangeQuestionText,
  answerStrs,
  correctStrs,
  onChange,
  error,
  errorStr,
  onRemoveQuestion,
  clearInputOutlineStyle = {},
  questionType,
  viewMode,
}: GroupItemProps) {
  const {t} = useTranslation()
  const inputRef = useRef(null)

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }
  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)
  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }
  return (
    <div className="form-input-stretch" style={{ position: 'relative' }}>
      <div style={{ position: 'relative' }}>
        <ContentField
          ref={inputRef}
          label={t('common')['question']}
          strValue={questionText?.replaceAll(/\%s\%/g, '##')}
          disableTabInput={true}
          isMultiTab={true}
          onBlur={onChangeQuestionText}
          className={question_error ? 'error' : ''}
          style={{ marginBottom: '1rem', height: '9rem' }}
          inputStyle={{ paddingRight: '3rem' }}
          viewMode={viewMode}
          specialChar={['#','*']}
          disableStyleText={questionType !== 'MC5' ? ['b','u','i'] : []}
        >
          <div className="box-action-info">
            <div style={{ display: 'flex', alignItems: 'flex-end' }}>
              <Whisper
                placement="top"
                trigger={'hover'}
                speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
              >
                <Button className="add-new-answer" onMouseDown={onAddSpace}>
                  <img src="/images/icons/ic-underscore.png" alt="" />
                </Button>
              </Whisper>
              {questionType === 'MC5' && (
                <>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onBold}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onItalic}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-italic.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onUnderline}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-underline.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                </>
              )}
            </div>
            <div className="clear-outline" style={clearInputOutlineStyle}></div>
          </div>
        </ContentField>
        {/* <div className="box-action-info">
          <span className="add-new-answer" onMouseDown={onAddSpace}>
            <img src="/images/icons/ic-plus-sub.png" width={17} height={17} />{' '}
            Thêm khoảng trống
          </span>
          <div className="box-media"></div>
        </div> */}
      </div>
      {questionType === 'MC5' ? (
        <MultiChoiceTableMC5
          answerStr={answerStrs}
          correctStr={correctStrs}
          onChange={onChange}
          className={error ? 'error' : ''}
          errorStr={errorStr}
          styleBold={true}
          viewMode={viewMode}
        />
      ) : (
        <MultiChoiceTable
          answerStr={answerStrs}
          correctStr={correctStrs}
          onChange={onChange}
          className={error ? 'error' : ''}
          errorStr={errorStr}
          viewMode={viewMode}
        />
      )}
      <img
        className="ic-delete-question"
        src="/images/icons/ic-trash.png"
        onClick={onRemoveQuestion}
        alt=""
      />
    </div>
  )
}

type AnswerGroup = {
  key: string
  answerStrs: string
  correctStrs: string
  questionText: string
}
