import { useEffect, useRef, useState, useCallback } from 'react'

import ContentEditable from 'react-contenteditable'

import { MyCustomCSS } from 'interfaces/types'
import {
  convertHtmlToStr,
  convertStrToHtml,
  formatHtmlText,
  getTextWithoutFontStyle,
  onPaste,
  onType
} from "utils/string";
import useTranslation from '@/hooks/useTranslation';

type PropType = {
  answerStr: string
  correctStr: string
  styleBold?: boolean
  className?: string
  errorStr?: string
  onChange: (data: any) => void
  questionType?:string
  viewMode?: boolean
}

export default function MultiChoiceTableMC5({
  answerStr,
  correctStr,
  styleBold = false,
  className = '',
  errorStr = '',
  onChange,
  viewMode,
}: PropType) {
  const {t} = useTranslation()
  const [answers, setAnswers] = useState<Answers[]>(
    answerStr?.split('*')?.map((m: string) => {
      const textAnswer = getTextWithoutFontStyle(m)
      return {
        text: m,
        isCorrect: (styleBold ? textAnswer : m) === correctStr,
      }
    }),
  )

  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  useEffect(() => {
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        onChange({
          answers: answers.map((m) => m.text.trim()).join('*'),
          correct_answers: getTextWithoutFontStyle(answers
            .find((m) => m.isCorrect)
            ?.text.trim()),
        })
      }
    }

    setTimeout(() => {
      Array.from(document.getElementsByClassName(`${groupKey}`)).forEach(
        (el) => {
          const div = el as HTMLDivElement
          div.style.width = `${div.offsetWidth}px`
        },
      )
    }, 0)
  }, [answers])

  const onAddAnswer = () => {
    if (answers) {
      setAnswers([...answers, { text: '', isCorrect: false }])
    } else {
      setAnswers([{ text: '', isCorrect: false }])
    }

    setTimeout(() => {
      document.getElementById(`${groupKey}_${answers?.length ?? 0}`)?.focus()
    }, 0)
  }

  const onChangeIsCorrect = (index: number) => {
    answers.forEach((answer: Answers, answerIndex: number) => {
      answer.isCorrect = index === answerIndex
    })
    setAnswers([...answers])
  }

  const onChangeText = (index: number, event: any) => {
    const el: HTMLElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    answers[index].text = text.replace(/(\*|\#)/g, '')
    setAnswers([...answers])
  }
  const onBlurText = (index: number, event: any) => {
    const el: HTMLElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    answers[index].text = text.replace(/(\*|\#)/g, '').trim()
    el.innerHTML = convertStrToHtml(answers[index].text)
    // setAnswers([...answers])
  }

  const onDeleteAnswer = (index: number) => {
    answers.splice(index, 1)
    setAnswers([...answers])
  }
  const onClickType = (type: "Bold" | "Underline" | "Italic") => {
    onType({
      classElement: "div-input",
      type: type,
      groupKey: groupKey,
      action: (position, index, value) => {
        answers[index].text = value.replace(/(\*|\#)/g, "");
        setAnswers([...answers]);
      }
    });
  }
  const listErrors = errorStr.split(',')
  return (
    <div className={className} style={{ position: 'relative' }}>
      <table className={`question-table`}>
        <thead>
          {<tr>
            <th>{t('create-question')['correct-answer']}</th>
            <th>{t('create-question')['answer-key']}</th>
            <th align="right">
              {(<div style={{ display: 'flex' }}>
                {styleBold  && (
                  <div
                    className="icon-mask icon-action-style"
                    onMouseDown={() => onClickType("Bold")}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                )}
                <div
                  className="icon-mask icon-action-style"
                  onMouseDown={() => onClickType("Italic")}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-italic.png)',
                    } as MyCustomCSS
                  }
                ></div>
                <div
                  className="icon-mask icon-action-style"
                  onMouseDown={() => onClickType("Underline")}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-underline.png)',
                    } as MyCustomCSS
                  }
                ></div>
              </div>)}
            </th>
          </tr>}
        </thead>
        <tbody>
          {answers?.map((answer: Answers, answerIndex: number) => [
            <tr key={`${groupKey}_${answerIndex}`}>
              <td align="center" className="option">
                <input
                  type="radio"
                  name={groupKey.toString()}
                  checked={answer.isCorrect}
                  onChange={onChangeIsCorrect.bind(null, answerIndex)}
                />
              </td>
              <td
                className={`input-placeholder ${
                  listErrors.includes(answerIndex.toString()) ? 'error' : ''
                }`}
              >
                <ContentEditable
                  id={`${groupKey}_${answerIndex}`}
                  spellCheck={false}
                  className={`div-input ${groupKey}`}
                  html={convertStrToHtml(answer.text)}
                  contentEditable="true"
                  onChange={onChangeText.bind(null, answerIndex)}
                  onBlur={onBlurText.bind(null, answerIndex)}
                  onPaste={onPaste}
                  onKeyDown={(event)=>{
                    if (event.key === 'Enter') {
                      event.preventDefault()
                    }
                  }}
                />
                <span className="placeholder">{`${t('create-question')['answer']} ${
                  answerIndex + 1
                }`}</span>
              </td>
              <td align="right">
                <img
                  onClick={onDeleteAnswer.bind(null, answerIndex)}
                  className="ic-action"
                  src="/images/icons/ic-trash.png"
                  alt=''
                />
              </td>
            </tr>,
          ])}
          {answers.length < 26 && 
          !viewMode && (
            <tr className="action">
              <td></td>
              <td colSpan={2} align="left">
                <label className={`add-new-answer ${answers.length === 0 ? 'error' : ''}`} onClick={onAddAnswer}>
                  <img
                    src="/images/icons/ic-plus-sub.png"
                    width={17}
                    height={17}
                    alt=''
                  />{' '}
                  {t('create-question')['add-answer']}
                </label>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}

type Answers = {
  text: string
  isCorrect: boolean
}
