import browserFile from "lib/browserFile"
import { useState, useRef, ChangeEvent, useEffect, useCallback } from "react";

import ContentEditable from "react-contenteditable"
import { Toggle } from "rsuite"
import { MyCustomCSS } from "@/interfaces/types";
import { convertStrToHtml } from "@/utils/string";
import useTranslation from "@/hooks/useTranslation";

type PropType = {
    setValue?: any
    styleBold?: boolean
    className?: string
    errorStr?: string
    register?:any
    correctErrorStr?: string
    onChange?: (data: any) => void
    errors?: any
    question?:any
    viewMode?:boolean
    isImage?:boolean
}
function QuestionTableTF4({
    question,
    register,
    errors = {},
    setValue,
    className = '',
    errorStr = '',
    correctErrorStr = '',
    onChange,
    viewMode = false,
    isImage,
}: PropType) {
    const {t} = useTranslation()
    const listImage = (question?.image ?? '###').split('#') ?? []
    const listAnswer = (question?.answers ?? '###').split('#') ?? []
    const listCorrect = (question?.correct_answers ?? '###').split('#') ?? []
    const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
        listAnswer.map((answer:string, answerIndex:number)=>{
            const list:any[] = listAnswer
            if(list[answerIndex].startsWith('*')){
                list[answerIndex] = list[answerIndex].replace('*','')
            }
            return ({
                key: new Date().getTime().toString() + answerIndex,
                imageStr:listImage[answerIndex],
                answerStr:list[answerIndex],
                correctStr:listCorrect[answerIndex],
                isExam: answer.startsWith('*') ? false : true,
        })})
    )
    const [arrNewIndex, setArrNewIndex] = useState([])
    useEffect(() => {
        register('answers', {
      validate: (value: string) => {
        // if ((value ?? '') === '') return ''
        const errs = value
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return arrNewIndex[mIndex]
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '###',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const ansl = ['T', 'F']
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (!ansl.includes(m)) return arrNewIndex[mIndex]
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers ?? '###',
    })
        register('isExam', {
            validate: (value: string) => {
                const errs = (value ?? '').split('#')
                const errE = errs.filter((item: string) => item === 'false')
                const errA = errs.filter((item: string) => item === 'true')
                if (errE.length > 3) return t(t('question-validation-msg')['example-allow-total'], ['3'])
                if (errA.length < 1) return t(t('question-validation-msg')['question-fb-required'], ['1'])
                return true
            }
        })
        if (isImage) {
            register('image_files',{
                validate: async(files:any) =>
                    {
                      // check format if has file
                      if(!files){
                        return true;
                      }
                      const checkFile = files?.filter((item:any)=> item !== null)
                      if(checkFile?.length === 0){
                        return true;
                      }
                      let mIndex = -1
                      const errTemp:number[] = []
                      for await (const file of files) {
                        mIndex += 1
                        if (file !== null) {
                          if (!['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)) {
                            errTemp.push(arrNewIndex[mIndex])
                            continue
                          } else if (!(await browserFile.checkFileExist(file))) {
                            errTemp.push(arrNewIndex[mIndex])
                            continue
                          }
                        }else{
                            errTemp.push(null)
                        }
                      }
                      const arrErr = errTemp.filter(m => m !== null)
                     return arrErr.length > 0 ? arrErr.join('#') : true
                    },
              })
            register('image', {
              validate: (value: string) => {
                const errs = (value ?? '')
                  .split('#')
                  .map((m, mIndex) => {
                    if (m === '') return arrNewIndex[mIndex]
                    return null
                  })
                  .filter((m) => m !== null)
                return errs.length > 0 ? errs.join() : true
              },
              maxLength: {
                value: 2000,
                message: t(t('question-validation-msg')['image-max-length'], ['2000']),
              },
              value: question?.image,
            })
          }
    }, [arrNewIndex])
   
    const answerGroupRef = useRef(answerGroup)
    const imageFiles = useRef(answerGroup.map((m) => null))

    const groupKey = useRef(Math.random()).current
    const isInit = useRef(true)
   
   
    useEffect(() => {
        answerGroupRef.current = answerGroup
        const example = []
        const ans = []
        const arrIndexEx = []
        const arrIndexAns = []
        const imageFilesClone = [...imageFiles?.current]
        for(let i=0; i<answerGroup.length;i++){
          if(answerGroup[i].isExam){
            ans.push(answerGroup[i])
            arrIndexAns.push(i)
          }else{
            example.push(answerGroup[i])
            arrIndexEx.push(i)
          }
        }
        const arrNew = [...arrIndexEx, ...arrIndexAns]
        setArrNewIndex(arrNew);
        if(imageFiles?.current){
            for(let i=0; i<arrNew.length; i++){
                imageFilesClone[i] = imageFiles?.current[arrNew[i]]
            }
            setValue('image_files', imageFilesClone)
        }
        if(answerGroup){
            setValue('image', [...example,...ans].map((m) => m.imageStr).join('#'))
            setValue('answers', [...example,...ans].map((m) => m.answerStr).join('#'))
            setValue('correct_answers', [...example,...ans].map((m) => m.correctStr).join('#'))
            setValue('isExam',[...example,...ans].map((m) => m.isExam).join('#'))

            
        }
        if (isInit.current) {
            isInit.current = false
        } else {
            if (typeof onChange === 'function') {
                onChange({
              answers: [...example,...ans].map((m) => (m.answerStr ?? '').trim()).join('#'),
              correct_answers: [...example,...ans].map((m) => (m.correctStr ?? '').trim()).join('#'),
              image: [...example,...ans].map((m)=>m.imageStr).join('#')
                })
            }
        }

        setTimeout(() => {
            Array.from(document.getElementsByClassName(`${groupKey}`)).forEach(
                (el) => {
                    const div = el as HTMLDivElement
                    div.style.width = `${div.offsetWidth}px`
                },
            )
        }, 0)
    }, [answerGroup])
    const onAddAnswer = () => {
        const newAnswer = {
            key: new Date().getTime().toString() + (answerGroup?.length ?? 0),
            imageStr: '',
            answerStr: '',
            correctStr: '',
            isExam:true,
        }
        if (answerGroup) {
            setAnswerGroup([...answerGroup, newAnswer])
        } else {
            setAnswerGroup([newAnswer])
        }
        imageFiles.current.push(null)
        setTimeout(() => {
            document.getElementById(`${groupKey}_${answerGroup?.length ?? 0}`)?.focus()
        }, 0)
    }
    const onDeleteAnswer = (index: number) => {
        // console.log("index----",index);
        const ans:any[] = answerGroup.filter((m: any, mIndex: number) => mIndex !== index)
        if(imageFiles.current.length > index){
            imageFiles.current.splice(index, 1)
        }
        setAnswerGroup(ans)
    }
    const onChangeIsCorrect = (
        index: number,
        event: ChangeEvent<HTMLInputElement>,
    ) => {
        answerGroup[index].correctStr = event.target.dataset.value
        setAnswerGroup([...answerGroup])
    }
    const onChangeText = (index: number, event: any) => {
        const el: HTMLElement = event.currentTarget
        let text = ''
        for (let i = 0, len = el.childNodes.length; i < len; i++) {
            const childNode = el.childNodes[i]
            text += convertHtmlToStr([childNode])
        }
        answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '')
        setAnswerGroup([...answerGroup])
    }
    const onBlurText = (index: number, event: any) => {
        const el: HTMLElement = event.currentTarget
        let text = ''
        for (let i = 0, len = el.childNodes.length; i < len; i++) {
            const childNode = el.childNodes[i]
            text += convertHtmlToStr([childNode])
        }
        answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '').trim()
        el.innerHTML = convertStrToHtml(answerGroup[index].answerStr)
        // setAnswerGroup([...answerGroup])
    }
    const onChangeImage = (index: number, files: FileList) => {
        if(files && files[0]){
            console.log("files[0]===",files[0]);
            const urlImage = URL.createObjectURL(files[0])
            answerGroup[index].imageStr = urlImage
            imageFiles.current[index] = files[0]
            setAnswerGroup([...answerGroup])
            
        }else{
            answerGroup[index].imageStr = ''
            imageFiles.current[index] = null
            setAnswerGroup([...answerGroup])
        }

    }
    const listExamError = answerGroup.filter((item:any)=> item.isExam === false)
    const listAError = answerGroup.filter((item:any)=> item.isExam === true)
    const listError = errorStr.split(',')
    const listCError = correctErrorStr.split(',')
    const listImageError = (errors['image']?.message ?? '').split(',')
    const listImageFilesError:string[] = errors['image_files']?.message.split('#') ?? []
    const [err, setErr] = useState(false)
    const [error, setError] = useState('')
    const renderError = () => {
        if(listExamError.length > 3){
            setErr(true)
            setError(`* ${t(t('question-validation-msg')['example-allow-total'], ['3'])}`)
        }else{
            setErr(false)
        setError('')
        }
        if(listAError.length < 1){
            setErr(true)
            setError(`* ${t(t('question-validation-msg')['question-fb-required'], ['1'])}`)
        }else{
            setErr(false)
            setError('')
        }
        if(listImageFilesError.length > 0){
            setErr(true)
        }else{
            setErr(false)
        }
    }
    const handleToggle = (id:number,value:boolean) =>{
        answerGroup[id].isExam = !value
        setAnswerGroup([...answerGroup])
    }
    useEffect(()=>{
        renderError()
    },[listExamError,listAError])

    let indexE = -1;
    let indexA = -1;

    const convertHtmlToStr = useCallback((dataD: ChildNode[]) => {
        let str = ''
        for (let i = 0, len = dataD.length; i < len; i++) {
            const child = dataD[i] as Node
            if (child.nodeName === '#text') {
                str += child.textContent
            } else if (child.nodeName === 'SPAN') {
                str += child.firstChild.textContent
            } else if (child.nodeName === 'BR') {
                // str += '\n'
            } else if (child.nodeName === 'INPUT') {
                const inputEl = child as HTMLInputElement
                str += `#${inputEl.value}#`
            } else {
                const el = child as Element
                const tag = child.nodeName.toLowerCase()
                const content = convertHtmlToStr(Array.from(el.childNodes))
                if (content !== '') {
                    str += `%${tag}%${content}%${tag}%`
                }
            }
        }
        return str
    }, [])
    const onPaste = (event: any) => {
        event.preventDefault()
        let data = event.clipboardData.getData('text').replace(/(\#|\*)/g, '')
        data = data?.replace(/\r?\n|\r/g, '')
        const selection = window.getSelection()
        const range = selection.getRangeAt(0)
        range.deleteContents()
        const node = document.createTextNode(data)
        range.insertNode(node)
        selection.removeAllRanges()
        const newRange = document.createRange()
        newRange.setStart(node, data.length)
        selection.addRange(newRange)
    }
    const onBold = () => {
        const selection = window.getSelection()
        if (selection.rangeCount === 0) return
        const range = selection.getRangeAt(0)
        const parent = range.commonAncestorContainer
        let el: HTMLElement = null
        let parentEl =
            parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
        while (parentEl.parentElement) {
            if (parentEl.classList.contains('div-input')) {
                el = parentEl
                break
            }
            parentEl = parentEl.parentElement
        }
        document.execCommand('Bold', false, null)
        if (el) {
            const index = parseInt(el.id.replace(`${groupKey}_`, ''))
            let text = ''
            for (let i = 0, len = el.childNodes.length; i < len; i++) {
                const childNode = el.childNodes[i]
                text += convertHtmlToStr([childNode])
            }
            answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '')
            setAnswerGroup([...answerGroup])
        }
    }
    const onItalic = () => {
        const selection = window.getSelection()
        if (selection.rangeCount === 0) return
        const range = selection.getRangeAt(0)
        const parent = range.commonAncestorContainer
        let el: HTMLElement = null
        let parentEl =
            parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
        while (parentEl.parentElement) {
            if (parentEl.classList.contains('div-input')) {
                el = parentEl
                break
            }
            parentEl = parentEl.parentElement
        }
        document.execCommand('Italic', false, null)
        if (el) {
            const index = parseInt(el.id.replace(`${groupKey}_`, ''))
            let text = ''
            for (let i = 0, len = el.childNodes.length; i < len; i++) {
                const childNode = el.childNodes[i]
                text += convertHtmlToStr([childNode])
            }
            answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '')
            setAnswerGroup([...answerGroup])
        }
    }
    const onUnderline = () => {
        const selection = window.getSelection()
        if (selection.rangeCount === 0) return
        const range = selection.getRangeAt(0)
        const parent = range.commonAncestorContainer
        let el: HTMLElement = null
        let parentEl =
            parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
        while (parentEl.parentElement) {
            if (parentEl.classList.contains('div-input')) {
                el = parentEl
                break
            }
            parentEl = parentEl.parentElement
        }
        document.execCommand('Underline', false, null)
        if (el) {
            const index = parseInt(el.id.replace(`${groupKey}_`, ''))
            let text = ''
            for (let i = 0, len = el.childNodes.length; i < len; i++) {
                const childNode = el.childNodes[i]
                text += convertHtmlToStr([childNode])
            }
            answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '')
            setAnswerGroup([...answerGroup])
        }
    }
    return (
        <div className={`question-table-tf4 ${className}`} style={{ position: 'relative' }}>
            <table className={`question-table question-table-tf4-table`}>
                <thead className={`question-table-tf4-thead`}>
                    <tr>
                        <th
                            dangerouslySetInnerHTML={{
                                __html: t('create-question')['select-question-example']
                            }}
                        ></th>
                        <th align="left">{t('create-question')['list-questions-examples']}</th>
                        <th>
                            <div style={{ display: 'flex' }}>
                                <div
                                    className="icon-mask icon-action-style"
                                    onMouseDown={onBold}
                                    style={
                                        {
                                            '--image': 'url(/images/icons/ic-bold.png)',
                                        } as MyCustomCSS
                                    }
                                ></div>
                                <div
                                    className="icon-mask icon-action-style"
                                    onMouseDown={onItalic}
                                    style={
                                        {
                                            '--image': 'url(/images/icons/ic-italic.png)',
                                        } as MyCustomCSS
                                    }
                                ></div>
                                <div
                                    className="icon-mask icon-action-style"
                                    onMouseDown={onUnderline}
                                    style={
                                        {
                                            '--image': 'url(/images/icons/ic-underline.png)',
                                        } as MyCustomCSS
                                    }
                                ></div>
                            </div>
                        </th>
                        <th align="center">True</th>
                        <th align="center">False</th>
                        <th align="center"></th>
                    </tr>
                </thead>
                <tbody className={`question-table-tf4-tbody`}>
                    {answerGroup?.map((answer: AnswerGroup, answerIndex: number) => {
                        if(answer.isExam === true){
                            indexA +=1;
                        }else{
                            indexE +=1;
                        }
                        return (
                            <tr key={answer.key} id={answer.key}>
                                <td align="center"
                                    className={`${listExamError.length > 3 || listAError.length < 1 ? 'error' : ''}`}
                                >
                                    <>
                                        <Toggle
                                            size="md"
                                            checked={answer.isExam}
                                            // defaultChecked={answer.isExam}
                                            checkedChildren={t('common')['question']}
                                            unCheckedChildren={t('common')['example']}
                                        onChange={()=>handleToggle(answerIndex,answer.isExam)}
                                        />
                                    </>
                                </td>
                                <td
                            className={`${(listImageError.includes(answerIndex.toString()) || listImageFilesError.includes(answerIndex.toString())) ? 'error' : ''}`}
                                >
                                    <div className="image-input-tf4">
                                        <ImageInputTF4
                                            url={answer.imageStr}
                                            onChangeFile={onChangeImage.bind(null, answerIndex)}
                                        isError={listImageError.includes(answerIndex.toString())}
                                        labelDefault={`${answer.isExam === true ? t(t('create-question')['question-image'], [`${indexA + 1}`]) : t(t('create-question')['image-example-with-num'], [`${indexE + 1}`])}`}
                                            viewMode={viewMode}
                                        />
                                    </div>
                                </td>
                            <td className={`input-placeholder ${listError.includes(answerIndex.toString()) ? 'error' : ''}`}>
                                    <ContentEditable
                                        id={`${groupKey}_${answerIndex}`}
                                        spellCheck={false}
                                        className={`div-input ${groupKey}`}
                                        html={convertStrToHtml(answer.answerStr)}
                                        contentEditable="true"
                                        onChange={onChangeText.bind(null, answerIndex)}
                                        onBlur={onBlurText.bind(null, answerIndex)}
                                        onPaste={onPaste}
                                        onKeyDown={(event)=>{
                                            if (event.key === 'Enter') {
                                                event.preventDefault()
                                            }
                                        }}
                                    />
                                    <span className="placeholder">
                                    {`${answer.isExam === true ? `${t('common')['question']} ${indexA+1}` : `${t('common')['example']} ${indexE+1}`}`}
                                    </span>
                                </td>
                                <td
                                    align="center"
                                    className={
                                    listCError.includes(answerIndex.toString()) ? 'error' : ''
                                    }
                                >
                                    <input
                                        type="radio"
                                        data-value="T"
                                        name={`${groupKey}_${answerIndex}`}
                                        checked={answer.correctStr === 'T'}
                                        onChange={onChangeIsCorrect.bind(null, answerIndex)}
                                    />
                                </td>
                                <td
                                    align="center"
                                    className={
                                    listCError.includes(answerIndex.toString()) ? 'error' : ''
                                    }
                                >
                                    <input
                                        type="radio"
                                        data-value="F"
                                        name={`${groupKey}_${answerIndex}`}
                                        checked={answer.correctStr === 'F'}
                                        onChange={onChangeIsCorrect.bind(null, answerIndex)}
                                    />
                                </td>
                                <td align="right">
                                    <img
                                        onClick={onDeleteAnswer.bind(null, answerIndex)}
                                        className="ic-action"
                                        src="/images/icons/ic-trash.png"
                                        alt=""
                                    />
                                </td>
                            </tr>
                    )})}
                    {!viewMode && answerGroup.length < 25 && (<tr className="question-table-tf4-action">
                        <td colSpan={1} align="center">
                            <label className={`add-new-answer-tf4 ${answerGroup.length === 0 ? 'error' : ''}`} onClick={onAddAnswer}>
                                <img
                                    src="/images/icons/ic-plus-sub.png"
                                    alt=""
                                    width={17}
                                    height={17}
                                />{' '}
                                {t('create-question')['add-question']}
                            </label>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>)}
                </tbody>
            </table>
            {err && (
                <div className="render_error">
                    {error && <span>{error}</span>}
                    {listImageFilesError.length > 0 && <span>* {t('question-validation-msg')['list-images']}</span>}
                </div>
            )}
        </div>
    );
}

export default QuestionTableTF4;
type PropsType = {
    labelDefault?: string
    isError?: boolean
    url: string
    onChangeFile?: (files: FileList) => void
    viewMode?: boolean
}
function ImageInputTF4({ url, onChangeFile, isError, labelDefault, viewMode }: PropsType) {
    const [imageUrl, setImageUrl] = useState(url)
    const fileRef = useRef(null)
    const src = imageUrl.startsWith('blob') ? url : `/upload/${url}`
    return (
        <div className={`image-tf4 ${imageUrl !== '' ? 'has-data' : ''}`}


        >
            <div className={`image ${isError ? 'error' : ''}`}  onClick={() => fileRef.current.dispatchEvent(new MouseEvent('click'))}>
                <input
                    ref={fileRef}
                    type={"file"}

                    accept=".png,.jpeg,.jpg"
                    onChange={(e) => {
                        if (fileRef.current.files.length > 0) {
                            isError = null
                            setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                            onChangeFile && onChangeFile(e.target.files)
                        }
                    }}
                />
                {src && url? (
                    <img
                        src={src}
                        alt=''

                    />
                ) : (
                    <div className="image-default">
                        <img
                            src="/images/icons/ic-image.png"
                            alt=""

                        />
                        <span className="label-image">{labelDefault}</span>
                    </div>

                )}

            </div>
            {!viewMode && src && url && <img
                className="image-input-tf4___isDelete"
                src="/images/icons/ic-remove-opacity.png" 
                alt=""
                onClick={(event)=>{
                    fileRef.current.value = ""
                    event.preventDefault();
                    event.stopPropagation();
                    onChangeFile(null)
                }}
            />}

        </div>
    )
}

type AnswerGroup = {
    key:string,
    imageStr:string,
    answerStr:string,
    correctStr:string,
    isExam:boolean,
}