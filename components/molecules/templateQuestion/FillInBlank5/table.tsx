import { useEffect, useRef, useState,useCallback,KeyboardEvent } from 'react'

import ContentEditable from 'react-contenteditable'
import useTranslation from "@/hooks/useTranslation";

type PropType = {
  correctStr: string
  styleBold?: boolean
  className?: string
  errorStr?: string
  onChange: (data: any) => void
  viewMode?: boolean
}

export default function FillInBlank5Table({
  correctStr,
  className = '',
  errorStr,
  onChange,
  viewMode,
}: PropType) {
  const {t} = useTranslation()
  const [answers, setAnswers] = useState<Answer[]>(
    correctStr?.split('#')?.map((m, mIndex) => ({
      id: `${Math.random()}_${mIndex}`,
      texts: m.split('%/%').map((g, gIndex) => ({
        id: `${Math.random()}_${gIndex}`,
        text: g,
      })),
    })),
  )

  const isInit = useRef(true)

  useEffect(() => {
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        onChange({
          correct_answers: answers
            ?.map((m) => m.texts.map((g) => (g.text ?? '').trim()).join('%/%'))
            .join('#'),
        })
      }
    }
  }, [answers])

  const onAddAnswer = () => {
    if (answers) {
      setAnswers([
        ...answers,
        {
          id: `${Math.random()}_${answers?.length ?? 1}`,
          texts: [{ id: `${Math.random()}_1}`, text: '' }],
        },
      ])
    } else {
      setAnswers([
        {
          id: `${Math.random()}_${answers?.length ?? 1}`,
          texts: [{ id: `${Math.random()}_1}`, text: '' }],
        },
      ])
    }

    setTimeout(() => {
      const el = document.querySelector(
        `table tbody tr:nth-child(${(answers?.length ?? 0) + 1}) td input`,
      ) as HTMLInputElement
      el?.focus()
    }, 0)
  }

  const onChangeText = (answerIndex: number, index: number, event: any) => {
    const el: HTMLInputElement = event.currentTarget
    answers[answerIndex].texts[index].text = el.value
    setAnswers([...answers])
  }
  const onBlurText = (answerIndex: number, index: number, event: any) => {
    const el: HTMLInputElement = event.currentTarget
    answers[answerIndex].texts[index].text = el.value = el.value.trim()
    // setAnswers([...answers])
  }

  const onDeleteAnswer = (answerIndex: number, index: number) => {
    answers[answerIndex].texts.splice(index, 1)
    setAnswers(answers.filter((m) => m.texts.length > 0))
  }

  const onAddExtraAnswer = (answerIndex: number) => {
    answers[answerIndex].texts.push({
      id: `${Math.random()}_${answers[answerIndex].texts.length}}`,
      text: '',
    })
    setAnswers([...answers])

    setTimeout(() => {
      const el = document.querySelector(
        `table tbody tr:nth-child(${answerIndex + 1}) td:nth-child(${
          answers[answerIndex].texts.length + 1
        }) input`,
      ) as HTMLInputElement
      el?.focus()
    }, 0)
  }

  const maxLength =
    answers?.map((m) => m.texts.length).sort((a, b) => b - a)[0] ?? 1

  const listError = errorStr.split('#').map((m) => m.split(','))
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
      return
    }
  }, [])
  return (
    <div className={className} style={{ position: 'relative' }}>
      <table className={`question-table fill-in-blank-5`}>
        <thead>
          <tr>
            <th></th>
            <th colSpan={maxLength} align="left">
              {t('create-question')['questions-answer-keys']}
            </th>
          </tr>
        </thead>
        <tbody>
          {answers?.map((answer: Answer, answerIndex: number) => (
            <tr key={answer.id}>
              <td align="center">{`(${answerIndex + 1})`}</td>
              {Array.from({ length: maxLength }).map((_, gIndex) => (
                <td
                  key={answer.texts[gIndex]?.id ?? `${Math.random()}`}
                  className={`td-input ${
                    listError[answerIndex]?.includes(gIndex.toString())
                      ? 'error'
                      : ''
                  }`}
                >
                  {answer.texts[gIndex] !== undefined && (
                    <>
                      <input
                        type="text"
                        defaultValue={answer.texts[gIndex].text}
                        onChange={onChangeText.bind(null, answerIndex, gIndex)}
                        onBlur={onBlurText.bind(null, answerIndex, gIndex)}
                        onKeyDown={onKeyDown}
                      />
                      <div className="action">
                        <img
                          src="/images/icons/ic-plus-gray.png"
                          className="ic-action"
                          onClick={onAddExtraAnswer.bind(null, answerIndex)}
                          alt="add-extra-answer-icon"
                        />
                        <img
                          className="ic-action"
                          src="/images/icons/ic-trash.png"
                          onClick={onDeleteAnswer.bind(
                            null,
                            answerIndex,
                            gIndex,
                          )}
                          alt="delete-answer-icon"
                        />
                      </div>
                    </>
                  )}
                </td>
              ))}
            </tr>
          ))}
          {!viewMode && (
          <tr className="action">
            <td></td>
            <td colSpan={maxLength} align="left">
              <label className="add-new-answer" onClick={onAddAnswer}>
                <img
                  src="/images/icons/ic-plus-sub.png"
                  width={17}
                  height={17}
                  alt="add-new-answer-icon"
                />{' '}
                {t('create-question')['add-answer']}
              </label>
            </td>
          </tr>
          )}
        </tbody>
      </table>
      <div className="border-error"></div>
    </div>
  )
}

type Answer = {
  id: string
  texts: AnswerText[]
}
type AnswerText = {
  id: string
  text: string
}
