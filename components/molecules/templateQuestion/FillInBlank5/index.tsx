import { useEffect, useRef, useState } from 'react'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'
import browserFile from 'lib/browserFile'

import AudioInput from '../AudioInput'
import ImageInput from '../ImageInput'
import FillInBlank5Table from './table'
import useTranslation from "@/hooks/useTranslation";

export default function FillInBlank5({
  question,
  register,
  setValue,
  errors = {},
  viewMode,
}: QuestionPropsType) {
  const { t } = useTranslation()
  const [imgUrl, setImgUrl] = useState(question?.image || '')

  useEffect(() => {
    register('image', {
      maxLength: {
        value: 2000,
        message: t(t('question-validation-msg')['image-max-length'], ['2000']),
      },
      required: t('question-validation-msg')['image-required'],
      value: question?.image,
    })
    register('audio_file',{
      validate: {
        acceptedFormats: async(file) => {
          if (!file) {
              return true
            }
            if(file.size ==0){
              return t('question-validation-msg')['audio-link-error']
            }
            if(!['audio/mp3','audio/mpeg'].includes(file?.type)){
              return t('question-validation-msg')['audio-type']
            }
            if(!await browserFile.checkFileExist(file)){
              return t('question-validation-msg')['audio-link-error']
            }
            return true;
        },
      }
    })
    register('image_file', {
      validate: {
        acceptedFormats: async(file) => {
          // check format if has file
          if (!file) {
            return true
          }
          if(file.size ==0){
            return t('question-validation-msg')['image-deleted']
          }
          if(!['image/jpeg', 'image/png'].includes(file?.type)){
            return t('question-validation-msg')['image-file']
          }
          if(!await browserFile.checkFileExist(file)){
            return t('question-validation-msg')['image-deleted']
          }
          return true;
        },
      },
    })
    register('audio', {
      maxLength: {
        value: 2000,
        message: t(t('question-validation-msg')['audio-max-length'],['2000']),
      },
      required: t('question-validation-msg')['audio-required'],
      value: question?.audio,
    })
    register('audio_script', {
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
      },
      value: question?.audio_script,
    })
    register('correct_answers', {
      validate: (value: string) => {
        if ((value ?? '') === '') return ''
        const errs = value.split('#').map((m, mIndex) =>
          m
            .split('%/%')
            .map((g, gIndex) => {
              if (g === '') return gIndex
              return null
            })
            .filter((m) => m != null),
        )

        return errs.some((m) => m.length > 0)
          ? errs.map((m) => m.join()).join('#')
          : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [])

  const onChangeAudioScript = (data: any) => {
    setValue('audio_script', data)
  }

  const onChange = (data: any) => {
    setValue('correct_answers', data.correct_answers)
  }

  const onChangeFile = (files: FileList) => {
    setValue('audio_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('audio', urlAudio, {
      shouldValidate: true,
    })
  }

  const onChangeImageFile = (files: FileList) => {
    if (files && files[0]) {
      setValue('image_file', files[0])
      const urlImage = URL.createObjectURL(files[0])
      setValue('image', urlImage)
      setImgUrl(urlImage)
    } else {
      setValue('image_file', null)
      setValue('image', '')
      setImgUrl('')
    }
  }

  return (
    <div className="m-fill-in-blank">
      <AudioInput
        url={question?.audio}
        script={question?.audio_script}
        isError={errors['audio'] || errors['audio_file']}
        onChangeFile={onChangeFile}
        onChangeScript={onChangeAudioScript}
        style={{ height: '373px', width:"50%" }}
        viewMode={viewMode}
      />
      <div style={{  width:"50%"}}>
        <TextAreaField
          setValue={setValue}
          label={t('create-question')['question-description']}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          style={{ height: '6rem' }}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          viewMode={viewMode}
          specialChar={['#','*']}
        />
        <ImageInputFB5
          url={imgUrl}
          onChangeFile={onChangeImageFile}
          isError={errors['image'] || errors['image_file']}
          viewMode={viewMode}
        />
        <FillInBlank5Table
          className={`form-input-stretch ${
            errors['correct_answers']?.message === '' ? 'error' : ''
          }`}
          correctStr={question?.correct_answers}
          errorStr={errors['correct_answers']?.message ?? ''}
          onChange={onChange}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}

type PropsType = {
  labelDefault?: string
  isError?: any
  url: string
  onChangeFile?: (file: FileList) => void
  viewMode?: boolean
}
function ImageInputFB5({
  url,
  onChangeFile,
  isError,
  viewMode = false,
}: PropsType) {
  const {t} = useTranslation()
  const [imageUrl, setImageUrl] = useState(url ?? '')
  const fileRef = useRef(null)
  const src = imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`
  return (
    <div
      className={`box-image-input-fb5 ${imageUrl !== '' ? 'has-data' : ''} ${
        isError ? 'error' : ''
      }`}
    >
      <div className="image-section">
        <div className="image-input-origin">
          {src && url ? (
            <>
              <img
                className="image-upload"
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                src={src}
                alt="image-upload"
                style={{ cursor: 'pointer' }}
              />
              {!viewMode && src && url && (
                <img
                  className="image-delete-icon"
                  src="/images/icons/ic-remove-opacity.png"
                  alt=""
                  onClick={(event) => {
                    fileRef.current.value = ''
                    event.preventDefault()
                    event.stopPropagation()
                    onChangeFile(null)
                  }}
                />
              )}
            </>
          ) : (
            <>
              <div
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  cursor: 'pointer',
                }}
              >
                <img
                  className="icon"
                  src="/images/icons/ic-image.png"
                  alt="delete-img-icon"
                />
                {t('create-question')['image']}
              </div>
            </>
          )}
          <input
            ref={fileRef}
            type="file"
            style={{ display: 'none' }}
            accept=".png,.jpeg,.jpg"
            onChange={(e) => {
              if (fileRef.current.files.length > 0) {
                setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                onChangeFile && onChangeFile(e.target.files)
              }
            }}
          />
        </div>
      </div>
    </div>
  )
}