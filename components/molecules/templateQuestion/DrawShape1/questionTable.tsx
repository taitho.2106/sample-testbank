import useTranslation from '@/hooks/useTranslation'
import { COLOR_SHAPE } from '@/interfaces/constants'
import ShapeComponent from 'components/atoms/shape/index'
import { useState, useRef, ChangeEvent, useEffect } from 'react'

import ContentEditable from 'react-contenteditable'
import { SelectPicker, Toggle } from 'rsuite'
import Guid from 'utils/guid'

type PropType = {
  setValue?: any
  styleBold?: boolean
  className?: string
  errorStr?: string
  register?: any
  onChange?: (data: any) => void
  errors?: any
  question?: any
  viewMode?: boolean
  isImage?: boolean
}
type ShapeColorError = {
  shapeErr: boolean
  colorErr: boolean
}
export default function QuestionTable({
  question,
  register,
  errors = {},
  setValue,
  className = '',
  errorStr = '',
  onChange,
  viewMode = false,
}: PropType) {
  const {t} = useTranslation()
  const listAnswer = (question?.answers ?? '**#**#**').split('#') ?? []
  const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
    listAnswer.map((answer: string, answerIndex: number) => {
      const list: any[] = answer.split('*')
      const isExample = list[0].startsWith('ex|')
      return {
        key: Guid.newGuid(),
        shape: list[1],
        color: list[2],
        isExam: isExample,
      }
    }),
  )
  const errorShapeColorRef = useRef<ShapeColorError[]>(
    answerGroup.map((m) => {
      const item: ShapeColorError = { shapeErr: false, colorErr: false }
      return item
    }),
  )
  const answerGroupRef = useRef(answerGroup)
  const imageFiles = useRef(answerGroup.map((m) => null))

  const groupKey = useRef(Math.random()).current
  const isInit = useRef(false)
  useEffect(() => {
    answerGroupRef.current = answerGroup
    if (isInit.current) {
      isInit.current = false
    } else {

      if (typeof onChange === 'function') {
        const dataSend = {
          answers: answerGroup
            .map((m) => {
              if (!m.isExam) {
                return `*${m.shape || ''}*${m.color || ''}`
              } else {
                return `ex|*${m.shape || ''}*${m.color || ''}`
              }
            })
            .join('#'),
        }
        onChange(dataSend)
      }
    }
    let passData = true
    if (answerGroup.length == 0) passData = false
    if (answerGroup.filter((m) => m.isExam).length > 1) passData = false
    if (answerGroup.filter((m) => !m.isExam).length == 0) passData = false
    setValue('passData', passData)
  }, [answerGroup])
  const onAddAnswer = () => {
    const newAnswer = {
      key: Guid.newGuid(),
      color: '',
      shape: '',
      isExam: false,
    }
    if (answerGroup) {
      setAnswerGroup([...answerGroup, newAnswer])
    } else {
      setAnswerGroup([newAnswer])
    }
    imageFiles.current.push(null)
    setTimeout(() => {
      document.getElementById(`${groupKey}_${answerGroup?.length ?? 0}`)?.focus()
    }, 0)
  }
  const onDeleteAnswer = (index: number) => {
    const ans: any[] = answerGroup.filter((m: any, mIndex: number) => mIndex !== index)
    setAnswerGroup(ans)
  }

  const listError = errorStr.split(',')
  const [error, setError] = useState('')
  const renderError = () => {
    if (listExamError.length > 1) {
      return setError(`* ${t(t('question-validation-msg')['example-allow-total'],['1'])}`)
    }
    if (listAError.length < 1) {
      return setError(`* ${t(t('question-validation-msg')['question-fb-required'],['1'])}`)
    }
    return setError('')
  }
  const handleToggle = (id: number, value: boolean) => {
    answerGroup[id].isExam = !value
    setAnswerGroup([...answerGroup])
  }
  const onChangeShape = (id: number, value: string) => {
    answerGroup[id].shape = value
    setAnswerGroup([...answerGroup])
  }
  const onChangeColor = (id: number, value: string) => {
    answerGroup[id].color = value
    setAnswerGroup([...answerGroup])
  }
  const listExamError = answerGroup.filter((item: any) => item.isExam === true)
  const listAError = answerGroup.filter((item: any) => item.isExam === false)
  let indexE = -1
  let indexA = -1
  const createDataShapeDropout = (shapeSelected: string) => {
    return [
      {
        value: 'circle',
        label: (
          <div className="--item-dropout-ds1">
            <ShapeComponent.CircleShape fill="#d4dde7" height={18} strokeWidth={shapeSelected == 'circle' ? 1 : 0} />
          </div>
        ),
      },
      {
        value: 'triangle',
        label: (
          <div className="--item-dropout-ds1">
            <ShapeComponent.TriangleShape
              fill="#d4dde7"
              height={18}
              strokeWidth={shapeSelected == 'triangle' ? 1 : 0}
            />
          </div>
        ),
      },
      {
        value: 'square',
        label: (
          <div className="--item-dropout-ds1">
            <ShapeComponent.SquareShape fill="#d4dde7" height={18} strokeWidth={shapeSelected == 'square' ? 1 : 0} />
          </div>
        ),
      },
      {
        value: 'star',
        label: (
          <div className="--item-dropout-ds1">
            <ShapeComponent.StarShape fill="#d4dde7" height={18} strokeWidth={shapeSelected == 'star' ? 1 : 0} />
          </div>
        ),
      },
      {
        value: 'rectangle',
        label: (
          <div className="--item-dropout-ds1">
            <ShapeComponent.RectangleShape
              fill="#d4dde7"
              height={18}
              strokeWidth={shapeSelected == 'rectangle' ? 1 : 0}
            />
          </div>
        ),
      },
      {
        value: 'oval',
        label: (
          <div className="--item-dropout-ds1">
            <ShapeComponent.OvalShape fill="#d4dde7" height={18} strokeWidth={shapeSelected == 'oval' ? 1 : 0} />
          </div>
        ),
      },
    ]
  }
  const createDataColorDropout = (colorSelected: string) => {
    return COLOR_SHAPE.map((m) => {
      return {
        value: m.color,
        label: (
          <div className="--item-dropout-ds1"><ShapeComponent.CircleShape fill={`#${m.color}`} height={18} strokeWidth={colorSelected == m.color ? 1 : 0} /></div>
        ),
      }
    })
  }
  const renderShapeSelected = (shape: string, color: string) => {
    const height = 32
    switch (shape) {
      case 'circle':
        return  <ShapeComponent.CircleShape fill={color} height={height} />
      case 'triangle':
        return <ShapeComponent.TriangleShape fill={color} height={height} />
      case 'square':
        return <ShapeComponent.SquareShape fill={color} height={height} />
      case 'star':
        return <ShapeComponent.StarShape fill={color} height={height} />
      case 'rectangle':
        return <ShapeComponent.RectangleShape fill={color} height={height} />
      case 'oval':
        return <ShapeComponent.OvalShape fill={color} height={height} />
    }
  }
  let passData = true
  if (answerGroup.length == 0) passData = false
  if (answerGroup.filter((m) => m.isExam).length > 1) passData = false
  if (answerGroup.filter((m) => !m.isExam).length == 0) passData = false
  const listShapeErr = []
  for (let i = 0; i < answerGroup.length; i++) {
    const item = answerGroup[i]
    const itemErr: ShapeColorError = {
      shapeErr: false,
      colorErr: false,
    }
    listShapeErr.push(itemErr)
    if (!item.shape) continue
    const count = answerGroup.filter((m) => m.shape == item.shape).length
    if (count > 3) {
      itemErr.shapeErr = true
      passData = false
    }
    if (answerGroup.filter((m) => m.shape == item.shape && m.color && m.color == item.color).length > 1) {
      itemErr.colorErr = true
      passData = false
    }
  }
  errorShapeColorRef.current = listShapeErr
  setValue('passData', passData)
  useEffect(() => {
    renderError()
  }, [listExamError, listAError])
  return (
    <div className={`question-table-ds1 ${className}`} style={{ position: 'relative' }}>
      <table className={`question-table question-table-ds1-table`}>
        <thead className={`question-table-ds1-thead`}>
          <tr>
            <th
              dangerouslySetInnerHTML={{
                __html: t('create-question')['select-question-example']
              }}
            ></th>
            <th align="left">{t('create-question')['image-options']}</th>
            <th>{t('create-question')['question-example-image-1']}</th>
            <th align="center"></th>
          </tr>
        </thead>
        <tbody className={`question-table-ds1-tbody`}>
          {answerGroup?.map((answer: AnswerGroup, answerIndex: number) => {
            const errorMessage = listError[answerIndex]
            const shapeError =
              (errorMessage && errorMessage[0] == '0') || errorShapeColorRef.current[answerIndex]?.shapeErr
            const colorError =
              (errorMessage && errorMessage[1] == '0') || errorShapeColorRef.current[answerIndex]?.colorErr
            if (!answer.isExam) {
              indexA += 1
            } else {
              indexE += 1
            }
            return (
              <tr key={answer.key} id={answer.key}>
                <td align="center" className={`${listExamError.length > 1 && answer.isExam ? 'error' : ''}`}>
                <Toggle
                     className="__switcher"
                      size="md"
                      checked={!answer.isExam}
                      checkedChildren={t('common')['question']}
                      unCheckedChildren={t('common')['example']}
                      onChange={() => handleToggle(answerIndex, answer.isExam)}
                    />
                </td>
                <td>
                  <div style={{ position: 'relative' }} className={`${shapeError ? 'error' : ''}`}>
                    <SelectPicker
                      onChange={(value) => onChangeShape(answerIndex, value)}
                      data={createDataShapeDropout(answer.shape)}
                      appearance="default"
                      placeholder=" "
                      value={answer.shape}
                      searchable={false}
                      cleanable={false}
                      size="xs"
                    />
                    {!answer.shape && (
                      <div className="dropout-label" style={{ left: 8 }}>
                        <ShapeComponent.DefaultShape fill={answer.color ? `#${answer.color}` : '#000000'} height={20} />
                      </div>
                    )}
                  </div>
                  <div
                    style={{ marginLeft: '1.6rem', position: 'relative' }}
                    className={`${colorError ? 'error' : ''}`}
                  >
                    <SelectPicker
                      onChange={(value) => onChangeColor(answerIndex, value)}
                      data={createDataColorDropout(answer.color)}
                      appearance="default"
                      placeholder=" "
                      value={answer.color}
                      searchable={false}
                      cleanable={false}
                      size="xs"
                    />
                    <div className="dropout-label">
                      <ShapeComponent.IcFillShape fill={answer.color ? `#${answer.color}` : ''} height={20} />
                    </div>
                  </div>
                </td>
                <td className={`${listError.includes(answerIndex.toString()) ? 'error' : ''}`}>
                  <div>
                    {answer.shape && answer.color ? (
                      <div className="shape-preview">{renderShapeSelected(answer.shape, `#${answer.color}`)}</div>
                    ) : (
                      <>
                        <div className="shape-preview"></div>
                        <div>
                          <span>{`${answer.isExam ? t(t('create-question')['example-image-with-num'],[`${indexE + 1}`]) : t(t('create-question')['question-image-with-num'],[`${indexA + 1}`])}`}</span>
                        </div>
                      </>
                    )}
                  </div>
                </td>
                <td align="right">
                  <img
                    onClick={onDeleteAnswer.bind(null, answerIndex)}
                    className="ic-action"
                    src="/images/icons/ic-trash.png"
                    alt="delete"
                  />
                </td>
              </tr>
            )
          })}
          {!viewMode && (
            <tr className="question-table-ds1-action">
              <td colSpan={1} align="center">
                <label
                  className={`add-new-answer-ds1 ${answerGroup.length === 0 ? 'error' : ''}`}
                  onClick={onAddAnswer}
                >
                  <img src="/images/icons/ic-plus-sub.png" alt="add" width={17} height={17} /> {t('create-question')['add-question']}
                </label>
              </td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          )}
        </tbody>
      </table>
      <div className="render_error">
        {error && <span>{error}</span>}
        {errorShapeColorRef.current.filter((m) => m.shapeErr).length > 0 && (
          <span>* {t('question-validation-msg')['validate-shape']}</span>
        )}
        {errorShapeColorRef.current.filter((m) => m.colorErr).length > 0 && (
          <span>* {t('question-validation-msg')['validate-color']}</span>
        )}
      </div>
    </div>
  )
}

type PropsType = {
  labelDefault?: string
  isError?: boolean
  url: string
  onChangeFile?: (files: FileList) => void
  viewMode?: boolean
}

type AnswerGroup = {
  key: string
  shape: string
  color: string
  isExam: boolean
}
