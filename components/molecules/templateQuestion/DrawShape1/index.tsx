import { useEffect } from 'react'

import { ContentField } from 'components/atoms/contentField'
import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import AudioInput from '../AudioInput'
import QuestionTable from './questionTable'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation'

function DrawShape1({
  question,
  register,
  setValue,
  isAudio = false,
  isReading = false,
  errors = {},
  viewMode = false,
  isImage = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  useEffect(() => {
    register('audio_file', {
      validate: {
        acceptedFormats: async (file) => {
          // check format if has file
          if (!file) {
            return true
          }
          if (file.size == 0) {
            return t('question-validation-msg')['audio-link-error']
          }
          if (!['audio/mp3', 'audio/mpeg'].includes(file?.type)) {
            return t('question-validation-msg')['audio-type']
          }
          if (!(await browserFile.checkFileExist(file))) {
            return t('question-validation-msg')['audio-link-error']
          }
          return true
        },
      },
    })
    register('passData', {
      validate: (value) => {
        return value
      },
    })
    register('audio', {
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['audio-max-length'], ['10000']),
      },
      required: t('question-validation-msg')['audio-required'],
      value: question?.audio,
    })
    register('audio_script', {
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['audio-script-max-length'], ['10000']),
      },
      value: question?.audio_script,
    })
    register('answers', {
      validate: (value: string) => {
        console.log(' answers=----value', value)
        let hasErr = false
        const errs: any[] = (value ?? '').split('#').map((m, mIndex) => {
          if (!m) {
            hasErr = true
            return '00'
          }
          const answerItems = m.split('*')
          let resutl = ''
          if (answerItems[1] != '') {
            resutl += '1'
          } else {
            resutl += '0'
            hasErr = true
          }
          if (answerItems[2] != '') {
            resutl += '1'
          } else {
            resutl += '0'
            hasErr = true
          }
          return resutl
        })
        console.log('hasErr---', hasErr)
        return errs.length > 0 && hasErr ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '#',
    })
  }, [])

  const onChangeAudioScript = (data: any) => {
    setValue('audio_script', data)
  }

  const onDataChange = (data: any) => {
    console.log('data---answer', data.answers)
    setValue('answers', data.answers)
  }

  const onChangeFile = (files: FileList) => {
    if (files.length > 0) {
      setValue('audio_file', files[0])
      const urlAudio = URL.createObjectURL(files[0])
      setValue('audio', urlAudio)
    }
  }
  return (
    <div className="m-draw-shape-ds1" style={{ display: 'flex' }}>
      <div className="form-input-stretch" style={{ position: 'relative', padding: '0 1rem' }}>
        <TextAreaField
          label={t('create-question')['question-description']}
          setValue={setValue}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          style={{ height: '4.5rem' }}
          className={errors['question_description'] ? 'error' : ''}
          viewMode={viewMode}
          specialChar={['#', '*']}
        />
      </div>
      <div className="form-input-stretch" style={{ display: 'flex' }}>
        <AudioInput
          url={question?.audio}
          script={question?.audio_script}
          isError={errors['audio'] || errors['audio_file']}
          onChangeFile={onChangeFile}
          onChangeScript={onChangeAudioScript}
          viewMode={viewMode}
        />
        <div className="content-table">
          <QuestionTable
            className={`${errors['answers']?.message === '' ? 'error' : ''}`}
            errors={errors}
            setValue={setValue}
            register={register}
            question={question}
            errorStr={errors['answers']?.message ?? ''}
            onChange={onDataChange}
            viewMode={viewMode}
            isImage={isImage}
          />
        </div>
      </div>
    </div>
  )
}

export default DrawShape1
