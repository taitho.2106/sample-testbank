import { useEffect } from 'react'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import MultiChoiceGroupQuestion from './question'
import browserFile from 'lib/browserFile'
import useTranslation from "@/hooks/useTranslation";

export default function MultiChoiceGroupMC8({
    question,
    register,
    setValue,
    errors = {},
    viewMode,
}: QuestionPropsType) {
    const { t } = useTranslation()
    useEffect(() => {
        register('question_text', {
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
            },
            value: question?.question_text,
        })
        register('answers', {
            validate: (value: string) => {
                const errs = (value ?? '').split('#').map((m) => m
                    .split('*')
                    .map((g, gIndex) => {
                        if (g === '') return gIndex
                        return null
                    })
                    .filter((m) => m !== null),
                )
                return errs.some((m) => m.length > 0)
                    ? errs.map((m) => m.join()).join('#')
                    : true
            },
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
            },
            value: question?.answers ?? '***',
        })
        register('correct_answers', {
            validate: (value: string) => {
                const errs = (value ?? '')
                    .split('#')
                    .map((m, mIndex) => {
                        if (m=== '' || m === '-1') return mIndex
                        return null
                    })
                    .filter((m) => m !== null)
                return errs.length > 0 ? errs.join() : true
            },
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
            },
            value: question?.correct_answers,
        })
        register('mc8_image_files', {
            validate: async(files: FileList[]) => {
                if (!files) {
                  return true
                }
                const arrFiles = files.length
                const err = Array.from(Array(arrFiles), () => [])
                const errArr = Array.from(Array(arrFiles), () => '')
                let i =-1;
                let isFileDeleted = false;
                for await (const file of Array.from(files)){
                    i+=1;
                    let mIndex = -1;
                    for await (const item of Array.from(file)){
                        mIndex+=1;
                        if(!item){
                            err[i].push(null);
                            continue;
                        }
                        if (item.size == 0) {
                            err[i].push(mIndex.toString())
                          } else if (!['image/jpeg', 'image/png'].includes(item?.type)) {
                              err[i].push(mIndex.toString())
                            } else {
                                if(!await browserFile.checkFileExist(item)){
                                    isFileDeleted = true;
                                    err[i].push(mIndex.toString())
                                }else{
                                    err[i].push(null)
                                }
                          }
                    }
                    errArr[i] = err[i].filter((m) => m != null).join('*')
                }
                const temp = errArr.filter((m: string) => m !== '')
                const errStr = errArr.join('#')
                if (errStr === '') return true
                return temp.length > 0 ? `${errStr}${isFileDeleted?"#deleted":""}` : true
              },
        })
    }, [])

    const onMCChange = (dataV: any) => {
        setValue('answers', dataV.answers)
        setValue('mc8_image_files',dataV.image_files)
        setValue('correct_answers', dataV.correctStrs)
        setValue('question_text', dataV.questions)
    }

    const listErrFileImage = errors['mc8_image_files']?.message?.split('#')
    return (
        <div className="m-multi-choice-mc8" style={{ display: 'flex',width:'100%' }}>
            <div style={{ marginBottom:'2rem', width:'100%' }}>
                <div className="form-input-stretch" style={{ position: 'relative' }}>
                    <TextAreaField
                        label={t('create-question')['question-description']}
                        setValue={setValue}
                        register={register('parent_question_description', {
                            required: t('question-validation-msg')['description-required'],
                            maxLength: {
                                value: 2000,
                                message: t(t('question-validation-msg')['description-max-length'], ['2000']),
                            },
                        })}
                        defaultValue={question?.parent_question_description}
                        style={{ height: '4.5rem' }}
                        className={errors['parent_question_description'] ? 'error' : ''}
                        viewMode={viewMode}
                        specialChar={['#','*']}
                    />
                </div>
                <MultiChoiceGroupQuestion
                    questions={question?.question_text}
                    answers={question?.answers}
                    corrects={question?.correct_answers}
                    onQuestionChange={onMCChange}
                    errorStr={errors['answers']?.message ?? ''}
                    correctErrorStr={errors['correct_answers']?.message ?? ''}
                    questionErrorStr={errors['question_text']?.message ?? ''}
                    listErrFileImage={listErrFileImage || []}
                    viewMode={viewMode}
                    setValue={setValue}
                    register={register}
                />
            </div>
        </div>
    )
}