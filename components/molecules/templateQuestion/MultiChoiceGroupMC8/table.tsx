import { useEffect, useRef, useState } from 'react'

import Guid from 'utils/guid'
import useTranslation from "@/hooks/useTranslation";

type PropType = {
    answerStr: string
    correctStr: string
    styleBold?: boolean
    className?: string
    errorStr?: string
    onChange: (data: any) => void
    viewMode?: boolean
    image_files?:any[]
    errFiles:any
}

export default function MultiChoiceTableMC8({
    answerStr,
    correctStr,
    styleBold = false,
    className = '',
    errorStr = '',
    onChange,
    viewMode,
    image_files,
    errFiles
}: PropType) {
    const { t } = useTranslation()
    const answerArr =  answerStr?.split('*')
    const [answers, setAnswers] = useState<Answers[]>(
        answerArr?.map((m: string) => {
            return ({
            answer: m,
            isCorrect: answerArr[parseInt(correctStr)] === m,
        })}),
    )

    // const groupKey = useRef(Math.random()).current
    const isInit = useRef(true)
    const imageFiles = useRef(answers.map((m:any) => null))
    useEffect(() => {
        if (isInit.current) {
            isInit.current = false
        } else {
            if (typeof onChange === 'function') {
                onChange({
                    answers: answers.map((m:any) => m.answer).join('*'),
                    correct_answers: answers.findIndex((m:any) => m.isCorrect).toString(),
                })
            }
        }

        setTimeout(() => {
            Array.from(document.getElementsByClassName(`${Guid.newGuid()}`)).forEach(
                (el) => {
                    const div = el as HTMLDivElement
                    div.style.width = `${div.offsetWidth}px`
                },
            )
        }, 0)
    }, [answers])

    const onAddAnswer = () => {
        if (answers) {
            setAnswers([...answers, { answer: '', isCorrect: false}])
        } else {
            setAnswers([{ answer: '', isCorrect: false }])
        }
    }

    const onChangeIsCorrect = (index: number) => {
        answers.forEach((answer: Answers, answerIndex: number) => {
            answer.isCorrect = index === answerIndex
        })
        setAnswers([...answers])
    }
    const onDeleteAnswer = (index: number) => {
        answers.splice(index, 1)
        image_files.splice(index,1)
        imageFiles.current.splice(index,1)
        setAnswers([...answers])
    }
    const onChangeImage = (index: number, files: FileList) => {
        if (files && files[0]) {
            const urlImage = URL.createObjectURL(files[0])
            answers[index].answer = urlImage
            imageFiles.current[index] = files[0]
            setAnswers([...answers])
            image_files[index] = imageFiles.current[index]
        } else {
            answers[index].answer = ''
            imageFiles.current[index] = null
            image_files[index]= null;
            setAnswers([...answers])
        }

    }
    const listErrors = errorStr?.split(',')
   
    return (
        <div className={className} style={{ position: 'relative' }}>
            <table className={`question-table`}>
                <thead>
                    <tr>
                        <th>{t('create-question')['correct-answer']}</th>
                        <th>{t('create-question')['answer-key']}</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    {answers?.map((answer: Answers, answerIndex: number) => [
                        <tr key={`${Guid.newGuid()}_${answerIndex}`}>
                            <td align="center" className="option">
                                <input
                                    type="radio"
                                    name={Guid.newGuid().toString()}
                                    checked={answer.isCorrect}
                                    onChange={onChangeIsCorrect.bind(null, answerIndex)}
                                />
                            </td>
                            <td
                                className={`input-placeholder ${listErrors?.includes(answerIndex.toString()) || errFiles?.includes(answerIndex.toString()) ? 'error' : ''
                                    }`}
                            >
                                <div className="image-input-mc8">
                                    <ImageInputMC8
                                        url={answer.answer}
                                        labelDefault={t(t('create-question')['answer-image'], [`${answerIndex + 1}`])}
                                        onChangeFile={onChangeImage.bind(null, answerIndex)}
                                        viewMode={viewMode}
                                    />
                                </div>
                            </td>
                            <td align="right">
                                <img
                                    onClick={onDeleteAnswer.bind(null, answerIndex)}
                                    className="ic-action"
                                    src="/images/icons/ic-trash.png"
                                    alt="delete-answer-icon"
                                />
                            </td>
                        </tr>,
                    ])}
                    {answers.length < 26 &&
                        !viewMode && (
                            <tr className="action">
                                <td></td>
                                <td align="left" className={`${answers.length === 0 ? 'error' : ''}`}>
                                    <label className="add-new-answer" onClick={onAddAnswer}>
                                        <img
                                            src="/images/icons/ic-plus-sub.png"
                                            width={17}
                                            height={17}
                                            alt="add-new-answer-icon"
                                        />{' '}
                                        {t('create-question')['add-answer']}
                                    </label>
                                </td>
                                <td></td>
                            </tr>
                        )}
                </tbody>
            </table>
        </div>
    )
}

type Answers = {
    answer: string
    isCorrect: boolean
}
function ImageInputMC8({ url, onChangeFile, isError = false, labelDefault, viewMode }: PropsType) {
    const [imageUrl, setImageUrl] = useState(url)
    const fileRef = useRef(null)
    const src = imageUrl.startsWith('blob') ? url : `/upload/${url}`
    return (
        <div className={`image-mc8 ${imageUrl !== '' ? 'has-data' : ''}`}>
            <div 
                className={`image ${isError ? 'error' : ''}`} 
                onClick={() => 
                    fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
            >
                <input
                    ref={fileRef}
                    type={"file"}
                    accept=".png,.jpeg,.jpg"
                    onChange={(e) => {
                        if (fileRef.current.files.length > 0) {
                            isError = null
                            setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                            onChangeFile && onChangeFile(e.target.files)
                        }
                    }}
                />
                {src && url ? (
                    <div>
                        <img
                            src={src}
                            alt='image-upload'
                        />
                    </div>
                ) : (
                    <div className="image-default">
                        <img
                            src="/images/icons/ic-image.png"
                            alt="image-default"
                        />
                        <span className="label-image">{labelDefault}</span>
                    </div>
                )}
            </div>
            {!viewMode && src && url && (
                <img
                    className="image-input-mc8___isDelete"
                    src="/images/icons/ic-remove-opacity.png" alt=""
                    onClick={(event) => {
                        fileRef.current.value = ""
                        event.preventDefault();
                        event.stopPropagation();
                        onChangeFile(null)
                    }}
                />
            )}
        </div>
    )
}
type PropsType = {
    labelDefault?: string
    isError?: boolean
    url: string
    onChangeFile?: (files: FileList) => void
    viewMode?: boolean
}