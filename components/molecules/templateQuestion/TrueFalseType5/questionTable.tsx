import { useState, useRef, ChangeEvent, useEffect, useCallback } from "react";

import ContentEditable from "react-contenteditable"
import { Toggle } from "rsuite"
import { MyCustomCSS } from "@/interfaces/types";
import { convertStrToHtml } from "@/utils/string";
import useTranslation from "@/hooks/useTranslation";

type PropType = {
    setValue?: any
    styleBold?: boolean
    className?: string
    errorStr?: string
    register?:any
    correctErrorStr?: string
    onChange?: (data: any) => void
    errors?: any
    question?:any
    viewMode?:boolean
    isImage?:boolean
}


export default function QuestionTableTF5({
    question,
    register,
    errors = {},
    setValue,
    className = '',
    errorStr = '',
    correctErrorStr = '',
    onChange,
    viewMode = false,
}: PropType) {
    const {t} = useTranslation()
    const listImage = (question?.image ?? '#').split('#') ?? []
    const listAnswer = (question?.answers ?? '#').split('#') ?? []
    const listCorrect = (question?.correct_answers ?? '#').split('#') ?? []
    const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
        listAnswer.map((answer:string, answerIndex:number)=>{
            const list:any[] = listAnswer
            if(list[answerIndex].startsWith('*')){
                list[answerIndex] = list[answerIndex].replace('*','')
            }
            return ({
                key: new Date().getTime().toString() + answerIndex,
                imageStr:listImage[answerIndex],
                answerStr:list[answerIndex].replace('*',''),
                correctStr:listCorrect[answerIndex],
                isNotExam: answer.startsWith('*') ? false : true,
        })})
    )
    useEffect(() => {
        register('isNotExam', {
            validate: (value: string) => {
                const errs = (value ?? '').split('#')
                const errE = errs.filter((item: string) => item === 'false')
                const errA = errs.filter((item: string) => item === 'true')
                if (errE.length > 1) return t(t('question-validation-msg')['example-allow-total'], ['1'])
                if (errA.length < 1) return t(t('question-validation-msg')['question-fb-required'], ['1'])
                return true
            }
        })  
    }, [])
   
    const answerGroupRef = useRef(answerGroup)
    const imageFiles = useRef(answerGroup.map((m) => null))

    const groupKey = useRef(Math.random()).current
    const isInit = useRef(true)
   
   
    useEffect(() => {
        answerGroupRef.current = answerGroup
        if(answerGroup){
            setValue('isNotExam',answerGroup.map((m) => m.isNotExam).join('#'))
        }
        if (isInit.current) {
            isInit.current = false
        } else {
            if (typeof onChange === 'function') {
                onChange({
                answers: answerGroup.map((m) => {
                    if(m.isNotExam){
                        return (m.answerStr ?? '').trim()
                    }else{
                        return `*${(m.answerStr ?? '').trim()}`
                    }
                }).join('#'),
                correct_answers: answerGroup.map((m) => (m.correctStr ?? '').trim()).join('#'),
                image: answerGroup.map((m)=>m.imageStr).join('#'),
                image_files: imageFiles?.current
                })
            }
        }

        setTimeout(() => {
            Array.from(document.getElementsByClassName(`${groupKey}`)).forEach(
                (el) => {
                    const div = el as HTMLDivElement
                    div.style.width = `${div.offsetWidth}px`
                },
            )
        }, 0)
    }, [answerGroup])
    const onAddAnswer = () => {
        const newAnswer = {
            key: new Date().getTime().toString() + (answerGroup?.length ?? 0),
            imageStr: '',
            answerStr: '',
            correctStr: '',
            isNotExam:true,
        }
        if (answerGroup) {
            setAnswerGroup([...answerGroup, newAnswer])
        } else {
            setAnswerGroup([newAnswer])
        }
        imageFiles.current.push(null)
        setTimeout(() => {
            document.getElementById(`${groupKey}_${answerGroup?.length ?? 0}`)?.focus()
        }, 0)
    }
    const onDeleteAnswer = (index: number) => {
        const ans:any[] = answerGroup.filter((m: any, mIndex: number) => mIndex !== index)
        if(imageFiles.current.length > index){
            imageFiles.current.splice(index, 1)
        }
        setAnswerGroup(ans)
    }
    const onChangeIsCorrect = (
        index: number,
        event: ChangeEvent<HTMLInputElement>,
    ) => {
        answerGroup[index].correctStr = event.target.dataset.value
        setAnswerGroup([...answerGroup])
    }
    const onChangeText = (index: number, event: any) => {
        const el: HTMLElement = event.currentTarget
        let text = ''
        for (let i = 0, len = el.childNodes.length; i < len; i++) {
            const childNode = el.childNodes[i]
            text += convertHtmlToStr([childNode])
        }
        answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '')
        setAnswerGroup([...answerGroup])
    }
    const onBlurText = (index: number, event: any) => {
        const el: HTMLElement = event.currentTarget
        let text = ''
        for (let i = 0, len = el.childNodes.length; i < len; i++) {
            const childNode = el.childNodes[i]
            text += convertHtmlToStr([childNode])
        }
        answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '').trim()
        el.innerHTML = convertStrToHtml(answerGroup[index].answerStr)
        // setAnswerGroup([...answerGroup])
    }
    const onChangeImage = (index: number, files: FileList) => {
        if(files && files[0]){
            const urlImage = URL.createObjectURL(files[0])
            answerGroup[index].imageStr = urlImage
            imageFiles.current[index] = files[0]
            setAnswerGroup([...answerGroup])
        }else{
            answerGroup[index].imageStr = ''
            imageFiles.current[index] = null
            setAnswerGroup([...answerGroup])
        }

    }

    const listExamError = answerGroup.filter((item:any)=> item.isNotExam === false)
    const listAError = answerGroup.filter((item:any)=> item.isNotExam === true)
    const listError = errorStr.split(',')
    const listCError = correctErrorStr.split(',') ?? []
    const listImageError = (errors['image']?.message ?? '').split(',') ?? []
    const listImageFileError = (errors['image_files']?.message ?? '').split(',') ?? []
    const [err, setErr] = useState(false)
    const [error, setError] = useState('')
    const renderError = () => {
        if(listExamError.length > 1){
            setErr(true)
            return  setError(`* ${t(t('question-validation-msg')['example-allow-total'], ['1'])}`)
        }
        if(listAError.length < 1){
            setErr(true)
            return setError(`* ${t(t('question-validation-msg')['question-fb-required'], ['1'])}`)
        }
        return setError('')
    }
    const handleToggle = (id:number,value:boolean) =>{
        answerGroup[id].isNotExam = !value
        setAnswerGroup([...answerGroup])
    }
    useEffect(()=>{
        renderError()
    },[listExamError,listAError])

    let indexE = -1;
    let indexA = -1;
    const onPaste = (event: any) => {
        event.preventDefault()
        let data = event.clipboardData.getData('text').replace(/(\#|\*)/g, '')
        data = data?.replace(/\r?\n|\r/g, '')
        const selection = window.getSelection()
        const range = selection.getRangeAt(0)
        range.deleteContents()
        const node = document.createTextNode(data)
        range.insertNode(node)
        selection.removeAllRanges()
        const newRange = document.createRange()
        newRange.setStart(node, data.length)
        selection.addRange(newRange)
      }
    const onBold = () => {
        const selection = window.getSelection()
        console.log("selection====",selection)
        if (selection.rangeCount === 0) return
        const range = selection.getRangeAt(0)
        const parent = range.commonAncestorContainer
        let el: HTMLElement = null
        let parentEl =
            parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
        while (parentEl.parentElement) {
            if (parentEl.classList.contains('div-input')) {
                el = parentEl
                break
            }
            parentEl = parentEl.parentElement
        }
        document.execCommand('Bold', false, null)
        if (el) {
            const index = parseInt(el.id.replace(`${groupKey}_`, ''))
            let text = ''
            for (let i = 0, len = el.childNodes.length; i < len; i++) {
                const childNode = el.childNodes[i]
                text += convertHtmlToStr([childNode])
            }
            answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '')
            setAnswerGroup([...answerGroup])
        }
    }
    const onItalic = () => {
        const selection = window.getSelection()
        if (selection.rangeCount === 0) return
        const range = selection.getRangeAt(0)
        const parent = range.commonAncestorContainer
        let el: HTMLElement = null
        let parentEl =
            parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
        while (parentEl.parentElement) {
            if (parentEl.classList.contains('div-input')) {
                el = parentEl
                break
            }
            parentEl = parentEl.parentElement
        }
        document.execCommand('Italic', false, null)
        if (el) {
            const index = parseInt(el.id.replace(`${groupKey}_`, ''))
            let text = ''
            for (let i = 0, len = el.childNodes.length; i < len; i++) {
                const childNode = el.childNodes[i]
                text += convertHtmlToStr([childNode])
            }
            answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '')
            setAnswerGroup([...answerGroup])
        }
    }
    const onUnderline = () => {
        const selection = window.getSelection()
        if (selection.rangeCount === 0) return
        const range = selection.getRangeAt(0)
        const parent = range.commonAncestorContainer
        let el: HTMLElement = null
        let parentEl =
            parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
        while (parentEl.parentElement) {
            if (parentEl.classList.contains('div-input')) {
                el = parentEl
                break
            }
            parentEl = parentEl.parentElement
        }
        document.execCommand('Underline', false, null)
        if (el) {
            const index = parseInt(el.id.replace(`${groupKey}_`, ''))
            let text = ''
            for (let i = 0, len = el.childNodes.length; i < len; i++) {
                const childNode = el.childNodes[i]
                text += convertHtmlToStr([childNode])
            }
            answerGroup[index].answerStr = text.replace(/(\*|\#)/g, '')
            setAnswerGroup([...answerGroup])
        }
    }

    const convertHtmlToStr = useCallback((dataD: ChildNode[]) => {
        let str = ''
        for (let i = 0, len = dataD.length; i < len; i++) {
            const child = dataD[i] as Node
            if (child.nodeName === '#text') {
                str += child.textContent
            } else if (child.nodeName === 'SPAN') {
                str += child.firstChild.textContent
            } else if (child.nodeName === 'BR') {
                // str += '\n'
            } else if (child.nodeName === 'INPUT') {
                const inputEl = child as HTMLInputElement
                str += `#${inputEl.value}#`
            } else {
                const el = child as Element
                const tag = child.nodeName.toLowerCase()
                const content = convertHtmlToStr(Array.from(el.childNodes))
                if (content !== '') {
                    str += `%${tag}%${content}%${tag}%`
                }
            }
        }
        return str
    }, [])
    return (
        <div className={`question-table-tf5 ${className}`} style={{ position: 'relative' }}>
            <table className={`question-table question-table-tf5-table`}>
                <thead className={`question-table-tf5-thead`}>
                    <tr>
                        <th
                            dangerouslySetInnerHTML={{
                                __html: t('create-question')['select-question-example']
                            }}
                        ></th>
                        <th align="left">{t('create-question')['list-questions-examples']}</th>
                        <th>
                            <div style={{ display: 'flex' }}>
                                <div
                                    className="icon-mask icon-action-style"
                                    onMouseDown={onBold}
                                    style={
                                        {
                                            '--image': 'url(/images/icons/ic-bold.png)',
                                        } as MyCustomCSS
                                    }
                                ></div>
                                <div
                                    className="icon-mask icon-action-style"
                                    onMouseDown={onItalic}
                                    style={
                                        {
                                            '--image': 'url(/images/icons/ic-italic.png)',
                                        } as MyCustomCSS
                                    }
                                ></div>
                                <div
                                    className="icon-mask icon-action-style"
                                    onMouseDown={onUnderline}
                                    style={
                                        {
                                            '--image': 'url(/images/icons/ic-underline.png)',
                                        } as MyCustomCSS
                                    }
                                ></div>
                            </div>
                        </th>
                        <th align="center">True</th>
                        <th align="center">False</th>
                        <th align="center"></th>
                    </tr>
                </thead>
                <tbody className={`question-table-tf5-tbody`}>
                    {answerGroup?.map((answer: AnswerGroup, answerIndex: number) => {
                        if(answer.isNotExam === true){
                            indexA +=1;
                        }else{
                            indexE +=1;
                        }
                        return (
                            <tr key={answer.key} id={answer.key}>
                                <td align="center"
                                    className={`input-placeholder ${listExamError.length > 1 || listAError.length < 1 ? 'error' : ''}`}
                                >
                                    <>
                                        <Toggle
                                            size="md"
                                            checked={answer.isNotExam}
                                            checkedChildren={t('common')['question']}
                                            unCheckedChildren={t('common')['example']}
                                            onChange={()=>handleToggle(answerIndex,answer.isNotExam)}
                                        />
                                    </>
                                </td>
                                <td
                                    className={`input-placeholder 
                                    ${(listImageError.includes(answerIndex.toString()) || 
                                        listImageFileError.includes(answerIndex.toString())) ? 'error' : ''}`}
                                >
                                    <div className="image-input-tf5">
                                        <ImageInputTF5
                                            url={answer.imageStr}
                                            onChangeFile={onChangeImage.bind(null, answerIndex)}
                                            isError={listImageError.includes(answerIndex.toString())}
                                            labelDefault={`${answer.isNotExam === true ? t(t('create-question')['question-image'], [`${indexA + 1}`]) : t(t('create-question')['image-example-with-num'], [`${indexE + 1}`])}`}
                                            viewMode={viewMode}
                                        />
                                    </div>
                                </td>
                            <td className={`input-placeholder ${listError.includes(answerIndex.toString()) ? 'error' : ''}`}>
                                    <ContentEditable
                                        id={`${groupKey}_${answerIndex}`}
                                        spellCheck={false}
                                        className={`div-input ${groupKey}`}
                                        html={convertStrToHtml(answer.answerStr)}
                                        contentEditable="true"
                                        onChange={onChangeText.bind(null, answerIndex)}
                                        onBlur={onBlurText.bind(null, answerIndex)}
                                        onPaste={onPaste}
                                        onKeyDown={(event)=>{
                                            if (event.key === 'Enter') {
                                                event.preventDefault()
                                            }
                                        }}
                                    />
                                    <span className="placeholder">
                                    {`${answer.isNotExam === true ? `${t('common')['question']} ${indexA+1}` : `${t('common')['example']} ${indexE+1}`}`}
                                    </span>
                                </td>
                                <td
                                    align="center"
                                    className={`input-placeholder ${listCError.includes(answerIndex.toString()) ? 'error' : ''}`}
                                >
                                    <input
                                        type="radio"
                                        data-value="T"
                                        name={`${groupKey}_${answerIndex}`}
                                        checked={answer.correctStr === 'T'}
                                        onChange={onChangeIsCorrect.bind(null, answerIndex)}
                                    />
                                </td>
                                <td
                                    align="center"
                                    className={`input-placeholder ${listCError.includes(answerIndex.toString()) ? 'error' : ''}`}
                                >
                                    <input
                                        type="radio"
                                        data-value="F"
                                        name={`${groupKey}_${answerIndex}`}
                                        checked={answer.correctStr === 'F'}
                                        onChange={onChangeIsCorrect.bind(null, answerIndex)}
                                    />
                                </td>
                                <td align="right">
                                    <img
                                        onClick={onDeleteAnswer.bind(null, answerIndex)}
                                        className="ic-action"
                                        src="/images/icons/ic-trash.png"
                                        alt="delete"
                                    />
                                </td>
                            </tr>
                    )})}
                    {!viewMode && answerGroup.length < 25 && (<tr className="question-table-tf5-action">
                        <td></td>
                        <td align="left" className="action">
                            <label className={`add-new-answer-tf5 ${answerGroup.length === 0 ? 'error' : ''}`} onClick={onAddAnswer}>
                                <img
                                    src="/images/icons/ic-plus-sub.png"
                                    alt="add"
                                    width={17}
                                    height={17}
                                />{' '}
                               {t('create-question')['add-question']}
                            </label>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>)}
                </tbody>
            </table>
            <div className="render_error">
                {err && (
                    <span>{error}</span>
                )}
                {errors['image_files']?.message && (
                    <span>* {t('question-validation-msg')['list-images']}</span>
                )}
            </div>
        </div>
    );
}

type PropsType = {
    labelDefault?: string
    isError?: boolean
    url: string
    onChangeFile?: (files: FileList) => void
    viewMode?: boolean
}
function ImageInputTF5({ url, onChangeFile, isError, labelDefault, viewMode }: PropsType) {
    const [imageUrl, setImageUrl] = useState(url)
    const fileRef = useRef(null)
    const src = imageUrl?.startsWith('blob') ? url : `/upload/${url}`
    return (
        <div className={`image-tf5 ${imageUrl !== '' ? 'has-data' : ''}`}>
            <div className={`image ${isError ? 'error' : ''}`}  onClick={() => fileRef.current.dispatchEvent(new MouseEvent('click'))}>
                <input
                    ref={fileRef}
                    type={"file"}

                    accept=".png,.jpeg,.jpg"
                    onChange={(e) => {
                        if (fileRef.current.files.length > 0) {
                            isError = null
                            setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                            onChangeFile && onChangeFile(e.target.files)
                        }
                    }}
                />
                {src && url? (
                    <img
                        src={src}
                        alt='image-upload'

                    />
                ) : (
                    <div className="image-default">
                        <img
                            src="/images/icons/ic-image.png"
                            alt="image-default"

                        />
                        <span className="label-image">{labelDefault}</span>
                    </div>

                )}

            </div>
            {!viewMode && src && url && <img
                className="image-input-tf5___isDelete"
                src="/images/icons/ic-remove-opacity.png" alt=""
                                    onClick={()=>{
                    fileRef.current.value = ""
                    event.preventDefault();
                    event.stopPropagation();
                    onChangeFile(null)
                }}
            />}

        </div>
    )
}

type AnswerGroup = {
    key:string,
    imageStr:string,
    answerStr:string,
    correctStr:string,
    isNotExam:boolean,
    imageFile?: FileList
}