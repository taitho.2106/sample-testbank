import { useEffect } from 'react'
import { MultiChoiceTable11 } from './table'
import { QuestionPropsType } from 'interfaces/types'
import browserFile from 'lib/browserFile'
import useTranslation from "@/hooks/useTranslation";

export default function MultiChoiceGroupMC11({
    question, register, setValue, errors = {},viewMode,
}: QuestionPropsType) {
    const {t} = useTranslation()
    useEffect(() => {   
        register('exampleRule', {
            validate: (value: string) => {
                return value
            }
        })
        register('questionRule', {
            validate: (value: string) => {
                return value
            }
        })
        register('answers', {
            validate: (value: string) => {
                const stringGroup = (value ?? '').split('#').map((item:string) => 
                    item.split('*').map((v, i) => {
                        if( v === '' )
                            return i
                        return null
                    }).filter(i => i !== null)
                )
                const error = stringGroup.some(item => item.length > 0 )
                    ? stringGroup.map(item => item.join()).join('#') : true
                return error
            },
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['answers-mc-max-length'], ['10000']),
            },
            value: question?.answers,
        })
        register('correct_answers', {
            validate: (value: string) => {
                const stringGroup = value.split('#').map((item, index) => {
                    if( item === '' )
                        return index
                    return null
                }).filter(i => i !== null)
                
                const error = stringGroup.length > 0 
                    ? stringGroup.join('#') : true

                return error
            },
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
            },
            value: question?.correct_answers,
        })
        register('mc11_image_files', {
            validate: async (fileGroup: FileList[]) => {
                const typeFile = ['image/jpeg', 'image/jpg', 'image/png']
                const groupError = Array.from(Array(fileGroup.length), () => [])

                let i = 0
                for await( const files of Array.from(fileGroup) ){
                    let j = 0
                    for await ( const item of Array.from(files)){
                        if( !item ){
                            groupError[i].push(null)
                        }else
                        if( item && typeFile.includes(item?.type) == false){
                            groupError[i].push(`${j}|undefined`)
                        }else
                        if( item && ! await browserFile.checkFileExist(item) ){
                            groupError[i].push(`${j}|deleted`)
                        }
                        j++ 
                    }
                    groupError[i] = groupError[i].filter(k => k !== null)
                    i++
                }

                const error = groupError.some((item: any) => item.length > 0)
                    ? groupError.map((item: any) => item.join()).join('#') : true
                return error
            },
        })
        register('audio', {
            maxLength: {
                value: 2000,
                message:  t(t('question-validation-msg')['audio-max-length'],['2000']),
            },
            required: t('question-validation-msg')['audio-required'],
            value: question?.audio,
        })
        register('audio_file',{
            validate: {
                acceptedFormats: async (file) => {
                    // check format if has file
                    if (!file) {
                        return true
                    }
                    if( ! await browserFile.checkFileExist(file) || file.size == 0 ){
                        return t('question-validation-msg')['audio-link-error']
                    }
                    return (
                        ['audio/mp3','audio/mpeg'].includes(file?.type) ||
                        t('question-validation-msg')['audio-type']
                    )
                },
            }
        })
        register('audio_script', {
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
            },
            value: question?.audio_script,
        })
    }, [])

    const onChange = (data: any) => {
        setValue('answers', data.answers)
        setValue('correct_answers', data.correct_answers)
        setValue('mc11_image_files', data.image_files)
        setValue('questionRule', data.question_rule)
        setValue('exampleRule', data.example_rule)
    }

    return <>
        <MultiChoiceTable11 
            description={question?.question_description ?? ''}
            audioString={question?.audio_script ?? ''}
            audioFile={question?.audio ?? null}
            answerString={question?.answers ?? '**'} 
            correctString={question?.correct_answers} 
            className={''} 
            errorString={errors['answers']?.message ?? ''} 
            onChange={onChange} 
            register={register} 
            setValue={setValue}
            errors={errors} 
            viewMode={viewMode}/>
    </>
}


