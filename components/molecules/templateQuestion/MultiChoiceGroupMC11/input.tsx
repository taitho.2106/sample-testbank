import React, { useState, useRef, ChangeEvent } from 'react' 

type TImagePropsType = {
    labelDefault?: string,
    isError?: boolean,
    isEmpty?: boolean,
    url: string,
    onChangeFile?: (files: FileList) => void,
    viewMode?: boolean
}
function ImageInputMC11( propsType: TImagePropsType ){
    const [ _path, setPath ] = useState<string>(propsType.url)
    const _package = useRef(null)
    const src = _path.startsWith('blob') ? propsType.url : `/upload/${propsType.url}`

    const handleOnClick = () => _package.current.dispatchEvent(new MouseEvent('click'))
    const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
        if( _package.current.files.length > 0 ){
            setPath( URL.createObjectURL(_package.current.files[0]) )
            propsType.onChangeFile && propsType.onChangeFile(event.target.files)
        }
    }
    const handleOnClickDelete = (event: any) => {
        _package.current.value = ''
        event.preventDefault()
        event.stopPropagation()
        propsType.onChangeFile(null)
    }

    return<>
        <div className={`image-mc11 ${_path !== '' ? 'has-data' : ''} ${propsType.isEmpty ? 'error' : ''}`} onClick={handleOnClick}>
            <div className={`image ${propsType.isError ? 'error' : ''}`}>
                <input ref={_package} type={"file"} accept='.jpeg,.jpg,.png' onChange={handleOnChange} />
                { src && propsType.url 
                    ? (<img src={src} alt='image-upload' />) 
                    : ( <div className='image-default'>
                            <img src='/images/icons/ic-image.png' alt='image-default'/>
                        </div> )}
                { ! propsType.viewMode && src && propsType.url 
                    && <img className='image-input-mc11___isDelete' 
                    src='/images/icons/ic-remove-opacity.png' alt='icon-remove' onClick={handleOnClickDelete}/> }
            </div>
            <div className='label-image'>
                <span className='__text' >{propsType.labelDefault}</span>
            </div>
        </div>
    </> 
}

export { ImageInputMC11 }