import { useState } from 'react'

import { UseFormRegister, UseFormSetValue } from 'react-hook-form'
import { Button, Toggle, Checkbox } from 'rsuite'

import { TextAreaField } from 'components/atoms/textAreaField'
import Guid from 'utils/guid'

import AudioInput from '../AudioInput'
import { ImageInputMC11 } from './input'
import useTranslation from "@/hooks/useTranslation";

type PropType = {
    description: string
    audioString: string
    audioFile: string
    answerString: string
    correctString: string
    styleBold?: boolean
    className?: string
    errorString?: string
    register?:UseFormRegister<any>
    setValue?:UseFormSetValue<any>
    errors?:any
    viewMode?: boolean
    onChange: (data: any) => void
}

type AnswerGroup = {
    key: string
    answerString: string
    correctString: string | null
    mark: boolean
    error?: boolean
    files?: Array<File>
}

function MultiChoiceTable11( multipleChoices: PropType){
    const {t} = useTranslation()
    const answerArray = multipleChoices.answerString?.split('#') ?? []
    const correctArray = multipleChoices.correctString?.split('#') ?? []
    const errorArray = multipleChoices.errors

    const [bunchOfAnswer, setBunchOfAnswer] = useState<AnswerGroup[]>(
        answerArray.map((item: string, index: number) => {
            return({
                key: `${Math.random()}_${index}`,
                answerString: item.replace('ex|',''),
                correctString: correctArray[index] ?? '',
                mark: item.startsWith('ex') ? false : true,
                files: item.split('*').map(item => null)
            })
        }) ?? []
    )

    const lengthyMark = bunchOfAnswer.filter(item => item.mark === false).length ?? 0 
    const maxExamples = 1
    const errorMessage: string[] = []
    const answerError = errorArray['answers']?.message?.split('#') ?? []
    const correctError = errorArray['correct_answers']?.message?.split('#') ?? []
    let imagesError = errorArray['mc11_image_files']?.message?.split('#') ?? []

    if( lengthyMark > maxExamples ) errorMessage.push(t(t('question-validation-msg')['example-allow-total'], [`${maxExamples}`]))
    if( lengthyMark == bunchOfAnswer.length ) errorMessage.push(t(t('question-validation-msg')['question-fb-required'], ['1']))
    if( correctError.length > 0 ) errorMessage.push(t(t('question-validation-msg')['chosen-answer'], ['1']))
    if( imagesError.length > 0 ) {
        const formatUndefined = imagesError.filter((i:string) => i.includes('|undefined')).length ?? 0
        if( formatUndefined > 0 ) {
            errorMessage.push(t('question-validation-msg')['image-file'])
        }
        const imageDeleted = imagesError.filter((i:string) => i.includes('|deleted')).length ?? 0
        if( imageDeleted > 0 ){
            errorMessage.push(t('question-validation-msg')['list-images'])
        }
        imagesError = imagesError.map((i:string) => i.replace(/\|undefined|\|deleted/g, ''))
    }        
        
    multipleChoices.onChange({
        answers: bunchOfAnswer.map(group => group.mark == false ? `ex|${group.answerString}` : group.answerString).join('#'),
        correct_answers: bunchOfAnswer.map(group => group.correctString).join('#'),
        image_files: bunchOfAnswer.map(group => group.files.map(file => file ?? null)),
        example_rule: lengthyMark > maxExamples ? false : true,
        question_rule: lengthyMark == bunchOfAnswer.length ? false : true
    })
    const handleAddQuestionGroup = () => {
        const stringGroup = '**'
        const emptyAnswerGroup:AnswerGroup = {
            key: `${Math.random()}_${bunchOfAnswer.length}`,
            answerString: stringGroup,
            correctString: '',
            mark: true,
            files: stringGroup.split('*').map(item => null)
        }
        setBunchOfAnswer(previous => [...previous, emptyAnswerGroup])
    }
    const handleAddQuestionItem = (key: string) => {
        const indexGroup = bunchOfAnswer.findIndex(item => item.key === key)
        bunchOfAnswer[indexGroup].answerString+='*'
        setBunchOfAnswer(previous => [...previous])
    }

    const handleDeleteQuestionGroup = (key: string) => {
        const indexGroup = bunchOfAnswer.findIndex(item => item.key === key)
        bunchOfAnswer.splice(indexGroup, 1)
        setBunchOfAnswer(previous => [...previous])
    }
    const handleDeleteQuestionItem = (key: string, index: number) => {
        const indexGroup = bunchOfAnswer.findIndex(item => item.key === key)

        const answerArray = bunchOfAnswer[indexGroup].answerString.split('*')
        const correctArray = bunchOfAnswer[indexGroup].correctString.split('*')
        answerArray.splice(index, 1)
        bunchOfAnswer[indexGroup].files.splice(index,1)
        bunchOfAnswer[indexGroup].answerString = answerArray.join('*')
        if( correctArray.includes(index.toString()) ){
            const indexCorrect = correctArray.findIndex(item => item == index.toString())
            correctArray.splice(indexCorrect, 1)
        }
        bunchOfAnswer[indexGroup].correctString = 
            correctArray.map(item => ( (parseInt(item) > index)
            ? (parseInt(item) - 1).toString() : item)).join('*')
        setBunchOfAnswer(previous => [...previous])
    }

    const handleOnCheckQuestionItem = (key: string, value: string) => {
        const indexGroup = bunchOfAnswer.findIndex(item => item.key === key)
        const correctArray = bunchOfAnswer[indexGroup]
            .correctString.split('*').filter(i => i != '')
        if( correctArray.includes(value) ){
            const indexExist = correctArray.findIndex(i => i.includes(value))
            correctArray.splice(indexExist, 1)
        }else{
            correctArray.push(value)     
        }
        bunchOfAnswer[indexGroup].correctString = correctArray.join('*')
        setBunchOfAnswer(previous => [...previous])
    }
    const handleOnChangeToggle = (key: string) => {
        const indexGroup = bunchOfAnswer.findIndex(item => item.key === key)
        bunchOfAnswer[indexGroup].mark = ! bunchOfAnswer[indexGroup].mark
        setBunchOfAnswer(previous => [...previous])
    }

    const handleOnChangeAudioFile = (files: FileList) => {
        if (files.length > 0) {
            multipleChoices.setValue('audio_file', files[0])
            const path = URL.createObjectURL(files[0])
            multipleChoices.setValue('audio', path)
        }
    }
    const handleOnChangeScript = (data: any) => {
        multipleChoices.setValue('audio_script', data)
    }
    const handleChangeImage = (key: string, index: number, files: FileList) => {
        const indexAnswerGroup = bunchOfAnswer.findIndex(item => item.key === key)
        if(indexAnswerGroup ==-1) return;

        const answerGroup = bunchOfAnswer[indexAnswerGroup]
        const answerGroupString = answerGroup.answerString.split('*')
        if(files && files[0]){
            const path = URL.createObjectURL(files[0])
            answerGroupString[index] = path
            answerGroup.files[index] = files[0]
        }else{
            answerGroupString[index] = ''
            answerGroup.files[index] = null
        }
        answerGroup.answerString = answerGroupString.join('*')
        bunchOfAnswer[indexAnswerGroup] = {...answerGroup}
        setBunchOfAnswer([...bunchOfAnswer])
    }
    return (<>
        <div className="m-multi-choice-mc11">
            <AudioInput
                url={multipleChoices.audioFile}
                script={multipleChoices.audioString}
                isError={multipleChoices.errors['audio'] || multipleChoices.errors['audio_file']}
                onChangeFile={handleOnChangeAudioFile}
                onChangeScript={handleOnChangeScript}
                viewMode={multipleChoices.viewMode}/>
            <div className="box-content-mc11">
                <TextAreaField
                    label={t('create-question')['question-description']}
                    setValue={multipleChoices.setValue}
                    defaultValue={multipleChoices.description}
                    className={`form-input-stretch ${
                        multipleChoices.errors['question_description'] ? 'error' : ''
                    }`}
                    register={multipleChoices.register('question_description', {
                        required: t('question-validation-msg')['description-required'],
                        maxLength: {
                            value: 2000,
                            message: t(t('question-validation-msg')['description-max-length'], ['2000']),
                        },
                    })}
                    viewMode={multipleChoices.viewMode}
                    specialChar={['#','*']}/>
                <div className="wrapper-table-mc11">
                    { bunchOfAnswer.map((item: AnswerGroup, index: number) => 
                        <RenderQuestionTableGroup 
                            key={`${Guid.newGuid()}`}
                            itemGroup={item}
                            markedOfLength={lengthyMark}
                            bunchOfLength={bunchOfAnswer.length}
                            onDeleteTableGroup={handleDeleteQuestionGroup}
                            onChangeToggle={handleOnChangeToggle}>
                            <RenderQuestionTableItem 
                                itemGroup={item}
                                onAddTableItem={handleAddQuestionItem}>
                                <RenderQuestionRowItem 
                                    error={[{
                                        checkbox: correctError.includes(index.toString()) ?? null, 
                                        answers: answerError[index]?.split(',') ?? [],
                                        images: imagesError[index]?.split(',') ?? []}]}
                                    itemGroup={item} 
                                    onDeleteTableItem={handleDeleteQuestionItem}
                                    onCheckTableItem={handleOnCheckQuestionItem}
                                    onChangeImage={handleChangeImage}/>
                            </RenderQuestionTableItem>
                        </RenderQuestionTableGroup>
                    ) }
                </div>

                <div className="form-input-stretch">
                    {!multipleChoices.viewMode && bunchOfAnswer.length < 26 && (
                    <Button
                        appearance={'primary'}
                        type="button"
                        onClick={handleAddQuestionGroup}
                        className={`btn-add-new-question ${bunchOfAnswer.length==0 ? "error" : ""}`}>
                        <label className="add-new-question">
                            <img src="/images/icons/ic-plus-sub.png" width={17} height={17} alt="plus" />
                            {' '}{t('create-question')['add-question']}
                        </label>
                    </Button>
                    )}
                </div>
                {errorMessage.length > 0 ? 
                    <div className="render_error">
                        {errorMessage.map((messages:string) => <span>* {messages}</span>)}
                    </div>
                : ''}
            </div>
        </div> 
    </>)
}
interface IRenderItem{ itemGroup: AnswerGroup, error?: Array<any> ,children?: React.ReactNode }
interface RenderTableGroup extends IRenderItem{ 
    markedOfLength?: number
    bunchOfLength?: number 
    onChangeToggle?: (key: string) => void
    onDeleteTableGroup?: (key: string) => void
}
interface RenderTableItem extends IRenderItem{
    onAddTableItem?: (key: string) => void
}
interface RenderRowItem extends IRenderItem{
    onDeleteTableItem?: (key: string, index: number) => void
    onCheckTableItem?: (key: string, value: string) => void
    onChangeImage?: (key:string, index: number, files: FileList) => void
}


const RenderQuestionTableGroup = ( render: RenderTableGroup) => {
    const {t} = useTranslation()
    const markedError = (!render.itemGroup.mark && render.markedOfLength > 1) 
        || ( render.markedOfLength == render.bunchOfLength ) ? 'error' : ''
    return(<>
        <table className="question-table-mc11" key={render.itemGroup.key}>
            <thead>
                <tr>
                    <th
                        dangerouslySetInnerHTML={{
                            __html: t('create-question')['select-question-example']
                        }}
                    ></th>
                    <th align="left">{t("create-question")["correct-answer"]}</th>
                    <th align="left">{t("create-question")["answer-key"]}</th>
                    <th>
                        {render.bunchOfLength > 1 ? <div className="action-reverse" 
                            onClick={() => render.onDeleteTableGroup(render.itemGroup.key)}>
                            <img className="ic-action" 
                                src="/images/icons/ic-trash-white.png" alt="delete-icon"/>
                        </div> : ''}
                    </th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td className="optional-toggle">
                        <div className={`toggle-switch ${markedError}`}>
                            <Toggle
                                size="md"
                                checked={render.itemGroup.mark}
                                checkedChildren={t("common")["question"]}
                                unCheckedChildren={t('common')['example']}
                                onChange={() => render.onChangeToggle(render.itemGroup.key)}/>
                        </div>
                    </td>
                    <td colSpan={3}>
                        {render.children}
                    </td>
                </tr>
            </tbody>
        </table>
    </>)
}

const RenderQuestionTableItem = ( render: RenderTableItem ) => {
    const {t} = useTranslation()
    const answerArray = render.itemGroup.answerString.split('*') ?? []
    return(
        <table className="answer-table-mc-11" key={render.itemGroup.key}>
            {render.children}
            {answerArray.length < 10 ? <tr>
                <td className="add-item" align="left" colSpan={3}>
                    <label className="add-new-answer" 
                        onClick={() => render.onAddTableItem(render.itemGroup.key)}>
                        <img alt='plus' src="/images/icons/ic-plus-sub.png" 
                            width={17} height={17}
                        />{' '}{t('create-question')['add-answer']}
                    </label>
                </td>
            </tr> : ''}
        </table>
    )
}

const RenderQuestionRowItem = ( render: RenderRowItem ) => {
    const {t} =useTranslation()
    const answerArray = render.itemGroup.answerString.split('*') ?? []
    const correctArray = render.itemGroup.correctString.split('*') ?? []
    
    const checkboxError = render.error[0].checkbox ? 'error' : false
    const answerError = render.error[0].answers 
    const imagesError = render.error[0].images

    const isChecked = (key: number) : boolean => {
        return correctArray.includes(key.toString()) ? true : false
    }
    return(<>
        {render.itemGroup.answerString.split('*').map((value: any, i: number) => {
            return (
                <tr key={`${render.itemGroup.key}_${value?value:i}`}>
                    <td><Checkbox className={`checkbox-mc-11 ${checkboxError}`} checked={isChecked(i)} 
                        onClick={() => render.onCheckTableItem(render.itemGroup.key, i.toString())}/></td>
                    <td>
                        <div className='image-input-mc11'>
                            <ImageInputMC11 
                                url={value} 
                                isEmpty={answerError.includes(i.toString())}
                                isError={imagesError.includes(i.toString())}
                                labelDefault={t(t('create-question')['answer-image'],[`${i + 1}`])}
                                onChangeFile={render.onChangeImage.bind(null, render.itemGroup.key, i)}/>
                        </div>
                    </td>
                    <td>
                        { answerArray.length > 1 ? <div className='action'>
                            <img
                                onClick={() => render.onDeleteTableItem(render.itemGroup.key, i)}
                                className="ic-action"
                                src="/images/icons/ic-trash.png"
                                alt="delete-icon"
                            />
                        </div> : ''}      
                    </td>
                </tr>
            )
        })}
    </>)
}

export { MultiChoiceTable11 }