import { useCallback, useEffect, useState } from 'react'

import { ContentField } from 'components/atoms/contentField'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionDataType, QuestionPropsType } from 'interfaces/types'

import LikertScaleTable from '../LikertScale/table'
import MultiChoiceGroupQuestion from '../MultiChoiceGroup/question'
import useTranslation from '@/hooks/useTranslation'

export default function MixGame2({
  question,
  register,
  setValue,
  errors = {},
  viewMode,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const convertData = useCallback((ques: QuestionDataType) => {
    const descriptions = ques?.question_description?.split('[]') || []
    const questionTexts = ques?.question_text?.split('[]') || []
    const answerTexts = ques?.answers?.split('[]') || []
    const correctTexts = ques?.correct_answers?.split('[]') || []
    return [
      {
        description: descriptions[0] ?? '',
        questions: '',
        answers: answerTexts[0] ?? '###',
        corrects: correctTexts[0] ?? '###',
      },
      {
        description: descriptions[1] ?? '',
        questions: questionTexts[1],
        answers: answerTexts[1] ?? '***',
        corrects: correctTexts[1],
      },
    ]
  }, [])
  const [data, setData] = useState(convertData(question))

  useEffect(() => {
    register('parent_question_text', {
      required: t('question-validation-msg')['reading-script-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['reading-script-max-length'],['10000']),
      },
      value: question?.parent_question_text,
    })
    register('question_description', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('[]')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m != null)
        return errs.length > 0 ? errs.join('') : true
      },
      required: t('question-validation-msg')['description-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['description-max-length'],['10000']),
      },
      value: question?.question_description,
    })
    register('question_text', {
      validate: (value: string) => {
        const errs = (value ?? '[]')
          .split('[]')[1]
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
    },
      value: question?.question_description,
    })
    register('answers', {
      validate: (value: string) => {
        const listV: any[] = (value ?? '[]').split('[]')
        const errs: any[] = [null, null]
        errs[0] = listV[0]
          .split('#')
          .map((m: string, mIndex: number) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m: any) => m !== null)
        errs[1] = listV[1].split('#').map((m: string, mIndex: number) =>
          m
            .split('*')
            .map((g: string, gIndex: number) => {
              if (g === '') return gIndex
              return null
            })
            .filter((m: any) => m !== null),
        )
        return errs[0].length > 0 || errs[1].some((m: any) => m.length > 0)
          ? `${listV[0] === '' ? true : errs[0].join()}[]${errs[1]
            .map((m: any) => m.join())
            .join('#')}`
          : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers,
    })
    register('correct_answers', {
      validate: (value: string) => {
        const ansl = ['T', 'F', 'NI']
        const listV: any[] = (value ?? '[]').split('[]')
        const errs: any[] = [null, null]
        errs[0] = listV[0]
          .split('#')
          .map((m: string, mIndex: number) => {
            if (!ansl.includes(m)) return mIndex
            return null
          })
          .filter((m: any) => m !== null)
        errs[1] = listV[1]
          .split('#')
          .map((m: string, mIndex: number) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m: any) => m !== null)
        return errs[0].length > 0 || errs[1].length > 0
          ? `${errs[0].join()}[]${errs[1].join()}`
          : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [])

  useEffect(() => {
    setValue('question_description', data.map((m) => m.description).join('[]'))
    setValue('question_text', data.map((m) => m.questions).join('[]'))
    setValue('answers', data.map((m) => m.answers).join('[]'))
    setValue('correct_answers', data.map((m) => m.corrects).join('[]'))
  }, [data])

  const onChangeParentQuestionText = (data: any) => {
    setValue('parent_question_text', data)
  }

  const onChangeQuestionText = (index: number, value: any) => {
    data[index].description = value.trim()
    setData([...data])
  }

  const onLSTableChange = (dataV: any) => {
    data[0].answers = dataV.answers
    data[0].corrects = dataV.correct_answers
    setData([...data])
  }

  const onMCChange = (dataV: any) => {
    console.log(dataV)
    data[1].questions = dataV.questions
    data[1].answers = dataV.answers
    data[1].corrects = dataV.corrects
    setData([...data])
  }

  const checkError = useCallback(
    (errorList: any, name: string, index: number) => {
      return errorList[name] && errorList[name].message.includes(index)
    },
    [],
  )

  const tfAnswerError = errors['answers']
    ? errors['answers'].message.split('[]')
    : []

  const tfCorrectError = errors['correct_answers']
    ? errors['correct_answers'].message.split('[]')
    : []

  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)

  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }

  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }

  return (
    <div className="m-mix-game-1">
      <div
        className="box-wrapper"
        style={{ flex: 1, display: 'flex', flexDirection: 'column' }}
      >
        <label>{t('create-question')['task-instructions']}</label>
        <TextAreaField
          label={t('create-question')['instructions']}
          setValue={setValue}
          defaultValue={question?.parent_question_description}
          register={register('parent_question_description', {
            required: t('question-validation-msg')['instruction-required'],
            maxLength: {
              value: 2000,
              message: t(t('question-validation-msg')['instruction-max-length'], ['2000']),
            },
          })}
          style={{ height: '6rem' }}
          className={`form-input-stretch ${errors['parent_question_description'] ? 'error' : ''
            }`}
          viewMode={viewMode}
          specialChar={['#', '*']}
        />
        <ContentField
          className={`form-input-stretch parent-question-text${errors['parent_question_text'] ? 'error' : ''
            }`}
          label={t('create-question')['reading-text']}
          strValue={question?.parent_question_text}
          disableTab={true}
          onChange={onChangeParentQuestionText}
          onBlur={onChangeParentQuestionText}
          style={{ flex: 1 }}
          viewMode={viewMode}
          specialChar={['#', '*']}
        >
          <div className="box-action-info" style={{
            height: 45,
            justifyContent: "flex-start"
          }}>
            <div style={{ display: 'flex', alignItems: "flex-end" }}>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onBold}
                style={
                  {
                    '--image': 'url(/images/icons/ic-bold.png)',
                  } as MyCustomCSS
                }
              ></div>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onItalic}
                style={
                  {
                    '--image': 'url(/images/icons/ic-italic.png)',
                  } as MyCustomCSS
                }
              ></div>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onUnderline}
                style={
                  {
                    '--image': 'url(/images/icons/ic-underline.png)',
                  } as MyCustomCSS
                }
              ></div>
            </div>
          </div>
        </ContentField>
      </div>
      <div style={{ flex: 1 }}>
        <div className="box-wrapper ls-question">
          <label>{t(t('create-question')['task-with-num'], ['1'])}</label>
          <TextAreaField
            label={t('create-question')['question-description']}
            defaultValue={data[0].description}
            onBlur={onChangeQuestionText.bind(null, 0)}
            style={{ height: '6rem' }}
            className={`form-input-stretch ${
              checkError(errors, 'question_description', 0) ? 'error' : ''
            }`}
            viewMode={viewMode}
            specialChar={['#','*']}
          />
          <LikertScaleTable
            className={`form-input-stretch ${
              tfAnswerError[0] === 'true' ? 'error' : ''
            }`}
            answerStr={data[0].answers ?? '###'}
            correctStr={data[0].corrects}
            onChange={onLSTableChange}
            errorStr={tfAnswerError[0]}
            correctErrorStr={tfCorrectError[0] ?? ''}
            viewMode={viewMode}
          />
        </div>
        <div className='box-wrapper mc-question'>
          <label>{t(t('create-question')['task-with-num'], ['2'])}</label>
          <TextAreaField
            label={t('create-question')['question-description']}
            defaultValue={data[1].description}
            onBlur={onChangeQuestionText.bind(null, 1)}
            style={{ height: '6rem' }}
            className={`form-input-stretch ${
              checkError(errors, 'question_description', 1) ? 'error' : ''
            }`}
            viewMode={viewMode}
            specialChar={['#','*']}
          />

          <MultiChoiceGroupQuestion
            questions={data[1].questions}
            answers={data[1].answers}
            corrects={data[1].corrects}
            onQuestionChange={onMCChange}
            errorStr={tfAnswerError[1]}
            correctErrorStr={tfCorrectError[1] ?? ''}
            questionErrorStr={errors['question_text']?.message ?? ''}
            clearInputOutlineStyle={{backgroundColor: '#f5f8fc'}}
            viewMode={viewMode}
            questionType={'MC5'}
          />
        </div>
      </div>
    </div>
  )
}
