import React, { KeyboardEvent, MouseEvent, useCallback, useEffect, useMemo, useRef, useState } from 'react'

import { Button, Tooltip, Whisper } from 'rsuite'

import { ContentField } from 'components/atoms/contentField'
import { InputField } from 'components/atoms/inputField'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionPropsType } from "interfaces/types";
import useTranslation from "@/hooks/useTranslation";

export default function FillInBlank({
  question,
  register,
  setValue,
  errors = {},
  viewMode,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const [selectedValue, setSelectedValue] = useState(null)
  console.log({question, errors, viewMode})
  const inputRef = useRef(null)
  const tableRef = useRef<HTMLDivElement>(null)

  const convertListInput = useCallback((strV: string) => {
    const listV: Answer[][] =
      strV?.split('#').map((m) =>
        m.split('%/%').map((g) => ({
          text: g,
        })),
      ) ?? []
    return listV
  }, [])

  const listInput = useRef(convertListInput(question?.correct_answers))
  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)


  useEffect(() => {
    register('question_text', {
      required: t('question-validation-msg')['question-text-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
      },
      value: question?.question_text,
    })
    register('correct_answers', {
      validate: (value: string) => {
        if(value === '' && listInput.current[0].length === 0) return 'sos'
        console.log('value-----------------', value)
        const result: any = (value ?? '').split('%/%').map((m: any, index) => {
          if(m === ''){
            return index
          }else{
            return null
          }
        }).filter(x => x !== null).join('%/%')
        console.log('result----------------', result)
        return result ? result : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
    
  }, [])

  useEffect(() => {
    if (selectedValue === null) return
    if (isInit.current) {
      isInit.current = false
    } else {
      onUpdateCorrectAnswer()
    }
  }, [selectedValue])

  const onUpdateCorrectAnswer = useCallback(() => {
    setValue(
      'correct_answers',
      listInput.current
        .map((m: any) => m?.map((g: any) => g.text)?.join('%/%'))
        .join('#')
    )
  }, [])

  const onChange = (data: any) => {
    
    setValue(
      'question_text',
      data.replaceAll('##', '%s%').replaceAll('%lb%', '\n'),
    )
    onClosePopup()
  }

  const formatText = useCallback((answerStr: string, corrects: string) => {
    if (!corrects) return answerStr
    let result: any = answerStr
    result = result?.replaceAll('%lb%', '\n')
    const correctList = corrects.split('#')
    for (const correct of correctList) {
      result = result?.replace('%s%', `##`)
    }
    return result
  }, [])

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }
  console.log('listInput.current--------------', listInput.current)
  const onTabCreated = (id: string, index: number) => {
    listInput.current.splice(index, 0, [])
    onUpdateCorrectAnswer()
  }

  const onTabsDeleted = (indexes: number[]) => {
    listInput.current = listInput.current.filter(
      (m, mIndex) => !indexes.includes(mIndex),
    )
    onUpdateCorrectAnswer()
  }

  const onTabClick = (e: MouseEvent<HTMLDivElement>, index: number) => {
    const input = e.target as HTMLInputElement
    setSelectedValue({ index: index, value: listInput.current[index] })

    const inputRect = input.getBoundingClientRect()
    const parentRect = inputRef.current.getBoundingClientRect()
    const offetTop = inputRect.top - parentRect.top + inputRect.height + 10
    let offsetLeft = inputRect.left - parentRect.left + inputRect.width / 2 - 5
    let tableLeft = 0
    if (offsetLeft > 260) {
      tableLeft = offsetLeft - 260
      offsetLeft = 260
    }
    tableRef.current.style.top = `${offetTop}px`
    tableRef.current.style.left = `${tableLeft}px`
    tableRef.current.style.display = 'unset'
    tableRef.current.style.setProperty('--offset-left', `${offsetLeft}px`)
  }

  const onClosePopup = () => {
    setSelectedValue(null)
    tableRef.current.style.display = null
  }

  const onChangeAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value
    const item = listInput.current[selectedValue.index]
    item[answerIndex].text = e.target.value
    setSelectedValue({ ...selectedValue })
  }
  const onBlurAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value = e.target.value.trim()
    const item = listInput.current[selectedValue.index]
    item[answerIndex].text = e.target.value
    setSelectedValue({ ...selectedValue })
  }

  const onAddAnswer = () => {
    selectedValue.value.push({ text: '' })
    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })
  }

  const onDeleteAnswerText = (answerIndex: number) => {
    selectedValue.value.splice(answerIndex, 1)
    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })
    console.log(selectedValue)
  }

  const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
    }
  }, [])

  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)
  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }

  const errorFieldCAnswers: string[] =  (errors['correct_answers']?.message ?? '').split('%/%') ?? []
  // const errorFieldCAnswers: string[] = []
  console.log('errorFieldCAnswers----------', errorFieldCAnswers)
  return (
    <div className="m-fill-in-blank" style={{ display: 'flex' }}>
      <div
        className="box-wrapper"
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          padding: 0,
        }}
      >
        <div
          style={{
            padding: '40px 30px',
            backgroundColor: 'rgba(255, 200, 51, 0.3)',
            borderRadius: '32px',
          }}
        >
          <InputField
            label={t('create-question')['keyword']}
            type="text"
            setValue={setValue}
            register={register('answers', {
              required: t('question-validation-msg')['keyword-required'],
              maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['keyword-max-length'], ['10000']),
              },
            })}
            defaultValue={question?.answers}
            className={`form-input-stretch ${errors['answers'] ? 'error' : ''}`}
            style={{ width: '200px', margin: 0 }}
          />
        </div>
      </div>
      <div className="box-wrapper">
        <TextAreaField
          setValue={setValue}
          label={t('create-question')['question-description']}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          style={{ height: '9rem' }}
          viewMode={viewMode}
        />
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <ContentField
            ref={inputRef}
            label={t('common')['question']}
            strValue={formatText(
              question?.question_text,
              question?.correct_answers,
            )}
            onChange={onChange}
            isMultiTab={false}
            disableTabInput={true}
            onTabClick={onTabClick}
            onTabCreated={onTabCreated}
            onTabsDeleted={onTabsDeleted}
            className={
              errors['question_text'] || errors['correct_answers']
                ? 'error'
                : ''
            }
            viewMode={viewMode}
            specialChar={['*','#']}
          >
            <div className="box-action-info">
              <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                <>
                  <div
                      className="icon-mask icon-action-style-highlight"
                      onMouseDown={onBold}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-bold.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                  <div
                      className="icon-mask icon-action-style-highlight"
                      onMouseDown={onItalic}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-italic.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                  <div
                      className="icon-mask icon-action-style-highlight"
                      onMouseDown={onUnderline}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-underline.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                </>
                <Whisper
                    placement="top"
                    trigger={'hover'}
                    speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
                >
                  <Button className="add-new-answer" onMouseDown={onAddSpace}>
                    <img src="/images/icons/ic-underscore.png" alt="" />
                  </Button>
                </Whisper>
              </div>
            </div>
          </ContentField>
          {/* <div className="box-action-info">
            <span className="add-new-answer" onMouseDown={onAddSpace}>
              <img src="/images/icons/ic-plus-sub.png" width={17} height={17} />{' '}
              Thêm khoảng trống
            </span>
          </div> */}
          <div ref={tableRef} className="popup-table">
            {selectedValue && (
              <table className={`question-table`}>
                <thead>
                  <tr>
                    <th align="left">{t('create-question')['correct-answer']}</th>
                    <th className="action" style={{ width: '60px' }}>
                      <img
                        src="/images/icons/ic-close-dark.png"
                        width={20}
                        height={20}
                        onClick={onClosePopup}
                        style={{ pointerEvents: 'all', cursor: 'pointer' }}
                        alt="close-icon"
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {selectedValue?.value?.map(
                    (answer: Answer, answerIndex: number) => (
                      <tr key={`${groupKey}_${answerIndex}`}>
                        <td>
                          <input
                            type="text"
                            value={answer.text}
                            onKeyDown={onKeyDown}
                            onChange={onChangeAnswerText.bind(
                              null,
                              answerIndex,
                            )}
                            onBlur={onBlurAnswerText.bind(
                              null,
                              answerIndex,
                            )}
                            className={(listInput.current[0].length !== 0) && (errorFieldCAnswers.includes(`${answerIndex}`)) ? 'error' : ''}
                          />
                        </td>
                        <td align="center">
                          <img
                            onClick={onDeleteAnswerText.bind(null, answerIndex)}
                            className="ic-action"
                            src="/images/icons/ic-trash.png"
                            alt="delete-answer-icon"
                          />
                        </td>
                      </tr>
                    ),
                  )}
                  {!viewMode && (
                    <tr className="action">
                      <td colSpan={2} align="left">
                        <span className={`add-new-answer ${(listInput.current[0].length === 0 && errors['correct_answers']) ? 'error' : ''}`} onClick={onAddAnswer}>
                          <img
                            src="/images/icons/ic-plus-sub.png"
                            width={17}
                            height={17}
                            alt="add-new-answer-icon"
                          />{' '}
                          {t('create-question')['add-answer']}
                        </span>
                      </td>
                    </tr>
                  )}
                </tbody>
              </table>
            )}
          </div>
        </div>
      </div>
    </div>
  )
}

type Answer = {
  text: string
}
