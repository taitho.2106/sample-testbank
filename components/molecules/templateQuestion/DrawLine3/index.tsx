import React, {
  useEffect,
  useState,
  useRef,
  CSSProperties,
  KeyboardEvent,
} from 'react'

import { Button, Toggle } from 'rsuite'

import { SplitImage } from 'components/atoms/SplitImage/Index'
import { TextAreaField } from 'components/atoms/textAreaField'
import { ContentItemPosType, PosType, QuestionPropsType } from 'interfaces/types'
import coordinates from 'utils/coordinates'
import Guid from 'utils/guid'
import useTranslation from '@/hooks/useTranslation'

type DataImageType = {
  data: string
  deleted: boolean
  key: string
  name?: string
  isExample?: boolean
}
type ImageCurrentInfoType = {
  src: string
  width: number
  height: number
}
const sizeWordExport = 680
export default function DrawLine3({
  question,
  register,
  setValue,
  isImage = false,
  isReading = false,
  viewMode = false,
  errors = {},
  questionType,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const getFirstDataImage = (correct_answers: string) => {
    const correct_answersArr = correct_answers?.split('#') || []
    const tempArr: DataImageType[] = correct_answersArr.map(
      (item: any, i: number) => {
        const key = Guid.newGuid()
        const itemCorrectArr = item?.split('*') || []
        return {
          name: itemCorrectArr[2],
          key: key,
          isExample: itemCorrectArr[0].startsWith('ex|'),
          data: '',
          deleted: false,
        }
      },
    )
    return tempArr
  }

  const splitImageRef = useRef(null)
  const imageRef = useRef(null)
  const [jsonData, setJsonData] = useState(null)
  const df = getFirstDataImage(question?.correct_answers)
  const listImageRef = useRef(df)
  const [listImage, setListImage] = useState<DataImageType[]>(
    listImageRef.current,
  )
  const timeOutImageWord = useRef(null)
  const [indexSelected, setIndexSelected] = useState(-1)
  const imgCurrentInfoRef = useRef<ImageCurrentInfoType>({
    src: '',
    width: 0,
    height: 0,
  })

  useEffect(() => {
    const getData = async () => {
      try {
        if (!question?.answers) return
        const res = await fetch(`/${question.answers}`, {
          method: 'GET',
        })
        const json = await res.json()
        if (json && Array.isArray(json)) {
          const imageArr = question?.image.split('*') || []
          if (json[0]?.listPos) {
            //old:
            const positionArr = json.map((item: any, i: number) => {
              const key = listImageRef.current[i]?.key
              return { ...item, key: key }
            })
            setJsonData({
              imageUrl: imageArr.length > 0 ? `/upload/${imageArr[0]}` : '',
              positionArr,
            })
          } else {
            //new
            const positionArr = handleStringPositionToLoad(json)
            setJsonData({
              imageUrl: imageArr.length > 0 ? `/upload/${imageArr[0]}` : '',
              positionArr,
            })
          }
        }
      } catch (ex) {
        console.log('ex======-----------------------', ex)
      }
    }
    getData()
  }, [])
  useEffect(() => {
    if (splitImageRef.current) {
      const listPos = splitImageRef?.current?.getImagePostions()
      const errorImageArr: any[] = []
      let imageData: any[] = []
      let countExample = 0
      if (listPos && listPos.length > 0) {
        imageData = listImage.map((m, index) => {
          if (m.isExample) countExample += 1
          const imageErr = !m.data
          const nameErr = !m.name
          const exampleErr = m.isExample && countExample > 1
          const isErr = imageErr || nameErr || exampleErr

          errorImageArr.push({
            imageErr,
            nameErr,
            exampleErr,
            isErr,
            key: m.key,
          })
          let coorMaxMin = {    minX: 0,
            minY: 0,
            maxX: 0,
            maxY: 0}
            if(listPos[index]?.listPos){
              coorMaxMin = coordinates.getMaxMinXY(listPos[index].listPos)
            }
          return { ...m, ...coorMaxMin }
        })
      }
      setValue('list_answer_item', JSON.stringify(imageData))
      setValue('list_answer_item_error', errorImageArr)
    }
    let countQuestion = 0
    let countExample = 0
    listImage.forEach((m) => {
      if (m.isExample) {
        countExample += 1
      } else {
        countQuestion += 1
      }
    })
    if (countExample > 1 || countQuestion == 0) {
      setValue('passData', false)
    } else {
      setValue('passData', true)
    }
    // console.log('lisstimage-----useEffect', listImage)
    // const imageExportElemet = document.getElementById("image-export-question-id") as HTMLImageElement;
    // if(imageExportElemet){
    //   imageExportElemet.src = imgCurrentInfoRef.current.src;
    // }
    if(timeOutImageWord.current){
      clearTimeout(timeOutImageWord.current)
    }
    timeOutImageWord.current = setTimeout(() => {
      drawImageExportQuestion()
    }, 150)

    //check style btn add new answer
    setTimeout(() => {
      const scrollElement = document.getElementById(
        'class-image-list-content-id',
      )
      if (scrollElement) {
        if (scrollElement.scrollWidth > scrollElement.offsetWidth) {
          const addBtnElement = document.getElementById('add-new-answer-id')
          if (addBtnElement) {
            if (!addBtnElement.classList.contains('scroll')) {
              addBtnElement.classList.add('scroll')
            }
          }
        } else {
          const addBtnElement = document.getElementById('add-new-answer-id')
          if (addBtnElement) {
            if (addBtnElement.classList.contains('scroll')) {
              addBtnElement.classList.remove('scroll')
            }
          }
        }
      }
    }, 10)
  }, [listImage])
  useEffect(() => {
    register('passData', {
      validate: (value: string) => {
        return value
      },
    })
    register('image_question_base64', {
      required: t('question-validation-msg')['image_question_base64'],
    })
    register('image_question_export_base64', {
      required: t('question-validation-msg')['image_question_export_base64'],
    })
    register('list_answer_item', {
      validate: (value) => {
        if (!value) return t('question-validation-msg')['canvas-answers']
        try {
          const arr = JSON.parse(value)
          if (Array.isArray(arr) && arr.length == 0)
            return t('question-validation-msg')['canvas-answers']
        } catch {}
        return true
      },
      required: t('question-validation-msg')['canvas-answers'],
    })
    register('list_answer_item_error', {
      validate: (value) => {
        if (!value) return true
        // const value = JSON.parse(value)
        const indexErr = value.findIndex((m: any) => m.isErr)
        if (indexErr == -1) {
          return true
        } else {
          return JSON.stringify(value)
        }
      },
    })
  }, [])
  const onToggleExample = (key: string) => {
    const tempArr: DataImageType[] = listImage.map((m) => {
      if (m.key == key) {
        return { ...m, isExample: !m.isExample }
      } else {
        return m
      }
    })
    console.log('onToggleExample-----------', key)
    listImageRef.current = tempArr
    setListImage([...tempArr])
  }
  const itemImageErrArr: any[] = errors['list_answer_item_error']?.message
    ? JSON.parse(errors['list_answer_item_error']?.message)
    : []
  const exampleErr = listImage.filter((m) => m.isExample).length > 1
  const answerErr = listImage.filter((m) => !m.isExample).length == 0
  const listAnswerErr = errors['list_answer_item']
  const imageErr = errors['image_question_base64']
  // console.log("listAnswerErr----",listAnswerErr)
  // console.log("imgUrl----",imgUrl)
  // console.log("listAnswerErr && !imgUrl----",(listAnswerErr && !imgUrl))
  const drawImageExportQuestion = () => {

    setValue('image_question_export_base64', '')
    if(!imgCurrentInfoRef.current || !imgCurrentInfoRef.current.src ){
      console.debug("noimage---")
      return
    }
    const container = document.getElementById(
      'content-draw-image-export-id',
    ) as HTMLElement
    if (!container || !splitImageRef?.current) return
    const canvas = document.getElementById(
      'canvas-export-id',
    ) as HTMLCanvasElement
    canvas.setAttribute('height', `${container.offsetHeight}`)
    canvas.setAttribute('width', `${container.offsetWidth}`)
    const imageTarget = document.getElementById(
      'image-export-question-id',
    ) as HTMLCanvasElement

    const leftImage = imageTarget.offsetLeft
    const topImage = imageTarget.offsetTop
    const indexExample = listImage.findIndex((m) => m?.isExample)
    let ratio = 1
    if (imageTarget.offsetWidth < imgCurrentInfoRef.current.width) {
      ratio = imageTarget.offsetWidth / imgCurrentInfoRef.current.width
      console.log('ratio', ratio)
    }
    
     
      const ctxCanvasDraw: any = canvas.getContext('2d')
      
      ctxCanvasDraw.drawImage(
        imageTarget,
        canvas.width / 2 - imageTarget.offsetWidth / 2,
        canvas.height / 2 - imageTarget.offsetHeight / 2,
        imageTarget.offsetWidth,
        imageTarget.offsetHeight,
      )
      if (indexExample != -1) {
        let leftExample = 10
      const topExample = 0

      let divide = 5
      const lenAnswer = listImage.length
      if (lenAnswer <= 4) {
        divide = lenAnswer
      } else if (lenAnswer <= 8) {
        divide = 4
      }
      const listPos = splitImageRef?.current?.getImagePostions()
      const posExample = listPos[indexExample]
      
        if (posExample){
          leftExample = sizeWordExport / divide / 2
      const maxMin = coordinates.getMaxMinXY(posExample.listPos)
      let direction = "topLeft";
      if(leftImage + maxMin.minX <= leftExample){
        if(leftImage + maxMin.maxX <=leftExample){
          direction ='topRight'
        }else{
          direction ='top'
        }
      }
      const position = coordinates.getPointPosition(
        posExample.listPos,
        direction as any,
      )
          ctxCanvasDraw.beginPath()
          ctxCanvasDraw.moveTo(leftExample, topExample)
          ctxCanvasDraw.lineTo(
            position.x * ratio + leftImage,
            position.y * ratio + topImage,
          )
          ctxCanvasDraw.stroke()
          ctxCanvasDraw.fill()
        }
      }
     
      const imageData = canvas.toDataURL('image/png', 1)
      setValue('image_question_export_base64', imageData)
  }
  let answerCount = 0;

  const onChangeItem = (dataImage:any)=>{
    if (dataImage) {
      const tempArr: DataImageType[] = dataImage.map(
        (m: DataImageType, index: number) => {
          const item = listImageRef.current.find(
            (n) => n.key == m?.key,
          )
          return {
            key: m.key,
            data: m.data,
            name: item?.name || '',
            isExample: item?.isExample,
          }
        },
      )
      listImageRef.current = tempArr
      setListImage(tempArr)
      //set value position.
      const listPos: ContentItemPosType[] = splitImageRef?.current?.getImagePostions()
      const listDataPos = handleStringPositionToSave(listPos)
      setValue('list_image_item_position', JSON.stringify(listDataPos))
    } else {
      setListImage([])
      setValue('list_image_item_position', JSON.stringify([]))
    }
  }
  const handleStringPositionToSave = (listPos: ContentItemPosType[]) => {
    try {
      const listDataPos: any[] = []
      listPos.forEach((item) => {
        const dataColor = item.colorData
        const dataColorStr: any = {}
        const listTemp: any[] = []
        Object.keys(dataColor).forEach(function (key) {
          dataColorStr[key] = dataColor[key].join('*')
          listTemp.push(`${key}|${dataColor[key].join('*')}`)
        })
        listDataPos.push({ strPos: listTemp.join('#'), key: item.key })
      })
      return listDataPos
    } catch {
      return [] as any[]
    }
  }
  const handleStringPositionToLoad = (json: any[]) => {
    try {
      const positionArr = json.map((item: any, i: number) => {
        const itemsWithColor = item.strPos.split('#') || []
        const key = listImageRef.current[i]?.key
        const listPos: PosType[] = []
        itemsWithColor.forEach((m: string) => {
          const items = m.split('|')
          const color = items[0]
          items[1].split('*').forEach((m) => {
            const xy = m.split(':')
            const poss: PosType = { x: parseInt(xy[0]), y: parseInt(xy[1]), c: `#${color}` }
            listPos.push(poss)
          })
        })
        return { listPos: listPos, key: key }
      })
      return positionArr
    } catch {
      return []
    }
  }
  const onChangeImageData = (data:any)=>{
    const boxImage = document.querySelector(".box-image-handle-answer") as HTMLElement;
    if(data.src){
     if(boxImage.classList.contains("no-image")){
      boxImage.classList.remove("no-image")
     }
    }else{
      if(!boxImage.classList.contains("no-image")){
        boxImage.classList.add("no-image")
       }
       if (imageRef.current) imageRef.current.value = null
    }
    const imgExportElement = document.getElementById(
      'image-export-question-id',
    ) as HTMLImageElement
    if (imgExportElement) {
      imgExportElement.src = data.src
    }
    imgCurrentInfoRef.current = data
    setValue('image_question_base64', data.src)
  }
  return (
    <div className={`m-draw-line-template-3`}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          flex: '1',
          width: '100%',
        }}
      >
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            label={t('create-question')['question-description']}
            setValue={setValue}
            defaultValue={question?.question_description}
            register={register('question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['description-max-length'], ['10000']),
              },
            })}
            style={{ height: '4rem', marginLeft: '0', width: '100%' }}
            className={`form-input-stretch ${
              errors['question_description'] ? 'error' : ''
            }`}
            viewMode={viewMode}
          />

          <div className={`box-image-handle-answer ${jsonData && jsonData.imageUrl?"":"no-image"}`}>
            {
              <SplitImage
                className={`${imageErr ? 'error' : ''}`}
                ref={splitImageRef}
                jsonData={jsonData}
                viewMode={viewMode}
                onChangeItemIndexSelected={(indexSelected) => {
                  setIndexSelected(indexSelected)
                }}
                onChangeItem={(dataImage) => onChangeItem(dataImage)}
                onChangeImageData={(data) => onChangeImageData(data)}
                onClickChangeImage={() => {
                  imageRef.current.dispatchEvent(new MouseEvent('click'))
                }}
              ></SplitImage>
            }
            <input
              ref={imageRef}
              type="file"
              style={{ display: 'none' }}
              accept=".png,.jpeg,.jpg"
              onChange={(e) => {
                if (imageRef.current.files.length > 0) {
                  const file = imageRef.current.files[0]
                  if (['image/jpeg', 'image/png'].includes(file?.type)) {
                    const url = URL.createObjectURL(imageRef.current.files[0])
                    splitImageRef.current &&
                      splitImageRef.current.changeImageUrl(url)
                  }
                }
              }}
            />
            <div className="text-user-guide">
              <img alt="" src="/images/icons/ic-info-blue.png"></img>
              <span>{t('create-question')['chosen-image']}</span>
            </div>
            <div
              className="class-image-list"
              style={
                !listImage || listImage.length == 0 ? { height: '13.2rem' } : {}
              }
            >
              {listImage && (
                <div
                  id="class-image-list-content-id"
                  className="class-image-list__content"
                  style={listImage.length==10?{maxWidth:"100%"}:{}}
                >
                  {listImage.map((item: DataImageType, indexItem: number) => {
                    const itemErr = itemImageErrArr.find(
                      (m) => m.key == item.key && m.isErr,
                    )
                    if(!item.isExample) answerCount +=1;
                    return (
                      item && (
                        <div
                        data-id={`key_${item.key}`}
                          className={`class-image-list__content__card-image ${viewMode?"view-mode":""}`}
                          key={item.key}
                        >
                          <div
                            className={`box-image ${
                              indexItem == indexSelected ? 'selected' : ''
                            } ${item.isExample?"example":""}`}
                          >
                            <img
                              className="image-delete-icon"
                              src="/images/icons/ic-remove-opacity.png"
                              alt=""
                              onClick={() => {
                                event.preventDefault()
                                event.stopPropagation()
                                splitImageRef.current &&
                                  splitImageRef.current.removeItem(indexItem)
                              }}
                            />

                            <div
                              className={`__content-image ${
                                itemErr?.imageErr ? 'error' : ''
                              }`}
                              onClick={() => {
                                splitImageRef.current &&
                                  splitImageRef.current.selectItem(indexItem)
                              }}
                            >
                              <img
                                alt="img"
                                className={`${item.data?"":"img-default"}`}
                                src={item.data || '/images/icons/ic-image.png'}
                              />
                            </div>

                            <textarea
                              className={`${itemErr?.nameErr ? 'error' : ''}`}
                              style={{ width: '100%' }}
                              defaultValue={item?.name || ''}
                              maxLength= {1000}
                              onKeyDown={(event) => {
                                if(viewMode) event.preventDefault()
                                if (event.keyCode == 13) {
                                  event.preventDefault()
                                }
                                if (event.key == '*' || event.key == '#') {
                                  event.preventDefault()
                                }
                              }}
                              placeholder={`${item.isExample ? t('common')['example'] : `${t('common')['question']} ${answerCount}`}`}
                              onChange={(event) => {
                                event.target.value = event.target.value
                                  .replace(/(\*|\#)/g, '')
                                  .replace(/\r?\n|\r/g, '')
                                listImage[indexItem].name = event.target.value
                                listImageRef.current = listImage
                                setListImage([...listImage])
                              }}
                              onBlur={(event) => {
                                event.target.value = event.target.value.trim()
                                  .replace(/(\*|\#)/g, '')
                                  .replace(/\r?\n|\r/g, '')
                                listImage[indexItem].name = event.target.value
                                listImageRef.current = listImage
                                setListImage([...listImage])
                              }}
                            ></textarea>
                          </div>
                          <div
                            className={`__content-toggle ${
                              exampleErr && item.isExample ? 'error' : ''
                            }`}
                          >
                            <Toggle
                              size="md"
                              className="__switcher"
                              disabled={viewMode}
                              checked={!item.isExample}
                              checkedChildren={t('common')['question']}
                              unCheckedChildren={t('common')['example']}
                              onChange={(e) => onToggleExample(item.key)}
                            />
                          </div>
                        </div>
                      )
                    )
                  })}
                </div>
              )}
              {listImage.length < 10 && (
                <div
                  className={`add-new-answer`}
                  id="add-new-answer-id"
                  style={
                    !listImage || listImage.length == 0
                      ? { borderRadius: '0.8rem', alignItems: 'center' }
                      : {}
                  }
                >
                  <div
                    className={`add-new-answer__btn ${
                      (!listImage || listImage.length == 0) && listAnswerErr
                        ? 'error'
                        : ''
                    }`}
                    onClick={() => {
                      console.log('splitImageRef----', splitImageRef)
                      imgCurrentInfoRef.current && imgCurrentInfoRef.current.src && splitImageRef.current &&
                        splitImageRef.current.addNewItem()
                        setTimeout(() => {
                          const scrollElement = document.getElementById(
                            'class-image-list-content-id',
                          )
                          if (scrollElement) {
                            if (scrollElement.scrollWidth > scrollElement.offsetWidth) {
                              scrollElement.scrollTo(scrollElement.scrollWidth, 0)
                            }
                          }
                        }, 50);

                    }}
                  >
                    <img alt="" src="/images/icons/ic-plus-sub.png"></img>
                    <span>{t('create-question')['add-answer']}</span>
                  </div>
                </div>
              )}
              {/* <button
                type="button"
                onClick={() => {
                  splitImageRef.current && splitImageRef.current.addNewItem()
                }}
              >
                Thêm đáp án
              </button> */}
            </div>
            <div className="text-user-guide">
              <img alt="" src="/images/icons/ic-info-blue.png"></img>
              <span>
                {' '}{t('create-question')['chosen-answer-des']}
              </span>
            </div>
          </div>
        </div>
        {(answerErr || exampleErr) && (
          <div className="form-error-message">
            {answerErr && <span>{`* ${t(t('question-validation-msg')['add-answer-with-num'],['1'])}`}</span>}
            {exampleErr && <span>{`* ${t(t('question-validation-msg')['example-allow-total'],['1'])}`}</span>}
          </div>
        )}
        <div style={{ width: `${sizeWordExport}px`, position: 'fixed', zIndex:'-11', visibility:"hidden"}}>
          <div
            id="content-draw-image-export-id"
            style={{
              width: '100%',
              padding: '1rem 8rem',
              display: 'flex',
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <img
              id="image-export-question-id"
              alt=""
              style={{
                maxWidth: `${sizeWordExport}px`,
                visibility:"hidden",
                maxHeight: '338px',
              }}
              onLoad={(e) => {
                const container = document.getElementById(
                  'content-draw-image-export-id',
                ) as HTMLElement
                const canvas = document.getElementById(
                  'canvas-export-id',
                ) as HTMLCanvasElement
                canvas.setAttribute('height', `${container.offsetHeight}`)
                canvas.setAttribute('width', `${container.offsetWidth}`)
              }}
            ></img>
          </div>
          <canvas
            id="canvas-export-id"
            style={{ position: 'absolute', top: 0, left: 0 }}
          ></canvas>
        </div>
      </div>
    </div>
  )
}
type PropsType = {
  labelDefault?: string
  isError?: any
  url: string
  onChangeFile?: (file: FileList) => void
  viewMode?: boolean
  style?: CSSProperties
}

type Answer = {
  text: string
}
