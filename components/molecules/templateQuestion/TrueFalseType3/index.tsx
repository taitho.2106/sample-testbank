import { useEffect, useRef, useState } from 'react'

import { InputField } from 'components/atoms/inputField'
import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import ImageInput from '../ImageInput'
import TrueFalseTableType3 from './table'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation'

export default function TrueFalseType3({
  question,
  register,
  setValue,
  isImage = false,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const [imgUrl, setImgUrl] = useState(question?.image)
  const [arrNewIndex, setArrNewIndex] = useState([])
  useEffect(() => {
    if (isImage) {
      register('image_file', {
        validate: {
          acceptedFormats: async(file) => {
            // check format if has file
            if (!file) {
              return true
            }
            if(file.size ==0){
              return t('question-validation-msg')['image-deleted']
            }
            if(!['image/jpeg', 'image/png'].includes(file?.type)){
              return t('question-validation-msg')['image-file']
            }
            if(!await browserFile.checkFileExist(file)){
              return t('question-validation-msg')['image-deleted']
            }
            return true;
          },
        },
      })
      register('image', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['image-max-length'], ['2000']),
        },
        required: t('question-validation-msg')['image-required'],
        value: question?.image,
      })
    }
    register('answers', {
      validate: (value: string) => {
        const errs = value
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return arrNewIndex[mIndex]
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '###',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const ansl = ['T', 'F']
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (!ansl.includes(m)) return arrNewIndex[mIndex]
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers ?? '###',
    })
  }, [arrNewIndex])

  const onCorrectAnswerChange = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers)
  }

  const onChangeFile = (files: FileList) => {
    if (files && files[0]) {
      setValue('image_file', files[0])
      const urlImage = URL.createObjectURL(files[0])
      setValue('image', urlImage)
      setImgUrl(urlImage)
    } else {
      setValue('image_file', null)
      setValue('image', '')
      setImgUrl('')
    }
  }

  return (
    <div className="m-true-false-03" style={{ display: 'flex' }}>
      <div className="box-wrapper-tf-03">
        <TextAreaField
          label={t('create-question')['question-description']}
          name="text-tf3"
          setValue={setValue}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          defaultValue={question?.question_description}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          style={{height:'5rem'}}
          viewMode={viewMode}
          specialChar={['#','*']}
        />
        {isImage && (
          <ImageInputTF3
            url={imgUrl}
            onChangeFile={onChangeFile}
            isError={errors['image'] || errors['image_file']}
            viewMode={viewMode}
          />
        )}
      </div>
      <div style={{ minWidth: '55%', flex: 1 }}>
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TrueFalseTableType3
            className={`form-input-stretch`}
            answerStr={question?.answers ?? '###'}
            correctStr={question?.correct_answers ?? '###'}
            onChange={onCorrectAnswerChange}
            errorStr={errors['answers']?.message ?? ''}
            correctErrorStr={errors['correct_answers']?.message ?? ''}
            setValue={setValue}
            setArrNewIndex={setArrNewIndex}
            register={register}
            viewMode={viewMode}
          />
        </div>
      </div>
    </div>
  )
}

type PropsType = {
  isError?: boolean
  url: string
  onChangeFile?: (files: FileList) => void
  viewMode?: boolean
}
function ImageInputTF3({ isError, url, onChangeFile, viewMode }: PropsType) {
  const {t} = useTranslation()
  const [imageUrl, setImageUrl] = useState(url ?? '')
  const fileRef = useRef(null)
  const src = imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`
  return (
    <div
      className={`box-image-input-tf3 ${imageUrl !== '' ? 'has-data' : ''} ${
        isError ? 'error' : ''
      }`}
    >
      <div className="image-section">
        <div
          className="image-input-origin"
          onClick={() => fileRef.current.dispatchEvent(new MouseEvent('click'))}
        >
          {src && url ? (
            <>
              <img className="image-upload" src={src} alt="image-upload" />
              {!viewMode && src && url && (
                <img
                  className="image-delete-icon"
                  src="/images/icons/ic-remove-opacity.png"
                  alt=""
                  onClick={(event) => {
                    fileRef.current.value = ''
                    event.preventDefault()
                    event.stopPropagation()
                    onChangeFile(null)
                  }}
                />
              )}
            </>
          ) : (
            <>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                }}
              >
                <img className="icon" src="/images/icons/ic-image.png" alt="delete-img-icon" />
                {t('create-question')['image']}
              </div>
            </>
          )}
          <input
            ref={fileRef}
            type="file"
            style={{ display: 'none' }}
            accept=".png,.jpeg,.jpg"
            onChange={(e) => {
              if (fileRef.current.files.length > 0) {
                setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                onChangeFile && onChangeFile(e.target.files)
              }
            }}
          />
        </div>
      </div>
    </div>
  )
}
