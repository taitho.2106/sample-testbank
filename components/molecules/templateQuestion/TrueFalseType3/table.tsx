import { ChangeEvent, useCallback, useEffect, useRef, useState } from "react";

import ContentEditable from 'react-contenteditable'
import { Toggle } from 'rsuite'
import Guid from 'utils/guid'
import { MyCustomCSS } from "@/interfaces/types";
import { convertStrToHtml } from "@/utils/string";
import useTranslation from "@/hooks/useTranslation";

type PropType = {
  setValue?: any
  answerStr: string
  correctStr: string
  styleBold?: boolean
  className?: string
  errorStr?: string
  correctErrorStr?: string
  onChange: (data: any) => void
  setArrNewIndex: (data: any) => void
  register: any
  viewMode?:boolean
}

export default function TrueFalseTableType3({
  setValue,
  answerStr,
  correctStr,
  className = '',
  errorStr = '',
  correctErrorStr = '',
  onChange,
  setArrNewIndex,
  register,
  viewMode
}: PropType) {
  const {t} = useTranslation()
  useEffect(() => {
    register('isExam', {
      validate: (value: string) => {
        const errs = (value ?? '').split('#')
        const errE = errs.filter((item: string) => item === 'false')
        const errA = errs.filter((item: string) => item === 'true')
        if (errE.length > 3) return t(t('question-validation-msg')['example-allow-total'], ['3'])
        if (errA.length < 1) return t(t('question-validation-msg')['question-fb-required'], ['1'])
        return true
      },
    })
  }, [])
  const listAnswer = answerStr.split('#')
  const listCorrect = correctStr?.split('#') ?? []
  const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
    listAnswer.map((m: string, mIndex: number) => {
      const list: any[] = listAnswer
      if (list[mIndex].startsWith('*')) {
        list[mIndex] = list[mIndex].replace('*', '')
      }
      return {
        key: Guid.newGuid(),
        text: list[mIndex],
        correct: listCorrect[mIndex],
        isExam: m.startsWith('*') ? false : true,
      }
    }),
  )

  const answerGroupRef = useRef(answerGroup)
  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  useEffect(() => {
    answerGroupRef.current = answerGroup
    const example = []
    const ans = []
    const arrIndexEx = []
    const arrIndexAns = []
    for(let i=0; i<answerGroup.length;i++){
      if(answerGroup[i].isExam){
        ans.push(answerGroup[i])
        arrIndexAns.push(i)
      }else{
        example.push(answerGroup[i])
        arrIndexEx.push(i)
      }
    }
    if (answerGroup) {
      setValue('answers', [...example, ...ans].map((m) => m.text).join('#'))
      setValue(
        'correct_answers',
        [...example, ...ans].map((m) => m.correct).join('#'),
      )
      setValue('isExam', [...example, ...ans].map((m) => m.isExam).join('#'))
      setArrNewIndex([...arrIndexEx, ...arrIndexAns])
    }
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        onChange({
          answers: [...example, ...ans].map((m) => (m.text ?? '').trim()).join('#'),
          correct_answers: [...example, ...ans].map((m) => m.correct).join('#'),
        })
      }
    }

    setTimeout(() => {
      Array.from(document.getElementsByClassName(`${groupKey}`)).forEach(
        (el) => {
          const div = el as HTMLDivElement
          div.style.width = `${div.offsetWidth}px`
        },
      )
    }, 0)
  }, [answerGroup])

  const onAddAnswer = () => {
    const newAnswer = {
      key: Guid.newGuid(),
      text: '',
      correct: '',
      isExam: true,
    }
    if (answerGroup) {
      setAnswerGroup([...answerGroup, newAnswer])
    } else {
      setAnswerGroup([newAnswer])
    }

    setTimeout(() => {
      document
        .getElementById(`${groupKey}_${answerGroup?.length ?? 0}`)
        ?.focus()
    }, 0)
  }

  const onChangeIsCorrect = (
    index: number,
    event: ChangeEvent<HTMLInputElement>,
  ) => {
    answerGroup[index].correct = event.target.dataset.value
    setAnswerGroup([...answerGroup])
  }

  const onChangeText = (index: number, event: any) => {
    const el: HTMLElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    answerGroup[index].text = text.replace(/(\*|\#)/g, '')
    setAnswerGroup([...answerGroup])
  }
  const onBlurText = (index: number, event: any) => {
    const el: HTMLElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    answerGroup[index].text = text.replace(/(\*|\#)/g, '').trim()
    el.innerHTML = convertStrToHtml(answerGroup[index].text)
    // setAnswerGroup([...answerGroup])
  }

  const onDeleteAnswer = (index: number) => {
    const ans: any[] = answerGroup.filter(
      (m: any, mIndex: any) => mIndex !== index,
    )
    setAnswerGroup([...ans])
  }

  const handleToggle = (id: number, value: boolean) => {
    answerGroup[id].isExam = !value
    setAnswerGroup([...answerGroup])
  }

  const listExamError = answerGroup.filter((item: any) => item.isExam === false)
  const listAnsError = answerGroup.filter((item: any) => item.isExam === true)
  const listError = errorStr.split(',')
  const listCError = correctErrorStr.split(',')
  const [err, setErr] = useState(false)
  const [error, setError] = useState('')

  const renderError = () => {
    if (listExamError.length > 3) {
      setErr(true)
      return setError(t(t('question-validation-msg')['example-allow-total'], ['3']))
    }
    if (listAnsError.length < 1) {
      setErr(true)
      return setError(t(t('question-validation-msg')['question-fb-required'], ['1']))
    }
    return setError('')
  }

  useEffect(() => {
    renderError()
  }, [listExamError, listAnsError])

  let indexE = -1
  let indexA = -1
  const convertHtmlToStr = useCallback((dataD: ChildNode[]) => {
    let str = ''
    for (let i = 0, len = dataD.length; i < len; i++) {
      const child = dataD[i] as Node
      if (child.nodeName === '#text') {
        str += child.textContent
      } else if (child.nodeName === 'SPAN') {
        str += child.firstChild.textContent
      } else if (child.nodeName === 'BR') {
        // str += '\n'
      } else if (child.nodeName === 'INPUT') {
        const inputEl = child as HTMLInputElement
        str += `#${inputEl.value}#`
      } else {
        const el = child as Element
        const tag = child.nodeName.toLowerCase()
        const content = convertHtmlToStr(Array.from(el.childNodes))
        if (content !== '') {
          str += `%${tag}%${content}%${tag}%`
        }
      }
    }
    return str
  }, [])
  const onPaste = (event: any) => {
    event.preventDefault()
    let data = event.clipboardData.getData('text').replace(/(\#|\*)/g, '')
    data = data?.replace(/\r?\n|\r/g, '')
    const selection = window.getSelection()
    const range = selection.getRangeAt(0)
    range.deleteContents()
    const node = document.createTextNode(data)
    range.insertNode(node)
    selection.removeAllRanges()
    const newRange = document.createRange()
    newRange.setStart(node, data.length)
    selection.addRange(newRange)
  }
  const onBold = () => {
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    const range = selection.getRangeAt(0)
    const parent = range.commonAncestorContainer
    let el: HTMLElement = null
    let parentEl =
        parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
    while (parentEl.parentElement) {
      if (parentEl.classList.contains('div-input')) {
        el = parentEl
        break
      }
      parentEl = parentEl.parentElement
    }
    document.execCommand('Bold', false, null)
    if (el) {
      const index = parseInt(el.id.replace(`${groupKey}_`, ''))
      let text = ''
      for (let i = 0, len = el.childNodes.length; i < len; i++) {
        const childNode = el.childNodes[i]
        text += convertHtmlToStr([childNode])
      }
      answerGroup[index].text = text.replace(/(\*|\#)/g, '')
      setAnswerGroup([...answerGroup])
    }
  }
  const onItalic = () => {
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    const range = selection.getRangeAt(0)
    const parent = range.commonAncestorContainer
    let el: HTMLElement = null
    let parentEl =
        parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
    while (parentEl.parentElement) {
      if (parentEl.classList.contains('div-input')) {
        el = parentEl
        break
      }
      parentEl = parentEl.parentElement
    }
    document.execCommand('Italic', false, null)
    if (el) {
      const index = parseInt(el.id.replace(`${groupKey}_`, ''))
      let text = ''
      for (let i = 0, len = el.childNodes.length; i < len; i++) {
        const childNode = el.childNodes[i]
        text += convertHtmlToStr([childNode])
      }
      answerGroup[index].text = text.replace(/(\*|\#)/g, '')
      setAnswerGroup([...answerGroup])
    }
  }
  const onUnderline = () => {
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    const range = selection.getRangeAt(0)
    const parent = range.commonAncestorContainer
    let el: HTMLElement = null
    let parentEl =
        parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
    while (parentEl.parentElement) {
      if (parentEl.classList.contains('div-input')) {
        el = parentEl
        break
      }
      parentEl = parentEl.parentElement
    }
    document.execCommand('Underline', false, null)
    if (el) {
      const index = parseInt(el.id.replace(`${groupKey}_`, ''))
      let text = ''
      for (let i = 0, len = el.childNodes.length; i < len; i++) {
        const childNode = el.childNodes[i]
        text += convertHtmlToStr([childNode])
      }
      answerGroup[index].text = text.replace(/(\*|\#)/g, '')
      setAnswerGroup([...answerGroup])
    }
  }
  return (
    <div className={className} style={{ position: 'relative' }}>
      <table className={`question-table`}>
        <thead>
          <tr>
            <th
                dangerouslySetInnerHTML={{
                    __html: t('create-question')['select-question-example']
                }}
            ></th>
            <th align="left">{t('create-question')['list-questions-examples']}</th>
            <th align="center">
              <div style={{ display: 'flex' }}>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={onBold}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                ></div>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={onItalic}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-italic.png)',
                      } as MyCustomCSS
                    }
                ></div>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={onUnderline}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-underline.png)',
                      } as MyCustomCSS
                    }
                ></div>
              </div>
            </th>
            <th align="center">True</th>
            <th align="center">False</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {answerGroup?.map((answer: AnswerGroup, answerIndex: number) => {
            if (answer.isExam === true) {
              indexA += 1
            } else {
              indexE += 1
            }
            console.log({answer});
            return (
              <tr key={answer.key} id={answer.key}>
                <td
                  align="center"
                  className={`${
                    listExamError.length > 3 || listAnsError.length < 1
                      ? 'error'
                      : ''
                  }`}
                >
                  <div
                    className="__toggle"
                    style={{
                      pointerEvents: 'all',
                    }}
                  >
                    <Toggle
                      disabled={viewMode}
                      size="md"
                      className="__switcher"
                      // defaultChecked={answer.isExam}
                      checked={answer.isExam}
                      checkedChildren={t('common')['question']}
                      unCheckedChildren={t('common')['example']}
                      onChange={() => handleToggle(answerIndex, answer.isExam)}
                    />
                  </div>
                </td>
                <td
                  className={`input-placeholder 
                ${listError.includes(answerIndex.toString()) ? 'error' : ''}`}
                >
                  <ContentEditable
                    id={`${groupKey}_${answerIndex}`}
                    spellCheck={false}
                    className={`div-input ${groupKey}`}
                    html={convertStrToHtml(answer.text)}
                    contentEditable="true"
                    onChange={onChangeText.bind(null, answerIndex)}
                    onBlur={onBlurText.bind(null, answerIndex)}
                    onPaste={onPaste}
                    style={{
                      width: 'unset'
                    }}
                    onKeyDown={(event)=>{
                      if (event.key === 'Enter') {
                        event.preventDefault()
                      }
                    }}
                  />
                  <span className="placeholder">
                    {!answer.isExam
                      ? `${t('common')['example']} ${indexE + 1}`
                      : `${t('common')['question']} ${indexA + 1}`}
                  </span>
                </td>
                <td></td>
                <td
                  align="center"
                  className={
                    listCError.includes(answerIndex.toString()) ? 'error' : ''
                  }
                >
                  <input
                    type="radio"
                    data-value="T"
                    name={`${groupKey}_${answerIndex}`}
                    checked={answer.correct === 'T'}
                    onChange={onChangeIsCorrect.bind(null, answerIndex)}
                  />
                </td>
                <td
                  align="center"
                  className={
                    listCError.includes(answerIndex.toString()) ? 'error' : ''
                  }
                >
                  <input
                    type="radio"
                    data-value="F"
                    name={`${groupKey}_${answerIndex}`}
                    checked={answer.correct === 'F'}
                    onChange={onChangeIsCorrect.bind(null, answerIndex)}
                  />
                </td>
                <td align="right">
                  <img
                    onClick={onDeleteAnswer.bind(null, answerIndex)}
                    className="ic-action"
                    src="/images/icons/ic-trash.png"
                    alt="delete-answer"
                  />
                </td>
              </tr>
            )
          })}
          {!viewMode &&(<tr className="action">
            <td></td>
            <td colSpan={1} align="left">
              <label className={`add-new-answer ${answerGroup.length === 0 ? 'error' : ''}`} onClick={onAddAnswer}>
                <img
                  src="/images/icons/ic-plus-sub.png"
                  width={17}
                  height={17}
                  alt="add-new-answer"
                />{' '}
                {t('create-question')['add-question']}
              </label>
            </td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>)}
        </tbody>
      </table>
      {err && (
        <div className="render_error">
          <span>{error}</span>
        </div>
      )}
      <div className="border-error"></div>
    </div>
  )
}

type AnswerGroup = {
  key:string
  text: string
  correct: string
  isExam: boolean
}
