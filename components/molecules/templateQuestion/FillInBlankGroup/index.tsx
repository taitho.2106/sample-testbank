import React, {
  useEffect,
  useState,
  useRef,
  useCallback,
  KeyboardEvent,
} from 'react'

import { Button, Toggle, Tooltip, Whisper } from 'rsuite'

import { ContentField } from 'components/atoms/contentField'
import { ContentFieldFB } from 'components/atoms/contentFieldFB'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionPropsType } from 'interfaces/types'
import browserFile from 'lib/browserFile'

import AudioInput from '../AudioInput'
import ImageInput from '../ImageInput'
import useTranslation from "@/hooks/useTranslation";

const ID = 'fb-group'
export default function FillInBlankGroup({
  question,
  register,
  setValue,
  isAudio = false,
  isImage = false,
  isReading = false,
  viewMode = false,
  errors = {},
  questionType,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const [displayPopUp, setDisplayPopUp] = useState(false)
  const [selectedValue, setSelectedValue] = useState(null)
  const [errExample, setErrExample] = useState(false)
  const exampleTexts =
    question?.correct_answers
      ?.split('#')
      .map((m) => (m.startsWith('*') ? m.replace('*', '') : '')) || []
  const inputRef = useRef(null)
  const tableRef = useRef<HTMLDivElement>(null)
  const inputIndexRef = useRef(-1)
  const exampleModesRef = useRef(
    question?.correct_answers?.split('#').map((m) => m.startsWith('*')) || [],
  )

  const convertListInput = useCallback((strV: string) => {
    const listV: Answer[][] =
      strV?.split('#').map((m) =>
        m.split('%/%').map((g) => ({
          text: g,
        })),
      ) ?? []
    return listV
  }, [])

  const listInput = useRef(convertListInput(question?.correct_answers))
  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  useEffect(() => {
    if (isImage) {
      register('image_file', {
        validate: {
          acceptedFormats: (file) => {
            // check format if has file
            if (!file) {
              return true
            }
            return (
              ['image/jpeg', 'image/png'].includes(file?.type) ||
              t('question-validation-msg')['image-file']
            )
          },
        },
      })
      register('image', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['image-max-length'], ['2000']),
        },
        required: t('question-validation-msg')['image-required'],
        value: question?.image,
      })
    }
    if (isAudio) {
      register('audio_file', {
        validate: {
          acceptedFormats: async (file) => {
            if (!file) {
              return true
            }
            if (file.size == 0) {
              return t('question-validation-msg')['audio-link-error']
            }
            if (!['audio/mp3', 'audio/mpeg'].includes(file?.type)) {
              return t('question-validation-msg')['audio-type']
            }
            if (!(await browserFile.checkFileExist(file))) {
              return t('question-validation-msg')['audio-link-error']
            }
            return true
          },
        },
      })
      register('audio', {
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['audio-max-length'],['10000']),
        },
        required: t('question-validation-msg')['audio-required'],
        value: question?.audio,
      })
      register('audio_script', {
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
        },
        value: question?.audio_script,
      })
    }
    if (isReading) {
      register('parent_question_text', {
        required: t('question-validation-msg')['reading-script-required'],
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['reading-script-max-length'],['10000']),
        },
        value: question?.parent_question_text,
      })
    }
    register('question_text', {
      required:t('question-validation-msg')['question-text-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'],['10000']),
      },
      value: question?.question_text,
    })
    register('example_mode_fb', {
      validate: (value: string) => {
        const arrExample = (value ?? '').split('#').filter((m) => m == 'true')
        return arrExample.length > 1 ? t(t('question-validation-msg')['example-allow-total'], ['1']) : true
      },
      value:
        question?.correct_answers
          ?.split('#')
          .map((m) => m?.startsWith('*'))
          .join('#') || '',
    })
    register('question_mode_fb', {
      validate: (value: string) => {
        const arrExample = (value ?? '').split('#').filter((m) => m !== 'true')
        return arrExample.length < 1 ? t(t('question-validation-msg')['question-fb-required'], ['1']) : true
      },
      value:
        question?.correct_answers
          ?.split('#')
          .map((m) => m?.startsWith('*'))
          .join('#') || '',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errArr = (value ?? '').split('#').map((m, mIndex) =>
          m
            .replaceAll('*', '')
            .split('%/%')
            .map((i, index) => {
              if (i === '') return index
              return null
            })
            .filter((m) => m !== null)
            .join('*'),
        )
        const errStr = errArr.join('#')
        const check = errArr.filter((item) => item.length > 0)
        if (errStr === '') return true
        return check.length > 0 ? errStr : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [])

  useEffect(() => {
    if (selectedValue === null) return
    if (isInit.current) {
      isInit.current = false
    } else {
      onUpdateCorrectAnswer()
    }
  }, [selectedValue])

  useEffect(() => {
    const inputErrorMsg = document.getElementById('input-error-msg')
    if (exampleModesRef.current.filter((m: any) => m == false).length == 0) {
      inputErrorMsg.innerText = `${t(t('question-validation-msg')['input-required'],['1'])}`
      inputErrorMsg.style.display = 'block'
    }
  }, [])

  useEffect(() => {
    const parentDiv = document.getElementById(ID)
    parentDiv.addEventListener('scroll', () => {
      setDisplayPopUp(!displayPopUp)
    })
  }, [])
  
  useEffect(() => {
    if (inputIndexRef.current < 0) return
    let offsetTop = 0
    const parentDiv = document.getElementById(ID)
    const parentRect = inputRef.current.getBoundingClientRect()
    const inputNodeIndex = inputRef.current.listInput[inputIndexRef.current].id
    const inputElement = document.getElementById(inputNodeIndex)
    const inputRect = inputElement.getBoundingClientRect()
    if (parentDiv.scrollTop !== 0) {
      offsetTop = inputElement.offsetTop - (parentDiv.scrollTop - 27)
    } else {
      offsetTop = inputElement.offsetTop + 27
    }
    // let offsetTop = inputRect.top - parentRect.top + inputRect.height + 10
    let offsetLeft = inputRect.left - parentRect.left + inputRect.width / 2 - 5
    let tableLeft = 5
    if (offsetLeft > 315) {
      tableLeft = offsetLeft - 310
      offsetLeft = 310
    } else {
      offsetLeft -= 5
    }
    tableRef.current.style.top = `${offsetTop}px`
    tableRef.current.style.left = `${tableLeft}px`
    tableRef.current.style.display = 'unset'
    tableRef.current.style.setProperty('--offset-left', `${offsetLeft}px`)
  }, [displayPopUp])

  const updateErrorMsg = (data: any[]) => {
    const exampleErrorMsg = document.getElementById('example-error-msg')
    const inputErrorMsg = document.getElementById('input-error-msg')
    if (data.filter((m: any) => m == 'true').length > 1) {
      exampleErrorMsg.innerText = `* ${t(t('question-validation-msg')['example-allow-total'],['1'])}`
      exampleErrorMsg.style.display = 'block'
    } else {
      exampleErrorMsg.style.display = 'none'
    }
    if (data.filter((m: any) => m == 'false').length == 0) {
      inputErrorMsg.innerText = `${t(t('question-validation-msg')['input-required'],['1'])}`
      inputErrorMsg.style.display = 'block'
    } else {
      inputErrorMsg.style.display = 'none'
    }
  }

  const onUpdateModeFB = () => {
    const data: any[] = []
    listInput.current.map((m: any, i: number) => {
      if (exampleModesRef.current[i]) {
        data.push('true')
      } else {
        data.push('false')
      }
    })
    setValue('example_mode_fb', data.join('#'))
    setValue('question_mode_fb', data.join('#'))
    setTimeout(() => {
      if (!selectedValue || exampleModesRef.current[selectedValue.index]) {
        const inputId = `${groupKey}_0`
        document.getElementById(inputId)?.focus()
      } else {
        document
          .getElementById(`${groupKey}_${selectedValue.value?.length - 1 ?? 0}`)
          ?.focus()
      }
    }, 0)
    updateErrorMsg(data)
  }

  const onUpdateCorrectAnswer = () => {
    setValue(
      'correct_answers',
      listInput.current
        .map((m: any, i: number) => {
          const text = m
            ?.map((g: any) => g.text.trim())
            ?.join('%/%')
            .replaceAll('*', '')
          return exampleModesRef.current[i] ? `*${text?.split('%/%')[0]}` : text
        })
        .join('#'),
    )
  }

  const onChange = (data: any) => {
    setValue('question_text', data.replaceAll('##', '%s%'))
    tableRef.current.style.display = 'none'
  }

  const onBlur = (data: any) => {
    setValue('question_text', data.replaceAll('##', '%s%'))
  }

  const onChangeAudioScript = (data: string) => {
    setValue('audio_script', data)
  }

  const onChangeReadingScript = (data: any) => {
    setValue('parent_question_text', data)
  }

  const formatText = useCallback((answerStr: string, corrects: string) => {
    if (!corrects) return answerStr
    let result: any = answerStr
    const correctList = corrects.split('#')
    for (let i = 0; i < correctList.length; i++) {
      result = result?.replace('%s%', `##`)
    }
    return result
  }, [])

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }

  const onTabCreated = (id: string, index: number) => {
    listInput.current.splice(index, 0, [{ text: '' }])
    const data: any[] = exampleModesRef.current
    data.splice(index, 0, false)
    exampleModesRef.current = [...data]
    onUpdateModeFB()
    onUpdateCorrectAnswer()
    onClosePopup()
  }

  const onTabsDeleted = (indexes: number[]) => {
    listInput.current = listInput.current.filter(
      (m, mIndex) => !indexes.includes(mIndex),
    )
    const data = exampleModesRef.current.filter(
      (m, mIndex) => !indexes.includes(mIndex),
    )
    exampleModesRef.current = [...data]
    onUpdateModeFB()
    onUpdateCorrectAnswer()
    onClosePopup()
  }

  const onTabClick = (e: React.MouseEvent<HTMLDivElement>, index: number) => {
    const input = e.target as HTMLInputElement
    setSelectedValue({ index: index, value: listInput.current[index] })
    if (exampleModesRef.current.filter((m) => m).length > 1) {
      setErrExample(true)
    } else {
      setErrExample(false)
    }
    inputIndexRef.current = index
    setDisplayPopUp(!displayPopUp)
    if (!viewMode) {
      setTimeout(() => {
        if (exampleModesRef.current[index]) {
          document.getElementById(`${groupKey}_0`)?.focus()
        } else {
          document
            .getElementById(
              `${groupKey}_${listInput.current[index]?.length - 1 ?? 0}`,
            )
            ?.focus()
        }
      }, 0)
    }
  }

  const onClosePopup = () => {
    // setSelectedValue(null)
    inputIndexRef.current = -1
    tableRef.current.style.display = null
  }

  const onToggleExample = (answerIndex: number) => {
    const listMode = [...exampleModesRef.current]
    listMode[answerIndex] = !listMode[answerIndex]
    exampleModesRef.current = listMode
    if (listMode.filter((m) => m).length > 1) {
      setErrExample(true)
    } else {
      setErrExample(false)
    }
    inputIndexRef.current = answerIndex
    setDisplayPopUp(!displayPopUp)
    onUpdateModeFB()
    onUpdateTextInput(
      listMode[answerIndex],
      answerIndex,
      selectedValue?.value[0]?.text,
    )
    setValue(
      'correct_answers',
      listInput.current
        .map((m: any, i: number) => {
          const text = m
            ?.map((g: any) => g.text)
            ?.join('%/%')
            .replaceAll('*', '')
          return listMode[i] ? `*${text?.split('%/%')[0]}` : text
        })
        .join('#'),
    )
  }

  const onUpdateTextInput = (
    isExample: boolean,
    index: number,
    value: string,
  ) => {
    const inputData = inputRef.current.listInput[index]
    inputData.value = value?.replaceAll('*', '') || ''
    if (isExample) {
      inputData.className = 'input-example'
    } else {
      inputData.className = ''
      inputData.value = ''
    }
  }

  const onChangeAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value
    setSelectedValue({ ...selectedValue })
    setDisplayPopUp(!displayPopUp)
    if (exampleModesRef.current[selectedValue.index]) {
      onUpdateTextInput(true, selectedValue.index, e.target.value)
    } else {
      onUpdateTextInput(false, selectedValue.index, e.target.value)
    }
  }
  const onBlurAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value = e.target.value.trim() 
    setSelectedValue({ ...selectedValue })
    if (exampleModesRef.current[selectedValue.index]) {
      onUpdateTextInput(true, selectedValue.index, e.target.value)
    } else {
      onUpdateTextInput(false, selectedValue.index, e.target.value)
    }
  }

  const onAddAnswer = () => {
    selectedValue.value.push({ text: '' })
    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })
    setTimeout(() => {
      document
        .getElementById(
          `${groupKey}_${
            listInput.current[selectedValue.index]?.length - 1 ?? 0
          }`,
        )
        ?.focus()
    }, 0)
  }

  const onDeleteAnswerText = (answerIndex: number) => {
    if (!exampleModesRef.current[selectedValue.index]) {
      selectedValue.value.splice(answerIndex, 1)
      listInput.current[selectedValue.index] = selectedValue.value
      setSelectedValue({ ...selectedValue })
    } else {
      selectedValue.value[0].text = ''
      listInput.current[selectedValue.index] = selectedValue.value
      setSelectedValue({ ...selectedValue })
    }
    setDisplayPopUp(!displayPopUp)
    onUpdateTextInput(
      exampleModesRef.current[selectedValue.index],
      selectedValue.index,
      selectedValue?.value[0]?.text,
    )
  }

  const onChangeFile = (files: FileList) => {
    setValue('audio_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('audio', urlAudio, {
      shouldValidate: true,
    })
  }

  const onChangeImageFile = (files: FileList) => {
    setValue('image_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('image', urlAudio, {
      shouldValidate: true,
    })
  }

  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)
  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }

  const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
      return
    }
  }, [])

  const listErr = errors['correct_answers']?.message?.split('#') ?? []
  
  return (
    <div className="m-fill-in-blank-group-custom">
      {isAudio && (
        <AudioInput
          url={question?.audio}
          script={question?.audio_script}
          isError={errors['audio'] || errors['audio_file']}
          onChangeFile={onChangeFile}
          onChangeScript={onChangeAudioScript}
          style={isImage ? { height: '460px' } : {}}
          viewMode={viewMode}
        />
      )}
      {isReading && (
        <div
          style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            position: 'relative',
          }}
        >
          <ContentField
            className={`form-input-stretch ${
              errors['parent_question_text'] ? 'error' : ''
            }`}
            label={t('create-question')['reading-text']}
            strValue={question?.parent_question_text}
            disableTab={true}
            onBlur={onChangeReadingScript}
            style={{ flex: 1 }}
            inputStyle={{
              paddingTop: '4rem',
              paddingRight: '3rem',
              height: '100%',
            }}
            specialChar={['#', '*']}
            disableStyleText={questionType !== 'FB6' ? ['b', 'u', 'i'] : []}
            viewMode={viewMode}
          >
            {questionType === 'FB6' && (
              <div className="box-action-info">
                <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onBold}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onItalic}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-italic.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onUnderline}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-underline.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                </div>
                <div className="clear-outline"></div>
              </div>
            )}
          </ContentField>
        </div>
      )}
      <div style={{ flex: 1 }}>
        {
          <>
            <TextAreaField
              setValue={setValue}
              label={t('create-question')['question-description']}
              defaultValue={question?.question_description}
              register={register('question_description', {
                required: t('question-validation-msg')['description-required'],
                maxLength: {
                  value: 10000,
                  message: t(t('question-validation-msg')['description-max-length'], ['10000']),
                },
              })}
              style={{ height: '9rem' }}
              className={`form-input-stretch ${
                errors['question_description'] ? 'error' : ''
              }`}
              viewMode={viewMode}
              specialChar={['#', '*']}
            />
            {isImage && (
              <ImageInput
                url={question?.image}
                onChangeFile={onChangeImageFile}
                isError={errors['image'] || errors['image_file']}
              />
            )}
          </>
        }
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <ContentFieldFB
            id={ID}
            ref={inputRef}
            label={t('common')['question']}
            strValue={formatText(
              question?.question_text,
              question?.correct_answers,
            )}
            onChange={onChange}
            onBlur={onBlur}
            isMultiTab={true}
            disableTabInput={true}
            onTabClick={onTabClick}
            onTabCreated={onTabCreated}
            onTabsDeleted={onTabsDeleted}
            inputStyle={{ padding: '4.5rem 1.2rem 1rem 1.2rem' }}
            className={
              errors['question_text'] ||
              errors['correct_answers'] ||
              errors['example_mode_fb'] ||
              errors['question_mode_fb']
                ? 'error'
                : ''
            }
            exampleTexts={exampleTexts}
            viewMode={viewMode}
            specialChar={['#', '*']}
          >
            <div className="box-action-info">
              <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                <Whisper
                  placement="top"
                  trigger={'hover'}
                  speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
                >
                  <Button className="add-new-answer" onMouseDown={onAddSpace}>
                    <img src="/images/icons/ic-underscore.png" alt="" />
                  </Button>
                </Whisper>
                <>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onBold}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onItalic}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-italic.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                  <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={onUnderline}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-underline.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                </>
              </div>
              <div className="clear-outline"></div>
            </div>
          </ContentFieldFB>
          {/* <div className="box-action-info">
            <span className="add-new-answer" onMouseDown={onAddSpace}>
              <img src="/images/icons/ic-plus-sub.png" width={17} height={17} />{' '}
              Thêm khoảng trống
            </span>
          </div> */}

          <div ref={tableRef} className="popup-table">
            {selectedValue && (
              <div>
                <table className={`question-table`}>
                  <thead>
                    <tr>
                      <th align="left"
                          dangerouslySetInnerHTML={{
                            __html: t('create-question')['select-question-example']}}
                      >
                      </th>
                      <th align="left">{t('create-question')['correct-answer']}</th>
                      <th className="action" style={{ width: '60px' }}>
                        <img
                          alt=""
                          src="/images/icons/ic-close-dark.png"
                          width={20}
                          height={20}
                          onClick={onClosePopup}
                          style={{ pointerEvents: 'all', cursor: 'pointer' }}
                        />
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {selectedValue?.value &&
                      !exampleModesRef.current[selectedValue.index] &&
                      selectedValue?.value?.map(
                        (answer: Answer, answerIndex: number) => {
                          const listItemErr = listErr[selectedValue.index]
                          const answerText = answer.text.replace('*', '')
                          return (
                            <tr key={`${groupKey}_${answerIndex}`}>
                              {answerIndex == 0 && (
                                <td
                                  rowSpan={selectedValue?.value.length}
                                  width="110px"
                                >
                                  <div className={`__content-toggle`}>
                                    <Toggle
                                      size="md"
                                      className="__switcher"
                                      disabled={viewMode}
                                      checked={
                                        !exampleModesRef.current[
                                          selectedValue.index
                                        ]
                                      }
                                      checkedChildren={t("common")["question"]}
                                      unCheckedChildren={t('common')['example']}
                                      onChange={() =>
                                        onToggleExample(selectedValue.index)
                                      }
                                    />
                                  </div>
                                </td>
                              )}
                              <td
                                className={`td-input ${
                                  listItemErr?.includes(answerIndex.toString())
                                    ? 'error'
                                    : ''
                                }`}
                              >
                                <input
                                  id={`${groupKey}_${answerIndex}`}
                                  onKeyDown={onKeyDown}
                                  type="text"
                                  value={answerText}
                                  onChange={onChangeAnswerText.bind(
                                    null,
                                    answerIndex,
                                  )}
                                  onBlur={onBlurAnswerText.bind(
                                    null,
                                    answerIndex,
                                  )}
                                />
                              </td>
                              <td align="center">
                                <img
                                  alt=""
                                  onClick={onDeleteAnswerText.bind(
                                    null,
                                    answerIndex,
                                  )}
                                  className="ic-action"
                                  src="/images/icons/ic-trash.png"
                                />
                              </td>
                            </tr>
                          )
                        },
                      )}
                    {selectedValue?.value &&
                      exampleModesRef.current[selectedValue.index] && (
                        <tr
                          key={`${groupKey}_${0}`}
                          style={{ backgroundColor: 'rgba(61,196,125,0.15)' }}
                        >
                          <td>
                            <div
                              className={`__content-toggle ${
                                errExample ? 'toggle-error' : ''
                              }`}
                            >
                              <Toggle
                                size="md"
                                className="__switcher"
                                disabled={viewMode}
                                checked={
                                  !exampleModesRef.current[selectedValue.index]
                                }
                                checkedChildren={t("common")["question"]}
                                unCheckedChildren={t('common')['example']}
                                onChange={() =>
                                  onToggleExample(selectedValue.index)
                                }
                              />
                            </div>
                          </td>
                          <td
                            className={`td-input ${
                              listErr[selectedValue.index]?.includes('0')
                                ? 'error'
                                : ''
                            }`}
                          >
                            <input
                              className={`${
                                listErr[selectedValue.index]?.includes('0')
                                  ? 'error'
                                  : ''
                              }`}
                              id={`${groupKey}_0`}
                              onKeyDown={onKeyDown}
                              type="text"
                              value={selectedValue.value[0].text.replace(
                                '*',
                                '',
                              )}
                              onChange={onChangeAnswerText.bind(null, 0)}
                              onBlur={onBlurAnswerText.bind(null, 0)}
                            />
                          </td>
                          <td align="center">
                            <img
                              alt=""
                              onClick={onDeleteAnswerText.bind(
                                null,
                                selectedValue.value[0],
                              )}
                              className="ic-action"
                              src="/images/icons/ic-trash.png"
                            />
                          </td>
                        </tr>
                      )}
                    {!viewMode &&
                      !exampleModesRef.current[selectedValue.index] &&
                      (!selectedValue?.value ||
                        selectedValue?.value?.length < 26) && (
                        <tr className="action">
                          <td></td>
                          <td align="left">
                            <span
                              style={
                                selectedValue?.value?.length == 0
                                  ? {
                                      padding: '1rem 1.5rem',
                                      borderRadius: '0.8rem',
                                      border: '1px solid #ff0018',
                                    }
                                  : {}
                              }
                              className="add-new-answer"
                              onClick={onAddAnswer}
                            >
                              <img
                                alt=""
                                src="/images/icons/ic-plus-sub.png"
                                width={17}
                                height={17}
                              />{' '}
                              {t('create-question')['add-answer']}
                            </span>
                          </td>
                          <td></td>
                        </tr>
                      )}
                  </tbody>
                </table>
              </div>
            )}
          </div>
        </div>
        <div className="form-error-message">
          <span id="example-error-msg" style={{ display: 'none' }}></span>
          <span id="input-error-msg" style={{ display: 'none' }}></span>
        </div>
      </div>
    </div>
  )
}

type Answer = {
  text: string
}
