import useTranslation from '@/hooks/useTranslation'
import { useCallback, useEffect, useRef, useState } from 'react'

import { TagPicker } from 'rsuite'

type PropType = {
  answerStr: string
  correctStr: string
  styleBold?: boolean
  className?: string
  errorStrCA?: string
  errorStrA?:string
  errorStrCAI?:number[]
  onChange: (data: any) => void
  viewMode?: boolean
  labelDefault: string
}

export default function DragDropBoxWord({
  answerStr,
  correctStr,
  className = '',
  errorStrCA,
  errorStrA,
  errorStrCAI,
  onChange,
  viewMode,
  labelDefault,
}: PropType) {
  const {t} = useTranslation()
  const [words, setWords] = useState<Word[]>(
    (answerStr?.split('*') ?? []).map((m) => ({ id: Math.random(), text: m })),
  )
console.log("answerStr----",answerStr)
  const convertCorrect = useCallback((listWord, correctW) => {
    return correctW?.split('%/%')?.map((m: any) => {
      const parts = m.split('*') ?? []
      const wordsTemp = [...listWord]
      const result = []
      for (const part of parts) {
        const index = wordsTemp.findIndex((m) => m.text === part)
        result.push(wordsTemp.splice(index, 1)[0])
      }
      return result
    })
  }, [])

  const [correctWords, setCorrectWords] = useState<Word[][]>(
    convertCorrect(words, correctStr),
  )

  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  useEffect(() => {
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        onChange({
          answers: words?.map((m) => m.text).join('*'),
          correct_answers:
            correctWords
              ?.map((m) => m?.map((g) => g.text).join('*'))
              .join('%/%') ?? '',
        })
      }
    }
  }, [words, correctWords])

  const onKeyDown =(event: any) => {
    if (event.code === 'Enter') {
      event.preventDefault()
      if (event.target.value.trim() === '') return
      setWords([...words, { id: Math.random(), text: event.target.value.trim() }])
      event.target.value = ''
    }
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
      return
    }
  }

  const onClickRemove = (word: Word, e: any) => {
    setWords(words.filter((w) => w.id != word.id))
    setCorrectWords(correctWords?.map((m) => m.filter((g) => g.id != word.id)))
  }

  const onChangeCorrectAnswer = (index: number, value: any) => {
    correctWords[index] = value.map((m: any) => words.find((g) => g.id === m))
    // console.log(correctWords)
    setCorrectWords([...correctWords])
  }

  const onAddNewAnswer = () => {
    if (correctWords) {
      setCorrectWords([...correctWords, []])
    } else {
      setCorrectWords([[]])
    }
  }

  const onDeleted = (index: number) => {
    setCorrectWords(correctWords.filter((m, mIndex) => mIndex != index))
  }

  
  return (
    <div className={`${className}`}>
      <div style={{ position: 'relative' }}>
        <div className={`box-words ${errorStrA}`} data-exist="true">
          {words?.map((word: Word, wordIndex: number) => (
            <div key={`word_${wordIndex}`}>
              <span>{word.text}</span>
              <img
                src="/images/icons/ic-close-circle.png"
                onClick={onClickRemove.bind(null, word)}
                alt=""
              />
            </div>
          ))}
          <input onKeyDown={onKeyDown} />
          <label>
            <span>{t('create-question')['keyword']} {labelDefault}</span>
          </label>
        </div>
        <div className="box-action-info" style={{top:'unset',borderRadius: '0 0 8px 8px', borderBottom: 'none', borderTop: '1px solid #d5dee8'}}>
          <span className="text-guide">
            <img src="/images/icons/ic-info-blue.png" alt=""/>
            {t('create-question')['enter-create-keyword']}
          </span>
        </div>
      </div>
      <div className={errorStrCA} style={{ position: 'relative' }}>
        <table className={`question-table`}>
          <thead>
            <tr>
              <th align="left">{t('create-question')['list-correct-answers']}</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {correctWords?.map((wordList: Word[], wordsIndex: number) => {
              let hasErr = false;
              if(!wordList || wordList.length == 0 || words.length != wordList.length ){
                hasErr = true;
              }
              return(
              <tr
                key={`${groupKey}_${wordsIndex}`}
              >
                <td className={`${hasErr ? 'error-item' : ''}`}>
                  <TagPicker
                    data={words.map((m) => ({ label: m.text, value: m.id }))}
                    value={wordList.map((m) => m.id)}
                    onChange={onChangeCorrectAnswer.bind(null, wordsIndex)}
                    placeholder={' '}
                    menuStyle={{
                      maxWidth:'550px'
                    }}
                  />
                </td>
                <td align="right">
                  <img
                    className="ic-action"
                    src="/images/icons/ic-trash.png"
                    onClick={onDeleted.bind(null, wordsIndex)}
                    alt=""
                  />
                </td>
              </tr>
            )})}
            {!viewMode && (
            <tr className="action">
              <td align="left">
                <label className="add-new-answer" onClick={onAddNewAnswer}>
                  <img
                    src="/images/icons/ic-plus-sub.png"
                    width={17}
                    height={17}
                    alt=""
                  />{' '}
                  {t('create-question')['add-answer']}
                </label>
              </td>
              <td></td>
            </tr>
            )}
          </tbody>
        </table>
        <div className="border-error"></div>
      </div>
    </div>
  )
}

type Word = {
  id: number
  text: string
}
