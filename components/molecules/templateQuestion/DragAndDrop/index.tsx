import { useEffect, useState, useRef } from 'react'

import { Button, Toggle } from 'rsuite'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import DragDropBoxWord from './boxWord'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation'

export default function DragAndDrop({
  question,
  register,
  setValue,
  errors = {},
  viewMode,
  isImage,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const listImage = question?.image?.split('#') ?? []
  const listAnswer = question?.answers?.split('#') ?? []
  const listCorrect = question?.correct_answers?.split('#') ?? []
  const questionList = (question?.question_text || '')?.split('#') ?? []
  const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
    questionList.map((isExam: string, mIndex: number) => {
      return {
        key: new Date().getTime().toString() + mIndex,
        imageStr: listImage[mIndex] || '',
        answerStr: listAnswer[mIndex]  || null,
        correctStr: listCorrect[mIndex] || null,
        isExam: isExam,
      }
    }),
  )

  const answerGroupRef = useRef(answerGroup)
  const imageFiles = useRef(answerGroup.map((m) => null))
  const [arrNewIndex, setArrNewIndex] = useState([] as number[])
  
  useEffect(() => {
    register('passData', {
      validate: (value: string) => {
        return value
      },
    })
    register('answers', {
      validate: (value: string) => {
        const errs = value.split('#')
          .map((m:string, mIndex:number) => {
            if (m === '') {
              return mIndex
            }
            return null
          })
          .filter((m:number) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['keyword-max-length'], ['10000']),
      },
      value: question?.answers || '#',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const eItem:number[] = []
        const errs = value
          .split('#')
          .map((m, mIndex) => {
            if (m === '') {
              return mIndex
            }
            return null
          })
          .filter((m:number) => m !== null)
          value?.split('#')?.map((item:string)=>{
            if(item === ''){

            }else{
              item?.split('%/%')?.map((m:string,mIndex:number)=>{
                if(m === ''){
                  eItem.push(mIndex)
                }
              })
            }
          })
          setArrNewIndex(eItem)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers || '#',
    })

    register('question_text', {
      validate: (value: string) => {
        const errs = (value ?? '').split('#')
        const errE = errs.filter((item: string) => item === '*')
        const errA = errs.filter((item: string) => item === '')
        if (errE.length > 3) return t(t('question-validation-msg')['example-allow-total'], ['3'])
        if (errA.length < 1) return t(t('question-validation-msg')['question-fb-required'], ['1'])
        return true
      },
    })

    register('image_files', {
      validate: async(files: any) => {
            if (!files) {
              return true
            }
            let fileErr: number[] = []
                let mIndex = -1
                for await (const file of files) {
                  mIndex += 1
                  if (file) {
                    if (!['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)) {
                      fileErr.push(mIndex)
                      continue
                    } else if (!(await browserFile.checkFileExist(file))) {
                      fileErr.push(mIndex)
                      continue
                    } else {
                      fileErr.push(null)
                      continue
                    }
                  }
                  fileErr.push(null)
                }
                fileErr= fileErr.filter(m=>m!=null)
            return fileErr.length > 0 ? fileErr.join() : true
        },
    })
    register('image', {
      value: question?.image,
    })
  }, [])
  const onAddQuestion = () => {
    const newQuestion:AnswerGroup = {
      key: new Date().getTime().toString() + (answerGroup?.length ?? 0),
      imageStr: '',
      answerStr: null,
      correctStr: null,
      isExam: ''
    }
    if (answerGroup) {
      setAnswerGroup([...answerGroup, newQuestion])
    } else {
      setAnswerGroup([newQuestion])
    }
    imageFiles.current.push(null)
  }


  useEffect(() => {
    answerGroupRef.current = answerGroup
    const example = []
    const ans = []
    const arrIndexEx = []
    const arrIndexAns = []
    const imageFilesClone = [...imageFiles?.current]
    for (let i = 0; i < answerGroup.length; i++) {
      if (answerGroup[i].isExam === '') {
        ans.push(answerGroup[i])
        arrIndexAns.push(i)
      } else {
        example.push(answerGroup[i])
        arrIndexEx.push(i)
      }
    }
    const arrNew = [...arrIndexEx, ...arrIndexAns]
    // setArrNewIndex([...arrIndexEx, ...arrIndexAns])
    
    if (imageFiles?.current) {
      for (let i = 0; i < arrNew.length; i++) {
        imageFilesClone[i] = imageFiles?.current[arrNew[i]]
      }
      setValue('image_files', imageFilesClone)
    }

    if (answerGroup) {
      setValue('answers', answerGroup.map((m) => m.answerStr).join('#'))
      setValue(
        'correct_answers',
        answerGroup.map((m) => m.correctStr).join('#'),
      )
      setValue('image', answerGroup.map((m) => m.imageStr).join('#'))
      setValue('question_text', answerGroup.map((m) => m.isExam).join('#'))

      let checkErr = false;
      for(let i=0; i< answerGroup.length;i++ ){
        const itemAnswerTempArr = answerGroup[i].answerStr?.split("*")||[]
        if(itemAnswerTempArr.length == 0){
          checkErr = true;
          break;
        }
        const itemCorrectList = answerGroup[i].correctStr?.split("%/%")||[]
        for(let j=0; j< itemCorrectList.length; j++){
            const subItemCorrectList = itemCorrectList[j]?.split("*")||[]
            if(!itemCorrectList ||  subItemCorrectList.length !== itemAnswerTempArr.length){
              checkErr = true;
              break;
            }
            for(let iAns=0; iAns <itemAnswerTempArr.length; iAns ++ ){
              if(!subItemCorrectList.find(m=> m == itemAnswerTempArr[iAns])){
                checkErr = true;
                break;
              }
            }
        }
      }
      setValue("passData",!checkErr)
    }
  }, [answerGroup])

  const onChange = (index: number, data: any) => {
    answerGroupRef.current[index].answerStr = data.answers
    answerGroupRef.current[index].correctStr = data.correct_answers
    setAnswerGroup([...answerGroupRef.current])
  }

  const onChangeImage = (index: number, files: FileList) => {
    if (files && files[0]) {
      const urlImage = URL.createObjectURL(files[0])
      answerGroup[index].imageStr = urlImage
      imageFiles.current[index] = files[0]
      setAnswerGroup([...answerGroup])
    } else {
      answerGroup[index].imageStr = ''
      imageFiles.current[index] = null
      setAnswerGroup([...answerGroup])
    }
  }

  const onDeleteAnswer = (index: number) => {
    const ans: any[] = answerGroup.filter(
      (m: any, mIndex: number) => mIndex !== index,
    )
    setAnswerGroup([...ans])
  }

  const handleToggle = (id: number, value: string) => {
    if (value == '*') answerGroup[id].isExam = ''
    else answerGroup[id].isExam = '*'
    setAnswerGroup([...answerGroup])
  }

  const listExamError = answerGroup.filter((item: any) => item.isExam === '*')
  const listAnsError = answerGroup.filter((item: any) => item.isExam === '')
  const [err, setErr] = useState(false)
  const [error, setError] = useState('')
  const arrErrorCA = errors['correct_answers']?.message?.split(',') || []
  const arrErrorA = errors['answers']?.message?.split(',') || []
  const arrErrorI = errors['image_files']?.message?.split(',') || []
  const renderError = () => {
    if (listExamError.length > 3) {
      setErr(true)
      return setError(t(t('question-validation-msg')['example-allow-total'], ['3']))
    }
    if (listAnsError.length < 1) {
      setErr(true)
      return setError(t(t('question-validation-msg')['question-fb-required'], ['1']))
    }
    if(arrErrorI?.length > 0){
      setErr(true)
      return setError(t('question-validation-msg')['list-images'])
    }
    return setError('')
  }

  useEffect(() => {
    renderError()
  }, [listExamError,listAnsError,arrErrorI])

  let indexE = -1
  let indexA = -1

  return (
    <div className="m-drag-and-drop">
      <TextAreaField
        label={t('create-question')['question-description']}
        setValue={setValue}
        defaultValue={question?.parent_question_description}
        register={register('parent_question_description', {
          required: t('question-validation-msg')['description-required'],
          maxLength: {
            value: 10000,
            message: t(t('question-validation-msg')['description-max-length'], ['10000']),
          },
        })}
        className={`form-input-stretch ${
          errors['parent_question_description'] ? 'error' : ''
        }`}
        style={{ height: '6rem' }}
        viewMode={viewMode}
        specialChar={['#','*']}
      />

      <div className={`question-table-dd1 `} style={{ position: 'relative' }}>
        <table className="question-table question-table-dd1-table">
          <thead className="table-thead">
            <tr>
              <th
                dangerouslySetInnerHTML={{
                  __html: t('create-question')['select-question-example']
                }}
              ></th>
              <th align="left">{t('create-question')['list-questions-examples']}</th>
              <th></th>
            </tr>
          </thead>
          <tbody className="table-tbody">
            {answerGroup?.map((answer: AnswerGroup, answerIndex: number) => {
              if (answer.isExam === '') {
                indexA += 1
              } else {
                indexE += 1
              }
              return (
                <tr key={answer.key} id={answer.key}>
                  <td
                    align="center"
                    className={`${
                      listExamError.length > 3 || listAnsError.length < 1
                        ? 'error'
                        : ''
                    }`}
                  >
                    <div className='toggle-switch'>
                      <Toggle
                        size="md"
                        checked={answer.isExam == ''}
                        // defaultChecked={answer.isExam}
                        checkedChildren={t("common")["question"]}
                        unCheckedChildren={t('common')['example']}
                        onChange={() =>
                          handleToggle(answerIndex, answer.isExam)
                        }
                      />
                    </div>
                  </td>
                  <td>
                    <div className="image-input-dd1">
                      <ImageInputDD1
                        url={answer.imageStr}
                        onChangeFile={onChangeImage.bind(null, answerIndex)}
                        labelDefault={
                          answer.isExam === ''
                            ? `${t(t("create-question")["question-image"], [`${indexA + 1}`])}`
                            : `${t(t('create-question')['image-example-with-num'], [`${indexE + 1}`])}`
                        }
                        viewMode={viewMode}
                        isError={arrErrorI?.includes(answerIndex.toString())}
                      />
                      <span className="image-dd1-guide">
                        <img src="/images/icons/ic-info-blue.png" alt="" />
                        <span>{t('create-question')['optional-image-question']}</span>
                      </span>
                    </div>  
                  </td>
                  <td>
                    <DragDropBoxWord
                        answerStr={answer.answerStr}
                        correctStr={answer.correctStr}
                        onChange={onChange.bind(null, answerIndex)}
                        className={`form-input-stretch`}
                        errorStrCA={arrErrorCA?.includes(answerIndex.toString()) ? 'error' : ''}
                        errorStrA={arrErrorA?.includes(answerIndex.toString()) ? 'error' : ''}
                        errorStrCAI={arrNewIndex}
                        viewMode={viewMode}
                        labelDefault={answer.isExam === '' ? t("common")["question"] : t('common')['example']}
                      />
                      <img
                        onClick={onDeleteAnswer.bind(null, answerIndex)}
                        className="ic-action"
                        src="/images/icons/ic-trash.png"
                        alt="delete-icon"
                      />
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
        {err && (
          <div className="render_error">
            <span>{error}</span>
          </div>
        )}
      </div>

      <div className="form-input-stretch">
        {!viewMode && (
          <Button
            appearance={'primary'}
            type="button"
            style={{
              backgroundColor: 'white',
              width: '20rem',
              color: '#5CB9D8',
              fontSize: '1.6rem',
              fontWeight: 600,
              border: answerGroup.length == 0 ? '1px solid #dd3a09' : 'none'
            }}
            onClick={onAddQuestion}
          >
            <label className="add-new-answer">
              <img
                src="/images/icons/ic-plus-sub.png"
                width={17}
                height={17}
                alt=""
              />{' '}
              {t('create-question')['add-question']}
            </label>
          </Button>
        )}
      </div>
    </div>
  )
}

type PropsType = {
  labelDefault?: string
  isError?: boolean
  url: string
  onChangeFile?: (files: FileList) => void
  viewMode?: boolean
}

function ImageInputDD1({
  isError,
  url,
  onChangeFile,
  viewMode,
  labelDefault,
}: PropsType) {
  const {t} = useTranslation()
  const [imageUrl, setImageUrl] = useState(url ?? '')
  const fileRef = useRef(null)
  const src = imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`
  return (
    <div
      className={`box-image-input-dd1 ${imageUrl !== '' ? 'has-data' : ''} ${isError ? 'error' : ''}`}
    >
      <div className="image-section">
        <div
          className="image-input-origin"
          onClick={() => fileRef.current.dispatchEvent(new MouseEvent('click'))}
        >
          {src && url ? (
            <>
              <img className="image-upload" src={src} alt="image-upload" />
              {!viewMode && src && url && (
                <img
                  className="image-delete-icon"
                  src="/images/icons/ic-remove-opacity.png"
                  alt=""
                  onClick={(event) => {
                    fileRef.current.value = ''
                    event.preventDefault()
                    event.stopPropagation()
                    onChangeFile(null)
                  }}
                />
              )}
            </>
          ) : (
            <>
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                }}
              >
                <img
                  className="icon"
                  src="/images/icons/ic-image.png"
                  alt="delete-img-icon"
                />
                {labelDefault}
              </div>
            </>
          )}
          <input
            ref={fileRef}
            type="file"
            style={{ display: 'none' }}
            accept=".png,.jpeg,.jpg"
            onChange={(e) => {
              if (fileRef.current.files.length > 0) {
                setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                onChangeFile && onChangeFile(e.target.files)
              }
            }}
          />
        </div>
      </div>
    </div>
  )
}

type AnswerGroup = {
  key: string
  imageStr: string
  answerStr: string
  correctStr: string
  isExam: string
}
