import React, {
  useEffect,
  useState,
  useRef,
  CSSProperties,
} from 'react'

import { Toggle } from 'rsuite'

import { SplitImage } from 'components/atoms/SplitImage/Index'
import { TextAreaField } from 'components/atoms/textAreaField'
import { ContentItemPosType, PosType, QuestionPropsType } from 'interfaces/types'
import coordinates from 'utils/coordinates'
import Guid from 'utils/guid'

import ShapeComponent from 'components/atoms/shape'
import { DropdownColorFC } from 'components/atoms/dropdownColorFC'
import { FaChevronDown } from 'react-icons/fa'
import useTranslation from '@/hooks/useTranslation'

type DataImageType = {
  data: string
  deleted: boolean
  key: string
  name?: string
  isExample?: boolean
  color:string
}
type ImageCurrentInfoType = {
  src: string
  width: number
  height: number
}

const LIST_COLOR=[
  '000000',
  'ffffff',
  '880015',
  'ed1c24',
  'ff7f27',
  'fcfb2d',
  '62aa2d',
  'b5e61d',
  '3d009e',
  '038af9',
  '8400ab',
  'c8bfe7',
  '773829',
  'afafaf',
  'fb2576'
]
const sizeWordExport = 680
export default function FillInColour2({
  question,
  register,
  setValue,
  viewMode = false,
  errors = {},
}: QuestionPropsType) {
  const {t} = useTranslation()
  const getFirstDataImage = (correct_answers: string) => {
    const correct_answersArr = correct_answers?.split('#') || []
    const tempArr: DataImageType[] = correct_answersArr.map(
      (item: any, i: number) => {
        const key = Guid.newGuid()
        const itemCorrectArr = item?.split('*') || []
        return {
          color: itemCorrectArr[2],
          key: key,
          isExample: itemCorrectArr[0].startsWith('ex|'),
          data: '',
          deleted: false,
        }
      },
    )
    return tempArr
  }

  const splitImageRef = useRef(null)
  const imageRef = useRef(null)
  const [jsonData, setJsonData] = useState(null)
  const df = getFirstDataImage(question?.correct_answers)
  const listImageRef = useRef(df)
  const [listImage, setListImage] = useState<DataImageType[]>(
    listImageRef.current,
  )
  const timeOutImageWord = useRef(null)
  const [indexSelected, setIndexSelected] = useState(-1)
  const imgCurrentInfoRef = useRef<ImageCurrentInfoType>({
    src: '',
    width: 0,
    height: 0,
  })

  useEffect(() => {
    const getData = async () => {
      try {
        if (!question?.answers) return
        const tempAnswer = question.answers.split('*')
        const res = await fetch(`/${tempAnswer[0]}`, {
          method: 'GET',
        })
        const json = await res.json()
        if (json && Array.isArray(json)) {
          const imageArr = question?.image.split('*') || []
          if (json[0]?.listPos) {
            //old:
            const positionArr = json.map((item: any, i: number) => {
              const key = listImageRef.current[i]?.key
              return { ...item, key: key }
            })
            setJsonData({
              imageUrl: imageArr.length > 0 ? `/upload/${imageArr[0]}` : '',
              positionArr,
            })
          } else {
            //new
            const positionArr = handleStringPositionToLoad(json)
            setJsonData({
              imageUrl: imageArr.length > 0 ? `/upload/${imageArr[0]}` : '',
              positionArr,
            })
          }
        }
      } catch (ex) {
        console.log('ex======-----------------------', ex)
      }
    }
    getData()
  }, [])

  useEffect(() => {
    if (splitImageRef.current) {
      const listPos = splitImageRef?.current?.getImagePostions()
      const errorImageArr: any[] = []
      let imageData: any[] = []
      let countExample = 0
      
      if (listPos && listPos.length > 0) {
        imageData = listImage.map((m, index) => {
          if (m.isExample) countExample += 1
          const imageErr = !m.data
          const colorErr = !m.color
          const exampleErr = m.isExample && countExample > 1
          const isErr = imageErr || colorErr || exampleErr

          errorImageArr.push({
            imageErr,
            colorErr,
            exampleErr,
            isErr,
            key: m.key,
          })
          let coorMaxMin = {    minX: 0,
            minY: 0,
            maxX: 0,
            maxY: 0}
            if(listPos[index]?.listPos){
              coorMaxMin = coordinates.getMaxMinXY(listPos[index].listPos)
            }
          return { ...m, ...coorMaxMin }
        })
      }
      const listImageDraw:string[] = []
      const indexEx = listImage.findIndex(item => item.isExample)
      listImage?.forEach((item:DataImageType,mIndex:number)=>{
        if(indexEx !== mIndex){
          const imageItem = new Image
          
          imageItem.onload = () => {
            const canvasItem :HTMLCanvasElement = document.createElement('canvas')
            canvasItem.height = imageItem.naturalHeight
            canvasItem.width = imageItem.naturalWidth
            const ctxItem = canvasItem.getContext('2d')
            
            ctxItem.drawImage(imageItem, 0, 0, canvasItem.width, canvasItem.height)
  
            ctxItem.fillStyle = `#00000000`
            ctxItem.fillRect(0, 0, canvasItem.width, canvasItem.height)
            // ctxItem.fill()

            ctxItem.beginPath()
            ctxItem.drawImage(imageItem, 0, 0, canvasItem.width, canvasItem.height)

            ctxItem.beginPath()
            ctxItem.globalCompositeOperation = 'source-atop'
            ctxItem.fillStyle = `#${item.color}80`
            ctxItem.fillRect(0,0,canvasItem.width, canvasItem.height)
            // ctxItem.fill()

            const imgURL = canvasItem.toDataURL('image/png', 1)
            listImageDraw.push(imgURL)
            setValue('list_image_item',JSON.stringify(listImageDraw))
          }
          
          imageItem.src = item.data
        }
      })
      setValue('list_answer_item', JSON.stringify(imageData))
      setValue('list_answer_item_error', errorImageArr)
    }
    let countQuestion = 0
    let countExample = 0
    listImage.forEach((m) => {
      if (m.isExample) {
        countExample += 1
      } else {
        countQuestion += 1
      }
    })
    if (countExample > 1 || countQuestion == 0) {
      setValue('passData', false)
    } else {
      setValue('passData', true)
    }
    if(timeOutImageWord.current){
      clearTimeout(timeOutImageWord.current)
    }
    timeOutImageWord.current = setTimeout(() => {
      drawImageExportQuestion()
    }, 150)

    //check style btn add new answer
    setTimeout(() => {
      const scrollElement = document.getElementById(
        'class-image-list-content-id',
      )
      if (scrollElement) {
        if (scrollElement.scrollWidth > scrollElement.offsetWidth) {
          const addBtnElement = document.getElementById('add-new-answer-id')
          if (addBtnElement) {
            if (!addBtnElement.classList.contains('scroll')) {
              addBtnElement.classList.add('scroll')
            }
          }
        } else {
          const addBtnElement = document.getElementById('add-new-answer-id')
          if (addBtnElement) {
            if (addBtnElement.classList.contains('scroll')) {
              addBtnElement.classList.remove('scroll')
            }
          }
        }
      }
    }, 10)
  }, [listImage])

  useEffect(() => {
    register('passData', {
      validate: (value: string) => {
        return value
      },
    })
    register('image_question_base64', {
      required: t('question-validation-msg')['image_question_base64'],
    })
    register('image_question_export_base64', {
      required: t('question-validation-msg')['image_question_export_base64'],
    })
    register('list_answer_item', {
      validate: (value) => {
        if (!value) return t('question-validation-msg')['canvas-answers']
        try {
          const arr = JSON.parse(value)
          if (Array.isArray(arr) && arr.length == 0)
            return t('question-validation-msg')['canvas-answers']
        } catch {}
        return true
      },
      required: t('question-validation-msg')['canvas-answers'],
    })
    register('list_answer_item_error', {
      validate: (value) => {
        if (!value) return true
        // const value = JSON.parse(value)
        const indexErr = value.findIndex((m: any) => m.isErr)
        if (indexErr == -1) {
          return true
        } else {
          return JSON.stringify(value)
        }
      },
    })
  }, [])
  const onToggleExample = (key: string) => {
    const tempArr: DataImageType[] = listImage.map((m) => {
      if (m.key == key) {
        return { ...m, isExample: !m.isExample }
      } else {
        return m
      }
    })
    // console.log('onToggleExample-----------', key)
    listImageRef.current = tempArr
    setListImage([...tempArr])
  }

  console.log('error----------------', errors)
  const itemImageErrArr: any[] = errors['list_answer_item_error']?.message
    ? JSON.parse(errors['list_answer_item_error']?.message)
    : []
  const exampleErr = listImage.filter((m) => m.isExample).length > 1
  const answerErr = listImage.filter((m) => !m.isExample).length == 0
  const listAnswerErr = errors['list_answer_item']
  const imageErr = errors['image_question_base64']
  const drawImageExportQuestion = () => {

    setValue('image_question_export_base64', '')
    if(!imgCurrentInfoRef.current || !imgCurrentInfoRef.current.src ){
      console.debug("noimage---")
      return
    }
    const container = document.getElementById(
      'content-draw-image-export-id',
    ) as HTMLElement
    if (!container || !splitImageRef?.current) return
    const canvas = document.getElementById(
      'canvas-export-id',
    ) as HTMLCanvasElement
    canvas.setAttribute('height', `${container.offsetHeight}`)
    canvas.setAttribute('width', `${container.offsetWidth}`)
    const imageTarget = document.getElementById(
      'image-export-question-id',
    ) as HTMLCanvasElement

    let ratio = 1
    if (imageTarget.offsetWidth < imgCurrentInfoRef.current.width) {
      ratio = imageTarget.offsetWidth / imgCurrentInfoRef.current.width
    }
    const indexExample = listImage.findIndex((m) => m?.isExample)   
      const ctxCanvasDraw: any = canvas.getContext('2d')
      
      ctxCanvasDraw.drawImage(
        imageTarget,
        canvas.width / 2 - imageTarget.offsetWidth / 2,
        canvas.height / 2 - imageTarget.offsetHeight / 2,
        imageTarget.offsetWidth,
        imageTarget.offsetHeight,
      )
      
      if (indexExample != -1) {
        const itemExample = listImage[indexExample];
        const listPos = splitImageRef?.current?.getImagePostions()
            const posExample = listPos[indexExample]
            const leftImage = imageTarget.offsetLeft
            const topImage = imageTarget.offsetTop
            if (posExample && posExample.listPos) {
              const coorMaxMin = coordinates.getMaxMinXY(posExample.listPos)
              const itemCanvas = document.createElement("canvas")
              itemCanvas.width = coorMaxMin.maxX - coorMaxMin.minX
              itemCanvas.height = coorMaxMin.maxY - coorMaxMin.minY
              const itemCanvasCtx = itemCanvas.getContext("2d")
              for (let i = 0; i < posExample.listPos.length; i++) {
                itemCanvasCtx.beginPath()
                itemCanvasCtx.lineJoin = 'round'
                itemCanvasCtx.globalCompositeOperation = 'source-over'
                itemCanvasCtx.fillStyle = `#${itemExample.color}${80}`
                itemCanvasCtx.fillRect(posExample.listPos[i].x - coorMaxMin.minX,  posExample.listPos[i].y - coorMaxMin.minY, 1, 1)
                // ctxCanvasDraw.fill()
              }
              ctxCanvasDraw.drawImage(itemCanvas,leftImage +  coorMaxMin.minX*ratio,topImage + coorMaxMin.minY*ratio,itemCanvas.width * ratio, itemCanvas.height * ratio);
              itemCanvas.remove();
            }
      }
      const imageData = canvas.toDataURL('image/png', 1)
      setValue('image_question_export_base64', imageData)
  }
  let answerCount = 0;
  const clearCanvas = (canvas:HTMLCanvasElement) => {
    if(!canvas) return;
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
  }
  const onChangeItem = (dataImage:any)=>{
    if (dataImage) {
      // console.log("m?.color----------------------", dataImage.map((m:any)=>m.color))
      const tempArr: DataImageType[] = dataImage.map(
        (m: DataImageType, index: number) => {
          const item = listImageRef.current.find(
            (n) => n.key == m?.key,
          )
          if(m.data.length > 0){
          
            const boxElement = document.getElementById(m.key)
            const contentElement:HTMLDivElement = boxElement.querySelector('.__content-image')
            const imgElement:HTMLImageElement = contentElement.querySelector('img')
            if(imgElement.classList.contains('img-item')){
              const listPos = splitImageRef?.current?.getImagePostions()
              if(listPos && listPos.length > 0){
                const coorMaxMin = coordinates.getMaxMinXY(listPos[index].listPos)
                const ratioX = imgElement.offsetWidth / imgElement.naturalWidth
                const ratioY = imgElement.offsetHeight / imgElement.naturalHeight
                const minRatio = Math.min(ratioX,ratioY)
                const widthItem = (coorMaxMin.maxX - coorMaxMin.minX) * minRatio
                const heightItem = (coorMaxMin.maxY - coorMaxMin.minY) * minRatio
  
                const canvasItem: HTMLCanvasElement = document.querySelector(`#canvas-item-image-${index}`)
                canvasItem.width = widthItem
                canvasItem.height = heightItem
                const ctxItem = canvasItem.getContext('2d')
                clearCanvas(canvasItem)
                canvasItem.width = widthItem
                ctxItem.drawImage(imgElement, 0, 0, canvasItem.width, canvasItem.height)
                if(m.color.length > 0){
                  ctxItem.beginPath()
                  ctxItem.globalCompositeOperation = 'source-in'
                  ctxItem.fillStyle = `#${m.color}80`
                  ctxItem.fillRect(0, 0, canvasItem.width, canvasItem.height)
                }
              }
              
            }
            
          }

          return {
            key: m.key,
            data: m.data,
            name: item?.name || '',
            isExample: item?.isExample,
            color: m?.color||item?.color || ""
          }
        },
      )
      listImageRef.current = tempArr
      setListImage(tempArr)
     //set value position.
     const listPos: ContentItemPosType[] = splitImageRef?.current?.getImagePostions()
     const listDataPos = handleStringPositionToSave(listPos)
     setValue('list_image_item_position', JSON.stringify(listDataPos))
   } else {
     setListImage([])
     setValue('list_image_item_position', JSON.stringify([]))
   }
  }
  const handleStringPositionToSave = (listPos: ContentItemPosType[]) => {
    try {
      const listDataPos: any[] = []
      listPos.forEach((item) => {
        const dataColor = item.colorData
        const dataColorStr: any = {}
        const listTemp: any[] = []
        Object.keys(dataColor).forEach(function (key) {
          dataColorStr[key] = dataColor[key].join('*')
          listTemp.push(`${key}|${dataColor[key].join('*')}`)
        })
        listDataPos.push({ strPos: listTemp.join('#'),key: item.key,color: item.color })
      })
      return listDataPos
    } catch {
      return [] as any[]
    }
  }
  const handleStringPositionToLoad = (json: any[]) => {
    try {
      const positionArr = json.map((item: any, i: number) => {
        const itemsWithColor = item.strPos.split('#') || []
        const key = listImageRef.current[i]?.key
        const listPos: PosType[] = []
        itemsWithColor.forEach((m: string) => {
          const items = m.split('|')
          const color = items[0]
          items[1].split('*').forEach((m) => {
            const xy = m.split(':')
            const poss: PosType = { x: parseInt(xy[0]), y: parseInt(xy[1]), c: `#${color}` }
            listPos.push(poss)
          })
        })
        return { listPos: listPos, key: key, color:item.color }
      })
      return positionArr
    } catch {
      return []
    }
  }
  const onChangeImageData = (data:any)=>{
    const boxImage = document.querySelector(".box-image-handle-answer") as HTMLElement;
    if(data.src){
     if(boxImage.classList.contains("no-image")){
      boxImage.classList.remove("no-image")
     }
    }else{
      if(!boxImage.classList.contains("no-image")){
        boxImage.classList.add("no-image")
       }
       if (imageRef.current) imageRef.current.value = null
    }
    const imgExportElement = document.getElementById(
      'image-export-question-id',
    ) as HTMLImageElement
    if (imgExportElement) {
      imgExportElement.src = data.src
    }
    imgCurrentInfoRef.current = data
    setValue('image_question_base64', data.src)
  }
  const createDataColorDropout = (colorSelected: string) => {
    return LIST_COLOR.map((color) => {
      const strokeWidth = (color == colorSelected || color == 'ffffff')?1:0
      const stroke = color == colorSelected?"#000000":"#d4dde7"
      return {
        value: color,
        label:<ShapeComponent.CircleShape fill={`#${color}`} height={18}  stroke={stroke} strokeWidth={strokeWidth} />
      }
    })
  }
  const onChangeColor = (index:number, value: string) => {
    splitImageRef.current.changeColorItem(index, value)
  }
  // console.log("listImage---",listImage.map(m=> m.color))
  return (
    <div className={`m-fill-color-2`}>
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          flex: '1',
          width: '100%',
        }}
      >
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            label={t('create-question')['question-description']}
            setValue={setValue}
            defaultValue={question?.question_description}
            register={register('question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['description-max-length'], ['10000']),
              },
            })}
            style={{ height: '4rem', marginLeft: '0', width: '100%' }}
            className={`form-input-stretch ${
              errors['question_description'] ? 'error' : ''
            }`}
            viewMode={viewMode}
          />

          <div className={`box-image-handle-answer ${jsonData && jsonData.imageUrl?"":"no-image"}`}>
            {
              <SplitImage
                className={`${imageErr ? 'error' : ''}`}
                ref={splitImageRef}
                jsonData={jsonData}
                viewMode={viewMode}
                onChangeItemIndexSelected={(indexSelected) => {
                  setIndexSelected(indexSelected)
                }}
                onChangeItem={(dataImage) => onChangeItem(dataImage)}
                onChangeImageData={(data) => onChangeImageData(data)}
                onClickChangeImage={() => {
                  imageRef.current.dispatchEvent(new MouseEvent('click'))
                }}
              ></SplitImage>
            }
            <input
              ref={imageRef}
              type="file"
              style={{ display: 'none' }}
              accept=".png,.jpeg,.jpg"
              onChange={(e) => {
                // console.log('e--------------', e)
                if (imageRef.current.files.length > 0) {
                  // console.log(
                  //   'imageRef.current.files[0]',
                  //   imageRef.current.files[0],
                  // )
                  const file = imageRef.current.files[0]
                  if (['image/jpeg', 'image/png'].includes(file?.type)) {
                    const url = URL.createObjectURL(imageRef.current.files[0])
                    splitImageRef.current &&
                      splitImageRef.current.changeImageUrl(url)
                  }
                }
              }}
            />
            <div className="text-user-guide">
              <img alt="" src="/images/icons/ic-info-blue.png"></img>
              <span>{t('create-question')['chosen-image']}</span>
            </div>
            <div
              className="class-image-list"
              style={
                !listImage || listImage.length == 0 ? { height: '13.2rem' } : {}
              }
            >
              {listImage && (
                <div
                  id="class-image-list-content-id"
                  className="class-image-list__content"
                  style={listImage.length==10?{maxWidth:"100%"}:{}}
                >
                  {listImage.map((item: DataImageType, indexItem: number) => {
                    const itemErr = itemImageErrArr.find(
                      (m) => m.key == item.key && m.isErr,
                    )
                    if(!item.isExample) answerCount +=1;
                    // console.log("item--color",item.color)
                    return (
                      item && (
                        <div
                        id={item.key}
                        data-id={`key_${item.key}`}
                          className={`class-image-list__content__card-image ${viewMode?"view-mode":""}`}
                          key={item.key}
                        >
                          <div
                            className={`box-image ${indexItem == indexSelected ? 'selected' : ''} ${item.isExample ? '--example' : ''}`}
                          >
                            <img
                              className="image-delete-icon"
                              src="/images/icons/ic-remove-opacity.png"
                              alt=""
                              onClick={(event) => {
                                event.preventDefault()
                                event.stopPropagation()
                                splitImageRef.current &&
                                  splitImageRef.current.removeItem(indexItem)
                              }}
                            />

                            <div
                              className={`__content-image ${
                                itemErr?.imageErr ? 'error' : ''
                              }`}
                              onClick={() => {
                                splitImageRef.current &&
                                  splitImageRef.current.selectItem(indexItem)
                              }}
                            >
                              <img
                                alt="img"
                                className={`${item.data?"img-item":"img-default"} ${item.isExample?"img-draw__example":"img-draw"}`}
                                src={item.data || '/images/icons/ic-image.png'}
                                ref={(value)=>{
                                  if(!value) return
                                  const img = value
                                  const drawCanvas = () =>{
                                    if(item.data){
                                      const listPos = splitImageRef?.current?.getImagePostions()
                                      if(listPos && listPos.length > 0){
                                        const coorMaxMin = coordinates.getMaxMinXY(listPos[indexItem].listPos)
                                        const ratioX = img.offsetWidth / img.naturalWidth
                                        const ratioY = img.offsetHeight / img.naturalHeight
                                        const minRatio = Math.min(ratioX,ratioY)
                                        const widthItem = (coorMaxMin.maxX - coorMaxMin.minX) * minRatio
                                        const heightItem = (coorMaxMin.maxY - coorMaxMin.minY) * minRatio
                                        const canvasItem: HTMLCanvasElement = document.querySelector(`#canvas-item-image-${indexItem}`)
                                        canvasItem.width = widthItem
                                        canvasItem.height = heightItem
                                        const ctxItem = canvasItem.getContext('2d')
                                        clearCanvas(canvasItem)
                                        canvasItem.width = widthItem
                                        ctxItem.drawImage(img, 0, 0, canvasItem.width, canvasItem.height)
                                        if(item.color){
                                          ctxItem.beginPath()
                                          ctxItem.globalCompositeOperation = 'source-in'
                                          ctxItem.fillStyle = `#${item.color}80`
                                          ctxItem.fillRect(0, 0, canvasItem.width, canvasItem.height)
                                        }
                                      }
                                      
                                    }
                                  }
                                  img.onload = () => {
                                    if(img.complete){
                                      drawCanvas()
                                    }
                                  }
                                  
                                }}
                              />
                              <canvas height={50} width={70} id={`canvas-item-image-${indexItem}`} 
                                style={{
                                  position:'absolute'
                                }}
                            ></canvas>
                            </div>

                            <div
                    style={{position: 'relative' }}>
                    <DropdownColorFC
                      data={createDataColorDropout(item.color)}
                      value={item.color}
                      style={{width:90}}
                      onChangeColor={(color)=> onChangeColor(indexItem, color)}
                    >
                    <div className={`dropdown-color-label ${itemErr?.colorErr ? 'error' : ''}`}>
                      <div className='__icon-fill'>
                      <ShapeComponent.IcFillShape fill={item.color ? `#${item.color}` : ''} height={20} />
                      </div>
                      <FaChevronDown height={12} fill='#d7d7d7'></FaChevronDown>
                    </div>
                    </DropdownColorFC>
                  </div>
                          </div>
                          <div
                            className={`__content-toggle ${
                              exampleErr && item.isExample ? 'error' : ''
                            }`}
                          >
                            <Toggle
                              size="md"
                              className="__switcher"
                              disabled={viewMode}
                              checked={!item.isExample}
                              checkedChildren={t('common')['question']}
                              unCheckedChildren={t('common')['example']}
                              onChange={(e) => onToggleExample(item.key)}
                            />
                          </div>
                        </div>
                      )
                    )
                  })}
                </div>
              )}
              {listImage.length < 10 && (
                <div
                  className={`add-new-answer`}
                  id="add-new-answer-id"
                  style={
                    !listImage || listImage.length == 0
                      ? { borderRadius: '0.8rem', alignItems: 'center' }
                      : {}
                  }
                >
                  <div
                    className={`add-new-answer__btn ${
                      (!listImage || listImage.length == 0) && listAnswerErr
                        ? 'error'
                        : ''
                    }`}
                    onClick={() => {
                      // console.log('splitImageRef----', splitImageRef)
                      imgCurrentInfoRef.current && imgCurrentInfoRef.current.src && splitImageRef.current &&
                        splitImageRef.current.addNewItem("")
                        setTimeout(() => {
                          const scrollElement = document.getElementById(
                            'class-image-list-content-id',
                          )
                          if (scrollElement) {
                            if (scrollElement.scrollWidth > scrollElement.offsetWidth) {
                              scrollElement.scrollTo(scrollElement.scrollWidth, 0)
                            }
                          }
                        }, 50);

                    }}
                  >
                    <img alt="" src="/images/icons/ic-plus-sub.png"></img>
                    <span>{t('create-question')['add-answer']}</span>
                  </div>
                </div>
              )}
            </div>
            <div className="text-user-guide">
              <img alt="" src="/images/icons/ic-info-blue.png"></img>
              <span>
                {' '}{t('create-question')['chosen-answer-des']}
              </span>
            </div>
          </div>
        </div>
        {(answerErr || exampleErr) && (
          <div className="form-error-message">
            {answerErr && <span>{`* ${t(t('question-validation-msg')['add-answer-with-num'],['1'])}`}</span>}
            {exampleErr && <span>{`* ${t(t('question-validation-msg')['example-allow-total'],['1'])}`}</span>}
          </div>
        )}
        <div style={{ width: `${sizeWordExport}px`, position: 'fixed', zIndex:'-11', visibility:"hidden"}}>
          <div
            id="content-draw-image-export-id"
            style={{
              width: '100%',
              padding: '1rem 8rem',
              display: 'flex',
              justifyContent: 'flex-start',
              alignItems: 'center',
              flexDirection: 'column',
            }}
          >
            <img
              id="image-export-question-id"
              alt=""
              style={{
                maxWidth: `${sizeWordExport}px`,
                visibility:"hidden",
                maxHeight: '338px',
              }}
              onLoad={(e) => {
                const container = document.getElementById(
                  'content-draw-image-export-id',
                ) as HTMLElement
                const canvas = document.getElementById(
                  'canvas-export-id',
                ) as HTMLCanvasElement
                canvas.setAttribute('height', `${container.offsetHeight}`)
                canvas.setAttribute('width', `${container.offsetWidth}`)
              }}
            ></img>
          </div>
          <canvas
            id="canvas-export-id"
            style={{ position: 'absolute', top: 0, left: 0 }}
          ></canvas>
        </div>
      </div>
    </div>
  )
}
type PropsType = {
  labelDefault?: string
  isError?: any
  url: string
  onChangeFile?: (file: FileList) => void
  viewMode?: boolean
  style?: CSSProperties
}

type Answer = {
  text: string
}
