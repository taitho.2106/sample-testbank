import React, { useEffect, useState, useRef, useCallback } from 'react'

import { Button } from 'rsuite'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'
import browserFile from 'lib/browserFile'

import AudioInput from '../AudioInput'
import SelectFromList2Question from './question'
import useTranslation from '@/hooks/useTranslation'

export default function SelectFromList2({
  question,
  register,
  setValue,
  isAudio = false,
  isImage = false,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const imageList = question?.image?.split('#') ?? []
  const correctList = question?.correct_answers?.split('#') ?? []
  const answerList = question?.answers?.split('#') ?? []
  const questionList = (question?.question_text ?? '')?.split('#') ?? []
  const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
    questionList?.map((g: string, gIndex: number) => {
      return {
        key: new Date().getTime().toString() + gIndex,
        imageStr: imageList[gIndex] || '',
        correctStrs: correctList[gIndex] || '',
        questionStr: g || '',
        answerStrs: answerList[gIndex] || '',
      }
    }) ?? [],
  )
  const answerGroupRef = useRef(answerGroup)
  const imageFiles = useRef(answerGroup.map((m) => null))

  useEffect(() => {
    if (isImage) {
      register('image_files', {
        validate: {
          acceptedFormats: async (files: any) => {
            // check format if has file
            if (!files) {
              return true
            }
            const checkFile = files?.filter((item: any) => item !== null)
            if (checkFile?.length === 0) {
              return true
            }
            let mIndex = -1
            let modeError = 0; //1:format, 2:deleted
            for await (const file of files) {
              mIndex += 1
              if (file !== null) {
                if (!['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)) {
                  answerGroup[mIndex].imageStr = ''
                  modeError = 1
                  continue
                } else if (!(await browserFile.checkFileExist(file))) {
                  answerGroup[mIndex].imageStr = ''
                  modeError = 2
                  continue
                } else {
                  continue
                }
              }
            }
            setValue("image",answerGroup.map(m=>m.imageStr).join("#"))
            if (modeError) {
              return t('question-validation-msg')['list-images']
            } else {
              return true
            }
          },
        },
      })
      register('image', {
        validate: (value: string) => {
          const errs = (value ?? '')
            .split('#')
            .map((m, mIndex) => {
              if (m === '') return mIndex
              return null
            })
            .filter((m) => m !== null)
          return errs.length > 0 ? errs.join() : true
        },
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['image-max-length'], ['2000']),
        },
        value: question?.image,
      })
    }
    if (isAudio) {
      register('audio_file', {
        validate: {
          acceptedFormats: async(file) => {
            // check format if has file
            if (!file) {
              return true
            }
            if(file.size ==0){
              return t('question-validation-msg')['audio-link-error']
            }
            if(!['audio/mp3','audio/mpeg'].includes(file?.type)){
              return t('question-validation-msg')['audio-type']
            }
            if(!await browserFile.checkFileExist(file)){
              return t('question-validation-msg')['audio-link-error']
            }
            return true;
          },
        },
      })
      register('audio', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['audio-max-length'],['2000']),
        },
        required: t('question-validation-msg')['audio-required'],
        value: question?.audio,
      })
      register('audio_script', {
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
        },
        value: question?.audio_script,
      })
    }
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '').split('#').map((m) =>
          m
            .split('*')
            .map((g, gIndex) => {
              if (g === '') return gIndex
              return null
            })
            .filter((g) => g !== null),
        )
        return errs.some((m) => m.length > 0) ? errs.map((m) => m.join()).join('#') : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers,
    })
    register('question_text', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
    },
      value: question?.question_text,
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [answerGroup])

  useEffect(() => {
    answerGroupRef.current = answerGroup
    if (answerGroup) {
      setValue('question_text', answerGroup.map((m) => m.questionStr).join('#'))
      setValue('answers', answerGroup.map((m) => m.answerStrs).join('#'))
      setValue('correct_answers', answerGroup.map((m) => m.correctStrs).join('#'))
      setValue('image', answerGroup.map((m) => m.imageStr).join('#'))
    }
  }, [answerGroup])

  const onChange = (index: number, data: any) => {
    answerGroupRef.current[index].questionStr = data.question
    answerGroupRef.current[index].answerStrs = data.answers
    answerGroupRef.current[index].correctStrs = data.corrects
    setAnswerGroup([...answerGroupRef.current])
  }

  const onChangeAudioScript = (data: string) => {
    setValue('audio_script', data)
  }

  const onChangeFile = (files: FileList) => {
    setValue('audio_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('audio', urlAudio, {
      shouldValidate: true,
    })
  }

  const onAddQuestion = () => {
    const newQuestion: any = {
      key: new Date().getTime().toString() + (answerGroup?.length ?? 0),
      imageStr: '',
      correctStrs: '',
      questionStr: '',
      answerStrs: '',
    }
    if (answerGroup) {
      setAnswerGroup([...answerGroup, newQuestion])
    } else {
      setAnswerGroup([newQuestion])
    }
    imageFiles.current.push(null)
  }

  const onChangeImage = (index: number, files: FileList) => {
    if (files && files[0]) {
      const urlImage = URL.createObjectURL(files[0])
      answerGroup[index].imageStr = urlImage
      imageFiles.current[index] = files[0]
      setAnswerGroup([...answerGroup])
      setValue('image_files', imageFiles.current)
    } else {
      answerGroup[index].imageStr = ''
      imageFiles.current[index] = null
      setAnswerGroup([...answerGroup])
    }
  }

  const onRemoveQuestion = (index: number) => {
    const ans = answerGroup.filter((m: any, mIndex: any) => mIndex !== index)
    setAnswerGroup([...ans])
  }

  const listQError = (errors['question_text']?.message ?? '').split(',')
  const listAError = errors['answers'] ? errors['answers']?.message.split('#') : []
  const listCError:string[] = (errors['correct_answers']?.message ?? '').split(',') ?? []
  const listImageError = (errors['image']?.message ?? '').split(',')
  return (
    <div className="m-select-from-list">
      {isAudio && (
        <AudioInput
          url={question?.audio}
          script={question?.audio_script}
          isError={errors['audio'] || errors['audio_file']}
          onChangeFile={onChangeFile}
          onChangeScript={onChangeAudioScript}
          viewMode={viewMode}
          style={{ height: '468px' }}
        />
      )}
      <div style={{ flex: 1, position: 'relative' }}>
        <TextAreaField
          label={t('create-question')['question-description']}
          setValue={setValue}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          style={{ height: '6rem' }}
          className={`form-input-stretch ${errors['question_description'] ? 'error' : ''}`}
          viewMode={viewMode}
          specialChar={['#', '*']}
        />
        {answerGroup.map((m, mIndex) => (
          <SelectFromList2Question
            key={m.key}
            image={m.imageStr}
            questionStr={m.questionStr || ''}
            answerStr={m.answerStrs}
            correctStr={m.correctStrs}
            onChange={onChange.bind(null, mIndex)}
            onChangeImage={onChangeImage.bind(null, mIndex)}
            onRemove={onRemoveQuestion.bind(null, mIndex)}
            error={
              listQError.includes(mIndex.toString()) 
            }
            errorStr={listAError[mIndex] ?? ''}
            errorAddAnswer={listAError[mIndex] ?? ''}
            errorRadioClassName={listCError.includes(mIndex.toString()) ? 'error' : ''}
            image_error={listImageError.includes(mIndex.toString())}
            viewMode={viewMode}
          />
        ))}

        <div className="form-input-stretch">
          {!viewMode && (
            <Button
              appearance={'primary'}
              type="button"
              style={{
                backgroundColor: 'white',
                width: '20rem',
                color: '#5CB9D8',
                fontSize: '1.6rem',
                fontWeight: 600,
                border: answerGroup.length === 0 ? '1px solid #dd3a09' : 'none',
              }}
              onClick={onAddQuestion}
            >
              <label className="add-new-answer">
                <img
                  src="/images/icons/ic-plus-sub.png"
                  width={17}
                  height={17}
                  alt="add-new-answer-icon"
                />{' '}
                {t('create-question')['add-question']}
              </label>
            </Button>
          )}
        </div>
      </div>
    </div>
  )
}

type AnswerGroup = {
  key: string
  imageStr: string
  correctStrs: string
  answerStrs: string
  questionStr: string
}
