import { useCallback, useEffect, useRef, useState, KeyboardEvent } from 'react'

import { Button, Tooltip, Whisper } from 'rsuite'

import { MyCustomCSS } from '@/interfaces/types'
import { ContentField } from 'components/atoms/contentField'
import useTranslation from '@/hooks/useTranslation'

type PropsType = {
  image: string
  questionStr: string
  answerStr: string
  correctStr: string
  isMulti?: boolean
  image_error?: boolean
  error?: boolean
  viewMode?: boolean
  errorStr?: string
  onChange?: (data: any) => void
  onChangeImage?: (files: FileList) => void
  onRemove?: () => void
  errorRadioClassName: string
  errorAddAnswer: string;
}

export default function SelectFromList2Question({
  image,
  questionStr,
  answerStr,
  correctStr,
  isMulti = false,
  image_error = false,
  error = false,
  viewMode = false,
  errorStr = '',
  onChange,
  onChangeImage,
  onRemove,
  errorRadioClassName,
  errorAddAnswer
}: PropsType) {
  const {t} = useTranslation()
  const listCorrect = correctStr?.split('#') ?? []
  const [selectedValue, setSelectedValue] = useState(null)

  const inputRef = useRef(null)
  const tableRef = useRef<HTMLDivElement>(null)


  const convertListInput = useCallback((strV: string, listC: string[]) => {
    if (strV === '') return []
    const listV: Answer[][] =
      strV?.split('#').map((m) =>
        m.split('*').map((g) => ({
          text: g,
          isCorrect: listC.includes(g),
        })),
      ) ?? []
    return listV
  }, [])

  const listInput = useRef(convertListInput(answerStr, listCorrect))
  const questionInput = useRef(questionStr)
  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  useEffect(() => {
    if (selectedValue === null) return
    if (isInit.current) {
      isInit.current = false
    } else {
      onUpdateCorrectAnswer()
    }
  }, [selectedValue])

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }

  const onUpdateCorrectAnswer = useCallback(() => {
    onChange &&
      onChange({
        question: questionInput.current,
        answers: listInput.current
          .map((m: any) => m.map((g: any) => g.text).join('*'))
          .join('#'),
        corrects: listInput.current
          .map((m: any) => m.find((g: any) => g.isCorrect)?.text)
          .join('#'),
      })
  }, [])

  const onChangeContent = (data: any) => {
    questionInput.current = data.replace(/\#\#/g, '%s%')
    onUpdateCorrectAnswer()
  }

  const onTabCreated = (id: string, index: number) => {
    listInput.current.splice(index, 0, [])
    // onUpdateCorrectAnswer()
  }

  const onTabsDeleted = (indexes: number[]) => {
    listInput.current = listInput.current.filter(
      (m, mIndex) => !indexes.includes(mIndex),
    )
    onUpdateCorrectAnswer()
    onClosePopup()
  }

  const onTabClick = (e: React.MouseEvent<HTMLDivElement>, index: number) => {
    const input = e.target as HTMLInputElement
    setSelectedValue({ index: index, value: listInput.current[index] })

    const inputRect = input.getBoundingClientRect()
    const parentRect = inputRef.current.getBoundingClientRect()
    const offsetTop = inputRect.top - parentRect.top + inputRect.height + 10
    let offsetLeft = inputRect.left - parentRect.left + inputRect.width / 2 - 5
    let tableLeft = 0
    if (offsetLeft > 360) {
      tableLeft = offsetLeft - 360
      offsetLeft = 360
    }
    tableRef.current.style.top = `${offsetTop}px`
    tableRef.current.style.left = `${tableLeft}px`
    tableRef.current.style.display = 'unset'
    tableRef.current.style.setProperty('--offset-left', `${offsetLeft}px`)
  }

  const onClosePopup = () => {
    // setSelectedValue(null)
    tableRef.current.style.display = null
  }

  const onChangeIsCorrect = (answerIndex: number) => {
    selectedValue.value.forEach((m: any, mIndex: number) => {
      m.isCorrect = mIndex === answerIndex
    })
    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })
  }

  const onChangeAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value
    const item = listInput.current[selectedValue.index]
    item[answerIndex].text = e.target.value
    setSelectedValue({ ...selectedValue })
  }
  const onBlurAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value = e.target.value.trim()
    const item = listInput.current[selectedValue.index]
    item[answerIndex].text = e.target.value
    setSelectedValue({ ...selectedValue })
  }

  const onAddAnswer = () => {
    selectedValue.value.push({ text: '', isCorrect: false })
    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })
  }

  const onDeleteAnswerText = (answerIndex: number) => {
    selectedValue.value.splice(answerIndex, 1)
    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })
  }

  const onClickRemove = () => {
    onRemove && onRemove()
  }
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
      return
    }
  }, [])

  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)
  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }

  return (
    <div className="box-wrapper" style={{ position: 'relative' }}>
      <ImageInputSL2
        url={image}
        onChangeFile={onChangeImage}
        isError={image_error}
        viewMode={viewMode}
      />
      <div className="form-input-stretch" style={{ position: 'relative' }}>
        <ContentField
          ref={inputRef}
          label={t('common')['question']}
          strValue={(questionStr ?? '').replace(/\%s\%/g, `##`)}
          onBlur={onChangeContent}
          isMultiTab={isMulti}
          disableTabInput={true}
          onTabClick={onTabClick}
          onTabCreated={onTabCreated}
          onTabsDeleted={onTabsDeleted}
          className={error ? 'error' : ''}
          viewMode={viewMode}
          specialChar={['#','*']}
        >
          <div className="box-action-info">
            <div style={{ display: 'flex', alignItems: 'flex-end' }}>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onBold}
                style={
                  {
                    '--image': 'url(/images/icons/ic-bold.png)',
                  } as MyCustomCSS
                }
              />
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onItalic}
                style={
                  {
                    '--image': 'url(/images/icons/ic-italic.png)',
                  } as MyCustomCSS
                }
              />
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onUnderline}
                style={
                  {
                    '--image': 'url(/images/icons/ic-underline.png)',
                  } as MyCustomCSS
                }
              />
              <Whisper
                placement="top"
                trigger={'hover'}
                speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
              >
                <Button className="add-new-answer" onMouseDown={onAddSpace}>
                <img src="/images/icons/ic-underscore.png" alt=""/>
                </Button>
              </Whisper>
              <div
                className="clear-outline"
                style={{ backgroundColor: '#f5f8fc' }}
              ></div>
            </div>
          </div>
        </ContentField>
        {/* <div className="box-action-info">
          <span className="add-new-answer" onMouseDown={onAddSpace}>
            <img src="/images/icons/ic-plus-sub.png" width={17} height={17} />{' '}
            Thêm khoảng trống
          </span>
        </div> */}
        <div ref={tableRef} className="popup-table">
          {selectedValue && (
            <table className={`question-table`}>
              <thead>
                <tr>
                  <th style={{ width: '105px' }}>{t('create-question')['correct-answer']}</th>
                  <th align="left">{t('create-question')['answer-key']}</th>
                  <th className="action">
                    <img
                      src="/images/icons/ic-close-dark.png"
                      width={20}
                      height={20}
                      onClick={onClosePopup}
                      style={{ pointerEvents: 'all', cursor: 'pointer' }}
                      alt="close-icon"
                    />
                  </th>
                </tr>
              </thead>
              <tbody>
                {selectedValue?.value?.map(
                  (answer: Answer, answerIndex: number) => (
                    <tr key={`${groupKey}_${answerIndex}`}>
                      <td align="center" className={errorRadioClassName}>
                        <input
                          type="radio"
                          name={groupKey.toString()}
                          checked={answer.isCorrect}
                          onChange={onChangeIsCorrect.bind(null, answerIndex)}
                        />
                      </td>
                      <td className={errorStr.split(',').includes(answerIndex.toString()) ? 'error' : ''}>
                        <input
                          type="text"
                          value={answer.text}
                          onKeyDown={onKeyDown}
                          onChange={onChangeAnswerText.bind(null, answerIndex)}
                          onBlur={onBlurAnswerText.bind(null, answerIndex)}
                        />
                      </td>
                      <td align="right">
                        <img
                          onClick={onDeleteAnswerText.bind(null, answerIndex)}
                          className="ic-action"
                          src="/images/icons/ic-trash.png"
                          alt="delete-answer-icon"
                        />
                      </td>
                    </tr>
                  ),
                )}
                {!viewMode && (
                  <tr className="action">
                    <td></td>
                    <td colSpan={2} align="left" >
                      <label className={`add-new-answer ${(errorAddAnswer !== '' && selectedValue.value.length === 0) ? 'error' : ''}`} onClick={onAddAnswer}>
                        <img
                          src="/images/icons/ic-plus-sub.png"
                          width={17}
                          height={17}
                          alt="add-new-answer-icon"
                        />{' '}
                        {t('create-question')['add-answer']}
                      </label>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          )}
        </div>
      </div>
      <img
        className="ic-delete-question"
        src="/images/icons/ic-trash.png"
        style={{ top: '0.3rem', right: '0.3rem' }}
        onClick={onClickRemove}
        alt="icon-delete-question"
      />
    </div>
  )
}

type Answer = {
  text: string
  isCorrect: boolean
}

type ImagePropsType = {
  labelDefault?: string
  isError?: any
  url: string
  onChangeFile?: (file: FileList) => void
  viewMode?: boolean
}
function ImageInputSL2({
  url,
  onChangeFile,
  isError,
  viewMode = false,
}: ImagePropsType) {
  const {t} = useTranslation()
  const [imageUrl, setImageUrl] = useState(url ?? '')
  const fileRef = useRef(null)
  const src = imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`
  return (
    <div
      className={`box-image-input-sl2 ${imageUrl !== '' ? 'has-data' : ''} ${
        isError ? 'error' : ''
      }`}
    >
      <div className="image-section">
        <div className="image-input-origin">
          {src && url ? (
            <>
              <img
                className="image-upload"
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                src={src}
                alt="image-upload"
                style={{ cursor: 'pointer' }}
              />
              {!viewMode && src && url && (
                <img
                  className="image-delete-icon"
                  src="/images/icons/ic-remove-opacity.png"
                  alt=""
                  onClick={() => {
                    fileRef.current.value = ''
                    event.preventDefault()
                    event.stopPropagation()
                    onChangeFile(null)
                  }}
                />
              )}
            </>
          ) : (
            <>
              <div
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  cursor: 'pointer',
                }}
              >
                <img
                  className="icon"
                  src="/images/icons/ic-image.png"
                  alt="delete-img-icon"
                />
                {t('create-question')['image']}
              </div>
            </>
          )}
          <input
            ref={fileRef}
            type="file"
            style={{ display: 'none' }}
            accept=".png,.jpeg,.jpg"
            onChange={(e) => {
              if (fileRef.current.files.length > 0) {
                setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                onChangeFile && onChangeFile(e.target.files)
              }
            }}
          />
        </div>
      </div>
    </div>
  )
}