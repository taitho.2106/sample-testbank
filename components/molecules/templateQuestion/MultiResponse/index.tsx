import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'
import browserFile from 'lib/browserFile'
import { useEffect } from 'react'
import AudioInput from '../AudioInput'
import MultiResponseTable from './table'
import useTranslation from '@/hooks/useTranslation'

export default function MultiResponse({
  question,
  register,
  setValue,
  isAudio = false,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  useEffect(() => {
    if (isAudio) {
      register('audio_file',{
        validate: {
          acceptedFormats: async(file) => {
            // check format if has file
            if (!file) {
              return true
            }
            if(file.size ==0){
              return t('question-validation-msg')['audio-link-error']
            }
            if(!['audio/mp3','audio/mpeg'].includes(file?.type)){
              return t('question-validation-msg')['audio-type']
            }
            if(!await browserFile.checkFileExist(file)){
              return t('question-validation-msg')['audio-link-error']
            }
            return true;
          },
        }
      })
      register('audio', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['audio-max-length'],['2000']),
        },
        required: t('question-validation-msg')['audio-required'],
        value: question?.audio,
      })
      register('audio_script', {
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
        },
        value: question?.audio_script,
      })
    }
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m != null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '###',
    })
    register('correct_answers', {
      required: t('question-validation-msg')['correct-answers-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [])

  const onChangeAudioScript = (data: any) => {
    if (isAudio) {
      setValue('audio_script', data)
    }
  }

  const onCorrectAnswerChange = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers)
  }

  const onChangeFile = (files: FileList) => {
    setValue('audio_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('audio', urlAudio)
  }

  return (
    <div
      className="m-multi-response"
      style={{ display: 'flex', flexDirection: 'row' }}
    >
      {isAudio && (
        <AudioInput
          url={question?.audio}
          script={question?.audio_script}
          isError={errors['audio'] || errors['audio_file']}
          onChangeFile={onChangeFile}
          onChangeScript={onChangeAudioScript}
          style={{ height: '398px' }}
          viewMode={viewMode}
        />
      )}
      <div style={{ flex: 1 }}>
        <TextAreaField
          label={t('create-question')['question-description']}
          setValue={setValue}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          style={{ height: '9rem' }}
          viewMode={viewMode}
          specialChar={['#','*']}
        />
        <MultiResponseTable
          className={`form-input-stretch ${
            errors['answers']?.message === '' || errors['correct_answers']
              ? 'error'
              : ''
          }`}
          answerStr={question?.answers ?? '###'}
          correctStr={question?.correct_answers}
          onChange={onCorrectAnswerChange}
          errorStr={errors['answers']?.message ?? ''}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}
