import { useEffect, useRef } from 'react'

import { Button, Tooltip, Whisper } from 'rsuite'

import convertFontStyle from '@/questions_middleware/convertFontStyle'
import convertSpaceWord from '@/questions_middleware/convertSpaceWord'
import { ContentField } from 'components/atoms/contentField'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionPropsType } from 'interfaces/types'

import AudioInput from '../AudioInput'
import MultiChoiceGroupQuestion from './question'
import browserFile from 'lib/browserFile'
import useTranslation from "@/hooks/useTranslation";

export default function MultiChoiceGroupExample({
  question,
  register,
  setValue,
  isAudio = false,
  errors = {},
  viewMode,
  questionType,
}: QuestionPropsType) {
  const { t } = useTranslation()
  useEffect(() => {
    register('question_text', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
      },
      value: question?.question_text,
    })
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '').split('#').map((m) =>
          m
            .split('*')
            .map((g, gIndex) => {
              if (g === '') return gIndex
              return null
            })
            .filter((m) => m !== null),
        )
        return errs.some((m) => m.length > 0)
          ? errs.map((m) => m.join()).join('#')
          : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
    if (isAudio) {
      register('audio_file',{
        validate: {
          acceptedFormats: async(file) => {
            // check format if has file
            if (!file) {
              return true
            }
            if(file.size ==0){
              return t('question-validation-msg')['audio-link-error']
            }
            if(!['audio/mp3','audio/mpeg'].includes(file?.type)){
              return t('question-validation-msg')['audio-type']
            }
            if(!await browserFile.checkFileExist(file)){
              return t('question-validation-msg')['audio-link-error']
            }
            return true;
          },
        }
      })
      register('audio', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['audio-max-length'],['2000']),
        },
        required: t('question-validation-msg')['audio-required'],
        value: question?.audio,
      })
      register('audio_script', {
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
        },
        value: question?.audio_script,
      })
    } else {
      register('parent_question_text', {
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['parent-question-text-max-length'],['10000']),
        },
        required: t('question-validation-msg')['parent-question-text-required'],
        value: question?.parent_question_text,
      })
    }
  }, [])
  const inputRef = useRef(null)

  const onMCChange = (dataV: any) => {
    setValue('answers', dataV.answers)
    setValue('correct_answers', dataV.corrects)
    setValue('question_text', dataV.questions.trim())
  }

  const onChangeAudioScript = (data: any) => {
    setValue('audio_script', data)
  }
  const onChangeParentQuestionText = (data: string) => {
    setValue('parent_question_text', data)
  }
  const onChangeFile = (files: FileList) => {
    setValue('audio_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('audio', urlAudio)
  }
  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)

  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }
  return (
    <div className="m-multi-choice m-multi-choice-group-example" style={{ display: 'flex' }}>
      {!isAudio ? (
        <div
          className={`form-input-stretch`}
          style={{
            flex: 1,
            display: 'flex',
            flexDirection: 'column',
            position: 'relative'
          }}
        >
          <ContentField
            ref={inputRef}
            label={t('create-question')['reading-text']}
            strValue={question?.parent_question_text?.replace(/\%s\%/g, '##')}
            disableTabInput={true}
            isMultiTab={true}
            onBlur={onChangeParentQuestionText}
            // onBlur={onChangeParentQuestionText.bind(null,)}
            style={{ flex: 1 }}
            className={`${errors['parent_question_text'] ? 'error' : ''}`}
            inputStyle={{ paddingRight: '3rem', height: '100%' }}
            viewMode={viewMode}
            specialChar={['#','*']}
            disableTab={true}
          >
            <div className="box-action-info">
              <div style={{ display: 'flex', alignItems: "flex-end" }}>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onBold}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-bold.png)',
                    } as MyCustomCSS
                  }
                ></div>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onItalic}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-italic.png)',
                    } as MyCustomCSS
                  }
                ></div>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onUnderline}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-underline.png)',
                    } as MyCustomCSS
                  }
                ></div>
              </div>
              <div className="clear-outline" style={{}}></div>
            </div>
          </ContentField>
        </div>
      ) : (
        <AudioInput
          url={question?.audio}
          script={question?.audio_script}
          isError={errors['audio'] || errors['audio_file']}
          onChangeFile={onChangeFile}
          onChangeScript={onChangeAudioScript}
          style={{ height: '468px' }}
          viewMode={viewMode}
        />
      )}
      <div style={{ flex: 1 }}>
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            label={t('create-question')['question-description']}
            setValue={setValue}
            register={register('parent_question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 2000,
                message: t(t('question-validation-msg')['parent-description-required'], ['2000']),
              },
            })}
            defaultValue={question?.parent_question_description}
            style={{ height: '9rem' }}
            inputStyle={isAudio ? { paddingBottom: '4.5rem' } : {}}
            className={errors['parent_question_description'] ? 'error' : ''}
            viewMode={viewMode}
            specialChar={['#','*']}
          />
        </div>
        <MultiChoiceGroupQuestion
          questionType={questionType}
          questions={question?.question_text}
          answers={question?.answers}
          corrects={question?.correct_answers}
          onQuestionChange={onMCChange}
          errorStr={errors['answers']?.message ?? ''}
          correctErrorStr={errors['correct_answers']?.message ?? ''}
          questionErrorStr={errors['question_text']?.message ?? ''}
          // clearInputOutlineStyle={{backgroundColor: '#f5f8fc'}}
          viewMode={viewMode}
          register={register}
          setValue={setValue}
        />
      </div>
    </div>
  )
}
