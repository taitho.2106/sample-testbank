import { useEffect } from 'react'

import { QuestionPropsType } from "@/interfaces/types";
import { InputField } from "components/atoms/inputField";
import browserFile from 'lib/browserFile';

import QuestionTable from "./questionTable";
import useTranslation from '@/hooks/useTranslation';

export default function MatchingGameType5({
    question,
    register,
    setValue,
    errors = {},
    viewMode = false,
  }: QuestionPropsType) {
    const {t} = useTranslation()
    useEffect(() => {
        register('correct_answers',{
            maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
            },
            value: question?.correct_answers,
        })

        register('answers', {
            validate: (value: string) => {
              
              if (!value) {
                setValue('errDataQuestion', t(t('question-validation-msg')['input-required'], ['1']))
              } else {
                setValue('errDataQuestion', "")
                const leftRight = value.split('#')
                if(!leftRight[0]){
                  setValue('errDataQuestion',t(t('question-validation-msg')['input-required'], ['1']))
                }
                if (leftRight[2]) {
                  const checkModeArr = leftRight[2].split('*')||[]
                  if(checkModeArr.length>0){
                    if (checkModeArr.filter((m: string) => m == 'ex').length > 1) {
                      setValue('passDataExample',false)
                    }else{
                      setValue('passDataExample',true)
                    }
                    if (checkModeArr.filter((m: string) => m == '').length == 0) {
                        setValue('passDataQuestion',false)
                    }else{
                      setValue('passDataQuestion',true)
                    }
                  }
                }
              }
              const errs = (value ?? '#').split('#').map((m, indexSplit) =>
                m
                  .split('*')
                  .map((g, gIndex) => {
                    if (g === '' && indexSplit < 2) return gIndex
                    const temp = m.split('*').filter(n => n === g)
                    if(temp.length > 1 && indexSplit === 1) return gIndex
                    return null
                  })
                  .filter((m) => m !== null),
              )
              return errs.some((m) => m.length > 0)
                ? errs.map((m) => m.join()).join('#')
                : true
            },
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['total-question-answer'], ['10000']),
            },
            value: question?.answers ?? '**#**',
        })

        register('passDataQuestion', {
            validate: (value: string) => {
              return value
            },
        })

        register('passDataExample', {
            validate: (value: string) => {
              return value
            },
        })

        register('image_files',{
            validate:  async(files:any) =>{
                  // check format if has file
                  if (!files) {
                    return true
                  }
                  let fileErr: number[] = []
                  let mIndex = -1
                  for await (const file of files) {
                    mIndex += 1
                    if (file !== null) {
                      if (!['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)) {
                        fileErr.push(mIndex)
                        continue
                      } else if (!(await browserFile.checkFileExist(file))) {
                        fileErr.push(mIndex)
                        continue
                      } else {
                        fileErr.push(null)
                        continue
                      }
                    }
                    fileErr.push(null)
                  }
                  fileErr= fileErr.filter(m=>m!=null)
                    return fileErr.length > 0 ? fileErr.join('#') : true
                },
        })
        register('image', {
            validate: (value: string) => {
                const errs = (value ?? '').split('#').map((m, mIndex) => {
                    if (m.length === 0) return mIndex
                    return null
                })
                .filter((m) => m !== null)
                return errs.length > 0 ? errs.join('#') : true
            },
            maxLength: {
                value: 2000,
                message: t(t('question-validation-msg')['image-max-length'], ['2000']),
            },
            value: question?.image ?? '##',
        })
    }, [])

    const onCorrectAnswerChange = (data: any) => {
        // console.log("data=====onChange======",data);
        setValue('answers', data.answers)
        setValue('correct_answers', data.correct_answers)
        setValue('image',data.imageStr)
        setValue('image_files',data.image_files)
    }
    console.log("errrr===",errors);
    return (
        <div className="m-matching-game-type-5">
        <div className="m-matching-game-type-5__right">
            <div className="form-input-stretch">
                <InputField
                    label={t('create-question')['question-description']}
                    type="text"
                    setValue={setValue}
                    register={register('question_description', {
                      required: t('question-validation-msg')['description-required'],
                      maxLength: {
                        value: 10000,
                        message: t(t('question-validation-msg')['description-max-length'], ['10000']),
                      },
                    })}
                    defaultValue={question?.question_description}
                    className={`form-input-stretch ${
                        errors['question_description'] ? 'error' : ''
                    }`}
                    preventKey={['#','*']}
                    style={{
                        width:'100%',
                        marginLeft:'0',
                        marginRight:'0'
                    }}
                />
                <div className="m-matching-game-type-5__table-matching">
                    <QuestionTable 
                        className={`${errors['answers']?.message === '' ? 'error' : ''}`}
                        errors = {errors}
                        setValue={setValue}
                        register={register}
                        errorStr={errors['answers']?.message ?? ''}
                        onChange={onCorrectAnswerChange}
                        viewMode={viewMode}
                        errExample={errors['passDataExample']}
                        answerStr={question?.answers ?? '**#**'}
                        correctStr={question?.correct_answers}
                        imageStr={question?.image}
                        errorCorrectStr={errors['correct_answers'] ? 'error' : ""}
                        errorImageStr={errors['image']?.message  ?? ''}
                        errorFileImageStr={errors['image_files']?.message ?? ''}
                    />
                </div>
            </div>
        </div>
    </div>
    );
};
