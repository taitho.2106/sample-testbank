import useTranslation from "@/hooks/useTranslation"
import { useState, useRef, ChangeEvent,useEffect } from "react"

import ContentEditable from "react-contenteditable"
import { SelectPicker, Toggle } from "rsuite"

type PropType = {
    answerStr: string
    correctStr: string
    styleBold?: boolean
    className?: string
    errorStr?: string
    errorCorrectStr?:string
    onChange: (data: any) => void
    register?:any
    setValue?:any
    errors?:any,
    viewMode:boolean,
    errExample?:any,
    imageStr?:string,
    errorImageStr?:string,
    errorFileImageStr?:string
  }


export default function QuestionTable({
    answerStr,
    correctStr,
    className = '',
    errorStr = '',
    onChange,
    register,
    setValue,
    errors,
    viewMode = false,
    errExample,
    errorCorrectStr,
    imageStr,
    errorImageStr,
    errorFileImageStr
  }: PropType) {
    const {t} = useTranslation()
    const correctList = correctStr?.split('#') ?? []
    const leftRight = answerStr?.split('#') ?? []
    const imageList = imageStr?.split('#') ?? []
    const modeQuestion = leftRight[2]?leftRight[2].split("*"):Array.from(Array(leftRight[0].split("*").length), () => '');
    if(modeQuestion.length < leftRight[0].split("*").length){
        modeQuestion.push(...Array.from(Array(leftRight[0].split("*").length - modeQuestion.length), () => ''))
    }
    const [modeQuestionsState, setModeQuestionState ] = useState(modeQuestion)
    const [strErrExample, setStrErrExample ] = useState("")
    const [strErrQuestion, setStrErrQuestion ] = useState("")
    const [left, setLeft] = useState<AnswerLeft[]>(
        leftRight[0]?.split('*')?.map((m, mIndex) => ({
        id: `${Math.random()}_${mIndex}`,
        text: m,
        imageStr: imageList[mIndex]
        })) ?? [],
    )
    const [right, setRight] = useState<AnswerRight[]>(
        leftRight[1]?.split('*')?.map((m, mIndex) => ({
        id: `${Math.random()}_${mIndex}`,
        text: m,
        order: correctList.findIndex((g) => g === m) + 1,
        })) ?? [],
    )

    const isInit = useRef(true)
    const imageFiles = useRef(left.map((m) => null))
    useEffect(() => {
        setValue('order',right.map((item:any)=>item.order).join('#'))
    if (isInit.current) {
        isInit.current = false
    } else {
        if (typeof onChange === 'function') {
        const rigtTemp = right.filter((m) => m.order > 0)
        onChange({
            answers: `${left.map((m) => (m.text ?? '').trim()).join('*')}#${right.map((m) => (m.text ?? '').trim()).join('*')}${modeQuestionsState && modeQuestionsState.filter(m=>m == "ex").length>0?`#${modeQuestionsState.join("*")}`:""}`,
            correct_answers: rigtTemp.sort((a, b) => a.order - b.order).map((m) => (m.text ?? '').trim()).join('#'),
            imageStr:left.map((item:AnswerLeft)=>item.imageStr).join('#'),
            image_files: imageFiles.current
        })
        }
    }
    }, [left, right,modeQuestionsState])
    useEffect(() => {
    if(modeQuestionsState.filter(m=> m == "ex").length >1){
        setStrErrExample(t(t('question-validation-msg')['example-allow-total'], ['1']))
    }else{
        setStrErrExample("")
    }
    if(modeQuestionsState.filter(m=> m == "").length ==0){
        setStrErrQuestion(t(t('question-validation-msg')['question-fb-required'], ['1']))
    }else{
        setStrErrQuestion("")
    }
    }, [modeQuestionsState])

    
    useEffect(()=>{
        register('order', {
            validate: (value: string) => {
            const lengthLeft = left.length;
            const arrLeft = Array.from(Array(lengthLeft),(item,i)=>"err");
            const err = value.split('#').map((item:any, i:number)=>{
                if(item >0 && item<=lengthLeft){
                arrLeft[parseInt(item)-1] = ""
                }
                if(i>=lengthLeft || item > 0) return true
                return false
            })
            if(arrLeft.find(m=> m!="") || err.find(m=>!m)){
                return `${arrLeft.join()}#${err.map(m=> m?"":"err").join(",")}`
            }else{
                return true
            }
            },
            value:right.map((item:any)=>item.order).join('#')
        })
    })
    const onAddAnswer = (index: number) => {
    if (index === 0) {
        left.push({ id: `${Math.random()}_0`, text: '' })
        modeQuestionsState.push("")
        setLeft([...left])
        setModeQuestionState([...modeQuestionsState])
    } else {
        right.push({ id: `${Math.random()}_0`, text: '', order: 0 })
        setRight([...right])
    }

    }
    const onChangeText = (index: number, answerIndex: number, event: any) => {
        const el: HTMLInputElement = event.currentTarget
        if (index === 0) {
        left[answerIndex].text = el.value.replace(/(\*|\#)/g, '')
        setLeft([...left])
        } else {
        right[answerIndex].text = el.value.replace(/(\*|\#)/g, '')
        setRight([...right])
        }
    }
    const onBlurText = (index: number, answerIndex: number, event: any) => {
        const el: HTMLInputElement = event.currentTarget
        if (index === 0) {
        left[answerIndex].text = el.value = el.value.replace(/(\*|\#)/g, '').trim()
        // setLeft([...left])
        } else {
        right[answerIndex].text = el.value = el.value.replace(/(\*|\#)/g, '').trim()
        // setRight([...right])
        }
    }
    const onChangeOrder = (value: any, answerIndex: any) => {
        if (value != NaN) {
        const rightTemp = [...right];
        const item = rightTemp.find(m=> m.order== value);
        if(item){
            item.order = 0;
        }
        rightTemp[answerIndex].order = value
        setRight([...rightTemp])
        setValue('order',rightTemp.filter((m:any) => m.order))
        }
    }

    const onDeleteAnswer = (index: number, answerIndex: number) => {
        if (index === 0) {
        left.splice(answerIndex, 1)
        if(modeQuestionsState&& modeQuestionsState.length>answerIndex ){
            modeQuestionsState.splice(answerIndex, 1)
        }
        if(answerIndex < imageFiles.current.length){
            imageFiles.current.splice(answerIndex, 1)
        }
        const rightClone = right.map((item:any)=> {
            const newOrder = item.order == answerIndex+1?0:item.order>left.length?0:item.order
            return {
            ...item,
            order:newOrder,
            }
        })
        setModeQuestionState([...modeQuestionsState])
        setLeft([...left])
        setRight([...rightClone])
        } else {
        
        right.splice(answerIndex, 1)
        setRight([...right])
        }
    }
    const onToggleExample = (index:number)=>{
        //only 1 example
        if(modeQuestionsState[index]== "ex"){
        modeQuestionsState[index]=""
        }else{
        modeQuestionsState[index] = "ex"
        }
        setModeQuestionState([...modeQuestionsState])
    }
    const onChangeImage = (index: number, files: FileList) => {
        if(files && files[0]){
            const urlImage = URL.createObjectURL(files[0])
            left[index].imageStr = urlImage
            imageFiles.current[index] = files[0]
            setLeft([...left])
            
        }else{
            left[index].imageStr = ''
            imageFiles.current[index] = null
            setLeft([...left])
        }

    }
    const listError = errorStr.split('#')
    const leftError = listError[0] ? listError[0].split(',') : []
    const rightError = listError[1] ? listError[1].split(',') : []
    const errOrderMessage = errors['order']?.message?.split("#")||[];
    const errOrderLeft = errOrderMessage[0]?.split(",")||[]
    const errOrderRight = errOrderMessage[1]?.split(",")||[]
    const listErrImage = errorImageStr.split('#') ?? []
    const listErrImageFile = errorFileImageStr.split('#') ?? []

    let indexE = -1;
    let indexA = -1;

    const renderIsErrEx = () => {
        if(modeQuestionsState){
            const lengthEx = modeQuestionsState.filter((m)=> m==='ex').length
            if(lengthEx > 1){
                return true
            }else{
                return false
            }
        }
        
    }
    useEffect(()=>{
        renderIsErrEx()
    },[modeQuestionsState])
    return (
        <div className={`question-table-MG5 ${className}`} style={{ position: 'relative' }}>
            <div className={`question-table-MG5__content-talbe`} style={{ position: 'relative', display:'flex' }}>
                <div className={`question-table-MG5__left`}>
                    <table className={`question-table question-table-MG5-table ${left.length === 0 || leftError.length > 0  || errorCorrectStr ? 'error' : ''}`}>
                        <thead className={`question-table-tf5-thead`}>
                            <tr>
                                <th
                                    dangerouslySetInnerHTML={{
                                        __html: t('create-question')['select-question-example']
                                    }}
                                ></th>
                                <th align="left"
                                    dangerouslySetInnerHTML={{
                                        __html: t('create-question')['question-example-image']
                                    }}
                                ></th>
                                <th align="left"
                                    dangerouslySetInnerHTML={{
                                        __html: t('create-question')['list-questions-examples-1']
                                    }}
                                ></th>
                                <th align="center"></th>
                            </tr>
                        </thead>
                        <tbody className={`question-table-tf5-tbody`}>
                            {left?.map((answer: AnswerLeft, answerIndex: number) => {
                                const showErrTr = (left.length > right.length)
                                const isExample = modeQuestionsState && modeQuestionsState[answerIndex] === "ex";
                                if(isExample){
                                indexE +=1;
                                }else{
                                indexA+=1;
                                }
                                
                                return (
                                    <tr key={`left_${answer.id}`}
                                        className={`${(showErrTr && left.length - 1 === answerIndex) || errOrderLeft[answerIndex] === 'err' ? 'error' : ''} ${isExample ? '--example' : ''}`}
                                        >
                                        <td align="center"
                                            className={`input-placeholder ${ isExample && renderIsErrEx() ? "toggle-error":""}`}
                                        >
                                            <>
                                                <Toggle
                                                    size="md"
                                                    defaultChecked={!isExample}
                                                    disabled={viewMode}
                                                    checkedChildren={t('common')['question']}
                                                    unCheckedChildren={t('common')['example']}
                                                    onChange={()=>onToggleExample(answerIndex)}
                                                />
                                            </>
                                        </td>
                                        <td
                                            className={`input-placeholder`}
                                        >
                                            <div className="image-input-mg5">
                                                <ImageInputMG5
                                                    className={`${(listErrImage.includes(answerIndex.toString()) || 
                                                        listErrImageFile.includes(answerIndex.toString())) ? 'error' : ''}`}
                                                    url={answer.imageStr}
                                                    onChangeFile={onChangeImage.bind(null, answerIndex)}
                                                    labelDefault={`${isExample ? t(t('create-question')['image-example-with-num'], [`${indexE + 1}`]) : t(t('create-question')['question-image'], [`${indexA + 1}`])}`}
                                                    viewMode={viewMode}
                                                />
                                            </div>
                                        </td>
                                        <td className={`input-placeholder ${leftError.includes(answerIndex.toString()) ? 'error' : ''}`}>
                                            <div className={`custom-input ${isExample ? '--example' : ''}`}>
                                                <span >___</span>
                                                <input 
                                                    id={`left_${answerIndex}`}
                                                    type="text"
                                                    defaultValue={answer.text}
                                                    className={`div-input`}
                                                    spellCheck={false}
                                                    autoComplete={'off'}
                                                    data-exist={answer.text != ''}
                                                    onChange={onChangeText.bind(null, 0, answerIndex)}
                                                    onBlur={onBlurText.bind(null, 0, answerIndex)}
                                                    placeholder={`${isExample ? `${t('common')['example']} ${indexE+1}` : `${t('common')['question']} ${indexA+1}`}`}
                                                    readOnly={viewMode}
                                                />
                                            </div>
                                        </td>
                                        <td align="right">
                                            <img
                                                onClick={onDeleteAnswer.bind(null, 0, answerIndex)}
                                                className="ic-action"
                                                src="/images/icons/ic-trash.png"
                                                alt=""
                                            />
                                        </td>
                                    </tr>
                            )})}
                            {!viewMode && left.length < 26 && (<tr className="question-table-mg5-action">
                                <td colSpan={4} align="left">
                                    <label className={`add-new-answer-mg5 ${left.length === 0 ? 'error' : ''}`} 
                                        onClick={onAddAnswer.bind(null, 0)}
                                    >
                                        <img
                                            src="/images/icons/ic-plus-sub.png"
                                            alt=""
                                            width={17}
                                            height={17}
                                        />{' '}
                                        {t('create-question')['add-question']}
                                    </label>
                                </td>
                            </tr>)}
                        </tbody>
                    </table>
                </div>
                <div
                    className={`question-table-MG5__right`}>
                <table className={`question-table question-table-MG5-table ${right.length === 0 || rightError.length > 0  || errorCorrectStr ? 'error' : ''}`}>
                    <thead>
                    <tr>
                        <th align="left"
                            dangerouslySetInnerHTML={{
                                __html: t('create-question')['answer-key-1']
                            }}
                        ></th>
                        <th align="left">{t('create-question')['position']}</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    {right?.map((answer: AnswerRight, answerIndex: number) => {
                        const pickerData = left.map((item: AnswerRight, index:number) => ({
                            "label": `${index + 1}`,
                            "value": index + 1,
                        }))
                        pickerData.unshift({
                            "label": '_',
                            "value": 0,
                        })
                        const isExample = modeQuestionsState && modeQuestionsState[answer.order - 1]=="ex";
                        return (
                            <tr key={answer.id} 
                            className={`${isExample ? "--example" : "" }`}
                            >
                                <td
                                    className={`input-placeholder ${
                                        rightError.includes(answerIndex.toString()) ? 'error' : ''
                                    }`}
                                >
                                    <input
                                        type="text"
                                        id={`right_${answerIndex}`}
                                        defaultValue={answer.text}
                                        onChange={onChangeText.bind(null, 1, answerIndex)}
                                        onBlur={onBlurText.bind(null, 1, answerIndex)}
                                        data-exist={answer.text != ''}
                                        spellCheck={false}
                                        autoComplete={'off'}
                                        placeholder={`${t('create-question')['answer']} ${answerIndex + 1}`}
                                    />
                                </td>
                                <td
                                    align='center'
                                    className={`input-placeholder ${errOrderRight[answerIndex] ? 'error' : ''} ${answer.order == 0 ? "order_default" : ""}`}
                                    style={{padding:0}}
                                >
                                    <SelectPicker 
                                        key={`${new Date().getMilliseconds()}`}
                                        onChange={(value, event) => onChangeOrder(value, answerIndex)}  
                                        data={pickerData}
                                        appearance="default" 
                                        placeholder=" " 
                                        value={answer.order}
                                        searchable={false} 
                                        cleanable={false} 
                                        size="xs"
                                    />
                                </td>
                                <td>
                                    <img alt=''
                                    className="ic-action"
                                    src="/images/icons/ic-trash.png"
                                    onClick={onDeleteAnswer.bind(null, 1, answerIndex)}
                                    />
                                </td>
                            </tr>
                        )})}
                        {!viewMode && right.length < 26 && (
                                <tr className="question-table-mg5-action">
                                <td colSpan={3} align="left">
                                    <label 
                                        className={`add-new-answer-mg5 ${right.length === 0 ? 'error' : ''}`} 
                                        onClick={onAddAnswer.bind(null, 1)}
                                    >
                                        <img
                                            src="/images/icons/ic-plus-sub.png"
                                            alt=""
                                            width={17}
                                            height={17}
                                        />{' '}
                                        {t('create-question')['add-answer']}
                                    </label>
                                </td>
                            </tr>
                            )}
                    </tbody>
                </table>
            </div>
            </div>
            {(strErrExample || strErrQuestion || errorFileImageStr) && 
                <div className='form-error-message'>
                    {strErrExample &&<span>{`* ${strErrExample}`}</span> }
                    {strErrQuestion &&<span>{`* ${strErrQuestion}`}</span> }
                    {errorFileImageStr &&<span>* Hình ảnh đã bị xóa hoặc không đúng định dạng (png, jpg, jpeg)</span> }
                </div>
            }
        </div>
    );
}

type PropsType = {
    labelDefault?: string
    url: string
    onChangeFile?: (files: FileList) => void
    viewMode?: boolean
    className?:string
}
function ImageInputMG5({ className = '', url, onChangeFile, labelDefault, viewMode }: PropsType) {
    const [imageUrl, setImageUrl] = useState(url ?? '')
    const fileRef = useRef(null)
    const src = imageUrl.startsWith('blob') ? url : `/upload/${url}`
    return (
        <div className={`image-mg5 ${imageUrl !== '' ? 'has-data' : ''} ${className}`}
        >
            <div className={`image`}  
                onClick={() => fileRef.current.dispatchEvent(new MouseEvent('click'))}>
                <input
                    ref={fileRef}
                    type={"file"}

                    accept=".png,.jpeg,.jpg"
                    onChange={(e) => {
                        if (fileRef.current.files.length > 0) {
                            setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                            onChangeFile && onChangeFile(e.target.files)
                        }
                    }}
                />
                <div className="image-container">
                        <div className="image-wraper">
                            {src && url ? (
                                <img
                                    src={src}
                                    alt=''
                                    className="image-upload"
                                />
                            ) : (
                                <img
                                    src="/images/icons/ic-image.png"
                                    alt=""
                                    className="image-default"
                                />
                            )}
                            {!viewMode && src && url && (
                                <img
                                    className="image-input-mg5___isDelete"
                                    src="/images/icons/ic-remove-opacity.png" alt=""
                                    onClick={(event:any)=>{
                                        event.preventDefault();
                                        event.stopPropagation();
                                        fileRef.current.value = ""
                                        onChangeFile(null)
                                    }}
                                />)}
                        </div>
                        <span className="label-image">{labelDefault}</span>
                    </div>

            </div>
            

        </div>
    )
}
type AnswerRight = {
    id: string
    text: string
    order?: number
}

type AnswerLeft = {
    id: string
    text: string
    order?: number
    imageStr?: string
}