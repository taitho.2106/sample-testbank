import { useEffect } from 'react'
import { QuestionPropsType } from 'interfaces/types'
import MultiChoiceTable from '../MultiChoice/table'
import { InputField } from 'components/atoms/inputField'
import useTranslation from '@/hooks/useTranslation'

export default function MultiChoicePronoun({
  question,
  register,
  setValue,
  errors = {},
}: QuestionPropsType) {
  const {t} = useTranslation()
  console.log("MultiChoicePronoun---------")
  useEffect(() => {
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('*')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***',
    })
    register('correct_answers', {
      required: 'Đáp án không được để trống',
      maxLength: {
        value: 10000,
        message: 'Đáp án không được lớn hơn 10000 ký tự',
      },
      value: question?.correct_answers,
    })
  }, [])

  const updateFormData = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers)
  }

  return (
    <div className="m-multi-choice">
      <div className="box-wrapper">
        <InputField
          label="Yêu cầu đề bài"
          type="text"
          setValue={setValue}
          register={register('question_description', {
            required: 'Yêu cầu đề bài không được để trống',
            maxLength: {
              value: 10000,
              message: 'Yêu cầu đề bài không được lớn hơn 10000 ký tự',
            },
          })}
          defaultValue={question?.question_description}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
        />
        <MultiChoiceTable
          className={`form-input-stretch ${
            errors['correct_answers'] ? 'error' : ''
          }`}
          answerStr={question?.answers ?? '***'}
          correctStr={question?.correct_answers}
          styleBold={true}
          onChange={updateFormData}
          errorStr={errors['answers']?.message ?? ''}
        />
      </div>
    </div>
  )
}
