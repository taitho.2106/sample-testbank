import { CSSProperties, useRef, useState } from 'react'

import CloseIcon from '@rsuite/icons/Close'
import { Button, Modal } from 'rsuite'

import { ContentField } from 'components/atoms/contentField'
import useTranslation from "@/hooks/useTranslation";

type PropsType = {
  style?: CSSProperties
  isError?: boolean
  url: string
  script: string
  viewMode?: boolean
  onChangeFile?: (files: FileList) => void
  onChangeScript?: (data: string) => void
}

export default function AudioInput({
  style,
  isError,
  url,
  script,
  viewMode = false,
  onChangeFile,
  onChangeScript,
}: PropsType) {
    const { t } = useTranslation()
  const [audioUrl, setAudioUrl] = useState(url ?? '')
  const fileRef = useRef(null)
  const audioRef = useRef(null)
  const [showAudioModal, setShowAudioModal] = useState(false)
  const audioScript = useRef(script)
  const audioScriptTemp = useRef(script)
  const [hasScript, setHasScript] = useState(
    script !== '' && script !== undefined,
  )

  const hasAudio = audioUrl !== ''

  return (
    <div
      className="box-wrapper"
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 0,
        height: '265px',
        ...style,
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
        }}
      >
        <div
          className={`box-audio-input ${isError ? 'error' : ''}`}
          onClick={() => fileRef.current.dispatchEvent(new MouseEvent('click'))}
        >
          <img
            src={
              hasAudio
                ? '/images/collections/upload-audio-active.png'
                : '/images/collections/upload-audio.png'
            }
            alt=""
          />
          <input
            ref={fileRef}
            type="file"
            style={{ display: 'none' }}
            accept=".mp3"
            onChange={(e) => {
              if (e.target.files.length > 0) {
                setAudioUrl(URL.createObjectURL(e.target.files[0]))
                onChangeFile && onChangeFile(e.target.files)
                if (audioRef.current) {
                  audioRef.current.pause()
                  audioRef.current.load()
                }
              }
            }}
          />
        </div>
        {hasAudio && (
          <audio
            controls
            style={{ marginTop: '1rem', pointerEvents: 'all' }}
            ref={audioRef}
          >
            <source
              src={
                audioUrl.startsWith('blob') ? audioUrl : `/upload/${audioUrl}`
              }
              type="audio/mpeg"
            />
          </audio>
        )}
        <Button
          appearance={'primary'}
          type="button"
          style={{
            marginTop: '2rem',
            backgroundColor: hasScript ? 'rgb(104, 104, 172)' : '#3498ff',
            pointerEvents: 'all',
          }}
          onClick={() => setShowAudioModal(true)}
        >
            {t('create-question')['listening-script']}
        </Button>
      </div>

      <Modal
        size="md"
        className="audio-script-modal"
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          margin: 0,
        }}
        open={showAudioModal}
        onClose={() => {
          setShowAudioModal(false)
          audioScriptTemp.current = audioScript.current
        }}
      >
        <CloseIcon
          style={{
            fontSize: '20px',
            alignItems: 'right',
            position: 'absolute',
            right: '1rem',
            top: '1rem',
            color: '#6868AB',
            zIndex: '1',
            background: '#fff',
          }}
          onClick={() => {
            setShowAudioModal(false)
            audioScriptTemp.current = audioScript.current
          }}
        />
        <ContentField
          label={t('create-question')['listening-script']}
          strValue={audioScript.current}
          disableTab={true}
          onChange={(data: string) => {
            audioScriptTemp.current = data
          }}
          style={{
            width: 'min(calc(100vw - 40px), 600px)',
            height: '300px',
            pointerEvents: viewMode ? 'none' : 'auto',
          }}
          disableStyleText={['b','i','u']}
          viewMode={viewMode}
        />
        {!viewMode && (
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              marginTop: '1rem',
            }}
          >
            <Button
              appearance={'primary'}
              type="button"
              onClick={() => {
                setShowAudioModal(false)
                audioScript.current = audioScriptTemp.current
                setHasScript(
                  audioScriptTemp.current !== '' &&
                    audioScriptTemp.current !== undefined,
                )
                onChangeScript && onChangeScript(audioScriptTemp.current.trim())
              }}
              style={{ marginRight: '1rem', backgroundColor: '#319712' }}
            >
                {t('common')['save']}
            </Button>
            <Button
              appearance={'primary'}
              type="button"
              onClick={() => {
                setShowAudioModal(false)
                audioScriptTemp.current = audioScript.current
              }}
            >
                {t('common')['cancel']}
            </Button>
          </div>
        )}
      </Modal>
    </div>
  )
}
