import { useEffect, useCallback, useState, useRef, KeyboardEvent } from 'react'

import { Button, Toggle, Tooltip, Whisper } from 'rsuite'

import { ContentFieldFB } from 'components/atoms/contentFieldFB'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionPropsType } from 'interfaces/types'
import useTranslation from '@/hooks/useTranslation'

const ID = 'sl-01'
export default function SelectFromList({
  question,
  register,
  setValue,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const [displayPopUp, setDisplayPopUp] = useState(false)
  const listCorrect = question?.correct_answers?.split('#') ?? []
  const [selectedValue, setSelectedValue] = useState(null)
  const [errExample, setErrExample] = useState(false)
  const exampleTexts =
    question?.correct_answers
      ?.split('#')
      .map((m) => (m.startsWith('*') ? m.replace('*', '') : '')) || []

  const inputRef = useRef(null)
  const tableRef = useRef<HTMLDivElement>(null)
  const exampleModesRef = useRef(
    question?.correct_answers?.split('#').map((m) => m.startsWith('*')) || [],
  )
  const inputIndexRef = useRef(-1)
  const convertListInput = useCallback((strV: string, listC: string[]) => {
    const listV: Answer[][] =
      strV?.split('#').map((m, mIndex) =>
        m.split('*').map((g) => ({
          text: g,
          isCorrect: listC[mIndex] === g,
        })),
      ) ?? []
    return listV
  }, [])

  const listInput = useRef(convertListInput(question?.answers, listCorrect))
  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  useEffect(() => {
    register('answers', {
      validate: (value: string) => {
        const errArr = (value ?? '').split('#').map((m, mIndex) =>
          m
            .split('*')
            .map((i, index) => {
              if (i === '') return index
              return null
            })
            .filter((m) => m !== null)
            .join('*'),
        )
        const errStr = errArr.join('#')
        const check = errArr.filter((item) => item.length > 0)
        if (errStr === '') return true
        return check.length > 0 ? errStr : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers,
    })
    register('question_text', {
      required: t('question-validation-msg')['question-text-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
      },
      value: question?.question_text,
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, gIndex) => {
            if (m === '') return gIndex
            return null
          })
          .filter((m) => m !== null)
        const re = errs.length > 0 ? errs.join() : true
        return re
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
    register('example_mode_sl', {
      validate: (value: string) => {
        const arrExample = (value ?? '').split('#').filter((m) => m == 'true')
        return arrExample.length > 1 ? t(t('question-validation-msg')['example-allow-total'], ['1']) : true
      },
      value:
        question?.correct_answers
          ?.split('#')
          .map((m) => m?.startsWith('*'))
          .join('#') || '',
    })
    register('question_mode_sl', {
      validate: (value: string) => {
        const arrExample = (value ?? '').split('#').filter((m) => m !== 'true')
        return arrExample.length < 1 ? t(t('question-validation-msg')['question-fb-required'], ['1']) : true
      },
      value:
        question?.correct_answers
          ?.split('#')
          .map((m) => m?.startsWith('*'))
          .join('#') || '',
    })
  }, [])

  useEffect(() => {
    if (selectedValue === null) return
    if (isInit.current) {
      isInit.current = false
    } else {
      onUpdateCorrectAnswer()
    }
  }, [selectedValue])

  useEffect(() => {
    const inputErrorMsg = document.getElementById('input-error-msg')
    if (exampleModesRef.current.filter((m: any) => m == false).length == 0) {
      inputErrorMsg.innerText = t(t('question-validation-msg')['input-required'], ['1'])
      inputErrorMsg.style.display = 'block'
    }
  }, [])

  useEffect(() => {
    const parentDiv = document.getElementById(ID)
    parentDiv.addEventListener('scroll', () => {
      setDisplayPopUp(!displayPopUp)
    })
  }, [])

  useEffect(() => {
    if (inputIndexRef.current < 0) return
    const WIDTH_SWITCH_EXAMPLE = 40
    let offsetTop = 0
    const parentDiv = document.getElementById(ID)
    const parentRect = inputRef.current.getBoundingClientRect()
    const inputNodeIndex = inputRef.current.listInput[inputIndexRef.current].id
    const inputElement = document.getElementById(inputNodeIndex)
    const inputRect = inputElement.getBoundingClientRect()
    // const offsetTop = inputRect.top - parentRect.top + inputRect.height + 10
    if (parentDiv.scrollTop !== 0) {
      offsetTop = inputElement.offsetTop - (parentDiv.scrollTop - 27)
    } else {
      offsetTop = inputElement.offsetTop + 27
    }
    // vị trí mũi tên tính từ điểm bắt đầu của input bên trái
    let offsetLeft = inputRect.left - parentRect.left + inputRect.width / 2 - 5
    let tableLeft = 5
    if (offsetLeft > 315) {
      tableLeft = offsetLeft - 310 - WIDTH_SWITCH_EXAMPLE
      offsetLeft = 310 + WIDTH_SWITCH_EXAMPLE
    } else {
      offsetLeft -= 5
    }
    tableRef.current.style.top = `${offsetTop}px`
    tableRef.current.style.left = `${tableLeft}px`
    tableRef.current.style.display = 'unset'
    tableRef.current.style.setProperty('--offset-left', `${offsetLeft}px`)
  }, [displayPopUp])

  const updateErrorMsg = (data: any[]) => {
    const exampleErrorMsg = document.getElementById('example-error-msg')
    const inputErrorMsg = document.getElementById('input-error-msg')
    if (data.filter((m: any) => m == 'true').length > 1) {
      exampleErrorMsg.innerText = `* ${t(t('question-validation-msg')['example-allow-total'], ['1'])}`
      exampleErrorMsg.style.display = 'block'
    } else {
      exampleErrorMsg.style.display = 'none'
    }
    if (data.filter((m: any) => m == 'false').length == 0) {
      inputErrorMsg.innerText = t(t('question-validation-msg')['input-required'], ['1'])
      inputErrorMsg.style.display = 'block'
    } else {
      inputErrorMsg.style.display = 'none'
    }
  }

  const onUpdateModeSL = () => {
    const data: any[] = []
    listInput.current.map((m: any, i: number) => {
      if (exampleModesRef.current[i]) {
        data.push('true')
      } else {
        data.push('false')
      }
    })
    setValue('example_mode_sl', data.join('#'))
    setValue('question_mode_sl', data.join('#'))
    setTimeout(() => {
      if (!selectedValue || exampleModesRef.current[selectedValue.index]) {
        const inputId = `${groupKey}_0`
        document.getElementById(inputId)?.focus()
      } else {
        document
          .getElementById(`${groupKey}_${selectedValue.value?.length - 1 ?? 0}`)
          ?.focus()
      }
    }, 0)
    updateErrorMsg(data)
  }

  const onChange = (data: any) => {
    setValue('question_text', data.replaceAll('##', '%s%'))
    tableRef.current.style.display = 'none'
  }

  const onBlur = (data: any) => {
    setValue('question_text', data.replaceAll('##', '%s%'))
  }

  const formatText = useCallback((answerStr: string, corrects: string) => {
    if (!corrects) return answerStr
    let result: any = answerStr
    //result = result?.replaceAll('\n', '%lb%')
    const correctList = corrects.split('#')
    for (const correct of correctList) {
      result = result?.replace('%s%', `##`)
    }
    return result
  }, [])

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }

  const onUpdateCorrectAnswer = useCallback(() => {
    setValue(
      'answers',
      listInput.current
        .map((m: any, i: number) => {
          // m.map((g: any) => g.text).join('*')
          const text = m.map((g: any) => g.text?.trim())
          return exampleModesRef.current[i] ? text[0] : text.join('*')
        })
        .join('#'),
    )
    setValue(
      'correct_answers',
      listInput.current
        .map((m: any, i: number) => {
          const textCorrect = m.find((g: any) => g.isCorrect)?.text.trim()
          const text = m.map((g: any) => g.text.trim())
          return exampleModesRef.current[i] ? `*${text[0]}` : textCorrect
        })
        .join('#'),
    )
  }, [])

  const onTabCreated = (id: string, index: number) => {
    listInput.current.splice(index, 0, [{ text: '', isCorrect: false }])
    const data: any[] = exampleModesRef.current
    data.splice(index, 0, false)
    exampleModesRef.current = [...data]
    onUpdateModeSL()
    onUpdateCorrectAnswer()
    onClosePopup()
  }

  const onTabsDeleted = (indexes: number[]) => {
    listInput.current = listInput.current.filter(
      (m, mIndex) => !indexes.includes(mIndex),
    )
    const data = exampleModesRef.current.filter(
      (m, mIndex) => !indexes.includes(mIndex),
    )
    exampleModesRef.current = [...data]
    onUpdateModeSL()
    onUpdateCorrectAnswer()
    onClosePopup()
  }

  const onTabClick = (e: React.MouseEvent<HTMLDivElement>, index: number) => {
    const input = e.target as HTMLInputElement
    setSelectedValue({ index: index, value: listInput.current[index] })
    if (exampleModesRef.current.filter((m) => m).length > 1) {
      setErrExample(true)
    } else {
      setErrExample(false)
    }
    inputIndexRef.current = index
    setDisplayPopUp(!displayPopUp)
    if (!viewMode) {
      setTimeout(() => {
        if (exampleModesRef.current[index]) {
          document.getElementById(`${groupKey}_0`)?.focus()
        } else {
          document
            .getElementById(
              `${groupKey}_${listInput.current[index]?.length - 1 ?? 0}`,
            )
            ?.focus()
        }
      }, 0)
    }
  }

  const onClosePopup = () => {
    // setSelectedValue(null)
    inputIndexRef.current = -1
    tableRef.current.style.display = null
  }

  const onToggleExample = (answerIndex: number) => {
    const listMode = [...exampleModesRef.current]
    listMode[answerIndex] = !listMode[answerIndex]
    exampleModesRef.current = listMode
    if (exampleModesRef.current.filter((m) => m).length > 1) {
      setErrExample(true)
    } else {
      setErrExample(false)
    }
    inputIndexRef.current = answerIndex
    setDisplayPopUp(!displayPopUp)
    onUpdateModeSL()
    onUpdateTextInput(
      listMode[answerIndex],
      answerIndex,
      selectedValue?.value[0]?.text,
    )
    setValue(
      'answers',
      listInput.current
        .map((m: any, i: number) => {
          const text = m.map((g: any) => g.text?.trim())
          return exampleModesRef.current[i] ? text[0] : text.join('*')
        })
        .join('#'),
    )
    setValue(
      'correct_answers',
      listInput.current
        .map((m: any, i: number) => {
          const textCorrect = m.find((g: any) => g?.isCorrect)?.text.trim()
          const text = m.map((g: any) => g.text.trim())
          return exampleModesRef.current[i] ? `*${text[0]}` : textCorrect
        })
        .join('#'),
    )
  }

  const onUpdateTextInput = (
    isExample: boolean,
    index: number,
    value: string,
  ) => {
    const inputData = inputRef.current.listInput[index]
    inputData.value = value?.replaceAll('*', '') || ''
    if (isExample) {
      inputData.className = 'input-example'
    } else {
      inputData.className = ''
      inputData.value = ''
    }
  }

  const onChangeIsCorrect = (answerIndex: number) => {
    selectedValue.value.forEach((m: any, mIndex: number) => {
      m.isCorrect = mIndex === answerIndex
    })
    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })
  }

  const onChangeAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value
    setSelectedValue({ ...selectedValue })
    setDisplayPopUp(!displayPopUp)
    if (exampleModesRef.current[selectedValue.index]) {
      onUpdateTextInput(true, selectedValue.index, e.target.value)
    } else {
      onUpdateTextInput(false, selectedValue.index, e.target.value)
    }
  }
  const onBlurAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value = e.target.value.trim()
    setSelectedValue({ ...selectedValue })
    if (exampleModesRef.current[selectedValue.index]) {
      onUpdateTextInput(true, selectedValue.index, e.target.value)
    } else {
      onUpdateTextInput(false, selectedValue.index, e.target.value)
    }
  }

  const onAddAnswer = () => {
    selectedValue.value.push({ text: '', isCorrect: false })
    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })
    setTimeout(() => {
      document
        .getElementById(
          `${groupKey}_${
            listInput.current[selectedValue.index]?.length - 1 ?? 0
          }`,
        )
        ?.focus()
    }, 0)
  }

  const onDeleteAnswerText = (answerIndex: number) => {
    if (!exampleModesRef.current[selectedValue.index]) {
      selectedValue.value.splice(answerIndex, 1)
      listInput.current[selectedValue.index] = selectedValue.value
      setSelectedValue({ ...selectedValue })
    } else {
      selectedValue.value[0].text = ''
      listInput.current[selectedValue.index] = selectedValue.value
      setSelectedValue({ ...selectedValue })
    }
    setDisplayPopUp(!displayPopUp)
    onUpdateTextInput(
      exampleModesRef.current[selectedValue.index],
      selectedValue.index,
      selectedValue?.value[0]?.text,
    )
  }
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
      return
    }
  }, [])

  const listAnswerErr = errors['answers']?.message.split('#') ?? []
  const listErr: string[] = errors['correct_answers']?.message?.split(',') ?? []

  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)
  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }

  return (
    <div className="m-select-from-list-01">
      <TextAreaField
        label={t('create-question')['question-description']}
        setValue={setValue}
        defaultValue={question?.question_description}
        register={register('question_description', {
          required: t('question-validation-msg')['description-required'],
          maxLength: {
            value: 10000,
            message: t(t('question-validation-msg')['description-max-length'], ['10000']),
          },
        })}
        className={`form-input-stretch ${
          errors['question_description'] ? 'error' : ''
        }`}
        style={{ height: '9rem' }}
        viewMode={viewMode}
        specialChar={['#', '*']}
      />
      <div className="form-input-stretch" style={{ position: 'relative' }}>
        <ContentFieldFB
          id={ID}
          ref={inputRef}
          label={t('common')['question']}
          strValue={formatText(
            question?.question_text,
            question?.correct_answers,
          )}
          onChange={onChange}
          onBlur={onBlur}
          isMultiTab={true}
          disableTabInput={true}
          onTabClick={onTabClick}
          onTabCreated={onTabCreated}
          onTabsDeleted={onTabsDeleted}
          inputStyle={{ padding: '4.5rem 1.2rem 1rem' }}
          className={
            errors['question_text'] ||
            errors['answers'] ||
            errors['correct_answers'] ||
            errors['question_mode_sl'] ||
            errors['example_mode_sl']
              ? 'error'
              : ''
          }
          viewMode={viewMode}
          exampleTexts={exampleTexts}
          specialChar={['#', '*']}
        >
          <div className="box-action-info">
            <div style={{ display: 'flex', alignItems: 'flex-end' }}>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onBold}
                style={
                  {
                    '--image': 'url(/images/icons/ic-bold.png)',
                  } as MyCustomCSS
                }
              />
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onItalic}
                style={
                  {
                    '--image': 'url(/images/icons/ic-italic.png)',
                  } as MyCustomCSS
                }
              />
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onUnderline}
                style={
                  {
                    '--image': 'url(/images/icons/ic-underline.png)',
                  } as MyCustomCSS
                }
              />
              <Whisper
                placement="top"
                trigger={'hover'}
                speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
              >
                <Button className="add-new-answer" onMouseDown={onAddSpace}>
                  <img alt="" src="/images/icons/ic-underscore.png" />
                </Button>
              </Whisper>
            </div>
            <div className="clear-outline"></div>
          </div>
        </ContentFieldFB>
        <div ref={tableRef} className="popup-table">
          {selectedValue && (
            <div>
              <table className={`question-table`}>
                <thead>
                  <tr>
                    <th align="left" dangerouslySetInnerHTML={{
                            __html: t('create-question')['select-question-example']}}>
                    </th>
                    <th
                      align="left"
                      style={
                        exampleModesRef.current[selectedValue.index]
                          ? { display: 'none' }
                          : {}
                      }
                    >
                      {t('create-question')['correct-answer']}
                    </th>
                    <th align="left">{t('create-question')['answer-key']}</th>
                    <th className="action">
                      <img
                        alt=""
                        src="/images/icons/ic-close-dark.png"
                        width={20}
                        height={20}
                        onClick={onClosePopup}
                        style={{ pointerEvents: 'all', cursor: 'pointer' }}
                      />
                    </th>
                  </tr>
                </thead>
                <tbody>
                  {selectedValue?.value &&
                    !exampleModesRef.current[selectedValue.index] &&
                    selectedValue?.value?.map(
                      (answer: Answer, answerIndex: number) => {
                        const listItemAnswerErr =
                          listAnswerErr[selectedValue.index]
                        const answerText = answer.text.replace('*', '')
                        return (
                          <tr key={`${groupKey}_${answerIndex}`}>
                            {answerIndex == 0 && (
                              <td
                                rowSpan={selectedValue?.value.length}
                                width="110px"
                              >
                                <div className={`__content-toggle`}>
                                  <Toggle
                                    size="md"
                                    className="__switcher"
                                    disabled={viewMode}
                                    checked={
                                      !exampleModesRef.current[
                                        selectedValue.index
                                      ]
                                    }
                                    checkedChildren={t("common")["question"]}
                                    unCheckedChildren={t('common')['example']}
                                    onChange={() =>
                                      onToggleExample(selectedValue.index)
                                    }
                                  />
                                </div>
                              </td>
                            )}
                            <td
                              align="center"
                              className={`td-radio ${
                                listErr?.includes(
                                  selectedValue.index.toString(),
                                )
                                  ? 'error'
                                  : ''
                              }`}
                            >
                              <input
                                type="radio"
                                name={groupKey.toString()}
                                checked={answer.isCorrect}
                                onChange={onChangeIsCorrect.bind(
                                  null,
                                  answerIndex,
                                )}
                              />
                            </td>
                            <td
                              className={`td-input ${
                                listItemAnswerErr?.includes(
                                  answerIndex.toString(),
                                )
                                  ? 'error'
                                  : ''
                              }`}
                            >
                              <input
                                id={`${groupKey}_${answerIndex}`}
                                onKeyDown={onKeyDown}
                                type="text"
                                value={answerText}
                                onBlur={onBlurAnswerText.bind(
                                  null,
                                  answerIndex,
                                )}
                                onChange={onChangeAnswerText.bind(
                                  null,
                                  answerIndex,
                                )}
                              />
                            </td>
                            <td align="center">
                              <img
                                alt=""
                                onClick={onDeleteAnswerText.bind(
                                  null,
                                  answerIndex,
                                )}
                                className="ic-action"
                                src="/images/icons/ic-trash.png"
                              />
                            </td>
                          </tr>
                        )
                      },
                    )}
                  {selectedValue?.value &&
                    exampleModesRef.current[selectedValue.index] && (
                      <tr
                        key={`${groupKey}_${0}`}
                        style={{ backgroundColor: 'rgba(61,196,125,0.15)' }}
                      >
                        <td>
                          <div
                            className={`__content-toggle ${
                              errExample ? 'toggle-error' : ''
                            }`}
                          >
                            <Toggle
                              size="md"
                              className="__switcher"
                              disabled={viewMode}
                              checked={
                                !exampleModesRef.current[selectedValue.index]
                              }
                              checkedChildren={t("common")["question"]}
                              unCheckedChildren={t('common')['example']}
                              onChange={() =>
                                onToggleExample(selectedValue.index)
                              }
                            />
                          </div>
                        </td>
                        <td style={{ display: 'none' }}></td>
                        <td
                          className={`td-input ${
                            listAnswerErr[selectedValue.index]?.includes('0')
                              ? 'error'
                              : ''
                          }`}
                        >
                          <input
                            className={`${
                              listErr[selectedValue.index]?.includes('0')
                                ? 'error'
                                : ''
                            }`}
                            id={`${groupKey}_0`}
                            onKeyDown={onKeyDown}
                            type="text"
                            value={selectedValue.value[0].text.replace('*', '')}
                            onChange={onChangeAnswerText.bind(null, 0)}
                            onBlur={onBlurAnswerText.bind(null, 0)}
                          />
                        </td>
                        <td align="center">
                          <img
                            alt=""
                            onClick={onDeleteAnswerText.bind(
                              null,
                              selectedValue.value[0],
                            )}
                            className="ic-action"
                            src="/images/icons/ic-trash.png"
                          />
                        </td>
                      </tr>
                    )}
                  {!viewMode &&
                    !exampleModesRef.current[selectedValue.index] &&
                    (!selectedValue?.value ||
                      selectedValue?.value?.length < 26) && (
                      <tr className="action">
                        <td colSpan={2} align="left">
                          <span
                            style={
                              selectedValue?.value?.length == 0
                                ? {
                                    padding: '1rem 1.5rem',
                                    borderRadius: '0.8rem',
                                    border: '1px solid #ff0018',
                                  }
                                : {}
                            }
                            className="add-new-answer"
                            onClick={onAddAnswer}
                          >
                            <img
                              alt=""
                              src="/images/icons/ic-plus-sub.png"
                              width={17}
                              height={17}
                            />{' '}
                            {t('create-question')['add-answer']}
                          </span>
                        </td>
                        <td></td>
                        <td></td>
                      </tr>
                    )}
                </tbody>
              </table>
            </div>
          )}
        </div>
      </div>
      <div className="form-error-message">
        <span id="example-error-msg" style={{ display: 'none' }}></span>
        <span id="input-error-msg" style={{ display: 'none' }}></span>
      </div>
    </div>
  )
}

type Answer = {
  text: string
  isCorrect: boolean
}
