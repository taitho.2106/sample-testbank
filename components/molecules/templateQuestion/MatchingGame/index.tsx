import { useEffect } from 'react'

import { ContentField } from 'components/atoms/contentField'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionPropsType } from "interfaces/types";

import AudioInput from '../AudioInput'
import MatchingGameTable from './table'
import browserFile from 'lib/browserFile'
import { onType } from "@/utils/string";
import useTranslation from '@/hooks/useTranslation';

export default function MatchingGame({
  question,
  register,
  setValue,
  isAudio = false,
  isReading = false,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  useEffect(() => {
    if (isAudio) {
      register('audio_file',{
        validate: {
          acceptedFormats: async (file) => {
            // check format if has file
            if (!file) {
              return true
            }
            if (file.size == 0) {
              return t('question-validation-msg')['audio-link-error']
            }
            if (!['audio/mp3', 'audio/mpeg'].includes(file?.type)) {
              return t('question-validation-msg')['audio-type']
            }
            if (!(await browserFile.checkFileExist(file))) {
              return t('question-validation-msg')['audio-link-error']
            }
            return true
          },
        }
      })
      register('audio', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['audio-max-length'],['2000']),
        },
        required: t('question-validation-msg')['audio-required'],
        value: question?.audio,
      })
      register('audio_script', {
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
        },
        value: question?.audio_script,
      })
    }
    if (isReading) {
      register('question_text', {
        required: t('question-validation-msg')['question-text-required'],
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
        },
        value: question?.question_text,
      })
    }
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '#').split('#').map((m) =>
          m
            .split('*')
            .map((g, gIndex) => {
              if (g === '') return gIndex
              return null
            })
            .filter((m) => m !== null),
        )
        return errs.some((m) => m.length > 0)
          ? errs.map((m) => m.join()).join('#')
          : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***#***',
    })
    register('correct_answers', {
      required: t('question-validation-msg')['correct-answers-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [])

  const onChangeAudioScript = (data: any) => {
    if (isAudio) {
      setValue('audio_script', data)
    }
    if (isReading) {
      setValue('question_text', data)
    }
  }

  const onChange = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers)
  }

  const onChangeFile = (files: FileList) => {
    setValue('audio_file', files[0])
    const urlAudio = URL.createObjectURL(files[0])
    setValue('audio', urlAudio)
  }
  const onTypeClick = (type : "Bold" | "Italic" | "Underline", e:any) => {
    onType({
      classElement: 'question_text',
      type
    })
  }
  return (
    <div className="m-matching-game" style={{ display: 'flex' }}>
      {isAudio && (
        <AudioInput
          url={question?.audio}
          script={question?.audio_script}
          isError={errors['audio'] || errors['audio_file']}
          onChangeFile={onChangeFile}
          onChangeScript={onChangeAudioScript}
          style={{ height: '398px' }}
          viewMode={viewMode}
        />
      )}
      {isReading && (
        <div className={"form-input-stretch"} style={{ flex: 1, display: 'flex' }}>
          <ContentField
            label={t('create-question')['reading-text']}
            strValue={question?.question_text}
            disableTab={true}
            onChange={onChangeAudioScript}
            style={{ height: 'unset', flex: 1  }}
            className={`question_text ${errors["question_text"] ? "error" : ""}`}
            specialChar={['#','*']}
            viewMode={viewMode}
          >
            <div className="box-action-info">
              <div style={{ display: 'flex', alignItems: "flex-end" }}>
                <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={(e)=>onTypeClick("Bold",e)}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                ></div>
                <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={(e)=>onTypeClick("Italic",e)}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-italic.png)',
                      } as MyCustomCSS
                    }
                ></div>
                <div
                    className="icon-mask icon-action-style-highlight"
                    onMouseDown={(e)=>onTypeClick("Underline",e)}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-underline.png)',
                      } as MyCustomCSS
                    }
                ></div>
              </div>
              <div className="clear-outline" style={{}}></div>
            </div>
          </ContentField>
        </div>
      )}
      <div style={{ flex: 2 }}>
        <TextAreaField
          label={t('create-question')['question-description']}
          setValue={setValue}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          style={{ height: '9rem' }}
          viewMode={viewMode}
          specialChar={['#','*']}
        />
        <MatchingGameTable
          className={`form-input-stretch ${
            errors['correct_answers'] ? 'error' : ''
          }`}
          answerStr={question?.answers ?? '***#***'}
          correctStr={question?.correct_answers}
          onChange={onChange}
          errorStr={errors['answers']?.message ?? ''}
          register={register}
          setValue={setValue}
          errors={errors}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}
