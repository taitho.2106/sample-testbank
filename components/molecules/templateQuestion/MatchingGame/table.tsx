import React, { useEffect, useRef, useState } from "react";

import ContentEditable from 'react-contenteditable'
import { SelectPicker } from 'rsuite'
import { MyCustomCSS } from "@/interfaces/types";
import { convertHtmlToStr, convertStrToHtml, onPaste, onType } from "@/utils/string";
import useTranslation from "@/hooks/useTranslation";

type PropType = {
  answerStr: string
  correctStr: string
  styleBold?: boolean
  className?: string
  errorStr?: string
  onChange: (data: any) => void
  register?:any
  setValue?:any
  errors?:any
  viewMode?: boolean
}

export default function MatchingGameTable({
  answerStr,
  correctStr,
  className = '',
  errorStr = '',
  onChange,
  register,
  setValue,
  errors, 
  viewMode,
}: PropType) {
  const {t} = useTranslation()
  const groupKey = useRef(Math.random()).current
  const correctList = correctStr?.split('#') ?? []
  const leftRight = answerStr?.split('#') ?? []
  const [left, setLeft] = useState<Answer[]>(
    leftRight[0]?.split('*')?.map((m, mIndex) => ({
      id: `${Math.random()}_${mIndex}`,
      text: m,
    })) ?? [],
  )
  const [right, setRight] = useState<Answer[]>(
    leftRight[1]?.split('*')?.map((m, mIndex) => ({
      id: `${Math.random()}_${mIndex}`,
      text: m,
      order: correctList.findIndex((g) => g === m) + 1,
    })) ?? [],
  )

  const isInit = useRef(true)

  useEffect(() => {
    setValue('order',right.map((item:any)=>item.order).join('#'))
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        const rigtTemp = right.filter((m) => m.order > 0)
        onChange({
          answers: `${left.map((m) => (m.text ?? '').trim()).join('*')}#${right
            .map((m) => (m.text ?? '').trim())
            .join('*')}`,
          correct_answers: rigtTemp
            .sort((a, b) => a.order - b.order)
            .map((m) => (m.text ?? '').trim())
            .join('#'),
        })
      }
    }
  }, [left, right])
  useEffect(()=>{
    register('order', {
      validate: (value: string) => {
        const lengthLeft = left.length;
        const arrLeft = Array.from(Array(lengthLeft),(item,i)=>"err");
        const err = value.split('#').map((item:any, i:number)=>{
          if(item >0 && item<=lengthLeft){
            arrLeft[parseInt(item)-1] = ""
          }
          if(i>=lengthLeft || item > 0) return true
          return false
        })
        if(arrLeft.find(m=> m!="") || err.find(m=>!m)){
          return `${arrLeft.join()}#${err.map(m=> m?"":"err").join(",")}`
        }else{
          return true
        }
      },
      value:right.map((item:any)=>item.order).join('#')
    })
  },[right])
  const onAddAnswer = (index: number) => {
    if (index === 0) {
      left.push({ id: `${Math.random()}_0`, text: '' })
      setLeft([...left])
    } else {
      right.push({ id: `${Math.random()}_0`, text: '', order: 0 })
      setRight([...right])
    }

    const posIndex = index === 0 ? left?.length : right?.length
    setTimeout(() => {
      console.log(posIndex)
      const el = document.querySelector(
        `table:nth-child(${index + 1}) tbody tr:nth-child(${
          posIndex ?? 1
        }) td input`,
      ) as HTMLInputElement
      el?.focus()
    }, 0)
  }

  const onChangeText = (index: number, answerIndex: number, event: any) => {
    const el: HTMLDivElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    if (index === 0) {
      left[answerIndex].text = text.replace(/(\*|\#)/g, '')
      setLeft([...left])
    } else {
      right[answerIndex].text = text.replace(/(\*|\#)/g, '')
      setRight([...right])
    }
  }
  const onBlurText = (index: number, answerIndex: number, event: any) => {
    const el: HTMLDivElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    if (index === 0) {
      left[answerIndex].text = text.replace(/(\*|\#)/g, '').trim()
      el.innerHTML = convertStrToHtml(left[answerIndex].text)
    } else {
      right[answerIndex].text =text.replace(/(\*|\#)/g, '').trim()
      el.innerHTML = convertStrToHtml(right[answerIndex].text)
    }
  }

  const onChangeOrder = (value: any, answerIndex: any) => {
    if (!Number.isNaN(value)) {
      // right[answerIndex].order = value
      // setRight([...right])
      // setValue('order',right.filter((m:any) => m.order))
      const rightTemp = [...right]
      const item = rightTemp.find(m => m.order === value)
      if(item){
        item.order = 0
      }
      rightTemp[answerIndex].order = value
      setRight([...rightTemp])
      setValue('order',rightTemp.filter(m=> m.order))
    }
  }

  const onDeleteAnswer = (index: number, answerIndex: number) => {
    if (index === 0) {
      left.splice(answerIndex, 1)
      const rightClone = right.map((item:any)=> {
        return {
          ...item,
          order:0,
        }
      })
      setLeft([...left])
      setRight([...rightClone])
    } else {
      right.splice(answerIndex, 1)
      setRight([...right])
    }
  }

  const listError = errorStr.split('#')
  const leftError = listError[0] ? listError[0].split(',') : []
  const rightError = listError[1] ? listError[1].split(',') : []
  const onTypeClick = (tablePosition:number ,type : "Bold" | "Italic" | "Underline") => {
    onType({
      classElement: "div-input",
      groupKey,
      type,
      tablePosition,
      action: (position, index, value) => {
        if (position === 0) {
          left[index].text = value.replace(/(\*|\#)/g, "")
          setLeft([...left])
        } else {
          right[index].text = value.replace(/(\*|\#)/g, "")
          setRight([...right])
        }
      }
    })
  }

  return (
    <div className={`box-table ${className}`} style={{ position: 'relative' }}>
      <div
        style={{
          width: 'calc(50% - 1rem)',
          marginRight: "2rem",
          border: leftError.length > 0 ? '1px solid #dd3a09' : 'none',
          borderRadius:'8px'
        }}
      >
        <table className={`question-table`}>
          <thead>
            <tr>
              <th align="left">{t('create-question')['left-column']}</th>
              <th>
                <div style={{ display: 'flex' }}>
                  <div
                      className="icon-mask icon-action-style"
                      onMouseDown={() => onTypeClick(0,"Bold")}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-bold.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                  <div
                      className="icon-mask icon-action-style"
                      onMouseDown={() => onTypeClick(0,"Italic")}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-italic.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                  <div
                      className="icon-mask icon-action-style"
                      onMouseDown={() => onTypeClick(0,"Underline")}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-underline.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                </div>
              </th>
            </tr>
          </thead>
          <tbody>
            {left?.map((answer: Answer, answerIndex: number) => (
              <tr key={answer.id}>
                <td
                  className={`input-placeholder ${
                    leftError.includes(answerIndex.toString()) ? 'error' : ''
                  }`}
                >
                  <ContentEditable
                      aria-colindex={0}
                      id={`${groupKey}_${answerIndex}`}
                      spellCheck={false}
                      className={`div-input ${groupKey}`}
                      html={convertStrToHtml(answer.text)}
                      contentEditable="true"
                      onChange={(e) => onChangeText(0, answerIndex, e)}
                      onBlur={(e) => onBlurText(0, answerIndex, e)}
                      onPaste={onPaste}
                      onKeyDown={(event)=>{
                        if (event.key === 'Enter') {
                          event.preventDefault()
                        }
                      }}
                  />
                  <span className="placeholder">{`${t('create-question')['answer']} ${
                    answerIndex + 1
                  }`}</span>
                </td>
                <td align="right">
                  <img
                    className="ic-action"
                    src="/images/icons/ic-trash.png"
                    onClick={onDeleteAnswer.bind(null, 0, answerIndex)}
                    alt=''
                  />
                </td>
              </tr>
            ))}
            {!viewMode && (
            <tr className="action">
              <td align="left" colSpan={2}>
                <label
                  className={`add-new-answer ${left.length === 0 ? 'error' : ''}`}
                  onClick={onAddAnswer.bind(null, 0)}
                >
                  <img
                    src="/images/icons/ic-plus-sub.png"
                    width={17}
                    height={17}
                    alt=''
                  />{' '}
                  {t('create-question')['add-answer']}
                </label>
              </td>
            </tr>
            )}
          </tbody>
        </table>
      </div>
      <div
      style={{
        width: 'calc(50% - 1rem)',
        border: rightError.length > 0 ? '1px solid #dd3a09' : 'none',
        borderRadius:'8px'
      }}
      >
      <table className={`question-table table-right-mg`}>
        <thead>
          <tr>
            <th align="left">
              <span>{t('create-question')['right-column']}</span>
              <div style={{ display: 'flex' }}>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={() => onTypeClick(1,"Bold")}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                ></div>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={() => onTypeClick(1,"Italic")}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-italic.png)',
                      } as MyCustomCSS
                    }
                ></div>
                <div
                    className="icon-mask icon-action-style"
                    onMouseDown={() => onTypeClick(1,"Underline")}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-underline.png)',
                      } as MyCustomCSS
                    }
                ></div>
              </div>
            </th>
            <th align="left">{t('create-question')['position']}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {right?.map((answer: Answer, answerIndex: number) => {
            const pickerData = left.map((item: Answer, index:number) => ({
              "label": `${index + 1}`,
              "value": index + 1,
            }))
            pickerData.unshift({
              "label": '-',
              "value": 0,
            })
            return (
            <tr key={answer.id}>
              <td
                className={`input-placeholder ${
                  rightError.includes(answerIndex.toString()) ? 'error' : ''
                }`}
              >
                <ContentEditable
                    aria-colindex={1}
                    id={`${groupKey}_${answerIndex}`}
                    spellCheck={false}
                    className={`div-input ${groupKey}`}
                    html={convertStrToHtml(answer.text)}
                    contentEditable="true"
                    onChange={(e) => onChangeText(1, answerIndex, e)}
                    onBlur={(e) => onBlurText(1, answerIndex, e)}
                    onPaste={onPaste}
                    onKeyDown={(event)=>{
                      if (event.key === 'Enter') {
                        event.preventDefault()
                      }
                    }}
                />
                <span className="placeholder">{`${t('create-question')['answer']} ${
                  answerIndex + 1
                }`}</span>
              </td>
              <td
                align='center'
                className={`input-placeholder ${errors['order'] ? 'error' : ''}`}
                style={{padding:0}}
              >
                <SelectPicker 
                  onChange={(value, event) => onChangeOrder(value, answerIndex)} 
                  data={pickerData} 
                  appearance="default" 
                  placeholder="-" 
                  value={answer.order}
                  searchable={false} 
                  cleanable={false} 
                  size="xs"
                />
              </td>
              <td>
                <img
                  className="ic-action"
                  src="/images/icons/ic-trash.png"
                  onClick={onDeleteAnswer.bind(null, 1, answerIndex)}
                  alt=''
                />
              </td>
            </tr>
          )})}
          {!viewMode && (
          <tr className="action">
            <td align="left" colSpan={3}>
              <label
                className={`add-new-answer ${right.length === 0 ? 'error' : ''}`}
                onClick={onAddAnswer.bind(null, 1)}
              >
                <img
                  src="/images/icons/ic-plus-sub.png"
                  width={17}
                  height={17}
                  alt=''
                />{' '}
                {t('create-question')['add-answer']}
              </label>
            </td>
          </tr>
          )}
        </tbody>
      </table>
      </div>
    </div>
  )
}

type Answer = {
  id: string
  text: string
  order?: number
}
