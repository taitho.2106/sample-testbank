import { useEffect } from 'react'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import AudioInput from '../AudioInput'
import QuestionTableDD2 from './questionTable'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation'

export default function DragAndDrop2({
  question,
  register,
  setValue,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  useEffect(() => {
    register('audio_file', {
      validate: {
        acceptedFormats: async (file) => {
          // check format if has file
          if (!file) {
            return true
          }
          if (file.size == 0) {
            return t('question-validation-msg')['audio-link-error']
          }
          if (!['audio/mp3', 'audio/mpeg'].includes(file?.type)) {
            return t('question-validation-msg')['audio-type']
          }
          if (!(await browserFile.checkFileExist(file))) {
            return t('question-validation-msg')['audio-link-error']
          }
          return true
        },
      },
    })
    register('audio', {
      maxLength: {
        value: 2000,
        message: t(t('question-validation-msg')['audio-max-length'],['2000']),
      },
      required: t('question-validation-msg')['audio-required'],
      value: question?.audio,
    })
    register('audio_script', {
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
      },
      value: question?.audio_script,
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errs: any[] = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '' || m == '*') {
              return mIndex
            }
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'],['10000']),
      },
      value: question?.correct_answers ?? '#',
    })
    register('image_files', {
      validate: async (files: any) => {
        // check format if has file
        if (!files) {
          return true
        }
        let fileErr: number[] = []
        let mIndex = -1
        for await (const file of files) {
          mIndex += 1
          if (file !== null) {
            if (!['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)) {
              fileErr.push(mIndex)
              continue
            } else if (!(await browserFile.checkFileExist(file))) {
              fileErr.push(mIndex)
              continue
            } else {
              fileErr.push(null)
              continue
            }
          }
          fileErr.push(null)
        }
        fileErr = fileErr.filter((m) => m != null)

        return fileErr.length > 0 ? fileErr.join() : true
      },
    })
    register('image', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 2000,
        message: t(t('question-validation-msg')['image-max-length'], ['2000']),
      },
      value: question?.image ?? '#',
    })
  }, [])

  const onChangeAudioScript = (data: any) => {
    setValue('audio_script', data)
  }
  const onChangeFile = (files: FileList) => {
    if (files.length > 0) {
      setValue('audio_file', files[0])
      const urlAudio = URL.createObjectURL(files[0])
      setValue('audio', urlAudio)
    }
  }
  const onCorrectAnswerChange = (data: any) => {
    setValue('correct_answers', data.correct_answers)
    setValue('image', data.image)
    setValue('image_files', data.image_files)
  }

  return (
    <div className="m-drag-and-drop-2" style={{ display: 'flex' }}>
      <AudioInput
        url={question?.audio}
        script={question?.audio_script}
        isError={errors['audio'] || errors['audio_file']}
        onChangeFile={onChangeFile}
        onChangeScript={onChangeAudioScript}
        viewMode={viewMode}
        style={{
          height: '290px',
          width: '280px',
        }}
      />
      <div style={{ flex: 3 }}>
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            label={t('create-question')['question-description']}
            setValue={setValue}
            defaultValue={question?.question_description}
            register={register('question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['description-max-length'], ['10000']),
              },
            })}
            style={{ height: '4.5rem' }}
            className={errors['question_description'] ? 'error' : ''}
            viewMode={viewMode}
            specialChar={['#', '*']}
          />
        </div>
        <QuestionTableDD2
          className={`${errors['correct_answers']?.message === '' ? 'error' : ''}`}
          errors={errors}
          setValue={setValue}
          register={register}
          question={question}
          correctErrorStr={errors['correct_answers']?.message ?? ''}
          onChange={onCorrectAnswerChange}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}
