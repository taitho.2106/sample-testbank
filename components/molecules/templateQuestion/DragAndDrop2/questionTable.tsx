import useTranslation from "@/hooks/useTranslation"
import { useState, useRef, useEffect, useCallback, KeyboardEvent } from "react"

import { Toggle } from "rsuite"

type PropType = {
    setValue?: any
    styleBold?: boolean
    className?: string
    register?:any
    correctErrorStr?: string
    onChange?: (data: any) => void
    errors?: any
    question?:any
    viewMode?:boolean
}

export default function QuestionTableDD2({
    setValue,
    className = '',
    register,
    correctErrorStr = '',
    onChange,
    errors = {},
    question,
    viewMode = false,
}: PropType) {
    const {t} = useTranslation()
    const listImage = (question?.image ?? '#').split('#') ?? []
    const listCorrect = (question?.correct_answers ?? '#').split('#') ?? []
    const [correctAnswerGroup, setCorrectAnswerGroup] = useState<AnswerGroup[]>(
        listCorrect.map((correctAns:string, caIndex:number)=>{
            const list:any[] = listCorrect
            if(list[caIndex].startsWith('*')){
                list[caIndex] = list[caIndex].replace('*','')
            }
            return ({
                key: new Date().getTime().toString() + caIndex,
                imageStr:listImage[caIndex],
                correctAnsStr:list[caIndex].replace('*',''),
                isNotExam: correctAns.startsWith('*') ? false : true,
        })})
    )
    useEffect(() => {
        register('isNotExam', {
            validate: (value: string) => {
                const errs = (value ?? '').split('#')
                const errE = errs.filter((item: string) => item === 'false')
                const errA = errs.filter((item: string) => item === 'true')
                if (errE.length > 1) return t(t('question-validation-msg')['example-allow-total'], ['1'])
                if (errA.length < 1) return t(t('question-validation-msg')['question-fb-required'], ['1'])
                return true
            }
        })  
    }, [])
   
    const answerGroupRef = useRef(correctAnswerGroup)
    const imageFiles = useRef(correctAnswerGroup.map((m) => null))

    const groupKey = useRef(Math.random()).current
    const isInit = useRef(true)
      
    useEffect(() => {
        answerGroupRef.current = correctAnswerGroup
        if(correctAnswerGroup){
            setValue('isNotExam', correctAnswerGroup.map((m) => m.isNotExam).join('#'))
        }
        if (isInit.current) {
            isInit.current = false
        } else {
            if (typeof onChange === 'function') {
                onChange({
                    correct_answers: correctAnswerGroup.map((m) => {
                        if(m.isNotExam){
                            return m.correctAnsStr
                        }else{
                            return `*${m.correctAnsStr}`
                        }
                    }).join('#'),
                    image: correctAnswerGroup.map((m)=>m.imageStr).join('#'),
                    image_files: imageFiles?.current
                })
            }
        }
        setTimeout(() => {
            Array.from(document.getElementsByClassName(`${groupKey}`)).forEach(
                (el) => {
                    const div = el as HTMLDivElement
                    div.style.width = `${div.offsetWidth}px`
                },
            )
        }, 0)
    }, [correctAnswerGroup])

    const onAddAnswer = () => {
        const newAnswer = {
            key: new Date().getTime().toString() + (correctAnswerGroup?.length ?? 0),
            imageStr: '',
            correctAnsStr: '',
            isNotExam:true,
        }
        if (correctAnswerGroup) {
            setCorrectAnswerGroup([...correctAnswerGroup, newAnswer])
        } else {
            setCorrectAnswerGroup([newAnswer])
        }
        imageFiles.current.push(null)
        setTimeout(() => {
            document.getElementById(`${groupKey}_${correctAnswerGroup?.length ?? 0}`)?.focus()
        }, 0)
    }
    const onDeleteAnswer = (index: number) => {
        const ans:any[] = correctAnswerGroup.filter((m: any, mIndex: number) => mIndex !== index)
        if(imageFiles?.current && imageFiles.current.length>index){
            imageFiles.current.splice(index,1)
        }
        setCorrectAnswerGroup([...ans])
    }
    const onChangeText = (index: number, event: any) => {
        const value = event.target.value;
        event.target.value = value
        correctAnswerGroup[index].correctAnsStr = value;
        setCorrectAnswerGroup([...correctAnswerGroup])
    }

    const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
        const key = event.key
        const keyCode = event.keyCode 
        if ( key === '!' || key === '@'|| key === '#'|| key === '$'|| key === '%'|| key === '^'|| key === '&' || key === '*' || key === '(' || key === ')' || key === '{' || key === '}'|| key === '['|| key === ']'){
            event.preventDefault()
        }
        if ( !( (keyCode >= 48 && keyCode <= 57) 
        ||(keyCode >= 65 && keyCode <= 90) 
        || (keyCode >= 97 && keyCode <= 122) 
        || keyCode === 231 ) 
        && keyCode != 8 && keyCode != 32) {
            event.preventDefault();
        }
    }, [])
    
    const onChangeImage = (index: number, files: FileList) => {
        if(files && files[0]){
            const urlImage = URL.createObjectURL(files[0])
            correctAnswerGroup[index].imageStr = urlImage
            imageFiles.current[index] = files[0]
            setCorrectAnswerGroup([...correctAnswerGroup])
            
        }else{
            correctAnswerGroup[index].imageStr = ''
            imageFiles.current[index] = null
            setCorrectAnswerGroup([...correctAnswerGroup])
        }

    }
    const listExamError = correctAnswerGroup.filter((item:any)=> item.isNotExam === false)
    const listAError = correctAnswerGroup.filter((item:any)=> item.isNotExam === true)
    const listError = correctErrorStr.split(',')
    const listImageError = (errors['image']?.message ?? '').split(',') ?? []
    const listImageFileError = (errors['image_files']?.message ?? '').split(',') ?? []
    const [err, setErr] = useState(false)
    const [error, setError] = useState('')
    const renderError = () => {
        if(listExamError.length > 1){
            setErr(true)
            return setError(`* ${t(t('question-validation-msg')['example-allow-total'], ['1'])}`)
        }
        if(listAError.length < 1){
            setErr(true)
            return setError(`* ${t(t('question-validation-msg')['question-fb-required'], ['1'])}`)
        }
        return setError('')
    }
    const handleToggle = (id:number,value:boolean) =>{
        correctAnswerGroup[id].isNotExam = !value
        setCorrectAnswerGroup([...correctAnswerGroup])
    }
    useEffect(()=>{
        renderError()
    },[listExamError,listAError])
    
    let indexE = -1;
    let indexA = -1;

    const onPaste = (event: any, answerIndex: any) => {
        event.preventDefault()
        const data = event.clipboardData.getData('text')
            .replace(/(?!\w|\s)./g, '')
            .replace(/\s+/g, ' ')
            .replace(/^(\s*)([\W\w]*)(\b\s*$)/g, '$2')
        // .replace(/(\#|\*|\~|\!|\@|\$|\%|\^|\&|\(|\)|\{|\}|\<|\>)/g, '')
        if(data.length > 0){
            correctAnswerGroup[answerIndex] = {...correctAnswerGroup[answerIndex],correctAnsStr: data.substring(0,12)}
            setCorrectAnswerGroup([...correctAnswerGroup])
        }
    }

    return (
        <div className={`question-table-dd2 ${className}`} style={{ position: 'relative' }}>
            <table className={`question-table question-table-dd2-table`}>
                <thead className={`question-table-dd2-thead`}>
                    <tr>
                        <th
                            dangerouslySetInnerHTML={{
                                __html: t('create-question')['select-question-example']
                            }}
                        ></th>
                        <th align="left">{t('create-question')['list-of-questions-and-examples']}</th>
                        <th align="center">{t('create-question')['correct-answer']}</th>
                        <th align="center"></th>
                    </tr>
                </thead>
                <tbody className={`question-table-dd2-tbody`}>
                    {correctAnswerGroup?.map((answer: AnswerGroup, answerIndex: number) => {
                        if(answer.isNotExam === true){
                            indexA +=1;
                        }else{
                            indexE +=1;
                        }
                        return (
                            <tr key={answer.key} id={answer.key}>
                                <td align="center"
                                    className={`input-placeholder ${listExamError.length > 1 || listAError.length < 1 ? 'error' : ''}`}
                                >
                                    <>
                                        <Toggle
                                            size="md"
                                            checked={answer.isNotExam}
                                            checkedChildren={t('common')['question']}
                                            unCheckedChildren={t('common')['example']}
                                            onChange={()=>handleToggle(answerIndex,answer.isNotExam)}
                                        />
                                    </>
                                </td>
                                <td
                                    className={`input-placeholder 
                                    ${(listImageError.includes(answerIndex.toString()) || 
                                        listImageFileError.includes(answerIndex.toString())) ? 'error' : ''}`}
                                >
                                    <div className="image-input-dd2">
                                        <ImageInputDD1
                                            url={answer.imageStr}
                                            onChangeFile={onChangeImage.bind(null, answerIndex)}
                                            isError={listImageError.includes(answerIndex.toString())}
                                            labelDefault={`${answer.isNotExam === true ? t(t('create-question')['question-image'], [`${indexA + 1}`]) : t(t('create-question')['image-example-with-num'], [`${indexE + 1}`])}`}
                                            viewMode={viewMode}
                                        />
                                    </div>
                                </td>
                                <td className={`input-placeholder ${listError.includes(answerIndex.toString()) || listExamError.length > 1 || listAError.length < 1 ? 'error' : ''}`}>
                                    <div style={answer.correctAnsStr?{display:"none"}:{}}
                                        dangerouslySetInnerHTML={{
                                            __html: t('create-question')['answer-max-12']
                                        }}
                                    ></div>
                                    <input
                                        onFocus={(e)=>{
                                            e.currentTarget.parentElement.querySelector("div").style.display = "none"
                                        }}
                                        onBlur = {(e)=>{
                                            if(!answer.correctAnsStr){
                                                e.currentTarget.parentElement.querySelector("div").style.display = "flex"
                                            }
                                            const value = e.target.value.trim();
                                            e.target.value = value
                                            correctAnswerGroup[answerIndex].correctAnsStr = value;
                                            setCorrectAnswerGroup([...correctAnswerGroup]) 
                                        }}
                                        id={`${groupKey}_${answerIndex}`}
                                        maxLength={12}
                                        className={`div-input ${groupKey} `}
                                        value={answer.correctAnsStr}
                                        onChange={(e) => onChangeText(answerIndex, e)}
                                        onKeyDown={onKeyDown}
                                        onPaste={(e)=> onPaste(e, answerIndex)}
                                        placeholder=""
                                    />                                    
                                </td>
                                <td align="right">
                                    <img
                                        onClick={onDeleteAnswer.bind(null, answerIndex)}
                                        className="ic-action"
                                        src="/images/icons/ic-trash.png"
                                        alt="delete"
                                    />
                                </td>
                            </tr>
                    )})}
                    {!viewMode && correctAnswerGroup.length <25 && (<tr className="question-table-dd2-action">
                        <td colSpan={1} align="center">
                            <label className={`add-new-answer-dd2 ${correctAnswerGroup.length === 0 ? 'error' : ''}`} onClick={onAddAnswer}>
                                <img
                                    src="/images/icons/ic-plus-sub.png"
                                    alt="add"
                                    width={17}
                                    height={17}
                                />{' '}
                                {t('create-question')['add-answer']}
                            </label>
                        </td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>)}
                </tbody>
            </table>
            <div className="render_error">
            {err && (
                <span>{error}</span>
            )}
            {errors['image_files']?.message && (
                <span>* {t('question-validation-msg')['list-images']}</span>
            )}
            </div>
        </div>
    );
}

type PropsType = {
    labelDefault?: string
    isError?: boolean
    url: string
    onChangeFile?: (files: FileList) => void
    viewMode?: boolean
}
function ImageInputDD1({ 
    url, 
    onChangeFile, 
    isError, 
    labelDefault, 
    viewMode 
}: PropsType) {
    const [imageUrl, setImageUrl] = useState(url)
    const fileRef = useRef(null)
    const src = imageUrl.startsWith('blob') ? url : `/upload/${url}`
    return (
        <div className={`image-dd2 ${imageUrl !== '' ? 'has-data' : ''}`}>
            <div 
                className={`image ${isError ? 'error' : ''}`}  
                onClick={() => 
                    fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
            >
                <input
                    ref={fileRef}
                    type={"file"}
                    accept=".png,.jpeg,.jpg"
                    onChange={(e) => {
                        if (fileRef.current.files.length > 0) {
                            isError = null
                            setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                            onChangeFile && onChangeFile(e.target.files)
                        }
                    }}
                />
                {src && url? (
                    <img src={src} alt='image-upload'/>
                ) : (
                    <div className="image-default">
                        <img
                            src="/images/icons/ic-image.png"
                            alt="image-default"
                        />
                        <span className="label-image">{labelDefault}</span>
                    </div>
                )}
            </div>
            {!viewMode && src && url && (
            <img
                className="image-input-dd2___isDelete"
                src="/images/icons/ic-remove-opacity.png" 
                alt=""
                onClick={(event)=>{
                    fileRef.current.value = ""
                    event.preventDefault();
                    event.stopPropagation();
                    onChangeFile(null)
                }}
            />
            )}
        </div>
    )
}

type AnswerGroup = {
    key:string,
    imageStr:string,
    correctAnsStr:string,
    isNotExam:boolean,
}