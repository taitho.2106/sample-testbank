import { useEffect, useCallback, KeyboardEvent } from 'react'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import AudioInput from '../AudioInput'
import MultiChoiceGroupQuestion from './question'
import useTranslation from "@/hooks/useTranslation";

export default function MultiChoiceGroupMC1({
  question,
  register,
  setValue,
  isAudio = false,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const { t } = useTranslation()
  useEffect(() => {
    // register('question_description', {
    //   maxLength: {
    //     value: 10000,
    //     message: 'Yêu cầu đề bài không được lớn hơn 10000 ký tự',
    //   },
    //   required: 'Yêu cầu đề bài không được để trống',
    //   value: question?.question_description,
    // })
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '').split('#').map((m) =>
          m
            .split('*')
            .map((g, gIndex) => {
              if (g === '') return gIndex
              return null
            })
            .filter((m) => m !== null),
        )
        return errs.some((m) => m.length > 0)
          ? errs.map((m) => m.join()).join('#')
          : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
    register('question_text', {
      // validate: (value: string) => {
      //   const errs = (value ?? '')
      //     .split('#')
      //     .map((m, mIndex) => {
      //       if (m === '') return mIndex
      //       return null
      //     })
      //     .filter((m) => m !== null)
      //   return errs.length > 0 ? errs.join() : false
      // },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
      },
      value: question?.question_text,
    })
  }, [])

  const onMCChange = (dataV: any) => {
    setValue('answers', dataV.answers)
    setValue('correct_answers', dataV.corrects)
    setValue('question_text', dataV.questions)
  }
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLDivElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
      return
    }
  }, [])

  return (
    <div className="m-multi-choice m-multi-choice-mc1" style={{ display: 'flex' }}> 
      <div style={{ flex: 1 }}>
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            label={t('create-question')['question-description']}
            setValue={setValue}
            register={register('question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['description-max-length'], ['10000']),
              },
            })}
            defaultValue={question?.question_description}
            style={{ height: '9rem' }}
            inputStyle={isAudio ? { paddingBottom: '4.5rem' } : {}}
            className={errors['question_description'] ? 'error' : ''}
            viewMode={viewMode}
            specialChar={['#', '*']}
          />
        </div>
        <MultiChoiceGroupQuestion
          questions={question?.question_text}
          answers={question?.answers}
          corrects={question?.correct_answers}
          onQuestionChange={onMCChange}
          errorStr={errors['answers']?.message ?? ''}
          correctErrorStr={errors['correct_answers']?.message ?? ''}
          questionErrorStr={errors['question_text']?.message ?? ''}
          // clearInputOutlineStyle={{backgroundColor: '#f5f8fc'}}
          viewMode={viewMode}
          register={register}
          setValue={setValue}
        />
      </div>
    </div>
  )
}
