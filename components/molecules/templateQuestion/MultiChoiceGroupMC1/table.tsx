import { useEffect, useRef, useState, useCallback } from 'react'

import ContentEditable from 'react-contenteditable'

import { MyCustomCSS } from 'interfaces/types'
import { convertStrToHtml, formatHtmlText, getTextWithoutFontStyle, replaceTyphoStr } from 'utils/string'
import useTranslation from "@/hooks/useTranslation";

type PropType = {
  answerStr: string
  correctStr: string
  styleBold?: boolean
  className?: string
  errorStr?: string
  onChange: (data: any) => void
  viewMode?: boolean
}

export default function MultiChoiceTable({
  answerStr,
  correctStr,
  styleBold = false,
  className = '',
  errorStr = '',
  onChange,
  viewMode,
}: PropType) {
  const { t } = useTranslation()
  const [answers, setAnswers] = useState<Answers[]>(
    answerStr?.split('*')?.map((m: string) => {
      const textAnswer = getTextWithoutFontStyle(m)
      return {
        text: m,
        isCorrect: (styleBold ? textAnswer : m) === correctStr,
      }
    }),
  )

  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  useEffect(() => {
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        onChange({
          answers: answers.map((m) => m.text.trim()).join('*'),
          correct_answers: getTextWithoutFontStyle(answers
            .find((m) => m.isCorrect)
            ?.text.trim()),
        })
      }
    }

    setTimeout(() => {
      Array.from(document.getElementsByClassName(`${groupKey}`)).forEach(
        (el) => {
          const div = el as HTMLDivElement
          div.style.width = `${div.offsetWidth}px`
        },
      )
    }, 0)
  }, [answers])

  const onAddAnswer = () => {
    if (answers) {
      setAnswers([...answers, { text: '', isCorrect: false }])
    } else {
      setAnswers([{ text: '', isCorrect: false }])
    }

    setTimeout(() => {
      document.getElementById(`${groupKey}_${answers?.length ?? 0}`)?.focus()
    }, 0)
  }

  const onChangeIsCorrect = (index: number) => {
    answers.forEach((answer: Answers, answerIndex: number) => {
      answer.isCorrect = index === answerIndex
    })
    setAnswers([...answers])
  }

  const onChangeText = (index: number, event: any) => {
    const el: HTMLElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    answers[index].text = text.replace(/(\*|\#)/g, '')
    setAnswers([...answers])
  }
  const onBlurText = (index: number, event: any) => {
    const el: HTMLElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      text += convertHtmlToStr([childNode])
    }
    answers[index].text = text.replace(/(\*|\#)/g, '').trim()
    el.innerHTML = convertStrToHtml(answers[index].text)
    // setAnswers([...answers])
  }

  const onDeleteAnswer = (index: number) => {
    answers.splice(index, 1)
    setAnswers([...answers])
  }
  const convertHtmlToStr = useCallback((dataD: ChildNode[]) => {
    let str = ''
    for (let i = 0, len = dataD.length; i < len; i++) {
      const child = dataD[i] as Node
      if (child.nodeName === '#text') {
        str += child.textContent
      } else if (child.nodeName === 'SPAN') {
        str += child.firstChild.textContent
      } else if (child.nodeName === 'BR') {
        str += ''
      } else if (child.nodeName === 'INPUT') {
        const inputEl = child as HTMLInputElement
        str += `#${inputEl.value}#`
      } else {
        const el = child as Element
        const tag = child.nodeName.toLowerCase()
        const content = convertHtmlToStr(Array.from(el.childNodes))
        if (content !== '') {
          str += `%${tag}%${content}%${tag}%`
        }
      }
    }
    return str
  }, [])
  
  const onBold = () => {
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    const range = selection.getRangeAt(0)
    const parent = range.commonAncestorContainer
    let el: HTMLElement = null
    let parentEl =
      parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
    while (parentEl.parentElement) {
      if (parentEl.classList.contains('div-input')) {
        el = parentEl
        break
      }
      parentEl = parentEl.parentElement
    }
    document.execCommand('Bold', false, null)
    if (el) {
      const index = parseInt(el.id.replace(`${groupKey}_`, ''))
      let text = ''
      for (let i = 0, len = el.childNodes.length; i < len; i++) {
        const childNode = el.childNodes[i]
        text += convertHtmlToStr([childNode])
      }
      answers[index].text = text.replace(/(\*|\#)/g, '')
      setAnswers([...answers])
    }
  }
  const onItalic = () => {
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    const range = selection.getRangeAt(0)
    const parent = range.commonAncestorContainer
    let el: HTMLElement = null
    let parentEl =
      parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
    while (parentEl.parentElement) {
      if (parentEl.classList.contains('div-input')) {
        el = parentEl
        break
      }
      parentEl = parentEl.parentElement
    }
    document.execCommand('Italic', false, null)
    if (el) {
      const index = parseInt(el.id.replace(`${groupKey}_`, ''))
      let text = ''
      for (let i = 0, len = el.childNodes.length; i < len; i++) {
        const childNode = el.childNodes[i]
        text += convertHtmlToStr([childNode])
      }
      answers[index].text = text.replace(/(\*|\#)/g, '')
      setAnswers([...answers])
    }
  }
  const onUnderline = () => {
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    const range = selection.getRangeAt(0)
    const parent = range.commonAncestorContainer
    let el: HTMLElement = null
    let parentEl =
      parent.nodeType === 3 ? parent.parentElement : (parent as HTMLElement)
    while (parentEl.parentElement) {
      if (parentEl.classList.contains('div-input')) {
        el = parentEl
        break
      }
      parentEl = parentEl.parentElement
    }
    document.execCommand('Underline', false, null)
    if (el) {
      const index = parseInt(el.id.replace(`${groupKey}_`, ''))
      let text = ''
      for (let i = 0, len = el.childNodes.length; i < len; i++) {
        const childNode = el.childNodes[i]
        text += convertHtmlToStr([childNode])
      }
      answers[index].text = text.replace(/(\*|\#)/g, '')
      setAnswers([...answers])
    }
  }
  const listErrors = errorStr.split(',')
  const onPaste = (event: any) => {
    event.preventDefault()
    let data = event.clipboardData.getData('text').replace(/(\#|\*)/g, '')
    data = data?.replace(/\r?\n|\r/g, '')
    const selection = window.getSelection()
    const range = selection.getRangeAt(0)
    range.deleteContents()
    const node = document.createTextNode(data)
    range.insertNode(node)
    selection.removeAllRanges()
    const newRange = document.createRange()
    newRange.setStart(node, data.length)
    selection.addRange(newRange)
  }
  return (
    <div className={className} style={{ position: 'relative' }}>
      <table className={`question-table`}>
        <thead>
          <tr>
            <th>{t('create-question')['correct-answer']}</th>
            <th>{t('create-question')['answer-key']}</th>
            <th align="right">
              <div style={{ display: 'flex' }}>
                {styleBold && (
                  <div
                    className="icon-mask icon-action-style"
                    onMouseDown={onBold}
                    style={
                      {
                        '--image': 'url(/images/icons/ic-bold.png)',
                      } as MyCustomCSS
                    }
                  ></div>
                )}
                <div
                  className="icon-mask icon-action-style"
                  onMouseDown={onItalic}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-italic.png)',
                    } as MyCustomCSS
                  }
                ></div>
                <div
                  className="icon-mask icon-action-style"
                  onMouseDown={onUnderline}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-underline.png)',
                    } as MyCustomCSS
                  }
                ></div>
              </div>
            </th>
          </tr>
        </thead>
        <tbody>
          {answers?.map((answer: Answers, answerIndex: number) => {
            return [
              <tr key={`${groupKey}_${answerIndex}`}>
                <td align="center" className="option">
                  <input
                    type="radio"
                    name={groupKey.toString()}
                    checked={answer.isCorrect}
                    onChange={onChangeIsCorrect.bind(null, answerIndex)}
                  />
                </td>
                <td
                  className={`input-placeholder ${
                    listErrors.includes(answerIndex.toString()) ? 'error' : ''
                  }`}
                >
                  <ContentEditable
                    id={`${groupKey}_${answerIndex}`}
                    spellCheck={false}
                    className={`div-input ${groupKey}`}
                    html={convertStrToHtml(answer.text)}
                    contentEditable="true"
                    onChange={onChangeText.bind(null, answerIndex)}
                    onBlur={onBlurText.bind(null, answerIndex)}
                    onPaste={onPaste}
                    onKeyDown={(event)=>{
                      if (event.key === 'Enter') {
                        event.preventDefault()
                      }
                    }}
                  />
                  <span className="placeholder">{`${t('create-question')['answer']} ${
                    answerIndex + 1
                  }`}</span>
                </td>
                <td align="right">
                  <img
                    onClick={onDeleteAnswer.bind(null, answerIndex)}
                    className="ic-action"
                    src="/images/icons/ic-trash.png"
                    alt='delete-answer-icon'
                  />
                </td>
              </tr>,
            ]
          })}
          {answers.length < 26 &&
          !viewMode && (
            <tr className="action">
              <td></td>
              <td colSpan={2} align="left">
                <label className="add-new-answer" style={answers.length==0?{padding:"1rem 1.5rem", borderRadius:"0.8rem", border:"1px solid #dd3a09"}:{}} 
                onClick={onAddAnswer}>
                  <img
                    src="/images/icons/ic-plus-sub.png"
                    width={17}
                    height={17}
                    alt="add-new-answer-icon"
                  />{' '}
                  {t('create-question')['add-answer']}
                </label>
              </td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}

type Answers = {
  text: string
  isCorrect: boolean
}
