import { useEffect } from 'react'

import { InputField } from 'components/atoms/inputField'
import { QuestionPropsType } from 'interfaces/types'

import MultiChoiceTable from './table'
import useTranslation from "@/hooks/useTranslation";

export default function MultiChoice4({
  question,
  register,
  setValue,
  errors = {},
  viewMode,
}: QuestionPropsType) {
  console.log("MultiChoice4---------")
  const { t } = useTranslation()
  useEffect(() => {
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('*')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***',
    })
    register('correct_answers', {
      required: t('question-validation-msg')['canvas-answers'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [])

  const updateFormData = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers)
  }

  return (
    <div className="m-multi-choice">
      <div className="box-wrapper">
        <InputField
          label={t('create-question')['question-description']}
          type="text"
          setValue={setValue}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          defaultValue={question?.question_description}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          preventKey={['#','*']}
        />
        <MultiChoiceTable
          className={`form-input-stretch ${
            errors['correct_answers'] ? 'error' : ''
          }`}
          answerStr={question?.answers ?? '***'}
          correctStr={question?.correct_answers}
          onChange={updateFormData}
          errorStr={errors['answers']?.message ?? ''}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}
