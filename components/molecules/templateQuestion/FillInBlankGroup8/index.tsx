import React, {
  useEffect,
  useState,
  useRef,
  useCallback,
  CSSProperties,
  KeyboardEvent,
} from 'react'

import { Button, Toggle, Tooltip, Whisper } from 'rsuite'

import { ContentFieldFB } from 'components/atoms/contentFieldFB'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionPropsType } from 'interfaces/types'
import browserFile from 'lib/browserFile'
import Guid from 'utils/guid'
import useTranslation from '@/hooks/useTranslation'

const ID = 'fb-group-8'
export default function FillInBlankGroup8({
  question,
  register,
  setValue,
  isImage = false,
  viewMode = false,
  errors = {},
}: QuestionPropsType) {
  const {t} = useTranslation()
  const [displayPopUp, setDisplayPopUp] = useState(false)
  const [errExample, setErrExample] = useState(false)
  const arrImage = question?.image?.split('*') ?? []
  const image_group = arrImage[1]?.split('#') || []

  const [imgUrl, setImgUrl] = useState(arrImage[0] || '')

  const inputRef = useRef(null)
  const exampleModesRef = useRef(
    question?.correct_answers?.split('#').map((m) => m.startsWith('*')) || [],
  )
  const tableRef = useRef<HTMLDivElement>(null)
  const inputIndexRef = useRef(-1)
  const [selectedValue, setSelectedValue] = useState(null)

  const lengthImageGroup1 = question?.correct_answers?.split('#').length || 0
  const arrImageGroup1 = image_group?.slice(0, lengthImageGroup1) || []
  const arrImageGroup2 = image_group?.slice(lengthImageGroup1) || []
  const exampleTexts =
    question?.correct_answers
      ?.split('#')
      .map((m) => (m.startsWith('*') ? m.replace('*', '') : '')) || []

  const convertListInput = (strV: string) => {
    const listV: Answer[][] =
      strV?.split('#').map((m, i) => {
        const imageArr = arrImageGroup1[i]?.split('|') || []
        return m.split('%/%').map((g, j) => ({
          text: g.replaceAll('*', ''),
          imageStr: imageArr[j] ?? '',
          listFile: null,
        }))
      }) ?? []
    return listV
  }

  const listInput = useRef(convertListInput(question?.correct_answers))
  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  const temp = question?.correct_answers?.split('#') ?? []

  const dataTableArr =
    temp?.map((item: any, mIndex: number) => {
      const imageArr = arrImageGroup1[mIndex]?.split('|') ?? []
      const itemData = item?.split('%/%').map((m: string, i: number) => {
        return {
          text: m?.replaceAll('*', '') || '',
          imageStr: imageArr[i] || '',
          isE: m?.startsWith('*') ? false : true,
        }
      })

      return itemData
    }) ?? []

  const [dataTable, setDataTable] = useState<data[][]>([...dataTableArr] ?? [])

  useEffect(() => {
    if (isImage) {
      //image_group0 là image chính của FB8
      //image_group1 là những image của câu hỏi
      //image_group2 là những image của gợi ý

      register('image_file_group0', {
        validate: {
          acceptedFormats: async(file) => {
            // check format if has file
            if (!file) {
              return true
            }
            if(file.size ==0){
              return t('question-validation-msg')['image-deleted']
            }
            if(!['image/jpeg', 'image/png'].includes(file?.type)){
              return t('question-validation-msg')['image-file']
            }
            if(!await browserFile.checkFileExist(file)){
              return t('question-validation-msg')['image-deleted']
            }
            return true;
          },
        },
      })
      register('image_group0', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['image-max-length'], ['2000']),
        },
        required: t('question-validation-msg')['image-required'],
        value: question?.image?.split('*')[0],
      })
      register('image_file_group1', {
        validate: async(files: FileList[]) => {
          if (!files) {
            return true
          }
          const arrFiles = files.length
          const err = Array.from(Array(arrFiles), () => [])
          const errArr = Array.from(Array(arrFiles), () => '')
          let i =-1;
          let isFileDeleted = false;
          for await (const file of Array.from(files)){
              i+=1;
              let mIndex = -1;
              for await (const item of Array.from(file)){
                  mIndex+=1;
                  if(!item) continue;
                  if (item.size == 0) {
                      err[i].push(mIndex.toString())
                    } else if (!['image/jpeg', 'image/png'].includes(item?.type)) {
                        err[i].push(mIndex.toString())
                      } else {
                          if(!await browserFile.checkFileExist(item)){
                              isFileDeleted = true;
                              err[i].push(mIndex.toString())
                          }else{
                              err[i].push(null)
                          }
                    }
              }
              errArr[i] = err[i].filter((m) => m != null).join('*')
          }
          const temp = errArr.filter((m: string) => m !== '')
          const errStr = errArr.join('#')
          if (errStr === '') return true
          return temp.length > 0 ? `${errStr}${isFileDeleted?"#deleted":""}` : true
        },
      })
      register('image_group1', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['image-max-length'], ['2000']),
        },
        validate: (value: string) => {
          const errArr = (value ?? '').split('#').map((m, mIndex) =>
            m
              .split('|')
              .map((i, index) => {
                if (i === '') return index
                return null
              })
              .filter((m) => m !== null)
              .join('*'),
          )

          const errStr = errArr.join('#')
          const check = errArr.filter((item) => item.length > 0)
          if (errStr === '') return true
          return check.length > 0 ? errStr : true
        },
        value:
          question?.image
            ?.split('*')[1]
            ?.split('#')
            ?.slice(0, lengthImageGroup1) ?? [],
      })
    }
    register('question_text', {
      required: t('question-validation-msg')['question-text-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
    },
      value: question?.question_text,
    })
    register('example_mode_fb', {
      validate: (value: string) => {
        const arrExample = (value ?? '').split('#').filter((m:string) => m === 'false')
        return arrExample.length > 1 ? t(t('question-validation-msg')['example-allow-total'], ['1']) : true
      },
      value:
      question?.correct_answers
      ?.split('#')
      .map((m: string) => {
        const arr:string[] = []
        if(m.startsWith('*')){
          arr.push("false")
      }else{
        arr.push("true")
      }
      return arr})
      .join('#') || '',
    })
    register('question_mode_fb', {
      validate: (value: string) => {
        const arrQuestion = (value ?? '').split('#').filter((m:string) => m === 'true')
        return arrQuestion.length < 1 ? 'Vui lòng có ít nhất 1 câu hỏi' : true
      },
      value:
        question?.correct_answers
          ?.split('#')
          .map((m: string) => {
            const arr:string[] = []
            if(m.startsWith('*')){
              arr.push("false")
          }else{
            arr.push("true")
          }
          return arr})
          .join('#') || '',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errArr = (value ?? '').split('#').map((m, mIndex) =>
          m
            .replaceAll('*', '')
            .split('%/%')
            .map((i, index) => {
              if (i === '') return index
              return null
            })
            .filter((m) => m !== null)
            .join('*'),
        )

        const errStr = errArr.join('#')
        const check = errArr.filter((item) => item.length > 0)
        if (errStr === '') return true
        return check.length > 0 ? errStr : true
      },
      maxLength: {
        value: 10000,
        message: 'Đáp án không được lớn hơn 10000 ký tự',
      },
      value: question?.correct_answers,
    })
  }, [])

  useEffect(() => {
    if (selectedValue === null) return
    if (isInit.current) {
      isInit.current = false
    } else {
      onUpdateCorrectAnswer()
    }
  }, [selectedValue])

  useEffect(() => {
    const inputErrorMsg = document.getElementById('input-error-msg')
    if (exampleModesRef.current.filter((m: any) => m == false).length == 0) {
      inputErrorMsg.innerText = t(t('question-validation-msg')['input-required'],['1'])
      inputErrorMsg.style.display = 'block'
    }
  }, [])

  const updateErrorMsg = (data: any[]) => {
    const exampleErrorMsg = document.getElementById('example-error-msg')
    const inputErrorMsg = document.getElementById('input-error-msg')
    if (data.filter((m: any) => m == 'true').length > 1) {
      exampleErrorMsg.innerText = `* ${t(t('question-validation-msg')['example-allow-total'], ['1'])}`
      exampleErrorMsg.style.display = 'block'
    } else {
      exampleErrorMsg.style.display = 'none'
    }
    if (data.filter((m: any) => m == 'false').length == 0) {
      inputErrorMsg.innerText = t(t('question-validation-msg')['input-required'],['1'])
      inputErrorMsg.style.display = 'block'
    } else {
      inputErrorMsg.style.display = 'none'
    }
  }

  const onUpdateModeFB = () => {
    const data: any[] = []
    listInput.current.map((m: any, i: number) => {
      if (exampleModesRef.current[i]) {
        data.push('true')
      } else {
        data.push('false')
      }
    })
    setValue('example_mode_fb', data.join('#'))
    setValue('question_mode_fb', data.join('#'))

    setTimeout(() => {
      document
        .getElementById(
          `${groupKey}_${
            !exampleModesRef.current[selectedValue?.index]
              ? selectedValue?.value?.length - 1 ?? 0
              : 0
          }`,
        )
        ?.focus()
    }, 0)
    updateErrorMsg(data)
  }

  useEffect(() => {
    const parentDiv = document.getElementById(ID)
    parentDiv.addEventListener('scroll', () => {
      setDisplayPopUp(!displayPopUp)
    })
  }, [])

  useEffect(() => {
    if (inputIndexRef.current < 0) return
    let offsetTop = 0
    const parentDiv = document.getElementById(ID)
    const parentRect = inputRef.current.getBoundingClientRect()
    const inputNodeIndex = inputRef.current.listInput[inputIndexRef.current].id
    const inputElement = document.getElementById(inputNodeIndex)
    const inputRect = inputElement.getBoundingClientRect()
    // let offsetTop = inputRect.top - parentRect.top + inputRect.height + 66
    if (parentDiv.scrollTop !== 0) {
      offsetTop = inputElement.offsetTop - (parentDiv.scrollTop - 84)
    } else {
      offsetTop = inputElement.offsetTop + 84
    }
    let offsetLeft = inputRect.left - parentRect.left + inputRect.width / 2 - 5
    let tableLeft = 5
    if (offsetLeft > 415) {
      tableLeft = offsetLeft - 310 - 138
      offsetLeft = 310 + 138
    } else {
      offsetLeft -= 5
    }
    tableRef.current.style.top = `${offsetTop}px`
    tableRef.current.style.left = `${tableLeft}px`
    tableRef.current.style.display = 'unset'
    tableRef.current.style.setProperty('--offset-left', `${offsetLeft}px`)
  }, [displayPopUp])

  const onUpdateCorrectAnswer = () => {
    setValue(
      'correct_answers',
      listInput.current
        .map((m: any, i: number) => {
          const text = m
            ?.map((g: any) => g.text.trim())
            ?.join('%/%')
            .replaceAll('*', '')
          return exampleModesRef.current[i] ? `*${text?.split('%/%')[0]}` : text
        })
        .join('#'),
    )
  }

  const onChange = (data: any) => {
    setValue('question_text', data.replaceAll('##', '%s%'))
    tableRef.current.style.display = 'none'
  }

  const onBlur = (data: any) => {
    setValue('question_text', data.replaceAll('##', '%s%'))
  }

  const formatText = useCallback((answerStr: string, corrects: string) => {
    if (!corrects) return answerStr
    let result: any = answerStr
    const correctList = corrects.split('#')
    for (let i = 0; i < correctList.length; i++) {
      result = result?.replace('%s%', `##`)
    }
    return result
  }, [])

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }

  const onTabCreated = (id: string, index: number) => {
    listInput.current.splice(index, 0, [
      { text: '', imageStr: '', listFile: null },
    ])
    const data: any[] = exampleModesRef.current
    data.splice(index, 0, false)
    exampleModesRef.current = [...data]
    if (listInput.current) {
      const dataTemp = listInput.current.map((item: any, indexItem: number) => {
        return item.map((m: any) => {
          const e: boolean = !exampleModesRef.current[indexItem] ?? true
          return { ...m, isE: e }
        })
      })
      setDataTable(dataTemp)
    }
    onUpdateModeFB()
    onUpdateCorrectAnswer()
  }

  const onTabsDeleted = (indexes: number[]) => {
    listInput.current = listInput.current.filter(
      (m, mIndex) => !indexes.includes(mIndex),
    )
    const data = exampleModesRef.current.filter(
      (m, mIndex) => !indexes.includes(mIndex),
    )
    exampleModesRef.current = [...data]
    onUpdateModeFB()
    onUpdateCorrectAnswer()
    onClosePopup()
    setDataTable(
      [...dataTable].filter((item, index) => !indexes.includes(index)),
    )
  }

  const onTabClick = (e: React.MouseEvent<HTMLDivElement>, index: number) => {
    const input = e.target as HTMLInputElement

    setSelectedValue({ index: index, value: listInput.current[index] })
    if (exampleModesRef.current.filter((m) => m).length > 1) {
      setErrExample(true)
    } else {
      setErrExample(false)
    }
    inputIndexRef.current = index
    setDisplayPopUp(!displayPopUp)

    if (!viewMode) {
      setTimeout(() => {
        setTimeout(() => {
          document
            .getElementById(
              `${groupKey}_${
                !exampleModesRef.current[index]
                  ? listInput.current[index]?.length - 1 ?? 0
                  : 0
              }`,
            )
            ?.focus()
        }, 0)
      }, 0)
    }
  }

  const onClosePopup = () => {
    // setSelectedValue(null)
    inputIndexRef.current = -1
    tableRef.current.style.display = null
  }
  const onUpdateTextInput = (
    isExample: boolean,
    index: number,
    value: string,
  ) => {
    const inputData = inputRef.current.listInput[index]
    inputData.value = value?.replaceAll('*', '') || ''
    if (isExample) {
      inputData.className = 'input-example'
    } else {
      inputData.className = ''
      inputData.value = ''
    }
  }
  const onChangeAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value
    setSelectedValue({ ...selectedValue })
    setDisplayPopUp(!displayPopUp)
    if (exampleModesRef.current[selectedValue.index]) {
      onUpdateTextInput(true, selectedValue.index, e.target.value)
    } else {
      onUpdateTextInput(false, selectedValue.index, e.target.value)
    }
  }
  const onBlurAnswerText = (answerIndex: number, e: any) => {
    selectedValue.value[answerIndex].text = e.target.value = e.target.value.trim()
    setSelectedValue({ ...selectedValue })
    if (exampleModesRef.current[selectedValue.index]) {
      onUpdateTextInput(true, selectedValue.index, e.target.value)
    } else {
      onUpdateTextInput(false, selectedValue.index, e.target.value)
    }
  }
  const onToggleExample = (answerIndex: number) => {
    const listMode = [...exampleModesRef.current]
    listMode[answerIndex] = !listMode[answerIndex]
    exampleModesRef.current = listMode
    inputIndexRef.current = answerIndex
    setDisplayPopUp(!displayPopUp)
    onUpdateModeFB()
    onUpdateTextInput(
      listMode[answerIndex],
      answerIndex,
      selectedValue?.value[0]?.text,
    )
    setValue(
      'correct_answers',
      listInput.current
        .map((m: any, i: number) => {
          const text = m
            ?.map((g: any) => g?.text)
            ?.join('%/%')
            .replaceAll('*', '')
          return listMode[i] ? `*${text?.split('%/%')[0]}` : text
        })
        .join('#'),
    )
  }
  const onAddAnswer = () => {
    selectedValue.value.push({ text: '', imageStr: '' })

    listInput.current[selectedValue.index] = selectedValue.value
    setSelectedValue({ ...selectedValue })

    setTimeout(() => {
      document
        .getElementById(`${groupKey}_${selectedValue?.value?.length - 1 ?? 0}`)
        ?.focus()
    }, 0)
  }

  const onDeleteAnswerText = (answerIndex: number) => {
    if (!exampleModesRef.current[selectedValue.index]) {
      selectedValue.value.splice(answerIndex, 1)
      listInput.current[selectedValue.index] = selectedValue.value
      setSelectedValue({ ...selectedValue })
    } else {
      selectedValue.value[0].text = ''
      listInput.current[selectedValue.index] = selectedValue.value
      setSelectedValue({ ...selectedValue })
    }
    setDisplayPopUp(!displayPopUp)
    onUpdateTextInput(
      exampleModesRef.current[selectedValue.index],
      selectedValue.index,
      selectedValue?.value[0]?.text,
    )
  }

  const onChangeImageFile = (files: FileList) => {
    if (files && files[0]) {
      setValue('image_file_group0', files[0])
      const urlImage = URL.createObjectURL(files[0])
      setValue('image_group0', urlImage)
      setImgUrl(urlImage)
    } else {
      setValue('image_file_group0', null)
      setValue('image_group0', '')
      setImgUrl('')
    }
  }

  const onChangeImageQuestion = (
    index: number,
    index2: number,
    files: FileList,
  ) => {
    if (files && files[0]) {
      const urlImage = URL.createObjectURL(files[0])
      listInput.current[index][index2].imageStr = urlImage
      listInput.current[index][index2].listFile = files[0]
      setSelectedValue({ index: index, value: [...listInput.current[index]] })
    } else {
      listInput.current[index][index2].imageStr = ''
      listInput.current[index][index2].listFile = null
      setSelectedValue({ index: index, value: [...listInput.current[index]] })
    }
  }

  useEffect(() => {
    if (typeof onChange === 'function') {
      if (selectedValue) {
        const data = [...selectedValue.value].map((item: any) => {
          const e: boolean =
            !exampleModesRef.current[selectedValue?.index] ?? true
          return { ...item, isE: e }
        })

        dataTable[selectedValue?.index] = !exampleModesRef.current[
          selectedValue?.index
        ]
          ? data
          : [{ ...data[0] }]
        setDataTable([...dataTable])
      }
    }
  }, [selectedValue, exampleModesRef.current])

  useEffect(() => {
    if (dataTable) {
      const imageGroup1 = dataTable
        .map((item: any[]) => item?.map((m) => m.imageStr).join('|'))
        .join('#')
      setValue('image_group1', imageGroup1)
      const listImageGroup1 = listInput.current.map(
        (item: Answer[], index: number) => {
          return exampleModesRef.current[index]
            ? [item[0].listFile]
            : item.map((m) => m.listFile)
        },
      )
      const dataIsEx = dataTable.map((item: any[]) => item?.some((m) => m.isE))
      setValue('example_mode_fb', dataIsEx.join('#'))
      setValue('question_mode_fb', dataIsEx.join('#'))
      setValue('image_file_group1', listImageGroup1)
    }
  }, [dataTable])

  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)
  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLDivElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
      return
    }
  }, [])
// console.debug("errors--fb8",errors)
  const listErr = errors['correct_answers']?.message?.split('#') ?? []
  const listErrImage = errors['image_group1']?.message?.split('#') ?? []
  const listErrImageFile =
    errors['image_file_group1']?.message?.split('#') ?? []
  return (
    <div className="m-fill-in-blank-8">
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          flex: 'none',
          width: '40%',
        }}
      >
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <div className="text_suggestion_image">
            {/* <ContentField
              // className={`form-input-stretch `}
              label="Chú thích hình ảnh"
              strValue={question?.audio_script}
              disableTab={true}
              onChange={onChangeAudioScript}
              inputStyle={{
                height: '4rem',
                width: '100%',
                padding: '1rem 1.2rem',
              }}
              viewMode={viewMode}
            /> */}
            <TextAreaField
              setValue={setValue}
              label={t('create-question')['image-caption']}
              defaultValue={question?.audio_script}
              register={register('audio_script', {
                maxLength: {
                  value: 100,
                  message: t(t('question-validation-msg')['image-caption-length'],['100']),
                },
              })}
              style={{ height: '4rem', marginLeft: '0', width: '100%' }}
              className={`form-input-stretch ${
                errors['audio_script'] ? 'error' : ''
              }`}
              viewMode={viewMode}
              id={Guid.newGuid()}
              specialChar={['#', '*']}
            />
          </div>

          <div>
            <ImageInputFB8
              url={imgUrl}
              onChangeFile={onChangeImageFile}
              isError={
                errors['image_group0']?.message ||
                errors['image_file_group0']?.message
              }
              viewMode={viewMode}
              title={t('create-question')["image"]}
            />
          </div>
          <>
            <TableSuggestions
              viewMode={viewMode}
              data={dataTable.filter((item) => item !== undefined)}
              suggestionData={question?.answers}
              imageStr={arrImageGroup2.join('#')}
              register={register}
              setValue={setValue}
              errors={errors}
              imageGroup0={imgUrl ?? ''}
              imageGroup1={
                listInput.current
                  ?.map((item: any[], indexI: number) => {
                    if (exampleModesRef.current[indexI]) {
                      return item[0]?.imageStr
                    }
                    return item.map((m) => m.imageStr).join('|')
                  })
                  .join('#') ?? ''
              }
            />
          </>
          {(errors['image_file_group0']?.message ||
            errors['image_file_group1']?.message ||
            errors['image_file_group2']?.message)?
            errors['image_file_group0']?.type != "acceptedFormats"?
            (
            <div style={{ padding: '1rem 2rem 0 2rem' }}>
              <span
                style={{
                  color: '#ff0018',
                  fontSize: '1.2rem',
                  lineHeight: '1.2',
                }}
              >
                * {t('question-validation-msg')['list-images']}
              </span>
            </div>
          ):"":""}
        </div>
      </div>
      <div style={{ display: 'flex', flex: 'none', width: '60%' }}>
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            setValue={setValue}
            label={t('create-question')['question-description']}
            defaultValue={question?.question_description}
            register={register('question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['description-max-length'], ['10000']),
              },
            })}
            style={{ height: '4rem', marginLeft: '0', width: '100%' }}
            className={`form-input-stretch ${
              errors['question_description'] ? 'error' : ''
            }`}
            viewMode={viewMode}
            id={Guid.newGuid()}
            specialChar={['#', '*']}
          />
          <ContentFieldFB
            id={ID}
            ref={inputRef}
            label={t('common')['question']}
            strValue={formatText(
              question?.question_text,
              question?.correct_answers,
            )}
            onChange={onChange}
            onBlur={onBlur}
            isMultiTab={true}
            disableTabInput={true}
            onTabClick={onTabClick}
            onTabCreated={onTabCreated}
            onTabsDeleted={onTabsDeleted}
            inputStyle={{ padding: '4.5rem 1.2rem 1rem 1.2rem' }}
            className={
              errors['question_text'] ||
              errors['correct_answers'] ||
              errors['question_mode_fb'] ||
              errors['example_mode_fb'] ||
              errors['image_file_group1'] ||
              errors['image_group1']
                ? 'error'
                : ''
            }
            style={{ height: '348px' }}
            viewMode={viewMode}
            exampleTexts={exampleTexts}
            specialChar={['#', '*']}
          >
            <div className="box-action-info">
              <div style={{ display: 'flex', alignItems: 'flex-end' }}>
                <Whisper
                  placement="top"
                  trigger={'hover'}
                  speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
                >
                  <Button className="add-new-answer" onMouseDown={onAddSpace}>
                    <img alt="" src="/images/icons/ic-underscore.png" />
                  </Button>
                </Whisper>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onBold}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-bold.png)',
                    } as MyCustomCSS
                  }
                ></div>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onItalic}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-italic.png)',
                    } as MyCustomCSS
                  }
                ></div>
                <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onUnderline}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-underline.png)',
                    } as MyCustomCSS
                  }
                ></div>
              </div>

              <div className="clear-outline"></div>
            </div>
          </ContentFieldFB>
          <div ref={tableRef} className="popup-table">
            {selectedValue && (
              <div>
                <table className={`question-table`}>
                  <thead>
                    <tr>
                      <th align="left" dangerouslySetInnerHTML={{
                            __html: t('create-question')['select-question-example']}}>
                      </th>
                      <th align="left">{t('create-question')['correct-answer']}</th>
                      <th></th>
                      <th className="action" style={{ width: '60px' }}>
                        <img
                          alt=""
                          src="/images/icons/ic-close-dark.png"
                          width={20}
                          height={20}
                          onClick={onClosePopup}
                          style={{ pointerEvents: 'all', cursor: 'pointer' }}
                        />
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    {selectedValue?.value &&
                      !exampleModesRef.current[selectedValue.index] &&
                      selectedValue?.value?.map(
                        (answer: Answer, answerIndex: number) => {
                          const listItemErr = listErr[selectedValue.index]
                          const listImageItemErr =
                            listErrImage[selectedValue.index]
                          const listImageFileItemErr =
                            listErrImageFile[selectedValue.index]
                          return (
                            <tr
                              key={`${groupKey}_${selectedValue.index}_${
                                answer.imageStr || answerIndex
                              }`}
                            >
                              {answerIndex == 0 && (
                                <td
                                  rowSpan={selectedValue?.value.length}
                                  width="110px"
                                >
                                  <div className={`__content-toggle`}>
                                    <Toggle
                                      size="md"
                                      className="__switcher"
                                      disabled={viewMode}
                                      checked={
                                        !exampleModesRef.current[
                                          selectedValue.index
                                        ]
                                      }
                                      checkedChildren={t("common")["question"]}
                                      unCheckedChildren={t('common')['example']}
                                      onChange={() =>
                                        onToggleExample(selectedValue.index)
                                      }
                                    />
                                  </div>
                                </td>
                              )}
                              <td>
                                <>
                                  <ImageInputFB8
                                    isError={
                                      listImageItemErr?.includes(
                                        answerIndex.toString(),
                                      ) ||
                                      listImageFileItemErr?.includes(
                                        answerIndex.toString(),
                                      )
                                    }
                                    url={answer.imageStr}
                                    className="image-question"
                                    onChangeFile={onChangeImageQuestion.bind(
                                      null,
                                      selectedValue.index,
                                      answerIndex,
                                    )}
                                    viewMode={viewMode}
                                  />
                                </>
                              </td>
                              <td>
                                <input
                                  className={`${
                                    listItemErr?.includes(
                                      answerIndex.toString(),
                                    )
                                      ? 'error'
                                      : ''
                                  }`}
                                  id={`${groupKey}_${answerIndex}`}
                                  onKeyDown={onKeyDown}
                                  type="text"
                                  placeholder={`${t('create-question')['answer']} ${answerIndex + 1}`}
                                  value={answer.text.replace('*', '')}
                                  onChange={onChangeAnswerText.bind(
                                    null,
                                    answerIndex,
                                  )}
                                  onBlur={onBlurAnswerText.bind(
                                    null,
                                    answerIndex,
                                  )}
                                />
                              </td>
                              <td align="center">
                                <img
                                  alt=""
                                  onClick={onDeleteAnswerText.bind(
                                    null,
                                    answerIndex,
                                  )}
                                  className="ic-action"
                                  src="/images/icons/ic-trash.png"
                                />
                              </td>
                            </tr>
                          )
                        },
                      )}
                    {selectedValue?.value && exampleModesRef.current[selectedValue.index] && (
                      <tr
                        key={`${groupKey}_${selectedValue.index}`}
                        id={`${groupKey}_${selectedValue.index}`}
                        style={{ backgroundColor: 'rgba(61,196,125,0.15)' }}
                      >
                        <td>
                          <div
                            className={`__content-toggle ${
                              errExample ? 'toggle-error' : ''
                            }`}
                          >
                            <Toggle
                              size="md"
                              className="__switcher"
                              disabled={viewMode}
                              checked={!exampleModesRef.current[selectedValue.index]}
                              checkedChildren={t("common")["question"]}
                              unCheckedChildren={t('common')['example']}
                              onChange={() =>
                                onToggleExample(selectedValue.index)
                              }
                            />
                          </div>
                        </td>
                        <td>
                          <>
                            <ImageInputFB8
                            isError={ listErrImage[selectedValue.index]?.includes('0') ||
                            listErrImageFile[selectedValue.index]?.includes('0')}
                              url={selectedValue.value[0].imageStr}
                              className="image-question"
                              onChangeFile={onChangeImageQuestion.bind(
                                null,
                                selectedValue.index,
                                0,
                              )}
                              viewMode={viewMode}
                            />
                          </>
                        </td>
                        <td
                        >
                          <input
                          className={`${
                            listErr[selectedValue.index]?.includes('0')
                              ? 'error'
                              : ''
                          }`}
                            id={`${groupKey}_${0}`}
                            onKeyDown={onKeyDown}
                            placeholder={t('create-question')['answer']}
                            type="text"
                            value={selectedValue.value[0].text.replace('*', '')}
                            onChange={onChangeAnswerText.bind(null, 0)}
                            onBlur={onBlurAnswerText.bind(null, 0)}
                          />
                        </td>
                        <td align="center">
                          <img
                            alt=""
                            onClick={onDeleteAnswerText.bind(
                              null,
                              selectedValue.value[0],
                            )}
                            className="ic-action"
                            src="/images/icons/ic-trash.png"
                          />
                        </td>
                      </tr>
                    )}
                    {!viewMode &&
                      !exampleModesRef.current[selectedValue.index] &&
                      (!selectedValue?.value ||
                        selectedValue?.value?.length < 26) && (
                        <tr className="action">
                          <td></td>
                          <td align="left">
                            <span
                              className="add-new-answer"
                              onClick={onAddAnswer}
                            >
                              <img
                                alt=""
                                src="/images/icons/ic-plus-sub.png"
                                width={17}
                                height={17}
                              />{' '}
                              {t('create-question')['add-answer']}
                            </span>
                          </td>
                          <td></td>
                          <td></td>
                        </tr>
                      )}
                  </tbody>
                </table>
              </div>
            )}
          </div>
          <div className="form-error-message">
            <span id="example-error-msg" style={{ display: 'none' }}></span>
            <span id="input-error-msg" style={{ display: 'none' }}></span>
          </div>
        </div>
      </div>
    </div>
  )
}
type PropsType = {
  labelDefault?: string
  isError?: any
  url: string
  onChangeFile?: (file: FileList) => void
  viewMode?: boolean
  style?: CSSProperties
  className?: string
  isIcon?: boolean
  title?: string
}
function ImageInputFB8({
  url,
  onChangeFile,
  isError,
  viewMode = false,
  style,
  className = '',
  isIcon = true,
  title,
}: PropsType) {
  const [imageUrl, setImageUrl] = useState(url || '')
  const fileRef = useRef(null)
  const src = imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`

  return (
    <div
      className={`box-image-input-fb8 ${className} ${
        imageUrl !== '' ? 'has-data' : ''
      } ${isError ? 'error' : ''}`}
      style={style}
    >
      <div className="image-section">
        <div className="image-input-origin">
          {src && url ? (
            <>
              <img
                className="image-upload"
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                src={src}
                alt="image-upload"
                style={{ cursor: 'pointer' }}
              />
              {!viewMode && src && url && isIcon && (
                <img
                  className="image-delete-icon"
                  src="/images/icons/ic-remove-opacity.png"
                  alt=""
                  onClick={() => {
                    fileRef.current.value = ''
                    event.preventDefault()
                    event.stopPropagation()
                    onChangeFile(null)
                  }}
                />
              )}
            </>
          ) : (
            <>
              <div
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  cursor: 'pointer',
                }}
              >
                <img
                  className="icon"
                  src="/images/icons/ic-image.png"
                  alt="delete-img-icon"
                />
                {title || ''}
              </div>
            </>
          )}
          <input
            ref={fileRef}
            type="file"
            style={{ display: 'none' }}
            accept=".png,.jpeg,.jpg"
            onChange={(e) => {
              if (fileRef.current.files.length > 0) {
                setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                onChangeFile && onChangeFile(e.target.files)
              }
            }}
          />
        </div>
      </div>
    </div>
  )
}
type PropsTypeSG = {
  data?: data[][]
  viewMode?: boolean
  style?: CSSProperties
  className?: string
  suggestionData?: string
  imageStr?: string
  register?: any
  setValue?: any
  errors?: any
  imageGroup0?: string
  imageGroup1?: string
}
function TableSuggestions({
  viewMode,
  className = '',
  style,
  data = [],
  suggestionData,
  imageStr = '',
  register,
  setValue,
  errors,
  imageGroup0,
  imageGroup1,
}: PropsTypeSG) {
  const {t} = useTranslation()
  const swapArr = (arr: any[]) => {
    const arrTemp: any[] = []
    for (const i in arr) {
      if (!arr[i]?.isE) {
        arrTemp.splice(0, 0, arr[i])
      } else {
        arrTemp.push(arr[i])
      }
    }
    return arrTemp
  }
  const arrData = swapArr(data?.reduce((a, b) => [...a, ...b], []) ?? [])
  const lengthData = [...arrData].length
  const arrImage = imageStr.split('#')
  const dataDefault =
    suggestionData?.length > 0
      ? (suggestionData?.split('#')?.map((item: string, mIndex: number) => {
          return {
            imageStr: arrImage[mIndex],
            suggestionStr: item,
            fileImage: null,
            key: Guid.newGuid(),
          }
        }) as Suggestion[])
      : []
  const [suggestion, setSuggestion] = useState(dataDefault)

  useEffect(() => {
    if (suggestion.length > 0) {
      register('answers', {
        validate: (value: string) => {
          const err = (value ?? '')
            .split('#')
            .map((item: string, index: number) => {
              if (item === '') return index
              return null
            })
            .filter((m) => m !== null)
          return err.length > 0 ? err.join('#') : true
        },
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['suggestion-max-length'], ['10000']),
        },
        value: suggestionData,
      })
      register('image_group2', {
        validate: (value: string) => {
          const err = (value ?? '')
            .split('#')
            .map((item: string, index: number) => {
              if (item === '') return index
              return null
            })
            .filter((m) => m !== null)
          return err.length > 0 ? err.join('#') : true
        },
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
        },
        value: imageStr,
      })
      register('image_file_group2', {
        validate: async (files: any) => {
          if (!files) {
            return true
          }
          let fileErr: number[] = []
          let mIndex = -1
          for await (const file of files) {
            mIndex += 1
            if (file !== null) {
              if (
                !['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)
              ) {
                fileErr.push(mIndex)
                continue
              } else if (!(await browserFile.checkFileExist(file))) {
                fileErr.push(mIndex)
                continue
              } else {
                fileErr.push(null)
                continue
              }
            }
            fileErr.push(null)
          }
          fileErr = fileErr.filter((m) => m != null)
          return fileErr.length > 0 ? fileErr.join('#') : true
        },
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
        },
      })
    } else {
      register('answers', {
        validate: () => {
          return true
        },
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['suggestion-max-length'], ['10000']),
        },
        value: suggestionData,
      })
      register('image_group2', {
        validate: () => {
          return true
        },
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
        },
        value: imageStr,
      })
      register('image_file_group2', {
        validate: () => {
          return true
        },
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
        },
      })
    }
  }, [suggestion])

  const onAddSuggestion = () => {
    const newSuggest: Suggestion = {
      imageStr: '',
      suggestionStr: '',
      fileImage: null,
      key: Guid.newGuid(),
    }
    if (suggestion) {
      setSuggestion([...suggestion, newSuggest])
    } else {
      setSuggestion([newSuggest])
    }

    setTimeout(() => {
      document
        .getElementById(`input-suggestion-${suggestion?.length ?? 0}`)
        ?.focus()
    }, 0)
  }

  const onDeleteSuggestion = (index: number) => {
    const sug: Suggestion[] = suggestion.filter(
      (m: Suggestion, sIndex: number) => sIndex !== index,
    )
    setSuggestion(sug)
  }

  const onChangeText = (index: number, e: any) => {
    suggestion[index].suggestionStr = e.target.value
    setSuggestion([...suggestion])
  }
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLDivElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
      return
    }
  }, [])
  // console.log("suggestion---",suggestion);
  useEffect(() => {
    if (suggestion.length > 0) {
      setValue(
        'answers',
        suggestion.map((item) => item.suggestionStr).join('#'),
      )
      setValue(
        'image_group2',
        suggestion.map((item) => item.imageStr).join('#'),
      )
      setValue(
        'image_file_group2',
        suggestion.map((item) => item.fileImage),
      )
      setValue(
        'image',
        `${imageGroup0}*${imageGroup1}#${suggestion
          ?.map((item) => item.imageStr)
          .join('#')}`,
      )
    } else {
      setValue('image_group2', '')
      setValue('image_file_group2', null)
      setValue('answers', null)
      setValue('image', `${imageGroup0}*${imageGroup1}`)
    }
  }, [suggestion, data])
  const onChangeFileSuggestion = (index: number, files: FileList) => {
    if (files && files[0]) {
      const urlImage = URL.createObjectURL(files[0])
      suggestion[index].imageStr = urlImage
      suggestion[index].fileImage = files[0]
      setSuggestion([...suggestion])
    } else {
      suggestion[index].imageStr = ''
      suggestion[index].fileImage = null
      setSuggestion([...suggestion])
    }
  }
  const listInputErr = errors['answers']?.message?.split('#') ?? []

  return (
    <div className={`table-suggestion-fb8 ${className}`} style={style}>
      <table className={`question-table`}>
        <thead>
          <tr>
            <th align="left" style={{ width: '12rem' }}>
              {t('create-question')['image']}
            </th>
            <th align="left">{t('create-question')['suggestions']}</th>
            <th
              style={{
                width: '50px',
              }}
            ></th>
          </tr>
        </thead>
        <tbody>
          {arrData.map((item: any, mIndex: number) => {
            return (
              item && (
                <tr
                  key={Guid.newGuid()}
                  className={`${item.isE ? '' : 'example_suggestion'}`}
                >
                  <td>
                    <>
                      <ImageInputFB8
                        url={item.imageStr ?? ''}
                        isIcon={false}
                        className="image-suggestion"
                        style={{
                          pointerEvents: 'none',
                        }}
                      />
                    </>
                  </td>
                  <td>
                    <input
                      style={{pointerEvents:"all"}}
                      title={item.text || ''}
                      type="text"
                      defaultValue={item.text || `${t('question-update-container')['suggestion']} ${mIndex + 1}`}
                      className="input-suggestion"
                      readOnly={true}
                    />
                  </td>
                  <td></td>
                </tr>
              )
            )
          })}
          {suggestion &&
            suggestion.map((item: Suggestion, sIndex: number) => {
              return (
                <tr key={item.key}>
                  <td>
                    <>
                      <ImageInputFB8
                        viewMode={viewMode}
                        url={item.imageStr}
                        className="image-suggestion"
                        onChangeFile={onChangeFileSuggestion.bind(null, sIndex)}
                        isError={
                          errors['image_group2']?.message
                            ?.split('#')
                            ?.includes(sIndex.toString()) ||
                          errors['image_file_group2']?.message
                            ?.split('#')
                            ?.includes(sIndex.toString())
                        }
                      />
                    </>
                  </td>
                  <td>
                    <input
                      className={`input-suggestion ${
                        listInputErr?.includes(sIndex.toString()) ? 'error' : ''
                      }`}
                      id={`input-suggestion-${sIndex}`}
                      onKeyDown={onKeyDown}
                      type="text"
                      // placeholder={`Gợi ý ${sIndex + data.length + 1}`}
                      placeholder={`${t('question-update-container')['suggestion']} ${sIndex + lengthData + 1}`}
                      defaultValue={item.suggestionStr}
                      onChange={onChangeText.bind(null, sIndex)}
                      style={{pointerEvents:"all"}}
                      readOnly={viewMode}
                    />
                  </td>
                  <td align="center">
                    <img
                      alt="icon-deleted"
                      onClick={onDeleteSuggestion.bind(null, sIndex)}
                      className="ic-action-suggestion"
                      src="/images/icons/ic-trash.png"
                      height={15}
                      width={14}
                    />
                  </td>
                </tr>
              )
            })}
          {!viewMode && suggestion && suggestion.length < 26 && (
            <tr className="action">
              <td align="left">
                <span className="add-new-suggestion" onClick={onAddSuggestion}>
                  <img
                    alt=""
                    src="/images/icons/ic-plus-sub.png"
                    width={17}
                    height={17}
                  />{' '}
                  {t('create-question')['add-suggestions']}
                </span>
              </td>
              <td></td>
              <td></td>
            </tr>
          )}
        </tbody>
      </table>
    </div>
  )
}
type Answer = {
  imageStr: string
  text: string
  listFile: any
}
type Suggestion = {
  imageStr: string
  fileImage: any
  suggestionStr: string
  key: string
}
type data = {
  text: string
  imageStr: string
  isE: boolean
  listFile: any
}
