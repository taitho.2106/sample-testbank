import React, { useState, useRef, ChangeEvent } from 'react' 
import { Toggle } from 'rsuite'

type TImagePropsType = {
    labelDefault?: string,
    isError?: boolean,
    url: string,
    onChangeFile?: (files: FileList) => void,
    viewMode?: boolean
}
function ImageInputMG6( {url, onChangeFile, isError, labelDefault, viewMode}: TImagePropsType ){
    const [ _path, setPath ] = useState<string>(url)
    const _package = useRef(null)
    const src = _path.startsWith('blob') ? url : `${location.origin}/upload/${url}`

    const handleOnClick = () => _package.current.dispatchEvent(new MouseEvent('click'))
    const handleOnChange = (event: ChangeEvent<HTMLInputElement>) => {
        if( _package.current.files.length > 0 ){
            isError = null
            setPath( URL.createObjectURL(_package.current.files[0]) )
            onChangeFile && onChangeFile(event.target.files)
        }
    }
    const handleOnClickDelete = (event: any) => {
        _package.current.value = ''
        event.preventDefault()
        event.stopPropagation()
        onChangeFile(null)
    }

    return<>
        <div className={`image-mg6 ${_path !== '' ? 'has-data' : ''}`}>
            <div className={`image ${isError ? 'error' : ''}`}
                onClick={handleOnClick}>
                <input ref={_package} type={"file"} accept='.jpeg,.jpg,.png' onChange={handleOnChange} />
                { src && url 
                    ? (<img src={src} alt='image-upload' />) 
                    : ( <div className='image-default'>
                            <img src='/images/icons/ic-image.png' alt='image-default'/>
                        </div> )}
                { ! viewMode && src && url 
                    && <img className='image-input-mg6___isDelete' 
                    src='/images/icons/ic-remove-opacity.png' alt='' onClick={handleOnClickDelete}/> }
            </div>
            <div className='label-image'>
                <span className='__text' >{labelDefault}</span>
            </div>
        </div>
    </> 
}

export { ImageInputMG6 }