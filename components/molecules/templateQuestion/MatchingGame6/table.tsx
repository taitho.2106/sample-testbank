import { useEffect, useRef, useState } from 'react'

import { SelectPicker, Toggle } from 'rsuite'

import { ImageInputMG6 } from './input'
import useTranslation from '@/hooks/useTranslation'

type PropType = {
  answerString: string
  correctString: string
  styleBold?: boolean
  className?: string
  errorString?: string
  onChange: (data: any) => void
  register?:any
  setValue?:any
  errors?:any
  viewMode?: boolean
}

type Answer = {
  id: string
  image: string
  order?: number,
  file?: File,
  marked?: boolean
}

function MatchingGameTable6( 
  { answerString, 
    correctString, 
    className = '', 
    errorString = '', 
    onChange, 
    register, 
    setValue, 
    errors,
    viewMode = false, 
  }: PropType) {
  const {t} = useTranslation()
  const correctArray = correctString?.split('#') ?? []
  const answerArray = answerString?.split('#') ?? []
  const leftArray = answerArray[0]?.split('*') ?? []
  const rightArray = answerArray[1]?.split('*') ?? []
  const exampleArray = answerArray[2]?.split('*') ?? []
  const [left, setLeft] = useState<Answer[]>(
    leftArray.map((value: string, index: number) => {
      const indexExample = exampleArray.findIndex(item => item.includes('ex'))
      const _order = correctArray.indexOf(rightArray[indexExample])
      return ({
        id: `${Math.random()}_${index}`,
        image: value,
        marked: _order == index ? false : true
      })}) ?? [],
  )
  const [right, setRight] = useState<Answer[]>(
    rightArray.map((value: string, index: number) => {
      const _order = correctArray.includes(value) 
        ?  correctArray.indexOf(value) + 1 : 0
      return {
        id: `${Math.random()}_${index}`,
        image: value,
        order: _order,
        marked: exampleArray[index]?.includes('ex') ? false : true 
    }}) ?? [],
  )
  const [maximumExample, setMaximumExample] = useState<string>('')
  const [minimumQuestion, setMinimumQuestion] = useState<string>('')
  const isInit = useRef(true)
  useEffect(() => {
    setValue( 'order', right.map((item:any) => item.order).join('#'))
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        const leftString = left.map((m) => m.image).join('*')
        const rightString = right.map((m) => m.image).join('*')
        const exampleString = left.map((item) => !item.marked ? 'ex' : '').join("*")
        onChange({
          answers:`${leftString}#${rightString}#${exampleString}`,
            correct_answers: right.filter((m) => m.order > 0)
            .sort((a, b) => a.order - b.order)
            .map((m) => m.image)
            .join('#'),
          mg6_images_left: left.map((m) => m.file ?? null),
          mg6_images_right: right.map((m) => m.file ?? null)
        })
      }
    }
    
    if( left.filter((item) => !item.marked).length > 1 ){
      setMaximumExample(t(t('question-validation-msg')['example-allow-total'], ['1']))
      setValue('exampleRule', false)
    }else{
      setMaximumExample('')
      setValue('exampleRule', true)
    }
    
    if( left.filter((item) => !item.marked).length == left.length ){
      setMinimumQuestion(t(t('question-validation-msg')['question-fb-required'], ['1']))
      setValue('questionRule', false)
    }else{
      setMinimumQuestion('')
      setValue('questionRule', true)
    }
  }, [left, right])

  useEffect(()=>{
    register('order', {
      // validate: (value: string) => {
      //   const err = value.split('#').map((item:any)=>{
      //     if(item > 0) 
      //       return true
      //     return null
      //   }).filter((item:any)=> item != null)
      //   return err.length == left.length ? true : false
      // },
      validate: (value: string) => {
        const lengthLeft = left.length;
        const arrLeft = Array.from(Array(lengthLeft),(item,i)=>"err");
        const err = value.split('#').map((item:any, i:number)=>{
          if(item >0 && item<=lengthLeft){
            arrLeft[parseInt(item)-1] = ""
          }
          if(i>=lengthLeft || item > 0) return true
          return false
        })
        if(arrLeft.find(m=> m!="") || err.find(m=>!m)){
          return `${arrLeft.join()}#${err.map(m=> m?"":"err").join(",")}`
        }else{
          return true
        }
      },
      value:right.map((item:any)=>item.order).join('#')
    }),
    register('exampleRule', {
      validate: (value: string) => {
        return value
      }
    }),
    register('questionRule', {
      validate: (value: string) => {
        return value
      }
    })
  },[right])

  const onAddAnswer = (index: number) => {
    if (index === 0) {
      left.push({ id: `${Math.random()}_0`, image: '', file: null, marked: true })
      setLeft([...left])
    } else {
      right.push({ id: `${Math.random()}_0`, image: '', order: 0, file: null, marked: true })
      setRight([...right])
    }

    const posIndex = index === 0 ? left?.length : right?.length
    setTimeout(() => {
      const el = document.querySelector(
        `table:nth-child(${index + 1}) tbody tr:nth-child(${
          posIndex ?? 1
        }) td input`,
      ) as HTMLInputElement
      el?.focus()
    }, 0)
  }

  const onDeleteAnswer = (index: number, answerIndex: number) => {
    
    if( index === 0 ){
      const _right = right.map((item: Answer) => {
        const __marked = ! left[answerIndex].marked ? true : item.marked
        const __order = item.order > left.length - 1 ? 0 : item.order
        return { ...item, marked: __marked, order: __order }
      })
      left.splice(answerIndex, 1)
      setLeft([...left])
      setRight([..._right])
    }else{
      const _left = left.map((item: Answer) => {
        const __marked = ! right[answerIndex].marked ? true : item.marked
        return { ...item, marked: __marked }
      })
      right.splice(answerIndex, 1)
      setLeft([..._left]) 
      setRight([...right])
    }
  }

  const onChangeImageLeft = (index: number, files: FileList) => {
    if( files && files[0] ){
      const path = URL.createObjectURL(files[0])
      left[index].image = path
      left[index].file = files[0]
      setLeft([...left])
    }else{
      left[index].image = ''
      left[index].file = null
      setLeft([...left])
    }
  }

  const onChangeImageRight = (index: number, files: FileList) => {
    if( files && files[0] ){
      const path = URL.createObjectURL(files[0])
      right[index].image = path
      right[index].file = files[0]
      setRight([...right])
    }else{
      right[index].image = ''
      right[index].file = null
      setRight([...right])
    }
  }

  const handleOnChangeToggle = (index: number) => {
    left[index].marked = left[index].marked ? false : true
    setLeft([...left])
    const _index = right.findIndex( (item) => item.order === index + 1 )
    if( _index < 0 || ! right[_index] ) return
    right[_index].marked = ! right[_index].marked
    setRight([...right])    
  }

  const handleOnChangePickerData = () => {
    const dataOrder = left.map(
      (item: Answer, index: number) => ({ label: `${index + 1}`, value: index + 1}))
    dataOrder.unshift({label: `_`, value: 0})
    return dataOrder
  }

  const handleOnChangePickOrder = (index: number, item: Answer) => {
    const beforeIndex = right.findIndex((value) => value.order === index)
    if( index > 0 && beforeIndex > -1 ){
      right[beforeIndex].order = item.order
      right[beforeIndex].marked = left[item.order - 1]?.marked ?? true
    }
    item.order = index
    item.marked = left[index - 1]?.marked ?? true
    setRight([...right])
    setValue( 'order', right.filter((item: Answer) => item.order))
  }

  const errorArray = errorString.split('#')
  const leftError = errorArray[0] ? errorArray[0].split(',') : []
  const rightError = errorArray[1] ? errorArray[1].split(',') : []

  const errorGroupMess = []
  if( maximumExample ) errorGroupMess.push( maximumExample )
  if( minimumQuestion ) errorGroupMess.push( minimumQuestion )
  const listImageLeftErr = errors['mg6_images_left']?.message?.split(",")||[]
  const listImageRightErr = errors['mg6_images_right']?.message?.split(",")||[]
  const listOrderErr = errors['order']?.message?.split("#")||[]
  const listOrderLeftErr = listOrderErr[0]?.split(",")||[]
  if( listImageLeftErr.length >0|| listImageRightErr.length>0) 
    errorGroupMess.push(t('question-validation-msg')['list-images'])
  let exampleIndexs = -1
  let questionIndexs = -1
  return (
    <div className={`box-table ${className}`}>
      <div className='left-table'
        style={{ border: leftError.length > 0 || left.length > right.length ? '1px solid #dd3a09' : 'none'}}>
        <table className={`question-table`}>
          <thead>
            <tr>
              <th align="left">{t('create-question')['select-question-example-1']}</th>
              <th align="left">{t('create-question')['question-example-image-2']}</th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {left?.map((answer: Answer, answerIndex: number) => {
              if( ! answer.marked ){ exampleIndexs++ }
              else{ questionIndexs++}
              const flaggedNotExist = 
                right.find((item) => item.order == answerIndex + 1) ? false : true
              const trErr = listOrderLeftErr[answerIndex]?true:false
              return(
                <tr className={ `mode${answer.marked ? '' : '-example'} ${trErr?"error":""}`} key={answer.id}>
                  <td>
                    <Toggle
                        size="md"
                        checked={answer.marked}
                        checkedChildren={t('common')['question']}
                        unCheckedChildren={t('common')['example']}
                        onChange={() => handleOnChangeToggle( answerIndex )}
                        className={`__switcher ${!answer.marked && left.filter((item) => !item.marked).length>1 ?"toggle-error":""}`}
                    />
                  </td>
                  <td className={`input-placeholder 
                    ${leftError.includes(`${answerIndex}`) 
                      || listImageLeftErr.findIndex((m:string)=> m == answerIndex+"") !=-1
                      ? 'error' : '' }`}>
                    <div className='wrapper-mg6'>
                      <div className='image-input-mg6'>
                        <ImageInputMG6 
                          url={answer.image} 
                          labelDefault={
                            answer.marked 
                            ? t(t('create-question')['question-image'], [`${questionIndexs + 1}`]) 
                            : t(t('create-question')['image-example-with-num'], [`${exampleIndexs + 1}`])}
                          onChangeFile={onChangeImageLeft.bind(null, answerIndex)}
                          viewMode={viewMode}/>
                      </div>
                    </div>
                  </td>
                  <td>
                    <img
                      className="ic-action"
                      alt=''
                      src="/images/icons/ic-trash.png"
                      onClick={onDeleteAnswer.bind(null, 0, answerIndex)}/>
                  </td>
                </tr>)})}
            { left.length <26 && (
              <tr className="action">
              <td align="left" colSpan={3}>
                <label
                  className={`add-new-answer ${left.length === 0 ? 'error' : ''}`}
                  onClick={onAddAnswer.bind(null, 0)}
                >
                  <img
                  alt=''
                    src="/images/icons/ic-plus-sub.png"
                    width={17}
                    height={17}
                  />{' '}
                  {t('create-question')['add-question']}
                </label>
              </td>
            </tr>
            )
            }
          </tbody>
        </table>
      </div>
      <div className='right-table'
      style={{ border: rightError.length > 0 ? '1px solid #dd3a09' : 'none' }}>
      <table className={`question-table`}>
        <thead>
          <tr>
            <th align="left">{t('create-question')['question-example-image-2']}</th>
            <th align="left">{t('create-question')['position']}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {right?.map((answer: Answer, index: number) => {
            return (
            <tr className={ `mode${answer.marked ? '' : '-example'}`} key={answer.id}>
              <td className={`input-placeholder ${
                  rightError.includes(`${index}`) 
                  || listImageRightErr.findIndex((m:string)=> m == index+"") !=-1 ? 'error' : ''}`}>
                <div className='image-input-mg6'>
                  <ImageInputMG6 
                    url={answer.image} 
                    labelDefault={t(t('create-question')['answer-image'], [`${index + 1}`])}
                    onChangeFile={onChangeImageRight.bind(null, index)}
                    viewMode={viewMode}/>
                </div>
              </td>
              <td
                align='center'
                className={`input-placeholder ${errors['order'] 
                  && answer.order == 0 
                  && index < left.length ? 'error' : ''}`}
                style={{padding:0}}>
                <SelectPicker 
                  className={ answer.order == 0 ? 'order_default' : ''}
                  onChange={(value) => handleOnChangePickOrder(value, answer)} 
                  data={handleOnChangePickerData()} 
                  appearance="default" 
                  placeholder="_" 
                  value={answer.order}
                  searchable={false} 
                  cleanable={false} 
                  size="xs"
                />
              </td>
              <td>
                <img
                alt=''
                  className="ic-action"
                  src="/images/icons/ic-trash.png"
                  onClick={onDeleteAnswer.bind(null, 1, index)}
                />
              </td>
            </tr>
          )})}
          {right.length < 26 && (
            <tr className="action">
            <td align="left" colSpan={3}>
              <label
                className={`add-new-answer ${right.length === 0 ? 'error' : ''}`}
                onClick={onAddAnswer.bind(null, 1)}>
                <img
                alt=''
                  src="/images/icons/ic-plus-sub.png"
                  width={17}
                  height={17}
                />{' '}
                {t('create-question')['add-answer']}
              </label>
            </td>
          </tr>
          )}
        </tbody>
      </table>
      </div>

      { errorGroupMess ? <><div className='error-message'>
        { errorGroupMess.map((item) => <span key={item} className='__text'>{`* ${item}`}</span> )}
      </div></> : ''}

    </div>
  )
}

export default MatchingGameTable6
