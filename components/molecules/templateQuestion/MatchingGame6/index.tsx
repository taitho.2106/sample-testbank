import { useEffect } from 'react'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'
import MatchingGameTable from './table'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation'

export default function MatchingGame6({
  question,
  register,
  setValue,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const {t} = useTranslation()
  useEffect(() => {
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '#').split('#').map((m, i) =>
          m
            .split('*')
            .map((g, gIndex) => {
              if (g.trim() === '' && i < 2) return gIndex
              return null
            })
            .filter((m) => m !== null),
        )
        return errs.some((m) => m.length > 0) ? errs.map((m) => m.join()).join('#') : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***#***',
    })
    register('correct_answers', {
      required: t('question-validation-msg')['canvas-answer'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
    register('mg6_images_left', {
      validate: async (files: any) => {
        if (!files) {
          return true
        }
        let fileErr: number[] = []
        let mIndex = -1
        for await (const file of files) {
          mIndex += 1
          if (file !== null) {
            if (!['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)) {
              fileErr.push(mIndex)
              continue
            } else if (!(await browserFile.checkFileExist(file))) {
              fileErr.push(mIndex)
              continue
            } else {
              fileErr.push(null)
              continue
            }
          }
          fileErr.push(null)
        }
        fileErr = fileErr.filter((m) => m != null)
        return fileErr.length > 0 ? fileErr.join() : true
      },
    })
    register('mg6_images_right', {
      validate: async(files: any) => {
        if (!files) {
          return true
        }
        let fileErr: number[] = []
        let mIndex = -1
        for await (const file of files) {
          mIndex += 1
          if (file !== null) {
            if (!['image/jpeg', 'image/jpg', 'image/png'].includes(file?.type)) {
              fileErr.push(mIndex)
              continue
            } else if (!(await browserFile.checkFileExist(file))) {
              fileErr.push(mIndex)
              continue
            } else {
              fileErr.push(null)
              continue
            }
          }
          fileErr.push(null)
        }
        fileErr = fileErr.filter((m) => m != null)
        return fileErr.length > 0 ? fileErr.join() : true
      },
    })
  }, [])

  const onChange = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers)
    setValue('mg6_images_left', data.mg6_images_left)
    setValue('mg6_images_right', data.mg6_images_right)
  }
  return (
    <div className="m-matching-game-6" style={{ display: 'flex' }}>
      <div style={{ flex: 1 }}>
        <TextAreaField
          label={t('create-question')['question-description']}
          setValue={setValue}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          className={`form-input-stretch ${errors['question_description'] ? 'error' : ''}`}
          style={{ height: '4rem' }}
          viewMode={viewMode}
          specialChar={['#', '*']}
        />
        <MatchingGameTable
          className={`form-input-stretch ${errors['correct_answers'] ? 'error' : ''}`}
          answerString={question?.answers ?? '***#***'}
          correctString={question?.correct_answers}
          onChange={onChange}
          errorString={errors['answers']?.message ?? ''}
          register={register}
          setValue={setValue}
          errors={errors}
        />
      </div>
    </div>
  )
}
