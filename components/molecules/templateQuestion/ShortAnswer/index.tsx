import { useEffect, useRef } from 'react'

import { Button, Tooltip, Whisper } from 'rsuite'

import { ContentField } from 'components/atoms/contentField'
import { TextAreaField } from 'components/atoms/textAreaField'
import { MyCustomCSS, QuestionPropsType } from "interfaces/types";

import ShortAnswerTable from './table'
import useTranslation from '@/hooks/useTranslation';

export default function ShortAnswer({
  question,
  register,
  setValue,
  errors = {},
  viewMode,
}: QuestionPropsType) {
  const {t} = useTranslation()
  const inputRef = useRef(null)
  const inputRefQuestion = useRef(null)

  useEffect(() => {
    register('question_text', {
      required: t('question-validation-msg')['question-text-required'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
    },
      value: question?.question_text,
    })
    register('answers', {
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['suggestion-max-length'], ['10000']),
      },
      value: question?.answers,
    })
    register('correct_answers', {
      validate: (value: string) => {
        if ((value ?? '') === '') return ''
        const errs = value
          .split('%/%')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [])

  const onChangeQuestionText = (data: any) => {
    // console.log("data---",data)
    setValue('question_text', data.replaceAll('##', '%s%'))
  }

  const onChangeHintText = (data: any) => {
    setValue('answers', data.replaceAll('##', '%s%'))
  }

  const onCorrectAnswerChange = (data: any) => {
    setValue('correct_answers', data)
  }

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }
  const onAddSpaceQuestion = (e: any) => {
    e.preventDefault()
    inputRefQuestion.current.pressTab()
  }
  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)

  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }
  return (
    <div className="m-short-answer" style={{ display: 'flex' }}>
      <div className="box-wrapper form-input-stretch" style={{
        flex: 1,
        display: 'flex',
        flexDirection: 'column',
        position: 'relative',
        padding: '10px'
      }}>
        <ContentField
            ref={inputRefQuestion}
            label={t('common')['question']}
            strValue={question?.question_text?.replaceAll('%s%', '##')}
            disableTabInput={true}
            isMultiTab={true}
            onChange={onChangeQuestionText}
            className={`${errors['question_text'] ? 'error' : ''}`}
            inputStyle={{ paddingRight: '3rem', height: '100%' }}
            viewMode={viewMode}
            specialChar={['#','*']}
        >
          <div className="box-action-info">
            <div style={{ display: 'flex', alignItems: "flex-end" }}>
              <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onBold}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-bold.png)',
                    } as MyCustomCSS
                  }
              ></div>
              <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onItalic}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-italic.png)',
                    } as MyCustomCSS
                  }
              ></div>
              <div
                  className="icon-mask icon-action-style-highlight"
                  onMouseDown={onUnderline}
                  style={
                    {
                      '--image': 'url(/images/icons/ic-underline.png)',
                    } as MyCustomCSS
                  }
              ></div>
              <Whisper
                  placement='top'
                  trigger={'hover'}
                  speaker={
                    <Tooltip>{t('question-update-container')['add-space']}</Tooltip>
                  }
              >
                <Button className="add-new-answer" onMouseDown={onAddSpaceQuestion}>
                  <img
                      src="/images/icons/ic-underscore.png"
                      alt="add-new-answer-btn"
                  />
                </Button>
              </Whisper>
            </div>
          </div>
        </ContentField>
      </div>
      <div className="box-wrapper">
        <TextAreaField
          label={t('create-question')['question-description']}
          setValue={setValue}
          defaultValue={question?.question_description}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          style={{ height: '6rem' }}
          viewMode={viewMode}
          specialChar={['#','*']}
        />
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <ContentField
            ref={inputRef}
            label={t('create-question')['clues']}
            strValue={question?.answers?.replaceAll('%s%', '##')}
            disableTabInput={true}
            onBlur={onChangeHintText}
            style={{ height: '9rem' }}
            className={errors['answers'] ? 'error' : ''}
            viewMode={viewMode}
            specialChar={['#','*']}
            disableStyleText={['b','u','i']}
          >
            <div className="box-action-info">
              <Whisper
                placement='top'
                trigger={'hover'}
                speaker={
                  <Tooltip>{t('question-update-container')['add-space']}</Tooltip>
                }
              >
                <Button className="add-new-answer" onMouseDown={onAddSpace}>
                  <img 
                    src="/images/icons/ic-underscore.png"
                    alt="add-new-answer-btn"
                  />
                </Button>
              </Whisper>
              <div className="clear-outline" style={{backgroundColor: '#f5f8fc', width: '4.3rem'}}></div>
            </div>
          </ContentField>
          {/* <div className="box-action-info">
            <span className="add-new-answer" onMouseDown={onAddSpace}>
              <img src="/images/icons/ic-plus-sub.png" width={17} height={17} />{' '}
              Thêm khoảng trống
            </span>
          </div> */}
        </div>
        <ShortAnswerTable
          className={`form-input-stretch ${
            errors['correct_answers']?.message === '' ? 'error' : ''
          }`}
          correctStr={question?.correct_answers}
          onChange={onCorrectAnswerChange}
          errorStr={errors['correct_answers']?.message ?? ''}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}
