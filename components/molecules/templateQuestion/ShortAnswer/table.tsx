import useTranslation from '@/hooks/useTranslation'
import { KeyboardEvent, useCallback, useEffect, useRef, useState } from 'react'

import ContentEditable from 'react-contenteditable'

type PropType = {
  correctStr: string
  className?: string
  errorStr?: string
  onChange: (data: any) => void
  viewMode?: boolean 
}

export default function ShortAnswerTable({
  correctStr,
  className = '',
  errorStr,
  onChange,
  viewMode,
}: PropType) {
  const {t} = useTranslation()
  const [answers, setAnswers] = useState<string[]>(correctStr?.split('%/%'))
  const answerRef = useRef(correctStr?.split('%/%')||[])
  const groupKey = useRef(Math.random()).current
  const isInit = useRef(true)

  useEffect(() => {
    if (isInit.current) {
      isInit.current = false
    } else {
      if (typeof onChange === 'function') {
        const str = answers?.map(m=>m?.trim()).join("%/%");
        onChange(str)
      }
    }

    setTimeout(() => {
      Array.from(document.getElementsByClassName(`${groupKey}`)).forEach(
        (el) => {
          const div = el as HTMLDivElement
          div.style.width = `${div.offsetWidth}px`
        },
      )
    }, 0)
  }, [answers])

  const onAddAnswer = () => {
    if (answers) {
      answerRef.current = [...answers, '']
      setAnswers([...answers, ''])
    } else {
      answerRef.current = [""]
      setAnswers([''])
    }

    setTimeout(() => {
      document.getElementById(`${groupKey}_${answers?.length ?? 0}`)?.focus()
    }, 0)
  }

  const onChangeText = (index: number, event: any) => {
    const el: HTMLElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      if (childNode.nodeName === 'B') {
        text += `%${childNode.textContent}%`
      } else {
        text += childNode.textContent
      }
    }
    answerRef.current[index] = text
    answers[index] = text
    setAnswers([...answers])
  }

  const onPaste = useCallback((event: any) => {
    event.preventDefault()
    const data = event.clipboardData.getData('text').split('\n').map((item: any) => item.trim()).join(' ')
    const selection = window.getSelection()
    const range = selection.getRangeAt(0)
    range.deleteContents()
    const node = document.createTextNode(data)
    range.insertNode(node)
    selection.removeAllRanges()
    const newRange = document.createRange()
    newRange.setStart(node, data.length)
    selection.addRange(newRange)
  }, [])

  const onBlurText = (index: number, event: any) => {
    const el: HTMLElement = event.currentTarget
    let text = ''
    for (let i = 0, len = el.childNodes.length; i < len; i++) {
      const childNode = el.childNodes[i]
      if (childNode.nodeName === 'B') {
        text += `%${childNode.textContent}%`
      } else {
        text += childNode.textContent
      }
    }
    const temps = [...answerRef.current]
    temps[index] = text.trim()
     el.innerHTML = temps[index]
     answerRef.current = temps;
     setAnswers(temps)
  }
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    if (event.key === '#' || event.key === '*') {
      event.preventDefault()
    }
    if(event.ctrlKey && ['b','u','i'].some(item => event.key.toLowerCase() === item)){
      event.preventDefault()
    }
  }, [])

  const onDeleteAnswer = (index: number) => {
    answerRef.current.splice(index, 1)
    setAnswers([...answerRef.current])
  }
  const listError = errorStr.split(',')

  return (
    <div className={className} style={{ position: 'relative' }}>
      <table className={`question-table`}>
        <thead>
          <tr>
            <th>{t('create-question')['correct-answer']}</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {answers?.map((answer: string, answerIndex: number) => (
            <tr key={`${groupKey}_${answerIndex}`}>
              <td
                className={
                  listError.includes(answerIndex.toString()) ? 'error' : ''
                }
              >
                <ContentEditable
                  id={`${groupKey}_${answerIndex}`}
                  spellCheck={false}
                  className={`div-input ${groupKey}`}
                  html={answer}
                  contentEditable="true"
                  onChange={onChangeText.bind(null, answerIndex)}
                  onBlur={onBlurText.bind(null, answerIndex)}
                  onKeyDown={onKeyDown}
                  onPaste={onPaste}
                />
              </td>
              <td align="right">
                <img
                  onClick={onDeleteAnswer.bind(null, answerIndex)}
                  className="ic-action"
                  src="/images/icons/ic-trash.png"
                  alt="delete-answer-icon"
                />
              </td>
            </tr>
          ))}
          {!viewMode && (
          <tr className="action">
            <td colSpan={2} align="left">
              <label className="add-new-answer" onClick={onAddAnswer}>
                <img
                  src="/images/icons/ic-plus-sub.png"
                  width={17}
                  height={17}
                  alt="add-new-answer-icon"
                />{' '}
                {t('create-question')['add-answer']}
              </label>
            </td>
          </tr>
          )}
        </tbody>
      </table>
      <div className="border-error"></div>
    </div>
  )
}
