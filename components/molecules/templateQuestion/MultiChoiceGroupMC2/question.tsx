import { MyCustomCSS } from '@/interfaces/types'
import { ContentField } from 'components/atoms/contentField'
import { Fragment, useEffect, useRef, useState } from 'react'
import { UseFormRegister, UseFormSetValue } from 'react-hook-form'
import { MdRestaurantMenu } from 'react-icons/md'
import { Button, Toggle, Tooltip, Whisper } from 'rsuite'
import MultiChoiceTable from './table'
import useTranslation from "@/hooks/useTranslation";

type PropsType = {
  corrects: string
  answers: string
  questions: string
  errorStr?: string
  correctErrorStr?: string
  questionErrorStr?: string
  onQuestionChange?: (data: any) => void
  viewMode?: boolean
  register: UseFormRegister<any>
  setValue: UseFormSetValue<any>
}
export default function QuestionMC2({
  corrects,
  answers,
  questions,
  errorStr = '',
  correctErrorStr = '',
  questionErrorStr = '',
  onQuestionChange,
  register,
  setValue,
  viewMode,
}: PropsType) {
  const { t } = useTranslation()
  const correctList = corrects?.split('#') ?? []
  const answerList = answers?.split('#') ?? ['***']
  const questionList = questions?.split('#') ?? []
  const [answerGroup, setAnswerGroup] = useState<AnswerGroup[]>(
    answerList.map((g: string, gIndex: number) => {
      return {
        key: new Date().getTime().toString() + gIndex,
        answerStrs: g,
        correctStrs: correctList[gIndex],
        questionText: questionList[gIndex]?.replace('*', ''),
        isQuestion: !questionList[gIndex]?.startsWith('*'),
        toggleErr: false,
      }
    }),
  )
  const answerGroupRef = useRef(answerGroup)
  const isInit = useRef(true)
  const renderError = () => {
    if (answerGroup.filter((m) => !m.isQuestion).length > 1) {
      setValue('passData', false)
      return setError(t(t('question-validation-msg')['example-allow-total'], ['1']))
    }
    if (answerGroup.filter((m) => m.isQuestion).length == 0) {
      setValue('passData', false)
      return setError(t(t('question-validation-msg')['question-fb-required'], ['1']))
    }
    setValue('passData', true)
    return setError('')
  }
  useEffect(() => {
    answerGroupRef.current = answerGroup
    if (!isInit.current) {
      onQuestionChange &&
        onQuestionChange({
          questions: answerGroup
            .map((m) => {
              if (m.isQuestion) {
                let question = m.questionText
                while (question?.startsWith('*')) {
                  question = question.substring(1)
                }
                return question
              } else {
                //example
                let question = m.questionText
                while (question?.startsWith('*')) {
                  question = question.substring(1)
                }
                if (question) {
                  return '*' + question
                }
                return ''
              }
            })
            .join('#'),
          answers: answerGroup.map((m) => m.answerStrs).join('#'),
          correctStrs: answerGroup.map((m) => m.correctStrs).join('#'),
        })
    }
    isInit.current = false
    renderError()
  }, [answerGroup])
  useEffect(() => {
    register('passData', {
      validate: (value: string) => {
        return value
      },
    })
  }, [])
  const onAddQuestion = () => {
    const newQuestion: any = {
      key: new Date().getTime().toString() + (answerGroup?.length ?? 0),
      questionText: '',
      answerStrs: '***',
      correctStrs: null,
      isQuestion: true,
      toggleErr: false,
    }
    if (answerGroup) {
      setAnswerGroup([...answerGroup, newQuestion])
    } else {
      setAnswerGroup([newQuestion])
    }
  }

  const onChange = (key: string, data: any) => {
    const answer = answerGroupRef.current.find((m) => m.key === key)
    answer.answerStrs = data.answers
    answer.correctStrs = data.correct_answers
    setAnswerGroup([...answerGroupRef.current])
  }

  const onChangeQuestionText = (key: string, data: string) => {
    const answer = answerGroupRef.current.find((m) => m.key === key)
    while (data?.startsWith('*')) {
      data = data.substring(1)
    }
    answer.questionText = data.replace(/##/g, '%s%').trim()
    setAnswerGroup([...answerGroupRef.current])
  }

  const onRemoveQuestion = (key: string) => {
    const ans = answerGroup.filter((m: any) => m.key !== key)
    setAnswerGroup([...ans])
  }
  const onToggleExample = (mIndex: number) => {
    let data = [...answerGroup]
    data[mIndex].toggleErr = false
    data[mIndex].isQuestion = !data[mIndex].isQuestion

    //only 1 example
    if (!data[mIndex].isQuestion && data.filter((m) => !m.isQuestion).length > 1) {
      data[mIndex].toggleErr = true
    }
    
    if (data.filter((m) => !m.isQuestion).length == 1) {
      data = data.map((m) => {
        return { ...m, toggleErr: false }
      })
    }
    setAnswerGroup(data)
  }
  const listErrors = errorStr?.split('#')
  const listQError = questionErrorStr?.split(',')
  const listCError = correctErrorStr?.split(',')
  const [error, setError] = useState('')

  return (
    <Fragment>
      {answerGroup?.map((m, mIndex) => (
        <GroupItem
          key={m.key}
          questionText={m.questionText}
          answerStrs={m.answerStrs}
          correctStrs={m.correctStrs?.trim()}
          onChangeQuestionText={onChangeQuestionText.bind(null, m.key)}
          onChange={onChange.bind(null, m.key)}
          onRemoveQuestion={onRemoveQuestion.bind(null, m.key)}
          error={listCError.includes(mIndex.toString())}
          errorStr={listErrors[mIndex]}
          question_error={listQError.includes(mIndex.toString())}
          onToggleExample={() => onToggleExample(mIndex)}
          isQuestion={m.isQuestion}
          viewMode={viewMode}
          toggleErr={m.toggleErr}
        />
      ))}
      <div className="form-input-stretch" style={{ marginBottom: 0 }}>
        {error && (
          <div className="form-error-message">
            <span>{error}</span>
          </div>
        )}
        <Button
          appearance={'primary'}
          type="button"
          style={{
            backgroundColor: 'white',
            width: '20rem',
            color: '#5CB9D8',
            fontSize: '1.6rem',
            marginTop: '1.6rem',
            fontWeight: 600,
            ...(answerGroup.length > 0
              ? {}
              : { border: '0.1rem solid #dd3a09' }),
          }}
          onClick={onAddQuestion}
        >
          <label className="add-new-answer">
            <img
              alt=""
              src="/images/icons/ic-plus-sub.png"
              width={17}
              height={17}
            />{' '}
            {t('create-question')['add-question']}
          </label>
        </Button>
      </div>
    </Fragment>
  )
}

type GroupItemProps = {
  questionText: string
  question_error: boolean
  onChangeQuestionText: (data: string) => void
  answerStrs: string
  correctStrs: string
  onChange: (data: string) => void
  error: boolean
  errorStr: string
  onRemoveQuestion: () => void
  onToggleExample: () => void
  isQuestion: boolean
  viewMode: boolean
  toggleErr: boolean
}

function GroupItem({
  questionText,
  question_error,
  onChangeQuestionText,
  answerStrs,
  correctStrs,
  onChange,
  error,
  errorStr,
  onRemoveQuestion,
  onToggleExample,
  isQuestion,
  viewMode,
  toggleErr,
}: GroupItemProps) {
  const { t } = useTranslation()
  const inputRef = useRef(null)

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }
  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)
  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }

  return (
    <div className="form-input-stretch " style={{ position: 'relative' }}>
      <div style={{ position: 'relative' }}>
        <ContentField
          disabled={true}
          ref={inputRef}
          label={isQuestion ? t('common')['question'] : t('common')['example']}
          strValue={questionText?.replace(/\%s\%/g, '##')}
          disableTabInput={true}
          isMultiTab={true}
          onBlur={onChangeQuestionText}
          className={question_error ? 'error' : ''}
          style={{ marginBottom: '1rem', height: '10rem' }}
          inputStyle={{ paddingRight: '3rem' }}
          viewMode={viewMode}
          specialChar={['#','*']}
        >
          <div className="box-action-info">
            <div
              className={
                toggleErr ? '__content-toggle toggle-error' : '__content-toggle'
              }
            >
              <Toggle
                size="md"
                className="__switcher"
                disabled={viewMode}
                checked={isQuestion}
                checkedChildren={t("common")["question"]}
                unCheckedChildren={t('common')['example']}
                onChange={onToggleExample}
              />
            </div>
            <div style={{ display: 'flex', alignItems: 'flex-end' }}>
              <Whisper
                placement="top"
                trigger={'hover'}
                speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
              >
                <Button className="add-new-answer" onMouseDown={onAddSpace}>
                  <img alt="" src="/images/icons/ic-underscore.png" />
                </Button>
              </Whisper>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onBold}
                style={
                  {
                    '--image': 'url(/images/icons/ic-bold.png)',
                  } as MyCustomCSS
                }
              ></div>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onItalic}
                style={
                  {
                    '--image': 'url(/images/icons/ic-italic.png)',
                  } as MyCustomCSS
                }
              ></div>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onUnderline}
                style={
                  {
                    '--image': 'url(/images/icons/ic-underline.png)',
                  } as MyCustomCSS
                }
              ></div>
            </div>

            <div className="clear-outline"></div>
          </div>
        </ContentField>
        {/* <div className="box-action-info">
          <span className="add-new-answer" onMouseDown={onAddSpace}>
            <img src="/images/icons/ic-plus-sub.png" width={17} height={17} />{' '}
            Thêm khoảng trống
          </span>
          <div className="box-media"></div>
        </div> */}
      </div>
      <MultiChoiceTable
        answerStr={answerStrs}
        correctStr={correctStrs}
        onChange={onChange}
        className={error ? 'error' : ''}
        errorStr={errorStr}
        styleBold={true}
      />
      <img
        alt=""
        className="ic-delete-question"
        src="/images/icons/ic-trash.png"
        onClick={onRemoveQuestion}
      />
    </div>
  )
}

type AnswerGroup = {
  key: string
  answerStrs: string
  correctStrs: string
  questionText: string
  isQuestion: boolean
  toggleErr?: boolean
}
