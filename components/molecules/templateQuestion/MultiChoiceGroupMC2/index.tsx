import { useEffect, useRef, useState } from 'react'

import { TextAreaField } from 'components/atoms/textAreaField'
import { QuestionPropsType } from 'interfaces/types'

import QuestionMC2 from './question'
import browserFile from 'lib/browserFile'
import useTranslation from "@/hooks/useTranslation";

export default function MultiChoiceGroupMC2({
  question,
  register,
  setValue,
  isAudio = false,
  errors = {},
  viewMode,
}: QuestionPropsType) {
  const { t } = useTranslation()
  const [imgUrl, setImgUrl] = useState(question?.image || '')
  useEffect(() => {
    register('image_file', {
      validate: {
        acceptedFormats: async(file) => {
          // check format if has file
          if (!file) {
            return true
          }
          if(file.size ==0){
            return t('question-validation-msg')['image-deleted']
          }
          if(!['image/jpeg', 'image/png'].includes(file?.type)){
            return t('question-validation-msg')['image-file']
          }
          if(!await browserFile.checkFileExist(file)){
            return t('question-validation-msg')['image-deleted']
          }
          return true;
        },
      },
    })
    register('image', {
      maxLength: {
        value: 2000,
        message: t(t('question-validation-msg')['image-max-length'], ['2000']),
      },
      required: t('question-validation-msg')['image-required'],
      value: question?.image,
    })
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '').split('#').map((m) =>
          m
            .split('*')
            .map((g, gIndex) => {
              if (g === '') return gIndex
              return null
            })
            .filter((m) => m !== null),
        )
        const re = errs.some((m) => m.length > 0)
          ? errs.map((m) => m.join()).join('#')
          : true
        console.log('re=----', re)
        return re
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, gIndex) => {
            if (m === '') return gIndex
            return null
          })
          .filter((m) => m !== null)
        const re = errs.length > 0 ? errs.join() : true
        return re
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
    register('question_text', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (!m) return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
      },
      value: question?.question_text,
    })
  }, [])
  const onMCChange = (dataV: any) => {
    setValue('answers', dataV.answers)
    setValue('correct_answers', dataV.correctStrs)
    setValue('question_text', dataV.questions.trim())
  }
  const onChangeFile = (files: FileList) => {
    if (files && files[0]) {
      setValue('image_file', files[0])
      const urlImage = URL.createObjectURL(files[0])
      setValue('image', urlImage)
      setImgUrl(urlImage)
    } else {
      setValue('image_file', null)
      setValue('image', '')
      setImgUrl('')
    }
  }
  return (
    <div className="m-multi-choice-mc2">
      <div className="m-multi-choice-mc2__left">
        <div className="form-input-stretch">
          <ImageInputMC2
            url={imgUrl}
            isError={errors['image'] || errors['image_file']}
            onChangeFile={onChangeFile}
            viewMode={viewMode}
          />
        </div>
      </div>
      <div className="m-multi-choice-mc2__right">
        <div className={`form-input-stretch mc-2`}>
          <TextAreaField
            label={t('create-question')['question-description']}
            setValue={setValue}
            register={register('parent_question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 2000,
                message: t(t('question-validation-msg')['description-max-length'], ['2000']),
              },
            })}
            defaultValue={question?.parent_question_description}
            style={{ height: '9rem' }}
            inputStyle={isAudio ? { paddingBottom: '4.5rem' } : {}}
            className={errors['parent_question_description'] ? 'error' : ''}
            viewMode={viewMode}
            specialChar={['#','*']}
          />
        </div>
        <QuestionMC2
          questions={question?.question_text}
          errorStr={errors['answers']?.message ?? ''}
          correctErrorStr={errors['correct_answers']?.message ?? ''}
          questionErrorStr={errors['question_text']?.message ?? ''}
          onQuestionChange={onMCChange}
          answers={question?.answers}
          corrects={question?.correct_answers}
          viewMode={viewMode}
          register={register}
          setValue={setValue}
        />
      </div>
    </div>
  )
}

type PropsType = {
  labelDefault?: string
  isError?: any
  url: string
  onChangeFile?: (file: FileList) => void
  viewMode?: boolean
}
function ImageInputMC2({
  url,
  onChangeFile,
  isError,
  viewMode = false,
}: PropsType) {
  const { t } = useTranslation()
  const [imageUrl, setImageUrl] = useState(url ?? '')
  const fileRef = useRef(null)
  const src = imageUrl.startsWith('blob') ? imageUrl : `/upload/${imageUrl}`
  return (
    <div
      className={`box-image-input-MC2 ${imageUrl !== '' ? 'has-data' : ''} ${
        isError ? 'error' : ''
      }`}
    >
      <div className="image-section">
        <div className="image-input-origin">
          {src && url ? (
            <>
              <img
                className="image-upload"
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                src={src}
                alt="image-upload"
                style={{cursor:'pointer'}}
              />
              {!viewMode && src && url && (
                <img
                  className="image-delete-icon"
                  src="/images/icons/ic-remove-opacity.png"
                  alt=""
                  onClick={() => {
                    fileRef.current.value = ''
                    event.preventDefault()
                    event.stopPropagation()
                    onChangeFile(null)
                  }}
                />
              )}
            </>
          ) : (
            <>
              <div
                onClick={() =>
                  fileRef.current.dispatchEvent(new MouseEvent('click'))
                }
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  justifyContent: 'center',
                  alignItems: 'center',
                  height: '100%',
                  cursor:'pointer'
                }}
              >
                <img
                  className="icon"
                  src="/images/icons/ic-image.png"
                  alt="delete-img-icon"
                />
                {t('create-question')['image']}
              </div>
            </>
          )}
          <input
            ref={fileRef}
            type="file"
            style={{ display: 'none' }}
            accept=".png,.jpeg,.jpg"
            onChange={(e) => {
              if (fileRef.current.files.length > 0) {
                setImageUrl(URL.createObjectURL(fileRef.current.files[0]))
                onChangeFile && onChangeFile(e.target.files)
              }
            }}
          />
        </div>
      </div>
      {!isError && src && url && <span>{t('create-question')['question-image-1']}</span>}
    </div>
  )
}
