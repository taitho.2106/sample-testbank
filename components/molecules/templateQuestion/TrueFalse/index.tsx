import { useEffect } from 'react'

import { ContentField } from 'components/atoms/contentField'
import { TextAreaField } from 'components/atoms/textAreaField'
import QuestionTableTF4 from 'components/molecules/templateQuestion/TrueFalseType4'
import { MyCustomCSS, QuestionPropsType } from "interfaces/types";

import AudioInput from '../AudioInput'
import TrueFalseTable from './table'
import browserFile from 'lib/browserFile'
import useTranslation from '@/hooks/useTranslation';

export default function TrueFalse({
  question,
  register,
  setValue,
  isAudio = false,
  isReading = false,
  errors = {},
  viewMode = false,
  isImage = false,
  questionType,
}: QuestionPropsType) {
  const {t} = useTranslation()
  useEffect(() => {
    if (isAudio) {
      register('audio_file',{
        validate: {
          acceptedFormats: async(file) => {
            // check format if has file
            if (!file) {
              return true
            }
            if(file.size ==0){
              return t('question-validation-msg')['audio-link-error']
            }
            if(!['audio/mp3','audio/mpeg'].includes(file?.type)){
              return t('question-validation-msg')['audio-type']
            }
            if(!await browserFile.checkFileExist(file)){
              return t('question-validation-msg')['audio-link-error']
            }
            return true;
          },
        }
      })
      register('audio', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['audio-max-length'],['2000']),
        },
        required: t('question-validation-msg')['audio-required'],
        value: question?.audio,
      })
      register('audio_script', {
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['audio-script-max-length'],['10000']),
        },
        value: question?.audio_script,
      })
    }
    if (isReading) {
      register('question_text', {
        required: t('question-validation-msg')['reading-script-required'],
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['reading-script-max-length'], ['10000']),
        },
        value: question?.question_text,
      })
    }
    register('answers', {
      validate: (value: string) => {
        // if ((value ?? '') === '') return ''
        const errs = value
          .split('#')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '###',
    })
    register('correct_answers', {
      validate: (value: string) => {
        const ansl = ['T', 'F']
        const errs = (value ?? '')
          .split('#')
          .map((m, mIndex) => {
            if (!ansl.includes(m)) return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers ?? '###',
    })
  }, [])

  const onChangeAudioScript = (data: any) => {
    if (isAudio) {
      setValue('audio_script', data)
    }
    if (isReading) {
      setValue('question_text', data)
    }
  }

  const onCorrectAnswerChange = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers)
    setValue('image',data.image)
  }

  const onChangeFile = (files: FileList) => {
    if (files.length > 0) {
      setValue('audio_file', files[0])
      const urlAudio = URL.createObjectURL(files[0])
      setValue('audio', urlAudio)
    }
  }
  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)

  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }
  return (
    <div className="m-true-false" style={{ display: 'flex' }}>
      {isAudio && (
        <AudioInput
          url={question?.audio}
          script={question?.audio_script}
          isError={errors['audio'] || errors['audio_file']}
          onChangeFile={onChangeFile}
          onChangeScript={onChangeAudioScript}
          style={{ height: '398px' }}
          viewMode={viewMode}
        />
      )}
      {isReading && (
        <div style={{ flex: 1, display: 'flex', flexDirection: 'column' }}>
          <div
              className={`form-input-stretch`}
              style={{
                flex: 1,
                display: 'flex',
                flexDirection: 'column',
                position: 'relative'
              }}
          >
            <ContentField
                label={t('create-question')['reading-text']}
                strValue={question?.question_text}
                disableTab={true}
                isMultiTab={true}
                onChange={onChangeAudioScript}
                style={{ flex: 1 }}
                className={`${errors['question_text'] ? 'error' : ''}`}
                inputStyle={{ paddingRight: '3rem', height: '100%' }}
                viewMode={viewMode}
                specialChar={['#','*']}
            >
              <div className="box-action-info" style={{
                height: 45,
                justifyContent: "flex-start"
              }}>
                <div style={{ display: 'flex', alignItems: "flex-end" }}>
                  <div
                      className="icon-mask icon-action-style-highlight"
                      onMouseDown={onBold}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-bold.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                  <div
                      className="icon-mask icon-action-style-highlight"
                      onMouseDown={onItalic}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-italic.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                  <div
                      className="icon-mask icon-action-style-highlight"
                      onMouseDown={onUnderline}
                      style={
                        {
                          '--image': 'url(/images/icons/ic-underline.png)',
                        } as MyCustomCSS
                      }
                  ></div>
                </div>
                <div className="clear-outline" style={{}}></div>
              </div>
            </ContentField>
          </div>
        </div>
      )}
      <div style={{ flex: 1 }}>
        <div className="form-input-stretch" style={{ position: 'relative' }}>
          <TextAreaField
            label={t('create-question')['question-description']}
            setValue={setValue}
            defaultValue={question?.question_description}
            register={register('question_description', {
              required: t('question-validation-msg')['description-required'],
              maxLength: {
                value: 10000,
                message: t(t('question-validation-msg')['description-max-length'], ['10000']),
              },
            })}
            style={questionType === 'TF4' ? {height: '4.5rem'} : { height: '9rem' }}
            className={errors['question_description'] ? 'error' : ''}
            viewMode={viewMode}
            specialChar={['#','*']}
          />
        </div>
        {questionType !== 'TF4' ? 
        (<TrueFalseTable
          className={`form-input-stretch ${
            errors['answers']?.message === '' ? 'error' : ''
          }`}
          answerStr={question?.answers ?? '###'}
          correctStr={question?.correct_answers ?? '###'}
          onChange={onCorrectAnswerChange}
          errorStr={errors['answers']?.message ?? ''}
          correctErrorStr={errors['correct_answers']?.message ?? ''}
          viewMode={viewMode}
        />)
        :
        (<QuestionTableTF4 
          className={`${errors['answers']?.message === '' ? 'error' : ''}`}
          errors = {errors}
          setValue={setValue}
          register={register}
          question={question}
          errorStr={errors['answers']?.message ?? ''}
          correctErrorStr={errors['correct_answers']?.message ?? ''}
          onChange={onCorrectAnswerChange}
          viewMode={viewMode}
          isImage={isImage}
          />)}
      </div>
    </div>
  )
}
