import { useCallback, useEffect, useRef, useState } from 'react'

import { Button, Tooltip, Whisper } from 'rsuite'

import { ContentField } from 'components/atoms/contentField'
import { InputField } from 'components/atoms/inputField'
import { QuestionPropsType } from 'interfaces/types'

import ImageInput from '../ImageInput'
import MultiChoiceTable from './table'
import useTranslation from "@/hooks/useTranslation";


export default function MultiChoice({
  question,
  register,
  setValue,
  isReading = false,
  isImage = false,
  errors = {},
  viewMode = false,
}: QuestionPropsType) {
  const { t } = useTranslation()
  const inputRef = useRef(null)
console.log("MultiChoice---------")
  useEffect(() => {
    if (isReading) {
      register('question_text', {
        required: t('question-validation-msg')['question-text-required'],
        maxLength: {
          value: 10000,
          message: t(t('question-validation-msg')['question-text-max-length'], ['10000']),
        },
        value: question?.question_text,
      })
    }
    if (isImage) {
      register('image_file', {
        validate: {
          acceptedFormats: (file) => {
            // check format if has file
            if (!file) {
              return true
            }
            return (
              ['image/jpeg', 'image/png'].includes(file?.type) ||
              t('question-validation-msg')['image-file']
            )
          },
        },
      })
      register('image', {
        maxLength: {
          value: 2000,
          message: t(t('question-validation-msg')['image-max-length'], ['2000']),
        },
        required: t('question-validation-msg')['image-required'],
        value: question?.image,
      })
    }
    register('answers', {
      validate: (value: string) => {
        const errs = (value ?? '')
          .split('*')
          .map((m, mIndex) => {
            if (m === '') return mIndex
            return null
          })
          .filter((m) => m !== null)
        return errs.length > 0 ? errs.join() : true
      },
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['answers-max-length'], ['10000']),
      },
      value: question?.answers ?? '***',
    })
    register('correct_answers', {
      required: t('question-validation-msg')['canvas-answers'],
      maxLength: {
        value: 10000,
        message: t(t('question-validation-msg')['correct-answers-max-length'], ['10000']),
      },
      value: question?.correct_answers,
    })
  }, [])

  const updateFormData = (data: any) => {
    setValue('answers', data.answers)
    setValue('correct_answers', data.correct_answers ?? '')
  }

  const onChangeQuestionText = useCallback((data: string) => {
    setValue('question_text', data.replace(/##/g, '%s%'))
  }, [])

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }

  const onChangeFile = (files: FileList) => {
    setValue('image_file', files[0])
    const urlImage = URL.createObjectURL(files[0])
    setValue('image', urlImage)
  }

  return (
    <div className="m-multi-choice" style={{ display: 'flex' }}>
      {isReading && (
        <div className="box-wrapper">
          {isImage && (
            <ImageInput
              url={question?.image}
              onChangeFile={onChangeFile}
              isError={errors['image'] || errors['image_file']}
            />
          )}
          <div className="form-input-stretch" style={{ position: 'relative' }}>
            <ContentField
              ref={inputRef}
              label={t('common')['question']}
              strValue={(question?.question_text as any)?.replaceAll(
                '%s%',
                '##',
              )}
              disableTabInput={true}
              isMultiTab={true}
              onChange={onChangeQuestionText}
              inputStyle={{ padding: '1rem 1.2rem 4.5rem 1.2rem' }}
              className={errors['question_text'] ? 'error' : ''}
              viewMode={viewMode}
              specialChar={['#','*']}
            >
              <div className="box-action-info">
                <Whisper
                  placement="top"
                  trigger={'hover'}
                  speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
                >
                  <Button className="add-new-answer" onMouseDown={onAddSpace}>
                    <img src="/images/icons/ic-underscore.png" alt='add-new-answer-btn'/>
                  </Button>
                </Whisper>
                <div
                  className="clear-outline"
                  style={{ backgroundColor: '#f5f8fc' }}
                ></div>
              </div>
            </ContentField>
          </div>
        </div>
      )}
      <div className="box-wrapper">
        <InputField
          label={t('create-question')['question-description']}
          type="text"
          setValue={setValue}
          register={register('question_description', {
            required: t('question-validation-msg')['description-required'],
            maxLength: {
              value: 10000,
              message: t(t('question-validation-msg')['description-max-length'], ['10000']),
            },
          })}
          defaultValue={question?.question_description}
          className={`form-input-stretch ${
            errors['question_description'] ? 'error' : ''
          }`}
          preventKey={['#','*']}
        />
        <MultiChoiceTable
          className={`form-input-stretch ${
            errors['correct_answers'] ? 'error' : ''
          }`}
          answerStr={question?.answers ?? '***'}
          correctStr={question?.correct_answers}
          errorStr={`${errors['answers']?.message ?? ''}`}
          onChange={updateFormData}
          viewMode={viewMode}
        />
      </div>
    </div>
  )
}
