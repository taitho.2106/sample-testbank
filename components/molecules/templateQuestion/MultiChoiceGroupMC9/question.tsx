import { Fragment, useEffect, useRef, useState } from 'react'

import { UseFormRegister, UseFormSetValue } from 'react-hook-form'
import { Button, Toggle, Tooltip, Whisper } from 'rsuite'

import { MyCustomCSS } from '@/interfaces/types'
import { ContentField } from 'components/atoms/contentField'
import Guid from 'utils/guid'

import MultiChoiceTable from './table'
import useTranslation from "@/hooks/useTranslation";

type PropsType = {
  corrects: string
  answers: string
  questions: string
  errorStr?: string
  correctErrorStr?: string
  questionErrorStr?: string
  onQuestionChange?: (data: any) => void
  viewMode?: boolean,
  isQuestion?:boolean
}
export default function QuestionMC9({
  corrects,
  answers,
  questions,
  errorStr = '',
  correctErrorStr = '',
  questionErrorStr = '',
  onQuestionChange,
  viewMode,
  isQuestion
}: PropsType) {
  const [answerGroup, setAnswerGroup] = useState<AnswerGroup>(
    {
      key: Guid.newGuid(),
        answerStrs: answers,
        correctStrs: corrects,
        isQuestion: isQuestion,
        questionText: questions?.replace('*', ''),
    }
  )
  const answerGroupRef = useRef(answerGroup)
  const isInit = useRef(true)
  useEffect(() => {
    answerGroupRef.current = answerGroup
    if (!isInit.current) {
      let question = answerGroup?.questionText?.replaceAll("#","");
      if (isQuestion) {
        while (question?.startsWith('*')) {
          question = question.substring(1)
        }
      } else {
        //example
        while (question?.startsWith('*')) {
          question = question.substring(1)
        }
        question = '*' + question??""
      }
      onQuestionChange &&
        onQuestionChange({
          questionText: question,
          answers: answerGroup.answerStrs,
          correctStrs: answerGroup.correctStrs,
        })
    }
    isInit.current = false
  }, [answerGroup])
  const onChange = (data: any) => {
   const answerGroupTemp = answerGroupRef.current
   answerGroupTemp.answerStrs = data.answers
   answerGroupTemp.correctStrs = data.correct_answers
    setAnswerGroup({...answerGroupRef.current})
  }

  const onChangeQuestionText = (data: string) => {
    const answerGroupTemp = answerGroupRef.current;
    while (data?.startsWith('*')) {
      data = data.substring(1)
    }
    answerGroupTemp.questionText = data.replace(/##/g, '%s%')
    setAnswerGroup({...answerGroupTemp})
  }

  const listErrors = errorStr
  const listQError = questionErrorStr
  const listCError = correctErrorStr

  return (
    <Fragment>
      <GroupItem
          key={answerGroup.key}
          questionText={answerGroup.questionText}
          answerStrs={answerGroup.answerStrs}
          correctStrs={answerGroup.correctStrs}
          onChangeQuestionText={onChangeQuestionText}
          onChange={onChange}
          error={listCError?true:false}
          errorStr={listErrors}
          question_error={listQError?true:false}
          isQuestion={isQuestion}
          viewMode={viewMode}
        />
    </Fragment>
  )
}

type GroupItemProps = {
  questionText: string
  question_error: boolean
  onChangeQuestionText: (data: string) => void
  answerStrs: string
  correctStrs: string
  onChange: (data: string) => void
  error: boolean
  errorStr: string
  isQuestion: boolean
  viewMode: boolean
}

function GroupItem({
  questionText,
  question_error,
  onChangeQuestionText,
  answerStrs,
  correctStrs,
  onChange,
  error,
  errorStr,
  isQuestion,
  viewMode,
}: GroupItemProps) {
  const {t} = useTranslation()
  const inputRef = useRef(null)

  const onAddSpace = (e: any) => {
    e.preventDefault()
    inputRef.current.pressTab()
  }
  const onBold = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Bold', false, null)
  }
  const onItalic = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Italic', false, null)
  }
  const onUnderline = (e: any) => {
    e.preventDefault()
    const selection = window.getSelection()
    if (selection.rangeCount === 0) return
    document.execCommand('Underline', false, null)
  }
  return (
    <div className="form-input-stretch " style={{ position: 'relative' }}>
      <div style={{ position: 'relative' }}>
        <ContentField
          ref={inputRef}
          label={isQuestion ? t("common")["question"] : t("common")["example"]}
          strValue={questionText?.replaceAll(/\%s\%/g, '##')}
          disableTabInput={true}
          isMultiTab={true}
          onBlur={onChangeQuestionText}
          className={question_error ? 'error' : ''}
          style={{ marginBottom: '1rem', height: '10rem' }}
          inputStyle={{ paddingRight: '3rem' }}
          viewMode={viewMode}
          specialChar={['#','*']}
        >
          <div className="box-action-info">
            <div style={{ display: 'flex', alignItems: 'flex-end' }}>
              <Whisper
                placement="top"
                trigger={'hover'}
                speaker={<Tooltip>{t('question-update-container')['add-space']}</Tooltip>}
              >
                <Button className="add-new-answer" onMouseDown={onAddSpace}>
                  <img alt="" src="/images/icons/ic-underscore.png" />
                </Button>
              </Whisper>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onBold}
                style={
                  {
                    '--image': 'url(/images/icons/ic-bold.png)',
                  } as MyCustomCSS
                }
              ></div>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onItalic}
                style={
                  {
                    '--image': 'url(/images/icons/ic-italic.png)',
                  } as MyCustomCSS
                }
              ></div>
              <div
                className="icon-mask icon-action-style-highlight"
                onMouseDown={onUnderline}
                style={
                  {
                    '--image': 'url(/images/icons/ic-underline.png)',
                  } as MyCustomCSS
                }
              ></div>
            </div>

            <div className="clear-outline"></div>
          </div>
        </ContentField>
      </div>
      <MultiChoiceTable
        answerStr={answerStrs}
        correctStr={correctStrs}
        onChange={onChange}
        className={error ? 'error' : ''}
        errorStr={errorStr}
        styleBold={true}
      />
    </div>
  )
}

type AnswerGroup = {
  key: string
  answerStrs: string
  correctStrs: string
  questionText: string
  isQuestion: boolean
  toggleErr?: boolean
}
