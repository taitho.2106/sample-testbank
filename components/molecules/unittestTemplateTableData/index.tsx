import { useContext, useEffect, useMemo, useRef, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { Collapse } from 'react-collapse'
import { Button, Pagination, Popover, Tooltip, Whisper } from 'rsuite'

import { templateDetailTransform, templateListTransformData } from 'api/dataTransform'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import { MultiSelectPicker } from 'components/atoms/selectPicker/multiSelectPicker'
import { StickyFooter } from 'components/organisms/stickyFooter'
import useGrade from 'hooks/useGrade'
import { PACKAGES, USER_ROLES } from 'interfaces/constants'
import { SingleTemplateContext } from 'interfaces/contexts'
import { SKILLS_SELECTIONS, TEMPLATE_CREATED_BY } from 'interfaces/struct'
import { userInRight } from 'utils'

import { paths } from '../../../api/paths'
import { callApi } from '../../../api/utils'
import { DefaultPropsType } from '../../../interfaces/types'
import { RowItem } from './RowItem'
import useTranslation from "@/hooks/useTranslation";

interface PropsType extends DefaultPropsType {
  title: string
  data: any[]
  total: number
  pageIndex: number
  nextStep?: () => void
}

export const UnittestTemplateTableData = ({
  className = '',
  data,
  total,
  pageIndex,
  style,
  nextStep = () => null,
}: PropsType) => {
  const { t } = useTranslation()
  const router = useRouter()

  const [session] = useSession()
  const { getGrade, gradeList } = useGrade()
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const dataGrade = useMemo(() => {
    return isSystem ? getGrade : gradeList
  }, [isSystem,getGrade,gradeList])

  const contextData = useContext(SingleTemplateContext)

  const chosenTemplate = contextData?.chosenTemplate

  // data
  const [tableData, setTableData] = useState(data)
  // filter
  const [filterLevel, setFilterLevel] = useState([] as any[])
  const [filterCreatedBy, setFilterCreatedBy] = useState([] as any[])
  const [filterName, setFilterName] = useState('')
  const [filterSkill, setFilterSkill] = useState([] as any[])
  const [filterTime, setFilterTime] = useState('')
  const [filterTotalQuestions, setFilterTotalQuestions] = useState('')
  // pagination
  const [activePage, setActivePage] = useState(pageIndex)
  const [totalPage, setTotalPage] = useState(total)
  // view
  const [triggerReset, setTriggerReset] = useState(false)
  const [isOpenFilterCollapse, setIsOpenFilterCollapse] = useState(false)
  const [searching, setSearching] = useState(false)

  const submitBtn = useRef<HTMLButtonElement>(null)

  const filterBagde = [
    filterCreatedBy.length !== 0,
    filterLevel.length > 0,
    filterTime ? true : false,
    filterTotalQuestions ? true : false,
    filterSkill.length > 0,
  ].filter((item) => item === true).length

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    })
  }

  const handlePageChange = (page: number) => {
    scrollToTop()
    const formData: any = {}
    const scope = filterCreatedBy.length === 1  ? Number(filterCreatedBy[0]) : 3
    formData['scope'] = scope
    if (filterLevel && filterLevel.length > 0) formData['level'] = [...filterLevel]
    if (filterName) formData['name'] = filterName
    if (filterSkill && filterSkill.length > 0) formData['skill'] = [...filterSkill]
    if (filterTime) formData['time'] = filterTime
    if (filterTotalQuestions) formData['totalQuestions'] = filterTotalQuestions

    handleSubmit(false, page, { ...formData })
  }
  const handleCreatedByChange = (val: any[]) => {
    setFilterCreatedBy([...val])
    // handleSubmit(1, { scope: [...val] })
  }
  const handleLevelChange = (val: any) => {
    let value: any[] = []
    if (val) {
      value = [val]
    }
    setFilterLevel(value)
    // handleSubmit(1, { level: [...value] })
  }
  const handleNameChange = (val: string) => setFilterName(val)
  const handleNameSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      handleSubmit(false, 1, { name: val })
      setSearching(true)
      e.target.blur()
    }
  }
  const handleSkillChange = (val: any[]) => {
    setFilterSkill(val)
    // handleSubmit(1, { skill: [...val] })
  }
  
  const handleTimeChange = (val: string) => {
    setFilterTime(!isNaN(Number(val)) ? val : '')
  }
  const handleTimeSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      handleSubmit(false, 1, { time: val })
      e.target.blur()
    }
  }
  const handleTotalQuestionsChange = (val: string) => setFilterTotalQuestions(val)
  const handleTotalQuestionsSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      handleSubmit(false, 1, { totalQuestions: val })
      e.target.blur()
    }
  }

  const gradeArray = ["G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8", "G9", "G10", "G11", "G12"]
  const handleNext = async () => {
    if (!chosenTemplate.state?.id) return
    const response: any = await callApi(`/api/templates/${chosenTemplate.state.id}`, 'get')
    const data = response?.data ? templateDetailTransform(response.data) : null
    // console.log('data---- create new unit', data);
    if (data) {
      // const isIncluded = getGrade.some((item) => item.code === data.templateLevelId)
      // console.log('data.templateLevelId-----', data.templateLevelId)
      const isIncluded: boolean = gradeArray.includes((data.templateLevelId).toString())
      chosenTemplate.setState({
        ...data,
        date: [null, null],
        scope: isSystem ? 0 : 1,
        unitType: '0',
        unit_test_type: null,
        is_international_exam: !isIncluded ? 1 : 0,
      })
      nextStep()
    }
  }
  const resetFilterState = () => {
      setFilterLevel([])
      setFilterCreatedBy([])
      setFilterName('')
      setFilterSkill([])
      setFilterTime('')
      setFilterTotalQuestions('')
  }
  const handleReset = () => {
    setTriggerReset(!triggerReset)
    resetFilterState()
    handleSubmit(true, 1)
  }

  const handleSubmit = async (isReset: boolean, page = 1, body: any = null) => {
    // collect data
    const data = !isReset ? {
      name: body?.name || filterName,
      level: body?.level && body.level.length > 0 ? body.level : filterLevel,
      page: page - 1,
      skill: body?.skill && body.skill.length > 0 ? body.skil : filterSkill,
      time: body?.time || filterTime,
      totalQuestions: body?.totalQuestions || filterTotalQuestions,
      scope: body?.scope || (filterCreatedBy.length === 1 ? Number(filterCreatedBy[0]) : 3),
      status: 1
    } : {
      page: page - 1,
      scope: body?.scope || 3,
      status: 1
    }
        // send request
    const response: any = await callApi(paths.api_templates_search, 'post', 'token', data)
    // check
    if (response) {
      setActivePage(page)
      setTotalPage(response?.totalPages || 1)
      setTableData(response?.templates ? response.templates.map(templateListTransformData) : [])
    }
  }

  useEffect(() => {
    setTotalPage(total)
  }, [total])

  const isSearchEmpty = useMemo(() =>{
    return (!tableData || tableData.length === 0) && searching
  }, [tableData, searching])
  const onKeyDownInputNumber = (e:any,val:any)=>{
    const key = e?.key?.toLowerCase();
    if(["e","+","-"].includes(key)){
      e.preventDefault();
      return;
    }
    if(!val && key == "0"){
      e.preventDefault();
      return;
    }
  }
  return (
    <div className={`m-unittest-template-table-data ${className}`} style={style}>
      <div className="m-unittest-template-table-data__content">
        <div className="content__sup" style={{ justifyContent: 'flex-start' }}>
          <InputWithLabel
            className="searchbox"
            label={t('table')['template-name']}
            placeholder={t('common')['placeholder-search-template']}
            triggerReset={triggerReset}
            type="text"
            style={{ width: '45rem', flex: 'unset', zIndex: 6 }}
            onBlur={handleNameChange}
            onKeyUp={handleNameSubmit}
            icon={<img src="/images/icons/ic-search-dark.png" alt="search" />}
          />
          <Whisper
            placement="bottom"
            trigger="hover"
            speaker={<Tooltip>{t('common-whisper')['show-more-filter']}</Tooltip>}
          >
            <Button
              className="__sub-btn"
              data-active={isOpenFilterCollapse}
              style={{ marginBottom: '1.6rem' }}
              onClick={() => setIsOpenFilterCollapse(!isOpenFilterCollapse)}
            >
              <div className="__badge" data-value={filterBagde} data-hidden={filterBagde <= 0}>
                <img src="/images/icons/ic-filter-dark.png" alt="filter" />
              </div>
              <span>{t('common')['filter']}</span>
            </Button>
          </Whisper>
          <Whisper placement="bottom" trigger="hover" speaker={<Tooltip>{t('unit-test')['tooltip-reset']}</Tooltip>}>
            <Button className="reset-btn" onClick={() => {
              handleReset()
              setSearching(false)
            }}>
              <img src="/images/icons/ic-reset-darker.png" alt="reset" />
              <span>{t('common')['reset']}</span>
            </Button>
          </Whisper>
          <Button ref={submitBtn} className="submit-btn" onClick={() => {
            handleSubmit(false, 1)
            setSearching(true)
          }}>
            <img src="/images/icons/ic-search-dark.png" alt="search" />
            <span>{t('common')['search']}</span>
          </Button>
        </div>
        <Collapse isOpened={isOpenFilterCollapse}>
          <div className="content__sub" style={{ marginTop: 0, marginBottom: '1.6rem' }}>
            <MultiSelectPicker
              className="__filter-box"
              data={TEMPLATE_CREATED_BY.map(item => ({...item, display: t('unit-test')[item.key]}))}
              label={t('table')['created-by']}
              menuSize="lg"
              triggerReset={triggerReset}
              style={{ width: 'calc(20% - 0.8rem)', flex: 'unset', zIndex: 5 }}
              onChange={handleCreatedByChange}
            />
            {dataGrade && (
              <SelectFilterPicker
                data={dataGrade}
                isMultiChoice={false}
                inputSearchShow={false}
                onChange={handleLevelChange}
                className={'__filter-box --grade'}
                triggerReset={triggerReset}
                label={t('common')['grade']}
                menuSize="lg"
                style={{ width: 'calc(20% - 0.8rem)', flex: 'unset', zIndex: 4 }}
              ></SelectFilterPicker>
            )}
            <InputWithLabel
              className="__filter-box"
              label={t('common')['time']}
              placeholder={t('common')['time-2']}
              triggerReset={triggerReset}
              type="number"
              style={{ width: 'calc(20% - 0.8rem)', flex: 'unset', zIndex: 3 }}
              onBlur={handleTimeChange}
              onKeyUp={handleTimeSubmit}
              onChange={handleTimeChange}
              min={0}
              onkeyDown={onKeyDownInputNumber}
            />
            <InputWithLabel
              className="__filter-box"
              label={t('common')['total-question']}
              triggerReset={triggerReset}
              type="number"
              style={{ width: 'calc(20% - 0.8rem)', flex: 'unset', zIndex: 2 }}
              onBlur={handleTotalQuestionsChange}
              onKeyUp={handleTotalQuestionsSubmit}
              min={0}
              onkeyDown={onKeyDownInputNumber}
            />
            <MultiSelectPicker
              className="__filter-box"
              data={SKILLS_SELECTIONS}
              label={t('common')['skill']}
              menuSize="lg"
              triggerReset={triggerReset}
              style={{ width: 'calc(20% - 0.8rem)', flex: 'unset', zIndex: 1 }}
              onChange={handleSkillChange}
            />
          </div>
        </Collapse>
      </div>
      {tableData.length > 0 ? (
        <div className="m-unittest-template-table-data__container">
          <div className="__table">
            <table>
              <thead>
                <tr>
                  <th style={{ width: '4.5rem' }}></th>
                  <th style={{ width: '4.5rem', padding: 0 }}>{t('STT')}</th>
                  <th
                    style={{
                      justifyContent: 'flex-start',
                      flex: 1,
                      textAlign: 'left',
                    }}
                  >
                    {t('table')['template-name']}
                  </th>
                  <th style={{ width: '12.5%' }}>{t('common')['grade']} / {t('table')['cefr']}</th>
                  <th style={{ width: '12.5%' }}>{t('common')['time']}</th>
                  <th style={{ width: '12.5%' }}>{t('common')['total-question']}</th>
                  <th style={{ width: '12.5%' }}>{t('common')['total-skill']}</th>
                </tr>
              </thead>
              <tbody>
                {tableData.length > 0 ? (
                  tableData.map((tr, i) => <RowItem key={tr.id} index={i} activePage={activePage} data={tr} />)
                ) : (
                  <tr>
                    <td colSpan={7} style={{ textAlign: 'center' }}>
                      No Data
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      ) : (
        <div className="o-question-drawer__empty" style={{ minHeight: '50vh', paddingTop: 32, paddingBottom: 32 }}>
          <img
            className="__banner"
            src={
              isSearchEmpty ? '/images/collections/clt-emty-result.png' : '/images/collections/clt-emty-template.png'
            }
            alt="banner"
          />
          <p className="__description">
            {isSearchEmpty ? (
              <>{t('no-field-data')['search-empty']}</>
            ) : (
              <>
                {t("no-field-data")['no-template-test']}
                <br />
                {t('unit-test')['pls-create']}
              </>
            )}
          </p>
          {!isSearchEmpty && (
            <Button
              className="__submit"
              onClick={() => router.push('/templates/[templateSlug]', '/templates/create-new-template')}
            >
              {t('unit-test')['create-template']}
            </Button>
          )}
        </div>
      )}
      {}
      <StickyFooter containerStyle={{ width: '100%', justifyContent: 'space-between' }}>
        <div className="m-unittest-template-table-data__ending" style={{ width: '100%' }}>
          <div className="__pagination">
            {tableData.length > 0 && (
              <Pagination
                prev
                next
                ellipsis
                size="md"
                total={totalPage * 10}
                maxButtons={10}
                limit={10}
                activePage={activePage}
                onChangePage={handlePageChange}
              />
            )}
          </div>
          <div className="__direction">
            {/* <Whisper
              placement="top"
              trigger="hover"
              speaker={<Tooltip>Tạo đề thi với một định dạng không có sẵn</Tooltip>}
            >
              <Button
                className="__sub-primary"
                onClick={() => {
                  chosenTemplate.setState({
                    id: null,
                    date: [null, null],
                    name: '',
                    point: null,
                    sections: [],
                    templateLevelId: '',
                    time: null,
                    totalQuestion: null,
                    scope: isSystem ? 0 : 1,
                    unitType: isSystem ? '0' : '1',
                    unit_test_type: null,
                    is_international_exam: 0,
                  })
                  nextStep()
                }}
              >
                <span>Dùng định dạng mới</span>
              </Button>
            </Whisper> */}
            <Whisper placement="top" trigger="hover" speaker={<Tooltip>{t('unit-test')['next-step']} 2</Tooltip>}>
              <div
                style={{
                  cursor: !chosenTemplate.state?.id ? 'not-allowed' : 'auto',
                }}
              >
                <Button
                  style={{
                    pointerEvents: !chosenTemplate.state?.id ? 'none' : 'auto',
                  }}
                  className="__primary"
                  disabled={chosenTemplate.state?.id ? false : true}
                  onClick={chosenTemplate.state?.id && handleNext}
                >
                  <span>{t('common')['next-btn']}</span>
                  <img src="/images/icons/ic-arrow-left-active.png" alt="arrow" />
                </Button>
              </div>
            </Whisper>
          </div>
        </div>
      </StickyFooter>
    </div>
  )
}
