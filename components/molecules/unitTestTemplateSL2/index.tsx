import { FBAudio } from 'components/atoms/fillintheblankAudio'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import { SLParagraph } from 'components/atoms/selectfromlistParagraph'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateSL2 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  const listQuestion = data?.question_text.split('#') || []
  const listAnswer = data?.answers.split('#') || []
  const listCorrect = data?.correct_answers.split('#') || []
  const listImage = data?.image.split('#') || []
  return (
    <div className={`m-unittest-sl1 ${className}`} style={style}>
      <div className="m-unittest-sl1__turorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.question_description || ''}
        />
      </div>
      <div style={{ display: 'flex' }}>
        <div style={{ flex: 1, marginRight: '1rem' }}>
          <div
            style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              height: '100px',
              backgroundColor: '#E6F6FC',
            }}
          >
            <audio controls>
              <source
                src={
                  data?.audio.startsWith('blob')
                    ? data.audio
                    : `/upload/${data?.audio || ''}`
                }
                type="audio/mpeg"
              />
            </audio>
          </div>
        </div>
        <div
          style={{
            flex: 1,
            marginLeft: '1rem',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          {listQuestion.map((m: any, mIndex: number) => (
            <div
              key={mIndex}
              style={{ marginBottom: '1rem' }}
              className="box-question"
            >
              <img
                src={
                  listImage[mIndex].startsWith('blob')
                    ? listImage[mIndex]
                    : `/upload/${listImage[mIndex] || ''}`
                }
                width={300}
              />
              <SLParagraph
                correctOptions={
                  listCorrect[mIndex] ? [listCorrect[mIndex]] : []
                }
                data={listQuestion[mIndex] || ''}
                options={
                  listAnswer[mIndex]
                    ? listAnswer[mIndex]
                        .split('#')
                        .map((item: string) => item.split('*'))
                    : []
                }
              />
            </div>
          ))}
        </div>
      </div>
    </div>
  )
}
