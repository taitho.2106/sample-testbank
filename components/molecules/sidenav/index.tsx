import { useContext } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import Link from 'next/link'
import { Collapse } from 'react-collapse'

import { AppContext, WrapperContext } from 'interfaces/contexts'
import { userInRight } from 'utils'

import { SIDENAV_ITEMS } from '../../../interfaces/constants'
import { DefaultPropsType } from '../../../interfaces/types'
import { SidenavItem } from '../../atoms/sidenavItem'
import useTranslation from "@/hooks/useTranslation";

interface Props extends DefaultPropsType {
  isExpand: boolean
}

export const Sidenav = ({ className = '', isExpand, style }: Props) => {
  const [session] = useSession()

  if (!session) return null

  const user: any = session.user

  return (
    <div
      className={`m-sidenav ${className}`}
      style={{ ...style, overflow: isExpand ? 'hidden' : 'unset' }}
    >
      {SIDENAV_ITEMS && SIDENAV_ITEMS.length > 0 && (
        <>
          <ul className="m-sidenav__list">
            {SIDENAV_ITEMS.filter((item) =>
              userInRight(item.access_role, session),
            ).map((item, i) => (
              <Item key={item.id} data={item} isExpand={isExpand} />
            ))}
          </ul>
        </>
      )}
    </div>
  )
}

const Item = ({ data, isExpand }: any) => {
  const { t } = useTranslation()
  const router = useRouter()

  const [session] = useSession()

  const { globalModal } = useContext(WrapperContext)

  if (!session) return null

  const user: any = session.user

  const { openingCollapseList }: any = useContext(AppContext)

  const checkRegexUrl = (router:any,regexs:RegExp[])=>{
    if(!regexs || regexs.length==0) return false;
    const url = router.asPath;
    for(let i=0; i< regexs.length;i++){
      const found = url.match(regexs[i]);
      if(found) return true;
    }
    return false;
  }
  const checkInCludeActive = () => {
    const pathname = router.asPath
    let check = false
    if (data?.list) {
      data.list.forEach((item: any) => {
        if (
          pathname === item.url ||
          item.baseUrl?.some((m: any) => checkUrl(m, router))||
          checkRegexUrl(router,item.regexUrls)
        ) {
          check = true
          return
        }
      })
    }
    return check
  }

  const checkCollapseExist = () => openingCollapseList.state.includes(data.id)

  const handleToggle = () => {
    const currentCollapseList = openingCollapseList.state
    const checkExist = checkCollapseExist()
    openingCollapseList.setState(
      checkExist
        ? [...currentCollapseList.filter((item: number) => item !== data.id)]
        : [...currentCollapseList, data.id],
    )
  }

  return (
    <li className="list-item">
      <Link href={data?.url === '/' ? '/home' : data?.url || '/'}>
        <a
          style={{
            cursor:
              data?.url === router.pathname && !data?.list
                ? 'default'
                : 'pointer',
          }}
          onClick={(e) => {
            if (data?.url === router.pathname || !data?.url) {
              e.preventDefault()
            }
          }}
        >
          <SidenavItem
            data={data}
            isActive={
              ((!isExpand || !checkCollapseExist()) && checkInCludeActive()) ||
              (data.url === '/'
                ? data.url === router.pathname
                : !data.list && router.pathname.startsWith(data.url))
            }
            isExpand={isExpand}
            isOpen={isExpand && checkCollapseExist() ? true : false}
            onToggle={handleToggle}
          />
        </a>
      </Link>
      {data?.list && data.list.length > 0 && (
        <Collapse isOpened={isExpand && checkCollapseExist() ? true : false}>
          {data.list
            .filter((item: any) => {
              if (userInRight(item.active_role, session)) {
                return item
              } else {
                return false
              }
            })
            .map((item: any) => {
              const fullPath = router.asPath
              return (
                <div
                  key={item.id}
                  className="a-sidenav-item__sub-item"
                  data-active={
                    fullPath === item.url ||
                    item.baseUrl?.some((m: any) => checkUrl(m, router))||
                    checkRegexUrl(router,item.regexUrls)
                  }
                  onClick={(e) => {
                    e.stopPropagation()
                    if (
                      router.asPath.includes('/unit-test/create-new-unittest')
                    ) {
                      globalModal.setState({
                        id: 'confirm-modal',
                        type: 'cancel-unit-test',
                        content: {
                          closeText: t('common')['cancel'],
                          submitText: t('common')['submit-btn'],
                          onSubmit: () => router.push(item.url),
                        },
                      })
                    } else {
                      router.push(item.url)
                    }
                  }}
                >
                  {item.name}
                </div>
              )
            })}
        </Collapse>
      )}
    </li>
  )
}

function checkUrl(path1: any, router: any) {
  const query1 = path1.query ?? {}
  const query2 = router.query ?? {}
  if (path1.url !== router.pathname) return false
  if (!query1.hasOwnProperty('m') && router.query.hasOwnProperty('m'))
    return false
  if (query1.hasOwnProperty('m') && !router.query.hasOwnProperty('m'))
    return false
  if (
    query1.hasOwnProperty('m') &&
    router.query.hasOwnProperty('m') &&
    query1['m'] !== query2['m']
  )
    return false
  return true
}
