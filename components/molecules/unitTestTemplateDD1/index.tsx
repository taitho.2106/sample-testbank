import { DDKeyword } from 'components/atoms/draganddropKeyword'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from '@/hooks/useTranslation'

interface PropsType extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateDD1 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  const {t} = useTranslation()
  const isExam = (data?.question_text ?? '').split('#') || []
  const answerArr = data?.answers?.split('#') || []
  const correctArr = data?.correct_answers.split('#') || []
  const imageArr = data?.image?.split('#') || []
  const fileImage = data?.image_files||[]
  const example:answer[] = []
  const question:answer[] = []
  for(let i = 0; i < isExam.length; i++){
    if(isExam[i] === '*'){
      example.push({
        answer:answerArr[i],
        correct_answers:correctArr[i],
        image:imageArr[i],
        isExam:isExam[i],
        image_files:fileImage[i]
      })
    }else{
      question.push({
        answer:answerArr[i],
        correct_answers:correctArr[i],
        image:imageArr[i],
        isExam:isExam[i],
        image_files:fileImage[i]
      })
    }
  }
  return (
    <div className={`m-unittest-dd1 ${className}`} style={style}>
      <div className="m-unittest-dd1__turorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.parent_question_description || data?.question_description ||''}
        />
      </div>
      <div className="m-unittest-dd1__question">
        <table>
          <thead></thead>
          <tbody>
            {example.map((answerExam: answer, answerIndex: number) => {
              const srcImg = answerExam?.image?.startsWith('blob') ? answerExam?.image : `/upload/${answerExam?.image}`
              return (
                <tr
                  key={answerIndex + 1}
                  id={`${(answerIndex + 1).toString()}`}
                >
                  <td>
                    <span
                      className={`type-format exam_answer`}
                    >
                      {t('common')['example']}
                    </span>
                  </td>
                  <td className={`__image ${answerExam?.image ? "" : "no-image"}`}>
                    <img
                      src={srcImg}
                      alt="answer-image"
                    />
                  </td>
                  <td className={`__content ${answerExam.image ? "" : "full-content"}`}>
                    <DDKeyword
                      answers={answerExam.answer ? answerExam.answer.split('*') : []}
                      correctAnswer={
                        answerExam.correct_answers
                          ? answerExam.correct_answers
                              .replace(/\*/g, ' ')
                              .replace(/%\/%/g, ' / ')
                          : ''
                      }
                      labelDefault={t(t('create-question')['key-word-dd'], [t('common')['example']])}
                    />
                  </td>
                </tr>
              )
            })}
            {question.map((answerQuestion: answer, answerIndex: number) => {
              const srcImg = answerQuestion?.image?.startsWith('blob') ? answerQuestion?.image : `/upload/${answerQuestion?.image}`
              return (
                <tr
                  key={answerIndex + 1}
                  id={`${(answerIndex + 1).toString()}`}
                >
                  <td>
                    <span
                      className={`type-format answer`}
                    >
                      {t('common')['question']}
                    </span>
                  </td>
                  <td className={`__image ${answerQuestion?.image ? "" : "no-image"}`}>
                    <img
                      src={srcImg}
                      alt="answer-image"
                    />
                  </td>
                  <td className={`__content ${answerQuestion.image ? "" : "full-content"}`}>
                    <DDKeyword
                      answers={answerQuestion.answer ? answerQuestion.answer.split('*') : []}
                      correctAnswer={
                        answerQuestion.correct_answers
                          ? answerQuestion.correct_answers
                              .replace(/\*/g, ' ')
                              .replace(/%\/%/g, ' / ')
                          : ''
                      }
                      labelDefault={t(t('create-question')['key-word-dd'], [t('common')['question']])}
                    />
                  </td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </div>
    </div>
  )
}
type answer = {
  answer?: string
  correct_answers?:string
  image?:string
  isExam?:string
  image_files?:any
}
