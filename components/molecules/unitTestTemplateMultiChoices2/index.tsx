import { MultiChoicesAnwersFont } from 'components/atoms/multichoiceAnswersFont'
import { MultiChoicesImage } from 'components/atoms/multichoiceImage'
import { MultiChoicesQuestion } from 'components/atoms/multichoiceQuestion'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import { getTextWithoutFontStyle } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from '@/hooks/useTranslation'

interface Props extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateMultiChoices2 = ({
  className = '',
  data,
  style,
}: Props) => {
  console.log('data----', data)
  const {t} = useTranslation()
  const listQuestionText = data?.question_text?.split('#')
  const listAnswer = data?.answers.split('#')
  const listCorrect = data?.correct_answers.split('#')
  const questionsExample = []
  const answersExample: any = []
  const answerCorrectsExample: any = []
  const questions = []
  const answers: any = []
  const answerCorrects: any = []
  for (let i = 0; i < listQuestionText.length; i++) {
    if (listQuestionText[i].startsWith('*')) {
      questionsExample.push(listQuestionText[i].substring(1))
      answersExample.push(listAnswer[i])
      answerCorrectsExample.push(listCorrect[i])
    } else {
      questions.push(listQuestionText[i])
      answers.push(listAnswer[i])
      answerCorrects.push(listCorrect[i])
    }
  }
  return (
    <div className={`m-unittest-multi-choice-2 ${className}`} style={style}>
      <div className="m-unittest-multi-choice-2__tutorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.parent_question_description || ''}
        />
      </div>
      <div className="m-unittest-multi-choice-2__listquestion">
        <div className="m-unittest-multi-choice-2__left">
          <div className="m-unittest-multi-choice-2__question">
            <div className="m-unittest-multi-choice-2__image">
              <MultiChoicesImage
                data={
                  data?.image.startsWith('blob')
                    ? data?.image
                    : `/upload/${data?.image || ''}`
                }
              />
            </div>
          </div>
        </div>
        <div className="m-unittest-multi-choice-2__right">
          {questionsExample.length > 0 && (
            <div className="content-block-question">
              <div>
              <span className='__title-example'>{t('common')['example']}:</span>
              </div>
              <div>
                {questionsExample.map((question: string, index: number) => {
                  return (
                    <div key={`example-${index}`}>
                      <MultiChoicesQuestion
                        className="question"
                        data={question || ''}
                      />
                      <div className="m-unittest-multi-choice-2__tutorial">
                        {answersExample[index]
                          ?.split('*')
                          .map((item: string, i: number) => {
                            return (
                              <MultiChoicesAnwersFont
                                key={`example-ans-${i}`}
                                radioName={`mc2-${data.id}`}
                                checked={
                                  answerCorrectsExample[index] ===
                                  getTextWithoutFontStyle(item)
                                }
                                className="answers"
                                data={item}
                              />
                            )
                          })}
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          )}
          {questions.length > 0 && (
            <div className="content-block-question">
              <div>
                <span className='__title-question'>{t('common')['question']}:</span>
              </div>
              <div>
                {questions.map((question: string, index: number) => {
                  return (
                    <div key={`question-${index}`}>
                      <MultiChoicesQuestion
                        key={`question-${index}`}
                        className="question"
                        data={question ? `${index + 1}. ${question}` : ''}
                      />
                      <div className="m-unittest-multi-choice-2__tutorial">
                        {answers[index]
                          ?.split('*')
                          .map((item: string, i: number) => (
                            <MultiChoicesAnwersFont
                              key={`question-ans-${i}`}
                              radioName={`mc2-${data.id}`}
                              checked={
                                answerCorrects[index] ===
                                getTextWithoutFontStyle(item)
                              }
                              className="answers"
                              data={item}
                            />
                          ))}
                      </div>
                    </div>
                  )
                })}
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  )
}
