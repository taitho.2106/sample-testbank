import { MultiChoicesAnwers } from 'components/atoms/multichoiceAnswers'
import { MultiChoicesQuestion } from 'components/atoms/multichoiceQuestion'
import { Fragment } from 'react'
import { convertStrToHtml, getTextWithoutFontStyle } from 'utils/string'
import { DefaultPropsType } from '../../../interfaces/types'
import { MultiChoicesGeneralQuestion } from "../../atoms/multichoiceGeneralQuestion";
import useTranslation from '@/hooks/useTranslation'

interface PropsType extends DefaultPropsType {
  data: any
}

export const TemplateMIX1 = ({ className = '', data, style }: PropsType) => {
  const {t} = useTranslation()
  const descriptionList = data.question_description.split('[]')
  const questionList = data.question_text.split('[]')
  const answerList = data.answers.split('[]')
  const correctAnswerList = data.correct_answers.split('[]')
  const correctListTF = correctAnswerList[0]
    .split('#')
    .map((item: 'T' | 'F') => item === 'T')

  const questionListMC = questionList[1]
    .split('#')

  const answerListMC = answerList[1]
    .split('#')
    .map((item: string) => item.split('*'))

  const correctAnswerListMC = correctAnswerList[1].split('#')

  return (
    <div className={`${className}`} style={style}>
      <p className={`m-template-true-false__instruction`}>
        {data?.parent_question_description || ''}
      </p>
      <div style={{ display: 'flex', flex: 1 }}>
        <div style={{ flex: 1, marginRight: '0.8rem' }}>
          <p
            className={`m-template-true-false__question-container__question__paragraph-mode`}
          >
            <MultiChoicesGeneralQuestion
                className="question"
                data={data?.parent_question_text || ''}
            />
          </p>
        </div>
        <div style={{ flex: 1, marginLeft: '0.8rem' }}>
          <div className="m-template-true-false__question-container__answer">
            <p
              className={`m-template-true-false__instruction`}
              style={{ marginLeft: 0 }}
            >
              {descriptionList[0]}
            </p>
            <ul>
              <li>
                <span>{t('create-question')['answer-key']}</span>
                <span>True/False</span>
              </li>
              {answerList[0].split('#').map((item: string, i: number) => (
                <li key={i}>
                  <span
                      dangerouslySetInnerHTML={{
                        __html: convertStrToHtml(item.trim()),
                      }}
                  ></span>
                  <span>
                    <img
                      src={
                        correctListTF[i]
                          ? '/images/icons/ic-true.png'
                          : '/images/icons/ic-false.png'
                      }
                      alt={correctListTF[i] ? 'true' : 'false'}
                    />
                  </span>
                </li>
              ))}
            </ul>
          </div>
          <div>
            <p
              className={`m-template-true-false__instruction`}
              style={{ marginLeft: 0 }}
            >
              {descriptionList[1]}
            </p>
            {questionListMC.map((item: string, i: number) => (
              <Fragment key={i}>
                {i !== 0 && <hr />}
                <div className="m-unittest-multi-choice-5__question">
                  <MultiChoicesQuestion
                    className="question"
                    data={item ? `${i + 1}. ${item}` : ''}
                  />
                </div>
                <div className="m-unittest-multi-choice-5__answers">
                  {answerListMC[i] &&
                    answerListMC[i].map((answer: any) => (
                      <MultiChoicesAnwers
                        key={i}
                        radioName={`mc1-${data.id}`}
                        checked={
                          correctAnswerListMC[i].trim() === getTextWithoutFontStyle(answer.trim())
                        }
                        className="answers"
                        data={
                          <span
                            dangerouslySetInnerHTML={{
                              __html: convertStrToHtml(answer),
                            }}
                          ></span>
                        }
                      />
                    ))}
                </div>
              </Fragment>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
