import { useContext, useMemo } from "react";

import { useSession } from 'next-auth/client'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { FaEye, FaPencilAlt, FaTrashAlt, FaFilePdf, FaSyncAlt, FaExchangeAlt } from 'react-icons/fa'
import { GoKebabVertical } from 'react-icons/go'
import { Whisper, Popover, Button, Loader, toaster } from 'rsuite'

import ExportAudio from "@/assets/icons/export-audio.svg"
import IconCopy from '@/assets/icons/icon-copy.svg'
import IconEdit from '@/assets/icons/icon-edit.svg'
import PremiumIcon from '@/assets/icons/premium.svg'
import PrintIcon from "@/assets/icons/print-icon.svg";
import { UNIT_TEST_GROUP } from "@/constants/index";
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'
import useExportAudio from "@/hooks/useExportAudio";
import useExportWord from '@/hooks/useExportWord'
import { usePrintPDF } from "@/hooks/usePrintPDF";
import { checkAccessPackageLevel } from '@/utils/user'
import { paths } from 'api/paths'
import { AlertNoti } from 'components/atoms/alertNoti'
import useGrade from 'hooks/useGrade'
import useNoti from 'hooks/useNoti'
import useSeries from 'hooks/useSeries'
import { paths as urlPaths} from 'interfaces/constants'
import { USER_ROLES } from 'interfaces/constants'
import { WrapperContext } from 'interfaces/contexts'
import { SKILLS_SELECTIONS, typeUnitType, UNIT_TYPE } from "interfaces/struct";
import { DefaultPropsType, StructType } from 'interfaces/types'
import api from 'lib/api'
import { enabledExportUnitTest, userInRight } from 'utils'

import PopoverUpgradeContent from './PopoverUpgradeContent'
import useTranslation from "@/hooks/useTranslation";

interface Props extends DefaultPropsType {
  currentPage: number
  data: any[]
  isLoading?: boolean
  setData?: any
  onDelete?: (id: number) => void
}

const skillThumbnails: any = {
  GR: '/images/collections/clt-grammar-green.png',
  LI: '/images/collections/clt-listening-green.png',
  PR: '/images/collections/clt-pronunciation-green.png',
  RE: '/images/collections/clt-reading-green.png',
  SP: '/images/collections/clt-speaking-green.png',
  US: '/images/collections/clt-use-of-english-green.png',
  VO: '/images/collections/clt-vocab-green.png',
  WR: '/images/collections/clt-writing-green.png',
}
const imagePrivacy:any = {
  private:'/images/icons/ic-private.png',
  public:'/images/icons/ic-public.png'
}

export const UnitTestTable = ({
  className = '',
  currentPage,
  data,
  isLoading = false,
  style,
  setData,
  onDelete,
}: Props) => {
  const router = useRouter()
  const [session] = useSession()
  const { t, locale, subPath } = useTranslation();
  const { getGrade } = useGrade()
  const { getSeries } = useSeries()
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const isAdmin = userInRight([], session)
  const isStudent = userInRight([-1, USER_ROLES.Student], session)
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
  const m = router.query?.m
  const isEditableDatetime = m === 'mine' ? !isSystem : m === 'teacher' ? isAdmin : isSystem
  const { namePackage } = useCurrentUserPackage();

  const user: any = session.user
  const isMTeacher = m === 'teacher' && user?.is_admin === 1
  const isShowPrivacy = user?.is_admin == 1 && !router.query.m
  const { getNoti } = useNoti()
  const { handleExportWord } = useExportWord()
  const { handleExportAudio } = useExportAudio()
  const { handlePrintUnitTest, printPDFComponents } = usePrintPDF()
  const { globalModal } = useContext(WrapperContext)

  const allGradeCode = getGrade.map((item: any) => item.code)
  const allSeriesCode = getSeries.map((item: any) => item.code)
  const listCodeSeriesSGK = getSeries.filter(series => series.group === UNIT_TEST_GROUP.TEXTBOOK).map(s => s.code)
  const seriesIdSGK = new Set(listCodeSeriesSGK)

  const checkCustomRight = () => {
    let check: any = null
    switch (router.query?.m) {
      case 'mine':
        check = [USER_ROLES.Teacher]
        break
      case 'teacher':
        check = []
        break
      default:
        check = [USER_ROLES.Operator]
        break
    }
    return userInRight(check, session)
  }

  const handleCopyURL = (id: number) => {
    const userId = user?.id
    const protocol = window.location.protocol
    const hostname = window.location.hostname
    const port = hostname === 'localhost' ? `:${window.location.port}` : ''
    const url = `${protocol}//${hostname}${port}${subPath}/practice-test/${userId}===${id}`
    navigator.clipboard.writeText(url)
    getNoti('success', 'topEnd', t('common-get-noti')['copy-url'])
  }

  const handleDeleteUniTtest = (id: number) => {
    globalModal.setState({
      id: 'confirm-modal',
      type: 'delete-unit-test',
      content: {
        closeText: t('common')['cancel'],
        submitText: t('common')['submit-btn'],
        onSubmit: () => onDelete(id),
      },
    })
  }

  const getSkills = (arr: any[]) => {
    const skillArr: any[] = []
    arr.forEach((parents) => {
      if (Array.isArray(parents?.name) && parents?.name && parents.name.length > 0) {
        parents.name.forEach((children: any) => {
          if (!skillArr.includes(children)) skillArr.push(children)
        })
      }
    })
    return skillArr
  }
  const handleOnSubmit = (id: any) => {
    router.push(`/unit-test/${id}?mode=edit`)
  }
  const handleCheckSeriesID = async (unitTestId: any) => {
    // console.log('check check', unitTestId)
    const response = await fetch(`/api/integrate-exam/unit-test/${unitTestId}`)
    const result = await response.json()
    if (result.code === 0) {
      const key = toaster.push(<AlertNoti type={'error'} message={result.message} />, {
        placement: 'topCenter',
      })
      setTimeout(() => toaster.remove(key), 2000)
    } else {
      if (
        result.data[0].series_id != 1 &&
        !allGradeCode.includes(result.data[0].template_level_id) &&
        !allSeriesCode.includes(result.data[0].series_id)
      ) {
        globalModal.setState({
          id: 'confirm-modal-integrate-exam',
          type: 'create-integrate-fail-for-grade-and-series',
          content: {
            closeText: t('common')['cancel'],
            submitText: t('table-action-popover')['edit-unit-test'],
            onSubmit: () => {
              handleOnSubmit(unitTestId)
            },
          },
        })
      } else if (
        result.data[0].series_id == 1 ||
        !result.data[0].series_id ||
        !allSeriesCode.includes(result.data[0].series_id)
      ) {
        globalModal.setState({
          id: 'confirm-modal-integrate-exam',
          type: 'create-integrate-fail',
          content: {
            closeText: t('common')['cancel'],
            submitText: t('table-action-popover')['edit-unit-test'],
            onSubmit: () => {
              handleOnSubmit(unitTestId)
            },
          },
          // color:{
          //   colorClose:'#738396',
          //   colorSubmit:'#D0222D'
          // }
        })
      } else if (!allGradeCode.includes(result.data[0].template_level_id)) {
        globalModal.setState({
          id: 'confirm-modal-integrate-exam',
          type: 'create-integrate-fail-for-grade',
          content: {
            closeText: t('common')['cancel'],
            submitText: t('table-action-popover')['edit-unit-test'],
            onSubmit: () => {
              handleOnSubmit(unitTestId)
            },
          },
        })
      } else {
        router.push(`/integrate-exam/${unitTestId}?mode=create`)
      }
    }
  }
  const createUnitTestSystem = async (id: number) => {
    const response = await fetch(`${paths.api_unit_test_create_from_teacher}?id=${id}`)
    const result = await response.json()
    if (result.code === 0) {
      const key = toaster.push(<AlertNoti type={'error'} message={result.message} />, {
        placement: 'topEnd',
      })
      setTimeout(() => toaster.remove(key), 2000)
    } else {
      const key = toaster.push(<AlertNoti type={'success'} message={t('common-get-noti')['create-unit-test-succeed']} />, {
        placement: 'topEnd',
      })
      setTimeout(() => toaster.remove(key), 2000)
      router.push('/unit-test')
    }
  }
  const syncUnitTestSystem = async (id: number) => {
    const response = await fetch(`${paths.api_unit_test_sync_from_teacher}?id=${id}`)
    const result = await response.json()
    if (result.code === 0) {
      const key = toaster.push(<AlertNoti type={'error'} message={result.message} />, {
        placement: 'topEnd',
      })
      setTimeout(() => toaster.remove(key), 2000)
    } else {
      const key = toaster.push(<AlertNoti type={'success'} message={t('common-get-noti')['update-unit-test-succeed']} />, {
        placement: 'topEnd',
      })
      setTimeout(() => toaster.remove(key), 2000)
      router.push(`/unit-test/${result.id}?mode=view`)
    }
  }
  const updatePrivacy = async (id: number, privacyBefore: any) => {
    const formData: any = { id: id }
    let privacy = null
    if (privacyBefore != 1) {
      // public
      formData.privacy = 1 //private
      privacy = 1
    }
    const response = await api.post(`${paths.api_unit_test_update_privacy}`, formData)
    if (response.status == 200) {
      const updateIndex = data.findIndex((item) => item.id === id)
      if (updateIndex === -1) return
      const newList = data
      const updatedItem = newList[updateIndex]
      newList[updateIndex] = {
        ...updatedItem,
        privacy: privacy,
      }
      setData([...newList])

      const key = toaster.push(<AlertNoti type={'success'} message={t('common-get-noti')['update-privacy-succeed']} />, {
        placement: 'topEnd',
      })
      setTimeout(() => toaster.remove(key), 2000)
    } else {
      const key = toaster.push(<AlertNoti type={'error'} message={t('common-get-noti')['update-privacy-failed']} />, {
        placement: 'topEnd',
      })
      setTimeout(() => toaster.remove(key), 2000)
    }
  }

  const checkRequireUpgradePackage = (unitTest: any) => {
    if (isSystem) {
        return false;
    }
    if( router.query?.m === 'mine') {
      return false
    }
    const { package_level } = unitTest || {};
    return !checkAccessPackageLevel(namePackage.code, package_level);
  }

  const hasRequireUpgrade = useMemo(() => data.some(checkRequireUpgradePackage), [ data ]);

  const handleRowClick = (id: number) => {
    isStudent ?
        router.push(`${urlPaths.student_unit_test_result}?unitTestId=${id}`)
        :
        router.push(`/unit-test/${id}?mode=view${router.query.m ? `&m=${router.query.m}` : ''}`)
  }

  return (
    <div className={`m-unit-test-table ${className}`} style={style}>
      {isLoading && <Loader backdrop content="loading..." vertical />}
      <div className="__table" style={{ opacity: isLoading ? 0 : 1 }}>
        <table>
          <thead className={hasRequireUpgrade ? 'require-upgrade' : ''}>
            <tr>
              <th>{t('STT')}</th>
              <th>{t('common')['unit-test-name']}</th>
              <th>{t('common')['unit-type']}</th>
              <th>{t('common')['grade']}</th>
              <th>{t('common')['time']}</th>
              <th>{t('common')['total-question']}</th>
              <th>{t('common')['total-skill']}</th>
              {/* <th>THỜI GIAN BẮT ĐẦU</th>
              <th>THỜI GIAN KẾT THÚC</th> */}
              <th style={isMTeacher ? {} : { display: 'none' }}>{t('table')['created-by']}</th>
              <th style={isShowPrivacy ? {} : { display: 'none' }}>{t('common')['private']}</th>
              <th></th>
            </tr>
          </thead>
          <tbody className={hasRequireUpgrade ? 'require-upgrade' : ''}>
            {
              data?.map((unitTest: any, index: number) => {
                const isRequireUpgrade = checkRequireUpgradePackage(unitTest);
                const isUnitTestForSeriesSGK = seriesIdSGK.has(unitTest.series_id)
                const isExportPDF = isTeacher && isUnitTestForSeriesSGK

                return (
                    <tr 
                      key={`unit-test_${index}`} 
                      className={isRequireUpgrade ? 'require-upgrade-row' : ''}
                      onClick={() => handleRowClick(unitTest.id)}
                    >
                        <td>{index + 1 + (currentPage - 1) * 10}</td>
                        <td title={unitTest?.name}>
                            <div className="__elipsis">{unitTest?.name || '---'}</div>
                        </td>
                        <td
                            title={
                            t('unit-type')[UNIT_TYPE.find((find: typeUnitType) => find?.code === `${unitTest?.unit_type}`)?.key] ||
                            unitTest?.unit_type
                            }
                        >
                            <div className="__elipsis">
                            {t('unit-type')[UNIT_TYPE.find((find: typeUnitType) => find?.code === `${unitTest?.unit_type}`)?.key] ||
                                unitTest?.unit_type ||
                                '---'}
                            </div>
                        </td>
                        <td
                            title={
                            getGrade.length > 0 ?
                            getGrade.find((find: StructType) => find?.code === unitTest?.template_level_id)?.display : ''
                            }
                        >
                            <div className="__elipsis">
                            {(getGrade.length > 0 &&
                                getGrade.find((find: StructType) => find?.code === unitTest?.template_level_id)?.display) ||
                                '---'}
                            </div>
                        </td>
                        <td title={unitTest?.time}>
                            <div className="__elipsis">{unitTest?.time || '---'}</div>
                        </td>
                        <td title={unitTest?.total_question}>
                            <div className="__elipsis">{unitTest?.total_question || '---'}</div>
                        </td>
                        <td>
                            <Whisper
                            placement={'auto'}
                            trigger={isRequireUpgrade ? 'none' : 'hover'}
                            container={() => document.querySelector('.t-wrapper__main')}
                            speaker={
                                <Popover>
                                <div className="m-unittest-template-table-data__section-popover">
                                    {getSkills(unitTest.sections).map((item: any, i: number) => (
                                    <div key={i} className="section-item">
                                        <img src={skillThumbnails[item]} alt={item} />
                                        <span>{SKILLS_SELECTIONS.find((skill:StructType) => skill.code === item)?.display || item}</span>
                                    </div>
                                    ))}
                                </div>
                                </Popover>
                            }
                            >
                                <span onClick={(e) => e.stopPropagation()} className="skill-col">
                                    <span>{getSkills(unitTest.sections).length || 0}</span>
                                    <sup>*</sup>
                                </span>
                            </Whisper>
                        </td>
                        <td title={unitTest?.user_name} style={isMTeacher ? {} : { display: 'none' }}>
                            <div className="__elipsis">{unitTest?.user_name || '---'}</div>
                        </td>
                        <td style={isShowPrivacy? {} : { display: 'none' }} className="privacy-col">
                            <img height={18} alt="img" src={unitTest?.privacy==1?imagePrivacy.private:imagePrivacy.public}></img>
                        </td>
                        <td>
                          {
                            isStudent ?
                                <></>
                                :
                               isRequireUpgrade ? (
                                      <Whisper
                                          placement="auto"
                                          trigger="click"
                                          speaker={<Popover><PopoverUpgradeContent /></Popover>}
                                      >
                                        <button className="btn-upgrade-package"><PremiumIcon />{t('common')['usage']}</button>
                                      </Whisper>
                                  ) : (
                                      <div
                                          className="__action-group"
                                          onClick={(e) => {
                                            e.stopPropagation()
                                            e.preventDefault()
                                          }}
                                      >
                                        <Whisper
                                            placement={'autoHorizontal'}
                                            preventOverflow
                                            trigger="click"
                                            container={() => document.querySelector('.m-unit-test-table')}
                                            speaker={
                                              <Popover>
                                                <div className="table-action-popover">
                                                  <div className="__item" onClick={() => handleCopyURL(unitTest?.id)}>
                                                    <IconCopy />
                                                    <span>{t('table-action-popover')['copy-url']}</span>
                                                  </div>
                                                  {checkCustomRight() && (
                                                      <Link
                                                          as={`/unit-test/${unitTest.id}?mode=edit${
                                                              router.query.m ? `&m=${router.query.m}` : ''
                                                          }`}
                                                          href={`/unit-test/[templateSlug]?mode=edit${
                                                              router.query.m ? `&m=${router.query.m}` : ''
                                                          }`}
                                                      >
                                                        <a className="__item">
                                                          <FaPencilAlt />
                                                          <span>{t('table-action-popover')['edit-unit-test']}</span>
                                                        </a>
                                                      </Link>
                                                  )}
                                                  <Link
                                                      as={`/unit-test/${unitTest.id}?mode=view${
                                                          router.query.m ? `&m=${router.query.m}` : ''
                                                      }`}
                                                      href={`/unit-test/[templateSlug]?mode=view${
                                                          router.query.m ? `&m=${router.query.m}` : ''
                                                      }`}
                                                  >
                                                    <a className="__item">
                                                      <FaEye />
                                                      <span>{t('table-action-popover')['view-detail-alt']}</span>
                                                    </a>
                                                  </Link>
                                                  {enabledExportUnitTest(user, unitTest) && (
                                                    <a
                                                      className="__item"
                                                      onClick={() => handleExportWord(unitTest.id)}
                                                    >
                                                      <FaFilePdf />
                                                      <span>{t('table-action-popover')['export-word']}</span>
                                                    </a>
                                                  )}
                                                  {checkCustomRight() && !isMTeacher && (
                                                      <div
                                                          className="__item"
                                                          onClick={() => handleCheckSeriesID(unitTest.id)}
                                                          style={isSystem || isAdmin ? {} : { display: 'none' }}
                                                      >
                                                        <FaPencilAlt />
                                                        <span>{t('table-action-popover')['integration']}</span>
                                                      </div>
                                                  )}
                                                  {isMTeacher && (
                                                      <>
                                                        <div className="__item" onClick={() => createUnitTestSystem(unitTest.id)}>
                                                          <IconEdit />
                                                          <span>{t('table-action-popover')['create-unit-test-system']}</span>
                                                        </div>
                                                        <div className="__item" onClick={() => syncUnitTestSystem(unitTest.id)}>
                                                          <FaSyncAlt />
                                                          <span style={{ lineHeight: '1.2' }}>{t('table-action-popover')['sync-unit-test-system']}</span>
                                                        </div>
                                                      </>
                                                  )}
                                                  {isAdmin && !isMTeacher && (
                                                      <div
                                                          className="__item"
                                                          onClick={() => updatePrivacy(unitTest.id, unitTest.privacy)}
                                                      >
                                                        <FaExchangeAlt />
                                                        <span style={{lineHeight:"1.2"}}>{t('table-action-popover')['change-privacy']}</span>
                                                      </div>
                                                  )}
                                                  {checkCustomRight() && (
                                                      <div className="__item" onClick={() => handleDeleteUniTtest(unitTest.id)}>
                                                        <FaTrashAlt />
                                                        <span>{t('table-action-popover')['delete-unit-test']}</span>
                                                      </div>
                                                  )}
                                                  {isExportPDF && (
                                                    <>
                                                      <div
                                                        className="__item"
                                                        onClick={() => handlePrintUnitTest(unitTest.id)}
                                                      >
                                                        <PrintIcon />
                                                        <span>{t('table-action-popover')['print-pdf']}</span>
                                                      </div>
                                                      <div
                                                        className="__item"
                                                        onClick={() => handleExportAudio(unitTest.id)}
                                                      >
                                                        <ExportAudio />
                                                        <span>{t('table-action-popover')['export-audio']}</span>
                                                      </div>
                                                    </>
                                                  )}
                                                </div>
                                              </Popover>
                                            }
                                        >
                                          <Button className="__action-btn">
                                            <GoKebabVertical color="#6262BC" />
                                          </Button>
                                        </Whisper>
                                      </div>
                                  )
                          }
                        </td>
                    </tr>
                );
            })}
          </tbody>
        </table>
      </div>
      {printPDFComponents}
    </div>
  )
}
