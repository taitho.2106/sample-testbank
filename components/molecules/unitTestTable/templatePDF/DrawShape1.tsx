import classNames from 'classnames'

import { COLOR_SHAPE } from '@/interfaces/constants';
import { QuestionDataType } from '@/interfaces/types'

import { Description } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const DrawShape1 = ({
    question,
    index
}: QuestionProps) => {
    const listAnswer = (question?.answers as string).split('#') ?? []
    const listShape = listAnswer.map((item) => {
        const listPropertyItem = item.split('*');
        const colorName = COLOR_SHAPE.find((item: { color: string; display: string }) => item?.color === listPropertyItem[2])?.display;
        const imageUrl = `/images/shapes/default-${listPropertyItem[0].startsWith("ex|") ? `ex-${listPropertyItem[1]}-${colorName}` : 'location'}.png`;

        return {
            color: colorName,
            image: imageUrl,
        };
    })

    let indexAns = -1

    return (
        <div className={classNames(styles.boxQuestion, styles.PreventPageBreak)}>
            <Description question={question} />
            <div
                className={classNames(styles.drawShapeGrid, {
                    [styles.default]: listShape.length < 4
                })}
            >
                {listShape.map((shapeItem: any, sIndex: number) => {
                    const isExample = shapeItem.image.includes('ex')
                    if (!isExample) {
                        indexAns++
                    }
                    return (
                        <div
                            key={sIndex}
                            className={styles.drawItem}
                        >
                            <img src={shapeItem.image} alt='' />
                            <span className={classNames(styles.questionNumber, {
                                [styles.isExample]: isExample
                            })}>
                                {isExample ? 'Example' : index + indexAns}
                            </span>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default DrawShape1