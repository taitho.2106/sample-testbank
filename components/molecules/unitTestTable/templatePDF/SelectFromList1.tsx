import { useRef } from 'react';

import { QuestionDataType } from '@/interfaces/types'

import { Description, RenderFBParagraphStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};
const SelectFromList1 = ({
    question,
    index
}: QuestionProps) => {
    const groupKey = useRef(Math.random()).current
    const listAnswers = question.answers
        .split('#')
        .map((m: string) => m.split('*'))
    const listCorrect = question.correct_answers.split("#") || []
    const exampleIndex = listCorrect.findIndex((item: any) => item.startsWith('*'))
    if (exampleIndex != -1) {
        listAnswers.splice(exampleIndex, 1)
    }

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            <div style={{ marginLeft: '15px' }}>
                <RenderFBParagraphStyle
                    answer={question?.correct_answers || ''}
                    data={question?.question_text}
                    answerId={index}
                />
                {listAnswers.map((answers: string[], answersIndex: number) => (
                    <div
                        key={`${groupKey}_${answersIndex}`}
                        className={styles.boxAnswers}
                    >
                        <div style={{ marginRight: '10px' }}>
                            {`(${index + answersIndex})`}
                        </div>
                        {answers.map((answer: string, answerIndex: number) => (
                            <div
                                key={`${groupKey}_${answerIndex}`}
                                className={styles.answer}
                            >
                                {`${String.fromCharCode(65 + answerIndex)}. ${answer}`}
                            </div>
                        ))}
                    </div>
                ))}
            </div>
        </div>
    )
}

export default SelectFromList1