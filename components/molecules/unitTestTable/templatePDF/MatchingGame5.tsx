import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'

import { Description, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MatchingGameQuestion5 = ({
    question,
    index
}: QuestionProps) => {
    const listAnswer: string[][] = question.answers
        .split('#')
        .map((m: string) => m.split('*'))
    const listImage: string[] = question.image.split('#') ?? []
    const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length)
    let indexAns = -1

    return (
        <div className={classNames(styles.boxQuestion, styles.preventPageBreak)}>
            <Description question={question} />
            {Array.from({ length: maxLength }, (_, mIndex: number) => {
                const isExample = listAnswer[2] && listAnswer[2][mIndex] == 'ex'
                if (!isExample) {
                    indexAns++
                }                                
                return (
                    <div key={mIndex}
                        className={classNames(styles.matchingRowFormat, styles.preventPageBreak)}
                    >
                        <span className={styles.textIndex} >
                            {isExample && (
                                <div
                                    style={{
                                        fontWeight: 'bold',
                                        textDecoration: 'underline'
                                    }}
                                >
                                    Example:
                                </div>
                            )}
                            {!isExample && listAnswer[0][mIndex] && `${index + indexAns}. `}
                        </span>
                        <div className={styles.boxImage}>
                            <img
                                src={`/upload/${listImage[mIndex]}`}
                                alt=''
                            />
                        </div>
                        <div className={styles.matchingSection}>
                            <div className={styles.leftData}>
                                {listAnswer[0][mIndex] && (
                                    <>
                                        _<RenderTextHasStyle data={listAnswer[0][mIndex]} />
                                    </>
                                )
                                }
                            </div>
                            <div className={styles.lineMatching}>
                                {isExample && (
                                    <>
                                        ● <img src={`/images/icons/line-export.png`} alt='' /> ●
                                    </>
                                )}
                                {!isExample && listAnswer[0][mIndex] && (
                                    <>
                                        ● <div style={{ width: '170px' }}></div> ●
                                    </>
                                )}
                                {!listAnswer[0][mIndex] && (
                                    <>
                                        <div style={{ width: '170px' }}></div> ●
                                    </>
                                )}
                            </div>
                            <div className={styles.rightData}>
                                {listAnswer[1][mIndex] && (
                                    <>
                                        {String.fromCharCode(65 + mIndex)}. <RenderTextHasStyle data={listAnswer[1][mIndex]} />
                                    </>
                                )}
                            </div>
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default MatchingGameQuestion5