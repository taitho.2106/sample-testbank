import { useRef } from 'react';

import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'

import { Description, RenderFBParagraphStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};
const SelectFromList2 = ({
    question,
    index
}: QuestionProps) => {
    const listImage = question.image.split('#')
    const listQuestionText: string[] = question.question_text.split('#')
    const groupKey = useRef(Math.random()).current
    const listAnswers = question.answers
        .split('#')
        .map((m: string) => m.split('*'))

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {listQuestionText.map((qText, mIndex) => (
                <div key={`${groupKey}_${mIndex}`} style={{ marginTop: '25px' }}>
                    <div className={classNames(styles.boxImage, styles.preventPageBreak)}>
                        <img src={`/upload/${listImage[mIndex]}`} alt='' />
                    </div>
                    <div
                        className={styles.question}
                        style={{
                            marginLeft: 'unset'
                        }}
                    >
                        <RenderFBParagraphStyle
                            answer={question.correct_answers}
                            data={qText}
                            answerId={index + mIndex}
                        />
                    </div>
                    <div className={styles.boxAnswers}>
                        {listAnswers[mIndex].map((answer: any, aIndex: number) => (
                            <div
                                key={`${groupKey}_${aIndex}`}
                                className={styles.answer}
                            >
                                {`${String.fromCharCode(65 + aIndex)}. ${answer}`}
                            </div>
                        ))}
                    </div>
                </div>
            ))}
        </div>
    )
}

export default SelectFromList2