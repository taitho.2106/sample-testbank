import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types';

import { Description } from '../TemplateExportPDF';
import styles from '../TemplateExportPDF.module.scss';

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MatchingGameQuestion6 = ({
    question,
    index
}: QuestionProps) => {
    const listAnswer: string[][] = question.answers
        .split('#')
        .map((m: string) => m.split('*'));

    const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length);
    let indexAns = -1
    return (
        <div className={classNames(styles.boxQuestion, styles.preventPageBreak)}>
            <Description question={question} />
            {Array.from({ length: maxLength }, (_, mIndex: number) => {
                const isExample = listAnswer[2] && listAnswer[2][mIndex] === 'ex';
                if (!isExample) {
                    indexAns++
                }        
                return (
                    <div key={mIndex} className={classNames(styles.matchingPicture, styles.preventPageBreak)}>
                        <span className={styles.textIndex}>
                            {isExample && (
                                <div
                                    style={{
                                        fontWeight: 'bold',
                                        textDecoration: 'underline'
                                    }}
                                >
                                    Example:
                                </div>
                            )}
                            {!isExample && listAnswer[0][mIndex] && `${index + indexAns}. `}
                        </span>
                        <div className={styles.leftImage}>
                            {listAnswer[0][mIndex] && (
                                <img src={`/upload/${listAnswer[0][mIndex]}`} alt='' />
                            )}
                        </div>
                        <div className={styles.lineMatching}>
                            {isExample && (
                                <>
                                    ● <img src={`/images/icons/line-export.png`} alt='' /> ●
                                </>
                            )}
                            {!isExample && listAnswer[0][mIndex] && (
                                <>
                                    ● <div style={{ width: '240px' }}></div> ●
                                </>
                            )}
                            {!listAnswer[0][mIndex] && (
                                <>
                                    <div style={{ width: '240px' }}></div> ●
                                </>
                            )}
                        </div>
                        <div className={styles.rightImage}>
                            {listAnswer[1][mIndex] && (
                                <>
                                    {String.fromCharCode(65 + mIndex)}.
                                    <img src={`/upload/${listAnswer[1][mIndex]}`} alt='' />
                                </>
                            )}
                        </div>
                    </div>
                );
            })}
        </div>
    );
};

export default MatchingGameQuestion6