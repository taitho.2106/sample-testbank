import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'

import { Description, ReadingScript, RenderFBParagraphStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};
const FillInBlank = ({ 
    question,
    index
}: QuestionProps) => {
    const imageUrl = `/upload/${question.image}`

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {question.parent_question_text && (
                <ReadingScript script={question.parent_question_text} />
            )}
            {question.image &&
                <div className={classNames(styles.boxImage, styles.preventPageBreak)}>
                    <img alt="" src={imageUrl} />
                </div>
            }
            <RenderFBParagraphStyle
                answer={question?.correct_answers || ''}
                data={question?.question_text}
                answerId={index}
            />
        </div>
    )
}

export default FillInBlank