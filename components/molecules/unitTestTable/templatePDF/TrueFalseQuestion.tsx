import { useRef } from 'react';

import { QuestionDataType } from '@/interfaces/types'

import { Description, ReadingScript, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};
const TrueFalseQuestion = ({
    question,
    index
}: QuestionProps) => {
    const groupKey = useRef(Math.random()).current
    const listAnswer: string[] = question.answers.split('#')

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {question.question_text && (
                <ReadingScript script={question.question_text} />
            )}
            <div className={styles.answerTable}>
                <table>
                    <thead>
                        <tr>
                            <th>Answers</th>
                            <th>True</th>
                            <th>False</th>
                        </tr>
                    </thead>
                    <tbody>
                        {listAnswer.map((answer, answerIndex) => [
                            <tr key={`${groupKey}_${answerIndex}`}>
                                <td>
                                    {index + answerIndex}. <RenderTextHasStyle data={answer} />
                                </td>
                                <td>
                                    <div className={styles.radio}></div>
                                </td>
                                <td>
                                    <div className={styles.radio}></div>
                                </td>
                            </tr>,
                            answerIndex < listAnswer.length - 1 && (
                                <tr
                                    key={`${groupKey}_${answerIndex}_divide`}
                                    className={styles.divide}
                                >
                                    <td colSpan={3}>
                                        <div></div>
                                    </td>
                                </tr>
                            ),
                        ])}
                    </tbody>
                </table>
            </div>
        </div>
    )
}

export default TrueFalseQuestion