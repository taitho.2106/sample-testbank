import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'

import { Description, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MatchingGameQuestion4 = ({
    question,
    index
}: QuestionProps) => {
    const listAnswer: string[][] = question.answers
        .split('#')
        .map((m: string) => m.split('*'))
    const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length)
    let indexAns = -1

    return (
        <div className={classNames(styles.boxQuestion, styles.preventPageBreak)}>
            <Description question={question} />
            <div className={styles.boxImage}>
                <img src={`/upload/${question.image}`} alt='' />
                <span className={styles.imageNote}>
                    {question.audio_script}
                </span>
            </div>
            <table className={styles.matchingTableHasExample}>
                <tbody>
                    {Array.from({ length: maxLength }, (_, mIndex: number) => {
                        const isExample = listAnswer[2] && listAnswer[2][mIndex] == 'ex'
                        if (!isExample) {
                            indexAns++
                        }
                        return (
                            <tr key={mIndex}>
                                <td>
                                    {isExample && (
                                        <div
                                            style={{
                                                fontWeight: 'bold',
                                                textDecoration: 'underline'
                                            }}
                                        >
                                            Example:
                                        </div>
                                    )}
                                    {!isExample && listAnswer[0][mIndex] && `${index + indexAns}. `}
                                </td>
                                <td>
                                    {listAnswer[0][mIndex] && <RenderTextHasStyle data={listAnswer[0][mIndex]} />}
                                </td>
                                <td>
                                    {isExample && (
                                        <div className={styles.lineMatching}>
                                            ● <img src={`/images/icons/line-export.png`} alt='' /> ●
                                        </div>
                                    )}
                                </td>
                                <td>
                                    {listAnswer[1][mIndex] && (
                                        <>
                                            {String.fromCharCode(65 + mIndex)}. <RenderTextHasStyle data={listAnswer[1][mIndex]} />
                                        </>
                                    )}
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default MatchingGameQuestion4