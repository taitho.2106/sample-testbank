import classNames from 'classnames'

import { QuestionDataType } from '@/interfaces/types'
import { convertStrToHtml } from '@/utils/string'

import { Description, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MultiChoiceGroup9 = ({
    question,
    index
}: QuestionProps) => {
    const listQuestionText: string[] = question?.question_text?.split('#') || []
    const listAnswer: string[] = question?.answers?.split('#') || []
    const listCorrect: string[] = question?.correct_answers?.split('#') || []
    const listImages = question?.image?.split('#') || []

    const questionsExample: any[] = []
    const answersExample: any[] = []
    const answerCorrectsExample: any[] = []
    const imagesExample: any[] = []

    const questions: any[] = []
    const answers: any[] = []
    const images: any[] = []

    listQuestionText.forEach((questionText, i) => {
        if (questionText.startsWith('*')) {
            questionsExample.push(questionText.substring(1));
            answersExample.push(listAnswer[i].split('*'));
            answerCorrectsExample.push(listCorrect[i]);
            imagesExample.push(listImages[i]);
        } else {
            questions.push(questionText);
            answers.push(listAnswer[i].split('*'));
            images.push(listImages[i]);
        }
    });

    const renderQuestionSection = (
        title: string,
        questionsData: string[],
        answersData: string[][],
        imagesData: string[],
    ) => {
        return (
            <>
                <div className={styles.titleType}>{title}:</div>
                {questionsData.map((ques: any, qIndex: number) => {
                    const imageUrl = `/upload/${imagesData[qIndex]}`;
                    return (
                        <div
                            key={`question-${qIndex}`}
                            className={styles.questionRowFormat}
                        >
                            <span className={styles.textIndex}>
                                {title === 'Example' ? '' : `${qIndex + index}.`}
                            </span>
                            <div className={styles.boxImage}>
                                <img alt='' src={imageUrl} />
                            </div>
                            <div className={styles.multiChoiceBox}>
                                <RenderTextHasStyle data={ques} />
                                <div
                                    className={classNames(styles.answersGrid, {
                                        [styles.twoColumns]: answersData[qIndex].length === 4 || answersData[qIndex].length <= 2,
                                    })}
                                >
                                    {answersData[qIndex].map((item: string, i: number) => (
                                        <div
                                            key={i}
                                            className={styles.itemAnswer}
                                        >
                                            <span className={classNames(styles.charCode, {
                                                [styles.correct]: title === 'Example' && item === answerCorrectsExample[0]
                                            })}>
                                                {`${String.fromCharCode(65 + i)}. `}
                                            </span>
                                            <span
                                                dangerouslySetInnerHTML={{
                                                    __html: convertStrToHtml(item),
                                                }}
                                            ></span>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        </div>
                    );
                })}
            </>
        );
    };

    return (
        <div className={classNames(styles.boxQuestion, styles.preventPageBreak)}>
            <Description question={question} />
            {!!questionsExample.length &&
                renderQuestionSection(
                    'Example',
                    questionsExample,
                    answersExample,
                    imagesExample
                )}
            {!!questions.length &&
                renderQuestionSection(
                    questions.length > 1 ? 'Questions' : 'Question',
                    questions,
                    answers,
                    images
                )}
        </div>
    )
}

export default MultiChoiceGroup9