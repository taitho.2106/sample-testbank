import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types';

import { Description } from '../TemplateExportPDF';
import styles from '../TemplateExportPDF.module.scss';

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const DragAndDrop2 = ({ question, index }: QuestionProps) => {
    const listImage = question.image.split('#') ?? []
    const listCorrect = question.correct_answers.split('#') ?? []

    return (
        <div className={classNames(styles.boxQuestion, styles.preventPageBreak)}>
            <Description question={question} />
            <div
                className={classNames(styles.dragDropGrid)}
            >
                {listImage.map((dragItem: string, dIndex: number) => {
                    return (
                        <div
                            key={dIndex}
                            className={styles.dragDropItem}
                        >
                            <img src={`/upload/${dragItem}`} alt='' />
                            <div className={styles.rectangleBox} >
                                {listCorrect[dIndex].startsWith('*') &&
                                    <div className={styles.exampleAnswer}>
                                        {listCorrect[dIndex].substring(1)}
                                    </div>
                                }
                            </div>
                        </div>
                    )
                })}
            </div>
        </div>
    )
}

export default DragAndDrop2