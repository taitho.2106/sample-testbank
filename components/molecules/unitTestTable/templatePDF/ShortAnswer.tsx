import { QuestionDataType } from '@/interfaces/types'

import { Description, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const ShortAnswer = ({
    question,
    index
}: QuestionProps) => {
    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            <div>
                <span>{index}.</span>
                <RenderTextHasStyle data={question?.question_text} />
            </div>
            <p>→{question?.answers.replaceAll(/\%s\%/g, '___________')}
            </p>
        </div>
    )
}

export default ShortAnswer