import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types';

import { Description } from '../TemplateExportPDF';
import styles from '../TemplateExportPDF.module.scss';

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const DrawLine = ({ question, index }: QuestionProps) => {
    const listImage = question.image.split("*") || []
    const mainDrawImage = listImage[1]
    const imageUrl = `/upload/${mainDrawImage}`
    const listCorrect = question.correct_answers.split('#') || [];
    const indexExample = listCorrect.findIndex((m: string) => m.startsWith('ex|'));

    if (indexExample !== -1) {
        const itemExample = listCorrect[indexExample];
        listCorrect.splice(indexExample, 1);
        listCorrect.unshift(itemExample);
    }

    const lenAnswer = listCorrect.length;
    const divide = lenAnswer <= 4 ? lenAnswer : lenAnswer <= 8 ? 4 : 5;

    const listItemTop = listCorrect.slice(0, divide);
    const listItemBottom = listCorrect.slice(divide);

    return (
        <div className={classNames(styles.boxQuestion, styles.preventPageBreak)}>
            <Description question={question} />
            <div className={styles.listDrawLineItem}>
                {listItemTop.map((item, tIndex) => {
                    const itemName = item.split('*') || [];
                    return (
                        <div key={`item-top-${tIndex}`} >
                            {question.question_type !== 'DL2' &&
                                <div className={styles.itemText}>
                                    {itemName[2]}
                                </div>
                            }
                            {question.question_type === 'DL2' && (
                                <div className={styles.itemImage}>
                                    <img src={`/upload/${itemName[2]}`} alt="" />
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>
            {question.image && (
                <div className={styles.selectDrawImage}>
                    <img src={imageUrl} alt="" />
                </div>
            )}
            <div className={styles.listDrawLineItem}>
                {listItemBottom.map((item, dIndex) => {
                    const itemName = item.split('*') || [];
                    return (
                        <div key={`item-bottom-${dIndex}`} >
                            {question.question_type !== 'DL2' &&
                                <div className={styles.itemText}>
                                    {itemName[2]}
                                </div>
                            }
                            {question.question_type === 'DL2' && (
                                <div className={styles.itemImage}>
                                    <img src={`/upload/${itemName[2]}`} alt="" />
                                </div>
                            )}
                        </div>
                    );
                })}
            </div>
        </div>
    );
};

export default DrawLine;