import { useRef } from 'react';

import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'

import { Description, ReadingScript, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MatchingGameQuestion = ({
    question,
    index
}: QuestionProps) => {
    const groupKey = useRef(Math.random()).current
    const listAnswer: string[][] = question.answers
        .split('#')
        .map((m: string) => m.split('*'))
    const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length)

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {question.question_text && (
                <ReadingScript script={question.question_text} />
            )}
            <table className={classNames(styles.matchingTable, styles.preventPageBreak)}>
                <tbody>
                    {Array.from({ length: maxLength }, (_, _index) => (
                        <tr key={`${groupKey}_${_index}`}>
                            <td>
                                {listAnswer[0][_index] && (
                                    <>
                                        {index + _index}. <RenderTextHasStyle data={listAnswer[0][_index]} />
                                    </>
                                )}
                            </td>
                            <td>
                                {listAnswer[1][_index] && (
                                    <>
                                        {String.fromCharCode(97 + _index)}. <RenderTextHasStyle data={listAnswer[1][_index]} />
                                    </>
                                )}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    )
}

export default MatchingGameQuestion