import { QuestionDataType } from '@/interfaces/types'
import { convertStrToHtml } from '@/utils/string'

import { Description } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MultiChoice = ({
    question,
    index
}: QuestionProps) => {
    const answer = question.answers

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            <div className={styles.boxAnswers}>
                <span style={{marginRight: '15px'}}>{index}.</span>
                {answer.split('*')?.map((answerItem: string, answerIndex: number) => (
                    <div key={answerIndex} className={styles.answer}>
                        {`${String.fromCharCode(65 + answerIndex)}. `}
                        <span
                            dangerouslySetInnerHTML={{
                                __html: convertStrToHtml(answerItem),
                            }}
                        ></span>
                    </div>
                ))}
            </div>
        </div>
    )
}

export default MultiChoice