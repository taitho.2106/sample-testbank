import classNames from 'classnames'

import { QuestionDataType } from '@/interfaces/types'
import { convertStrToHtml, getTextWithoutFontStyle } from '@/utils/string'

import { Description, ReadingScript, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MultiChoiceGroup = ({
    question,
    index
}: QuestionProps) => {
    const listQuestionText = question.question_text?.split('#')
    const listAnswer = question?.answers.split('#')
    const listCorrect = question?.correct_answers.split('#')
    const questionsExample = []
    const questions = []
    const imageUrl = `/upload/${question.image}`

    for (let i = 0; i < listQuestionText.length; i++) {
        if (listQuestionText[i].startsWith('*')) {
            questionsExample.push({
                answer: listAnswer[i],
                correct_answers: listCorrect[i],
                question_text: listQuestionText[i].replaceAll('*', ''),
            })
        } else {
            questions.push({
                answer: listAnswer[i],
                correct_answers: listCorrect[i],
                question_text: listQuestionText[i],
            })
        }
    }

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {question.question_type === 'MC2' && (
                <div className={classNames(styles.boxImage, styles.preventPageBreak)}>
                    <img alt="" src={imageUrl} />
                </div>
            )}
            {question.parent_question_text && (
                <ReadingScript script={question.parent_question_text} />
            )}
            {questionsExample.length > 0 && (
                <>
                    <div className={styles.titleType}>Example:</div>
                    {questionsExample.map((questionE, mIndex: number) => {
                        const answersList = questionE.answer.split('*') || []
                        return (
                            <div
                                key={mIndex}
                                className={styles.multiQuestion}
                            >
                                <RenderTextHasStyle data={questionE.question_text ? `${questionE.question_text}` : ''} />
                                <div className={classNames(styles.boxAnswers)}>
                                    {answersList.map((answer: string, answerIndex: number) => (
                                        <div
                                            key={answerIndex}
                                            className={styles.answer}
                                        >
                                            <span className={classNames(styles.charCode, {
                                                [styles.correct]: questionE.correct_answers.trim() === getTextWithoutFontStyle(answer).trim()
                                            })}>
                                                {`${String.fromCharCode(65 + answerIndex)}. `}
                                            </span>
                                            <span
                                                dangerouslySetInnerHTML={{
                                                    __html: convertStrToHtml(answer),
                                                }}
                                            ></span>
                                        </div>
                                    ))}
                                </div>
                            </div>
                        )
                    })}
                </>
            )}
            <div className={styles.titleType}>{questions.length > 1 ? 'Questions:' : 'Question:'}</div>
            {questions.map((questionA: any, qIndex: number) => {
                const answersList = questionA.answer.split('*')
                return (
                    <div
                        key={qIndex}
                        className={styles.multiQuestion}
                    >
                        <span>{index + qIndex}. </span>
                        <RenderTextHasStyle data={questionA.question_text ? `${questionA.question_text}` : ''} />
                        <div className={classNames(styles.boxAnswers)}>
                            {answersList.map((answer: string, answerIndex: number) => (
                                <div
                                    key={answerIndex}
                                    className={styles.answer}
                                >
                                    {`${String.fromCharCode(65 + answerIndex)}. `}
                                    <span
                                        dangerouslySetInnerHTML={{
                                            __html: convertStrToHtml(answer),
                                        }}
                                    ></span>
                                </div>
                            ))}
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default MultiChoiceGroup