import { QuestionDataType } from '@/interfaces/types'

import { Description, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};
const FillInBlank1 = ({
    question,
    index
}: QuestionProps) => {
    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            <span>{index}.</span>
            <RenderTextHasStyle data={question.question_text} />
            <div>
                <span className={styles.hintText}>{`(${question.answers})`}</span>
            </div>
        </div>
    )
}

export default FillInBlank1