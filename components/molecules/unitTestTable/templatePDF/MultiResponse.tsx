import React from 'react';

import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types';

import { Description, RenderTextHasStyle } from '../TemplateExportPDF';
import styles from '../TemplateExportPDF.module.scss';

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MultiResponseQuestion = ({ question, index }: QuestionProps) => {
    const answers: string[] = question.answers.split('#');
    const groupKey = React.useRef(Math.random()).current;
    const isMR3 = question.question_type === 'MR3';

    const boxAnswersClass = classNames(styles.boxAnswers, {
        [styles.vertical]: isMR3,
    });

    return (
        <div className={styles.boxQuestion}>
            <div style={{ display: 'flex' }}>
                <span className={styles.instruction}>{index}. </span>
                <Description question={question} />
            </div>
            {!isMR3 && (
                <div className={boxAnswersClass}>
                    {answers.map((answer, answerIndex) => (
                        <div
                            key={`${groupKey}_${answerIndex}`}
                            style={{
                                width: '25%',
                                padding: 5,
                            }}
                        >
                            {String.fromCharCode(65 + answerIndex)}.{' '}
                            <RenderTextHasStyle data={answer} />
                        </div>
                    ))}
                </div>
            )}
            {isMR3 && (
                <div className={boxAnswersClass}>
                    {answers.map((answer, answerIndex) => (
                        <div
                            key={`${groupKey}_${answerIndex}`}
                            style={{
                                padding: 5,
                            }}
                        >
                            {String.fromCharCode(65 + answerIndex)}.{' '}
                            <RenderTextHasStyle data={answer} />
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default MultiResponseQuestion