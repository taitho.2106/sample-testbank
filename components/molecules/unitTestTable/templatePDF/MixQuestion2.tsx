import { useRef } from 'react';

import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'
import { convertStrToHtml } from '@/utils/string';

import { Description, ReadingScript, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const MIXQuestion2 = ({
    question,
    index
}: QuestionProps) => {
    const groupKey = useRef(Math.random()).current
    const listAnswers: string[] = question.answers.split('[]')
    const LSAnswers: string[] = listAnswers[0].split('#')
    const MCAnswers: string[] = listAnswers[1].split('#')
    const listInstruction: string[] = question.question_description.split('[]')
    const MCQuestions: string[] = question.question_text.split('[]')[1].split('#')

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {question.parent_question_text && (
                <ReadingScript script={question.parent_question_text} />
            )}
            <span
                className={styles.instruction}
                style={{
                    marginTop: '20px'
                }}
            >
                {`Task 1: ${listInstruction[0]}`}
            </span>
            <div className={styles.answerTable}>
                <table>
                    <thead>
                        <tr>
                            <th>Answers</th>
                            <th>True</th>
                            <th>False</th>
                            <th>Not given</th>
                        </tr>
                    </thead>
                    <tbody>
                        {LSAnswers.map((answer, answerIndex) => [
                            <tr key={`${groupKey}_${answerIndex}`}>
                                <td>
                                    {index + answerIndex}. <RenderTextHasStyle data={answer} />
                                </td>
                                <td>
                                    <div className={styles.radio}></div>
                                </td>
                                <td>
                                    <div className={styles.radio}></div>
                                </td>
                                <td>
                                    <div className={styles.radio}></div>
                                </td>
                            </tr>,
                            answerIndex < LSAnswers.length - 1 && (
                                <tr
                                    key={`${groupKey}_${answerIndex}_divide`}
                                    className={styles.divide}
                                >
                                    <td colSpan={4}>
                                        <div></div>
                                    </td>
                                </tr>
                            ),
                        ])}
                    </tbody>
                </table>
            </div>

            <span
                className={styles.instruction}
                style={{
                    marginTop: '20px'
                }}
            >
                {`Task 2: ${listInstruction[1]}`}
            </span>
            {MCQuestions.map((questionA: any, qIndex: number) => {
                const answersList = MCAnswers[qIndex].split('*')
                return (
                    <div
                        key={qIndex}
                        className={styles.multiQuestion}
                    >
                        <span>{index + LSAnswers.length + qIndex}. </span>
                        <RenderTextHasStyle data={questionA} />
                        <div className={classNames(styles.boxAnswers, styles.vertical)}>
                            {answersList.map((answer: string, answerIndex: number) => (
                                <div
                                    key={answerIndex}
                                    className={styles.answer}
                                >
                                    {`${String.fromCharCode(65 + answerIndex)}. `}
                                    <span
                                        dangerouslySetInnerHTML={{
                                            __html: convertStrToHtml(answer),
                                        }}
                                    ></span>
                                </div>
                            ))}
                        </div>
                    </div>
                )
            })}
        </div>
    )
}

export default MIXQuestion2