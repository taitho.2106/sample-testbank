import { useRef } from 'react';

import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'

import { Description, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};
const TrueFalseQuestion3_4_5 = ({
    question,
    index
}: QuestionProps) => {
    const groupKey = useRef(Math.random()).current
    const listAnswer = question.answers.split('#')
    const listCorrect = question?.correct_answers.split('#')
    const listImages = question.image.split('#') || []

    const questionsExample = listAnswer.filter((answer) => answer.startsWith('*'))
        .map((answer, i) => {
            const correct = listCorrect[i];
            const image = question.question_type === 'TF4' || question.question_type === 'TF5' ? listImages[i] : '';

            return {
                answer: answer.replace('*', ''),
                correct_answers: correct,
                image,
            };
        });

    const questions = listAnswer
        .map((answer, i) => ({
            answer,
            correct_answers: listCorrect[i],
            image: question.question_type === 'TF4' || question.question_type === 'TF5' ? listImages[i] : '',
        }))
        .filter((ques) => !ques.answer.startsWith('*'));

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {question.question_type === 'TF3' && (
                <div className={classNames(styles.boxImage, styles.preventPageBreak)}>
                    <img alt="" src={`/upload/${question.image}`} />
                </div>
            )}
            {questionsExample.length > 0 && (
                <>
                    <span className={styles.titleType}>
                        {questionsExample.length > 1 ? 'Examples:' : 'Example:'}
                    </span>
                    <div>
                        {questionsExample.map((questionE: any, eIndex: number) => {
                            const correct = questionE.correct_answers
                            return (
                                <div
                                    className={classNames(styles.questionRowFormat, styles.preventPageBreak)}
                                    key={`${groupKey}_${eIndex}_ex`}
                                >
                                    <span className={styles.textIndex} />
                                    {questionE.image && (
                                        <div className={styles.boxImage}>
                                            <img alt="" src={`/upload/${questionE.image}`} />
                                        </div>
                                    )}
                                    <div className={styles.sectionTrueFalse}>
                                        <RenderTextHasStyle data={questionE.answer} />
                                        <div className={styles.checking}>
                                            <span className={styles.checkbox}>
                                                {`${correct == 'T' ? '☑' : '☐'}`}
                                            </span>
                                            <label className={styles.textTF}>True</label>
                                            <span className={styles.checkbox}>
                                                {`${correct == 'F' ? '☑' : '☐'}`}
                                            </span>
                                            <label className={styles.textTF}>False</label>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </>
            )}
            {questions.length > 0 && (
                <>
                    <span className={styles.titleType}>
                        {questions.length > 1 ? 'Questions:' : 'Question:'}
                    </span>
                    <div>
                        {questions.map((questionA: any, aIndex: number) => {
                            return (
                                <div
                                    className={classNames(styles.questionRowFormat, styles.preventPageBreak)}
                                    key={`${groupKey}_${aIndex}`}
                                >
                                    <span className={styles.textIndex} >
                                        {index + aIndex}.
                                    </span>
                                    {questionA.image && (
                                        <div className={styles.boxImage}>
                                            <img alt="" src={`/upload/${questionA.image}`} />
                                        </div>
                                    )}
                                    <div className={styles.sectionTrueFalse}>
                                        <RenderTextHasStyle data={questionA.answer} />
                                        <div className={styles.checking}>
                                            <span className={styles.checkbox}>
                                                {'☐'}
                                            </span>
                                            <label className={styles.textTF}>True</label>
                                            <span className={styles.checkbox}>
                                                {'☐'}
                                            </span>
                                            <label className={styles.textTF}>False</label>
                                        </div>
                                    </div>
                                </div>
                            )
                        })}
                    </div>
                </>
            )}
        </div>
    )
}

export default TrueFalseQuestion3_4_5