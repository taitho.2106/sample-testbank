import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'
import shuffleArray from 'pages/api/unit-test/export-zip/mix-data/shuffleArray';

import { Description, RenderFBParagraphStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const FillInBlank8 = ({
    question,
    index
}: QuestionProps) => {
    const imageNote = question?.audio_script || ''
    const image = question?.image
    const listSuggestAnswer = question?.answers?.split('#') || []
    const correctAnswers = question.correct_answers.split('#')
    const listCorrectAnswer: any[] = correctAnswers.map((item: any) => item.split('%/%')).reduce((a: any, b: any) => [...a, ...b], []) ?? [];

    const mainImage = image.split('*')[0]
    const arrayImg = image.split('*')[1].split('#')
    const arrayImgRaw = arrayImg.map((item: any) => item.split('|')).reduce((a: any, b: any) => [...a, ...b], [])

    const arrayTextString = [...listCorrectAnswer, ...listSuggestAnswer]
    const arrayImgEx = []
    const arrayImgQuestion = []
    const arrayCorrectEx = []
    const arrayCorrectQuestion = []

    const indexExam = listCorrectAnswer.findIndex((m: string) => m.startsWith("*"))
    if (indexExam != -1) {
        arrayImgEx.push(arrayImgRaw[indexExam]);
        arrayCorrectEx.push(arrayTextString[indexExam]);
        arrayImgRaw.splice(indexExam, 1)
        arrayTextString.splice(indexExam, 1)
    }

    let randomIndexes = Array.from({ length: arrayTextString.length }, (v, i) => i)
    randomIndexes = shuffleArray(randomIndexes)
    for (let i = 0; i < randomIndexes.length; i++) {
        const indexRandom = randomIndexes[i];
        arrayImgQuestion.push(arrayImgRaw[indexRandom])
        arrayCorrectQuestion.push(arrayTextString[indexRandom])
    }

    const answerData = [...arrayCorrectEx, ...arrayCorrectQuestion]
    const answerListImage = [...arrayImgEx, ...arrayImgQuestion]

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            <div className={classNames(styles.boxImage, styles.preventPageBreak)}>
                <img alt="" src={`/upload/${mainImage}`} />
                <span className={styles.imageNote}>
                    {imageNote}
                </span>
            </div>

            <RenderFBParagraphStyle
                answer={question?.correct_answers || ''}
                data={question?.question_text}
                answerId={index}
            />
            <div className={classNames(styles.boxImageGroup,)}>
                <div className={styles.multiChoiceGroupImages}>
                    <div className={classNames(styles.answersGrid, {
                        [styles.twoColumns]: answerListImage.length === 4 || answerListImage.length === 2,
                        [styles.default]: answerListImage.length === 1
                    })}>
                        {answerListImage.map((answer: any, aIndex: number) => (
                            <div className={styles.answerItem} key={aIndex}>
                                <img
                                    src={`/upload/${answer}`}
                                    className={styles.answerImage}
                                    alt="/"
                                />
                                <span className={classNames(styles.answerData, {
                                    [styles.isExample]: answerData[aIndex]?.startsWith('*')
                                })}>
                                    {answerData[aIndex]?.startsWith('*') ? answerData[aIndex].replace(/\*/g, '') : answerData[aIndex]}
                                </span>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </div>
    )
}

export default FillInBlank8