import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types'

import { Description } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};
const FillInBlank5 = ({ question,
    index
}: QuestionProps) => {
    const imageUrl = `/upload/${question.image}`

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {question.image && (
                <div className={classNames(styles.boxImage, styles.preventPageBreak)}>
                    <img alt="" src={imageUrl} />
                </div>
            )}
            <div className={classNames(styles.boxAnswers, styles.vertical)}>
                {Array.from(
                    { length: question.total_question },
                    (_, mIndex: number) => (
                        <div key={mIndex} className={styles.answer}>
                            {`(${index + mIndex})___________________`}
                        </div>
                    ),
                )}
            </div>
        </div>
    )
}

export default FillInBlank5