import React, { useRef } from 'react';

import classNames from 'classnames';

import { QuestionDataType } from '@/interfaces/types';

import { Description } from '../TemplateExportPDF';
import styles from '../TemplateExportPDF.module.scss';

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const DragAndDrop1 = ({ question, index }: QuestionProps) => {
    const groupKey = useRef(Math.random()).current;
    const answers = question.answers.split('#');
    const correctAnswers = question.correct_answers.split('#');
    const images = (question.image || '').split('#');
    const checkExam = (question.question_text || '').split('#');

    const example: any = {
        answers: [],
        corrects: [],
        images: [],
    };
    const mainQuestion: any = {
        answers: [],
        corrects: [],
        images: [],
    };

    answers.forEach((answer, i) => {
        if (answer) {
            if (!checkExam[i] || checkExam[i] === '') {
                mainQuestion.corrects.push(correctAnswers[i]);
                mainQuestion.answers.push(answer);
                mainQuestion.images.push(images[i]);
            } else {
                example.corrects.push(correctAnswers[i]);
                example.answers.push(answer);
                example.images.push(images[i]);
            }
        }
    });

    const renderQuestions = (questions: any, title: string) => (
        <>
            <span className={styles.titleType}>
                {questions.answers.length > 1 ? `${title}s:` : `${title}:`}
            </span>
            <div>
                {questions.answers.map((answer: string, answerIndex: number) => (
                    <div
                        className={classNames(styles.questionRowFormat, styles.preventPageBreak)}
                        key={`${groupKey}_${answerIndex}`}
                    >
                        <span className={styles.textIndex}>
                            {title === 'Question' ? `${index + answerIndex}.` : ''}
                        </span>
                        {questions.images[answerIndex] && (
                            <div className={styles.boxImage}>
                                <img alt="" src={`/upload/${questions.images[answerIndex]}`} />
                            </div>
                        )}
                        <div className={styles.answerDD1}>
                            <span
                                className={styles.question}
                                style={{
                                    marginLeft: 'unset'
                                }}
                            >
                                {`Keywords: ${answer?.replace(/\*/g, '/')}`}
                            </span>
                            <div
                                className={styles.question}
                                style={{
                                    marginLeft: 'unset'
                                }}
                            >
                                {title === 'Question' ?
                                    `→ _______________________________________` :
                                    `→ ${questions.corrects[answerIndex]?.replace(/\*/g, ' ')}`
                                }
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );

    return (
        <div className={classNames(styles.boxQuestion)}>
            <Description question={question} />
            {example.answers.length > 0 && renderQuestions(example, 'Example')}
            {mainQuestion.answers.length > 0 && renderQuestions(mainQuestion, 'Question')}
        </div>
    );
};

export default DragAndDrop1;
