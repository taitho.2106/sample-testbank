import classNames from 'classnames'

import { QuestionDataType } from '@/interfaces/types'

import { Description, RenderTextHasStyle } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

type QuestionRender = {
    questionAns: any,
    index?: number,
}

const MultiChoiceGroupImages = ({
    question,
    index
}: QuestionProps) => {
    const listQuestionText = question.question_text?.split('#') || []
    const listAnswer = question.answers.split('#')
    const listCorrect = question.correct_answers.split('#')
    const questionsExample: any[] = []
    const questions: any[] = []

    if (question.question_type === 'MC11') {
        for (let i = 0; i < listAnswer.length; i++) {
            const isExample = listAnswer[i].startsWith('ex');
            const questionItem = {
                answer: listAnswer[i],
                correct_answers: listCorrect[i],
                question_text: isExample
                    ? listQuestionText[i]?.replaceAll('*', '')
                    : listQuestionText[i],
            };
            (isExample ? questionsExample : questions).push(questionItem);
        }
    } else {
        for (let i = 0; i < listQuestionText.length; i++) {
            const isExample = listQuestionText[i].startsWith('*');
            const questionItem = {
                answer: listAnswer[i],
                correct_answers: listCorrect[i],
                question_text: isExample
                    ? listQuestionText[i]?.replaceAll('*', '')
                    : listQuestionText[i],
            };
            (isExample ? questionsExample : questions).push(questionItem);
        }
    }

    const hasExamples = questionsExample.length > 0;
    const hasQuestions = questions.length > 0;

    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            {hasExamples && (
                <>
                    <div className={styles.titleType}>
                        {questionsExample.length > 1 ? 'Examples:' : 'Example:'}
                    </div>
                    <div className={styles.boxAnswers}>
                        {questionsExample.map((questionE, mIndex: number) => (
                            <ExampleQuestion
                                key={`example-${mIndex}`}
                                questionAns={questionE}
                            />
                        ))}
                    </div>
                </>
            )}
            {hasQuestions && (
                <>
                    <div className={styles.titleType}>
                        {questions.length > 1 ? 'Questions:' : 'Question:'}
                    </div>
                    <div className={styles.boxAnswers}>
                        {questions.map((questionA, mIndex: number) => (
                            <StandardQuestion
                                key={`answer-${mIndex}`}
                                questionAns={questionA}
                                index={index + mIndex}
                            />
                        ))}
                    </div>
                </>
            )}
        </div>
    );
};

const ExampleQuestion = ({
    questionAns,
}: QuestionRender) => {
    const answersList = questionAns.answer.replace('ex|', '').split('*') || [];
    return (
        <div className={styles.multiChoiceGroupImages}>
            <RenderTextHasStyle data={questionAns.question_text || ''} />
            <div className={classNames(styles.answersGrid, {
                [styles.twoColumns]: answersList.length === 4 || answersList.length === 2,
                [styles.default]: answersList.length === 1
            })}>
                {answersList.map((exAnswer: any, exIndex: number) => {
                    return (
                        <AnswerItem
                            key={`answer-example-${exIndex}`}
                            imageUrl={`/upload/${exAnswer}`}
                            isCorrect={questionAns.correct_answers.includes(exIndex.toString())}
                            index={exIndex}
                        />
                    )
                })}
            </div>
        </div>
    );
};

const StandardQuestion = ({
    questionAns,
    index,
}: QuestionRender) => {
    const answersList = questionAns.answer.split('*') || [];
    return (
        <div className={styles.multiChoiceGroupImages}>
            <div className={styles.questionText}>
                <span>{index}. </span>
                <RenderTextHasStyle data={questionAns.question_text || ''} />
            </div>
            <div className={classNames(styles.answersGrid, {
                [styles.twoColumns]: answersList.length === 4 || answersList.length === 2,
                [styles.default]: answersList.length === 1
            })}>
                {answersList.map((answer: any, aIndex: number) => (
                    <AnswerItem
                        key={`answer-question-${aIndex}`}
                        imageUrl={`/upload/${answer}`}
                        isCorrect={false}
                        index={aIndex}
                    />
                ))}
            </div>
        </div>
    );
};

type AnswerItem = {
    imageUrl: string,
    isCorrect: boolean,
    index: number
}

const AnswerItem = ({ imageUrl, isCorrect, index }: AnswerItem) => (
    <div className={classNames(styles.answerItem, styles.preventPageBreak)}>
        <img src={imageUrl} className={styles.answerImage} alt="answer" />
        <span className={styles.charCode}>
            {String.fromCharCode(65 + index)}. <span className={styles.checkbox}>{isCorrect ? '☑' : '☐'}</span>
        </span>
    </div>
);

export default MultiChoiceGroupImages