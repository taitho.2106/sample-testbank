import { QuestionDataType } from '@/interfaces/types'

import { Description } from '../TemplateExportPDF'
import styles from '../TemplateExportPDF.module.scss'

type QuestionProps = {
    question: QuestionDataType;
    index: number;
};

const SelectObject1 = ({
    question,
    index
}: QuestionProps) => {
    const listImage = question.image.split("*") || []
    const image = listImage[1]
    const imageUrl = `/upload/${image}`
    return (
        <div className={styles.boxQuestion}>
            <Description question={question} />
            <div className={styles.selectDrawImage}>
                <img src={imageUrl} alt='' />
            </div>
        </div>
    )
}

export default SelectObject1