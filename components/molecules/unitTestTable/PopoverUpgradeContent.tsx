import React from 'react';

import Link from 'next/link';

import TickIcon from '@/assets/icons/tick.svg'
import ButtonCustom from '@/componentsV2/Dashboard/SideBar/ButtonCustom/Button';
import useTranslation from '@/hooks/useTranslation';
import { cleanObject } from '@/utils/index';

import {paths as urlPaths, STEPS, PAYMENT_TYPE} from "../../../interfaces/constants";
import styles from './PopoverUpgradeContent.module.scss';

type PropPoP = {
    dataPackage?:any[]
    price?:number
    itemId?:string
}
const PopoverUpgradeContent = ({ dataPackage = [], price = 0, itemId = '' }: PropPoP) => {
    const { t } = useTranslation()
    const formattedAmount = price.toLocaleString('vi', {
      style: 'currency',
      currency: 'VND',
    }).replaceAll('.',',')
    const findTextToHighLight = (text:any, highLightText:any) => {
      if (text && highLightText) {
        const re = new RegExp(highLightText, 'g')
        return text.replace(re, `<b>${highLightText}</b>`)
      }

      return text
    }
  return (
    <div className={styles.upgradeContent}>
      <div className={styles.benefits}>
        <h3 className={styles.title}>{t('popover-upgrade')['title']}</h3>
        {dataPackage.map((item:any,index:number)=>{
            const labelOption = item.labelTranslateOption?.map((item:string) =>
              t(item),
            )
            const label = findTextToHighLight(
              t(item.label, labelOption),
              labelOption?.join(' '),
            )
            return (
              <div className={styles.item} key={index}>
                <TickIcon />
                <span className='break-line' dangerouslySetInnerHTML={{ __html: label }}></span>
              </div>
            )
        })}
      </div>
      <Link href={{ pathname: urlPaths.payment, query: cleanObject({ type: PAYMENT_TYPE.UPGRADE, itemId }) }} passHref >
        <ButtonCustom>{t(t('popover-upgrade')['confirm-action'],[`${formattedAmount.slice(0,formattedAmount.length - 1).trim()}`])}</ButtonCustom>
      </Link>
    </div>
  )
}

export default PopoverUpgradeContent;