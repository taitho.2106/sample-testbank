import React, { useContext } from "react";

import classNames from "classnames";
import { useRouter } from "next/router";
import { Button, Modal } from "rsuite";

import IconArrow from "@/assets/icons/arrow-left.svg"
import IconOutlinedInfo from "@/assets/icons/info-outlined-circle.svg";
import IconLampOnOutlined from "@/assets/icons/lamp-on-outlined.svg";
import { paths } from "@/interfaces/constants";
import { WrapperContext } from "@/interfaces/contexts";

import { usePackageUser } from "../../../lib/swr-hook";
import styles from "./ModalConfirmUseUnitTest.module.scss"
import useTranslation from "@/hooks/useTranslation";

type PropsModal = {
    isActive: boolean
}
const ModalConfirmUseUnitTest = ({isActive = false}: PropsModal) => {
    const { t, locale, subPath } = useTranslation();
    const { push } = useRouter();
    const { dataPackage } = usePackageUser()
    const { globalModal } = useContext(WrapperContext)
    const unitTest = globalModal.state?.data.unitTest || {}
    const conditionUseUnitTest = globalModal.state?.data?.conditionUseUnitTest || false
    const icon = `<svg width="16" height="13" viewBox="0 0 16 13" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0.866211 6.69559C0.866211 6.9216 0.964146 7.1476 1.13742 7.31334L6.10951 12.2779C6.29032 12.4512 6.48619 12.534 6.70466 12.534C7.17927 12.534 7.52581 12.195 7.52581 11.7355C7.52581 11.4944 7.43541 11.291 7.2772 11.1403L5.58217 9.42271L3.39746 7.42634L5.15276 7.53181H14.2833C14.7806 7.53181 15.1271 7.18527 15.1271 6.69559C15.1271 6.19838 14.7806 5.85184 14.2833 5.85184H5.15276L3.40499 5.95731L5.58217 3.96094L7.2772 2.2433C7.43541 2.09263 7.52581 1.88923 7.52581 1.64816C7.52581 1.18862 7.17927 0.849609 6.70466 0.849609C6.48619 0.849609 6.28278 0.932478 6.08691 1.12081L1.13742 6.07031C0.964146 6.23605 0.866211 6.46205 0.866211 6.69559Z" fill="#757688"/>
                  </svg>`
    const dataModal: any = {
        addUnitTest: {
            title: t('modal-add-unittest')['title'],
            descriptions: <span dangerouslySetInnerHTML={{ __html: t(t('modal-add-unittest')['description'], [unitTest.name])}}></span>,
            note: `${t('modal-add-unittest')['note']}:`,
            iconNote: <IconOutlinedInfo />,
            contentNote: <><span className={styles.noteContent} dangerouslySetInnerHTML={{__html: `${t(t('modal-add-unittest')['note-description'],[dataPackage?.quantity, `<a href="${subPath}${paths.payment}?type=renew">${t('modal-add-unittest')['guild']} ${icon}</a>`])}`}}></span></>
        },
        confirmBuy: {
            title: t('modal-confirm-buy-unittest')['title'],
            descriptions: <><span dangerouslySetInnerHTML={{ __html: t(t('modal-confirm-buy-unittest')['description'], [unitTest.name])}}></span></>,
            note: `${t('modal-confirm-buy-unittest')['note']}:`,
            iconNote: <IconLampOnOutlined />,
            contentNote: <><span className={styles.noteContent} dangerouslySetInnerHTML={{__html: t(t('modal-confirm-buy-unittest')['note-description'], [`<a href="${subPath}${paths.home}?el=pricing" target="_blank">${t('modal-confirm-buy-unittest')['guild']} ${icon}</a>`])}}></span></>
        }
    }
    const dataUnit = dataModal[conditionUseUnitTest ? "addUnitTest" : "confirmBuy"]
    const onCloseModal = () => globalModal.setState(null)
    const onSubmit = () => {
        if (conditionUseUnitTest) {
            if (globalModal.state?.data?.onChooseToUse) {
                globalModal.state?.data?.onChooseToUse(unitTest.id)
            }
        } else {
            push({
                pathname: paths.payment,
                query: {
                    itemId: unitTest.sale_item_id
                }
            }).finally()
        }
    }
    return (
        <Modal
            open={isActive}
            backdrop={false}
            className={styles.modalWrapper}
            style={{
                overflow: 'hidden',
            }}
        >
            <Modal.Body>
                <div className={styles.contentModal}>
                    <p className={styles.title}>{dataUnit.title}</p>
                    <div className={styles.descriptions}>{dataUnit.descriptions}</div>
                    <div className={classNames(styles.boxNote,styles[conditionUseUnitTest ? "addUnitTest" : "confirmBuy"])}>
                        <span className={styles.note}>{dataUnit.iconNote} {dataUnit.note}</span>
                        {dataUnit.contentNote}
                    </div>
                    <div></div>
                    <div className={styles.action}>
                        <Button className={styles.close} onClick={onCloseModal}>{t('common')['cancel']}</Button>
                        <Button className={styles.submit} onClick={onSubmit}>{t('common')['submit-btn']}</Button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    );
};

export default ModalConfirmUseUnitTest;