import { ForwardedRef, forwardRef, Fragment } from "react";

import classNames from "classnames";

import { DefaultPropsType } from "@/interfaces/types";
import { convertStrToHtml, formatTextStyleObject } from "@/utils/string";

import styles from "./TemplateExportPDF.module.scss"
import DragAndDrop1 from "./templatePDF/DragAndDrop1";
import DragAndDrop2 from "./templatePDF/DragAndDrop2";
import DrawLine from "./templatePDF/DrawLine";
import DrawShape1 from "./templatePDF/DrawShape1";
import FillInBlank from "./templatePDF/FillInBlank";
import FillInBlank1 from "./templatePDF/FillInBlank1";
import FillInBlank5 from "./templatePDF/FillInBlank5";
import FillInBlank8 from "./templatePDF/FillInBlank8";
import FillInColorType1 from "./templatePDF/FillInColorType1";
import LikerScaleQuestion from "./templatePDF/LikerScale";
import MatchingGameQuestion from "./templatePDF/MatchingGame";
import MatchingGameQuestion4 from "./templatePDF/MatchingGame4";
import MatchingGameQuestion5 from "./templatePDF/MatchingGame5";
import MatchingGameQuestion6 from "./templatePDF/MatchingGame6";
import MIXQuestion1 from "./templatePDF/MixQuestion1";
import MIXQuestion2 from "./templatePDF/MixQuestion2";
import MultiChoice from "./templatePDF/MultiChoice";
import MultiChoiceGroup from "./templatePDF/MultiChoiceGroup";
import MultiChoiceGroup9 from "./templatePDF/MultiChoiceGroup9";
import MultiChoiceGroupImages from "./templatePDF/MultiChoiceGroupImages";
import MultiResponseQuestion from "./templatePDF/MultiResponse";
import SelectFromList1 from "./templatePDF/SelectFromList1";
import SelectFromList2 from "./templatePDF/SelectFromList2";
import SelectObject1 from "./templatePDF/SelectObject1";
import ShortAnswer from "./templatePDF/ShortAnswer";
import TrueFalseQuestion from "./templatePDF/TrueFalseQuestion";
import TrueFalseQuestion3_4_5 from "./templatePDF/TrueFalseQuestion3_4_5";

type typeTemplateExportDPF = {
    data?: any
}
export const TemplateExportPDF = forwardRef((
    {
        data
    }: typeTemplateExportDPF,
    ref: ForwardedRef<HTMLDivElement>) => {
    let questionIndex = 0
    const excludeType = ['MR1', 'MR2', 'MR3']
    const renderQuestion = (question: any, index: number) => {
        switch (question.question_type) {
            case 'MC1':
            case 'MC2':
                return <MultiChoiceGroup question={question} index={index} />
            case 'MC3':
            case 'MC4':
                return <MultiChoice question={question} index={index} />
            case 'MC5':
            case 'MC6':
                return <MultiChoiceGroup question={question} index={index} />
            case 'MC7':
            case 'MC8':
            case 'MC11':
                return <MultiChoiceGroupImages question={question} index={index} />
            case 'MC9':
            case 'MC10':
                return <MultiChoiceGroup9 question={question} index={index} />
            case 'FB1':
                return <FillInBlank1 question={question} index={index} />
            case 'FB2':
            case 'FB3':
            case 'FB4':
            case 'FB6':
            case 'FB7':
                return <FillInBlank question={question} index={index} />
            case 'FB5':
                return <FillInBlank5 question={question} index={index} />
            case 'FB8':
                return <FillInBlank8 question={question} index={index} />
            case 'SL1':
                return <SelectFromList1 question={question} index={index} />
            case 'SL2':
                return <SelectFromList2 question={question} index={index} />
            case 'DD1':
                return <DragAndDrop1 question={question} index={index} />
            case 'DD2':
                return <DragAndDrop2 question={question} index={index} />
            case 'DL1':
            case 'DL2':
            case 'DL3':
                return <DrawLine question={question} index={index} />
            case 'SA1':
                return <ShortAnswer question={question} index={index} />
            case 'TF1':
            case 'TF2':
                return <TrueFalseQuestion question={question} index={index} />
            case 'TF3':
            case 'TF4':
            case 'TF5':
                return <TrueFalseQuestion3_4_5 question={question} index={index} />
            case 'MG1':
            case 'MG2':
            case 'MG3':
                return <MatchingGameQuestion question={question} index={index} />
            case 'MG4':
                return <MatchingGameQuestion4 question={question} index={index} />
            case 'MG5':
                return <MatchingGameQuestion5 question={question} index={index} />
            case 'MG6':
                return <MatchingGameQuestion6 question={question} index={index} />
            case 'MR1':
            case 'MR2':
            case 'MR3':
                return <MultiResponseQuestion question={question} index={index} />
            case 'LS1':
            case 'LS2':
                return <LikerScaleQuestion question={question} index={index} />
            case 'MIX1':
                return <MIXQuestion1 question={question} index={index} />
            case 'MIX2':
                return <MIXQuestion2 question={question} index={index} />
            case 'SO1':
                return <SelectObject1 question={question} index={index} />
            case 'DS1':
                return <DrawShape1 question={question} index={index} />
            case 'FC1':
            case 'FC2':
                return <FillInColorType1 question={question} index={index} />
        }
    }

    if (!data) return null
    return (
        <div ref={ref} className={styles.unitTestContainer}>
            <div className={styles.unitTestExport}>
                <table className={styles.info}>
                    <tbody>
                        <tr>
                            <td
                                align="center"
                                className={styles.verticalTop}
                                style={{ width: '50%' }}
                            >
                                UBND QUẬN _________ <br /> PHÒNG GIÁO DỤC VÀ ĐÀO TẠO <br /> ĐỀ
                                CHÍNH THỨC
                            </td>
                            <td align="center" colSpan={2} style={{ width: '50%' }}>
                                ĐỀ KIỂM TRA __________ <br /> Năm học: 20__ - 20__ <br /> Tiếng Anh -{' '}
                                {
                                    data.grade_name
                                }{' '}
                                <br />
                                Thời gian làm bài: {data.time} phút
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div className={styles.userInfo}>
                                    Trường: _______________________________________
                                </div>
                                <div className={styles.userInfo}>
                                    Họ tên: _______________________________________
                                </div>
                                <div className={styles.userInfo}>Lớp: _________________</div>
                            </td>
                            <td align="center" className={styles.verticalTop}>
                                Điểm
                            </td>
                            <td align="center" className={styles.verticalTop}>
                                Nhận xét của giáo viên
                            </td>
                        </tr>
                    </tbody>
                </table>
                <p className={styles.unitTestTitle}>{data.name}</p>
                <p className={styles.unitTestTime}>Time allotted: {data.time} phút</p>
                {data.sections.map((section: any, sIndex: number) => {
                    const totalQuestionPart = section.parts[0].questions.reduce((total: number, item: any) => {
                        return excludeType.includes(item.question_type) ? total + 1 : total + item.total_question;
                    }, 0);

                    return (
                        <div
                            key={section.parts[0].name}
                            className={styles.unitTestSection}
                            data-id={section.id}
                        >
                            <div className={classNames(styles.unitTestPart, styles.preventPageBreak)}>
                                <div>PART {sIndex + 1}</div>
                                {section.parts[0].name}
                                {totalQuestionPart > 1 ? ` (${totalQuestionPart} questions)` : ""}
                            </div>
                            {section.parts[0].questions.map((question: any, qIndex: number) => {
                                const currentQuestion = questionIndex + 1
                                if (excludeType.includes(question.question_type)) {
                                    questionIndex += 1
                                } else {
                                    questionIndex += question.total_question
                                }
                                return (
                                    <Fragment key={`${section.parts[0].name}${qIndex}`}>
                                        {renderQuestion(question, currentQuestion)}
                                    </Fragment>
                                )
                            })}
                        </div>
                    )
                })}
                <span className={styles.endPage}>---THE END---</span>
            </div>
        </div>
    );
})

TemplateExportPDF.displayName = 'TemplateExportDPF';

interface PropsType extends DefaultPropsType {
    data?: string
}

interface PropsTypeFB extends DefaultPropsType {
    data?: string
    answer?: string
    answerId?: number
}

type QuestionProps = {
    question: any;
};

type ReadingScriptProps = {
    script: string;
};

export const RenderTextHasStyle = ({
    data,
}: PropsType) => {
    const formatData = () => {
        const dataStr = data.replaceAll("%s%", "###")
        const newData = formatTextStyleObject(dataStr) || []
        const result: any[] = []
        newData.forEach((obj: any, indexObj: number) => {
            const textArr = obj.text.split('###')
            const len = textArr.length
            textArr.map((item: any, aIndex: number) => {
                result.push({
                    ...obj,
                    text: item,
                })
                if (aIndex < len - 1) {
                    result.push({
                        text: '%s%'
                    })
                }
            })
        })

        let returnStr = ''
        result.forEach((item, i) => {
            if (item.text === '%s%') {
                returnStr += "______________";
            } else {
                let tempText = item.text;
                if (item.underline) {
                    tempText = `<u>${tempText}</u>`
                }
                if (item.bold) {
                    tempText = `<b>${tempText}</b>`
                }
                if (item.italic) {
                    tempText = `<i>${tempText}</i>`
                }
                returnStr += tempText;
            }
        })
        return returnStr
    }
    return (
        <span
            dangerouslySetInnerHTML={{
                __html: formatData(),
            }}
        ></span>
    )
}

export const RenderFBParagraphStyle = ({
    answer,
    data,
    answerId,
}: PropsTypeFB) => {
    const formatData = () => {
        const answerList = answer.split('#')
        const dataStr = data.replace(/\r\n/g, '<br/>').replaceAll("%s%", "###")
        const newData = formatTextStyleObject(dataStr)

        const result: any[] = []
        newData.forEach((obj: any, indexObj: number) => {
            const textArr = obj.text.split('###')
            const len = textArr.length
            textArr.map((item: any, aIndex: number) => {
                result.push({
                    ...obj,
                    text: item,
                })
                if (aIndex < len - 1) {
                    result.push({
                        text: '%s%',
                        bold: undefined,
                        underline: undefined,
                        italic: undefined,
                    })
                }
            })
        })

        let returnStr = ''
        let index = 0
        let renderIndex = 0
        result.forEach((item, i) => {
            if (item.text === '%s%') {
                if (answerList[index]?.startsWith("*")) {
                    returnStr += ` <span style="font-style: italic; color: #21C274;">${answerList[index]
                        ?.replace(
                            /\%\/\%/g,
                            '/',)
                        .replace("*", "")} </span>`
                } else {
                    returnStr += `${answerList[index]
                        ? `(${answerId + renderIndex})______________ `
                        : ''}`
                    renderIndex++
                }
                index++
            } else {
                let tempText = item.text;
                if (item.underline) {
                    tempText = `<u>${tempText}</u>`
                }
                if (item.bold) {
                    tempText = `<b>${tempText}</b>`
                }
                if (item.italic) {
                    tempText = `<i>${tempText}</i>`
                }
                returnStr += tempText;
            }
        })
        const textHtml = convertStrToHtml(returnStr)
        return textHtml
    }

    return (
        <p
            dangerouslySetInnerHTML={{
                __html: formatData(),
            }}
        ></p>
    )
}

export const Description = ({ question }: QuestionProps) => {
    const totalQuestion = question.total_question;
    const description = (question.question_type !== 'MIX1' && question.question_type !== 'MIX2') ?
        question?.question_description || question?.parent_question_description :
        question.parent_question_description;

    return (
        <span className={styles.instruction}>
            {totalQuestion > 1 ? `${description} (${totalQuestion} questions)` : description}
        </span>
    );
}

export const ReadingScript = ({ script }: ReadingScriptProps) => {
    return (
        <div className={classNames(styles.readingScript, styles.PreventPageBreak)}
            dangerouslySetInnerHTML={{
                __html: convertStrToHtml(script).replace(/%s%/g, '______________'),
            }}
        >
        </div>
    )
}