import { Fragment, useContext } from "react";

import classNames from 'classnames'
import { useSession } from 'next-auth/client'
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
    FaEye,
    FaPencilAlt,
    FaTrashAlt,
    FaFilePdf,
    FaSyncAlt,
    FaExchangeAlt,
} from 'react-icons/fa'
import { GoKebabVertical } from 'react-icons/go'
import { Whisper, Popover, toaster } from 'rsuite'

import CartIcon from '@/assets/icons/cart.svg';
import ClockIcon from '@/assets/icons/clock02.svg'
import InboxIcon from '@/assets/icons/direct-inbox.svg';
import ExportAudio from "@/assets/icons/export-audio.svg"
import ListIDIcon from '@/assets/icons/ic-list-ID.svg'
import IconCopy from '@/assets/icons/icon-copy.svg';
import IconEdit from '@/assets/icons/icon-edit.svg';
import LockIcon from '@/assets/icons/lock.svg'
import PrintIcon from "@/assets/icons/print-icon.svg";
import PrivateIcon from '@/assets/icons/private_icon.svg';
import RightUpIcon from '@/assets/icons/right-up-arrow.svg'
import PaginationComponent from '@/componentsV2/Common/Pagination/PaginationComponent'
import SkeletonLoading from '@/componentsV2/Common/SkeletonLoading'
import { studentPricings } from '@/componentsV2/Home/data/price'
import { dataWithPackage } from '@/componentsV2/TeacherPages/UnitTestPage/data/dataWithPackage'
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'
import useExportAudio from "@/hooks/useExportAudio";
import useExportExcelListIDs from '@/hooks/useExportExcelListIDs';
import useExportWord from '@/hooks/useExportWord'
import { usePrintPDF } from "@/hooks/usePrintPDF";
import useSeries from '@/hooks/useSeries'
import useTranslation from "@/hooks/useTranslation";
import { checkAccessPackageLevel } from '@/utils/user'
import { paths } from 'api/paths'
import { callApi } from 'api/utils'
import { AlertNoti } from 'components/atoms/alertNoti'
import useGrade from 'hooks/useGrade'
import useNoti from 'hooks/useNoti'
import { PACKAGES_FOR_STUDENT, paths as pathUrl } from "interfaces/constants";
import { USER_ROLES } from 'interfaces/constants'
import { AppContext, WrapperContext } from 'interfaces/contexts'
import { SKILLS_SELECTIONS } from 'interfaces/struct'
import { DefaultPropsType, StructType } from 'interfaces/types'
import api from 'lib/api'
import {delayResult, enabledExportUnitTest, formatNumber, userInRight} from 'utils'

import { UNIT_TEST_GROUP, UNIT_TEST_TYPE } from '../../../constants'
import { usePackageUser } from "../../../lib/swr-hook";
import PopoverUpgradeContent from './PopoverUpgradeContent'
import styles from './UnitTestGrid.module.scss'

interface Props extends DefaultPropsType {
    currentPage?: number
    totalPage?: number
    onChangePage?: any
    data: any[]
    isLoading?: boolean
    setData?: any
    onDelete?: (id: number) => void
}

const skillThumbnails: any = {
    GR: '/images/collections/clt-grammar-green.png',
    LI: '/images/collections/clt-listening-green.png',
    PR: '/images/collections/clt-pronunciation-green.png',
    RE: '/images/collections/clt-reading-green.png',
    SP: '/images/collections/clt-speaking-green.png',
    US: '/images/collections/clt-use-of-english-green.png',
    VO: '/images/collections/clt-vocab-green.png',
    WR: '/images/collections/clt-writing-green.png',
}

const unitTestThumbnail: any = {
    PRACTICE_UNIT_TEST_IMG: '/images-v2/unit-test/unit-test.png',
    PRACTICE_LANGUAGE_IMG: '/images-v2/unit-test/practice.png',
    PRACTICE_SKILL_IMG: '/images-v2/unit-test/skill.png',

    EXAM_15_IMG: '/images-v2/unit-test/15-exam.png',
    EXAM_20_IMG: '/images-v2/unit-test/20-exam.png',
    EXAM_MID_TERM_IMG: '/images-v2/unit-test/midterm-exam.png',
    EXAM_END_OF_TERM_35_IMG: '/images-v2/unit-test/35-exam.png',
    EXAM_END_OF_TERM_45_IMG: '/images-v2/unit-test/45-exam.png',
    EXAM_END_OF_TERM_60_IMG: '/images-v2/unit-test/60-exam.png',

    FOCUS_GOOD_STUDENT_IMG: '/images-v2/unit-test/good-students.png',
    FOCUS_SUPPLEMENT_IMG: '/images-v2/unit-test/support.png',
    FOCUS_REINFORCE_IMG: '/images-v2/unit-test/enhance.png',
}

const UnitTestGrid = ({
    className = '',
    currentPage,
    totalPage,
    onChangePage,
    data,
    isLoading = false,
    style,
    setData,
    onDelete,
}: Props) => {
    const { fullScreenLoading, listPlans } = useContext(AppContext)
    const {t, locale, subPath} = useTranslation()
    const router = useRouter()
    const [session] = useSession()
    const { getGrade } = useGrade()
    const { getSeries } = useSeries()
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const isAdmin = userInRight([], session)
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
    const m = router.query?.m

    const { namePackage, dataPlan } = useCurrentUserPackage()
    const user: any = session.user
    const isMTeacher = m === 'teacher' && user?.is_admin === 1
    const { getNoti } = useNoti()
    const { handleExportWord } = useExportWord()
    const { handleExportExcel} = useExportExcelListIDs()
    const { handleExportAudio } = useExportAudio()
    const { handlePrintUnitTest, printPDFComponents } = usePrintPDF()
    const { globalModal } = useContext(WrapperContext)
    const allGradeCode = getGrade.map((item: any) => item.code)
    const allSeriesCode = getSeries.map((item: any) => item.code)
    const listCodeSeriesSGK = getSeries.filter(series => series.group === UNIT_TEST_GROUP.TEXTBOOK).map(s => s.code)
    const seriesIdSGK = new Set(listCodeSeriesSGK)
    const { mutateUserPackage } = usePackageUser()
    const itemSkeleton = 10

    const checkCustomRight = () => {
        let check: any = null
        switch (router.query?.m) {
            case 'mine':
                check = [USER_ROLES.Teacher]
                break
            case 'teacher':
                check = []
                break
            default:
                check = [USER_ROLES.Operator]
                break
        }
        return userInRight(check, session)
    }

    const handleCopyURL = (id: number) => {
        const userId = user?.id
        const protocol = window.location.protocol
        const hostname = window.location.hostname
        const port = hostname === 'localhost' ? `:${window.location.port}` : ''
        const url = `${protocol}//${hostname}${port}${subPath}/practice-test/${userId}===${id}`
        navigator.clipboard.writeText(url)
        getNoti('success', 'topEnd', t(`${t("common-get-noti")["copy-url"]}`))
    }

    const handleDeleteUniTtest = (id: number) => {
        globalModal.setState({
            id: 'confirm-modal',
            type: 'delete-unit-test',
            content: {
                closeText: t('common')['back'],
                submitText: t('common')['submit-btn'],
                onSubmit: () => onDelete(id),
            },
        })
    }

    const getSkills = (arr: any[]) => {
        const skillArr: any[] = []
        arr.forEach((parents) => {
            if (
                Array.isArray(parents?.name) &&
                parents?.name &&
                parents.name.length > 0
            ) {
                parents.name.forEach((children: any) => {
                    if (!skillArr.includes(children)) skillArr.push(children)
                })
            }
        })
        return skillArr
    }

    const handleOnSubmit = (id: any) => {
        router.push(`/unit-test/${id}?mode=edit`)
    }

    const handleCheckSeriesID = async (unitTestId: any) => {
        const response = await fetch(`/api/integrate-exam/unit-test/${unitTestId}`)
        const result = await response.json()
        if (result.code === 0) {
            const key = toaster.push(
                <AlertNoti type={'error'} message={result.message} />,
                {
                    placement: 'topCenter',
                },
            )
            setTimeout(() => toaster.remove(key), 2000)
        } else {
            if (
                result.data[0].series_id != 1 &&
                !allGradeCode.includes(result.data[0].template_level_id) &&
                !allSeriesCode.includes(result.data[0].series_id)
            ) {
                globalModal.setState({
                    id: 'confirm-modal-integrate-exam',
                    type: 'create-integrate-fail-for-grade-and-series',
                    content: {
                        closeText: t(`${t("common")["cancel"]}`),
                        submitText: t("edit-unit-test"),
                        onSubmit: () => {
                            handleOnSubmit(unitTestId)
                        },
                    },
                })
            } else if (
                result.data[0].series_id == 1 ||
                !result.data[0].series_id ||
                !allSeriesCode.includes(result.data[0].series_id)
            ) {
                globalModal.setState({
                    id: 'confirm-modal-integrate-exam',
                    type: 'create-integrate-fail',
                    content: {
                        closeText: t(`${t("common")["cancel"]}`),
                        submitText: t("edit-unit-test"),
                        onSubmit: () => {
                            handleOnSubmit(unitTestId)
                        },
                    },
                })
            } else if (!allGradeCode.includes(result.data[0].template_level_id)) {
                globalModal.setState({
                    id: 'confirm-modal-integrate-exam',
                    type: 'create-integrate-fail-for-grade',
                    content: {
                        closeText: t(`${t("common")["cancel"]}`),
                        submitText: t("edit-unit-test"),
                        onSubmit: () => {
                            handleOnSubmit(unitTestId)
                        },
                    },
                })
            } else {
                router.push(`/integrate-exam/${unitTestId}?mode=create`)
            }
        }
    }

    const createUnitTestSystem = async (id: number) => {
        const response = await fetch(
            `${paths.api_unit_test_create_from_teacher}?id=${id}`,
        )
        const result = await response.json()
        if (result.code === 0) {
            const key = toaster.push(
                <AlertNoti type={'error'} message={result.message} />,
                {
                    placement: 'topEnd',
                },
            )
            setTimeout(() => toaster.remove(key), 2000)
        } else {
            const key = toaster.push(
                <AlertNoti type={'success'} message={t(`${t("common-get-noti")["create-unit-test-succeed"]}`)} />,
                {
                    placement: 'topEnd',
                },
            )
            setTimeout(() => toaster.remove(key), 2000)
            router.push('/unit-test')
        }
    }

    const syncUnitTestSystem = async (id: number) => {
        const response = await fetch(
            `${paths.api_unit_test_sync_from_teacher}?id=${id}`,
        )
        const result = await response.json()
        if (result.code === 0) {
            const key = toaster.push(
                <AlertNoti type={'error'} message={result.message} />,
                {
                    placement: 'topEnd',
                },
            )
            setTimeout(() => toaster.remove(key), 2000)
        } else {
            const key = toaster.push(
                <AlertNoti type={'success'} message={t(`${t("common-get-noti")["update-unit-test-succeed"]}`)} />,
                {
                    placement: 'topEnd',
                },
            )
            setTimeout(() => toaster.remove(key), 2000)
            router.push(`/unit-test/${result.id}?mode=view`)
        }
    }

    const updatePrivacy = async (id: number, privacyBefore: any) => {
        const formData: any = { id: id }
        let privacy = null
        if (privacyBefore != 1) {
            // public
            formData.privacy = 1 //private
            privacy = 1
        }
        const response = await api.post(
            `${paths.api_unit_test_update_privacy}`,
            formData,
        )
        if (response.status == 200) {
            const updateIndex = data.findIndex((item) => item.id === id)
            if (updateIndex === -1) return
            const newList = data
            const updatedItem = newList[updateIndex]
            newList[updateIndex] = {
                ...updatedItem,
                privacy: privacy,
            }
            setData([...newList])
            const key = toaster.push(
                <AlertNoti
                    type={'success'}
                    message={t(`${t("common-get-noti")["update-privacy-succeed"]}`)}
                />,
                {
                    placement: 'topEnd',
                },
            )
            setTimeout(() => toaster.remove(key), 2000)
        } else {
            const key = toaster.push(
                <AlertNoti type={'error'} message={t(`${t("common-get-noti")["update-privacy-failed"]}`)} />,
                {
                    placement: 'topEnd',
                },
            )
            setTimeout(() => toaster.remove(key), 2000)
        }
    }

    const checkRequireUpgradePackage = (unitTest: any) => {
        if (isSystem || isStudent || isAdmin) {
            return false
        }
        const { package_level } = unitTest || {}
        return !checkAccessPackageLevel(namePackage.code, package_level)
    }

    const handleDivClick = async (id: number) => {
        if(isAdmin || isSystem) {
            router.push( `/unit-test/${id}?mode=view`)
        }else{
            fullScreenLoading.setState(true)
            const response: any = await delayResult(
                callApi(`/api/unit-test/${id}?mode=view`, 'get', 'token'),
                400,
            )
            fullScreenLoading.setState(false)
            if (response.status === 200) {
                globalModal.setState({
                    id: 'copy-unit-test-short-step',
                    dataUnitTest: response.data,
                    onClose: () => {
                        //
                    },
                })
            } else {
                getNoti('error', 'topCenter', response.message)
            }
        }
    }

    const onChooseToUse = (id: number) => {
        callApi(paths.api_student_use_unit_test, "POST", "Token", { unitTestId: id })
            .then((res: any) => {
                if (res.result) {
                    mutateUserPackage()
                    getNoti("success", "topCenter", t(`${t("common-get-noti")["save-unit-test-succeed"]}`));
                    router.push(`${pathUrl.unitTest}?m=mine`);
                    return;
                }
                getNoti("error", "topCenter", t(`${t("common-get-noti")["save-unit-test-failed"]}`));
            }).catch(() => {
                getNoti("error", "topCenter", t(`${t("common-get-noti")["save-unit-test-failed"]}`));
            })
    }

    const handleDivClickStudent = (unitTest: any) => {
        if (!unitTest?.sale_item_id) {
            return onChooseToUse(unitTest.id);
        }

        if (dataPlan.code === PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE || !dataPlan.unit_test_types.includes(unitTest.unit_test_type)) {
            return router.push(`${pathUrl.payment}?itemId=${unitTest?.sale_item_id}`)
        }

        return globalModal.setState({
            id: "confirm-use-unit-test",
            data: {
                unitTest,
                conditionUseUnitTest: !dataPlan.isExpired && dataPlan.quantity > 0,
                onChooseToUse
            }
        });
    }

    const findUnitTestImage = (index: number) => {
        const unit = data.find((item) => item.id === index)

        switch (unit.unit_test_type) {
            case UNIT_TEST_TYPE.PRACTICE_UNIT_TEST:
                return unitTestThumbnail.PRACTICE_UNIT_TEST_IMG
            case UNIT_TEST_TYPE.PRACTICE_LANGUAGE:
                return unitTestThumbnail.PRACTICE_LANGUAGE_IMG
            case UNIT_TEST_TYPE.PRACTICE_SKILL:
                return unitTestThumbnail.PRACTICE_SKILL_IMG
            case UNIT_TEST_TYPE.EXAM_15:
                if (unit.time === 15) return unitTestThumbnail.EXAM_15_IMG
                return unitTestThumbnail.EXAM_20_IMG
            case UNIT_TEST_TYPE.EXAM_MID_TERM:
                return unitTestThumbnail.EXAM_MID_TERM_IMG
            case UNIT_TEST_TYPE.EXAM_END_OF_TERM:
                if (unit.time === 60) return unitTestThumbnail.EXAM_END_OF_TERM_60_IMG
                else if (unit.time === 45)
                    return unitTestThumbnail.EXAM_END_OF_TERM_45_IMG
                return unitTestThumbnail.EXAM_END_OF_TERM_35_IMG
            case UNIT_TEST_TYPE.FOCUS_GOOD_STUDENT:
                return unitTestThumbnail.FOCUS_GOOD_STUDENT_IMG
            case UNIT_TEST_TYPE.FOCUS_SUPPLEMENT:
                return unitTestThumbnail.FOCUS_SUPPLEMENT_IMG
            case UNIT_TEST_TYPE.FOCUS_REINFORCE:
                return unitTestThumbnail.FOCUS_REINFORCE_IMG
            default:
                return unitTestThumbnail.PRACTICE_UNIT_TEST_IMG
        }
    }
    const isShowPrivacy = isAdmin && !router.query.m
    const renderIconPrivate = (item:any) => {
        const isShow = isShowPrivacy && item?.privacy === 1
        return (
          isShow && (
            <div className={styles.iconPrivate}>
              <PrivateIcon />
            </div>
          )
        )
    }

    const renderActionBtnStudent = (unitTest: any) => {
        const conditionWithSaleItem = unitTest?.sale_item_id && !dataPlan.isExpired && dataPlan.quantity > 0 && dataPlan.unit_test_types.includes(unitTest.unit_test_type)
        const conditionUseUnitTest = !unitTest?.sale_item_id || conditionWithSaleItem
        if (conditionUseUnitTest) {
            return (
                <div className={classNames(styles.buyExam, styles.chooseToUse)}>
                    <InboxIcon/>
                    <div className={styles.price}>
                        <p>{t("choose-to-use")}</p>
                    </div>
                </div>
            )
        }

        return (
            <div className={styles.buyExam}>
                <CartIcon/>
                <div className={styles.price}>
                    <p>{t("buy-now")}</p>
                    <b>{formatNumber(unitTest?.exchange_price, 0)}đ</b>
                </div>
            </div>
        )
    }

    return (
        <div className={styles.unitTestGridWrapper}>
            {isLoading ? (
                <div className={styles.unitTestGridSeries}>
                    {Array.from(Array(itemSkeleton), (_,index: number) => {
                        return (
                            <Fragment key={index}>
                                <SkeletonLoading className={styles.containerItem}>
                                    <div
                                        className={`${styles.boxSkeleton} ${styles.boxImageSkeleton}`}
                                    ></div>
                                    <span
                                        className={styles.unitTestName}
                                        style={{
                                            width: '50%',
                                        }}
                                    ></span>
                                    <span className={styles.unitTestName}></span>
                                    <div
                                        className={`${styles.boxSkeleton} ${styles.boxInfoSkeleton}`}
                                    ></div>
                                </SkeletonLoading>
                            </Fragment>
                        )
                    })}
                </div>
            ) : (
                <>
                    <div className={styles.unitTestGridSeries}>
                        {
                            data?.map((unitTest: any) => {
                                const isRequireUpgrade = checkRequireUpgradePackage(unitTest)
                                const dataPackage = studentPricings.teacher.find(item => item.code === unitTest.package_level)?.descriptions || []
                                const price = listPlans.find((item:any) => item.code === unitTest.package_level)?.price || 0
                                const id = listPlans.find((item:any) => item.code === unitTest.package_level)?.id || ''
                                const isUnitTestForSeriesSGK = seriesIdSGK.has(unitTest.series_id)
                                const isExportPDF = isTeacher && isUnitTestForSeriesSGK
                                return (
                                    <div
                                        className={classNames(
                                            styles.containerItem,
                                            {
                                                [styles.locked]: isRequireUpgrade,
                                            },
                                            styles.hover,
                                        )}
                                        key={unitTest.id}
                                        onClick={() => {
                                            if(isStudent) {
                                                handleDivClickStudent(unitTest)
                                                return;
                                            }

                                            if (!isRequireUpgrade) handleDivClick(unitTest.id)
                                        }}
                                    >
                                        <div className={styles.thumbnail}>
                                            <img src={findUnitTestImage(unitTest.id)} alt="" />
                                            {
                                                isStudent ?
                                                    renderActionBtnStudent(unitTest)
                                                    :
                                                    <>
                                                        {renderIconPrivate(unitTest)}
                                                        {isRequireUpgrade ? (
                                                            <Whisper
                                                                placement="autoHorizontal"
                                                                preventOverflow
                                                                trigger="hover"
                                                                enterable
                                                                speaker={
                                                                    <Popover className={styles.poperverModal} arrow={false}>
                                                                        <PopoverUpgradeContent  dataPackage={dataPackage} price={price} itemId={id}/>
                                                                    </Popover>
                                                                }
                                                            >
                                                                <div className={styles.lockedBtn}>
                                                                    {dataWithPackage[unitTest.package_level].icon || <LockIcon />}
                                                                </div>
                                                            </Whisper>
                                                        ) : (
                                                            <div
                                                                className={styles.actionGroup}
                                                                onClick={(e) => {
                                                                    e.stopPropagation()
                                                                    e.preventDefault()
                                                                }}
                                                            >
                                                                <Whisper
                                                                    placement={'auto'}
                                                                    trigger="click"
                                                                    container={() => document.querySelector('.t-wrapper__main')}
                                                                    speaker={
                                                                        <Popover>
                                                                            <div className={styles.tableActionPopover}>
                                                                                <div
                                                                                    className={styles.item}
                                                                                    onClick={() => handleCopyURL(unitTest?.id)}
                                                                                >
                                                                                    <IconCopy />
                                                                                    <span>{t(`${t("table-action-popover")["copy-url"]}`)}</span>
                                                                                </div>
                                                                                {checkCustomRight() && (
                                                                                    <Link
                                                                                        as={`/unit-test/${unitTest.id}?mode=edit`}
                                                                                        href={`/unit-test/[templateSlug]?mode=edit`}
                                                                                    >
                                                                                        <a className={styles.item}>
                                                                                            <FaPencilAlt />
                                                                                            <span>{t(`${t("common")["edit"]}`)}</span>
                                                                                        </a>
                                                                                    </Link>
                                                                                )}
                                                                                <Link
                                                                                    as={`/unit-test/${unitTest.id}?mode=view`}
                                                                                    href={`/unit-test/[templateSlug]?mode=view`}
                                                                                >
                                                                                    <a className={styles.item}>
                                                                                        <FaEye />
                                                                                        <span>{t(`${t("table-action-popover")["view-detail"]}`)}</span>
                                                                                    </a>
                                                                                </Link>
                                                                                {!isSystem && (
                                                                                    <a
                                                                                        className={styles.item}
                                                                                        onClick={() => handleDivClick(unitTest.id)}
                                                                                    >
                                                                                        <RightUpIcon />
                                                                                        <span>{t(`${t("table-action-popover")["copy-unit-test"]}`)}</span>
                                                                                    </a>
                                                                                )}
                                                                                {enabledExportUnitTest(user, unitTest) && (
                                                                                    <a
                                                                                        className={styles.item}
                                                                                        onClick={() => handleExportWord(unitTest.id)}
                                                                                    >
                                                                                        <FaFilePdf />
                                                                                        <span>{t(`${t("table-action-popover")["export-word"]}`)}</span>
                                                                                    </a>
                                                                                )}
                                                                                {checkCustomRight() && !isMTeacher && (
                                                                                    <div
                                                                                        className={styles.item}
                                                                                        onClick={() =>
                                                                                            handleCheckSeriesID(unitTest.id)
                                                                                        }
                                                                                        style={
                                                                                            isSystem || isAdmin
                                                                                                ? {}
                                                                                                : { display: 'none' }
                                                                                        }
                                                                                    >
                                                                                        <FaPencilAlt />
                                                                                        <span>{t(`${t("table-action-popover")["integration"]}`)}</span>
                                                                                    </div>
                                                                                )}
                                                                                {isMTeacher && (
                                                                                    <>
                                                                                        <div
                                                                                            className={styles.item}
                                                                                            onClick={() =>
                                                                                                createUnitTestSystem(unitTest.id)
                                                                                            }
                                                                                        >
                                                                                            <IconEdit />
                                                                                            <span>{t(`${t("table-action-popover")["create-unit-test-system"]}`)}</span>
                                                                                        </div>
                                                                                        <div
                                                                                            className={styles.item}
                                                                                            onClick={() =>
                                                                                                syncUnitTestSystem(unitTest.id)
                                                                                            }
                                                                                        >
                                                                                            <FaSyncAlt />
                                                                                            <span style={{ lineHeight: '1.2' }}>
                                                                                                {t(`${t("table-action-popover")["sync-unit-test-system"]}`)}
                                                                                            </span>
                                                                                        </div>
                                                                                    </>
                                                                                )}
                                                                                {isAdmin && !isMTeacher && (
                                                                                    <div
                                                                                        className={styles.item}
                                                                                        onClick={() =>
                                                                                            updatePrivacy(
                                                                                                unitTest.id,
                                                                                                unitTest.privacy,
                                                                                            )
                                                                                        }
                                                                                    >
                                                                                        <FaExchangeAlt />
                                                                                        <span style={{ lineHeight: '1.2' }}>
                                                                                            {t(`${t("table-action-popover")["change-privacy"]}`)}
                                                                                        </span>
                                                                                    </div>
                                                                                )}
                                                                                {checkCustomRight() && (
                                                                                    <div
                                                                                        className={styles.item}
                                                                                        onClick={() =>
                                                                                            handleDeleteUniTtest(unitTest.id)
                                                                                        }
                                                                                    >
                                                                                        <FaTrashAlt />
                                                                                        <span>{t(`${t("common")["delete"]}`)}</span>
                                                                                    </div>
                                                                                )}
                                                                                {isAdmin && (
                                                                                    <div
                                                                                        className={styles.item}
                                                                                        onClick={() =>handleExportExcel(user, unitTest.id)}
                                                                                    >
                                                                                        <ListIDIcon />
                                                                                        <span>{t(`${t("table-action-popover")["export-list-id"]}`)}</span>
                                                                                    </div>
                                                                                )}
                                                                                {
                                                                                    isExportPDF && (
                                                                                        <>
                                                                                            <div
                                                                                                className={styles.item}
                                                                                                onClick={() => handlePrintUnitTest(unitTest.id)}
                                                                                            >
                                                                                                <PrintIcon />
                                                                                                <span>{t(`${t("table-action-popover")["print-pdf"]}`)}</span>
                                                                                            </div>
                                                                                            <div
                                                                                                className={styles.item}
                                                                                                onClick={() => handleExportAudio(unitTest.id)}
                                                                                            >
                                                                                                <ExportAudio />
                                                                                                <span>{t(`${t("table-action-popover")["export-audio"]}`)}</span>
                                                                                            </div>                                                                                            
                                                                                        </>
                                                                                    )
                                                                                }
                                                                            </div>
                                                                        </Popover>
                                                                    }
                                                                >
                                                                    <div className={styles.actionBtn}>
                                                                        <GoKebabVertical color="#7C68EE" />
                                                                    </div>
                                                                </Whisper>
                                                            </div>
                                                        )}
                                                    </>
                                            }
                                        </div>
                                        <div className={styles.unitTestName}>
                                            <span>{unitTest.name}</span>
                                        </div>
                                        <div className={styles.infoContent}>
                                            <div className={styles.lineInfo}>
                                                <div className={styles.totalTime}>
                                                    <ClockIcon />
                                                    <div className={styles.text}>
                                                        {unitTest?.time} {t("minutes")}
                                                    </div>
                                                </div>
                                                {/* <div className={styles.testType}>
                                                    {UNIT_TYPE.find(
                                                        (find: StructType) =>
                                                            find?.code === `${unitTest?.unit_type}`,
                                                    )?.display ||
                                                        unitTest?.unit_type ||
                                                        '---'}
                                                </div> */}
                                            </div>
                                            <div className={styles.lineInfo}>
                                                <div className={styles.textInfo}>{t(`${t("common")["total-question"]}`)}</div>
                                                <div className={styles.numberInfo}>
                                                    {unitTest.total_question}
                                                </div>
                                            </div>
                                            <div className={styles.lineInfo}>
                                                <div className={styles.textInfo}>{t(`${t("common")["total-skill"]}`)}</div>
                                                <div className={styles.numberInfo}>
                                                    <Whisper
                                                        placement={'auto'}
                                                        trigger={isRequireUpgrade ? 'none' : 'hover'}
                                                        speaker={
                                                            <Popover>
                                                                <div
                                                                    // className={styles.sectionPopover}
                                                                    className="m-unittest-template-table-data__section-popover"
                                                                >
                                                                    {getSkills(unitTest.sections).map(
                                                                        (item: any, i: number) => (
                                                                            <div
                                                                                key={i}
                                                                                // className={styles.sectionItem}
                                                                                className="section-item"
                                                                            >
                                                                                <img
                                                                                    src={skillThumbnails[item]}
                                                                                    alt={item}
                                                                                />
                                                                                <span>
                                                                                    {SKILLS_SELECTIONS.find(
                                                                                        (skill: StructType) =>
                                                                                            skill.code === item,
                                                                                    )?.display || item}
                                                                                </span>
                                                                            </div>
                                                                        ),
                                                                    )}
                                                                </div>
                                                            </Popover>
                                                        }
                                                    >
                                                        <span>
                                                            {getSkills(unitTest.sections).length || 0}
                                                        </span>
                                                    </Whisper>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                    </div>
                    <div className={styles.newPagination}>
                        <PaginationComponent
                            total={totalPage * 10}
                            pageSize={10}
                            current={currentPage}
                            showTotal={() => {
                                //
                            }}
                            onChange={onChangePage}
                            showSizeChanger={false}
                        />
                    </div>
                </>
            )}
            {printPDFComponents}
        </div>
    )
}

export default UnitTestGrid
