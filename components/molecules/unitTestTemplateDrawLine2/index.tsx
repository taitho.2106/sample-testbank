import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'
import { useEffect, useRef, useState } from 'react'
import Guid from 'utils/guid'
import { getTextWithoutFontStyle } from 'utils/string'

import { DefaultPropsType } from '../../../interfaces/types'

interface Props extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateDrawLine2 = ({ className = '', data, style }: Props) => {
  const elementRef = useRef<HTMLDivElement>(null)
  const [loading, setLoading] = useState(false)
  const [ratio, setRatio] = useState(1)
  const ratioRef = useRef(1)
  const isLoadImageExample = useRef(false)
  const sizeWordExport = 680
  let answerList = []
  let urlImage = data.image_question_base64
  if (data.list_answer_item) {
    answerList = JSON.parse(data.list_answer_item).map((m: any) => {
      return { ...m, correctUrl: m.correctUrl.startsWith('blob:') ? m.correctUrl : `/upload/${m.correctUrl}` }
    })
  } else {
    answerList = data.correct_answers.split('#').map((m: any) => {
      const items = m.split('*')
      const postion = items[1].split(':')
      const point = {
        minX: postion[0],
        minY: postion[1],
        maxX: postion[2],
        maxY: postion[3],
      }
      return {
        ...point,
        isExample: m.startsWith('ex|'),
        correctUrl: `/upload/${items[2]}`,
        data: `/upload/${items[0].replace('ex|', '')}`,
      }
    })
    urlImage = `/upload/${data.image?.split('*')[0]}`
  }
  const indexExample = answerList.findIndex((m: any) => m.isExample)
  let itemExample = answerList[0]
  if (indexExample != -1) {
    itemExample = answerList[indexExample]
    answerList.splice(indexExample, 1)
    answerList.splice(0, 0, itemExample)
  }
  let divide = 5
  const lenAnswer = answerList.length
  if (lenAnswer <= 4) {
    divide = lenAnswer
  } else if (lenAnswer <= 8) {
    divide = 4
  }
  const listItemTop = []
  const listItemBottom = []

  for (let i = 0; i < lenAnswer; i++) {
    if (i < divide) {
      listItemTop.push(answerList[i])
    } else {
      listItemBottom.push(answerList[i])
    }
  }
  const onLoadImageInstruction = () => {
    console.debug("zoooooooo")
    if (!isLoadImageExample.current) {
      return
    }

    if (!elementRef.current) return
    const correctElement = elementRef.current.querySelector(
      '.item-image-correct.--example .image-correct',
    ) as HTMLImageElement
    if (correctElement) {
      const answerElement = elementRef.current.querySelector('.item-image.--example') as HTMLImageElement
      if (!answerElement) return
      const canvas = elementRef.current.querySelector('.canvas-preview-dl2') as HTMLCanvasElement
      canvas.setAttribute('height', `${canvas.parentElement.offsetHeight}`)
      canvas.setAttribute('width', `${canvas.parentElement.offsetWidth}`)

      const ctx = canvas.getContext('2d')
      const pointCorrect = getPointDraw(correctElement, 'bottom',1)
      const pointAnswer = getPointDraw(answerElement, 'top', ratioRef.current)

      //offset parrent
      if (pointAnswer && pointCorrect) {
        //origin offset:--class:m-unittest-draw-line-2__container__answers
        const xCorrect = correctElement.offsetLeft + pointCorrect.x
        const yCorrect = correctElement.offsetTop + pointCorrect.y + 5

        const xAnswer = answerElement.offsetLeft + answerElement.parentElement.offsetLeft + pointAnswer.x
        const yAnswer = answerElement.offsetTop + answerElement.parentElement.offsetTop + pointAnswer.y
        ctx.beginPath()
        ctx.fillStyle = '#67ab8d'
        ctx.arc(xCorrect, yCorrect - 1, 4, 0, 2 * Math.PI)
        ctx.lineWidth = 1
        ctx.fill()
        ctx.strokeStyle = '#ffffff'
        ctx.lineWidth = 2
        ctx.stroke()

        ctx.beginPath()
        ctx.fillStyle = '#67ab8d'
        ctx.arc(xAnswer, yAnswer, 4, 0, 2 * Math.PI)
        ctx.lineWidth = 1
        ctx.fill()
        ctx.strokeStyle = '#ffffff'
        ctx.lineWidth = 2
        ctx.stroke()

        ctx.beginPath()
        ctx.moveTo(xAnswer, yAnswer)
        // ctx.lineCap = 'round'
        ctx.strokeStyle = '#67ab8d'
        ctx.lineTo(xCorrect, yCorrect)
        ctx.stroke()
        ctx.fill()
      }
    }
  }
  const getPointDraw = (element: HTMLImageElement, direction: string, ratioSize:number) => {
    const canvas = document.createElement('canvas')
    canvas.style.visibility = 'hidden'
    const ctx = canvas.getContext('2d')
    const imgHeight = element.offsetHeight*ratioSize
    const imgWidth = element.offsetWidth*ratioSize
    ctx.drawImage(element, 0, 0, imgWidth, imgHeight)
    if (direction == 'top') {
      for (let j = 0; j < imgHeight; j++) {
        const pixelData = ctx.getImageData(imgWidth / 2, j, 1, 1).data

        if (pixelData[0] !== 0 || pixelData[1] !== 0 || pixelData[2] !== 0 || pixelData[3] !== 0) {
          canvas.remove()
          return { x: imgWidth / 2, y: j }
        }
      }
    } else if (direction == 'bottom') {
      for (let j = imgHeight; j > 0; j--) {
        const pixelData = ctx.getImageData(imgWidth / 2, j, 1, 1).data
        if (pixelData[0] !== 0 || pixelData[1] !== 0 || pixelData[2] !== 0 || pixelData[3] !== 0) {
          canvas.remove()
          return { x: imgWidth / 2, y: j }
        }
      }
    }
  }

  useEffect(() => {
    if (indexExample == -1) return
    //tr-expand in view 'choose question for unit_test'
    let interval: any
    const trExpand = elementRef.current.closest('.tr-expand')
    if (trExpand) {
      let isDisplayNone = window.getComputedStyle(trExpand).display == 'none'
      if (isDisplayNone) {
        interval = setInterval(() => {
          isDisplayNone = window.getComputedStyle(trExpand).display == 'none'
          if (!isDisplayNone) {
            clearInterval(interval)
            setTimeout(() => {
              onLoadImageInstruction()
            }, 300)
          }
        }, 100)
        return
      }
    }
    setTimeout(() => {
      onLoadImageInstruction()
    }, 300)
    return () => {
      if (interval) {
        clearInterval(interval)
      }
    }
  }, [])
  useEffect(() => {
   isLoadImageExample.current && onLoadImageInstruction()
  }, [ratio])

  return (
    <div className={`m-unittest-draw-line-2 ${className}`} style={style} ref={elementRef}>
      <div>
        <MultiChoicesTutorial className="tutorial" data={data?.question_description || ''} />
      </div>
      <div className="m-unittest-draw-line-2__audio">
        <audio controls>
          <source src={data.audio.startsWith('blob') ? data.audio : `/upload/${data.audio}`} type="audio/mpeg" />
        </audio>
      </div>
      <div className="m-unittest-draw-line-2__container">
        <div className="m-unittest-draw-line-2__container__answers" style={{ width: `${sizeWordExport}px` }}>
          {listItemTop.length > 0 && (
            <div className="__top">
              {listItemTop.map((m) => (
                <div className={`item-image-correct ${m.isExample ? '--example' : ''}`} key={Guid.newGuid()}>
                  <img
                    className="image-correct"
                    alt="img"
                    src={m.correctUrl}
                  ></img>
                  <img
                    className="image-answer"
                    alt="img"
                    src={m.data}
                  ></img>
                </div>
              ))}
            </div>
          )}
          <div className="content-image">
            <img
              alt=""
              style={{ maxWidth: `${sizeWordExport}px` }}
              ref={(value) => {
                if (!value) return
                const img = value
                let ratioCurrent = sizeWordExport / value.naturalWidth
                if (ratioCurrent > 1) ratioCurrent = 1
                setRatio(ratioCurrent)
                ratioRef.current = ratioCurrent;
                const updateFunc = () => {
                  setLoading(true)
                }
                img.onload = updateFunc
                if (img.complete) {
                  updateFunc()
                }
              }}
              src={urlImage}
            ></img>
            {answerList.map((m: any) => {
              return (
                <img
                  className={`item-image ${m.isExample ? '--example' : ''}`}
                  key={Guid.newGuid()}
                  style={{
                    top: `${m.minY * ratio}px`,
                    left: `${m.minX * ratio}px`,
                    transformOrigin: 'top left',
                    transform: `scale(${ratio})`,
                  }}
                  alt="img"
                  src={m.data}
                  onLoad={() => {
                    if(m.isExample){
                      isLoadImageExample.current = true;
                      onLoadImageInstruction()
                    }
                  }}
                ></img>
              )
            })}
          </div>
          {listItemBottom.length > 0 && (
            <div className="__bottom">
              {listItemBottom.map((m) => (
                <div className={`item-image-correct ${m.isExample ? '--example' : ''}`} key={Guid.newGuid()}>
                  <img
                    className="image-correct"
                    alt="img"
                    src={m.correctUrl}
                  ></img>
                  <img className="image-answer" alt="img" src={m.data}
                  ></img>
                </div>
              ))}
            </div>
          )}
          <canvas className="canvas-preview-dl2"></canvas>
        </div>
      </div>
    </div>
  )
}
