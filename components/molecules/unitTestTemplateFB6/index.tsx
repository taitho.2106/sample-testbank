import { FBParagraphStyle } from 'components/atoms/fillintheblankParagraphStyle'
import { MultiChoicesGeneralQuestion } from 'components/atoms/multichoiceGeneralQuestion'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'

import { DefaultPropsType } from '../../../interfaces/types'

interface PropsType extends DefaultPropsType {
  data?: any
}

export const UnitTestTemplateFB6 = ({
  className = '',
  data,
  style,
}: PropsType) => {
  return (
    <div className={`m-unittest-fb4 ${className}`} style={style}>
      <div className="m-unittest-fb4__turorial">
        <MultiChoicesTutorial
          className="tutorial"
          data={data?.question_description || ''}
        />
      </div>
      <div className="m-unittest-fb4__listquestion">
        <div
          className="m-unittest-fb4__left"
          style={{ height: 'unset', padding: '10px' }}
        >
          <MultiChoicesGeneralQuestion
            className="question"
            data={data?.parent_question_text || ''}
          />
        </div>
        <div className="m-unittest-fb4__right">
          <div className="m-unittest-multi-choice-1__answers">
            <FBParagraphStyle
              answer={data?.correct_answers || ''}
              data={data?.question_text || ''}
            />
          </div>
        </div>
      </div>
    </div>
  )
}
