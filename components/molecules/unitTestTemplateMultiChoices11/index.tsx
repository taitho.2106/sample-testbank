import { MultiChoicesAnwers } from 'components/atoms/multichoiceAnswers'
import { MultiChoicesTutorial } from 'components/atoms/multichoiceTutorial'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from '@/hooks/useTranslation'

interface Props extends DefaultPropsType {
    data?: any
}

export const UnitTestTemplateMultiChoices11 = ({
    className = '',
    data,
    style,
}: Props) => {
    const {t} = useTranslation()
    const uploadPath = '/upload/'
    const description = data?.question_description ?? ''
    let answersGroup = data?.answers.split('#') ?? []
    let correctGroup = data?.correct_answers.split('#') ?? []
    const audioSource = data.audio.startsWith('blob') ? data.audio : `${uploadPath}${data.audio}`
    
    const exampleContainer: any = { answers: [], correct_answers: [], files: [] }
    const questionContainer: any = { answers: [], correct_answers: [], files: [] }
    
    answersGroup.forEach((value: string, index: number) => {
        if( value.startsWith('ex') ){
            exampleContainer.answers.push(answersGroup[index])
            exampleContainer.correct_answers.push(correctGroup[index])
            exampleContainer.files.push(answersGroup[index])
        }else{
            questionContainer.answers.push(answersGroup[index])
            questionContainer.correct_answers.push(correctGroup[index])
            questionContainer.files.push(answersGroup[index])
        }
    })

    answersGroup = [ ...exampleContainer.answers, ...questionContainer.answers]
    correctGroup = [ ...exampleContainer.correct_answers, ...questionContainer.correct_answers]

    const bunchOfAnswer = answersGroup.map((item: string, index: number) => {
        return { 
            key: `${Math.random()}_${index}`,
            answerString: item.replace('ex|', ''), 
            correctString: correctGroup[index] ?? '', 
            mark: item.startsWith('ex') ? false : true }})
     let indexExample  =-1;
     let indexQuestion = -1;       
    return (
        <div className={`m-unittest-multi-choice-11`}>
            <div className={`wrapper-tutorial`}>
                <MultiChoicesTutorial className="tutorial"
                    data={description}/>
            </div>
            <div className={`wrapper-audio`}>
                <audio controls>
                    <source src={audioSource} type="audio/mpeg"/>
                </audio>
            </div>
            {bunchOfAnswer.map((item: any, index: number) => {
                if(item.mark){
                    indexQuestion+=1;
                }else{
                    indexExample+=1;
                }
                return (
                    <div key={item.key} className={`wrapper-${item.mark ? 'questions' : 'examples'}`}>
                        {indexExample == 0 && !item.mark && <div className={`head`}>
                            <span className={`__text`}>{t('common')['example']}</span>
                        </div>}
                        {indexQuestion == 0 && item.mark && <div className={`head`}>
                            <span className={`__text`}>{t('common')['question']}</span>
                        </div>}
                        <div className={`body`}>
                            {item.answerString.split('*').map((v: string, i: number) => (
                                <div key={`${item.key}_${i}`} className={`item`}>
                                    <img src={`${v.startsWith('blob') ? v : `${uploadPath}${v}`}`} 
                                        alt='image-answer'/>
                                    <div className={`__correct`}>
                                        <MultiChoicesAnwers checked={
                                            item.correctString.includes(i.toString()) ? true : false } />
                                    </div>
                                </div>
                            ))}
                        </div>
                        {item.mark &&  index < bunchOfAnswer.length-1 && <hr></hr> }
                    </div>
                )
            })}
        </div>
    )
}
