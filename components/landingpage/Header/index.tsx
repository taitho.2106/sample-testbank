import Link from 'next/link'
import { LandingButton } from '../Shared/Button'
import { LandingNav } from '../Navigation'
import { LandingDropdown } from '../Shared/Dropdown'
import { DefaultPropsType } from 'interfaces/types'
import { useEffect, useState } from 'react'
import { LandingMenuIcon } from '../Shared/MenuIcon'
import useTranslation from 'hooks/useTranslation'
import { signOut, useSession } from 'next-auth/client'
import { useRouter } from "next/dist/client/router";

export const LandingHeader = ({
  className = '',
  style = {},
}: DefaultPropsType) => {
  const { t } = useTranslation()
  const [sticky, setSticky] = useState('')
  const [session] = useSession()
  const { asPath } = useRouter()
  const handleScroll = (e: any) => {
    if (window.scrollY >= 100) setSticky('sticky')
    else setSticky('')
  }

  useEffect(() => {
    document.addEventListener('scroll', handleScroll)

    return () => {
      document.removeEventListener('scroll', handleScroll)
    }
  }, [])

  const ScrollToSectionHandle = (section: string) => {
    window.scrollTo({
      top: document.getElementById(section).offsetTop - 95,
      behavior: 'smooth',
    })
  }

  const handleMenuIconClick = (toggle: any) => {
    if (toggle)
      document
        .getElementById('landing-header')
        .classList.add('mobile-menu-visible')
    else
      document
        .getElementById('landing-header')
        .classList.remove('mobile-menu-visible')
    toggle ? disable() : enable()
    var myDiv = document.getElementById('landing-header')
    myDiv.scrollTop = 0
  }

  function disable() {
    document.body.classList.add('stop-scrolling')
  }

  function enable() {
    document.body.classList.remove('stop-scrolling')
    var myDiv = document.getElementById('landing-header')
    myDiv.scrollTop = 0
  }

  return (
    <div
      id="landing-header"
      className={`landing-header ${sticky} ${className}`}
      style={style}
    >
      <div className="landing-header__wrapper landing-wrapper">
        <div className="landing-header__wrapper__header">
          <img
            className="landing-header__logo"
            src="/images/landingpage/logo-mobile.png"
            alt="logo"
          />
          <LandingMenuIcon
            className="landing-header__wrapper__header__menu-icon"
            onClick={handleMenuIconClick}
          />
        </div>
        <div className="landing-header__wrapper__left">
          <LandingNav className="landing-header__nav" />
        </div>
        <div className="landing-header__wrapper__right">
          <LandingDropdown />
          {session ? (
            <>
              <Link href={`/login?next=${asPath}`}>
                <a className="__link account-info" style={{ color: 'white' }}>
                  {(session.user as any).user_name}
                </a>
              </Link>
              <LandingButton
                className="login-btn"
                onClick={() => {
                  signOut({ callbackUrl: '/login', redirect: false })
                }}
              >
                <a className="__link">{t('sign-out-menu')}</a>
              </LandingButton>
            </>
          ) : (
            <>
              <LandingButton className="login-btn">
                <Link href={`/login?next=${asPath}`}>
                  <a className="__link">{t('sign-in-menu')}</a>
                </Link>
              </LandingButton>
              <LandingButton
                className="trial-btn"
                style={{ background: '#35B9E5', color: '#FFFFFF' }}
                onClick={() => ScrollToSectionHandle('free-trial')}
              >
                <a className="__link" style={{ color: 'white' }}>
                  {t('free-trial-menu')}
                </a>
              </LandingButton>
            </>
          )}
        </div>
      </div>
    </div>
  )
}
