import { useSession } from 'next-auth/client' // add session to get userInfo
import Link from 'next/link'
import { Button } from 'rsuite'

import { URL_FROM_EDUHOME } from '../../../interfaces/constants'
import { DefaultPropsType } from '../../../interfaces/types'
import { Sidenav } from '../../molecules/sidenav'

interface Props extends DefaultPropsType {
  isExpand: boolean
  onToggle: () => void
}

export const Sidebar = ({
  className = '',
  isExpand,
  style,
  onToggle,
}: Props) => {
  const [session] = useSession()
  const userSession: any = session.user
  const lastDay:any = userSession.end_date && userSession.end_date>0?new Date(userSession.end_date*1000) : null
  const currentDay:any = (new Date()).getTime();
  const subTime = (lastDay - currentDay) /(1000*60*60*24);
  const num = Math.floor(subTime);
  return (
    <>
      {userSession.is_trial_mode === 1 && subTime >= 0? (
      <aside
        className={`o-sidebar ${className}`}
        data-expand={isExpand}
        style={style}
      >
        <Button className="o-sidebar__toggle" onClick={onToggle}>
          <img src="/images/icons/ic-arrow-left-active.png" alt="toggle" />
        </Button>
        <div className="o-sidebar__brand">
          <Link href="/">
            <a>
              <img
                className="brand-logo"
                src="/images/collections/clt-logo.png"
                alt=""
              />
            </a>
          </Link>
        </div>
        <div
            className="o-sidebar__sidenav"
            style={{ overflow: isExpand ? 'auto' : 'unset' }}
          >
            <Sidenav isExpand={isExpand} />
        </div>

        <div className="o-sidebar__expired">         
        <span>
          
            <div className="o-sidebar__warning">
              <span style={{margin: '10px 0'}}>CÒN 
                <div style={{padding: "0 3px", color:"white"}} ><div className="o-sidebar__remain">{num}</div></div> 
                NGÀY DÙNG THỬ MIỄN PHÍ
              </span>
              <div>
              <Link href={URL_FROM_EDUHOME}>
                <Button style={{background: "#37B9E4", color:"white"}}>Nâng cấp tài khoản</Button>
                </Link>
              </div>
            </div>
            </span>
              </div>

      </aside>
      ) : (
        <aside
        className={`o-sidebar ${className}`}
        data-expand={isExpand}
        style={style}
      >
        <Button className="o-sidebar__toggle" onClick={onToggle}>
          <img src="/images/icons/ic-arrow-left-active.png" alt="toggle" />
        </Button>
        <div className="o-sidebar__brand">
          <Link href="/">
            <a>
              <img
                className="brand-logo"
                src="/images/collections/clt-logo.png"
                alt=""
              />
            </a>
          </Link>
        </div>
        <div
            className="o-sidebar__sidenav"
            style={{ overflow: isExpand ? 'auto' : 'unset' }}
          >
            <Sidenav isExpand={isExpand} />
        </div>        
      </aside>
      ) }
    </>
  )
}
