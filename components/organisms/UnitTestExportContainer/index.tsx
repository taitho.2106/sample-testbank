import { useEffect, useRef, useState } from 'react'
import api from 'lib/api'
import { paths } from 'api/paths'
import { Button } from 'rsuite'
import { formatDate } from 'utils/string'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { useRouter } from 'next/router'
type UnitTestExportContainerType = {
  id: string
  breadcrumbData: any[]
  SetBreadcrumbData: (data: any[]) => void
  setUnitTestName: (data: any) => void
}
export const UnitTestExportContainer = ({
  id,
  setUnitTestName,
  SetBreadcrumbData,
}: UnitTestExportContainerType) => {
  const [data, setData] = useState([])
  const [triggerReset, setTriggerReset] = useState(false)
  const [error, setError] = useState('')
  const [exportCode, setExportCode] = useState('')
  const [onProcessing, setOnProcessing] = useState(false)
  const mixQuestion = useRef(false)
  const router = useRouter()
  const getDataStart = async () => {
    const apiRes = await api.get(
      `${paths.api_unit_test_export}/histories?id=${id}&type=word`,
    )
    if (apiRes && apiRes.status == 200) {
      const dataRes: any = apiRes.data
      if (dataRes && dataRes.code == 1) {
        SetBreadcrumbData([
          { name: 'Tổng quan', url: '/' },
          { name: 'Danh sách đề thi', url: `/unit-test`},
          {
            name: dataRes.data.unit_test_name as string,
            url: `/unit-test/${id}?mode=view${router?.query?.m?`&m=${router?.query?.m}`:""}`,
            ignoreQueryCheck:true,
          },
          {
            name: 'Xuất đề thi',
            url: null,
          },
        ])
        setUnitTestName(dataRes.data.unit_test_name)
        setData(dataRes.data.export_histories)
      }
    }
  }
  useEffect(() => {
    getDataStart()
  }, [])
  const downloadHistory = async (historyId: any) => {
    console.log('download-=--')
    const url = `${paths.api_unit_test_export}/download?id=${historyId}`
    // window.open(url, "","width=200,height=100,top=-200, left=-200")
    // window.open(url, '_blank')
    window.open(url) || window.location.assign(url)
  }
  const createExportFile = async (isMixQuestions:boolean) => {
    try{
      mixQuestion.current = isMixQuestions;
      if (!exportCode || !exportCode.trim()) {
        setExportCode('')
        setTriggerReset(!triggerReset)
        setError('Vui lòng nhập Mã đề thi')
        return
      }
      if(exportCode.length>10){
        setError('Mã đề không được quá 10 ký tự')
        return
      }
      const strRex = /(\*|\/|\\|\:|\?|\"|\'|\.|\<|\>|\|)/g;
      if(exportCode.match(strRex)){
        setError('Mã đề không được chứa các ký tự: \ / : * ? " < > | .')
        return
      }
      setOnProcessing(true)
      const apiRes = await api.get(
        `${paths.api_unit_test_export}/create?id=${id}&code=${encodeURIComponent(
          exportCode.trim(),
        )}${isMixQuestions?"&mix=1":""}`,
      )
      setOnProcessing(false)
      if (apiRes) {
        const dataRes: any = apiRes?.data
        if (apiRes.status == 200) {
          if (dataRes) {
            if (dataRes.code == 1) {
              setExportCode("")
              setError('')
              setTriggerReset(!triggerReset)
              await getDataStart()
              downloadHistory(dataRes.data)
            } else {
              setError(dataRes.message)
            }
          }
        } else {
          setError(dataRes.message)
        }
      }
    }catch{
      setOnProcessing(false)
    }
    
  }
  const isProcessingMix = onProcessing && mixQuestion?.current;
  const isProcessingSave = onProcessing && !mixQuestion?.current
  return (
    <>
      <div className="unit-test-export-zip-content">
        <div className="__box-export">
          <div className="__title">
            <span>XUẤT ĐỀ THI FILE WORD</span>
          </div>
          <div className="__content">
            <div className={`__form-input ${error ? 'error' : ''}`}>
              <InputWithLabel
                className="info-input"
                label="Nhập mã đề thi"
                type="text"
                maxLength={10}
                defaultValue={exportCode}
                triggerReset={!triggerReset}
                onChange={(value: any) => {
                  setExportCode(value)
                  if(data.find(m=> m.code == value)){
                    setError("Đã tồn tại mã đề "+value)
                  }else{
                    setError("")
                  }
                }}
              />
              <div className={`error-message ${error ? '--open' : ''}`}>
                *<span>{error}</span>
              </div>
            </div>
            <Button
              className={`__export-btn --mix-question ${
                onProcessing && mixQuestion?.current ? '__on-processing' : ''
              }`}
              onClick={() => {
                if(onProcessing || error){
                  console.log("none--")
                }else{
                  createExportFile(true)
                }
              }}
            >
               <img style={!isProcessingMix?{display:"none"}:{}} src="/images/icon-loading-modal.png"  alt="loading"/>
               <img style={isProcessingMix?{display:"none"}:{}} src="/images/icons/ic-mix-white.png" alt="export" />
               <span>{isProcessingMix?"Đang xuất đề thi":'Trộn và lưu đề thi'}</span>

            </Button>
            <Button
              className={`__export-btn ${
                onProcessing && !mixQuestion?.current ? '__on-processing' : ''
              }`}
              onClick={() => {
                if(onProcessing || error){
                  console.log("none--")
                }else{
                  createExportFile(false)
                }
              }}
            >
              <img style={!isProcessingSave?{display:"none"}:{}} src="/images/icon-loading-modal.png"  alt="loading"/>
              <img style={isProcessingSave?{display:"none"}:{}} src="/images/icons/ic-download-white.png" alt="export" />
               <span>{isProcessingSave?"Đang xuất đề thi":"Lưu đề thi"}</span>
            </Button>
          </div>
        </div>
        <div className="__box-histories">
          <div className="__title">
            <span>LỊCH SỬ XUẤT ĐỀ THI</span>
          </div>
          <div className="__content-table">
            <div className="__table">
              <table>
                <thead>
                  <tr>
                    <th>STT</th>
                    <th>NGÀY XUẤT ĐỀ</th>
                    <th>MÃ ĐỀ</th>
                    <th>TRỘN CÂU HỎI</th>
                    <th>TẢI ĐỀ ĐÃ XUẤT</th>
                  </tr>
                </thead>
                {data && data.length > 0 ? (
                  <tbody>
                    {data.map((item: any, index: number) => (
                      <tr key={`export_${index}`}>
                        <td>{index + 1}</td>
                        <td>{formatDate(item.created_date, true)}</td>
                        <td>{item.code}</td>
                        <td>{item.mix_questions?"Có":"Không"}</td>
                        <td>
                          <Button
                            className="__download-btn"
                            onClick={() => {
                              downloadHistory(item.id)
                            }}
                          >
                            <img
                            height={16}
                            width={16}
                              src="/images/icons/ic-download-white.png"
                              alt="download"
                            />
                            <span>Tải xuống</span>
                          </Button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                ) : (
                  <tbody>
                    <tr className="tr_no-data">
                      <td colSpan={5}>
                        <div className="no-data">
                          <img src="/images/emty-result.png" alt=""></img>
                          <span>Không có dữ liệu</span>
                        </div>
                      </td>
                    </tr>
                  </tbody>
                )}
              </table>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
