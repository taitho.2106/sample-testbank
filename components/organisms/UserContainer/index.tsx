import { CSSProperties, useState, useRef } from 'react'

import { useRouter } from 'next/dist/client/router'
import { Collapse } from 'react-collapse'
import { Button, Tooltip, Whisper } from 'rsuite'

import { Card } from 'components/atoms/card'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { MultiSelectPicker } from 'components/atoms/selectPicker/multiSelectPicker'
import { UserTable } from 'components/molecules/userTable'
import { useRoles } from 'lib/swr-hook'
import { DateInput } from 'components/atoms/dateInput'

type PropsType = {
  className?: string
  style?: CSSProperties
}

export const UserContainer = ({ className = '', style }: PropsType) => {
  const router = useRouter()
  const { roles } = useRoles()

  const [triggerReset, setTriggerReset] = useState(false)
  const [textSearch, setTextSearch] = useState('')

  const [dataFilter, setDataFilter] = useState({
    name: '',
    role: '',
    status: '',
    created_date: ''
  })

  const [isOpenFilterCollapse, setIsOpenFilterCollapse] = useState(false)
  const [filterBagde, setFilterBagde] = useState(0)
  const filterDataRef = useRef<any>({})

  const fullRole: any = [{ display: 'Admin', code: '0' }]

  const setFilter = (type: string, value: any) => {
    filterDataRef.current[type] = value
    // setFilterBagde(
    //   Object.keys(filterDataRef.current).filter(
    //     (m) => filterDataRef.current[m].length > 0,
    //   ).length,
    // )

    let numberBagde = 0;
    Object.keys(filterDataRef.current).forEach((item) => {
      if (Array.isArray(filterDataRef.current[item])) {
        if (filterDataRef.current[item].length > 0) {
          numberBagde++;
        }
      } else {
        if (filterDataRef.current[item]) {
          numberBagde++;
        }
      }
    })

    setFilterBagde(numberBagde);


    const newData: any = { ...dataFilter }
    newData[type] = value
    setDataFilter(newData)
  }

  const handleFilterSubmit = async (isReset: boolean) => {
    if (isReset) {
      setDataFilter({
        name: '',
        role: '',
        status: '',
        created_date: ''
      })
      setFilterBagde(0)
      console.log('filterDataRef.current', filterDataRef.current)
      Object.keys(filterDataRef.current).filter(
        (m) => (filterDataRef.current[m] = ''),
      )
      return
    }
    const newData: any = { ...dataFilter }
    newData.name = textSearch
    setDataFilter(newData)
  }

  const onSubmit = async (e: any) => {
    e.preventDefault()
    handleFilterSubmit(false)
  }

  const getFullRole = () => {
    roles.map((m) => {
      fullRole.push({ display: m.name, code: m.id.toString() })
    })
    return fullRole
  }

  return (
    <div className={`m-user__content${className}`} style={style}>
      <form autoComplete="new-password" onSubmit={onSubmit}>
        <div className="content__sup">
          <div className="__input-group">
            <InputWithLabel
              className="__search-box"
              label="Tên người dùng/email"
              placeholder="Tìm kiếm tên người dùng/email"
              triggerReset={triggerReset}
              type="text"
              onChange={(val) => setTextSearch(val)}
              icon={<img src="/images/icons/ic-search-dark.png" alt="search" />}
            />
            <Whisper
              placement="bottom"
              trigger="hover"
              speaker={
                <Tooltip>Nhấn để chọn lọc dữ liệu theo nhiều điều kiện</Tooltip>
              }
            >
              <Button
                className="__sub-btn"
                data-active={isOpenFilterCollapse}
                style={{ marginLeft: '0.8rem' }}
                onClick={() => setIsOpenFilterCollapse(!isOpenFilterCollapse)}
              >
                <div
                  className="__badge"
                  data-value={filterBagde}
                  data-hidden={filterBagde <= 0}
                >
                  <img src="/images/icons/ic-filter-dark.png" alt="filter" />
                </div>
                <span>Lọc</span>
              </Button>
            </Whisper>
            <Whisper
              placement="bottom"
              trigger="hover"
              speaker={
                <Tooltip>Nhấn để xóa bỏ bộ lọc đang dùng</Tooltip>
              }
            >
              <Button
                className="__sub-btn"
                onClick={() => {
                  setTriggerReset(!triggerReset)
                  handleFilterSubmit(true)
                }}
              >
                <img src="/images/icons/ic-reset-darker.png" alt="reset" />
                <span>Mặc định</span>
              </Button>
            </Whisper>
            <Button
              className="__main-btn"
              onClick={() => {
                handleFilterSubmit(false)
              }}
            >
              <img src="/images/icons/ic-search-dark.png" alt="find" />
              <span>Tìm kiếm</span>
            </Button>
          </div>
          <div className="__action-group">
            <Button
              className="__action-btn"
              onClick={() => {
                router.push('/users/[userSlug]', `/users/${-1}`)
              }}
            >
              <img src="/images/icons/ic-plus-white.png" alt="create" />
              <span>Tạo mới</span>
            </Button>
          </div>
        </div>
        <Collapse isOpened={isOpenFilterCollapse}>
          <div className="content__sub">
            {roles && (
              <MultiSelectPicker
                className="__filter-box"
                label="Vai trò"
                triggerReset={triggerReset}
                data={getFullRole()}
                onChange={(val) => setFilter('role', val)}
              />
            )}
            <MultiSelectPicker
              className="__filter-box"
              data={[
                { code: 'AC', display: 'Hoạt động' },
                { code: 'DI', display: 'Không hoạt động' },
              ]}
              label="Trạng thái"
              triggerReset={triggerReset}
              onChange={(val) => setFilter('status', val)}
            />
            <DateInput
              className="__filter-box"
              defaultValue={null}
              label="Ngày tạo"
              placement="bottomEnd"
              time={false}
              style={{ zIndex: 3 }}
              onChange={(val) => {
                val !== null && setFilter('created_date', val)
              }}
              triggerReset={triggerReset}
              />
          </div>
        </Collapse>
      </form>
      <Card
        className={`m-user-table-data ${className}`}
        title={''}
        style={style}
      >
        <UserTable dataFilter={dataFilter} />
      </Card>
    </div>
  )
}
