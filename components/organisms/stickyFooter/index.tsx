import { CSSProperties, useContext, useEffect, useRef, useState } from 'react'

import { AppContext } from 'interfaces/contexts'

import { DefaultPropsType } from '../../../interfaces/types'

interface Props extends DefaultPropsType {
  containerStyle?: CSSProperties,
}

export const StickyFooter = ({
  className = '',
  style,
  containerStyle,
  children,
}: Props) => {
  const { isExpandSidebar } = useContext(AppContext)
  
  return (
    <div
      className={`o-sticky-footer ${className}`}
      data-blur={true}
      data-size={isExpandSidebar.state ? 'md' : 'lg'}
      style={style}
    >
      <div className="o-sticky-footer__container" style={containerStyle}>
        {children}
      </div>
    </div>
  )
}
