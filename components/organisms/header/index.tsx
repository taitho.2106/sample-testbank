import { useContext, useEffect, useState, useRef } from 'react'

import { signOut, useSession } from 'next-auth/client'
import { Dropdown, Button, Whisper, Tooltip } from 'rsuite'

import { WrapperContext } from 'interfaces/contexts'

import { DefaultPropsType } from '../../../interfaces/types'
import { useRouter } from 'next/router'
import { useUserInfo } from 'lib/swr-hook'
import HelpOutlineIcon from '@rsuite/icons/HelpOutline'

export const Header = ({ className = '', style }: DefaultPropsType) => {
  const { pageTitle, userInfo, globalModal } = useContext(WrapperContext)
  const router = useRouter()

  const [session] = useSession()

  if (!session) return <></>

  const userSession: any = session.user
  const { user } = useUserInfo()

  const [userData, setUserData] = useState({
    avatar: '',
    userName: userInfo?.user_name || userSession?.user_name || '',
  })

  const imgRef = useRef(null)

  const onLogOut = async () => {
    await signOut({ callbackUrl: '/login', redirect: false })
  }

  const onUserInfo = () => {
    router.push('/users/info', '/users/info')
  }

  const tooltip = <Tooltip>Hướng dẫn sử dụng</Tooltip>

  useEffect(() => {
    setUserData({
      avatar: userInfo?.avatar,
      userName: userInfo?.user_name,
    })
    if (userInfo?.avatar) {
      imgRef.current.setAttribute('src', userInfo?.avatar)
    }
  }, [userInfo])

  useEffect(() => {
    setUserData({
      ...userData,
      userName: userSession?.user_name,
    })
  }, [userSession])

  return (
    <header className={`o-header ${className}`} style={style}>
      <div className="o-header__title">
        <h1>{pageTitle}</h1>
      </div>
      <div className="o-header__action">
        {/* <div className="action-btn">
          <HeaderAction>
            <img src="/images/icons/ic-setting-dark.png" alt="notification" />
          </HeaderAction>
        </div>
        <div className="action-btn">
          <HeaderAction badge={{ content: '9+' }}>
            <img
              src="/images/icons/ic-notification-dark.png"
              alt="notification"
            />
          </HeaderAction>
        </div> */}
        <div className="instruction">
          {/* <Button
            className="submit-btn"
            onClick={() => globalModal.setState({id: 'instruction'})}
          > */}
          <Whisper
            placement="bottom"
            controlId="control-id-hover"
            trigger="hover"
            speaker={tooltip}
          >
            <HelpOutlineIcon
              onClick={() => globalModal.setState({ id: 'instruction' })}
              style={{ fontSize: 26 }}
            />
          </Whisper>

          {/* </Button> */}
        </div>
        <div
          className="action-account"
          data-size={userData?.userName?.length > 10 ? 'lg' : 'md'}
        >
          <div className="action-account-cover">
            <img
              ref={imgRef}
              className="avatar"
              src={
                user?.avatar
                  ? `/upload/${user?.avatar}`
                  : '/images/collections/clt-sample-avatar.png'
              }
              alt="avatar"
            />
            <div className="info">
              <div className="upper">
                <span>{userData?.userName}</span>
                <img
                  className="upper-toggle"
                  src="/images/icons/ic-chevron-down-dark.png"
                  alt="toggle"
                />
              </div>
              <div className="below">
                <span>
                  {userSession.is_admin === 1
                    ? 'Admin'
                    : userSession.user_role_name}
                </span>
              </div>
            </div>
          </div>
          <Dropdown className="action-account-dropdown" title="default">
            <Dropdown.Item onSelect={onUserInfo}>
              Thông tin tài khoản
            </Dropdown.Item>
            <Dropdown.Item onSelect={onLogOut}>Đăng xuất</Dropdown.Item>
          </Dropdown>
        </div>
      </div>
    </header>
  )
}
