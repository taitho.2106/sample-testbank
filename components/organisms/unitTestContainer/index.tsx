import React, { CSSProperties, useEffect, useMemo, useRef, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import { Collapse } from 'react-collapse'
import { Button, Pagination, Popover, Tooltip, Whisper } from "rsuite";

import NoFieldData from '@/componentsV2/NoDataSection/NoFieldData'
import { callApi } from 'api/utils'
import { DateInput } from 'components/atoms/dateInput'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { SelectPicker } from 'components/atoms/selectPicker'
import { MultiSelectPicker } from 'components/atoms/selectPicker/multiSelectPicker'
import { UnitTestTable } from 'components/molecules/unitTestTable'
import useGrade from 'hooks/useGrade'
import useSeries from 'hooks/useSeries'
import { PACKAGES, USER_ROLES } from "interfaces/constants";
import { paths as pathUrl } from 'interfaces/constants'
import { ACTIVE_STATUS, SKILLS_SELECTIONS } from 'interfaces/struct'
import { userInRight } from 'utils'

import { StickyFooter } from '../stickyFooter'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import { filterSeriesByGrade } from 'utils/series/filterSeriesByGrade'
import {paths} from "../../../api/paths";
import { checkAccessPackageLevel } from "@/utils/user";
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import styles from "@/componentsV2/TeacherPages/UnitTestPage/WrapperUnitTestV2.module.scss";
import PopoverUpgradeContent from "../../molecules/unitTestTable/PopoverUpgradeContent";
import { studentPricings } from "@/componentsV2/Home/data/price";
import { dataWithPackage } from "@/componentsV2/TeacherPages/UnitTestPage/data/dataWithPackage";
import useTranslation from "@/hooks/useTranslation";

type PropsType = {
  className?: string
  style?: CSSProperties
}

export const UnitTestContainer = ({ className = '', style }: PropsType) => {
  const router = useRouter()
  const query = router.query;
  const { t } = useTranslation();

  const seriesStart =useRef( query.s?parseInt(query.s as string):null);
  const gradeStart = useRef(query.g?query.g:null);

  const { getGrade } = useGrade()
  const { getSeries } = useSeries()
  const [listSeries, setListSeries] = useState([])
  const { dataPlan, listPlans } = useCurrentUserPackage();
  const [session] = useSession()
  const isSystem = userInRight([USER_ROLES.Operator], session);
  const isStudent = userInRight([-1, USER_ROLES.Student], session);
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session);
  const isSystemPage = !router.query.m
  const isAuthPage = router.query?.m === 'mine'
  const isCreatable = (isTeacher && dataPlan?.code !== PACKAGES.BASIC.CODE && isAuthPage) || (isSystem && isSystemPage)
  const dataDescription = studentPricings.teacher.find(item => item.code === PACKAGES.ADVANCE.CODE)?.descriptions || []
  const dataPackage = listPlans.find((item: any) => item.code === PACKAGES.ADVANCE.CODE)

  const [unitTestList, setUnitTestList] = useState([] as any[])
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPage, setTotalPage] = useState(1)

  const [isOpenFilterCollapse, setIsOpenFilterCollapse] = useState(true)
  const [triggerReset, setTriggerReset] = useState(false)

  const [filterName, setFilterName] = useState('')
  const [filterGrade, setFilterGrade] = useState([] as any[])
  const [filterTime, setFilterTime] = useState('')
  const [filterTotalQuestion, setFilterTotalQuestion] = useState('')
  const [filterSkill, setFilterSkill] = useState([] as any[])
  const [filterSeries, setFilterSeries] = useState([] as any[])
  const [filterStartDate, setFilterStartDate] = useState(null as any)
  const [filterEndDate, setFilterEndDate] = useState(null as any)
  const [filterStatus, setFilterStatus] = useState('' as '' | 'AC' | 'DI')
  const [isDataFilter, setIsDataFilter] = useState(false)

  const [isFilter, setIsFilter] = useState(false)
  const [gradeId, setGradeId] = useState(query.g)
  const [isLoading, setIsLoading] = useState(true)

  const handleNameChange = (val: string) => setFilterName(val)
  const handleNameSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      // handleFilterSubmit(false, { name: val })
      e.target.blur()
    }
  }
  const handleGradeChange = (val: any) => {
    setGradeId(val)
    if (!val) {
      setFilterGrade([])
    } else {
      setFilterGrade([val])
    }
  }
  const handleTimeChange = (val: string) => setFilterTime(!isNaN(Number(val)) ? val : '')
  const handleTimeSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      // handleFilterSubmit(false, { time: val })
      e.target.blur()
    }
  }
  const handleTotalQuestionChange = (val: string) => setFilterTotalQuestion(!isNaN(Number(val)) ? val : '')
  const handleTotalQuestionSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      // handleFilterSubmit(false, { totalQuestion: val })
      e.target.blur()
    }
  }
  const handleSkillChange = (val: any[]) => {
    setFilterSkill([...val])
    // handleFilterSubmit(false, { skills: [...val] })
  }
  const handleSeriesChange = (val: any) => {
    let value:any = []
    if(val){
      value = [val]
    }
    setFilterSeries([...value])
    // handleFilterSubmit(false, { series_id: [...val] })
  }

  const filterBagde = [
    filterGrade.length > 0,
    filterTime ? true : false,
    filterTotalQuestion ? true : false,
    filterSkill.length > 0,
    filterSeries.length > 0,
    filterStartDate ? true : false,
    filterEndDate ? true : false,
    filterStatus ? true : false,
  ].filter((item) => item === true).length
  const checkExistFilter = () =>
    (filterName ||
      filterGrade.length > 0 ||
      filterTime ||
      filterTotalQuestion ||
      filterSkill.length > 0 ||
      filterSeries.length > 0 ||
      filterStartDate ||
      filterEndDate ||
      filterStatus) &&
    isFilter
      ? true
      : false

  const collectData = (isReset: boolean, page = 0, body: any = null) => {
    const collection: any = { page }
    if (!isReset) {
      if (filterName || body?.name) collection['name'] = body?.name || filterName
      if ((filterGrade && filterGrade.length > 0) || (body?.templateLevelId && body.templateLevelId.length > 0))
        collection['template_level_id'] = body?.templateLevelId || filterGrade
      if(filterTime !== '' || body?.time) 
        collection['time'] = body?.time || filterTime
      if (filterTotalQuestion !== '' || body?.totalQuestion)
        collection['total_question'] = body?.totalQuestion || filterTotalQuestion
      if ((filterSkill && filterSkill.length > 0) || (body?.skills && body.skills.length > 0))
        collection['skills'] = body?.skills || filterSkill
      if ((filterSeries && filterSeries.length > 0) || (body?.series_id && body.series_id.length > 0))
        collection['series_id'] = body?.series_id || filterSeries
      if (filterStartDate || body?.startDate) collection['start_date'] = body?.startDate || filterStartDate
      if (filterEndDate || body?.endDate) collection['end_date'] = body?.endDate || filterEndDate
      if (filterStatus || body?.status) collection['status'] = body?.status === '' ? '' : body?.status || filterStatus
    }
    return collection
  }

  const handleItemDelete = async (id: number) => {
    const body = collectData(false, currentPage - 1)
    setIsFilter(true)
    const response = await callApi(`/api/unit-test/${id}`, 'delete')
    if (response) {
      const scope = router.query?.m ? ['mine', 'teacher'].findIndex((item) => item === router.query.m) : null
      const newUnitTest: any = await callApi(paths.api_unit_test_search, 'post', 'Token', {
        ...body,
        scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
      })
      if (newUnitTest?.unitTests?.length > 0){
        setUnitTestList([...newUnitTest.unitTests])
        setTotalPage(newUnitTest?.totalPages)
      }else{
        handlePageChange(newUnitTest?.currentPage > 0 ? newUnitTest?.currentPage : 1)
      }
    }
  }

  const handleFilterSubmit = async (isReset: boolean, customData: any = null) => {
    const body = collectData(isReset, 0, customData)
    setIsFilter(!isReset)
    setIsDataFilter(!isReset)
    setIsLoading(true)
    const scope = router.query?.m ? ['mine', 'teacher'].findIndex((item) => item === router.query.m) : null
    const response: any = await callApi(paths.api_unit_test_search, 'post', 'Token', {
      ...body,
      scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
    })
    setIsLoading(false)
    if (response) {
      if (response?.unitTests) setUnitTestList([...response.unitTests])
      if (response?.currentPage) setCurrentPage(response.currentPage + 1 > 0 ? response.currentPage + 1 : 1)
      if (response?.totalPages) setTotalPage(response.totalPages)
    }
  }

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
      /* you can also use 'auto' behaviour
         in place of 'smooth' */
    })
  }

  const handlePageChange = async (page: number) => {
    const body = collectData(false, page - 1)
    setIsFilter(true)
    scrollToTop()
    setIsLoading(true)
    const scope = router.query?.m ? ['mine', 'teacher'].findIndex((item) => item === router.query.m) : null
    const response: any = await callApi(paths.api_unit_test_search, 'post', 'Token', {
      ...body,
      scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
    })
    setIsLoading(false)
    if (response) {
      if (response?.unitTests) setUnitTestList([...response.unitTests])
      if (response?.currentPage || response.currentPage === 0) setCurrentPage(response.currentPage + 1)
      if (response?.totalPages) setTotalPage(response.totalPages)
    }
  }

  const resetFilterState = () => {
    setFilterGrade([])
    setFilterTime('')
    setFilterTotalQuestion('')
    setFilterSkill([])
    setFilterSeries([])
    setFilterStartDate('')
    setFilterEndDate('')
    setFilterStatus('')
    setFilterName('')
    handleFilterSubmit(true)
    setCurrentPage(1)
  }
  useEffect(() => {
    const dataSeries = filterSeriesByGrade(getSeries,getGrade, gradeId)
    setListSeries(dataSeries)
  }, [gradeId, getSeries])

  useEffect(() => {
    const fetchData = async () => {
      const scope = router.query?.m ? ['mine', 'teacher'].findIndex((item) => item === router.query.m) : null
      const bodyData:any = {
        page: 0,
        scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
        isSystem: router.query?.m === 'mine' ? false : true,
      }
      if(gradeStart.current){
        bodyData.template_level_id = [gradeStart.current]
      }
      if(seriesStart.current){
        bodyData.series_id = [seriesStart.current]
      }
      const response: any = await callApi(paths.api_unit_test_search, 'post', 'Token', bodyData)
      setIsDataFilter(false)
      if (response?.unitTests) setUnitTestList([...response.unitTests])
      setCurrentPage(1)
      if (response?.totalPages) setTotalPage(response.totalPages)
      if (response) setIsLoading(false)
    }
    if(gradeStart.current){
      setFilterGrade([gradeStart.current])
    }
    if(seriesStart.current){
      setFilterSeries([seriesStart.current])
    }
    setTriggerReset(!triggerReset)
    resetFilterState()
    fetchData()
  }, [router.asPath])

  const isDataEmpty = useMemo(() => {
    return !isDataFilter && !isLoading && !unitTestList.length 
  }, [isDataFilter, unitTestList])

  const onKeyDownInputNumber = (e:any,val:any)=>{
    const key = e?.key?.toLowerCase();
    if(["e","+","-"].includes(key)){
      e.preventDefault();
      return;
    }
    if(!val && key == "0"){
      e.preventDefault();
      return;
    }
  }

  const handlleTeacherCreateExam = () => {
    router.push({
      pathname: pathUrl.createUnitTest,
      query: query?.m ? { m: query?.m } : {}
    })
  }

  const renderTable = unitTestList.length > 0 || isLoading;

  return (
    <div className={`m-unittest-template-table-data  ${className}`} style={{ ...(renderTable ? {} : { height: '100%' }),...style }}>
      {!isDataEmpty && (
      <div className="m-unittest-template-table-data__content">
        <Collapse isOpened={isOpenFilterCollapse}>
          <div className="content__sub">
            {getGrade.length>0 && (
              <SelectFilterPicker
                data={getGrade}
                isMultiChoice={false}
                inputSearchShow={false}
                onChange={handleGradeChange}
                className={'__filter-box --grade'}
                triggerReset={triggerReset}
                label={t('common')['grade']}
                style={{ zIndex: 15 }}
                menuSize='lg'
                defaultValue={filterGrade[0]||""}
              />
            )}
            {listSeries.length>0 && (
              <SelectFilterPicker
                data={listSeries}
                isMultiChoice={false}
                inputSearchShow={true}
                onChange={handleSeriesChange}
                className={'__filter-box --series'}
                triggerReset={triggerReset}
                label={t('common')['series']}
                menuSize="xxl"
                defaultValue={filterSeries[0]||""}
              />
            )}
            <InputWithLabel
              className="__filter-box --search"
              label={t('common')['unit-test-name']}
              placeholder={t('common')['placeholder-unitTest-name']}
              triggerReset={triggerReset}
              type="text"
              onBlur={handleNameChange}
              onKeyUp={handleNameSubmit}
              icon={<img src="/images/icons/ic-search-dark.png" alt="search" />}
            />
            <InputWithLabel
              className="__filter-box"
              decimal={0}
              label={t('common')['time']}
              min={0}
              triggerReset={triggerReset}
              type="number"
              onkeyDown={onKeyDownInputNumber}
              onBlur={handleTimeChange}
              onKeyUp={handleTimeSubmit}
            />
            <InputWithLabel
              className="__filter-box"
              decimal={0}
              label={t('common')['total-question']}
              min={0}
              triggerReset={triggerReset}
              type="number"
              onkeyDown={onKeyDownInputNumber}
              onBlur={handleTotalQuestionChange}
              onKeyUp={handleTotalQuestionSubmit}
            />
            <MultiSelectPicker
              className="__filter-box"
              data={SKILLS_SELECTIONS}
              label={t('common')['skill']}
              triggerReset={triggerReset}
              onChange={handleSkillChange}
            />
          </div>
        </Collapse>
        <div className="content__sup">
          <div className="__action-group"></div>
          <div className="__input-group">
            {isCreatable ? (
                <>
                  <Button
                      className="__action-btn"
                      style={{ margin: '0 0.8rem 0 0.8rem' }}
                      onClick={() => {
                        router.push({
                          pathname: '/unit-test/create-new-unittest',
                          query: router.query.m ? { m: router.query.m } : {},
                        })
                      }}
                  >
                    <img src="/images/icons/ic-plus-white.png" alt="create" />
                    <span>{t('common')['create']}</span>
                  </Button>
                </>
            ) : router.query?.m !== 'teacher' && !isStudent && (
              <>
                <Whisper
                    trigger="hover"
                    placement="leftStart"
                    enterable
                    speaker={
                      <Popover className={styles.poperverModal} arrow={false}>
                        <PopoverUpgradeContent
                            dataPackage={dataDescription}
                            price={dataPackage?.exchange_price}
                            itemId={dataPackage?.item_id}
                        />
                      </Popover>}
                >
                  <Button
                      disabled={!isCreatable}
                      className={`__action-btn ${isCreatable ? "" : "__block"}`}
                      style={{ margin: '0 0.8rem 0 0.8rem' }}
                      onClick={() => {
                        isCreatable &&
                        router.push({
                          pathname: '/unit-test/create-new-unittest',
                          query: router.query.m ? { m: router.query.m } : {},
                        })
                      }}
                  >
                    <img src="/images/icons/ic-plus-white.png" alt="create" />
                    <span>{t('common')['create']}</span>
                    {!isCreatable && <div className="__icon-package">{dataWithPackage[PACKAGES.ADVANCE.CODE].icon}</div>}
                  </Button>
                </Whisper>
              </>
            )}
            <Whisper placement="bottom" trigger="hover" speaker={<Tooltip>{t('unit-test')['tooltip-reset']}</Tooltip>}>
              <Button
                className="__sub-btn"
                onClick={() => {
                  setTriggerReset(!triggerReset)
                  resetFilterState()
                  setIsDataFilter(false)
                }}
              >
                <img src="/images/icons/ic-reset-darker.png" alt="reset" />
                <span>{t('common')['default']}</span>
              </Button>
            </Whisper>
            <Button
              className="__main-btn"
              onClick={() => {
                handleFilterSubmit(false)
                setCurrentPage(1)
                setIsDataFilter(true)
              }}
            >
              <img src="/images/icons/ic-search-dark.png" alt="find" />
              <span>{t('common')['search']}</span>
            </Button>
          </div>
        </div>
      </div>
      )}
      {renderTable ? (
        <UnitTestTable
          currentPage={currentPage}
          data={unitTestList}
          isLoading={isLoading}
          setData={setUnitTestList}
          onDelete={handleItemDelete}
        />
      ) : (
        <div className="o-question-drawer__empty" 
          style={{ 
            minHeight: !isDataEmpty ? '50vh' : 'unset', 
            height: isDataEmpty ? '100%' : 'unset',
          }}>
          <NoFieldData
            field='teacher_unit_test'
            isSearchEmpty={isDataFilter}
            handleCreate={handlleTeacherCreateExam}
          />
        </div>
      )}
      {unitTestList.length > 0 && (
        <StickyFooter>
          <div className="__pagination">
            <Pagination
              prev
              next
              ellipsis
              size="md"
              total={totalPage * 10}
              maxButtons={10}
              limit={10}
              activePage={currentPage}
              onChangePage={(page: number) => handlePageChange(page)}
            />
          </div>
        </StickyFooter>
      )}
    </div>
  )
}
