import {  useCallback, useContext, useRef, useState } from "react";

import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { Button } from "rsuite";

import {  WrapperContext } from "@/interfaces/contexts";
import { callApi } from "api/utils";
import { InputWithLabel } from "components/atoms/inputWithLabel";
import { SelectField } from "components/atoms/selectField";
import useNoti from "hooks/useNoti";
import { formatDate } from "utils/string";

import { StickyFooter } from "../stickyFooter";

function IntegrateExamContainer({
    viewMode, 
    data, 
    getThirdParty,
    getSeries, 
    getGrade, 
    updateSuccess
}:any) {
    const formRef = useRef(null)
    const {getNoti} = useNoti()
    const router = useRouter()
    const { globalModal } = useContext(WrapperContext)
    const {
        register,
        formState: { errors },
        setValue,
        handleSubmit,
      } = useForm<any>()
      const checkError = useCallback((errors: any, prop: string) => {
        return errors[prop] ? 'error' : ''
      }, [])
    const onSelectChange = (name:string, value:any) => {
    setValue(name, value,{
        shouldValidate: true,
        })
    }
    
    const handleOnSubmit = (id: any) => {
        router.push(`/unit-test/${id}?mode=edit`)
    }

    const onSubmit = async (formData: any) =>{
        const cloneData:any = {...data, unit_test_origin_id: viewMode === 'create' ? data.id :data.unit_test_origin_id,third_party_code:formData.integrate}
        const response: any = await callApi(
            '/api/unit-test-third-party',
            viewMode === 'edit' ? 'put' : 'post',
            'Token',
            cloneData,
        )
        if(response){
            if(response.code === 3){
                globalModal.setState({
                    id: 'confirm-modal-integrate-exam',
                    type: 'create-integrate-fail-for-grade',
                    content: {
                      closeText: 'Hủy',
                      submitText: 'Chỉnh sửa đề thi',
                      onSubmit: () => {
                        data.unit_test_origin_id ? handleOnSubmit(data.unit_test_origin_id) : handleOnSubmit(data.id)
                      },
                    },
                })
            }
            else if(response.code === 4){
                globalModal.setState({
                    id: 'confirm-modal-integrate-exam',
                    type: 'create-integrate-fail',
                    content: {
                      closeText: 'Hủy',
                      submitText: 'Chỉnh sửa đề thi',
                      onSubmit: () => {
                        data.unit_test_origin_id ? handleOnSubmit(data.unit_test_origin_id) : handleOnSubmit(data.id)
                      },
                    },
                })
            }
            else if(response.code === 5){
                globalModal.setState({
                    id: 'confirm-modal-integrate-exam',
                    type: 'create-integrate-fail-for-grade-and-series',
                    content: {
                      closeText: 'Hủy',
                      submitText: 'Chỉnh sửa đề thi',
                      onSubmit: () => {
                        data.unit_test_origin_id ? handleOnSubmit(data.unit_test_origin_id) : handleOnSubmit(data.id)
                      },
                    },
                })
            }
            else if(response.code === 0 || response.code === 2){
                globalModal.setState({
                    id:'confirm-modal-integrate-exam',
                    type:'integrate-duplicate',
                    content:{
                        submitText:'Xem đề thi tích hợp đã tồn tại',
                        closeText:'Quay lại',
                        third_party_display: getThirdParty.find((m:any)=> m.code === cloneData.third_party_code)?.display,
                        onSubmit:()=>{router.push(`/integrate-exam/${response?.data}?mode=edit`)}
                    }
                })
            }else {
                viewMode === 'create' ? null : updateSuccess()
                globalModal.setState({id:'result-modal-integrate-exam',content:{onSubmit:()=>handleCopyURL(cloneData?.third_party_code,response?.id)}})
            }
        }
    }
    const onError = (error:any) => {
        console.log("error",error);
    }
    const listGradeCode = getGrade.map((m: any) => m.code)
    const listSeriesCode = getSeries.map((m: any) => m.code)
    const integrateGradeId = data?.template_level_id
    const integrateSeriesId = data?.series_id

    const handleCopyUnitTest = (id:number) => {
        if(!listGradeCode.includes(integrateGradeId) && !listSeriesCode.includes(integrateSeriesId)){
            getNoti('error', 'topCenter', 'Vui lòng cập nhật lại Khối lớp và Chương trình đề tích hợp này')
        }
        else if(!listGradeCode.includes(integrateGradeId) && listSeriesCode.includes(integrateSeriesId)){
            getNoti('error', 'topCenter', 'Vui lòng cập nhật lại Khối lớp đề tích hợp này')
        }
        else if(listGradeCode.includes(integrateGradeId) && !listSeriesCode.includes(integrateSeriesId)){
            getNoti('error', 'topCenter', 'Vui lòng cập nhật lại Chương trình đề tích hợp này')
        }
        else{
            router.push(`/integrate-exam/${id}?mode=create`)
        }
    }
    const handleCopyURL = (third_party_code:string,id: number) => {
        const protocol = window.location.protocol
        const hostname = window.location.hostname
        const port = hostname === 'localhost' ? `:${window.location.port}` : ''
        const integrate = getThirdParty.find( (m: any) => m.code === third_party_code)?.code.toString().toLowerCase()
        const url = `${protocol}//${hostname}${port}/${integrate}/${id}`
        navigator.clipboard.writeText(url)
        getNoti('success', 'topEnd', 'Đã copy URL')
    }
    return ( 
        <form
        ref={formRef}
        onSubmit={handleSubmit(onSubmit,onError)}
        >
            <div className="m-integrate-exam__container">
                <div className="m-integrate-exam__title">
                    <span>THÔNG TIN TÍCH HỢP</span>
                </div>
                <div 
                    style={{padding:'3rem 0 1rem 0'}}
                >
                    <div className="m-integrate-exam__form-item">
                        <div className="__form-field">
                            <SelectField 
                                label="Đối tác"
                                name="integrate"
                                defaultValue={viewMode === 'edit' ? data?.third_party_code : ''}
                                data={getThirdParty}
                                register={register('integrate', {
                                    required: 'Vui lòng chọn Đối tác',
                                })}
                                onChange={onSelectChange.bind(null, "integrate")}
                                style={{zIndex:'6'}}
                                className={`${checkError(errors, 'integrate')}`}
                            />
                        </div>
                        <div className="__form-field">
                            <SelectField
                                defaultValue={data?.series_id || ''}
                                label="Chương trình"
                                disabled={true}
                                data={getSeries}
                            />
                        </div>
                    </div>
                    <div className="m-integrate-exam__form-item">
                        <div className="__form-field">
                            <SelectField 
                                defaultValue={data?.template_level_id || ''}
                                label="Khối lớp"
                                disabled={true}
                                data={getGrade}
                            />
                        </div>
                        <div className="__form-field">
                            <InputWithLabel 
                                label="Tên đề thi"
                                defaultValue={data?.name || ''}
                                disabled={true}
                                type="text"
                            />
                        </div>
                    </div>
                </div>
                {errors['integrate'] && (
                     <div className="error">
                    <img src="/pt/images/icons/ic-notifi.svg" alt="notifi" />
                    <span>{errors['integrate'].message}</span>
                    </div>
                )}
               
            </div>
            {viewMode === 'edit' &&(<div className="m-integrate-exam__container">
                <div className="m-integrate-exam__title">
                    <span>LỊCH SỬ THAY ĐỔI</span>
                </div>
                <div style={{padding:'3rem 0 1rem 0', display:'flex',flexDirection:'column'}}
                >
                    <div className="m-integrate-table">
                        <div className="m-integrate-table__item"><span>NGÀY GIỜ</span></div>
                        <div className="m-integrate-table__item"><span>NGƯỜI THỰC HIỆN</span></div>
                        <div className="m-integrate-table__item"><span>HÀNH ĐỘNG</span></div>
                    </div>
                    {data?.created_by && (
                    <div className="m-integrate-table">
                        <div className="m-integrate-table__value"><span>{formatDate(data?.created_date,true)}</span></div>
                        <div className="m-integrate-table__value"><span>{data?.created_by}</span></div>
                        <div className="m-integrate-table__value"><span>Tạo mới</span></div>
                    </div>
                    )}
                    {data?.updated_by &&(
                    <div className="m-integrate-table">
                        <div className="m-integrate-table__value"><span>{formatDate(data?.updated_date,true)}</span></div>
                        <div className="m-integrate-table__value"><span>{data?.updated_by}</span></div>
                        <div className="m-integrate-table__value"><span>Cập nhật</span></div>
                    </div>
                    )}
                </div>
            </div>
            )}
            <StickyFooter className="m-integrate-exam__footer">
                <div className="btn-action" style={viewMode === 'create' ? {justifyContent:'flex-end'} : {}}>
                    {viewMode === 'edit' &&(<div>
                        <Button className="btn-copy-test" onClick={()=>handleCopyUnitTest(data?.unit_test_origin_id)}>
                            <span>Sao chép đề thi</span>
                        </Button>
                        <Button  className="btn-copy-URL" onClick={()=>handleCopyURL(data.third_party_code,data.unit_test_id)}>
                            <span>Sao chép URL</span>
                        </Button>
                    </div>)}
                    <div>
                        <Button type="submit" disabled={errors['integrate'] ? true : false}>
                            <span>Hoàn tất</span>
                        </Button>
                    </div>                    
                </div>
                
            </StickyFooter>
        </form>
     );
}

export default IntegrateExamContainer;