import { useContext, useMemo, useState } from 'react'

import { Whisper, Tooltip } from 'rsuite'

import { CheckBox } from 'components/atoms/checkbox'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { SelectPicker } from 'components/atoms/selectPicker'
import useGrade from 'hooks/useGrade'
import useNoti from 'hooks/useNoti'
import { SingleTemplateContext, WrapperContext } from 'interfaces/contexts'
import { DefaultPropsType } from 'interfaces/types'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import { PACKAGES, USER_ROLES } from "@/interfaces/constants";
import useTranslation from "@/hooks/useTranslation";
import { useSession } from 'next-auth/client'
import { userInRight } from '@/utils/index'

interface Props extends DefaultPropsType {
  isReady: boolean
}

export const TemplateInfoCard = ({ className = '', isReady, style }: Props) => {
  const { t } = useTranslation()
  const { getNoti } = useNoti()
  const {getGrade, gradeList} = useGrade()
  const { globalModal } = useContext(WrapperContext)
  const { mode, templateDetail } = useContext(SingleTemplateContext)
  const { dataPlan } = useCurrentUserPackage();
  const isCreateable = dataPlan?.code !== PACKAGES.BASIC.CODE
  const defaultName = templateDetail.state?.name || ''
  const defaultTime = templateDetail.state?.time || ''
  const defaultTotalQuestion = templateDetail.state?.totalQuestion || ''
  const defaultPoint = templateDetail.state?.point || ''
  const defaultGrade = getGrade.find(
    (item) => item.code === templateDetail.state?.templateLevelId,
  ) || { code: '', display: '' }

  let currentTotalQuestion = 0
  let currentPoint = 0
  templateDetail.state.sections.forEach((item: any) => {
    const countTotal = item.parts[0]?.totalQuestion || 0
    const countPoint = item.parts[0]?.points || 0
    currentTotalQuestion += countTotal
    currentPoint += countPoint
  })
  const [session] = useSession()
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const dataGrade = useMemo(() => {
    return isSystem ? getGrade : gradeList
  }, [dataPlan?.code, isTeacher, getGrade, gradeList])
  const handleNameChange = (str: string) => {
    let detail = templateDetail.state
    if (!detail) detail = {}
    detail.name = str
    templateDetail.setState({ ...detail })
  }

  const handleTimeChange = (str: string) => {
    const parseValue = parseInt(str || '0')
    // if (parseValue <= 0) getNoti('error', 'topEnd', 'Thời lượng phải lớn hơn 0')
    let detail = templateDetail.state
    if (!detail) detail = {}
    detail.time = parseValue
    templateDetail.setState({ ...detail })
  }

  const handleTotalQuestionChange = (value: any) => {
    const parseValue = parseInt(value || '0')
    // if (parseValue <= 0)
    //   getNoti('error', 'topEnd', 'Tổng số câu hỏi phải lớn hơn 0')
    const detail = templateDetail.state || {}
    detail.totalQuestion = parseValue
    templateDetail.setState({ ...detail })
  }

  const handleGradeChange = (value: any) => {
    let detail = templateDetail.state
    if (!detail) detail = {}
    detail.templateLevelId = value
    templateDetail.setState({ ...detail })
  }

  const handlePointChange = (str: string) => {
    const parseValue = parseFloat(str || '0')
    // if (parseValue <= 0) getNoti('error', 'topEnd', 'Tổng điểm phải lớn hơn 0')
    let detail = templateDetail.state
    if (!detail) detail = {}
    detail.point = parseValue
    templateDetail.setState({ ...detail })
  }

  const handleStatusChange = () => {
    const detail = templateDetail.state
    if ([true, false].includes(detail?.status)) {
      const currentValue = detail.status
      detail.status = !currentValue
    } else {
      detail.status = true
    }
    templateDetail.setState({ ...detail })
  }
  const onKeyDownInputNumber = (e:any,val:any)=>{
    const key = e?.key?.toLowerCase();
    if(["e","+","-"].includes(key)){
      e.preventDefault();
      return;
    }
    if(!val && key == "0"){
      e.preventDefault();
      return;
    }
  }
  if (['detail', 'update'].includes(mode) && !templateDetail.state?.id)
    return <></>
  return (
    <div className={`a-template-info-card ${className}`} style={style}>
      <div className="a-template-info-card__container">
        <InputWithLabel
          className="mainbox"
          defaultValue={defaultName}
          disabled={mode === 'detail' || !isCreateable}
          label={t('table')['template-name']}
          type="text"
          onBlur={handleNameChange}
          maxLength={100}
          style={{ maxWidth: '36rem' }}
        />
        {dataGrade.length>0 && (
          <SelectFilterPicker
            data={dataGrade}
            defaultValue={templateDetail.state?.templateLevelId}
            isMultiChoice={false}
            inputSearchShow={false}
            onChange={handleGradeChange}
            className="midbox"
            label={t('common')['grade']}
            menuSize="lg"
            disabled={mode === 'detail' || !isCreateable}
            isShowSelectAll={false}
          ></SelectFilterPicker>
        )}
        <InputWithLabel
          className="midbox"
          decimal={0}
          defaultValue={defaultTime}
          disabled={mode === 'detail' || !isCreateable}
          label={t('common')['time-2']}
          max={32767}
          min={1}
          type="number"
          onBlur={handleTimeChange}
          toolTip={t('common-whisper')['time-required']}
          status={
            templateDetail.state?.time != 0? 'default'
            : 'danger'
          }
          onkeyDown={onKeyDownInputNumber}
        />
        <InputWithLabel
          className="midbox"
          defaultValue={defaultTotalQuestion}
          disabled={mode === 'detail' || !isCreateable}
          min={0}
          placeholder={t('common')['total-question']}
          toolTip={t('common-whisper')['total-question-required']}
          status={
            Math.round(templateDetail.state?.totalQuestion) ===
            Math.round(currentTotalQuestion)
              ? 'default'
              : 'danger'
          }
          type="number"
          onkeyDown={onKeyDownInputNumber}
          onBlur={handleTotalQuestionChange}
          customLabel={
            <span>
              {t('common')['total-question']}{' '}
              <TooltipNumberBadge
                tooltip={
                  Math.round(templateDetail.state?.totalQuestion) ===
                  Math.round(currentTotalQuestion)
                    ? t('common-whisper')['total-question-validate']
                    : t('common-whisper')['total-question-error']
                }
                type={
                  Math.round(templateDetail.state?.totalQuestion) ===
                  Math.round(currentTotalQuestion)
                    ? 'success'
                    : 'danger'
                }
                value={Math.round(currentTotalQuestion)}
              />
            </span>
          }
        />
        <InputWithLabel
          className="midbox"
          decimal={2}
          defaultValue={defaultPoint}
          min={0}
          disabled={mode === 'detail' || !isCreateable}
          placeholder={t('common')['total-point']}
          toolTip={t('common-whisper')['point-required']}
          status={
            Math.round(templateDetail.state?.point * 100) / 100 ===
            Math.round(currentPoint * 100) / 100
              ? 'default'
              : 'danger'
          }
          type="number"
          onkeyDown={onKeyDownInputNumber}
          onBlur={handlePointChange}
          customLabel={
            <span>
              {t('common')['total-point']}{' '}
              <TooltipNumberBadge
                tooltip={
                  Math.round(templateDetail.state?.point * 100) / 100 ===
                  Math.round(currentPoint * 100) / 100
                    ? t('common-whisper')['point-validate']
                    : t('common-whisper')['point-error']
                }
                type={
                  Math.round(templateDetail.state?.point * 100) / 100 ===
                  Math.round(currentPoint * 100) / 100
                    ? 'success'
                    : 'danger'
                }
                value={Math.round(currentPoint * 100) / 100}
              />
            </span>
          }
        />
        {['detail', 'update'].includes(mode) && (
          <Whisper
            placement="top"
            trigger="hover"
            speaker={
              <Tooltip>{t('unit-test')['status-template']}</Tooltip>
            }
          >
            <div className="midbox __checkbox">
              <CheckBox
                checked={templateDetail.state?.status ? true : false}
                disabled={mode === 'detail'}
                onChange={handleStatusChange}
              />
              <span>{t('unit-test')['status-active']}</span>
            </div>
          </Whisper>
        )}
      </div>
    </div>
  )
}

interface TooltipNumberBadgeProps {
  tooltip: string
  type: 'danger' | 'success'
  value: number
}

const TooltipNumberBadge = ({
  tooltip,
  type,
  value,
}: TooltipNumberBadgeProps) => {
  
  return (
    <Whisper
      controlId="control-id-hover"
      placement="topEnd"
      trigger="hover"
      speaker={
        <Tooltip className="tooltip-number-badge-input" data-type={type}>
          {tooltip}
        </Tooltip>
      }
    >
      <span
        style={{
          marginLeft: '-1rem',
          padding: '1rem 0 1rem 1rem',
          color: type === 'success' ? '#568745' : '#c96951',
          cursor: 'pointer',
          pointerEvents: 'all',
        }}
      >
        {(value || value == 0) && <>({value})</>}
      </span>
    </Whisper>
  )
}
