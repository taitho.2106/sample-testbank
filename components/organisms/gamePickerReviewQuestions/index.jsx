import { Fragment, useContext, useEffect } from 'react'

// import { questionListTransform } from '../../../api/dataTransform'
import { PRACTICE_TEST_ACTIVITY } from '@/interfaces/struct'

import { Game1 } from './games/game1'
import { Game11 } from './games/game11'
import { Game11Audio } from './games/game11/Game11Audio'
import { Game12 } from './games/game12'
import { Game12Audio } from './games/game12/Game12Audio'
import { Game14 } from './games/game14'
import { Game15 } from './games/game15'
import { Game15Audio } from './games/game15/Game15Audio'
import { Game2 } from './games/game2'
import { Game3 } from './games/game3'
import { Game3Audio } from './games/game3/Game3Audio'
import { Game4 } from './games/game4'
import { Game5 } from './games/game5'
import { Game6 } from './games/game6'
import { Game7 } from './games/game7'
import { Game8 } from './games/game8'
import { Game8Audio } from './games/game8/Game8Audio'
import { Game9 } from './games/game9'
import { Game9Audio } from './games/game9/Game9Audio'
import { GameDD2 } from './games/gameDD2'
import GameDrawLine from './games/gameDrawLine'
import GameDrawLine2 from './games/gameDrawLine2'
import GameDrawLine3 from './games/gameDrawLine3'
import GameDrawShape1 from './games/gameDrawShape1'
import { GameFB4_v2 } from './games/gameFB4_v2'
import { GameFB6 } from './games/gameFB6'
import { GameFB7 } from './games/gameFB7'
import { GameFB8 } from './games/gameFB8'
import GameFillInColour1 from './games/gameFillInColour1'
import GameFillInColour2 from './games/gameFillInColour2'
import { GameMC10 } from './games/gameMC10'
import { GameMC11 } from './games/gameMC11'
import { GameMC1_v2 } from './games/gameMC1_v2'
import { GameMC2 } from './games/gameMC2'
import { GameMC7 } from './games/gameMC7'
import { GameMC8 } from './games/gameMC8'
import { GameMC9 } from './games/gameMC9'
import { GameMG4 } from './games/gameMG4'
import { GameMG5 } from './games/gameMG5'
import { GameMG6 } from './games/gameMG6'
import { GameMix1 } from './games/gameMix1'
import { GameMix2 } from './games/gameMix2'
import GameSelectObject1 from './games/gameSelectObject1'
import { GameSL2 } from './games/gameSL2'
import { GameTF3 } from './games/gameTF3'
import GameTF4 from './games/gameTF4'
import { GameTF5 } from './games/gameTF5'

export const GamePickerReviewQuestions = ({data, type,id}) => {

  const renderGamePicker = (type, data) =>{
    {switch (type) {
      case 1:
        return <Game1 key={data.id} data={data} />
      case 2:
        return <Game2 v data={data} />
      case 3:
        return data.audioInstruction ? (
          <Game3Audio key={data.id} data={data} />
        ) : (
          data.questionInstruction ?
          <GameFB6 key={data.id} data={data} />
          : <Game3 key={data.id} data={data} />
        )
      case 4:
        return <Game4 key={data.id} data={data} />
      case 5:
        return <Game5 key={data.id} data={data} />
      case 6:
        return <Game6 key={data.id} data={data} />
      case 7:
        return <Game7 key={data.id} data={data} />
      case 8:
        return data.audioInstruction ? (
          <Game8Audio key={data.id} data={data} />
        ) : data.imageInstruction ? (<GameTF4 key={data.id} data={data}/>) : (
          <Game8 key={data.id} data={data} />
        )
      case 9:
        return data.audioInstruction ? (
          <Game9Audio key={data.id} data={data} />
        ) : (
          <Game9 key={data.id} data={data} />
        )
      case 11:
        return data.audioInstruction ? (
          <Game11Audio key={data.id} data={data} />
        ) : (
          <Game11 key={data.id} data={data} />
        )
      case 12:
        return data.audioInstruction ? (
          <Game12Audio key={data.id} data={data} />
        ) : (
          <Game12 key={data.id} data={data} />
        )
      case 14:
        return <Game14 key={data.id} data={data} />
      case 15:
        return data.audioInstruction ? (
          <Game15Audio key={data.id} data={data} />
        ) : (
          <Game15 key={data.id} data={data} />
        )
      case 16:
        return <GameSL2 key={data.id} data={data}/>
      case 17:
        return <GameMix1 key={data.id} data={data}/>
      case 18:
        return <GameMix2 key={data.id} data={data}/>
      case 19:
        return <GameTF3 key={data.id} data={data}/>
      case 20: //MC1_v2
        return <GameMC1_v2 key={data.id} data={data} />
      case 21: //MC2_v2
        return <GameMC2 key={data.id} data={data} />
      case 22:
        return <GameMC7 key={data.id} data={data} />
      case 23: //MG4
        return <GameMG4 key={data.id} data={data} />
      case 24: //MC9
        return <GameMC9 key={data.id} data={data} />
      case 25: //FB4
        return <GameFB4_v2 key={data.id} data={data} />
      case 26: //FB7 new
        return <GameFB7 key={data.id} data={data} />
      case 27: //FB8 new
        return <GameFB8 key={data.id} data={data} />
      case 28: 
        return <GameDrawLine key={data.id} data={data} />
      case 29:
        return <GameTF5 key={data.id} data={data} />
      case 30: 
        return <GameMC8 key={data.id} data={data} />
      case 31: 
        return <GameSelectObject1 key={data.id} data={data} />
      case 32: 
        return <GameMC10 key={data.id} data={data} />
      case 34: 
        return <GameDrawLine2 key={data.id} data={data} />
      case 36: 
        return <GameDD2 key={data.id} data={data} />
      case 37: 
        return <GameMG5 key={data.id} data={data} />
      case 33: 
        return <GameMG6 key={data.id} data={data} />
      case 38: 
        return <GameDrawShape1 key={data.id} data={data} />
      case 39: 
        return <GameFillInColour1 key={data.id} data={data} />
      case 40: 
        return <GameDrawLine3 key={data.id} data={data} />
      case 42: 
        return <GameFillInColour2 key={data.id} data={data} />
      case 41: 
        return <GameMC11 key={data.id} data={data} />
      default:
        return <Fragment key={data.id}></Fragment>
    }}
  } 
  return (
    <div className="pt-o-game-picker" id={id} style={{maxHeight: '90%'}}>
      {renderGamePicker(PRACTICE_TEST_ACTIVITY[type], data)}
    </div>
  )
}
