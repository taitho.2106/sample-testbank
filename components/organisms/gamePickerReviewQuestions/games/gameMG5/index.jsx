import { useState } from 'react'

import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { GameWrapper } from '../../../../../practiceTest/components/templates/gameWrapper'

export const GameMG5 = ({ data }) => {
  const answers = data?.answers
  const answersExample = data?.answersExample ?? []
  const instruction = data?.instruction ?? ''
  let defaultRightColumn = []
  let leftColumn = []
  let trueAnswer = []
  let wrongAnswer = Array.from(Array(answers.length), (e, i) => i)

  answers.forEach((item, i) => {
    item.left && leftColumn.push(item.left)
    defaultRightColumn.push({
      id: `item-${i}`,
      content: item.right,
      index: i,
      isMatched: false,
    })
    if (item.rightAnswerPosition !== -1) {
      trueAnswer.push(item.rightAnswerPosition)
      wrongAnswer = wrongAnswer.filter((filter) => filter !== item.rightAnswerPosition)
    }
  })

  const [isZoom, setIsZoom] = useState(false)
  const [isZoomA, setIsZoomA] = useState(false)
  const [imageStr, setImageStr] = useState('')

  const orderRightColumn = Array.from(Array(defaultRightColumn.length), (e, i) => ({
    index: i,
    isMatched: false,
  }))

  let defaultArr = []
  orderRightColumn.forEach((item, i) => {
    const target = defaultRightColumn[i]
    if (target) {
      target.isMatched = item.isMatched
      defaultArr.push(target)
    }
  })
  const [rightColumn, setRightColumn] = useState({ items: defaultArr })

  return (
    <GameWrapper className="pt-o-game-mg5">
      <div className="pt-o-game-mg5__container">
        <div className="pt-o-game-mg5__right">
          <div className="__header">
            <FormatText tag="p">{instruction}</FormatText>
          </div>
          <div className="content-wrapper">
            <div className="__content">
              {answersExample && answersExample.length > 0 && (
                <div>
                  <div className="__example">
                    <span className="__title">Example:</span>
                    <div className="__center-column">
                      <div className={`__left-item`}>
                        <div className="__left-item__image">
                          <img src={answersExample[0].image} alt="" onClick={() => setIsZoom(true)} />
                          <ImageZooming
                            data={{ alt: 'img', src: `${answersExample[0].image}` }}
                            status={{
                              state: isZoom,
                              setState: setIsZoom,
                            }}
                          />
                        </div>
                        <span className="__left-item__text">{'__' + answersExample[0].left}</span>
                        <div className="__left-item__linked">
                          <div></div>
                          <div></div>
                        </div>
                      </div>
                      <div className={`__right-item`}>
                        <div className="__right-item__linked">
                          <div></div>
                          <div></div>
                        </div>
                        <span className="__right-item__text">{answersExample[0].right}</span>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className="__title-question">
                <span className="__title">{`Question${
                  leftColumn.length > 1 ? 's' : ''
                }:`}</span>
              </div>
              <div>
                <ImageZooming
                  data={{ alt: 'img', src: `${imageStr}` }}
                  status={{
                    state: isZoomA,
                    setState: setIsZoomA,
                  }}
                />
                <div className={`__left`}>
                  {leftColumn.map((item, i) => (
                    <div key={i} className={`__tag`}>
                      {' '}
                      <div className="__image">
                        <img
                          src={answers[i].image}
                          alt=""
                          onClick={() => {
                            setImageStr(answers[i].image)
                            setIsZoomA(true)
                          }}
                        />
                      </div>
                      <span>{'__' + item}</span>
                    </div>
                  ))}
                </div>
                <div className={`__right`}>
                  {rightColumn.items.map((item, index) => (
                    <div key={item.id} className="__item">
                      <div className="__tag">
                        <span>{item.content}</span>
                        <div className="__circle"></div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </GameWrapper>
  )
}
