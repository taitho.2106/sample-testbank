import { Fragment, useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml, formatHtmlText } from 'utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { BlockBottomGradientWithHeader } from '../../../../../practiceTest/components/organisms/blockBottomGradient/BlockBottomGradientWithHeader'

export const GameFB8 = ({ data }) => {
  // console.log("dataa===FB8===",data)
  let answerGroup = data.answers || []
  let answerData = []
  for (let i=0; i< answerGroup.length; i++){
      const item = answerGroup[i];
      if(answerGroup[i].answer.includes('/')){
          const answers = answerGroup[i].answer.split("/")
          const images = answerGroup[i].image.split("|")
          for(let j=0; j< answers.length; j++){
              answerData.push(
                  {
                  ...item,answer:answers[j], image:images[j]
                  }
              )
          }
      }
      else{
        answerData.push(item)
      }
  }

  const exampleKeyWord = answerData.filter((ans) => ans.answer.startsWith('*'))
  const answerKeyWord = answerData.filter((ans) => !ans.answer.startsWith('*'))

  const imageNote = data.audioScript 
  const answers = data?.answers || []
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const question = data?.question || ''
  const indexEx = answers.findIndex((item)=> item.answer.startsWith('*'))
//   const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
  const questionArr = convertStrToHtml(question).split('%s%')
  
  const contentRef = useRef(null)

  

  useEffect(()=>{
      if(indexEx !== -1){
          const example = contentRef.current.querySelectorAll('.__editableEX')            
          example.forEach((item, i)=>{
              item.innerHTML = answers[indexEx].answer.substring(1)
          })
      }
  },[])

  const [allKeyWord, setAllKeyWord] = useState([])
  useEffect(() => {
      let currentIndex = answerKeyWord.length
      let randomIndex
      while(currentIndex !== 0){
        randomIndex = Math.floor(Math.random() * currentIndex)
        currentIndex--
        [answerKeyWord[currentIndex], answerKeyWord[randomIndex]] = [answerKeyWord[randomIndex], answerKeyWord[currentIndex]];
        // if(answerGroup[currentIndex].im)
    }
    setAllKeyWord([...exampleKeyWord, ...answerKeyWord])
  }, [])

  const renderClassName = (value) => {
      let answerClassName = ''
      if (value?.length === 4) {
          answerClassName = '--images-flex-wrap'
      }
      else{
          answerClassName = '--images-multi-line'
      }
      return answerClassName
  }

  const [isZoomMain, setIsZoomMain] = useState(false)
  const [isZoomKey, setIsZoomKey] = useState(false)
  const [zoomStrMain, setZoomStrMain] = useState('')
  const [zoomStrKey, setZoomStrKey] = useState('')

  let x = -1;

  return (
      <div className="pt-o-game-fb8-image__container">
          <div className='pt-o-game-fb8-image__left'>
              <div className="__instruction">
                  <span>{instruction}</span>
              </div>
              <div className='box-all-images'>
                  <div className='__image-instruction'>
                      <div className='image-main'>
                          <img 
                              src={getFullUrl(imageInstruction)}
                              alt='instruction_image'
                              onClick={() => {
                                  // setIsZoom(true)
                                  setIsZoomMain(true)
                                  setZoomStrMain(getFullUrl(imageInstruction))
                              }}
                          />
                          {zoomStrMain.length > 0 && (
                          <ImageZooming
                              data={{
                                  alt: `instruction_image`,
                                  src: `${zoomStrMain}`,
                              }}
                              status={{
                                  state: isZoomMain,
                                  setState: setIsZoomMain,
                              }}
                          />  
                          )}
                      </div>
                      <div className='text-main'>
                          <span>{imageNote}</span>
                      </div>
                  </div>
                  <div className={`list-image-answers`}>
                  {zoomStrKey.length > 0 && (
                      <ImageZooming
                          data={{
                              alt: 'image_answer',
                              src: `${zoomStrKey}`,
                          }}
                          status={{
                              state: isZoomKey,
                              setState: setIsZoomKey,
                          }}
                      />
                  )}
                  {allKeyWord.map((answerItem, index) => {
                      return (
                          <div 
                              key={`image-ans-${index}`}
                              className={`list-image-answers-cell ${renderClassName(answerData)}`}
                          >   
                              
                              <div className='list-image-answers-cell__item' key={index}>
                                  <img 
                                      src={getFullUrl(answerItem.image)} 
                                      alt='image_answer'
                                      onClick={() => {
                                          setIsZoomKey(true)
                                          setZoomStrKey(getFullUrl(answerItem.image))
                                      }}
                                  />
                              </div>
                              <div className={`list-image-answers-cell__text ${answerItem.answer.startsWith('*') ? '--example' : '--keyword'}`}>
                                  <span>{answerItem.answer.replace(/\*/g, '')}</span>
                              </div>
                              
                          </div>
                      )
                  })}
                  </div>

              </div>
          </div>
              <div className='pt-o-game-fb8-image__right'>
              <BlockBottomGradientWithHeader
                  headerChildren={
                  <CustomHeading tag="h6" className="__heading">
                      <FormatText tag="span">{instruction}</FormatText>
                  </CustomHeading>
                  }
              >
                  <Scrollbars universal={true}>
                  <div ref={contentRef} className="__content">
                      {questionArr.map((item, i) => {
                      if(i === 0  || i === indexEx + 1){

                      }else{
                          x = x + 1
                      }
                      return(
                      <Fragment key={i}>
                          {i !== 0 && i !== indexEx + 1 && (
                          <span 
                              data-index={x}
                              className={`__editable --empty`}
                              contentEditable={false}
                              style={{
                                  pointerEvents:'none'
                              }}
                          ></span>
                          )}
                          {i !== 0 && i === indexEx + 1 && (
                          <span
                              className={`__editableEx`}
                              contentEditable={false}
                          >
                              {answers[indexEx].answer.substring(1)}
                          </span>
                          )}
                          <span
                          dangerouslySetInnerHTML={{
                              __html: (item),
                          }}
                          ></span>
                      </Fragment>
                      )})}
                  </div>
                  </Scrollbars>
              </BlockBottomGradientWithHeader>
              </div>
      </div>
  )
}
const getFullUrl = (url) => {
  const result = ''
  if (url != null && url != '') {
    return `${location.origin}/upload/${url}`
  }
  return result
}
