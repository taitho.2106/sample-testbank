import { useRef, useState, useEffect } from 'react'

import { useWindowSize } from 'utils/hook';

import { FormatText } from '../../../../../practiceTest/components/molecules/formatText';
import { GameWrapper } from '../../../../../practiceTest/components/templates/gameWrapper';


const LIST_COLOR=[
  { color:'000000',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '00000080'
  },
  { color:'ffffff',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'd4dde780'
  },
  { color:'880015',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '88001580'
  },
  { color:'ed1c24',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'ed1c2480'
  },
  { color:'ff7f27',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'ff7f2780'
  },
  { color:'fcfb2d',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'fcfb2d80'
  },
  { color:'62aa2d',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '62aa2d80'
  },
  { color:'b5e61d',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'b5e61d80'
  },
  {color:'3d009e',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity:'3d009e80'
  },
  {color:'038af9',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '038af980'
  },
  {color:'8400ab',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '8400ab80'
  },
  {color:'c8bfe7',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'c8bfe780'
  },
  {color:'773829',
  icon:'/images/icons/icon-selected-colour-white.png',
  opacity: '77382980'
  },
  {color:'afafaf',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'afafaf80'
  },
  {color:'fb2576',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'fb257680'
  }
]
export default function GameFillInColour2({ data }) {
  
  useEffect(() => {
    window.timeout_select_object_1=null;
   }, [])
  const answer = data?.answers.filter(item => !item.isExam) ?? []
  const indexExample = data?.answers?.findIndex(item => item.isExam)
  const isPortrait = useRef( window.matchMedia("(orientation: portrait)").matches)
  const instruction = data?.instruction || ''

  const ratio = useRef({ x: 0, y: 0 })
  const renderImage = (e) => {
    try {
     
      const checkEquationValue = (a, b) => {
        if (a - b > 4 || b - a > 4) {
          return false
        } else {
          return true
        }
      }
      const contentImage = document.getElementById('pt-o-game-fill-in-colour-2-content-id')
      const imageElement = document.getElementsByClassName('image-instruction')[0]
      const wraperImage = document.querySelector('.wrap-image')
      const paddingTop = window.getComputedStyle(contentImage,null).getPropertyValue('padding-top')

      if(isPortrait.current && width <= 480){
        const heightContent = contentImage.offsetHeight - 2 * parseInt(paddingTop)
        if(imageElement.naturalHeight > heightContent && imageElement.naturalHeight >= imageElement.naturalWidth){
          imageElement.style.height = `${heightContent}px`
        }
      }
      const imageInstruction = e.target
      imageInstruction.style.visibility = 'visible'
      
      if (
        !checkEquationValue(
          imageInstruction.offsetHeight,
          wraperImage.offsetHeight,
        )
      ) {
        const imgH = wraperImage.offsetHeight
        const imgW =
        wraperImage.offsetHeight *
          (imageInstruction.naturalWidth / imageInstruction.naturalHeight)
          wraperImage.style.height = `${imgH}px`
          wraperImage.style.width = `${imgW}px`
        imageInstruction.style.height = `${imgH}px`
        imageInstruction.style.width = `${imgW}px`
        renderImage(e)
        return
      }
      ratio.current.x = imageElement.offsetWidth / imageInstruction.naturalWidth
      ratio.current.y = imageElement.offsetHeight / imageInstruction.naturalHeight
      if(wraperImage.offsetHeight > imageInstruction.naturalHeight){
        wraperImage.style.height = `${imageInstruction.naturalHeight}px`
      }
      const divImageItem = document.querySelectorAll('.item-image')
      
      divImageItem.forEach((item,mIndex)=>{
        
        const imageItem = new Image()
        imageItem.onload = function(){
          if(imageItem.complete && imageItem.naturalHeight !== 0){
            const canvasItem = document.querySelector(`#canvas-image-item-${mIndex}`)
            const p = data?.answers[mIndex].position.split(':')
            const width = (p[2] - p[0]) * ratio.current.x
            const height = (p[3] - p[1]) * ratio.current.y
            const left = p[0] * ratio.current.x
            const top = p[1] * ratio.current.y
            
            item.style.position = 'absolute'
            item.style.display = 'block'
            item.style.height = `${height}px`
            item.style.width = `${width}px`
            item.style.top = `${top}px`
            item.style.left = `${left}px`

            canvasItem.height = height
            canvasItem.width = width
            clearCanvas(canvasItem)
            canvasItem.width = width
            canvasItem.height = height
            if(mIndex === indexExample){
              const image = item.querySelector('.item-image-content')
              const ctx = canvasItem.getContext('2d')
              ctx.beginPath()
              ctx.drawImage(image, 0, 0, canvasItem.width, canvasItem.height)
              ctx.globalCompositeOperation = 'source-in'
                
              ctx.fillStyle = `#${data?.answers[indexExample].color}80`
              ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
              // ctx.fill()
              image.style.display = 'none'
            }else{
              // console.log("draw");
              const image = item.querySelector('.item-image-content')
              const ctx = canvasItem.getContext('2d')
              ctx.beginPath()
              ctx.drawImage(image, 0, 0, canvasItem.width, canvasItem.height)
              ctx.globalCompositeOperation = 'source-in'
                
              ctx.fillStyle = '#00000000'
              ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
              // ctx.fill()
              image.style.display = 'none'
            }
          }
        }
        imageItem.src = data?.answers[mIndex].image
      })   
      
    } catch (error) {
      console.log("errrrr-----",error)
    }
  }
  const [width, height] = useWindowSize()

  useEffect(() => {
    if(width ==0 || height ==0) return
    isPortrait.current = window.matchMedia("(orientation: portrait)").matches;
    if (window.timeout_select_object_1) {
      clearTimeout(window.timeout_select_object_1)
    }
    const imageItem = document.querySelectorAll(
      '#pt-o-game-fill-in-colour-2-content-id .item-image',
    )
    imageItem.forEach(m=> m.style.display = "none")
    const imageInstruction = document.getElementsByClassName('image-instruction')[0]
    //set size default
    // imageInstruction.style.visibility = 'hidden'
    imageInstruction.style.height = ""
    imageInstruction.style.width= "";
    imageInstruction.parentElement.style.height = ""
    imageInstruction.parentElement.style.width= "";
    imageInstruction.parentElement.parentElement.style.height = ""
    imageInstruction.parentElement.parentElement.style.width= "";
      
    window.timeout_draw_line_1 = setTimeout(() => {
      imageInstruction.src = `${data?.imageInstruction}?t=${new Date().getMilliseconds()}`
    }, 250)
  }, [width, height])
  
 
  const clearCanvas = (canvas) => {
    if(!canvas) return;
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
  }

const generateClassName = (i) => {
  if(i >= 0){
    let className = ''
    if(!className){
      className = 'image-default'
    }
    return className
  }
} 

let iAnswer = -1
  return (
    <GameWrapper className="pt-o-game-fill-in-colour-2">
      <div
        className="pt-o-game-fill-in-colour-2__container"
      >
        <>
          <div
            className="pt-o-game-fill-in-colour-2__right"
          >
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-fill-in-colour-2-content-id">
              <div className='__temp_column'>
              </div>
              <div className='__content_image'>
                <div className="wrap-image" style={{maxWidth:"100%"}}>
                    <img
                      className="image-instruction"
                      alt="image-instruction"
                      src={data?.imageInstruction}
                      style={{ visibility: 'hidden', height: '100%' ,maxWidth:"100%"}}
                      onLoad={(e)=>renderImage(e)}
                    />
                    {data?.answers?.map((item, mIndex) => {
                      if(!item.isExam){
                        iAnswer++
                      }
                      return(
                      <div
                        key={item.key}
                        id={item.key}
                        data-index={`${mIndex}`}
                        data-ianswer={`${item.isExam ? -1 : iAnswer}`}
                        className={`item-image ${item.isExam ? '--example-div' : `--answer-div ${generateClassName(iAnswer)}`}`}
                        style={{
                          pointerEvents:'none',
                        }}
                      >
                        <img 
                          src={item.image}
                          alt=''
                          className={`item-image-content ${mIndex}`}
                        />
                        <canvas
                          id={`canvas-image-item-${mIndex}`}
                        ></canvas>
                        {!item.isExam &&(
                        <>
                          <img 
                            src='/images/icons/ic-true-so.png'
                            alt=''
                            className='icon-success-image'
                          />
                          <img 
                            src='/images/icons/ic-false-so.png'
                            alt=''
                            className='icon-danger-image'
                          />
                        </>)}
                      </div>
                    )})}
                </div>
              </div>
              <div className='__content_colour'>
                <div className='__colour_list'>
                  {LIST_COLOR.map((item,mIndex)=>
                    <div key={`color__${mIndex}`}
                      className='__colour_item'
                      data-index={mIndex}
                      data-color={item.color}
                      data-opacity={item.opacity}
                      style={{
                        backgroundColor: `#${item.color}`,
                        cursor: 'default'
                      }}
                    >
                      <img 
                        className='icon-selected'
                        src={item.icon}
                        alt=''
                      />
                    </div>
                  )}
                </div>
                <div className='__remove_colour_wrap'>
                  <img 
                    src='/images/icons/ic-remove-colour.png'
                    alt=''
                    className='__remove_colour'
                    style={{
                      cursor: 'default'
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </GameWrapper>
  )
}
