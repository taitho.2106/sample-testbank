import { convertStrToHtml } from 'utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'

export const GameMC8 = ({ data }) => {
    const instruction = data?.instruction || ''
    const questionsGroup = data?.questionsGroup || []
    
    const questionsGroupExample = questionsGroup.filter((m) =>
        m.question.startsWith('*'),
    )
    const questionsGroupGeneral = questionsGroup.filter(
        (m) => !m.question.startsWith('*'),
    )

    const renderClassName = (value) => {
        let answerClassName = ''
        if (value?.answers?.length === 4) {
            answerClassName = '--images-flex-wrap'
        }
        else{
            answerClassName = '--images-multi-line'
        }
    return answerClassName
    }

    const renderClassNameEx = (value) => {
        let answerClassName = ''
        if (value?.answers?.length === 4) {
            answerClassName = '--images-flex-wrap'
        }
        else{
            answerClassName = '--images-multi-line'
        }
        return answerClassName
    }
    
    return (
        <div className="pt-o-game-mc8" style={{width:'100%', height:'100%'}}>
            <div className='pt-o-game-mc8__container'>
                <div className="pt-o-game-mc8__right">
                    <div className="__header">
                        <FormatText tag="p">{instruction}</FormatText>
                    </div>
                    <div className="__content">
                    {questionsGroupExample && questionsGroupExample.length > 0 && (
                        <div className="__content-example">
                            <div className="__title">
                                <span>
                                    {`Example${questionsGroupExample.length > 1 ? 's' : ''}:`}
                                </span>
                            </div>
                            <div className={`__content`}>
                            {questionsGroupExample.map((itemQuestion, index) => {
                                const html = convertStrToHtml(itemQuestion.question.substring(1),).replaceAll('%s%', `<div class="__space"></div>`)
                                return (
                                <div
                                    key={`question-${index}`}
                                    className={`mc8-example-group`}
                                >
                                    <div className={`mc8-example-group__question`}>
                                        <div className="mc8-example-group__question__title">
                                            <span
                                            dangerouslySetInnerHTML={{
                                                __html: html,
                                            }}
                                            ></span>
                                        </div>
                                    </div>
                                    <div className={`mc8-example-group__radio-answer ${renderClassNameEx(itemQuestion)}`}>
                                    {itemQuestion.answers.map((itemChild, i) => (
                                        <div 
                                            key={`question-ans-${i}`}
                                            className="mc8-example-group__radio-answer__ex-item"
                                            style={{
                                                boxShadow: itemChild.isCorrect ? '0px 0px 0px 5px #2dd238' : '0px 0px 0px 2px #d4e9ff'
                                            }}
                                        >
                                            <img src={itemChild.imageAns} alt=''/>
                                            <div className={`icon-true ${itemChild.isCorrect}`}>
                                                <img 
                                                    src={'/images/icons/ic-checked-true.png'}
                                                    alt=''
                                                />
                                            </div>
                                        </div>
                                    ))}
                                    </div>
                                </div>
                                )
                            })}
                            </div>
                        </div>
                        )}
                        {questionsGroupGeneral && questionsGroupGeneral.length > 0 && (
                        <div className="__content-question">
                            <div className="__title">
                                <span>
                                    {`Question${questionsGroupGeneral.length > 1 ? 's' : ''}:`}
                                </span>
                            </div>
                            <div className="__content">
                            {questionsGroupGeneral.map((item, i) => {
                                const html = convertStrToHtml(item.question).replaceAll(
                                '%s%',
                                `<div class="__space"></div>`,
                                )
                                return (
                                <div key={i} className="__radio-group">
                                    {i > 0 && (<div style={{ width: '70%', transform: 'translateX(25%)'}}><hr style={{border:'1px solid #2692ff'}}/></div>)
                                    }
                                    <CustomHeading tag="h6" className="__radio-heading">
                                        <span
                                            dangerouslySetInnerHTML={{
                                            __html: html,
                                            }}
                                        ></span>
                                    </CustomHeading>
                                    <div className={`__radio-list ${renderClassName(item)}`}>
                                    {item.answers.map((childItem, j) => (
                                        <div
                                            key={j}
                                            className={`__radio-item`}
                                            style={{
                                                pointerEvents:'none',
                                            }}
                                        >
                                            <img src={childItem.imageAns}alt=''/>
                                            <div className={`icon-true`}>
                                                <img 
                                                    src={'/images/icons/ic-checked-false.png'}
                                                    alt=''
                                                />
                                            </div>
                                        </div>
                                    ))}
                                    </div>
                                </div>
                                )
                            })}
                            </div>
                        </div>
                        )}
                    </div>
                </div>
            </div>
        </div>
    )
}
