import { useRef, useState, useContext, useEffect } from 'react'
import { useWindowSize } from 'utils/hook'
import { formatDateTime } from 'practiceTest/utils/functions'
import { CustomButton } from 'practiceTest/components/atoms/button'
import { CustomImage } from 'practiceTest/components/atoms/image'
import { AudioPlayer } from 'practiceTest/components/molecules/audioPlayer'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'
import { BlockWave } from 'practiceTest/components/organisms/blockWave'
import { isIOS } from 'utils/log'
function GameDrawLine({ data }) {
  return <GameDrawLineWeb key={`pc_${data.id}`} data={data} />
}

export default GameDrawLine

function GameDrawLineWeb({ data }) {
  const isAppleDevice = useRef(isIOS() || navigator.platform.toUpperCase().indexOf('MAC') >= 0)
  const imageWidthOriginRef = useRef(0)
  const answer = data?.answers ?? []
  const ansLeft = []
  const ansRight = []
  let indexAnswer = 0
  const indexExample = answer.findIndex((m) => m.isExam)
  if (indexExample != -1) {
    const itemExample = answer.find((m) => m.isExam)
    answer.splice(indexExample, 1)
    answer.splice(0, 0, itemExample)
  }
  data.answers = data?.answers.map((item, index) => {
    const itemAnswer = item.isExam ? item : { ...item, indexAnswer }
    if (!item.isExam) {
      indexAnswer += 1
    }
    return itemAnswer
  })
  answer.map((item, index) => {
    if (index % 2 === 0) {
      ansLeft.push(item)
    } else {
      ansRight.push(item)
    }
  })
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const [countDown, setCountDown] = useState(null)
  const [scaleNumber, setScaleNumber] = useState(0)
  const scaleNumberRef = useState(0)
  const [offsetImage, setOffsetImage] = useState({ top: 0, left: 0 })
  const [isPlaying, setIsPLaying] = useState(false)
  const defaultAnswer = answer
  .filter((item) => !item.isExam)
  .map((item, index) => ({
    index: index,
    isMatched: false,
  }))
  const [width, height] = useWindowSize()
  useEffect(() => {
    const element = document.querySelector("#pt-o-game-draw-line-1-content-id .--example-div .item-image-content");
    if(element && element.complete && element.offsetHeight>0){
      setScaleNumber(element.width / element.naturalWidth)
      setTimeout(() => {
        linkImage(element)
      }, 0);
    }
  }, [width, height])
  const audio = useRef(null)
  useEffect(() => {
    return () => {
      clearCanvas()
        setScaleNumber(0)
    }
  }, [])
  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0) setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }
  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])
  const clearCanvas = () => {
    const canvas = document.getElementById('canvas-draw-line-1')
    if (!canvas) return
    
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    const content = document.getElementById('pt-o-game-draw-line-1-content-id')
    canvas.width = content.offsetWidth
    canvas.height = content.offsetHeight
  }
  const DrawLine = (elementFrom, elementTo, diffX, diffY, color) => {
    const canvas = document.getElementById('canvas-draw-line-1')
    const ctx = canvas.getContext('2d')
    const rectCanvas = canvas.getBoundingClientRect()

    const rectIconBtn = elementFrom.getBoundingClientRect()
    const xBtn = rectIconBtn.x - rectCanvas.x + 7
    const yBtn = rectIconBtn.top - rectCanvas.top + 7
    const rectImg = elementTo.getBoundingClientRect()
    const xImg = rectImg.x - rectCanvas.x + diffX
    const yImg = rectImg.top - rectCanvas.top + diffY

    //draw
    ctx.beginPath()
    ctx.moveTo(xBtn, yBtn)
    ctx.lineWidth = 2
    ctx.lineCap = 'round'
    ctx.strokeStyle = color
    ctx.lineTo(xImg, yImg)
    ctx.stroke()

    elementFrom.style.visibility = 'visible'
    elementTo.style.visibility = 'visible'
  }

  const setIsShowTapescript = (isShow) => {
    const elementScript = document.getElementById('pt-o-game-draw-line-1-script')
    const elementRight = document.querySelector('.pt-o-game-draw-line-audio__right')
    const elementLeft = document.querySelector('.pt-o-game-draw-line-audio__left')
    if (isShow) {
      elementScript.style.display = 'block'
      elementLeft.style.display = 'none'
      elementRight.style.display = 'none'
    } else {
      elementScript.style.display = 'none'
      elementLeft.style.display = 'flex'
      elementRight.style.display = 'flex'
    }
  }
  const onScrollListButton = (position) => {
    console.log("aadad---------------")
    let matchingButton = document.querySelector('.block-item_button_left.matching')
    if (position == 'right') {
      matchingButton = document.querySelector('.block-item_button_right.matching')
    }
    if (matchingButton) {
      const iconMatchingBtn = matchingButton.querySelector('.icon-matching')
      const divImageId = matchingButton.dataset['imageid']
      const divImageElement = document.getElementById(divImageId)
      const iconMatchingImage = divImageElement.querySelector(`.icon-matching-image.--${position}`)
      clearCanvas()
      const diftX = position == 'left' ? 0 : 6
      const diftY = 6
      DrawLine(
        iconMatchingBtn,
        iconMatchingImage,
        diftX,
        diftY,
        matchingButton.classList.contains('--example') ? '#67ab8d' : '#2692ff',
      )
    }
  }
  const drawPointMatching = (element, scale)=>{
    try {
      const imageElements = element.parentElement
      const iconImageRight = imageElements.querySelector('.icon-matching-image.--right')
      const iconImage = imageElements.querySelector('.icon-matching-image.--left')
      const canvasImage = document.createElement('canvas')
      const ctxLeft = canvasImage.getContext('2d', { willReadFrequently: true })
      const imgHeight = element.naturalHeight
      const imgWidth = element.naturalWidth
      canvasImage.width = imgWidth
      canvasImage.height = imgHeight
      ctxLeft.drawImage(element, 0, 0, imgWidth, imgHeight)
      if (iconImageRight) {
        // iconImageRight.style.visibility = 'hidden'
      }
      if (!iconImage) return
      for (let j = 0; j < imgWidth; j++) {
        const pixelData = ctxLeft.getImageData(j, imgHeight / 2, 1, 1).data
        if (pixelData[0] !== 0 || pixelData[1] !== 0 || pixelData[2] !== 0 || pixelData[3] !== 0) {
          iconImage.style.position = 'absolute'
          iconImage.style.top = `${(imgHeight * scale) / 2 - 6}px`
          iconImage.style.left = `${j * scale - 6}px`
          break
        }
      }
      for (let j = imgWidth; j > 0; j--) {
        const pixelData = ctxLeft.getImageData(j, imgHeight / 2, 1, 1).data
        if (pixelData[0] !== 0 || pixelData[1] !== 0 || pixelData[2] !== 0 || pixelData[3] !== 0) {
          iconImageRight.style.position = 'absolute'
          iconImageRight.style.top = `${(imgHeight * scale) / 2 - 6}px`
          iconImageRight.style.left = `${j * scale}px`
          break
        }
      }
    } catch (error) {
      console.error('drawPointMatching', error)
    }
  }
  const drawPoint2 = (e) => {
    drawPointMatching(e.currentTarget,scaleNumber)
  }
  const linkImage = (element) => {
    setTimeout(() => {
      element.style.visibility = 'visible'
    }, 0)
    const divImageMatching = element.parentElement
    const showImageAnswer = (src, buttonMatching) => {
      const thumbnailAnswer = buttonMatching.querySelector('.image-button')
      thumbnailAnswer.src = src
      thumbnailAnswer.style.display = 'block'
      
    }
    if (divImageMatching.classList.contains('--example-div')) {
      console.log("element-----",element)
      const buttonMatching = document.getElementById(divImageMatching.id.replace('image_', ''))
      showImageAnswer(element.src, buttonMatching)
      const iconMatchingBtn = buttonMatching.querySelector('.icon-matching')
      const iconMatchingImage = divImageMatching.querySelector(`.icon-matching-image.--left`)
      clearCanvas()
      const diftX = 0
      const diftY = 6
      DrawLine(
        iconMatchingBtn,
        iconMatchingImage,
        diftX,
        diftY,
        '#67ab8d'
      )
    }
  }
  const generateClassImage = (item, index) => {
    const className = {
      image: '',
      div: '',
    }
    if (item.isExam) {
      className.image = ' --example-image --image-matched'
      return className
    }else{
      className.image += ' --image-default'
    }
    return className
  }
  const generateClassButton = (item) => {
    let className = 'default'
    if (item.isExam) {
      className = ' matched matching --example'
    }
    return className
  }
  const getImageButton = (item) => {
    if (item.isExam) {
      return item.image
    } else {
      return ""
    }
  }
  const onLoadImageInstruction = (e) => {
    imageWidthOriginRef.current = e.currentTarget.naturalWidth
    if(e.currentTarget.height > e.currentTarget.parentElement.offsetHeight && e.currentTarget.parentElement.offsetHeight!=0){
      e.currentTarget.style.height = e.currentTarget.parentElement.offsetHeight + "px"
    }
    const ratio = e.currentTarget.width / e.currentTarget.naturalWidth;
    setScaleNumber(ratio)
    e.currentTarget.style.visibility = 'visible'
    scaleNumberRef.current = ratio;
    const canvas = document.getElementById('canvas-draw-line-1')
    const content = document.getElementById('pt-o-game-draw-line-1-content-id')
    canvas.width = content.offsetWidth
    canvas.height = content.offsetHeight
    setOffsetImage({ top: e.currentTarget.offsetTop, left: e.currentTarget.offsetLeft })
  }
  const isLargeButtonAnswer = data.answers.some((m) => m.answer.length > 12)

  return (
    <div className="pt-o-game-draw-line-audio pt-o-game-draw-line-1">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() => setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))}
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-draw-line-audio__container">
        <>
          <div className="__paper" id="pt-o-game-draw-line-1-script" style={{ display: 'none' }}>
            <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
          <BlockWave
          className={`pt-o-game-draw-line-audio__left pt-o-game-draw-line__result`}
          >
            <AudioPlayer className="__player" isPlaying={isPlaying} setIsPlaying={() => setIsPLaying(!isPlaying)} />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <div className="pt-o-game-draw-line-audio__right">
          <CustomButton className="__tapescript" onClick={() => setIsShowTapescript(true)}>
                Tapescript
              </CustomButton>
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-draw-line-1-content-id">
              <div className="block-item" onScroll={() => onScrollListButton('left')}>
                <div className="block-item__list">
                  {ansLeft.map((itemLeft, index) => {
                    const buttonImageUrl = getImageButton(itemLeft)
                    return (
                      <div
                        id={itemLeft.key}
                        key={itemLeft.key}
                        data-position="left"
                        className={`block-item_button block-item_button_left ${generateClassButton(itemLeft)}`}
                        data-index={itemLeft.indexAnswer}
                        style={{ pointerEvents: 'none' }}
                        data-imageid={itemLeft.isExam ? `image_${itemLeft.key}` : ''}
                      >
                        <button className={`${isLargeButtonAnswer ? '--lg' : ''}`}>{itemLeft.answer}</button>
                        <img
                          style={{ display: 'none' }}
                          className="icon-deleted"
                          height={25}
                          src="/images/icons/ic-delete-img.png"
                          alt="img-d"
                        />
                        <img
                          style={{ visibility: 'hidden' }}
                          className={`icon-matching ${itemLeft.isExam ? '--example-matching' : ''}`}
                          height={10}
                          src={`/images/icons/ic-matching${itemLeft.isExam ? '-ex' : ''}.png`}
                          alt="img-m"
                        />
                        <img
                          key={buttonImageUrl}
                          id={buttonImageUrl}
                          style={buttonImageUrl ? {} : { display: 'none' }}
                          height={30}
                          src={buttonImageUrl}
                          alt="img"
                          className="image-button"
                        />
                      </div>
                    )
                  })}
                </div>
              </div>
              <div className="block-item">
                <div
                  className="wrap-image"
                  style={{ objectFit: 'contain', maxHeight: '100%', position: 'relative', maxWidth: '100%' }}
                >
                  <img
                    data-animate="fade-in"
                    className="image-instruction"
                    alt="image-instruction"
                    id="dl1-image-instruction-id"
                    src={data?.imageInstruction}
                    style={{ visibility: 'visible', maxHeight: '100%' }}
                    onLoad={(e) => {
                      onLoadImageInstruction(e)
                    }}
                  />
                  {data?.answers?.map((item, mIndex) => {
                    if (!scaleNumber) return null
                    const p = item.position.split(':')
                    const width = (p[2] - p[0]) * scaleNumber
                    const height = (p[3] - p[1]) * scaleNumber
                    const left = p[0] * scaleNumber
                    const top = p[1] * scaleNumber

                    const style ={ pointerEvents: 'none' }
                    style.position = 'absolute'
                    style.left = `${offsetImage.left + left}px`
                    style.top = `${offsetImage.top + top}px`
                    style.width = `${width}px`
                    style.height = `${height}px`
                    const className = generateClassImage(item, item.indexAnswer)
                    return (
                      <div
                        key={`image_${item.key}`}
                        id={`image_${item.key}`}
                        className={`item-image ${item.isExam ? '--example-div' : '--matching-div'} ${className.div}`}
                        data-index={item.indexAnswer}
                        data-buttonid={item.isExam ? item.key : ''}
                        style={style}
                        data-scale_number={scaleNumber}
                      >
                        <img
                          alt=""
                          src={item.image}
                          className={`item-image-content ${className.image} ${
                            isAppleDevice.current ? '--is-apple-device' : ''
                          }`}
                          data-animate="fade-in"
                          style={{ visibility: 'visible' }}
                          onLoad={(e) => {
                            if(item.isExam){
                              drawPoint2(e)
                              linkImage(e.currentTarget)
                            }
                          }}
                        ></img>
                        <img
                          alt=""
                          src={`/images/icons/ic-matching${item.isExam ? '-ex' : ''}.png`}
                          className="icon-matching-image --left --matching-icon-matching"
                        ></img>
                        <img
                          alt=""
                          src={`/images/icons/ic-matching${item.isExam ? '-ex' : ''}.png`}
                          className="icon-matching-image --right --matching-icon-matching"
                        ></img>
                      </div>
                    )
                  })}
                </div>
              </div>
              <div className="block-item" onScroll={() => onScrollListButton('right')}>
                <div className="block-item__list">
                  {ansRight.map((itemRight, index) => {
                    const buttonImageUrl = getImageButton(itemRight)
                    return (
                      <div
                        id={itemRight.key}
                        key={itemRight.key}
                        data-position="right"
                        className={`block-item_button block-item_button_right ${generateClassButton(itemRight)}`}
                        data-index={itemRight.indexAnswer}
                        style={{ pointerEvents: 'none' }}
                        data-imageid={itemRight.isExam ? `image_${itemRight.key}` : ''}
                      >
                        <button className={`${isLargeButtonAnswer ? '--lg' : ''}`}>{itemRight.answer}</button>
                        <img
                          style={{ display: 'none' }}
                          className="icon-deleted"
                          height={25}
                          src="/images/icons/ic-delete-img.png"
                          alt="img-d"
                        />
                        <img
                          className={`icon-matching --right ${itemRight.isExam ? '--example-matching' : ''}`}
                          height={10}
                          src={`/images/icons/ic-matching${itemRight.isExam ? '-ex' : ''}.png`}
                          alt="img-m"
                          style={{ visibility: 'hidden' }}
                        />
                        <img
                          key={buttonImageUrl}
                          id={buttonImageUrl}
                          style={buttonImageUrl ? {} : { display: 'none' }}
                          height={30}
                          src={buttonImageUrl}
                          alt="img"
                          className="image-button"
                        />
                      </div>
                    )
                  })}
                </div>
              </div>
              <canvas id="canvas-draw-line-1" style={{zIndex:6}}></canvas>
            </div>
          </div>
        </>
      </div>
    </div>
  )
}
