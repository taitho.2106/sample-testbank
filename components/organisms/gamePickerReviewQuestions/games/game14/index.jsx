import { useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { formatDateTime } from '../../../../../practiceTest/utils/functions'

export const Game14 = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const answerData = answers[0]
  const question_id = data.id
  
  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)
  const [isZoom, setIsZoom] = useState(false)

  const [textArr, setTextArr] = useState(Array.from(Array(answerData.answers.length), () => ''))

  const audio = useRef(null)

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])
const key1=  `${question_id}`;
  return (
    <div className="pt-o-game-14" style={{width:'100%', height:'100%'}}>
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-14__container">
        {isShowTapescript ? (
          <div className="__paper">
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
        ) : (
          <>
            <BlockWave className={`__left`}>
              <AudioPlayer
                className="__player"
                isPlaying={isPlaying}
                setIsPlaying={() => setIsPLaying(!isPlaying)}
              />
              <span className="__duration">{countDown}</span>
            </BlockWave>
            <BlockBottomGradient className="__right">
              {(
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                >
                  Tapescript
                </CustomButton>
              )}
              <div className="__header">
                <CustomHeading tag="h6" className="__heading">
                  <FormatText tag="span">{instruction}</FormatText>
                </CustomHeading>
              </div>
              <div className="__content">
                <div className="__top">
                  <Scrollbars universal={true}>
                    <div className="__table">
                      {imageInstruction ? (
                        <>
                          <CustomImage
                            className="__image-instruction"
                            alt="Image instruction"
                            src={`${imageInstruction}`}
                            yRate={0}
                            onClick={() => setIsZoom(true)}
                          />
                          <ImageZooming
                            data={{
                              alt: 'Image Instruction',
                              src: `${imageInstruction}`,
                            }}
                            status={{
                              state: isZoom,
                              setState: setIsZoom,
                            }}
                          />
                        </>
                      ) : (
                        <>
                          <div className="__thead">
                            <div className="__tr">
                              
                            </div>
                          </div>
                          <div className="__tbody">
                            
                          </div>
                        </>
                      )}
                    </div>
                  </Scrollbars>
                  <div className="__background">
                    <CustomImage
                      className="__image"
                      alt="background"
                      src="/pt/images/backgrounds/bg-game-2.png"
                      fit="cover"
                      yRate={0}
                    />
                  </div>
                </div>
                <div className="__bottom">
                  <Scrollbars universal={true}>
                    <div className="__list">
                    <div className="__left">
                        {Array.from(
                          Array(
                            Math.ceil(
                              answerData.answers.length > 1
                                ? answerData.answers.length / 2
                                : answerData.answers.length,
                            ),
                          ),
                          (e, i) => (
                            <InputItem
                              key={`left_${key1}${i}`}
                              defaultValue={textArr[i]}
                              index={i}
                              indexStr={`(${i + 1})`}
                            />
                          ),
                        )}
                      </div>
                      {answerData.answers.length > 1 && (
                        <div className="__right">
                           {Array.from(
                            Array(Math.ceil((answerData.answers.length - 1) / 2)),
                            (e, i) => (
                              <InputItem
                              key={`right_${key1}${i}`}
                              defaultValue={
                                [...textArr][
                                  Math.ceil(answerData.answers.length / 2) + i
                                ]
                              }
                              index={
                                Math.ceil(answerData.answers.length / 2) + i
                              }
                              indexStr={`(${
                                Math.ceil(answerData.answers.length / 2) +
                                i +
                                1
                              })`}
                              />
                            ),
                          )}
                        </div>
                      )}
                    </div>
                  </Scrollbars>
                </div>
              </div>
            </BlockBottomGradient>
          </>
        )}
      </div>
    </div>
  )
}

const InputItem = ({
  className,
  defaultValue,
  index,
  indexStr,
  trueValue,
  onBlur,
  onFocus
}) => {
 
  return (
    <div className="__input-item">
      <label>{indexStr}</label>
      <input
        className={`${className} ${
          defaultValue && defaultValue.length > 0 ? '--primary' : ''
        }`}
        disabled={true}
        type="text"
        placeholder="Type here ..."
        defaultValue={defaultValue}
      />
    </div>
  )
}
