import { useRef, useState, useContext, useEffect } from 'react'


import { CustomButton } from 'practiceTest/components/atoms/button'
import { CustomImage } from 'practiceTest/components/atoms/image'
import { AudioPlayer } from 'practiceTest/components/molecules/audioPlayer'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'
import { BlockWave } from 'practiceTest/components/organisms/blockWave'
import { TEST_MODE } from 'practiceTest/interfaces/constants'
import { formatDateTime } from 'practiceTest/utils/functions'
import { useWindowSize } from 'utils/hook'
function GameDrawLine2({ data }) {
  const renderUIGameDrawLine = (data) => {
    // if (width <= 1024 && window.matchMedia("(orientation: portrait)").matches) {
    //   return <GameDrawLineMobile  key={`m_${data.id}`} data={data} />
    // }
    return <GameDrawLineWeb key={`pc_${data.id}`} data={data} />
  }
  return <>{renderUIGameDrawLine(data)}</>
}

export default GameDrawLine2

function GameDrawLineWeb({ data }) {
  const isPortrait = useRef( window.matchMedia("(orientation: portrait)").matches)
  const elementExampleId = useRef(null)
  const imageWidthOriginRef = useRef(0);
  const answer = data?.answers ?? []
  const ansLeft = []
  const ansRight = []
  let indexAnswer = 0
  const indexExample = answer.findIndex((m) => m.isExam)
  if (indexExample != -1) {
    const itemExample = answer.find((m) => m.isExam)
    answer.splice(indexExample, 1)
    answer.splice(0, 0, itemExample)
  }
  data.answers = data?.answers.map((item, index) => {
    const itemAnswer = item.isExam ? item : { ...item, indexAnswer }
    if (!item.isExam) {
      indexAnswer += 1
    }
    return itemAnswer
  })
  answer.map((item, index) => {
    if (index % 2 === 0) {
      ansLeft.push(item)
    } else {
      ansRight.push(item)
    }
  })
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)

  const mode ={
    state : TEST_MODE.check
  }
  const idButton = useRef('')
  const idImage = useRef('')
  const defaultAnswer = answer
  .filter((item) => !item.isExam)
  .map((item, index) => ({
    index: index,
    isMatched: false,
  }))
  const answerGroup = useRef(defaultAnswer)
  const audio = useRef(null)
  const ratio = useRef({ x: 0, y: 0 })
 
  const renderContentClone = (e) => {
    try {
      const checkEquationValue = (a, b) => {
        if (a - b > 4 || b - a > 4) {
          return false
        } else {
          return true
        }
      }
      const imageItem = document.getElementsByClassName(
        'item-image --matching-div',
      )
      const imageItemEx = document.getElementsByClassName(
        'item-image --example-div',
      )
      const wrapImage = document.querySelectorAll('#pt-o-game-draw-line-2-content-id .wrap-image')[0]
      const imageInstruction = e.target;
      if (
        !checkEquationValue(
          imageInstruction.offsetHeight,
          wrapImage.offsetHeight,
        )
      ) {
        const imgH = wrapImage.offsetHeight
        const imgW =
          wrapImage.offsetHeight *
          (imageInstruction.offsetWidth / imageInstruction.offsetHeight)
        wrapImage.style.height = `${imgH}px`
        wrapImage.style.width = `${imgW}px`
        imageInstruction.style.height = `${imgH}px`
        imageInstruction.style.width = `${imgW}px`
        imageInstruction.style.visibility = "hidden"
        renderContentClone(e)
        return
      }
      //set image block
      let distance = 70;
      if(!isPortrait.current){
        if(document.body.clientWidth <=1024){
         distance = 50;
        }
        if(wrapImage.offsetWidth < wrapImage.parentElement.offsetWidth - distance){
          wrapImage.parentElement.style.width =  wrapImage.offsetWidth + distance +"px"
        }
      }else{
        distance = 50;
        if(wrapImage.offsetHeight < wrapImage.parentElement.offsetHeight - distance){
          wrapImage.parentElement.style.height =  wrapImage.offsetHeight + distance +"px"
        }
      }
      const imageOrigion = e.target;
      imageInstruction.style.visibility = 'visible'
      const canvas = document.getElementById('canvas-draw-line-2')
      const content = document.getElementById(
        'pt-o-game-draw-line-2-content-id',
      )
      canvas.style.position = 'absolute'
      canvas.style.pointerEvents = 'none'
      canvas.style.top = '1rem'
      canvas.style.left = 0
      canvas.style.bottom = '1rem'
      canvas.width = content.offsetWidth
      canvas.height = content.offsetHeight - 20

      ratio.current.x =
        imageInstruction.offsetWidth / imageOrigion.naturalWidth
      ratio.current.y =
        imageInstruction.offsetHeight / imageOrigion.naturalHeight
        imageWidthOriginRef.current = imageOrigion.naturalWidth; 
      //example

      //answers
      const answersList = answer.filter((item) => !item.isExam)
      answer
        .filter((item) => !item.isExam)
        .map((item, index) => {
          const node = document.getElementById(`image_${item.key}`)
          while (node.lastElementChild) {
            node.removeChild(node.lastElementChild)
          }
          node.style.display = "block"
          const elementImg = document.createElement('img')
          const elementImgIconLeft = document.createElement('img')
          const elementImgIconRight = document.createElement('img')

          elementImgIconLeft.src = '/images/icons/ic-matching.png'
          elementImgIconLeft.classList.add('icon-matching-image')
          elementImgIconLeft.classList.add('--left')
          elementImgIconLeft.classList.add('--matching-icon-matching')

          elementImgIconRight.src = '/images/icons/ic-matching.png'
          elementImgIconRight.classList.add('icon-matching-image')
          elementImgIconRight.classList.add('--right')
          elementImgIconRight.classList.add('--matching-icon-matching')

          elementImg.style.visibility = 'hidden'
          elementImgIconLeft.style.visibility = 'hidden'
          elementImgIconRight.style.visibility = 'hidden'

          node.appendChild(elementImg)
          node.appendChild(elementImgIconLeft)
          node.appendChild(elementImgIconRight)
          elementImg.onload = function () {
            elementImg.style.visibility = 'visible'
            const p = item.position.split(':')
            const width = (p[2] - p[0]) * ratio.current.x
            const height = (p[3] - p[1]) * ratio.current.y
            const left = p[0] * ratio.current.x
            const top = p[1] * ratio.current.y
            elementImg.classList.add('item-image-content')
            elementImg.classList.add('--matching-image')
            imageItem[index].style.position = 'absolute'
            imageItem[index].style.left = `${left}px`
            imageItem[index].style.top = `${top}px`
            imageItem[index].style.width = `${width}px`
            imageItem[index].style.height = `${height}px`
            imageItem[index].style.transform = `scale(1,1)`

            const idItemImage = imageItem[index].id
            drawPoint(idItemImage, 'left')
            drawPoint(idItemImage, 'right')
            if (
              answerGroup.current[index] &&
              answerGroup.current[index].isMatched
            ) {
              const idItemButton =
                answersList[answerGroup.current[index].index].key
              clearCanvas()

              // const isShowLine = mode.state == TEST_MODE.play
              // doMatching(idItemButton, idItemImage, false)
            }
          }
          elementImg.src = item.image
        })
      answer
        .filter((item) => item.isExam)
        .map((item, index) => {
          const node = document.getElementById(`image_${item.key}`)
          while (node.lastElementChild) {
            node.removeChild(node.lastElementChild)
          }
          node.style.display = "block"
          const elementImg = document.createElement('img')
          const elementImgIconLeft = document.createElement('img')
          const elementImgIconRight = document.createElement('img')

          elementImgIconLeft.src = '/images/icons/ic-matching-ex.png'
          elementImgIconLeft.classList.add('icon-matching-image')
          elementImgIconLeft.classList.add('--left')
          elementImgIconLeft.classList.add('--example-icon-matching')

          elementImgIconRight.src = '/images/icons/ic-matching-ex.png'
          elementImgIconRight.classList.add('icon-matching-image')
          elementImgIconRight.classList.add('--right')
          elementImgIconRight.classList.add('--example-icon-matching')

          elementImg.style.visibility = 'hidden'
          elementImgIconLeft.style.visibility = 'hidden'
          elementImgIconRight.style.visibility = 'hidden'
          node.appendChild(elementImg)
          node.appendChild(elementImgIconLeft)
          node.appendChild(elementImgIconRight)
          elementImg.onload = function () {
            elementImg.style.visibility = 'visible'
            const p = item.position.split(':')
            const width = (p[2] - p[0]) * ratio.current.x
            const height = (p[3] - p[1]) * ratio.current.y
            const left = p[0] * ratio.current.x
            const top = p[1] * ratio.current.y
            elementImg.classList.add('item-image-content')
            elementImg.classList.add('--example-image')
            imageItemEx[index].style.position = 'absolute'
            imageItemEx[index].style.left = `${left}px`
            imageItemEx[index].style.top = `${top}px`
            imageItemEx[index].style.width = `${width}px`
            imageItemEx[index].style.height = `${height}px`
            imageItemEx[index].style.transform = `scale(1,1)`

            const idItemButton = item.key
            const idItemImage = imageItemEx[index].id
            drawPoint(idItemImage, 'left')
            drawPoint(idItemImage, 'right')
            clearCanvas()
            // const isShowLine = mode.state == TEST_MODE.play
            doMatching(idItemButton, idItemImage, true)
            elementExampleId.current = {idItemButton,idItemImage}
          }
          elementImg.src = item.image
        })
    } catch (error) {
      console.error('renderContent', error)
    }
  }
  useEffect(() => {
  window.timeout_draw_line_1 = null
    return () => {
      clearCanvas()
    }
  }, [])
  const [width, height] = useWindowSize()
  useEffect(() => {
    isPortrait.current = window.matchMedia("(orientation: portrait)").matches;
    if (window.timeout_draw_line_1) {
      clearTimeout(window.timeout_draw_line_1)
    }
    clearCanvas()
    const imageItem = document.querySelectorAll(
      '#pt-o-game-draw-line-2-content-id .item-image',
    )
    imageItem.forEach(m=> m.style.display = "none")
    const imageInstruction = document.getElementById(
      'dl1-image-instruction-id',
    )
        //set size default
    imageInstruction.style.height = ""
    imageInstruction.style.width= "";
    imageInstruction.parentElement.style.height = ""
    imageInstruction.parentElement.style.width= "";
    imageInstruction.parentElement.parentElement.style.height = ""
    imageInstruction.parentElement.parentElement.style.width= "";
    window.timeout_draw_line_1 = setTimeout(() => {
      imageInstruction.src = imageInstruction.src +"?t="+ new Date().getTime()
    }, 200)
  }, [width, height])
  
  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  const renderClassNameLeft = (index) => {
    let className = 'default'
    if (ansLeft[index].isExam) {
      className = 'matched --example'
    }
    return className
  }
  const renderClassNameRight = (index) => {
    let className = 'default'
    if (ansRight[index].isExam) {
      className = 'matched --example'
    }
    return className
  }

  const drawPoint = (idImg, position) => {
    // console.log("position====",position);
    try {
      if (position === 'left') {
        const imageElements = document.getElementById(idImg)
        const iconImageRight = imageElements.getElementsByClassName(
          'icon-matching-image --right',
        )[0]

        const iconImage = imageElements.querySelector(
          '.icon-matching-image.--left',
        )
        const canvasLeft = document.createElement('canvas')
        const ctxLeft = canvasLeft.getContext('2d')
        const imgHeight = imageElements.childNodes[0].offsetHeight
        const imgWidth = imageElements.childNodes[0].offsetWidth
        ctxLeft.drawImage(
          imageElements.childNodes[0],
          0,
          0,
          imgWidth,
          imgHeight,
        )
        if (iconImageRight) {
          iconImageRight.style.visibility = 'hidden'
        }
        if (!iconImage) return
        iconImage.style.visibility = 'hidden'
        if (!isPortrait.current) {
          for (let j = 0; j < imgWidth; j++) {
            const pixelData = ctxLeft.getImageData(j, imgHeight / 2, 1, 1).data
            if (
              pixelData[0] !== 0 ||
              pixelData[1] !== 0 ||
              pixelData[2] !== 0 ||
              pixelData[3] !== 0
            ) {
              // iconImage.style.display = 'flex'
              iconImage.style.position = 'absolute'
              iconImage.style.top = `${imgHeight / 2 - 6}px`
              iconImage.style.left = `${j - 6}px`
              break
            }
          }
        } else {
          for (let j = 0; j < imgHeight; j++) {
            const pixelData = ctxLeft.getImageData(imgWidth / 2, j, 1, 1).data
            if (
              pixelData[0] !== 0 ||
              pixelData[1] !== 0 ||
              pixelData[2] !== 0 ||
              pixelData[3] !== 0
            ) {
              // iconImage.style.display = 'flex'
              iconImage.style.position = 'absolute'
              iconImage.style.top = `${j - 7}px`
              iconImage.style.left = `${imgWidth / 2 - 6}px`
              break
            }
          }
        }
      } else if (position === 'right') {
        const imageElements = document.getElementById(idImg)
        const iconImageRight = imageElements.querySelector(
          '.icon-matching-image.--right',
        )
        if (!iconImageRight) return
        const iconImage = imageElements.querySelector(
          '.icon-matching-image.--left',
        )
        const canvasLeft = document.createElement('canvas')
        const ctxLeft = canvasLeft.getContext('2d')
        const imgHeight = imageElements.childNodes[0].offsetHeight
        const imgWidth = imageElements.childNodes[0].offsetWidth
        ctxLeft.drawImage(
          imageElements.childNodes[0],
          0,
          0,
          imgWidth,
          imgHeight,
        )
        if (iconImage) {
          iconImage.style.visibility = 'hidden'
        }
        if (!iconImageRight) return
        iconImageRight.style.visibility = 'hidden'
        if (!isPortrait.current) {
          for (let j = imgWidth; j > 0; j--) {
            const pixelData = ctxLeft.getImageData(j, imgHeight / 2, 1, 1).data
            if (
              pixelData[0] !== 0 ||
              pixelData[1] !== 0 ||
              pixelData[2] !== 0 ||
              pixelData[3] !== 0
            ) {
              // iconImage[i].style.display = 'flex'
              iconImageRight.style.position = 'absolute'
              iconImageRight.style.top = `${imgHeight / 2 - 6}px`
              iconImageRight.style.left = `${j}px`
              break
            }
          }
        } else {
          for (let j = imgHeight; j > 0; j--) {
            const pixelData = ctxLeft.getImageData(imgWidth / 2, j, 1, 1).data
            if (
              pixelData[0] !== 0 ||
              pixelData[1] !== 0 ||
              pixelData[2] !== 0 ||
              pixelData[3] !== 0
            ) {
              // iconImage[i].style.display = 'flex'
              iconImageRight.style.position = 'absolute'
              iconImageRight.style.top = `${j}px`
              iconImageRight.style.left = `${imgWidth / 2 - 6}px`
              break
            }
          }
        }
      }
    } catch (error) {
      console.error('drawPoint', error)
    }
  }

  const onClichButtonLeft = (index, position, e) => {
    e.preventDefault()
    idButton.current = e.currentTarget.id
    const element = document.getElementById(e.currentTarget.id)
    const iconButtonMatching = element.querySelector('.icon-matching')
    // if(element.childNodes.length > 0){
    //   element.childNodes[1].style.visibility = 'visible'
    // }
    const listButtonElement = document.querySelectorAll(
      '.pt-o-game-draw-line-2 .block-item_button',
    )
    iconButtonMatching.style.visibility = 'visible'
    listButtonElement.forEach((ele) => {
      if (ele?.classList.contains('selected') && ele.id != idButton.current) {
        ele.classList.remove('selected')
      }
    })

    if (
      element?.classList.contains('matched') ||
      element?.classList.contains('matching')
    ) {
      const imageMapingId = element.dataset['imageid']
      clearCanvas()
      doMatching(idButton.current, imageMapingId, true)
      return
    }
    element.classList.add('selected')

    clearCanvas()
    //hidden icon matching button and remove matching
    listButtonElement.forEach((ele) => {
      if (ele.id != idButton.current) {
        ele.classList.remove('matching')
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'hidden'
      }
      if (ele?.classList.contains('matching')) {
        ele.classList.remove('matching')
      }
    })

    const divImageAnswer = document.querySelectorAll(
      '.pt-o-game-draw-line-2 .item-image',
    )
    divImageAnswer.forEach((ele) => {
      const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
      iconMatchings.forEach((icon) => {
        if (
          icon?.classList.contains(`--${position}`) &&
          !ele.dataset['buttonid']
        ) {
          icon.style.visibility = 'visible'
        } else {
          icon.style.visibility = 'hidden'
        }
      })
      const imageElement = ele.querySelector('.item-image-content')
      if (
        imageElement &&
        imageElement?.classList.contains('--image-selected')
      ) {
        imageElement.classList.remove('--image-selected')
      }
    })
    //matching
    if (idButton.current && idImage.current) {
      doMatching(idButton.current, idImage.current, true)
    }
  }

  const onClickDeleted = (buttonId, position) => {
    const elementButton = document.getElementById(buttonId)

    const divImageMatchingId = elementButton.dataset['imageid']
    if (!divImageMatchingId) return

    //hidden icon matching image
    const divImageList = document.querySelectorAll(
      '.pt-o-game-draw-line-2 .item-image',
    )
    divImageList.forEach((ele) => {
      if (!ele.dataset['buttonid']) {
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => {
          if (icon?.classList.contains(`--${position}`)) {
            icon.style.visibility = 'visible'
          } else {
            icon.style.visibility = 'hidden'
          }
        })
      }
    })

    const imageResult = elementButton.querySelector('.image-button')
    imageResult.style.display = 'none'
    imageResult.src = ''

    const divImageElement = document.getElementById(divImageMatchingId)
    const imageElement = divImageElement.querySelector('.item-image-content')

    //remove matched
    elementButton.dataset['imageid'] = ''
    divImageElement.dataset['buttonid'] = ''

    if (elementButton?.classList.contains('matched')) {
      elementButton.classList.remove('matched')
    }
    if (elementButton?.classList.contains('matching')) {
      elementButton.classList.remove('matching')
    }
    if (!elementButton?.classList.contains('selected')) {
      elementButton.classList.add('selected')
    }
    idButton.current = buttonId
    idImage.current = ''

    if (imageElement?.classList.contains('--image-matched')) {
      imageElement.classList.remove('--image-matched')
    }
    if (imageElement?.classList.contains('--image-selected')) {
      imageElement.classList.remove('--image-selected')
    }
    clearCanvas()
  }

  const onClickImage = (index, value, e) => {
    idImage.current = e.currentTarget.id
    //handler
    const element = e.currentTarget
    const imageElement = element.querySelector('.item-image-content')
    clearCanvas()
    if (imageElement?.classList.contains('--image-matched')) {
      const buttonMatchingId = element.dataset['buttonid']
      doMatching(buttonMatchingId, idImage.current, true)
      return
    }
    const listButtonElement = document.querySelectorAll(
      '.block-item__list .block-item_button',
    )
    //hidden icon matching button.

    listButtonElement.forEach((ele) => {
      if (!ele.dataset['imageid']) {
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'visible'
        if (!ele?.classList.contains('selected')) {
          ele.classList.add('selected')
        }
      } else {
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'hidden'
      }
    })

    const divImageAnswer = document.querySelectorAll(
      '.pt-o-game-draw-line-2 .item-image',
    )
    divImageAnswer.forEach((ele, i) => {
      const imageEle = ele.querySelector('.item-image-content')
      if (imageEle?.classList.contains('--image-selected')) {
        imageEle.classList.remove('--image-selected')
      }
      if (ele.id != idImage.current) {
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => (icon.style.visibility = 'hidden'))
      } else {
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => (icon.style.visibility = 'visible'))
      }
    })
    imageElement?.classList.add('--image-selected')

    if (idButton.current && idImage.current) {
      doMatching(idButton.current, idImage.current, true)
    }
  }

  const clearCanvas = () => {
    const canvas = document.getElementById('canvas-draw-line-2')
    if (!canvas) return
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
  }
  const doMatching = (idBtn, idDivImg, isShowMatching = false) => {
    // console.log("id-------",{idBtn,idImg})
    try {
      if (idBtn && idDivImg) {
        const buttonMatching = document.getElementById(idBtn)
        const divImageMatching = document.getElementById(idDivImg)
        const buttonImage = buttonMatching.querySelector('.image-button')
        if (!buttonMatching || !divImageMatching) return
        const imageMatching = divImageMatching.querySelector(
          '.item-image-content',
        )

        if (imageMatching) {
          buttonImage.src = imageMatching.src
          buttonImage.style.display = 'block'
        }

        const listButton = document.querySelectorAll(
          '.pt-o-game-draw-line-2 .block-item_button',
        )
        listButton.forEach((ele) => {
          if (ele?.classList.contains('selected')) {
            ele.classList.remove('selected')
          }
          if (ele?.classList.contains('matching')) {
            ele.classList.remove('matching')
          }
        })

        const listDivImageSelected = document.querySelectorAll(
          '.pt-o-game-draw-line-2 .item-image',
        )
        listDivImageSelected.forEach((ele) => {
          const eleImage = ele.childNodes[0]

          if (eleImage) {
            if (ele.id != idDivImg) {
              if (eleImage?.classList.contains('--image-selected')) {
                eleImage.classList.remove('--image-selected')
              }
            }
          }
        })
        // if(buttonMatching?.classList.contains("matched") && !buttonMatching?.classList.contains("--example")){
        //   buttonMatching.classList.remove("matched")
        // }

        if (!buttonMatching?.classList.contains('matched')) {
          buttonMatching.classList.add('matched')
        }
        const listIconImageMatching = document.querySelectorAll(
          '.pt-o-game-draw-line-2 .icon-matching-image',
        )
        listIconImageMatching.forEach(
          (ele) => (ele.style.visibility = 'hidden'),
        )
        const listIconButtonMatching = document.querySelectorAll(
          '.pt-o-game-draw-line-2 .block-item_button .icon-matching',
        )
        listIconButtonMatching.forEach(
          (ele) => (ele.style.visibility = 'hidden'),
        )
        divImageMatching.childNodes[0].classList.add('--image-matched')

        buttonMatching.dataset['imageid'] = idDivImg
        divImageMatching.dataset['buttonid'] = idBtn

        if (isShowMatching) {
          //show matching
          // buttonMatching.scrollIntoView();
          buttonMatching.classList.add('selected')
          buttonMatching.classList.add('matching')
          divImageMatching.childNodes[0].classList.add('--image-selected')

          const positionButton = buttonMatching.dataset['position']
          const canvas = document.getElementById('canvas-draw-line-2')
          const ctx = canvas.getContext('2d')
          const rectCanvas = canvas.getBoundingClientRect()
          if (buttonMatching?.childNodes.length > 0) {
            if (positionButton === 'left') {
              // console.log("lefttttttt");
              const iconMatchingBtn =
                buttonMatching.querySelector('.icon-matching')
              const rectIconBtn = iconMatchingBtn.getBoundingClientRect()
              const xBtn = rectIconBtn.x - rectCanvas.x + 7
              const yBtn = rectIconBtn.top - rectCanvas.top + 7
              const iconImageMatching =
                divImageMatching.querySelector('.--left')
              // console.log("iconImageMatching====",iconImageMatching)
              const rectImg = iconImageMatching.getBoundingClientRect()
              let xImg = rectImg.x - rectCanvas.x
              const yImg = rectImg.top - rectCanvas.top + 6
              if(isPortrait.current){
                xImg +=6;
              }
              //draw
              ctx.beginPath()
              ctx.moveTo(xBtn, yBtn)
              ctx.lineWidth = 2
              ctx.lineCap = 'round'
              ctx.strokeStyle = buttonMatching?.classList.contains('--example')
                ? '#67ab8d'
                : '#2692ff'
              ctx.lineTo(xImg, yImg)
              ctx.stroke()

              idButton.current = ''
              idImage.current = ''
              divImageMatching.childNodes[0].classList.add('--image-matched')
              iconImageMatching.style.visibility = 'visible'
              iconMatchingBtn.style.visibility = 'visible'
              buttonMatching.classList.replace('selected', 'matching')
            } else if (positionButton === 'right') {
              const iconMatchingBtn =
                buttonMatching.querySelector('.icon-matching')
              const rectIconBtn = iconMatchingBtn.getBoundingClientRect()
              const xBtn = rectIconBtn.x - rectCanvas.x + 7
              const yBtn = rectIconBtn.top - rectCanvas.top + 7
              const iconImageMatching =
                divImageMatching.querySelector('.--right')
              const rectImg = iconImageMatching.getBoundingClientRect()
              const xImg = rectImg.x - rectCanvas.x + 6
              const yImg = rectImg.top - rectCanvas.top + 6

              //draw
              ctx.beginPath()
              ctx.moveTo(xBtn, yBtn)
              ctx.lineWidth = 2
              ctx.lineCap = 'round'
              ctx.strokeStyle = buttonMatching?.classList.contains('--example')
                ? '#67ab8d'
                : '#2692ff'
              ctx.lineTo(xImg, yImg)
              ctx.stroke()

              idButton.current = ''
              idImage.current = ''

              iconImageMatching.style.visibility = 'visible'
              iconMatchingBtn.style.visibility = 'visible'
            }
          }
        }
      }
    } catch (error) {
      console.error('doMatching', error)
    }
  }
  const setIsShowTapescript = (isShow) => {
    const elementScript = document.getElementById(
      'pt-o-game-draw-line-2-script',
    )
    const elementRight = document.querySelector('.pt-o-game-draw-line-2__right')
    const elementLeft = document.querySelector('.pt-o-game-draw-line-2__left')
    if (isShow) {
      elementScript.style.display = 'block'
      elementLeft.style.display = 'none'
      elementRight.style.display = 'none'
    } else {
      elementScript.style.display = 'none'
      elementLeft.style.display = 'flex'
      elementRight.style.display = 'flex'
    }
  }
  const onScrollListButton = (position) => {
    let matchingButton = document.querySelector(
      '.block-item_button_left.matching',
    )
    if (position == 'right') {
      matchingButton = document.querySelector(
        '.block-item_button_right.matching',
      )
    }
    if (matchingButton) {
      matchingButton.classList.remove('matching')
      matchingButton.querySelector('.icon-matching').style.visibility = 'hidden'
      const divImageId = matchingButton.dataset['imageid']
      const divImageElement = document.getElementById(divImageId)
      const matchingImage = divImageElement.querySelector(
        '.item-image-content.--image-selected',
      )
      const matchingicon = divImageElement.querySelector('.icon-matching-image')
      if (matchingImage) {
        matchingImage.classList.remove('--image-selected')
      }
      if (matchingicon) {
        matchingicon.style.visibility = 'hidden'
      }
      if (
        matchingButton?.classList.contains('matched') &&
        matchingButton?.classList.contains('selected')
      ) {
        matchingButton.classList.remove('selected')
      }
      clearCanvas()
      if(matchingButton.classList.contains("--example")){
        if(elementExampleId.current){
          doMatching(elementExampleId.current.idItemButton, elementExampleId.current.idItemImage, true)
        }
      }
    }
  }
  return (
    <div className="pt-o-game-draw-line-2">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-draw-line-2__container">
        <>
          <div
            className="__paper"
            id="pt-o-game-draw-line-2-script"
            style={{ display: 'none' }}
          >
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
          <BlockWave
            className={`pt-o-game-draw-line-2__left 
            ${
              mode.state !== TEST_MODE.play ? 'pt-o-game-draw-line__result' : ''
            }`}
          >
            <AudioPlayer
              className="__player"
              isPlaying={isPlaying}
              setIsPlaying={() => setIsPLaying(!isPlaying)}
            />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <div className="pt-o-game-draw-line-2__right">
            {mode.state !== TEST_MODE.play && (
              <CustomButton
                className="__tapescript"
                onClick={() => setIsShowTapescript(true)}
              >
                Tapescript
              </CustomButton>
            )}
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-draw-line-2-content-id">
              <div
                className="block-item"
                onScroll={() => onScrollListButton('left')}
              >
                <div className="block-item__list">
                  {ansLeft.map((itemLeft, index) => (
                    <div
                      id={itemLeft.key}
                      key={itemLeft.key}
                      data-position="left"
                      className={`block-item_button block-item_button_left ${renderClassNameLeft(
                        index,
                      )}`}
                      data-index={itemLeft.indexAnswer}
                      onClick={(e) => onClichButtonLeft(index, 'left', e)}
                      style={
                        mode.state != TEST_MODE.play
                          ? { pointerEvents: 'none' }
                          : {}
                      }
                    >
                      {/* <button>{itemLeft.answer}</button> */}
                      <div className="__content-answer">
                        <img alt="" src={`/upload/${itemLeft.answer}`}></img>
                      </div>
                      <img
                        style={
                          mode.state !== TEST_MODE.play
                            ? { display: 'none' }
                            : {}
                        }
                        className="icon-deleted"
                        height={25}
                        src="/images/icons/ic-delete-img.png"
                        alt="deleted-image"
                        onClick={onClickDeleted.bind(
                          null,
                          itemLeft.key,
                          'left',
                        )}
                      />
                      <img
                        style={{ visibility: 'hidden' }}
                        className={`icon-matching ${
                          itemLeft.isExam ? '--example-matching' : ''
                        }`}
                        height={10}
                        src={`/images/icons/ic-matching${
                          itemLeft.isExam ? '-ex' : ''
                        }.png`}
                        alt="matching-image"
                      />
                      <img
                        style={{ display: 'none' }}
                        // src={itemLeft.image}
                        alt="image example"
                        className="image-button"
                      />
                      <div className="__bg-blur"></div>
                    </div>
                  ))}
                </div>
              </div>
              <div className="block-item">
                <div
                  className="wrap-image"
                  style={{
                    objectFit: 'contain',
                    maxHeight: '100%',
                    position: 'relative',
                    maxWidth: '100%',
                  }}
                >
                  <img
                    className="image-instruction"
                    alt="image-instruction"
                    id="dl1-image-instruction-id"
                    src={data?.imageInstruction}
                    style={{ visibility: 'hidden', height: '100%' }}
                    onLoad={(e)=>{
                      renderContentClone(e);
                    }}
                  />
                  {data?.answers?.map((item, mIndex) => (
                    <div
                      key={`image_${item.key}`}
                      id={`image_${item.key}`}
                      onClick={onClickImage.bind(null, mIndex, item.image)}
                      className={`item-image ${
                        item.isExam ? '--example-div' : '--matching-div'
                      }`}
                      data-index={item.indexAnswer}
                      style={
                        mode.state != TEST_MODE.play
                          ? { pointerEvents: 'none' }
                          : {}
                      }
                    >
                    </div>
                  ))}
                </div>
              </div>
              <div
                className="block-item"
                onScroll={() => onScrollListButton('right')}
              >
                <div className="block-item__list">
                  {ansRight.map((itemRight, index) => (
                    <div
                      id={itemRight.key}
                      key={itemRight.key}
                      data-position="right"
                      className={`block-item_button block-item_button_right ${renderClassNameRight(
                        index,
                      )}`}
                      data-index={itemRight.indexAnswer}
                      onClick={(e) => onClichButtonLeft(index, 'right', e)}
                      style={
                        mode.state != TEST_MODE.play
                          ? { pointerEvents: 'none' }
                          : {}
                      }
                    >
                      {/* <button>{itemRight.answer}</button> */}
                      <div className="__content-answer">
                        <img alt="" src={`/upload/${itemRight.answer}`}></img>
                      </div>
                      <img
                        style={
                          mode.state !== TEST_MODE.play
                            ? { display: 'none' }
                            : {}
                        }
                        className="icon-deleted"
                        height={25}
                        src="/images/icons/ic-delete-img.png"
                        alt="deleted-image"
                        onClick={onClickDeleted.bind(
                          null,
                          itemRight.key,
                          'right',
                        )}
                      />
                      <img
                        className={`icon-matching --right ${
                          itemRight.isExam ? '--example-matching' : ''
                        }`}
                        height={10}
                        src={`/images/icons/ic-matching${
                          itemRight.isExam ? '-ex' : ''
                        }.png`}
                        alt="matching-image"
                        style={{ visibility: 'hidden' }}
                      />
                      <img
                        style={{ display: 'none' }}
                        // src={itemLeft.image}
                        alt="image example"
                        className="image-button"
                      />
                       <div className="__bg-blur"></div>
                    </div>
                  ))}
                </div>
              </div>
              <canvas id="canvas-draw-line-2" style={{zIndex:6}}></canvas>
            </div>
          </div>
        </>
      </div>
    </div>
  )
}
