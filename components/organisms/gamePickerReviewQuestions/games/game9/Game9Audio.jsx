import { useEffect, useRef, useState } from 'react'

import { CustomButton } from 'practiceTest/components/atoms/button'
import { CustomImage } from 'practiceTest/components/atoms/image'
import { AudioPlayer } from 'practiceTest/components/molecules/audioPlayer'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'
import { BlockWave } from 'practiceTest/components/organisms/blockWave'
import { formatDateTime } from 'practiceTest/utils/functions'

import { convertStrToHtml } from "../../../../../utils/string";

export const Game9Audio = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''
  const questionInstruction = data?.questionInstruction || ''

  let defaultRightColumn = []
  let leftColumn = []
  
  answers.forEach((item, i) => {
    item.left && leftColumn.push(item.left)
    defaultRightColumn.push({
      id: `item-${i}`,
      content: item.right,
      index: i,
      isMatched: false,
    })
  })

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)
  const [isTransparent, setIsTransparent] = useState(true)

  const orderRightColumn = Array.from(Array(defaultRightColumn.length), (e, i) => ({
    index: i,
    isMatched: false,
  }))
  let defaultArr = []
  orderRightColumn.forEach((item, i) => {
    const target = defaultRightColumn[i]
    target.isMatched = item.isMatched
    defaultArr.push(target)
  })
  const [rightColumn, setRightColumn] = useState({ items: defaultArr })
  const audio = useRef(null)

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => setIsTransparent(false), [])

  useEffect(() => {
    setRightColumn({ items: defaultArr })
  }, [answers])

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  return (
    <div
      className={`pt-o-game-9 ${questionInstruction ? '--text' : ''} ${
        audioInstruction ? '--audio' : ''
      } ${isTransparent ? '--transparent' : ''}`}
      style={{width:'100%', height:'100%'}}
    >
      {audioInstruction && (
        <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      )}
      {audioInstruction && isShowTapescript ? (
        <div className="__paper">
          <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
            <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
          </div>
          <BlockPaper>
            <div className="__content">
              <FormatText>{audioScript}</FormatText>
            </div>
          </BlockPaper>
        </div>
      ) : (
        <>
          {questionInstruction && (
            <div className="pt-o-game-9__left">
              <BlockPaper>
                <div className="__content">
                  <FormatText tag="p">{questionInstruction}</FormatText>
                </div>
              </BlockPaper>
            </div>
          )}
          {audioInstruction && (
            <BlockWave className={`pt-o-game-9__left`}>
              <AudioPlayer
                className="__player"
                isPlaying={isPlaying}
                setIsPlaying={() => setIsPLaying(!isPlaying)}
              />
              <span className="__duration">{countDown}</span>
              
            </BlockWave>
          )}
          <div className="pt-o-game-9__right">
            {(
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                >
                  Tapescript
                </CustomButton>
              )}
            <div className="__header" id='header-instruction'>
              <FormatText tag="p">{instruction}</FormatText>
            </div>
              <div className="__content">
                <div className="__left">
                  {leftColumn.map((item, i) => (
                    <div
                      key={i}
                      className={`__tag`}
                    >
                      <span dangerouslySetInnerHTML={{
                        __html: convertStrToHtml(item)
                      }} />
                    </div>
                  ))}
                </div>
                <div className="__right">
                  {rightColumn?.items.map((item, index) => (
                    <div
                      key={item.id}
                      className="__item"
                      style={{
                        maxHeight: `calc(100% / ${rightColumn?.items.length})`
                      }}
                    >
                      <div className={`__tag`}>
                        <span dangerouslySetInnerHTML={{
                          __html: convertStrToHtml(item.content)
                        }} />
                        <div className="__circle" />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
          </div>
        </>
      )}
    </div>
  )
}
