import { useEffect, useState } from 'react'

import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'

import { convertStrToHtml } from "../../../../../utils/string";

export const Game9 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const questionInstruction = data?.questionInstruction || ''

  let defaultRightColumn = []
  let leftColumn = []
  answers.forEach((item, i) => {
    item.left && leftColumn.push(item.left)
    defaultRightColumn.push({
      id: `item-${i}`,
      content: item.right,
      index: i,
      isMatched: false,
    })
  })
  const [isTransparent, setIsTransparent] = useState(true)

  const orderRightColumn =Array.from(Array(defaultRightColumn.length), (e, i) => ({
    index: i,
    isMatched: false,
  }))
  let defaultArr = []
  orderRightColumn.forEach((item, i) => {
    const target = defaultRightColumn[i]
    if (target) {
      target.isMatched = item.isMatched
      defaultArr.push(target)
    }
  })
  const [rightColumn, setRightColumn] = useState({ items: defaultArr })

  useEffect(() => setIsTransparent(false), [])

  return (
    <div
      className={`pt-o-game-9 ${questionInstruction ? '--text' : ''} ${isTransparent ? '--transparent' : ''}`}
      style={{width:'100%', height:'100%'}}
    >
      {(
        <>
          {questionInstruction && (
            <div className="pt-o-game-9__left">
              <BlockPaper>
                <div className="__content">
                  <p dangerouslySetInnerHTML={{
                    __html: convertStrToHtml(questionInstruction)
                  }}></p>
                </div>
              </BlockPaper>
            </div>
          )}
          <div className="pt-o-game-9__right">
            <div className="__header" id='header-instruction'>
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content">
              <div className="__left">
                {leftColumn.map((item, i) => (
                  <div
                    key={i}
                    className={`__tag`}
                  >
                    <span dangerouslySetInnerHTML={{
                      __html: convertStrToHtml(item)
                    }} />
                  </div>
                ))}
              </div>
              <div className="__right">
                {rightColumn.items.map((item, index) => (
                  <div
                    key={item.id}
                    className="__item"
                  >
                    <div className={`__tag`}>
                      <span dangerouslySetInnerHTML={{
                        __html: convertStrToHtml(item.content)
                      }} />
                      <div className="__circle" />
                    </div>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </>
      )}
    </div>
  )
}
