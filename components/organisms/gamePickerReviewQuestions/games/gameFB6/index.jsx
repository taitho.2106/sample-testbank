import { Fragment, useEffect, useRef } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml } from 'utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'

export const GameFB6 = ({ data }) => {
  const questionInstruction = data?.questionInstruction || ''
  const instruction = data?.instruction || ''
  const question = data?.question
  // const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
  const questionArr = convertStrToHtml(question).split('%s%')
  const answers = data?.answers || []
  const indexEx = answers.findIndex((item) => item.answer.startsWith('*'))
  const contentRef = useRef(null)
  useEffect(() => {
    if (indexEx !== -1) {
      const example = contentRef.current.querySelectorAll('.__editableEX')
      example.forEach((item, i) => {
        item.innerHTML = answers[indexEx].answer.substring(1)
      })
    }
  }, [])
  let x = -1
  return (
    <div
      className="pt-o-game-fb6"
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      <div className="pt-o-game-fb6__left">
        <BlockPaper>
          <div className="__content">
            <FormatText tag="p">
              {convertStrToHtml(questionInstruction)}
            </FormatText>
          </div>
        </BlockPaper>
      </div>
      <div className="pt-o-game-fb6__right">
        <BlockBottomGradient className="__container">
          <div className="__header">
            <CustomHeading tag="h6" className="__description">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          </div>
          <div className="__content">
            <div className="__list">
              <Scrollbars universal={true}>
                <div ref={contentRef} className="__content">
                  {questionArr.map((item, i) => {
                    if (i === 0 || i === indexEx + 1) {
                    } else {
                      x = x + 1
                    }
                    return (
                      <Fragment key={i}>
                        {i !== 0 && i !== indexEx + 1 && (
                          <span
                            data-index={x}
                            className={`__editable --empty`}
                            contentEditable={false}
                            style={{
                              pointerEvents: 'none',
                            }}
                          ></span>
                        )}
                        {i !== 0 && i === indexEx + 1 && (
                          <span
                            className={`__editableEx `}
                            contentEditable={false}
                          >
                            {answers[indexEx].answer.substring(1)}
                          </span>
                        )}
                        <span
                          dangerouslySetInnerHTML={{
                            __html: item,
                          }}
                        ></span>
                      </Fragment>
                    )
                  })}
                </div>
              </Scrollbars>
            </div>
          </div>
        </BlockBottomGradient>
      </div>
    </div>
  )
}
