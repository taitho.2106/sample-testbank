import { Fragment, useEffect, useRef } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml, formatHtmlText } from 'utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradientWithHeader } from '../../../../../practiceTest/components/organisms/blockBottomGradient/BlockBottomGradientWithHeader'

export const Game3 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const question = data?.question || ''
  const indexEx = answers.findIndex((item) => item.answer.startsWith('*'))
  // let tmpQuestion = question.replace(/%[0-9]%/g, '%s%')
  const questionArr = convertStrToHtml(question).split('%s%')
  const contentRef = useRef(null)
  let x = -1
  useEffect(() => {
    if (indexEx !== -1) {
      const example = contentRef.current.querySelectorAll('.__editableEX')
      example.forEach((item, i) => {
        item.innerHTML = answers[indexEx].answer.substring(1)
      })
    }
  }, [])

  return (
    <div
      className="pt-o-game-3"
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      <div className="pt-o-game-3__container">
        <BlockBottomGradientWithHeader
          headerChildren={
            <CustomHeading tag="h6" className="__heading">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          }
        >
          <Scrollbars universal={true}>
            <div ref={contentRef} className="__content">
              {questionArr.map((item, i) => {
                if (i === 0 || i === indexEx + 1) {
                } else {
                  x = x + 1
                }
                return (
                  <Fragment key={i}>
                    {i !== 0 && i !== indexEx + 1 && (
                      <span
                        data-index={x}
                        className={`__editable --empty`}
                        contentEditable={false}
                        style={{
                          pointerEvents: 'none',
                        }}
                      ></span>
                    )}
                    {i !== 0 && i === indexEx + 1 && (
                      <span className={`__editableEx`} contentEditable={false}>
                        {answers[indexEx].answer.substring(1)}
                      </span>
                    )}
                    <span
                      dangerouslySetInnerHTML={{
                        __html: item,
                      }}
                    ></span>
                  </Fragment>
                )
              })}
            </div>
          </Scrollbars>
        </BlockBottomGradientWithHeader>
      </div>
    </div>
  )
}
