import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { CustomText } from '../../../../../practiceTest/components/atoms/text'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockCloud } from '../../../../../practiceTest/components/organisms/blockCloud'
import { convertStrToHtml } from "../../../../../utils/string";

export const Game7 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const question = data?.question || ''

  let hintSentence = ''
  const hint = answers[0].start.split('%s%')
  if (hint.length > 1)
    hint.forEach(
      (item, i) => (hintSentence += i !== 0 ? `.......... ${item}` : item),
    )
  else hintSentence = hint[0]

  return (
    <div
      className="pt-o-game-7"
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      <div className="pt-o-game-7__left">
        <BlockCloud className="__container">
          <div className="__box">
            <span
                className="__text"
                dangerouslySetInnerHTML={{
                  __html:convertStrToHtml(question.replaceAll('%s%', `<div class="__space"></div>`))
                }}
            >
            </span>
          </div>
        </BlockCloud>
      </div>
      <div className="pt-o-game-7__right">
        <BlockBottomGradient className="__container">
          <div className="__header">
            <CustomHeading tag="h6" className="__description">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          </div>
          <div className="__content">
            {answers[0]?.start && (
              <div className="__hint">
                <CustomImage
                  className="__icon"
                  alt="hint"
                  src="/pt/images/icons/ic-hint.svg"
                  yRate={0}
                />
                {hintSentence && (
                  <CustomText tag="p" className="__text">
                    HINT: {hintSentence}
                  </CustomText>
                )}
              </div>
            )}
            <div className="__textarea">
              <textarea
                className={`__textarea-box`}
                placeholder="Write your answer here ..."
                style={{
                  pointerEvents: 'none',
                }}
              />
            </div>
          </div>
        </BlockBottomGradient>
      </div>
    </div>
  )
}
