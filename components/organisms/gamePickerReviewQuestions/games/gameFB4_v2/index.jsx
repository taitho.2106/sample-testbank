import { Fragment, useContext, useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml, formatHtmlText } from 'utils/string'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { BlockBottomGradientWithHeader } from '../../../../../practiceTest/components/organisms/blockBottomGradient/BlockBottomGradientWithHeader'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { formatDateTime } from '../../../../../practiceTest/utils/functions'

export const GameFB4_v2 = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const question = data?.question || ''
  const indexEx = answers.findIndex((item)=> item.answer.startsWith('*'))
  // const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
  const questionArr = convertStrToHtml(question).split('%s%')
  

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)
  const [isZoom, setIsZoom] = useState(false)

  const audio = useRef(null)
  const contentRef = useRef(null)

  

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }
  useEffect(()=>{
    if(indexEx !== -1){
        const example = contentRef.current.querySelectorAll('.__editableEX')
        
        example.forEach((item,i)=>{
            item.innerHTML = answers[indexEx].answer.substring(1)
        })
    }
  },[])
  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])
  
  let x = -1;
  return (
    <div className="pt-o-game-fb4-audio" style={{width:'100%', height:'100%'}}>
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-fb4-audio__container" data-animate="fade-in">
        {isShowTapescript ? (
          <div className="__paper">
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
        ) : (
          <>
            <BlockWave
              className={`pt-o-game-fb4-audio__left`}
            >
              <AudioPlayer
                className="__player"
                isPlaying={isPlaying}
                setIsPlaying={() => setIsPLaying(!isPlaying)}
              />
              <span className="__duration">{countDown}</span>
            </BlockWave>
            <div
              className={`pt-o-game-fb4-audio__right ${
                imageInstruction ? '--image' : ''
              }`}
            >
              {(
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                  style={{marginBottom:'3rem'}}
                >
                  Tapescript
                </CustomButton>
              )}
              <BlockBottomGradientWithHeader
                headerChildren={
                  <CustomHeading tag="h6" className="__heading">
                    <FormatText tag="span">{instruction}</FormatText>
                  </CustomHeading>
                }
              >
                <>
                  <div className="__image-instruction">
                    <CustomImage
                      alt="Image instruction"
                      src={`${imageInstruction}`}
                      yRate={0}
                      onClick={() => setIsZoom(true)}
                    />
                  </div>
                  <ImageZooming
                    data={{
                      alt: 'Image Instruction',
                      src: `${imageInstruction}`,
                    }}
                    status={{
                      state: isZoom,
                      setState: setIsZoom,
                    }}
                  />
                </>
                <Scrollbars universal={true}>
                  <div ref={contentRef} className="__content">
                    {questionArr.map((item, i) => {
                    if(i === 0  || i === indexEx + 1){

                    }else{
                        x = x + 1
                    }
                    return(
                      <Fragment key={i}>
                        {i !== 0 && i !== indexEx + 1 && (
                          <span 
                            data-index={x}
                            className={`__editable --empty`}
                            contentEditable={false}
                            style={{
                              pointerEvents:'none'
                            }}
                           
                          ></span>
                        )}
                        {i !== 0 && i === indexEx + 1 && (
                          <span
                            className={`__editableEx`}
                            contentEditable={false}
                          >{answers[indexEx].answer.substring(1)}</span>
                        )}
                        {/* <FormatText tag="span">{item}</FormatText> */}
                        <span
                          dangerouslySetInnerHTML={{
                            __html: (item),
                          }}
                        ></span>
                      </Fragment>
                    )})}
                  </div>
                </Scrollbars>
              </BlockBottomGradientWithHeader>
            </div>
          </>
        )}
        
      </div>
    </div>
  )
}
