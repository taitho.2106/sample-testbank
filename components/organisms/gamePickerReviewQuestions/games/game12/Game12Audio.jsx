import React, { useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml } from '@/utils/string'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { CustomText } from '../../../../../practiceTest/components/atoms/text'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { formatDateTime } from '../../../../../practiceTest/utils/functions'

export const Game12Audio = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  let isMr3 = false;
  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)
  const [shakingItem, setShakingItem] = useState(null)


  const audio = useRef(null)

  const checkNumber = (i) => {
    if (isMr3) return ''

    if (answers.length < 100) {
      if(answers.length === i + 1 && answers.length % 3 === 2)
        return ''
      else return (i % 3 === 1) ? '--down' : ''
    }
  }

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  return (
    <div className="pt-o-game-12" style={{width:'100%', height:'100%'}}>
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-12__container">
        {isShowTapescript ? (
          <div className="__paper">
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
        ) : (
          <>
            { audioInstruction &&
              <BlockWave className={`__left`}>
                <AudioPlayer
                  className="__player"
                  isPlaying={isPlaying}
                  setIsPlaying={() => setIsPLaying(!isPlaying)}
                />
                <span className="__duration">{countDown}</span>
              </BlockWave>
            }
            <BlockBottomGradient className="__right">
              {(
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                  style={{marginBottom:'3rem'}}
                >
                  Tapescript
                </CustomButton>
              )}
              <div className="__header">
                <CustomHeading tag="h6" className="__heading">
                  <FormatText tag="span">{instruction}</FormatText>
                </CustomHeading>
              </div>
              <div className="__content">
                <Scrollbars universal={true}>
                  <div
                    className={`__list ${isMr3 ? '--list-mr3' : ''}`}
                  >
                    {answers.map((item, i) => (
                      <CheckboxItem
                        key={i}
                        className={`${checkNumber(i)} ${
                          i === shakingItem ? '--shake' : ''
                        } ${ isMr3 ? '--mr3' : ''} `}
                        data={item}
                        style={{
                          pointerEvents:'none',
                          
                        }}
                      />
                    ))}
                  </div>
                </Scrollbars>
              </div>
            </BlockBottomGradient>
          </>
        )}
      </div>
    </div>
  )
}

const CheckboxItem = ({ className = '', data, style, onClick }) => {
  return (
    <div
      className={`__checkbox-item ${className}`}
      style={style}
    >
      <div className="__container">
        <CustomText tag="span" className="__text">
          <span dangerouslySetInnerHTML={{ __html: convertStrToHtml(data.text) }}>
          </span>
        </CustomText>
      </div>
      <div className="__shadow"></div>
    </div>
  )
}
