import { useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml, formatHtmlText } from 'utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'

export const GameMC2 = ({ data }) => {
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const questionsGroup = data?.questionsGroup || []
  const questionsGroupExample = questionsGroup.filter((m) =>
    m.question.startsWith('*'),
  )
  const questionsGroupGeneral = questionsGroup.filter(
    (m) => !m.question.startsWith('*'),
  )
  
  const [isZoom, setIsZoom] = useState(false)

  return (
    <div className="pt-o-game-mc2-image" style={{width:'100%', height:'100%'}}>
      <div className="pt-o-game-mc2-image__left">
        <div className="__image">
          <CustomImage
            className="__image-container"
            alt={'img'}
            src={`${imageInstruction}`}
            onClick={() => setIsZoom(true)}
          />
          <ImageZooming
            data={{ alt: 'img', src: `${imageInstruction}` }}
            status={{
              state: isZoom,
              setState: setIsZoom,
            }}
          />
        </div>
      </div>
      <div className="pt-o-game-mc2-image__right">
        <div className="__header">
          <FormatText tag="p">{instruction}</FormatText>
        </div>
        <div className="__content">
          <Scrollbars universal={true}>
            {questionsGroupExample && questionsGroupExample.length > 0 && (
              <div className="__content-example">
                <div className="__title">
                  <span>
                    {`Example${questionsGroupExample.length > 1 ? 's' : ''}:`}
                  </span>
                </div>
                <div className="__content">
                  {questionsGroupExample.map((itemQuestion, index) => {
                    const html = convertStrToHtml(
                      itemQuestion.question.substring(1),
                    ).replaceAll('%s%', `<div class="__space"></div>`)
                    return (
                      <div
                        key={`question-${index}`}
                        className={`mc2-a-example`}
                      >
                        <div className={`mc2-a-example__question`}>
                          <div className="mc2-a-example__question__title">
                            <span
                              dangerouslySetInnerHTML={{
                                __html: html,
                              }}
                            ></span>
                          </div>
                        </div>
                        <div className="mc2-a-example__radio-answer">
                          {itemQuestion.answers.map((itemChild, i) => (
                            <div
                              key={`question-ans-${i}`}
                              className={`mc2-a-multichoiceanswers answer`}
                            >
                              <div className="mc2-a-multichoiceanswers__answer">
                                <ul className="mc2-a-multichoiceanswers__input ">
                                  <li>
                                    <input
                                      type="checkbox"
                                      name={`mc2-id${i}`}
                                      className="option-input checkbox"
                                      defaultChecked={itemChild.isCorrect}
                                      style={{ pointerEvents: 'none' }}
                                    />
                                    <label
                                      dangerouslySetInnerHTML={{
                                        __html: convertStrToHtml(itemChild.text),
                                      }}
                                    ></label>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}
            {questionsGroupGeneral && questionsGroupGeneral.length > 0 && (
              <div className="__content-question">
                <div className="__title">
                  <span>
                    {`Question${questionsGroupGeneral.length > 1 ? 's' : ''}:`}
                  </span>
                </div>
                <div className="__content">
                  {questionsGroupGeneral.map((item, i) => {
                    const html = convertStrToHtml(item.question).replaceAll(
                      '%s%',
                      `<div class="__space"></div>`,
                    )
                    return (
                      <div key={i} className="__radio-group">
                        <CustomHeading tag="h6" className="__radio-heading">
                          <span
                            dangerouslySetInnerHTML={{
                              __html: html,
                            }}
                          ></span>
                        </CustomHeading>
                        <div className="__radio-list">
                          {item.answers.map((childItem, j) => (
                            <div
                              key={j}
                              className={`__radio-item`}
                              style={{
                                pointerEvents:'none'
                              }}
                            >
                              <p
                                className="pt-a-text"
                                dangerouslySetInnerHTML={{
                                  __html: convertStrToHtml(childItem.text),
                                }}
                              ></p>
                              {/* <CustomText tag="p">{childItem.text}</CustomText> */}
                            </div>
                          ))}
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}
          </Scrollbars>
          
        </div>
      </div>
      
    </div>
  )
}
