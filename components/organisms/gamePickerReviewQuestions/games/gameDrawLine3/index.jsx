import { useRef, useState, useContext, useEffect } from 'react'
import { formatDateTime } from 'practiceTest/utils/functions'
import { CustomButton } from 'practiceTest/components/atoms/button'
import { CustomImage } from 'practiceTest/components/atoms/image'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockWave } from 'practiceTest/components/organisms/blockWave'

function GameDrawLine3({ data }) {
  return <GameDrawLine3Web key={`pc_${data.id}`} data={data} />
}

export default GameDrawLine3

function GameDrawLine3Web({ data }) {
  const answer = data?.answers ?? []
  const ansLeft = []
  const ansRight = []
  let indexAnswer = 0
  const indexExample = answer.findIndex((m) => m.isExam)
  if (indexExample != -1) {
    const itemExample = answer.find((m) => m.isExam)
    answer.splice(indexExample, 1)
    answer.splice(0, 0, itemExample)
  }
  
  data.answers = data?.answers?.map((item) => {
    const itemAnswer = item.isExam ? item : { ...item, indexAnswer }
    if (!item.isExam) {
      indexAnswer += 1
    }
    return itemAnswer
  })
  answer.map((item, index) => {
    if (index % 2 === 0) {
      ansLeft.push(item)
    } else {
      ansRight.push(item)
    }
  })
  const instruction = data?.instruction || ''

  const idButton = useRef('')
  const idImage = useRef('')
 
  const isExamPos = useRef({ left: false, right: false })
  const ratio = useRef({ x: 0, y: 0 })
  const renderContent = () => {
    const imageElement = document.getElementsByClassName('image-instruction')[0]
    const imageItemEx = document.getElementsByClassName(
      'item-image --example-div',
    )
    const wrapImage = document.getElementsByClassName('wrap-image')[0]
    const imageOrigion = new Image()
    imageOrigion.onload = function () {
      if(imageOrigion.complete && imageOrigion.naturalWidth > 0 && imageOrigion.naturalHeight > 0){
        wrapImage.style.height = imageElement.offsetHeight
        wrapImage.style.width = imageElement.offsetWidth
       wrapImage.style.position = 'relative'
 
       ratio.current.x = imageElement.offsetWidth / imageOrigion.naturalWidth
       ratio.current.y = imageElement.offsetHeight / imageOrigion.naturalHeight
 
       answer
         .filter((item) => item.isExam)
         .map((item, index) => {
           const node = document.getElementById(`image_${item.key}`)
           while (node.lastElementChild) {
             node.removeChild(node.lastElementChild)
           }
           const elementImg = document.createElement('img')
           const elementImgIconLeft = document.createElement('img')
           const elementImgIconRight = document.createElement('img')
 
           elementImgIconLeft.src = '/images/icons/ic-matching-ex.png'
           elementImgIconLeft.classList.add('icon-matching-image')
           elementImgIconLeft.classList.add('--left')
           elementImgIconLeft.classList.add('--example-icon-matching')
 
           elementImgIconRight.src = '/images/icons/ic-matching-ex.png'
           elementImgIconRight.classList.add('icon-matching-image')
           elementImgIconRight.classList.add('--right')
           elementImgIconRight.classList.add('--example-icon-matching')
           elementImg.style.visibility="hidden"
           node.appendChild(elementImg)
           node.appendChild(elementImgIconLeft)
           node.appendChild(elementImgIconRight)
           elementImg.onload = function () {
             if(elementImg.complete && elementImg.naturalHeight > 0 && elementImg.naturalWidth > 0){
               elementImg.style.visibility="visible"
               const p = item.position.split(':')
               const width = (p[2] - p[0]) * ratio.current.x
               const height = (p[3] - p[1]) * ratio.current.y
               const left = p[0] * ratio.current.x
               const top = p[1] * ratio.current.y
               elementImg.classList.add('item-image-content')
               elementImg.classList.add('--example-image')
               imageItemEx[index].style.position = 'absolute'
               imageItemEx[index].style.left = `${left}px`
               imageItemEx[index].style.top = `${top}px`
               imageItemEx[index].style.width = `${width}px`
               imageItemEx[index].style.height = `${height}px`
               imageItemEx[index].style.transform = `scale(1,1)`
   
               const idItemButton = item.key
               const idItemImage = imageItemEx[index].id
               drawPoint(idItemImage)
               clearCanvas()
               doMatching(idItemButton, idItemImage, true)
             }
           }
           elementImg.src = item.image
         })
      }  
    }
    imageOrigion.src = data?.imageInstruction
  }
  useEffect(() => {
    clearCanvas();
    setTimeout(() => {
      renderContent()
    },150);
  }, [])

  useEffect(() => {
    const canvas = document.getElementById('canvas-draw-line-3')
    const content = document.getElementById('pt-o-game-draw-line-3-content-id')
    canvas.style.position = 'absolute'
    canvas.style.pointerEvents = 'none'
    canvas.style.top = '1rem'
    canvas.style.left = 0
    canvas.style.bottom = '1rem'
    canvas.width = content.offsetWidth
    canvas.height = content.offsetHeight - 20

    ansLeft.map((item) => {
      if (item.isExam) {
        isExamPos.current.left = true
      }
    })
    ansRight.map((item) => {
      if (item.isExam) {
        isExamPos.current.right = true
      }
    })
  }, [])

  const renderClassNameLeft = (index) => {
    let className = 'default'
    if (ansLeft[index].isExam) {
      className = 'matched --example'
    }
    return className
  }
  const renderClassNameRight = (index) => {
    let className = 'default'
    if (ansRight[index].isExam) {
      className = 'matched --example'
    }
    return className
  }

  const drawPoint = (idImg) => {
    const imageElements = document.getElementById(idImg)
      const iconImageRight = imageElements.getElementsByClassName('icon-matching-image --right')[0]
      
      const iconImage = imageElements.querySelector(
        '.icon-matching-image.--left',
      )
      const canvasLeft = document.createElement('canvas')
      const ctxLeft = canvasLeft.getContext('2d')
      const imgHeight = imageElements.childNodes[0].offsetHeight
      const imgWidth = imageElements.childNodes[0].offsetWidth
      ctxLeft.drawImage(imageElements.childNodes[0], 0, 0, imgWidth, imgHeight)
      if(iconImageRight){
        iconImageRight.style.visibility = 'hidden'
      }
      if(!iconImage) return;
      iconImage.style.visibility = 'hidden'
      for (let j = 0; j < imgWidth; j++) {
        const pixelData = ctxLeft.getImageData(j, imgHeight / 2, 1, 1).data
        if (
          pixelData[0] !== 0 ||
          pixelData[1] !== 0 ||
          pixelData[2] !== 0 ||
          pixelData[3] !== 0
        ) {
          iconImage.style.position = 'absolute'
          iconImage.style.top = `${imgHeight / 2 - 6}px`
          iconImage.style.left = `${j - 6}px`
          break
        }
      }
  }

  const clearCanvas = () => {
    const canvas = document.getElementById('canvas-draw-line-3')
    if(!canvas) return;
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
  }
  const doMatching = (idBtn, idDivImg, isShowLine = false) => {
    if (idBtn && idDivImg) {
      const buttonMatching = document.getElementById(idBtn)
      const divImageMatching = document.getElementById(idDivImg)
      const buttonImage = buttonMatching.querySelector('.image-button')
      if (!buttonMatching || !divImageMatching) return
      const imageMatching = divImageMatching.querySelector(
        '.item-image-content',
      )
      const listButton = document.querySelectorAll(
        '.pt-o-game-draw-line-3 .block-item_button',
      )
      listButton.forEach((ele) => {
        if (ele.classList.contains('selected')) {
          ele.classList.remove('selected')
        }
        if (ele.classList.contains('matching')) {
          ele.classList.remove('matching')
        }
      })

      const listDivImageSelected = document.querySelectorAll(
        '.pt-o-game-draw-line-3 .item-image',
      )
      listDivImageSelected.forEach((ele) => {
        const eleImage = ele.childNodes[0]

        if (eleImage) {
          if (ele.id != idDivImg) {
            if (eleImage.classList.contains('--image-selected')) {
              eleImage.classList.remove('--image-selected')
            }
          } else {
            eleImage.classList.add('--image-selected')
          }
        }
      })
      buttonMatching.classList.add('selected')
      buttonMatching.classList.add('matching')
      if (imageMatching) {
        buttonImage.src = imageMatching.src
        buttonImage.style.display = 'block'
      }
      if (!buttonMatching.classList.contains('matched')) {
        buttonMatching.classList.add('matched')
      }
      const listIconImageMatching = document.querySelectorAll(
        '.pt-o-game-draw-line-3 .icon-matching-image',
      )
      listIconImageMatching.forEach((ele) => (ele.style.visibility = 'hidden'))
      const listIconButtonMatching = document.querySelectorAll(
        '.pt-o-game-draw-line-3 .block-item_button .icon-matching',
      )
      listIconButtonMatching.forEach((ele) => (ele.style.visibility = 'hidden'))

      if (isShowLine) {
        const positionButton = buttonMatching.dataset['position']
        const canvas = document.getElementById('canvas-draw-line-3')
        const ctx = canvas.getContext('2d')
        const rectCanvas = canvas.getBoundingClientRect()

        if (
          positionButton === 'left' &&
          buttonMatching?.childNodes.length > 0
        ) {
          const iconMatchingBtn = buttonMatching.querySelector('.icon-matching')
          const rectIconBtn = iconMatchingBtn.getBoundingClientRect()
          const xBtn = rectIconBtn.x - rectCanvas.x + 7
          const yBtn = rectIconBtn.top - rectCanvas.top + 7
          const iconImageMatching = divImageMatching.querySelector('.--left')
          // console.log("iconImageMatching====",iconImageMatching)
          const rectImg = iconImageMatching.getBoundingClientRect()
          const xImg = rectImg.x - rectCanvas.x
          const yImg = rectImg.top - rectCanvas.top + 6
          //draw
          ctx.beginPath()
          ctx.moveTo(xBtn, yBtn)
          ctx.lineWidth = 2
          ctx.lineCap = 'round'
          ctx.strokeStyle = '#67ab8d'
          ctx.lineTo(xImg, yImg)
          ctx.stroke()

          idButton.current = ''
          idImage.current = ''
          divImageMatching.childNodes[0].classList.add('--image-matched')
          iconImageMatching.style.visibility = 'visible'
          iconMatchingBtn.style.visibility = 'visible'
          buttonMatching.classList.replace('selected', 'matching')
        } else if (
          positionButton === 'right' &&
          buttonMatching?.childNodes.length > 0
        ) {
          const iconMatchingBtn = buttonMatching.querySelector('.icon-matching')
          const rectIconBtn = iconMatchingBtn.getBoundingClientRect()
          const xBtn = rectIconBtn.x - rectCanvas.x + 7
          const yBtn = rectIconBtn.top - rectCanvas.top + 7
          const iconImageMatching = divImageMatching.querySelector('.--right')
          const rectImg = iconImageMatching.getBoundingClientRect()
          const xImg = rectImg.x - rectCanvas.x + 6
          const yImg = rectImg.top - rectCanvas.top + 6
          //draw
          ctx.beginPath()
          ctx.moveTo(xBtn, yBtn)
          ctx.lineWidth = 2
          ctx.lineCap = 'round'
          ctx.strokeStyle = '#67ab8d'
          ctx.lineTo(xImg, yImg)
          ctx.stroke()

          idButton.current = ''
          idImage.current = ''
          divImageMatching.childNodes[0].classList.add('--image-matched')
          iconImageMatching.style.visibility = 'visible'
          iconMatchingBtn.style.visibility = 'visible'
          buttonMatching.classList.replace('selected', 'matching')
        }
      }
    }
  }
  const onScrollListButton = (position) => {
    let matchingButton = document.querySelector(
      '.block-item_button_left.matching',
    )
    if (position == 'right') {
      matchingButton = document.querySelector(
        '.block-item_button_right.matching',
      )
    }
    if (matchingButton) {
      matchingButton.classList.remove('matching')
      matchingButton.querySelector('.icon-matching').style.visibility = 'hidden'
      const divImageId = matchingButton.dataset['imageid']
      const divImageElement = document.getElementById(divImageId)
      const matchingImage = divImageElement.querySelector(
        '.item-image-content.--image-selected',
      )
      const matchingicon = divImageElement.querySelector('.icon-matching-image')
      if (matchingImage) {
        matchingImage.classList.remove('--image-selected')
      }
      if (matchingicon) {
        matchingicon.style.visibility = 'hidden'
      }
      clearCanvas();
    }
  }
  return (
    <div className="pt-o-game-draw-line-audio pt-o-game-draw-line-3">
      <div
        data-animate="fade-in"
        className="pt-o-game-draw-line-audio__container"
      >
        <>
          <div
            className="pt-o-game-draw-line-audio__right"
            data-animate="fade-in"
          >
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-draw-line-3-content-id">
              <div
                className="block-item"
                onScroll={() => onScrollListButton('left')}
              >
                <div className="block-item__list">
                  {ansLeft.map((itemLeft, index) => (
                    <div
                      id={itemLeft.key}
                      key={itemLeft.key}
                      data-position="left"
                      className={`block-item_button block-item_button_left ${renderClassNameLeft(
                        index,
                      )}`}
                      data-index={itemLeft.indexAnswer}
                      style={{pointerEvents:"none"}}
                    >
                      <button>{itemLeft.answer}</button>
                      <img
                        style={ { display: 'none' }}
                        className="icon-deleted"
                        height={25}
                        src="/images/icons/ic-delete-img.png"
                        alt="deleted-image"
                      />
                      <img
                        className={`icon-matching ${
                          itemLeft.isExam ? '--example-matching' : ''
                        }`}
                        height={10}
                        src={`/images/icons/ic-matching${itemLeft.isExam?"-ex":""}.png`}
                        alt=""
                      />
                      <img
                        style={{ display: 'none' }}
                        height={30}
                        // src={itemLeft.image}
                        alt="image example"
                        className="image-button"
                      />
                    </div>
                  ))}
                </div>
              </div>
              <div className="block-item">
                <div className="wrap-image">
                  <img
                    className="image-instruction"
                    alt="image-instruction"
                    src={data?.imageInstruction}
                  />
                  {data?.answers?.map((item) => (
                    <div
                      key={`image_${item.key}`}
                      id={`image_${item.key}`}
                      className={`item-image ${
                        item.isExam ? '--example-div' : '--matching-div'
                      }`}
                      data-index={item.indexAnswer}
                      style={{pointerEvents:"none"}}
                    >
                    </div>
                  ))}
                </div>
              </div>
              <div
                className="block-item"
                onScroll={() => onScrollListButton('right')}
              >
                <div className="block-item__list">
                  {ansRight.map((itemRight, index) => (
                    <div
                      id={itemRight.key}
                      key={itemRight.key}
                      data-position="right"
                      className={`block-item_button block-item_button_right ${renderClassNameRight(
                        index,
                      )}`}
                      data-index={itemRight.indexAnswer}
                      style={{pointerEvents:"none"}}
                    >
                      <button>{itemRight.answer}</button>
                      <img
                        style={{ display: 'none' }}
                        className="icon-deleted"
                        height={25}
                        src="/images/icons/ic-delete-img.png"
                        alt="deleted-image"
                      />
                      <img
                        className={`icon-matching --right ${
                          itemRight.isExam ? '--example-matching' : ''
                        }`}
                        height={10}
                        src={`/images/icons/ic-matching${itemRight.isExam?"-ex":""}.png`}
                        alt=""
                      />
                      <img
                        style={{ display: 'none' }}
                        height={30}
                        // src={itemLeft.image}
                        alt="image example"
                        className="image-button"
                      />
                    </div>
                  ))}
                </div>
              </div>
              <canvas id="canvas-draw-line-3" style={{zIndex:6}}></canvas>
            </div>
          </div>
        </>
      </div>
    </div>
  )
}
