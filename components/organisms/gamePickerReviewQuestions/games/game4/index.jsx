import { convertStrToHtml, formatHtmlText } from 'utils/string'

import { CustomText } from '../../../../../practiceTest/components/atoms/text'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'

export const Game4 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''

  return (
    <div className="pt-o-game-4__container">
      <BlockBottomGradient className="__content">
        <div className="__description">
          <CustomText tag="p" className="__text">
            <FormatText tag="span">{instruction}</FormatText>
          </CustomText>
        </div>
        <div className="__list">
          {answers.map((item, i) => (
            <div key={i}>
              <div
                className={`__item`}
                style={{
                  pointerEvents: 'none',
                }}
              >
                <span
                  dangerouslySetInnerHTML={{
                    __html: convertStrToHtml(item.text),
                  }}
                ></span>
              </div>
            </div>
          ))}
        </div>
      </BlockBottomGradient>
    </div>
  )
}
