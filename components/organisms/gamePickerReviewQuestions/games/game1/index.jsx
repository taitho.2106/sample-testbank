import { Fragment } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomText } from '../../../../../practiceTest/components/atoms/text'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { convertStrToHtml } from "../../../../../utils/string";

export const Game1 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const question = data?.question || ''

  return (
    <div
      className="pt-o-game-1__animation"
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      <BlockBottomGradient className={`pt-o-game-1__container --center`}>
        {question && (
          <div className="__left">
            <CustomText tag="p" className="__sample">
              {questionDataTransform.map((item, i) => (
                <Fragment key={i}>
                  {i !== 0 && (
                    <CustomText tag="span" className="__space">
                      {''}
                    </CustomText>
                  )}
                  <FormatText tag="span">{item}</FormatText>
                </Fragment>
              ))}
            </CustomText>
          </div>
        )}
        <div className="__right">
          <div className="__header">
            <CustomHeading tag="h6" className="__description">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          </div>
          <div className="__content">
            <Scrollbars universal={true}>
              <div className="__radio-list">
                {answers.map((item, i) => (
                  <div
                    key={i}
                    className={`__radio-item`}
                    style={{
                      pointerEvents: 'none',
                    }}
                  >
                    <span dangerouslySetInnerHTML={{__html: convertStrToHtml(item.text)}}></span>
                  </div>
                ))}
              </div>
            </Scrollbars>
          </div>
        </div>
      </BlockBottomGradient>
    </div>
  )
}
