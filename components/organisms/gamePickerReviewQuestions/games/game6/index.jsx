import { useRef, useState } from 'react'

import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'

export const Game6 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const examList = answers.filter((item) => item.isExam === 'exam')
  const questionList = answers.filter((item) => item.isExam === 'question')
  const exampleImageList = examList.map((item) => item.imageInstruction)
  const questionImageList = questionList.map((item) => item.imageInstruction)
  const quesForExamList = examList.map((item) => item.question)
  const quesForQuestionList = questionList.map((item) => item.question)
  const answerExample = examList.map((item) => item.answer)
  const answerOrderQuestion = questionList.map((item) => item.answerOrder)

  const dragListGroup = []
  const trueArrGroup = []
  const trueArrGroup1 = []

  const trueAnswerGroup = []
  const trueAnswerGroup1 = []

  for (let qIndex = 0; qIndex < questionList.length; qIndex++) {
    const questionArr = quesForQuestionList[qIndex].split('/') //question
    const dragList = questionArr.map((item, i) => ({ index: i, value: item }))

    let trueAnswer = ''
    let trueAnswer1 = ''

    answerOrderQuestion[qIndex][0].forEach((element) => {
      trueAnswer += ` ${element}`
    })

    answerOrderQuestion[qIndex][1]?.forEach((element) => {
      trueAnswer1 += ` ${element}`
    })

    let trueArr = []
    let tmpTrueArr = []

    questionArr
      .sort((a, b) => b.length - a.length)
      .forEach((item, i) => {
        let indexOf = ` ${trueAnswer.toLowerCase()} `.indexOf(
          ` ${item.toLowerCase().trim()} `,
        )
        let filterList = tmpTrueArr.filter((filter) => filter.pos === indexOf)

        if (indexOf != -1) {
          while (filterList.length > 0) {
            indexOf = trueAnswer
              .toLowerCase()
              .indexOf(item.toLowerCase().trim(), filterList[0].pos + 1)
            filterList = tmpTrueArr.filter((filter) => filter.pos === indexOf)
          }
          tmpTrueArr.push({ index: i, pos: indexOf, value: item })
        }
      })
    trueArr = tmpTrueArr.sort((a, b) => a.pos - b.pos)
    trueArrGroup.push(trueArr)
    trueAnswerGroup.push(trueAnswer)

    let trueArr1 = []
    let tmpTrueArr1 = []

    if (answerOrderQuestion[qIndex][1]) {
      questionArr
        .sort((a, b) => b.length - a.length)
        .forEach((item, i) => {
          let indexOf = ` ${trueAnswer1.toLowerCase()} `.indexOf(
            ` ${item.toLowerCase().trim()} `,
          )
          let filterList = tmpTrueArr1.filter(
            (filter) => filter.pos === indexOf,
          )

          if (indexOf != -1) {
            while (filterList.length > 0) {
              indexOf = trueAnswer1
                .toLowerCase()
                .indexOf(item.toLowerCase().trim(), filterList[0].pos + 1)
              filterList = tmpTrueArr1.filter(
                (filter) => filter.pos === indexOf,
              )
            }
            tmpTrueArr1.push({ index: i, pos: indexOf, value: item })
          }
        })
      trueArr1 = tmpTrueArr1.sort((a, b) => a.pos - b.pos)
      trueArrGroup1.push(trueArr1)
      trueAnswerGroup1.push(trueAnswer1)
    } else {
      trueArrGroup1.push(trueArr1)
      trueAnswerGroup1.push(trueAnswer1)
    }

    dragListGroup.push(dragList)
  }

  const [correctIndex, setIsCorrectIndex] = useState(-1)

  const [chosenList, setChosenList] = useState(Array.from(Array(questionList.length), () => []))

  const dropBox = useRef(null)

  const [isZoomA, setIsZoomA] = useState(false)
  const [isZoomE, setIsZoomE] = useState(false)
  const [zoomStrA, setZoomStrA] = useState('')
  const [zoomStrE, setZoomStrE] = useState('')

  let answerClassName = ''
  let countImg = 0
  for (let i = 0; i < examList.length; i++) {
    const keywordExamLength = quesForExamList[i].split('/').length
    if (exampleImageList[i] === '') countImg += 1
    if (
      (keywordExamLength > 3 && exampleImageList[i] !== '') ||
      keywordExamLength > 6 ||
      countImg === exampleImageList.length
    ) {
      answerClassName = '--flex-content'
      break
    }
  }

  return (
    <div
      className="pt-o-game-6"
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      <div className="pt-o-game-6__instruction">
        <span>{instruction}</span>
      </div>
      <div
        className="pt-o-game-6__container"
        id={`game_question_container_${data.id}`}
      >
        <div className="game-dd1-section">
          {examList.length > 0 && (
            <div className="game-dd1-section__exam-answers">
              <div className="header_exam">
                <span>{`Example${examList.length > 1 ? 's' : ''}:`}</span>
              </div>
              <hr />
              {zoomStrE.length > 0 && (
                <ImageZooming
                  data={{
                    alt: `instruction_example`,
                    src: `${zoomStrE}`,
                  }}
                  status={{
                    state: isZoomE,
                    setState: setIsZoomE,
                  }}
                />
              )}
              <div className={`item_exam ${answerClassName}`}>
                {examList.map((item, eIndex) => (
                  <div
                    key={eIndex + 1}
                    className={`item_exam__list ${
                      item.imageInstruction ? '' : 'no-example-image'
                    }`}
                  >
                    <div className="__image">
                      <img
                        src={item.imageInstruction}
                        alt=""
                        height={100}
                        width={100}
                        onClick={() => {
                          setIsZoomE(true)
                          setZoomStrE(exampleImageList[eIndex])
                        }}
                      />
                    </div>
                    <div className="dd1-example-keyword">
                      <div className="dd1-example-keyword__item">
                        {quesForExamList[eIndex].split('/').map((item, i) => (
                          <div key={i} className="dd1-example-keyword__cell">
                            <span className="dd1-example-keyword__name">
                              {item}
                            </span>
                          </div>
                        ))}
                      </div>
                      <div className="dd1-example-answers">
                        <div className="dd1-example-answers__text">
                          <img
                            alt=""
                            src="/images/icons/ic-arrow-right-sub.png"
                          />
                          <span>{answerExample[eIndex]}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          )}
          <div className="game-dd1-section__content">
            <div className="header_questions">
              <span>{`Question${questionList.length > 1 ? 's' : ''}:`}</span>
            </div>
            <hr />
            {zoomStrA.length > 0 && (
              <ImageZooming
                data={{
                  alt: `instruction_answer`,
                  src: `${zoomStrA}`,
                }}
                status={{
                  state: isZoomA,
                  setState: setIsZoomA,
                }}
              />
            )}
            {questionList.map((item, aIndex) => (
              <div
                key={aIndex + 1}
                className={`box-answer ${
                  item.imageInstruction ? '' : 'no-image'
                }`}
              >
                <div
                  className="item-image"
                  onClick={() => {
                    setIsZoomA(true)
                    setZoomStrA(questionImageList[aIndex])
                  }}
                >
                  <img src={item.imageInstruction} alt="" />
                </div>
                <div className="item-answers" data-animate="fade-in">
                  <div
                    id={`question_${data.id}_${aIndex}`}
                    ref={dropBox}
                    className="__drop-box"
                  >
                    {chosenList[aIndex]?.map((item, i) => (
                      <div key={i} className="__drop-item" style={{}}>
                        <div
                          className={`__drop-content --invisible`}
                          draggable={true}
                          style={{
                            pointerEvents: 'none',
                          }}
                        >
                          {item.value}
                        </div>
                      </div>
                    ))}
                  </div>
                  <div className="__drag-box">
                    <div className="__drag-length">
                      {dragListGroup[aIndex].map((item, i) => (
                        <div
                          key={i}
                          className={`__drag-item  ${
                            chosenList[aIndex]?.findIndex(
                              (find) => find.index === i,
                            ) !== -1
                              ? '--hidden'
                              : ''
                          }`}
                        >
                          <div
                            className={`__drag-content ${
                              correctIndex === aIndex ? '--invisible' : ''
                            }`}
                            draggable={true}
                            style={{
                              pointerEvents: 'none',
                            }}
                          >
                            <span>{item.value}</span>
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}
