import { Fragment } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomText } from '../../../../../practiceTest/components/atoms/text'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
// import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockCloud } from '../../../../../practiceTest/components/organisms/blockCloud'
import { convertStrToHtml } from "../../../../../utils/string";

export const Game2 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const question = data?.question || ''
  const splitQuestion = question.split('%s%')

  return (
    <div
      className="pt-o-game-2"
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      <div className="pt-o-game-2__left">
        <BlockCloud className="__container">
          <div className="__box">
            <CustomText tag="span" className="__text">
              {answers[0].word}
            </CustomText>
          </div>
        </BlockCloud>
      </div>
      <div className="pt-o-game-2__right">
        <BlockBottomGradient className="__container">
          <div className="__header">
            <CustomHeading tag="h6" className="__description">
              {instruction}
            </CustomHeading>
          </div>
          <div className="__content">
            <Scrollbars universal={true}>
              <div className="__content-container">
                <CustomText tag="p" className="__text">
                  {splitQuestion.map((item, i) => (
                    <Fragment key={i}>
                      {i !== 0 && (
                        <span
                          // ref={space}
                          className={`__editable --inline`}
                          style={{
                            pointerEvents: 'none',
                          }}
                        ></span>
                      )}
                      <FormatText tag="span">{convertStrToHtml(item)}</FormatText>
                    </Fragment>
                  ))}
                </CustomText>
              </div>
            </Scrollbars>
          </div>
        </BlockBottomGradient>
      </div>
    </div>
  )
}
