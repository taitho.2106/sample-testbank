import { Fragment, useContext, useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { CustomHeading } from 'practiceTest/components/atoms/heading'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockBottomGradientWithHeader } from 'practiceTest/components/organisms/blockBottomGradient/BlockBottomGradientWithHeader'
import { convertStrToHtml } from '@/utils/string'

export const Game5 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const question = data?.question || ''

  // const replaceQuestion = question.replace(/%[0-9]%/g, '%s%')

  const finalSentences = question.split('%s%')
  const pickers = answers.filter(group=> !group.answer.startsWith("*")).map((group) => group.answerList.split('/'))

  const [currentGroupAnswer, setCurrentGroupAnswer] = useState(null)
  const [isOpenCollapse, setIsOpenCollapse] = useState(false)

  const [chosenAnswers, setChosenAnswers] = useState(
    Array.from(Array(pickers.length), () => null),
  )

  const drawer = useRef(null)

  const checkInline = (i) => {
    if (chosenAnswers[i] !== null && pickers[i][chosenAnswers[i]])
      return '--inline'
    return ''
  }
  useEffect(() => {
    window.addEventListener('click', closeCollapse)
    return () => window.removeEventListener('click', closeCollapse)
  }, [])

  const closeCollapse = (e) => {
    if (
      e.target.classList.contains('__space') ||
      e.target.classList.contains('__drawer') ||
      e.target.closest('.__drawer')
    )
      return
    setIsOpenCollapse(false)
  }

  const handleSpaceClick = (e) => {
    let time = 0
    if (isOpenCollapse) {
      time = 500
      setIsOpenCollapse(false)
    }
    const index = e.target.getAttribute('data-index')
    setCurrentGroupAnswer(parseInt(index))
    setTimeout(() => setIsOpenCollapse(true), time)
  }

  const handlePickerClick = (groupIndex, index) => {
    // update value
    let currentChosenAnswers = chosenAnswers
    currentChosenAnswers[groupIndex] = index

    setChosenAnswers([...currentChosenAnswers])
    setIsOpenCollapse(false)
  }

  useEffect(() => {
    if (drawer?.current)
      drawer.current.scrollTo({ top: 0, behaviour: 'smooth' })
  }, [isOpenCollapse, drawer.current])

  let spaceIndex = -1
  return (
    <div
      className="pt-o-game-5"
      style={{
        height: '100%',
        width: '100%',
      }}
    >
      <div className="pt-o-game-5__container">
        <BlockBottomGradientWithHeader
          className="--sm-header vertical-issue"
          headerChildren={
            <CustomHeading tag="h6" className="__heading">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          }
          customChildren={
            <div
              ref={drawer}
              className={`__drawer ${isOpenCollapse ? '--show' : ''}`}
            >
              {pickers.map((picker, i) => (
                <Fragment key={i}>
                  {picker.map(
                    (item, j) =>
                      i === currentGroupAnswer && (
                        <div
                          key={j}
                          className={`__drawer-item ${
                            j === chosenAnswers[i] ? '--active' : ''
                          }`}
                          style={{
                            pointerEvents: 'all',
                          }}
                          onClick={() => handlePickerClick(i, j)}
                        >
                          {item}
                        </div>
                      ),
                  )}
                </Fragment>
              ))}
            </div>
          }
        >
          <Scrollbars universal={true}>
            <div className="__content">
              {finalSentences.map((item, j) => {
                   const isExample  = answers[j-1]?.answer.startsWith("*");
                   if (j !== 0 && !isExample) spaceIndex++
                return (
                  <Fragment key={j}>
                    {j !== 0 && (
                      <span
                      className={`__space ${isExample?" --example":""} `}
                        data-index={spaceIndex}
                        style={{
                          pointerEvents: isExample?'none':'all',
                        }}
                        onClick={(e) => !isExample && handleSpaceClick(e)}
                      >
                      {isExample && answers[j-1].answer.substring(1)}
                      </span>
                    )}
                    <FormatText tag="span">{convertStrToHtml(item)}</FormatText>
                  </Fragment>
                )
              })}
            </div>
          </Scrollbars>
        </BlockBottomGradientWithHeader>
      </div>
    </div>
  )
}
