import { useState, useEffect, useRef } from 'react'

import { Toggle } from 'rsuite'

import { formatDateTime } from 'practiceTest/utils/functions'
import { useWindowSize } from 'utils/hook'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { convertStrToHtml } from "../../../../../utils/string";

export const GameTF5 = ({ data }) => {
    const answers = data?.answers || []
    const instruction = data?.instruction || ''
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const imageList = data.imageInstruction.split('#')
    const dataList = answers.map((item, index) => {
    return {
        text: item.text,
        isCorrect: item.isCorrect,
        image: imageList[index],
        }
    })
    const examList = dataList
        .filter((item) => item.text.startsWith('*'))
        .map((item) => {
        return {
            text: item.text.replace('*', ''),
            isCorrect: item.isCorrect,
            image: item.image,
        }
    })

    const answerList = dataList.filter((item) => !item.text.startsWith('*'))
    const exampleImageList = examList.map((item) => item.image)
    const answerImageList = answerList.map((item) => item.image)
    
    const [action, setAction] = useState(Array.from(Array(answerList.length), () => false))

    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)
    const [isShowTapescript, setIsShowTapescript] = useState(false)
    const [isZoomA, setIsZoomA] = useState(false)
    const [isZoomE, setIsZoomE] = useState(false)
    const [zoomStrA, setZoomStrA] = useState('')
    const [zoomStrE, setZoomStrE] = useState('')

    let answerClassName = ''
    for (const element of examList) {
        if (element.text.length > 300) {
            answerClassName = '--flex-content'
            break
        }
    }
    const [height, setHeight] = useState(0)
    const [countLine, setCountLine] = useState(0)
    const lineHeight = 22
    const [width] = useWindowSize()
    const padding = width < 1024 ? 10 : 20
    useEffect(() => {
        const el = document.getElementById('span_text')
        setHeight(el.offsetHeight + padding)
        setCountLine(el.offsetHeight / lineHeight)
        renderClassLineHeight()
    }, [width])

    const renderClassLineHeight = () => {
        let className = ''
        if (countLine > 2) {
        className = '--countLine'
        } else {
        className = ''
        }
        return className
    }

    const audio = useRef(null)
    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        const countDowntValue = audio.current.duration - audio.current.currentTime
        if (countDowntValue > 0)
            setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
        else {
            setIsPLaying(false)
            audio.current.currentTime = 0
        }
    }
    
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])

    return (
        <div className="pt-o-game-true-false-type5"
            style={{
                width:'100%',
                height: '100%',
            }}
        >
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>
            <div className="pt-o-game-true-false-type5__container">
            {isShowTapescript ? (
            <div className="__paper">
                <div
                    className="__toggle"
                    onClick={() => setIsShowTapescript(false)}
                >
                    <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                </div>
                <BlockPaper>
                    <div className="__content">
                        <FormatText>{audioScript}</FormatText>
                    </div>
                </BlockPaper>
            </div>
            ) : (
                <>
                <BlockWave
                    className={`pt-o-game-true-false-type5__left`}
                >
                    <AudioPlayer
                        className="__player"
                        isPlaying={isPlaying}
                        setIsPlaying={() => setIsPLaying(!isPlaying)}
                    />
                    <span className="__duration">{countDown}</span>
                </BlockWave>
                <div className={`pt-o-game-true-false-type5__right`}>
                        <CustomButton
                            className="__tapescript"
                            onClick={() => setIsShowTapescript(true)}
                        >
                            Tapescript
                        </CustomButton>
                    <div
                        className={`pt-o-game-true-false-type5__instruction`}
                        id="span_text"
                    >
                        <span>{instruction}</span>
                    </div>
                    <div
                        className="pt-o-game-true-false-type5__view-box"
                        style={{
                            maxHeight: `calc(100% - ${height}px)`,
                        }}
                    >
                        {examList.length > 0 && (
                        <div className="pt-o-game-true-false-type5__exam-answers">
                            <div className="header_exam">
                                <span>Example:</span>
                            </div>
                            {zoomStrE.length > 0 && (
                            <ImageZooming
                                data={{
                                    alt: `instruction_example`,
                                    src: `${zoomStrE}`,
                                }}
                                status={{
                                    state: isZoomE,
                                    setState: setIsZoomE,
                                }}
                            />
                            )}
                            <div className={`item_exam  ${answerClassName}`}>
                            {examList.map((item, index) => (
                                <div key={index + 1} className="item_exam__list">
                                <>
                                    <div className="__image">
                                    <img
                                        src={item.image}
                                        alt="image-example"
                                        height={100}
                                        width={100}
                                        onClick={() => {
                                            setIsZoomE(true)
                                            setZoomStrE(exampleImageList[index])
                                        }}
                                    />
                                    </div>
                                </>
                                <div className="__content_text">
                                    <span
                                        dangerouslySetInnerHTML={{
                                            __html: convertStrToHtml(item.text.trim()),
                                        }}
                                    ></span>
                                </div>
                                <div
                                    className={`__result ${
                                    item.isCorrect ? 'True' : 'False'
                                    }`}
                                >
                                    <span>{item.isCorrect ? 'True' : 'False'}</span>
                                </div>
                                </div>
                            ))}
                            </div>
                        </div>
                        )}
                        <div className="pt-o-game-true-false-type5__play-answers">
                            <div className="header_play-answers">
                                <span>
                                    {answerList.length > 1 ? 'Questions:' : 'Question:'}
                                </span>
                            </div>
                            {zoomStrA.length > 0 && (
                                <ImageZooming
                                data={{
                                    alt: `instruction_answer`,
                                    src: `${zoomStrA}`,
                                }}
                                status={{
                                    state: isZoomA,
                                    setState: setIsZoomA,
                                }}
                                />
                            )}
                            {answerList.map((item, index) => (
                            <div key={index + 1} className="item__answers">
                                <div className="item__answers__list">
                                    <>
                                        <div
                                            className="__image"
                                            onClick={() => {
                                            setIsZoomA(true)
                                            setZoomStrA(answerImageList[index])
                                            }}
                                        >
                                            <img src={item.image} alt="image-questions" />
                                        </div>
                                    </>
                                    <div className={`__content_text`}>
                                        <span
                                            dangerouslySetInnerHTML={{
                                                __html: convertStrToHtml(item.text.trim()),
                                            }}
                                        ></span>
                                    </div>
                                    <div className="__result">
                                        <Toggle
                                            size="md"
                                            checked={action[index]}
                                            checkedChildren="True"
                                            unCheckedChildren="False"
                                        />
                                    </div>
                                </div>
                            </div>
                            ))}
                        </div>
                    </div>

                </div>
                </>
            )}
            </div>
        </div>
    )
}
