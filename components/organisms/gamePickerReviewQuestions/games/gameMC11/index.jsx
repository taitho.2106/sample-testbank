import { Fragment, useEffect, useRef, useState } from 'react'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { GameWrapper } from '../../../../../practiceTest/components/templates/gameWrapper'
import { formatDateTime } from '../../../../../practiceTest/utils/functions'

export const GameMC11 = ({ data }) => {
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const instruction = data?.instruction || ''
    const questionsGroup = data?.questionsGroup || []
    
    const questionsGroupExample = questionsGroup.filter((m) =>m.isExample)
    const questionsGroupGeneral = questionsGroup.filter((m) => !m.isExample)
    const audio = useRef(null)
    
    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)
    const [isShowTapescript, setIsShowTapescript] = useState(false)
    
    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        else {
            const countDownValue = audio.current.duration - audio.current.currentTime
            if (countDownValue > 0)
                setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
            else {
                setIsPLaying(false)
                audio.current.currentTime = 0
            }
        }
    }
    
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])

    return (
        <GameWrapper className="pt-o-game-mc11-audio">
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>
            <div className='pt-o-game-mc11-audio__container'>
            {isShowTapescript ? (
                <div className="__paper">
                    <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
                        <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                    </div>
                    <BlockPaper>
                        <div className="__content">
                            <FormatText>{audioScript}</FormatText>
                        </div>
                    </BlockPaper>
                </div>
            ) : (
        <>
            <BlockWave className={`pt-o-game-mc11-audio__left pt-o-game-mc7__result`}>
                <AudioPlayer
                    className="__player"
                    isPlaying={isPlaying}
                    setIsPlaying={() => setIsPLaying(!isPlaying)}
                />
                <span className="__duration">{countDown}</span>
            </BlockWave>
            <div className="pt-o-game-mc11-audio__right">
                    <CustomButton
                    className="__tapescript"
                    onClick={() => setIsShowTapescript(true)}
                    >
                    Tapescript
                    </CustomButton>
                <div className="__header">
                    <FormatText tag="p">{instruction}</FormatText>
                </div>
                <div className="__content">
                {questionsGroupExample && questionsGroupExample.length > 0 && (
                    <div className="__content-example">
                        <div className="__title">
                            <span>Example:</span>
                        </div>
                        <hr />
                        <div className={`__content`}>
                        {questionsGroupExample.map((itemQuestion, index) => {
                            return (
                            <div
                                key={`example-question-${index}`}
                                className={`mc11-example-group`}
                            >
                                <div className={`mc11-example-group__radio-answer`}>
                                    {itemQuestion.answers.map((itemChild, i) => (
                                        <div 
                                            key={`example-question-ans-${i}`}
                                            className="mc11-example-group__radio-answer__ex-item"
                                            style={{
                                                boxShadow: itemChild.isCorrect ? '0px 0px 0px 1px #2dd238' : '0px 0px 0px 1px #d4e9ff'
                                            }}
                                        >
                                            <img src={itemChild.imageAns} alt=''/>
                                            <div className={`icon-true ${itemChild.isCorrect}`}>
                                                <img 
                                                    src={'/images/icons/ic-checked-true.png'}
                                                    alt=''
                                                />
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            )
                        })}
                        </div>
                    </div>
                )}
                {questionsGroupGeneral && questionsGroupGeneral.length > 0 && (
                <div className="__content-question">
                    <div className="__title">
                        <span>{questionsGroupGeneral.length === 1 ? 'Question:' : 'Questions:'}</span>
                    </div>
                    <hr />
                    <div className="__content">
                    {questionsGroupGeneral.map((item, i) => {
                        return (
                        <Fragment key={`question-item-${i}`}>   
                            {i > 0 && (
                                <div 
                                    className='space-answers'
                                >
                                    <hr />
                                </div>
                            )}
                            <div  className={`__radio-group`}>
                                <div className={`__radio-list`}>
                                {item.answers.map((childItem, j) => (
                                    <div
                                        key={j}
                                        className={`__radio-item`}
                                        style={{
                                            pointerEvents:'none',
                                        }}
                                        
                                    >
                                        <img src={childItem.imageAns}alt=''/>
                                    </div>
                                ))}
                                </div>
                            </div>
                        </Fragment>
                        )
                    })}
                    </div>
                </div>
                )}
                </div>
            </div>
        </>
        )}
            </div>
            
        </GameWrapper>
    )
}
