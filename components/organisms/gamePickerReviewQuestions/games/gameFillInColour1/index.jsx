import { useRef, useState, useEffect } from 'react'

import { CustomButton } from 'practiceTest/components/atoms/button';
import { CustomImage } from 'practiceTest/components/atoms/image';
import { AudioPlayer } from 'practiceTest/components/molecules/audioPlayer';
import { FormatText } from 'practiceTest/components/molecules/formatText';
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper';
import { BlockWave } from 'practiceTest/components/organisms/blockWave';
import { GameWrapper } from 'practiceTest/components/templates/gameWrapper';
import { formatDateTime } from 'practiceTest/utils/functions';
import { useWindowSize } from 'utils/hook';

const LIST_COLOR=[
  { color:'000000',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '00000080'
  },
  { color:'ffffff',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'd4dde780'
  },
  { color:'880015',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '88001580'
  },
  { color:'ed1c24',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'ed1c2480'
  },
  { color:'ff7f27',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'ff7f2780'
  },
  { color:'fcfb2d',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'fcfb2d80'
  },
  { color:'62aa2d',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '62aa2d80'
  },
  { color:'b5e61d',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'b5e61d80'
  },
  {color:'3d009e',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity:'3d009e80'
  },
  {color:'038af9',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '038af980'
  },
  {color:'8400ab',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '8400ab80'
  },
  {color:'c8bfe7',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'c8bfe780'
  },
  {color:'773829',
  icon:'/images/icons/icon-selected-colour-white.png',
  opacity: '77382980'
  },
  {color:'afafaf',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'afafaf80'
  },
  {color:'fb2576',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'fb257680'
  }
  
]
export default function GameFillInColour1({ data }) {
  
  useEffect(() => {
    window.timeout_select_object_1=null;
   }, [])
  const answer = data?.answers.filter(item => !item.isExam) ?? []
  const indexExample = data?.answers?.findIndex(item => item.isExam)
  const isPortrait = useRef( window.matchMedia("(orientation: portrait)").matches)
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  

  const audio = useRef(null)
  const ratio = useRef({ x: 0, y: 0 })
  const renderImage = (e) => {
    try {
     
      const checkEquationValue = (a, b) => {
        if (a - b > 4 || b - a > 4) {
          return false
        } else {
          return true
        }
      }
      const contentImage = document.getElementById('pt-o-game-fill-in-colour-1-content-id')
      const imageElement = document.getElementsByClassName('image-instruction')[0]
      const wraperImage = document.querySelector('.wrap-image')
      const paddingTop = window.getComputedStyle(contentImage,null).getPropertyValue('padding-top')

      if(isPortrait.current && width <= 480){
        const heightContent = contentImage.offsetHeight - 2 * parseInt(paddingTop)
        if(imageElement.naturalHeight > heightContent && imageElement.naturalHeight >= imageElement.naturalWidth){
          imageElement.style.height = `${heightContent}px`
        }
      }
      const imageInstruction = e.target
      imageInstruction.style.visibility = 'visible'
      
      if (
        !checkEquationValue(
          imageInstruction.offsetHeight,
          wraperImage.offsetHeight,
        )
      ) {
        const imgH = wraperImage.offsetHeight
        const imgW =
        wraperImage.offsetHeight *
          (imageInstruction.naturalWidth / imageInstruction.naturalHeight)
          wraperImage.style.height = `${imgH}px`
          wraperImage.style.width = `${imgW}px`
        imageInstruction.style.height = `${imgH}px`
        imageInstruction.style.width = `${imgW}px`
        renderImage(e)
        return
      }
      ratio.current.x = imageElement.offsetWidth / imageInstruction.naturalWidth
      ratio.current.y = imageElement.offsetHeight / imageInstruction.naturalHeight
      if(wraperImage.offsetHeight > imageInstruction.naturalHeight){
        wraperImage.style.height = `${imageInstruction.naturalHeight}px`
      }
      const divImageItem = document.querySelectorAll('.item-image')
      
      divImageItem.forEach((item,mIndex)=>{
        
        const imageItem = new Image()
        imageItem.onload = function(){
          if(imageItem.complete && imageItem.naturalHeight !== 0){
            const canvasItem = document.querySelector(`#canvas-image-item-${mIndex}`)
            const p = data?.answers[mIndex].position.split(':')
            const width = (p[2] - p[0]) * ratio.current.x
            const height = (p[3] - p[1]) * ratio.current.y
            const left = p[0] * ratio.current.x
            const top = p[1] * ratio.current.y
            
            item.style.position = 'absolute'
            item.style.display = 'block'
            item.style.height = `${height}px`
            item.style.width = `${width}px`
            item.style.top = `${top}px`
            item.style.left = `${left}px`

            canvasItem.height = height
            canvasItem.width = width

            const ctx = canvasItem.getContext('2d')
            clearCanvas(canvasItem)
            canvasItem.width = width
            canvasItem.height = height
            const image = item.querySelector('.item-image-content')
            if(mIndex === indexExample){
              ctx.beginPath()
              ctx.drawImage(image, 0, 0, canvasItem.width, canvasItem.height)
              ctx.globalCompositeOperation = 'source-in'
                
              ctx.fillStyle = `#${data?.answers[indexExample].color}80`
              ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
              // ctx.fill()
              image.style.display = 'none'
            }else{
              // console.log("draw");
              ctx.beginPath()
              ctx.drawImage(image, 0, 0, canvasItem.width, canvasItem.height)
              ctx.globalCompositeOperation = 'source-in'
                
              ctx.fillStyle = '#00000000'
              ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
              // ctx.fill()
              image.style.display = 'none'
            }
          }
        }
        imageItem.src = data?.answers[mIndex].image
      })   
      
    } catch (error) {
      console.log("errrrr-----",error)
    }
  }
  const [width, height] = useWindowSize()

  useEffect(() => {
    if(width ==0 || height ==0) return
    isPortrait.current = window.matchMedia("(orientation: portrait)").matches;
    if (window.timeout_select_object_1) {
      clearTimeout(window.timeout_select_object_1)
    }
    const imageItem = document.querySelectorAll(
      '#pt-o-game-fill-in-colour-1-content-id .item-image',
    )
    imageItem.forEach(m=> m.style.display = "none")
    const imageInstruction = document.getElementsByClassName('image-instruction')[0]
    //set size default
    // imageInstruction.style.visibility = 'hidden'
    imageInstruction.style.height = ""
    imageInstruction.style.width= "";
    imageInstruction.parentElement.style.height = ""
    imageInstruction.parentElement.style.width= "";
    imageInstruction.parentElement.parentElement.style.height = ""
    imageInstruction.parentElement.parentElement.style.width= "";
      
    window.timeout_draw_line_1 = setTimeout(() => {
      imageInstruction.src = `${data?.imageInstruction}?t=${new Date().getMilliseconds()}`
    }, 250)
  }, [width, height])
  
  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])
  
  const setIsShowTapescript = (isShow) => {
    const elementScript = document.getElementById(
      'pt-o-game-fill-in-colour-1-script',
    )
    const elementRight = document.querySelector(
      '.pt-o-game-fill-in-colour-audio__right',
    )
    const elementLeft = document.querySelector(
      '.pt-o-game-fill-in-colour-audio__left',
    )
    if (isShow) {
      elementScript.style.display = 'block'
      elementLeft.style.display = 'none'
      elementRight.style.display = 'none'
    } else {
      elementScript.style.display = 'none'
      elementLeft.style.display = 'flex'
      elementRight.style.display = 'flex'
    }
  }
  const clearCanvas = (canvas) => {
    if(!canvas) return;
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
  }

const generateClassName = (i) => {
  if(i >= 0){
    let className = ''
    if(!className){
      className = 'image-default'
    }
    return className
  }
} 

let iAnswer = -1
  return (
    <GameWrapper className="pt-o-game-fill-in-colour-audio pt-o-game-fill-in-colour-1">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div
        // data-animate="fade-in"
        className="pt-o-game-fill-in-colour-audio__container"
      >
        <>
          <div
            className="__paper"
            id="pt-o-game-fill-in-colour-1-script"
            style={{ display: 'none' }}
          >
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
          <BlockWave
            className={`pt-o-game-fill-in-colour-audio__left pt-o-game-circle__result`}
            // data-animate="fade-in"
          >
            <AudioPlayer
              className="__player"
              isPlaying={isPlaying}
              setIsPlaying={() => setIsPLaying(!isPlaying)}
            />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <div
            className="pt-o-game-fill-in-colour-audio__right"
            // data-animate="fade-in"
          >
              <CustomButton
                className="__tapescript"
                onClick={() => setIsShowTapescript(true)}
              >
                Tapescript
              </CustomButton>
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-fill-in-colour-1-content-id">
              <div className='__temp_column'>
              </div>
              <div className='__content_image'>
                <div className="wrap-image" style={{maxWidth:"100%"}}>
                    <img
                      className="image-instruction"
                      alt="image-instruction"
                      src={data?.imageInstruction}
                      style={{ visibility: 'hidden', height: '100%' ,maxWidth:"100%"}}
                      onLoad={(e)=>renderImage(e)}
                    />
                    {data?.answers?.map((item, mIndex) => {
                      if(!item.isExam){
                        iAnswer++
                      }
                      return(
                      <div
                        key={item.key}
                        id={item.key}
                        data-index={`${mIndex}`}
                        data-ianswer={`${item.isExam ? -1 : iAnswer}`}
                        className={`item-image ${item.isExam ? '--example-div' : `--answer-div ${generateClassName(iAnswer)}`}`}
                        style={{
                          pointerEvents: item.isExam ? 'none': 'all',
                        }}
                      >
                        <img 
                          src={item.image}
                          alt=''
                          className={`item-image-content ${mIndex}`}
                        />
                        <canvas
                          id={`canvas-image-item-${mIndex}`}
                        ></canvas>
                      </div>
                    )})}
                </div>
              </div>
              <div className='__content_colour'>
                <div className='__colour_list'>
                  {LIST_COLOR.map((item,mIndex)=>
                    <div key={`color__${mIndex}`}
                      className='__colour_item'
                      data-index={mIndex}
                      data-color={item.color}
                      data-opacity={item.opacity}
                      style={{
                        backgroundColor: `#${item.color}`,
                        cursor: "pointer"
                      }}
                    >
                      <img 
                        className='icon-selected'
                        src={item.icon}
                        alt=''
                      />
                    </div>
                  )}
                </div>
                <div className='__remove_colour_wrap'>
                  <img 
                    src='/images/icons/ic-remove-colour.png'
                    alt=''
                    className='__remove_colour'
                    style={{
                      cursor: "pointer"
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </GameWrapper>
  )
}
