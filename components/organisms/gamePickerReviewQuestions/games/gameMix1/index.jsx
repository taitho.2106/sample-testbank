import { useState, Fragment } from 'react'

import Scrollbars from 'react-custom-scrollbars'
import { Toggle } from 'rsuite'
import { convertStrToHtml } from 'utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomText } from '../../../../../practiceTest/components/atoms/text'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'

export const GameMix1 = ({ data }) => {
  const questionList = data?.questionList || []
  const questionInstruction = data?.questionInstruction || ''
  const instruction = data?.instruction || ''
  
  const dataTF = questionList.filter(item => item.activityType === 8)[0]
  const dataMC5 = questionList.filter(item => item.activityType === 11)[0]

  const answers = dataTF?.answers || []
  const questionsGroup = dataMC5?.questionsGroup || []
  

  const defaultData = [
    Array.from(Array(answers.length), () => false),
    Array.from(Array(questionsGroup.length), (e, i) => null),
  ]

  const [action, setAction] = useState(defaultData[0])
  
  return (
    <div className="pt-o-game-mix1" style={{width:'100%', height:'100%'}}>
      <div className="pt-o-game-mix1__left">
        <BlockPaper>
          <div className="__instrustion">{instruction}</div>
          <div className="__content">
            <span
              dangerouslySetInnerHTML={{
                __html: convertStrToHtml(questionInstruction),
              }}
            ></span>
          </div>
        </BlockPaper>
      </div>
      <div className="pt-o-game-mix1__right">
        <BlockBottomGradient className="__container">
          <div className="__content">
            <div className="__list">
              <Scrollbars universal={true}>
                <div className="des1">
                  <FormatText tag="span">{dataTF?.instruction}</FormatText>
                </div>
                {answers.map((item, i) => (
                  <RadioItem
                    key={i}
                    content={convertStrToHtml(item.text)}
                    index={i}
                    state={action}
                  />
                ))}
                <div className="des2">
                  <FormatText tag="span">{dataMC5?.instruction}</FormatText>
                </div>
                {questionsGroup.map((item, i) => (
                  <div key={i} className="__radio-group">
                    <CustomHeading tag="h6" className="__radio-heading">
                      {convertStrToHtml(item.question).split('%s%').map((splitItem, j) => (
                        <Fragment key={j}>
                          {j % 2 === 1 && <div className="__space"></div>}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: (splitItem),
                            }}
                          ></span>
                        </Fragment>
                      ))}
                    </CustomHeading>
                    <div className="__radio-list">
                      {item.answers.map((childItem, j) => (
                        <div
                          key={j}
                          className={`__radio-item`}
                          style={{
                            pointerEvents:'none',
                          }}
                        >
                          <span
                            dangerouslySetInnerHTML={{
                              __html: convertStrToHtml(childItem.text),
                            }}
                          ></span>
                        </div>
                      ))}
                    </div>
                  </div>
                ))}
              </Scrollbars>
            </div>
          </div>
        </BlockBottomGradient>
      </div>
    </div>
  )
}

const RadioItem = ({ content, index, trueAnswer, state, setState }) => {

  return (
    <div className="__item">
      <div className={`__text`}>
        <FormatText tag="p">{content}</FormatText>
      </div>
      <div
        className="__toggle"
        style={{
          pointerEvents: 'none',
        }}
      >
        <Toggle
          className="__switcher"
          checked={state[index]}
          checkedChildren="True"
          unCheckedChildren="False"
        />
      </div>
    </div>
  )
}
