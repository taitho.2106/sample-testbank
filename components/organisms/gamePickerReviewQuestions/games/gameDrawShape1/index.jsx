import { useRef, useState, useContext, useEffect } from 'react'

import ShapeComponent from 'components/atoms/shape'
import { CustomButton } from 'practiceTest/components/atoms/button'
import { CustomImage } from 'practiceTest/components/atoms/image'
import { AudioPlayer } from 'practiceTest/components/molecules/audioPlayer'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'
import { BlockWave } from 'practiceTest/components/organisms/blockWave'
import { TEST_MODE } from 'practiceTest/interfaces/constants'
import { formatDateTime } from 'practiceTest/utils/functions'
import Guid from 'utils/guid'
import { useWindowSize } from 'utils/hook'
const COLOR = {
  exam: '67ab8d',
  correct: '2dd238',
  danger: 'ff7f74',
  stroke_bg: '97bee5',
}
export default function GameDrawShape1({ data }) {
  const heightItemRef = useRef({ left: 60, right: 36 })
  const leftSelectedIndex = useRef(null)
  let answer = data?.answers ?? []
  const shapes = data.shapes
  const indexExample = answer.findIndex((m) => m.isExam)
  if (indexExample != -1) {
    const itemExample = answer.find((m) => m.isExam)
    answer.splice(indexExample, 1)
    answer.splice(0, 0, itemExample)
  }
  let indexAnswer = -1
  answer = answer.map((m) => {
    if (!m.isExam) {
      indexAnswer += 1
      return { ...m, indexAnswer: indexAnswer }
    } else {
      return m
    }
  })
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const audio = useRef(null)
  const mode={
    state: TEST_MODE.play
  }
  const defaultAnswer = answer.filter((item) => !item.isExam).map(() => null)
  const [answerGroup, setAnswerGroup] = useState(defaultAnswer)
  const [width, height] = useWindowSize()
  useEffect(() => {
    const isPortrait = window.matchMedia('(orientation: portrait)').matches
    if (!isPortrait) {
      if (width > 1300) {
        heightItemRef.current = { left: 80, right: 36 }
      } else if (width > 1024) {
        heightItemRef.current = { left: 60, right: 36 }
      } else if (width > 960) {
        heightItemRef.current = { left: 60, right: 35 }
      } else if (width > 768) {
        heightItemRef.current = { left: 48, right: 30 }
      } else if (width > 640) {
        heightItemRef.current = { left: 42, right: 30 }
      } else {
        heightItemRef.current = { left: 40, right: 26 }
      }
    } else {
      if (width > 480) {
        if (height > 1300) {
          heightItemRef.current = { left: 80, right: 44 }
        } else if (height > 1024) {
          heightItemRef.current = { left: 70, right: 42 }
        } else if (height > 960) {
          heightItemRef.current = { left: 68, right: 38 }
        } else if (height > 768) {
          heightItemRef.current = { left: 62, right: 36 }
        }else{
          heightItemRef.current = { left: 50, right: 36 }
        }
      } else {
        if (height > 1300) {
          heightItemRef.current = { left: 80, right: 36 }
        } else if (height > 1024) {
          heightItemRef.current = { left: 60, right: 36 }
        } else if (height > 960) {
          heightItemRef.current = { left: 60, right: 35 }
        } else if (height > 768) {
          heightItemRef.current = { left: 50, right: 30 }
        } else if (height > 640) {
          heightItemRef.current = { left: 46, right: 30 }
        } else {
          heightItemRef.current = { left: 40, right: 26 }
        }
      }
    }

    setAnswerGroup([...answerGroup])
  }, [width])
  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0) setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  const setIsShowTapescript = (isShow) => {
    const elementScript = document.getElementById('pt-o-game-draw-shape-1-script')
    const elementRight = document.querySelector('.pt-o-game-draw-shape-1__right')
    const elementLeft = document.querySelector('.pt-o-game-draw-shape-1__left')
    if (isShow) {
      elementScript.style.display = 'block'
      elementLeft.style.display = 'none'
      elementRight.style.display = 'none'
    } else {
      elementScript.style.display = 'none'
      elementLeft.style.display = 'flex'
      elementRight.style.display = 'flex'
    }
  }
  const renderShape = (shape, color, height, strokeStyle) => {
    // strokeDasharray, strokeColor="", strokeWidth,
    switch (shape) {
      case 'circle':
        return (
          <ShapeComponent.CircleShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'triangle':
        return (
          <ShapeComponent.TriangleShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'square':
        return (
          <ShapeComponent.SquareShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'star':
        return (
          <ShapeComponent.StarShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'rectangle':
        return (
          <ShapeComponent.RectangleShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'oval':
        return (
          <ShapeComponent.OvalShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
    }
  }

  const dragStart = (e, shape, color, className, indexAnswer) => {
    e.dataTransfer?.setData('item_class', className)
    e.dataTransfer?.setData('shape', shape)
    e.dataTransfer?.setData('color', color)
    if (indexAnswer) {
      e.dataTransfer?.setData('index_data_transfer', indexAnswer)
    }
  }
  const drop = (e, index) => {
    e.preventDefault()
  }
  const dropContentShape = (e) => {
    e.preventDefault()
  }
  const allowDrop = (e) => e.preventDefault()
  return (
    <div className="pt-o-game-draw-shape-1">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() => setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))}
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-draw-shape-1__container">
        <>
          <div className="__paper" id="pt-o-game-draw-shape-1-script" style={{ display: 'none' }}>
            <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
          <BlockWave className={`pt-o-game-draw-shape-1__left`}>
            <AudioPlayer className="__player" isPlaying={isPlaying} setIsPlaying={() => setIsPLaying(!isPlaying)} />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <div className="pt-o-game-draw-shape-1__right">
          <CustomButton className="__tapescript" onClick={() => setIsShowTapescript(true)}>
                Tapescript
              </CustomButton>
            <div className="__header">
            {instruction}
            </div>
            <div className="__content">
              <div className="__content-draw">
                {answer.map((item, index) => {
                  let color = item.color
                  let shape = item.shape
                  let isCorrect = false
                  let className = ''
                  const itemAnswerGroup = answerGroup[item.indexAnswer]
                  if (!item.isExam) {
                    if (mode.state === TEST_MODE.play || mode.state === TEST_MODE.review) {
                      color = itemAnswerGroup?.c
                      shape = itemAnswerGroup?.s
                    }
                  }
                  if (mode.state == TEST_MODE.play) {
                    if (itemAnswerGroup) {
                      className = '--matched'
                    }
                  }
                  if (mode.state == TEST_MODE.check) {
                    isCorrect = true
                    className = '--success'
                  }
                  if (mode.state == TEST_MODE.review) {
                    if(itemAnswerGroup){
                      className = '--danger'
                      if (item.color == color && item.shape == shape) {
                        isCorrect = true
                        className = '--success'
                      }
                    }
                  }
                  if (item.isExam) {
                    className = '--example'
                  }
                  return (
                    <div className={`item-draw-card`} key={item.key}>
                      <div
                        className={`item-draw-shape ${className}`}
                        data-indexanswer={item.indexAnswer}
                        onDrop={(e) => {
                          if (mode.state === TEST_MODE.play && !item.isExam) {
                            e.stopPropagation()
                            const listItemLeft = document.querySelectorAll(
                              '.pt-o-game-draw-shape-1__right .item-draw-shape',
                            )
                            listItemLeft.forEach((m) => {
                              if (!m.classList.contains('--example')) {
                                m.classList.remove('--selected')
                              }
                            })
                            const contentShape = document.querySelector(
                              '.pt-o-game-draw-shape-1__right .__content-shape',
                            )
                            contentShape.classList.remove('--selected')
                            drop(e, item.indexAnswer)
                          }
                        }}
                        onDragOver={(e) => {
                          if (mode.state === TEST_MODE.play && !item.isExam) {
                            // e.currentTarget.classList.add('--selected')
                            allowDrop(e)
                          }
                        }}
                        onDragEnd={(e) => {
                          if (mode.state === TEST_MODE.play && !item.isExam) {
                            const listItemLeft = document.querySelectorAll(
                              '.pt-o-game-draw-shape-1__right .item-draw-shape',
                            )
                            listItemLeft.forEach((m) => {
                              if (!m.classList.contains('--example')) {
                                m.classList.remove('--selected')
                              }
                            })
                            const contentShape = document.querySelector(
                              '.pt-o-game-draw-shape-1__right .__content-shape',
                            )
                            contentShape.classList.remove('--selected')
                          }
                        }}
                      >
                        <div
                          className="item-draw-shape__image"
                          draggable={!item.isExam && mode.state === TEST_MODE.play}
                          onDragStart={(e) => {
                            if (mode.state === TEST_MODE.play && !item.isExam && itemAnswerGroup) {
                              const listItemLeft = document.querySelectorAll(
                                '.pt-o-game-draw-shape-1__right .item-draw-shape',
                              )
                              listItemLeft.forEach((m) => {
                                if (!m.classList.contains('--example')) {
                                  m.classList.add('--selected')
                                }
                              })
                              const contentShape = document.querySelector(
                                '.pt-o-game-draw-shape-1__right .__content-shape',
                              )
                              contentShape.classList.add('--selected')
                              dragStart(e, shape, color, 'item-draw-shape__image', item.indexAnswer + '')
                            }
                          }}
                        >
                          {shape && color && renderShape(shape, `#${color}`, heightItemRef.current.left, null)}
                          {!item.isExam &&
                          shape &&
                          color &&
                          (mode.state == TEST_MODE.check || mode.state == TEST_MODE.review) ? (
                            <img className="icon-result" alt="" src={`/images/icons/ic-${isCorrect}-so.png`}></img>
                          ) : (
                            ''
                          )}
                        </div>
                      </div>
                      <span className="item-draw-text">{`${item.isExam ? 'Example' : item.indexAnswer + 1}`}</span>
                    </div>
                  )
                })}
              </div>
              <div
                className="__content-shape"
                draggable={false}
                onDrop={(e) => {
                  dropContentShape(e)
                }}
                onDragOver={(e) => {
                  if (e.dataTransfer?.types.indexOf('index_data_transfer') != -1) {
                    allowDrop(e)
                  }
                }}
              >
                <div>
                  {Object.keys(shapes).map((key, index) => {
                    const shapeItem = key
                    return (
                      <div className="list-shape" key={`shape_${key}`}>
                        {shapes[key].map((color) => {
                          let className = ''
                          let strokeDasharray = ''
                          let strokeColor = ''
                          let strokeWidth = 0
                          const isExam =
                            answer.findIndex((m) => m.color == color && m.shape == shapeItem && m.isExam) != -1
                          let itemAnswerGroupIndex = answerGroup.findIndex(
                            (item) => item && item.s == shapeItem && item.c == color,
                          )
                          if (isExam) {
                            color = `${COLOR.stroke_bg}55`
                            strokeColor = `#${COLOR.exam}`
                            strokeDasharray = '2,2'
                            strokeWidth = 1
                          } else {
                            if (mode.state == TEST_MODE.check) {
                              className = '--success'
                              const itemAnswer = answer.find((m) => m.shape == shapeItem && m.color == color)
                              if (itemAnswer) {
                                color = `${COLOR.correct}20`
                                strokeColor = `#${COLOR.correct}`
                                itemAnswerGroupIndex = itemAnswer.indexAnswer
                                strokeWidth = 1
                                strokeDasharray = '2,2'
                              } else {
                                itemAnswerGroupIndex = -1
                                color = `${color}20`
                                strokeColor = `#${COLOR.correct}`
                                strokeWidth = 0
                              }
                            } else {
                              if (mode.state == TEST_MODE.play) {
                                if (itemAnswerGroupIndex != -1) {
                                  //matched
                                  color = `${COLOR.stroke_bg}22`
                                  strokeColor = `#${COLOR.stroke_bg}`
                                  strokeDasharray = '2,2'
                                  strokeWidth = 1
                                }
                              }
                              if (mode.state == TEST_MODE.review) {
                                const itemAnswer = answer.find((m) => m.shape == shapeItem && m.color == color)
                                if (itemAnswerGroupIndex !== -1) {
                                  className = '--danger'
                                  color = `${COLOR.danger}20`
                                  strokeColor = `#${COLOR.danger}`
                                  strokeWidth = 1
                                  strokeDasharray = '2,2'

                                  if (itemAnswer != null) {
                                    if (itemAnswer.indexAnswer == itemAnswerGroupIndex) {
                                      color = `${COLOR.correct}20`
                                      strokeColor = `#${COLOR.correct}`
                                      strokeWidth = 1
                                      strokeDasharray = '2,2'
                                      className = 'success'
                                    }
                                  }
                                } else {
                                  color = `${color}20`
                                  strokeColor = `#${COLOR.stroke_bg}`
                                  strokeWidth = 0
                                  strokeDasharray = '2,2'
                                }
                              }
                            }
                          }
                          const id = `${shapeItem}_${color}`
                          return (
                            <div
                              key={Guid.newGuid()}
                              className={`item-shape ${className}`}
                              id={id}
                              data-shape={shapeItem}
                              data-color={color}
                              draggable={false}
                              onClick={() => {
                                if (leftSelectedIndex.current != null) {
                                  answerGroup[parseInt(leftSelectedIndex.current)] = { s: key, c: color }
                                  const listItemLeft = document.querySelectorAll(
                                    '.pt-o-game-draw-shape-1__right .item-draw-shape',
                                  )
                                  listItemLeft.forEach((m) => m.classList.remove('--selected'))
                                  setAnswerGroup([...answerGroup])
                                }
                              }}
                              onDrag={(e) => {
                                // e.target.style.transform="scale(1.2,1.2)"
                              }}
                              onDragStart={(e) => {
                                const listItemLeft = document.querySelectorAll(
                                  '.pt-o-game-draw-shape-1__right .item-draw-shape',
                                )
                                listItemLeft.forEach((m) => {
                                  if (!m.classList.contains('--example')) {
                                    m.classList.add('--selected')
                                  }
                                })
                                mode.state === TEST_MODE.play && itemAnswerGroupIndex == -1 && !isExam
                                dragStart(e, shapeItem, color, 'item-shape')
                              }}
                              
                            >
                              {renderShape(shapeItem, `#${color}`, heightItemRef.current.right, {
                                strokeDasharray,
                                strokeColor,
                                strokeWidth,
                              })}
                             {itemAnswerGroupIndex != -1 && (
                                <span draggable={false} className={`text-index-matching --text-in-shape__${shapeItem}`}>
                                  {itemAnswerGroupIndex + 1}
                                </span>
                              )}
                              {isExam && (
                                <span draggable={false} className={`text-index-matching ${isExam ? '--example' : ''} --text-in-shape__${shapeItem}`}>
                                  Ex
                                </span>
                              )}
                            </div>
                          )
                        })}
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </div>
  )
}
