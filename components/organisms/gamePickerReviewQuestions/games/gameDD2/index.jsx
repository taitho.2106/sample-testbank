import { useState, useEffect, useRef } from 'react'

import { formatDateTime } from 'practiceTest/utils/functions'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'

export const GameDD2 = ({ data }) => {
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const instruction = data?.instruction || ''
    let answers = data?.answers || []
    let indexAnswer = 0
    const indexExample = answers.findIndex((m) => m.isExam)
    if (indexExample != -1) {
        const itemExample = answers.find((m) => m.isExam)
        answers.splice(indexExample, 1)
        answers.splice(0, 0, itemExample)
    }
    answers = answers.map((item, index) => {
        const itemAnswer = item.isExam ? item : { ...item, indexAnswer }
        if (!item.isExam) {
            indexAnswer += 1
        }
        return itemAnswer
    })

    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)   
    const [touchStartTimeStamp, setTouchStartTimeStamp] = useState(0)
    const audio = useRef(null)
    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        else {
            const countDownValue = audio.current.duration - audio.current.currentTime
            if (countDownValue > 0)
                setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
            else {
                setIsPLaying(false)
                audio.current.currentTime = 0
            }
        }
    }
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])


    const setIsShowTapescript = (isShow) => {
        const elementScript = document.getElementById('pt-o-game-dd2-script')
        const elementRight = document.querySelector('.pt-o-game-dd2__right')
        const elementLeft = document.querySelector('.pt-o-game-dd2__left')
        if (isShow) {
            elementScript.style.display = 'block'
            elementLeft.style.display = 'none'
            elementRight.style.display = 'none'
        } else {
            elementScript.style.display = 'none'
            elementLeft.style.display = 'flex'
            elementRight.style.display = 'flex'
        }
    }

    const answersList = answers.filter((item) => !item.isExam)

    const [allKeyWord, setAllKeyWord] = useState([])
    useEffect(() => {
        let currentIndex = answersList.length
        let randomIndex
        while(currentIndex !== 0){
            randomIndex = Math.floor(Math.random() * currentIndex)
            currentIndex--
            [answersList[currentIndex], answersList[randomIndex]] = [answersList[randomIndex], answersList[currentIndex]];
        }
        setAllKeyWord([...answersList])
    }, [data.id])

    return (
        <div
            className="pt-o-game-dd2"
            style={{
                height: '100%',
                width: '100%',
            }}
        >
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>
            <div className='pt-o-game-dd2__container'>
                <div className="__paper" id="pt-o-game-dd2-script" style={{ display: 'none' }}>
                    <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
                        <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                    </div>
                    <BlockPaper>
                        <div className="__content">
                            <FormatText>{audioScript}</FormatText>
                        </div>
                    </BlockPaper>
                </div>
                <BlockWave className={`pt-o-game-dd2__left`}>
                    <AudioPlayer
                        className="__player"
                        isPlaying={isPlaying}
                        setIsPlaying={() => setIsPLaying(!isPlaying)}
                    />
                    <span className="__duration">{countDown}</span>
                </BlockWave>
                <div className={`pt-o-game-dd2__right`}>
                    <CustomButton
                        className="__tapescript"
                        onClick={() => setIsShowTapescript(true)}
                    >
                        Tapescript
                    </CustomButton>
                    <div className={`__header`} id="span_text">
                        <FormatText tag="p">{instruction}</FormatText>
                    </div>
                    <div className="__content">
                        <div className={`drag-drop-main-section`}>
                            {answers.map((item, caIndex) => {        
                                return (
                                    <div
                                        key={item.key + '_' + caIndex}
                                        className={item.isExam ? `example-area` : `drag-drop-area ${indexExample === 0 ? caIndex - 1 : caIndex}`}
                                    >
                                        <div 
                                            className={!item.isExam ? 
                                                `image-keyword-item ${indexExample === 0 ? 
                                                    caIndex - 1 : caIndex}` :
                                                    `image-example-item`}
                                        >
                                            <img 
                                                alt=''
                                                src={item.image}
                                                id={`image_${item.key}`}
                                            />
                                        </div>
                                        {item.isExam && (
                                        <div className={`example-input-cell`}>
                                            <span>{item.answer.replace('*','')}</span>
                                        </div>
                                        )}
                                        {!item.isExam && (
                                        <div className={`keyword-input-cell`}>
                                            <span className={''} id={`span_${item.key}`}>                            
                                            </span>
                                        </div>
                                        )}
                                    </div>
                                )
                            })}
                        </div>
                        <div className={`keyword-list-section`}>
                        {allKeyWord.map((item, kIndex) => {                                
                                return (
                                <div
                                    id={'div-'+item.key}
                                    key={kIndex}
                                    className={`__drag-item`}                                                          
                                >
                                    <div
                                        className={`__drag-content`}
                                        style={{
                                            pointerEvents: 'none',
                                        }}
                                    >
                                        <span id={item.key} style={{userSelect:"none"}}>
                                            {item.answer}
                                        </span>
                                    </div>    
                                </div>   
                               )                         
                            }
                            )}                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}