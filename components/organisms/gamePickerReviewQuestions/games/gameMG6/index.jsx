import { useState } from 'react'

import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'

import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { GameWrapper } from '../../../../../practiceTest/components/templates/gameWrapper'

export const GameMG6 = ({ data }) => {
  const answers = data?.answers
  const answersExample = data?.answersExample || []
  const instruction = data?.instruction || ''
  let defaultRightColumn = []
  let leftColumn = []

  answers.forEach((item, i) => {
    item.left && leftColumn.push(item.left)
    defaultRightColumn.push({
      id: `item-${i}`,
      content: item.right,
      index: i,
      isMatched: false,
    })
  })

  const [isZoom, setIsZoom] = useState(false)
  const [imageStr, setImageStr] = useState('')
  const orderRightColumn = Array.from(Array(defaultRightColumn.length), (e, i) => ({
    index: i,
    isMatched: false,
  }))
  let defaultArr = []
  orderRightColumn.forEach((item, i) => {
    const target = defaultRightColumn[i]
    if (target) {
      target.isMatched = item.isMatched
      defaultArr.push(target)
    }
  })
  const [rightColumn, setRightColumn] = useState({ items: defaultArr })

  const getStyle = (style, snapshot) => {
    if (!snapshot.isDragging) return {}
    if (!snapshot.isDropAnimating) return style
    return {
      ...style,
      transitionDuration: `0.001s`,
    }
  }

  return (
    <GameWrapper className="pt-o-game-mg6">
      <>
        <div className={`pt-o-game-mg6__right`}>
          <div className="__header" id="header-instruction">
            <FormatText tag="p">{instruction}</FormatText>
          </div>
          <div className="__content">
            <ImageZooming
              data={{ alt: 'img', src: `${imageStr}` }}
              status={{
                state: isZoom,
                setState: setIsZoom,
              }}
            />
            {answersExample && answersExample.length > 0 && (
              <div>
                <div className="__example">
                  <span className="__title">Example:</span>
                  <div className="__center-column">
                    <div className={`__left-item`}>
                      <div className="__left-item__image">
                        <img
                          src={answersExample[0].left}
                          alt=""
                          onClick={() => {
                            setIsZoom(true)
                            setImageStr(answersExample[0].left)
                          }}
                        />
                      </div>
                      <div className="__left-item__linked">
                        <div></div>
                        <div></div>
                      </div>
                    </div>
                    <div className={`__right-item`}>
                      <div className="__right-item__linked">
                        <div></div>
                        <div></div>
                      </div>
                      <div className="__right-item__image">
                        <img
                          src={answersExample[0].right}
                          alt=""
                          onClick={() => {
                            setIsZoom(true)
                            setImageStr(answersExample[0].right)
                          }}
                        />
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            )}
            <div className="__title-question">
              <span className="__title">{`Question${
                leftColumn.length > 1 ? 's' : ''
              }:`}</span>
            </div>
            <div>
              <div className={`__left`}>
                {leftColumn.map((item, i) => (
                  <div key={i} className={`__tag`}>
                    <div className="__left-item__image">
                      <img
                        src={item}
                        alt=""
                        onClick={() => {
                          setIsZoom(true)
                          setImageStr(item)
                        }}
                      />
                    </div>
                  </div>
                ))}
              </div>
              <div
                className={`__right`}
                style={{
                  pointerEvents: 'none',
                }}
              >
                <DragDropContext>
                  <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                      <div {...provided.droppableProps} ref={provided.innerRef}>
                        {rightColumn.items.map((item, index) => (
                          <div key={item.id} className="__item">
                            <Draggable draggableId={item.id} index={index}>
                              {(provided, snapshot) => (
                                <div
                                  className={`__tag ${
                                    snapshot.isDragging ? '--dragging' : ''
                                  } ${
                                    item.isMatched === true ? '--matched' : ''
                                  } `}
                                  ref={provided.innerRef}
                                  {...provided.draggableProps}
                                  {...provided.dragHandleProps}
                                  style={getStyle(
                                    provided.draggableProps.style,
                                    snapshot,
                                  )}
                                >
                                  <div className="__right-item__image">
                                    <img
                                      src={item.content}
                                      alt=""
                                      onClick={() => {
                                        setIsZoom(true)
                                        setImageStr(item.content)
                                      }}
                                    />
                                  </div>
                                  <div className="__circle"></div>
                                </div>
                              )}
                            </Draggable>
                          </div>
                        ))}
                        {provided.placeholder}
                      </div>
                    )}
                  </Droppable>
                </DragDropContext>
              </div>
            </div>
          </div>
        </div>
      </>
    </GameWrapper>
  )
}
