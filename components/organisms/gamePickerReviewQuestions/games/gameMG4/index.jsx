import { useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { convertStrToHtml } from "../../../../../utils/string";
export const GameMG4 = ({ data }) => {
  const answers = data?.answers
  const answersExample = data?.answersExample || []
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const audioScript = data.audioScript
  let defaultRightColumn = []
  let leftColumn = []
  let trueAnswer = []
  let wrongAnswer = Array.from(Array(answers.length), (e, i) => i)

  let checkClassItemLeft30 =
    answers.filter((m) => m.right && m.right.length > 200).length > 0 || (answersExample.length>0 && answersExample[0].right.length > 200)
  let checkClassItemRight30 =
    answers.filter((m) => m.left && m.left.length > 200).length > 0 || (answersExample.length>0 && answersExample[0].left.length > 200)
  answers.forEach((item, i) => {
    item.left && leftColumn.push(item.left)
    defaultRightColumn.push({
      id: `item-${i}`,
      content: item.right,
      index: i,
      isMatched: false,
    })
    if (item.rightAnswerPosition !== -1) {
      trueAnswer.push(item.rightAnswerPosition)
      wrongAnswer = wrongAnswer.filter(
        (filter) => filter !== item.rightAnswerPosition,
      )
    }

    ///check class
    if (item.left && item.left.length > 50) {
      checkClassItemLeft30 = false
    }
    if (item.right && item.right.length > 50) {
      checkClassItemRight30 = false
    }
  })
  if(answersExample.length>0){
    if(answersExample[0].left.length>50){
      checkClassItemLeft30 = false
    }
    if(answersExample[0].right.length>50){
      checkClassItemRight30 = false
    }
  }
  const [isZoom, setIsZoom] = useState(false)

  const orderRightColumn =  Array.from(Array(defaultRightColumn.length), (e, i) => ({
    index: i,
    isMatched: false,
  }))
  let defaultArr = []
  orderRightColumn.forEach((item, i) => {
    const target = defaultRightColumn[i]
    if (target) {
      target.isMatched = item.isMatched
      defaultArr.push(target)
    }
  })
  const [rightColumn, setRightColumn] = useState({ items: defaultArr })

  let leftClass =
    (checkClassItemLeft30 && checkClassItemRight30) ||
    (!checkClassItemLeft30 && !checkClassItemRight30)
      ? ''
      : checkClassItemLeft30
      ? '__33'
      : ''
  let rightClass =
    (checkClassItemLeft30 && checkClassItemRight30) ||
    (!checkClassItemLeft30 && !checkClassItemRight30)
      ? ''
      : checkClassItemRight30
      ? '__33'
      : ''
  if (leftClass && !rightClass) {
    rightClass = '__66'
  } else if (rightClass && !leftClass) {
    leftClass = '__66'
  }
  return (
    <div className="pt-o-game-mg4" style={{width:'100%', height:'100%'}}>
      <>
        {imageInstruction && (
          <div className="pt-o-game-mg4__left">
            <div className="__image">
              <img
                alt="image"
                className="__image-container"
                src={imageInstruction}
                onMouseDown={() => setIsZoom(true)}
              ></img>
              <ImageZooming
                data={{ alt: 'img', src: `${imageInstruction}` }}
                status={{
                  state: isZoom,
                  setState: setIsZoom,
                }}
              />
              <span className="__title-note">{audioScript}</span>
            </div>
          </div>
        )}
        <div
          className={`pt-o-game-mg4__right ${imageInstruction ? '' : '__100w'}`}
        >
          <div className="__header" id="header-instruction">
            <FormatText tag="p">{instruction}</FormatText>
          </div>
          <div className="__content">
            <Scrollbars universal={true}>
              {imageInstruction && (
                <div className="__image">
                  <img
                    alt="image"
                    className="__image-container"
                    src={imageInstruction}
                    onMouseDown={() => setIsZoom(true)}
                  ></img>
                  <ImageZooming
                    data={{ alt: 'img', src: `${imageInstruction}` }}
                    status={{
                      state: isZoom,
                      setState: setIsZoom,
                    }}
                  />
                  <span className="__title-note">{audioScript}</span>
                </div>
              )}
              {answersExample && answersExample.length > 0 && (
                <div>
                  <div className="__example">
                    <span className="__title">Example:</span>
                    <div className="__center-column">
                      <div className={`__left-item ${leftClass}`}>
                        <span className="__left-item__text"
                          dangerouslySetInnerHTML={{
                          __html: convertStrToHtml(answersExample[0].left)
                        }}>
                        </span>
                        <div className="__left-item__linked">
                          <div></div>
                          <div></div>
                        </div>
                      </div>
                      <div className={`__right-item ${rightClass}`}>
                        <div className="__right-item__linked">
                          <div></div>
                          <div></div>
                        </div>
                        <span className="__right-item__text"
                          dangerouslySetInnerHTML={{
                            __html: convertStrToHtml(answersExample[0].right)
                          }}
                        >
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className="__title-question">
                <span className="__title">{`Question${leftColumn.length > 1 ? 's' : ''}:`}</span>
              </div>
              <div>
                <div className={`__left ${leftClass}`}>
                  {leftColumn.map((item, i) => (
                    <div
                      key={i}
                      className={`__tag `}
                    >
                      <span dangerouslySetInnerHTML={{
                        __html: convertStrToHtml(item)
                      }}></span>
                    </div>
                  ))}
                </div>
                <div className={`__right ${rightClass}`}>
                  {rightColumn.items.map((item, index) => (
                    <div key={item.id} className="__item">
                      <div className={`__tag`}>
                        <span
                          dangerouslySetInnerHTML={{
                            __html: convertStrToHtml(item.content)
                          }}
                        />
                        <div className="__circle" />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </Scrollbars>
          </div>
        </div>
      </>
    </div>
  )
}
