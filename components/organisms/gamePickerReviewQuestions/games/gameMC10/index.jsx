import { useState, useEffect, useRef } from 'react'

import { formatDateTime } from 'practiceTest/utils/functions'
import { convertStrToHtml } from 'utils/string'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'

export const GameMC10 = ({ data }) => {
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const instruction = data?.instruction || ''
    const imageInstruction = data?.imageInstruction?.split('#') || []
    const questionsGroup = data?.questionsGroup || []

    const questionsGroupExample = questionsGroup.filter((m) =>
        m.question.startsWith('*'),
    )
    const questionsGroupGeneral = questionsGroup.filter(
        (m) => !m.question.startsWith('*'),
    )

    const audio = useRef(null)
    const [isZoomE, setIsZoomE] = useState(false)
    const [isZoomA, setIsZoomA] = useState(false)
    const [zoomStrA, setZoomStrA] = useState('')

    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)
    const [isShowTapescript, setIsShowTapescript] = useState(false)

    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        else {
            const countDownValue = audio.current.duration - audio.current.currentTime
            if (countDownValue > 0)
                setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
            else {
                setIsPLaying(false)
                audio.current.currentTime = 0
            }
        }
    }
    
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])

    return (
        <div 
            className="pt-o-game-mc10"
            style={{ width: '100%', height: '100%' }}
        >
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>
            <div className='pt-o-game-mc10__container'>
            {isShowTapescript ? (
                <div className="__paper">
                    <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
                        <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                    </div>
                    <BlockPaper>
                        <div className="__content">
                            <FormatText>{audioScript}</FormatText>
                        </div>
                    </BlockPaper>
                </div>
            ) : (
            <>
                <BlockWave className={`pt-o-game-mc10__left`}>
                    <AudioPlayer
                        className="__player"
                        isPlaying={isPlaying}
                        setIsPlaying={() => setIsPLaying(!isPlaying)}
                    />
                    <span className="__duration">{countDown}</span>
                </BlockWave>
                <div className={`pt-o-game-mc10__right`}>
                    <CustomButton
                        className="__tapescript"
                        onClick={() => setIsShowTapescript(true)}
                    >
                        Tapescript
                    </CustomButton>
                    <div className={`__header`} id="span_text">
                        <FormatText tag="p">{instruction}</FormatText>
                    </div>
                    <div className="__content">
                        {questionsGroupExample.length > 0 && (
                            <div className="__content-example">
                                <div className="__content-example__header">Example:</div>
                                <hr />
                                {questionsGroupExample.map((answer, answerIndex) => {
                                    const html = convertStrToHtml(
                                        answer.question.substring(1),
                                    ).replaceAll('%s%', `<div class="__space"></div>`)
                                    return (
                                        <div
                                            key={`example-${answerIndex}`}
                                            className="__content-example__item"
                                        >
                                            {html && <div className="__item__title">
                                                <span
                                                dangerouslySetInnerHTML={{
                                                    __html: html,
                                                }}
                                                ></span>
                                            </div>}
                                            <div className="__item__container">
                                                <div className="__item__image">
                                                    <div>
                                                        <img
                                                            alt=""
                                                            src={
                                                                imageInstruction[
                                                                questionsGroupExample.length > 0 ? 0 : null
                                                                ]
                                                            }
                                                            onClick={() => setIsZoomE(true)}
                                                        />
                                                    </div>
                                                </div>
                                                <ImageZooming
                                                    data={{
                                                        alt: 'Image Instruction',
                                                        src: `${
                                                        imageInstruction[
                                                            questionsGroupExample.length > 0 ? 0 : null
                                                        ]
                                                        }`,
                                                    }}
                                                    status={{
                                                        state: isZoomE,
                                                        setState: setIsZoomE,
                                                    }}
                                                />
                                                <div className="__item__question">
                                                    <div className={`mc10-a-multichoiceanswers answer`}>
                                                        <div className="mc10-a-multichoiceanswers__answer">
                                                            <ul className="mc10-a-multichoiceanswers__input ">
                                                                {answer.answers.map((item, index) => (
                                                                <li key={`question-ans-${index}`}>
                                                                    <input
                                                                        type="checkbox"
                                                                        name={`mc10-id${index}`}
                                                                        className="option-input checkbox"
                                                                        defaultChecked={item.isCorrect}
                                                                        style={{ pointerEvents: 'none' }}
                                                                    />
                                                                    <label
                                                                        dangerouslySetInnerHTML={{
                                                                            __html: convertStrToHtml(item.text),
                                                                        }}
                                                                    ></label>
                                                                </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        )}
                        <ImageZooming
                            data={{
                                alt: 'Image Instruction',
                                src: `${zoomStrA}`,
                            }}
                            status={{
                                state: isZoomA,
                                setState: setIsZoomA,
                            }}
                        />
                        <div className="__content-question">
                                <div className="__content-question__header">{`Question${questionsGroupGeneral.length > 1 ? 's' : ''}:`}</div>
                                <hr />
                                {questionsGroupGeneral.map((answer, answerIndex) => {
                                    const html = convertStrToHtml(answer.question).replaceAll(
                                        '%s%',
                                        `<div class="__space"></div>`,
                                    )
                                    return (
                                        <div
                                            key={`question-mc10-${answerIndex}`}
                                            className="__content-question__item"
                                        >
                                            {html && 
                                                <div className="__item__title">
                                                    <span
                                                        className="text"
                                                        dangerouslySetInnerHTML={{
                                                        __html: html,
                                                        }}
                                                    ></span>
                                                </div>
                                            }
                                            <div className="__item__container">
                                                <div className="__item__image">
                                                    <div>
                                                        <img
                                                            alt=""
                                                            src={
                                                                imageInstruction[
                                                                    questionsGroupExample.length > 0
                                                                    ? answerIndex + 1
                                                                    : answerIndex
                                                                ]
                                                            }
                                                            onClick={() => {
                                                            setIsZoomA(true),
                                                                setZoomStrA(
                                                                    imageInstruction[
                                                                        questionsGroupExample.length > 0
                                                                        ? answerIndex + 1
                                                                        : answerIndex
                                                                    ],
                                                                )
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="__item__question">
                                                    <div className="__radio-list">
                                                    {answer.answers.map((item, mIndex) => {
                                                        return (
                                                        <div
                                                            key={`__radio-item-${mIndex}`}
                                                            className={`__radio-item`}
                                                            style={{
                                                                pointerEvents: 'none',
                                                            }}
                                                        >
                                                            <label
                                                                className="pt-a-text"
                                                                dangerouslySetInnerHTML={{
                                                                    __html: convertStrToHtml(item.text),
                                                                }}
                                                            ></label>
                                                        </div>
                                                        )
                                                    })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                        </div>
                    </div>
                </div>
            </>
            )}
            </div>
        </div>
    )
}
