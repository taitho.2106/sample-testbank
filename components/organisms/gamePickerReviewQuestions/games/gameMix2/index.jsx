import {  useState, Fragment } from 'react'

import { convertStrToHtml } from 'utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'

export const GameMix2 = ({ data }) => {
  const questionList = data?.questionList || []
  const instruction = data?.instruction || ''
  const questionInstruction = data?.questionInstruction || ''
  
  const dataSL1 = questionList.filter(item => item.activityType === 8)[0]
  const dataMC5 = questionList.filter(item => item.activityType === 15)[0]

  const answers = dataSL1?.answers || []
  const choices = answers[0].answerList.split('/')

  const questionsGroup = dataMC5?.questionsGroup || []

  const defaultData = [
    Array.from(Array(answers.length), () => null),
    Array.from(Array(questionsGroup.length), (e, i) => null),
  ]

  const [action, setAction] = useState(defaultData[0])

  return (
    <div className="pt-o-game-mix2" style={{width:'100%', height:'100%'}}>
      <div className="pt-o-game-mix2__left">
        <BlockPaper>
          <div className="__instrustion">{instruction}</div>
          <div className="__content">
          <span
              dangerouslySetInnerHTML={{
                __html: convertStrToHtml(questionInstruction),
              }}
            ></span>
          </div>
        </BlockPaper>
      </div>
      <div className="pt-o-game-mix2__right">
        <BlockBottomGradient className="__container">
          <div className="__content">
            <div className="__list">
              {/* <Scrollbars universal={true}> */}
                <div className="__header">
                  <CustomHeading tag="h6" className="__description">
                    <FormatText tag="span">{dataSL1?.instruction}</FormatText>
                  </CustomHeading>
                </div>
                <div className="__title">
                  {choices.map((item, i) => (
                    <CustomHeading key={i} className="__heading">
                      <span>{item}</span>
                    </CustomHeading>
                  ))}
                </div>
                {answers.map((item, i) => (
                  <RadioItem
                    key={i}
                    choices={choices}
                    data={item.text}
                    index={i}
                    state={action}
                  />
                ))}
                <div className="des2">
                  <FormatText tag="span">{dataMC5?.instruction}</FormatText>
                </div>
                {questionsGroup.map((item, i) => (
                  <div key={i} className="__radio-group">
                    <CustomHeading tag="h6" className="__radio-heading">
                      {convertStrToHtml(item.question).split('%s%').map((splitItem, j) => (
                        <Fragment key={j}>
                          {j % 2 === 1 && <div className="__space"></div>}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: (splitItem),
                            }}
                          ></span>
                        </Fragment>
                      ))}
                    </CustomHeading>
                    <div className="__radio-list">
                      {item.answers.map((childItem, j) => (
                        <div
                          key={j}
                          className={`__radio-item`}
                          style={{
                            pointerEvents: 'none',
                          }}
                          
                        >
                          <span
                            dangerouslySetInnerHTML={{
                              __html: convertStrToHtml(childItem.text),
                            }}
                          ></span>
                        </div>
                      ))}
                    </div>
                  </div>
                ))}
              {/* </Scrollbars> */}
            </div>
          </div>
        </BlockBottomGradient>
      </div>
      
    </div>
  )
}

const RadioItem = ({ choices, data, index, trueValue, state, setState }) => {

  return (
    <div className="__item">
      <div className="__text">
        <FormatText tag="p">{convertStrToHtml(data)}</FormatText>
      </div>
      <div
        className={`__radio ${state[index] === choices[0] ? '--true' : ''
          }`}
        style={{ pointerEvents:'none' }}
      ></div>
      <div
        className={`__radio ${state[index] === choices[1] ? '--false' : ''
          } `}
        style={{ pointerEvents: 'none' }}
      ></div>
      <div
        className={`__radio ${state[index] === choices[2] ? '--not-given' : ''
          } `}
        style={{ pointerEvents: 'none' }}
      ></div>
    </div>
  )
}
