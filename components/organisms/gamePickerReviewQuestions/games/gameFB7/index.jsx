import { useContext, useEffect, useState, useRef, Fragment } from 'react'

import PageEnd from '@rsuite/icons/PageEnd'
import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml, formatHtmlText } from 'utils/string'

import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'


export const GameFB7 = ({ data }) => {
  const answers = data?.answers || []
  const imageInstruction = data?.imageInstruction
  const instruction = data?.instruction || ''
  const question = data?.question ;

  // const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
  const questionArr = convertStrToHtml(question).split('%s%')
  const indexEx = answers.findIndex((item)=> item.answer.startsWith('*'))

  const [isZoom, setIsZoom] = useState(false)

  const contentRef = useRef(null)


  useEffect(()=>{
    if(indexEx !== -1){
        const example = contentRef.current.querySelectorAll('.__editableEX')
        
        example.forEach((item,i)=>{
            item.innerHTML = answers[indexEx].answer.substring(1)
        })
    }
  },[])
  let x = -1;
  return (
    <div className="pt-o-game-fb7" style={{height:'100%', width:'100%'}}>
      <div className="pt-o-game-fb7__left">
        <div className="top">
          <div className="image-left">
            <CustomImage
              className="__image-container"
              alt="Image Question"
              src={imageInstruction}
              yRate={0}
              onClick={() => setIsZoom(true)}

            />
            <ImageZooming
            data={{ alt: question, src: imageInstruction }}
            status={{
              state: isZoom,
              setState: setIsZoom,
            }}
          />
          </div>
        </div>
        <div className="bottom">
          <span>{instruction}</span>
        </div>
      </div>
      <div className="pt-o-game-fb7__right">
        <BlockBottomGradient className="__container">
          <div className="__content">
            <div className="__list">
              <Scrollbars universal={true}>
                  <div ref={contentRef} className="__content">
                    {questionArr.map((item, i) => {
                      if(i === 0  || i === indexEx + 1){

                      }else{
                          x = x + 1
                      }
                      return (
                      <Fragment key={i}>
                        {i !== 0 && i !== indexEx + 1 && (
                          <span
                            data-index={x}
                            className={`__editable --empty`}
                            contentEditable={false}
                            style={{
                              pointerEvents:'none'}}
                          ></span>
                        )}
                        {i !== 0 && i === indexEx + 1 && (
                          <span
                            className={`__editableEx `}
                            contentEditable={false}
                          >{answers[indexEx].answer.substring(1)}</span>
                        )}
                        {/* <FormatText tag="span">{" " + item + " "}</FormatText> */}
                        <span
                          dangerouslySetInnerHTML={{
                            __html: (item),
                          }}
                        ></span>
                      </Fragment>
                    )})}
                  </div>
                </Scrollbars>
            </div>
          </div>
        </BlockBottomGradient>
      </div>
      
    </div>
  )
}

