import {useEffect, useRef, useState } from 'react'

import { convertStrToHtml } from 'utils/string'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { formatDateTime } from '../../../../../practiceTest/utils/functions'

export const GameMC7 = ({ data }) => {
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const instruction = data?.instruction || ''
    const questionsGroup = data?.questionsGroup || []
    
    const questionsGroupExample = questionsGroup.filter((m) =>
        m.question.startsWith('*'),
    )
    const questionsGroupGeneral = questionsGroup.filter(
        (m) => !m.question.startsWith('*'),
    )
    const audio = useRef(null)

    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)
    const [isShowTapescript, setIsShowTapescript] = useState(false)

    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        else {
            const countDownValue = audio.current.duration - audio.current.currentTime
            if (countDownValue > 0)
                setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
            else {
                setIsPLaying(false)
                audio.current.currentTime = 0
            }
        }
    }
    
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])

    const renderClassName = (value) => {
        let answerClassName = ''
        if (value?.answers?.length === 4) {
            answerClassName = '--images-flex-wrap'
        }
        else{
            answerClassName = '--images-multi-line'
        }
    return answerClassName

    }

    const renderClassNameEx = (value) => {
        let answerClassName = ''
        if (value?.answers?.length === 4) {
            answerClassName = '--images-flex-wrap'
        }
        else{
            answerClassName = '--images-multi-line'
        }
        return answerClassName
    }
    
    return (
        <div className="pt-o-game-mc7-audio" style={{width:'100%', height:'100%'}}>
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>
            <div className='pt-o-game-mc7-audio__container'>
            {isShowTapescript ? (
                <div className="__paper">
                    <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
                        <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                    </div>
                    <BlockPaper>
                        <div className="__content">
                            <FormatText>{audioScript}</FormatText>
                        </div>
                    </BlockPaper>
                </div>
            ) : (
        <>
            <BlockWave className={`pt-o-game-mc7-audio__left`}>
                <AudioPlayer
                    className="__player"
                    isPlaying={isPlaying}
                    setIsPlaying={() => setIsPLaying(!isPlaying)}
                />
                <span className="__duration">{countDown}</span>
            </BlockWave>
            <div className="pt-o-game-mc7-audio__right">
                {(
                    <CustomButton
                    className="__tapescript"
                    onClick={() => setIsShowTapescript(true)}
                    style={{marginBottom:'3rem'}}
                    >
                    Tapescript
                    </CustomButton>
                )}
                <div className="__header">
                    <FormatText tag="p">{instruction}</FormatText>
                </div>
                <div className="__content">
                {questionsGroupExample && questionsGroupExample.length > 0 && (
                    <div className="__content-example">
                        <div className="__title">
                            <span>
                                {`Example${questionsGroupExample.length > 1 ? 's' : ''}:`}
                            </span>
                        </div>
                        <div className={`__content`}>
                        {questionsGroupExample.map((itemQuestion, index) => {
                            const html = convertStrToHtml(itemQuestion.question.substring(1),).replaceAll('%s%', `<div class="__space"></div>`)
                            return (
                            <div
                                key={`question-${index}`}
                                className={`mc7-example-group`}
                            >
                                <div className={`mc7-example-group__question`}>
                                    <div className="mc7-example-group__question__title">
                                        <span
                                        dangerouslySetInnerHTML={{
                                            __html: html,
                                        }}
                                        ></span>
                                    </div>
                                </div>
                                <div className={`mc7-example-group__radio-answer ${renderClassNameEx(itemQuestion)}`}>
                                {itemQuestion.answers.map((itemChild, i) => (
                                    <div 
                                        key={`question-ans-${i}`}
                                        className="mc7-example-group__radio-answer__ex-item"
                                        style={{
                                            boxShadow: itemChild.isCorrect ? '0px 0px 0px 5px #2dd238' : '0px 0px 0px 2px #d4e9ff'
                                        }}
                                    >
                                        <img src={itemChild.imageAns} alt=''/>
                                        <div className={`icon-true ${itemChild.isCorrect}`}>
                                            <img 
                                                src={'/images/icons/ic-checked-true.png'}
                                                alt=''
                                            />
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                            )
                        })}
                        </div>
                    </div>
                    )}
                    {questionsGroupGeneral && questionsGroupGeneral.length > 0 && (
                    <div className="__content-question">
                        <div className="__title">
                            <span>
                                {`Question${questionsGroupGeneral.length > 1 ? 's' : ''}:`}
                            </span>
                        </div>
                        <div className="__content">
                        {questionsGroupGeneral.map((item, i) => {
                            const html = convertStrToHtml(item.question).replaceAll(
                            '%s%',
                            `<div class="__space"></div>`,
                            )
                            return (
                            <div key={i} className="__radio-group">
                                {i > 0 && (<div style={{ width: '70%', transform: 'translateX(25%)'}}><hr style={{border:'1px solid #2692ff'}}/></div>)
                                }
                                <CustomHeading tag="h6" className="__radio-heading">
                                    <span
                                        dangerouslySetInnerHTML={{
                                        __html: html,
                                        }}
                                    ></span>
                                </CustomHeading>
                                <div className={`__radio-list ${renderClassName(item)}`}>
                                {item.answers.map((childItem, j) => (
                                    <div
                                        key={j}
                                        className={`__radio-item`}
                                        style={{
                                            pointerEvents:'none'
                                        }}
                                       
                                    >
                                        <img src={childItem.imageAns}alt=''/>
                                        <div className={`icon-true`}>
                                            <img 
                                                src={'/images/icons/ic-checked-false.png'}
                                                alt=''
                                            />
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                            )
                        })}
                        </div>
                    </div>
                    )}
                </div>
            </div>
        </>
        )}
            </div>
            
        </div>
    )
}
