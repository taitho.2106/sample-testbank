import { useState } from 'react'

import { Toggle } from 'rsuite'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { convertStrToHtml } from "../../../../../utils/string";

export const GameTF3 = ({ data }) => {
  const answers = data?.answers || []
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''

  const answerList = answers.filter((item) => !item.text.startsWith('*'))
  const examList = answers
    .filter((item) => item.text.startsWith('*'))
    .map((item) => {
      return {
        text: item.text.replace('*', ''),
        isCorrect: item.isCorrect,
      }
    })

  const [isZoom, setIsZoom] = useState(false)
  const defaultData = Array.from(Array(answerList.length), () => false)
  const [action, setAction] = useState(defaultData)


  return (
    <div className="pt-o-game-tf3" style={{width:'100%', height:'100%'}}>
      <div className="pt-o-game-tf3__container">
        <div className="pt-o-game-tf3__left">
          <div className="__image">
            <img
              alt="instruction-image"
              src={`${imageInstruction}`}
              onClick={() => setIsZoom(true)}
            />
          </div>
          <ImageZooming
            data={{ alt: 'instruction', src: `${imageInstruction}` }}
            status={{
              state: isZoom,
              setState: setIsZoom,
            }}
          />
        </div>
        <div className="pt-o-game-tf3__right">
          <div className="__container">
            <div className="__header">
              <CustomHeading tag="h6" className="__description">
                <FormatText tag="span">{instruction}</FormatText>
              </CustomHeading>
            </div>
            <div className="__content" id="content-scrollbar">
              <div
                className="__example"
                style={{ display: examList.length > 0 ? '{}' : 'none' }}
              >
                <div className="header-example">
                  <span>{examList.length > 1 ? 'Examples:' : 'Example:'}</span>
                </div>
                {examList.map((item, i) => (
                  <div key={i + 1} className="list-example">
                    <div className="__example-text">
                      <span dangerouslySetInnerHTML={{__html: convertStrToHtml(item.text) }}></span>
                    </div>
                    <div
                      className={`__result ${
                        item.isCorrect ? 'True' : 'False'
                      }`}
                    >
                      <span>{item.isCorrect === true ? 'True' : 'False'}</span>
                    </div>
                  </div>
                ))}
              </div>
              <div className="__play-answers">
                <div className="header-answer">
                  <span>
                    {answerList.length > 1 ? 'Questions:' : 'Question:'}
                  </span>
                </div>
                <div className="list_answers">
                  {answerList.map((item, index) => (
                    <div key={index + 1} className="list-answers__item">
                      <div
                        className={`__answer-text`}
                      >
                        <span dangerouslySetInnerHTML={{__html: convertStrToHtml(item.text) }}></span>
                      </div>
                      <div className="__result" style={{pointerEvents:'none'}}>
                        <Toggle
                          size="md"
                          checked={action[index]}
                          checkedChildren="True"
                          unCheckedChildren="False"
                          
                        />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}
