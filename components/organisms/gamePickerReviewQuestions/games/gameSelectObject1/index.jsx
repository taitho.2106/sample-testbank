import { useRef, useState, useEffect } from 'react'

import { formatDateTime } from 'practiceTest/utils/functions'
import { useWindowSize } from 'utils/hook'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { GameWrapper } from '../../../../../practiceTest/components/templates/gameWrapper'

export default function GameSelectObject1({ data }) {
  const answer = data?.answers.filter(item => !item.isExam) ?? []
  const trueAnswer = answer
  
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''
  const isPortrait = useRef( window.matchMedia("(orientation: portrait)").matches)
  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
   
  const audio = useRef(null)
  const ratio = useRef({ x: 0, y: 0 })
  
  const renderImage = (e) => {
    try {
      const checkEquationValue = (a, b) => {
        if (a - b > 4 || b - a > 4) {
          return false
        } else {
          return true
        }
      }
      const imageElement = document.getElementsByClassName('image-instruction')[0]
      const wraperImage = document.querySelector('.wrap-image')
      const imageInstruction = e.target
      imageInstruction.style.visibility = 'visible'
      if (
        !checkEquationValue(
          imageInstruction.offsetHeight,
          wraperImage.offsetHeight,
        )
      ) {
        const imgH = wraperImage.offsetHeight
        const imgW =
        wraperImage.offsetHeight *
          (imageInstruction.naturalWidth / imageInstruction.naturalHeight)
          wraperImage.style.height = `${imgH}px`
          wraperImage.style.width = `${imgW}px`
        imageInstruction.style.height = `${imgH}px`
        imageInstruction.style.width = `${imgW}px`
        renderImage(e)
        return
      }
      ratio.current.x = imageElement.offsetWidth / imageInstruction.naturalWidth
      ratio.current.y = imageElement.offsetHeight / imageInstruction.naturalHeight
      if(wraperImage.offsetHeight > imageInstruction.naturalHeight){
        wraperImage.style.height = `${imageInstruction.naturalHeight}px`
      }
      const divImageItem = document.querySelectorAll('.item-image')
      
      divImageItem.forEach((item,mIndex)=>{
        
        const imageItem = new Image()
        imageItem.onload = function(){
          if(imageItem.complete && imageItem.naturalHeight !== 0){

            const p = data?.answers[mIndex].position.split(':')
            const width = (p[2] - p[0]) * ratio.current.x
            const height = (p[3] - p[1]) * ratio.current.y
            const left = p[0] * ratio.current.x
            const top = p[1] * ratio.current.y
            
            item.style.position = 'absolute'
            item.style.display = 'block'
            item.style.height = `${height}px`
            item.style.width = `${width}px`
            item.style.top = `${top}px`
            item.style.left = `${left}px`

          }
        }
        imageItem.src = data?.answers[mIndex].image
      })   
      
    } catch (error) {
      console.log("errrrr-----",error)
    }
  }
  const [width, height] = useWindowSize()
  
  useEffect(() => {
    isPortrait.current = window.matchMedia("(orientation: portrait)").matches;
    if (window.timeout_select_object_1) {
      clearTimeout(window.timeout_select_object_1)
    }
    const imageItem = document.querySelectorAll(
      '#pt-o-game-select-object-1-content-id .item-image',
    )
    imageItem.forEach(m=> m.style.display = "none")
    const imageInstruction = document.getElementsByClassName('image-instruction')[0]
    //set size default
    // imageInstruction.style.visibility = 'hidden'
    imageInstruction.style.height = ""
    imageInstruction.style.width= "";
    imageInstruction.parentElement.style.height = ""
    imageInstruction.parentElement.style.width= "";
    imageInstruction.parentElement.parentElement.style.height = ""
    imageInstruction.parentElement.parentElement.style.width= "";
      
    window.timeout_draw_line_1 = setTimeout(() => {
      imageInstruction.src = `${imageInstruction.src}?t=${new Date().getMilliseconds()}`
    }, 300)
  }, [width, height])
  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  const setIsShowTapescript = (isShow) => {
    const elementScript = document.getElementById(
      'pt-o-game-select-object-1-script',
    )
    const elementRight = document.querySelector(
      '.pt-o-game-select-object-audio__right',
    )
    const elementLeft = document.querySelector(
      '.pt-o-game-select-object-audio__left',
    )
    if (isShow) {
      elementScript.style.display = 'block'
      elementLeft.style.display = 'none'
      elementRight.style.display = 'none'
    } else {
      elementScript.style.display = 'none'
      elementLeft.style.display = 'flex'
      elementRight.style.display = 'flex'
    }
  }

  return (
    <GameWrapper className="pt-o-game-select-object-audio pt-o-game-select-object-1">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div
        className="pt-o-game-select-object-audio__container"
      >
        <>
          <div
            className="__paper"
            id="pt-o-game-select-object-1-script"
            style={{ display: 'none' }}
          >
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
          <BlockWave
            className={`pt-o-game-select-object-audio__left pt-o-game-circle__result`}
          >
            <AudioPlayer
              className="__player"
              isPlaying={isPlaying}
              setIsPlaying={() => setIsPLaying(!isPlaying)}
            />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <div
            className="pt-o-game-select-object-audio__right"
          >
            {(
              <CustomButton
                className="__tapescript"
                onClick={() => setIsShowTapescript(true)}
              >
                Tapescript
              </CustomButton>
            )}
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-select-object-1-content-id">
            <div className="wrap-image">
                  <img
                    className="image-instruction"
                    alt="image-instruction"
                    src={data?.imageInstruction}
                    style={{ visibility: 'hidden', height: '100%' }}
                    onLoad={(e)=>renderImage(e)}
                  />
                  {data?.answers?.map((item, mIndex) => {
                    const p = item.position.split(':')
                    const width = (p[2] - p[0]) * ratio.current.x
                    const height = (p[3] - p[1]) * ratio.current.y
                    const left = p[0] * ratio.current.x
                    const top = p[1] * ratio.current.y
                    
                    return(
                    <div
                      key={item.key}
                      id={item.key}
                      className={`item-image ${item.isExam ? '--example-div' : ''}`}
                      data-index={item.indexAnswer}
                      style={{
                        position:'absolute',
                        height:`${height}px`,
                        width:`${width}px`,
                        left:`${left}px`,
                        top:`${top}px`,
                        transform:'scale(1,1)',
                        pointerEvents:'none'
                      }}
                    >
                      <img 
                        src={item.image}
                        alt=''
                        className='item-image-content'
                      />
                    </div>
                  )})}
                </div>
            </div>
          </div>
        </>
      </div>
    </GameWrapper>
  )
}
