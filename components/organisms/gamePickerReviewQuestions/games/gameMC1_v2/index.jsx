import { GameWrapper } from 'practiceTest/components/templates/gameWrapper'

import { convertStrToHtml, formatHtmlText } from 'utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'

export const GameMC1_v2 = ({ data }) => {
  const instruction = data?.instruction || ''
  const questionsGroup = data?.questionsGroup || []
  const exampleLists = []
  const questionLists = []
  for (let i = 0; i < questionsGroup.length; i++) {
    const item = questionsGroup[i]
    if (item.isExample) {
      exampleLists.push(item)
    } else {
      questionLists.push(item)
    }
  }
  return (
    <GameWrapper className="pt-o-game-mc1-v2">
      <div className="pt-o-game-mc1-v2__right">
        <div className="__header">
          <CustomHeading tag="h6" className="__description">
            <FormatText tag="span">{instruction}</FormatText>
          </CustomHeading>
        </div>
        <div className="__content">
          <>
            <>
              {exampleLists.length > 0 && (
                <div className="__content__group_examples">
                  <div className="title_example">
                    <span>Example</span>
                  </div>
                  {exampleLists.map((item, i) => {
                    const html = convertStrToHtml(item.question).replaceAll(
                      '%s%',
                      `<div class="__space"></div>`,
                    )
                    return (
                      <div key={`ex_${i}`} className="__radio-group">
                        <CustomHeading tag="h6" className="__radio-heading">
                          <span
                            dangerouslySetInnerHTML={{
                              __html: html,
                            }}
                          ></span>
                        </CustomHeading>
                        <div className="__radio-list" style={{
                          paddingLeft:'2rem'
                        }}>
                          {item.answers.map((childItem, j) => (
                            <div
                              key={`ex-${i}_${j}`}
                              className={`mc1-a-multichoiceanswers answer`}
                            >
                              <div className="mc1-a-multichoiceanswers__answer">
                                <ul className="mc1-a-multichoiceanswers__input ">
                                  <li>
                                    <input
                                      type="checkbox"
                                      name={`mc1-id${i}`}
                                      className="option-input checkbox"
                                      defaultChecked={childItem.isCorrect}
                                      style={{ pointerEvents: 'none' }}
                                    />
                                    <label
                                      dangerouslySetInnerHTML={{
                                        __html: convertStrToHtml(
                                          childItem.text,
                                        ),
                                      }}
                                    ></label>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
            </>
            <div className="__content__group_questions">
              <div className="title_question">
                <span>
                  {questionLists.length > 1 ? 'Questions' : 'Question'}
                </span>
              </div>
              {questionLists.map((item, i) => {
                const html = convertStrToHtml(item.question).replaceAll(
                  '%s%',
                  `<div class="__space"></div>`,
                )
                return (
                  <div key={i} className="__radio-group">
                    <CustomHeading tag="h6" className="__radio-heading">
                      <span
                        dangerouslySetInnerHTML={{
                          __html: html,
                        }}
                      ></span>
                    </CustomHeading>
                    <div className="__radio-list">
                      {item.answers.map((childItem, j) => (
                        <div
                          key={`${i}_${j}`}
                          className={`__radio-item`}
                          style={{
                            pointerEvents: 'none',
                          }}
                        >
                          <p
                            className="pt-a-text"
                            dangerouslySetInnerHTML={{
                              __html: convertStrToHtml(childItem.text),
                            }}
                          ></p>
                        </div>
                      ))}
                    </div>
                  </div>
                )
              })}
            </div>
          </>
        </div>
      </div>
    </GameWrapper>
  )
}
