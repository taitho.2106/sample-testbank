import { useContext, useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'
import { Icon, Toggle } from 'rsuite'

import { CustomButton } from 'practiceTest/components/atoms/button'
import { CustomHeading } from 'practiceTest/components/atoms/heading'
import { CustomImage } from 'practiceTest/components/atoms/image'
import { CustomText } from 'practiceTest/components/atoms/text'
import { AudioPlayer } from 'practiceTest/components/molecules/audioPlayer'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from 'practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'
import { BlockWave } from 'practiceTest/components/organisms/blockWave'
import { formatDateTime } from 'practiceTest/utils/functions'
import { convertStrToHtml } from "../../../../../utils/string";

export const Game8Audio = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)

  const [action, setAction] = useState( Array.from(Array(answers.length), () => false))

  const audio = useRef(null)


  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [audio, isPlaying])

  return (
    <div className="pt-o-game-8-audio" style={{width:'100%', height:'100%'}}>
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-8-audio__container">
        {isShowTapescript ? (
          <div className="__paper">
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
        ) : (
          <>
            <BlockWave className={`pt-o-game-8-audio__left`}>
              <AudioPlayer
                className="__player"
                isPlaying={isPlaying}
                setIsPlaying={() => setIsPLaying(!isPlaying)}
              />
              <span className="__duration">{countDown}</span>
            </BlockWave>
            <div className="pt-o-game-8-audio__right">
              {(
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                  style={{marginBottom:'3rem'}}
                >
                  Tapescript
                </CustomButton>
              )}
              <BlockBottomGradient className="__container">
                <div className="__header">
                  <CustomHeading tag="h6" className="__description">
                    <FormatText tag="span">{instruction}</FormatText>
                  </CustomHeading>
                </div>
                <div className="__content">
                  <div className="__list">
                    <Scrollbars universal={true}>
                      {answers.map((item, i) => (
                        <RadioItem
                          key={i}
                          content={item.text}
                          index={i}
                          state={action}
                        />
                      ))}
                    </Scrollbars>
                  </div>
                </div>
              </BlockBottomGradient>
            </div>
          </>
        )}
      </div>
    </div>
  )
}

const RadioItem = ({ content, index, trueAnswer, state, setState }) => {

  return (
    <div className="__item">
      <div className={`__text`}>
        <p dangerouslySetInnerHTML={{__html: convertStrToHtml(content)}}></p>
      </div>
      <div
        className="__toggle"
        style={{
          pointerEvents: 'none',
        }}
      >
        <Toggle
          className="__switcher"
          checked={state[index]}
          checkedChildren="True"
          unCheckedChildren="False"
        />
      </div>
    </div>
  )
}
