import { useContext, useEffect, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'
import { Icon, Toggle } from 'rsuite'

import { CustomHeading } from 'practiceTest/components/atoms/heading'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from 'practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'
import { convertStrToHtml } from "../../../../../utils/string";


export const Game8 = ({ data }) => {
  const answers = data?.answers || []
  const questionInstruction = data?.questionInstruction || ''
  const instruction = data?.instruction || ''
  const [action, setAction] = useState(Array.from(Array(answers.length), () => false))
  
  return (
    <div className="pt-o-game-8" style={{width:'100%', height:'100%'}}>
      <div className="pt-o-game-8__left">
        <BlockPaper>
          <div className="__content">
            <FormatText tag="p">{convertStrToHtml(questionInstruction)}</FormatText>
          </div>
        </BlockPaper>
      </div>
      <div className="pt-o-game-8__right">
        <BlockBottomGradient className="__container">
          <div className="__header">
            <CustomHeading tag="h6" className="__description">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          </div>
          <div className="__content">
            <div className="__list">
              <Scrollbars universal={true}>
                {answers.map((item, i) => (
                  <RadioItem
                    key={i}
                    content={convertStrToHtml(item.text)}
                    index={i}
                    state={action}
                  />
                ))}
              </Scrollbars>
            </div>
          </div>
        </BlockBottomGradient>
      </div>
    </div>
  )
}

const RadioItem = ({ content, index, trueAnswer, state, setState }) => {
  
  return (
    <div className="__item">
      <div className={`__text`}>
          <p dangerouslySetInnerHTML={{__html: convertStrToHtml(content)}}></p>
      </div>
      <div
        className="__toggle"
        style={{
          pointerEvents:'none',
        }}
      >
        <Toggle
          className="__switcher"
          checked={state[index]}
          checkedChildren="True"
          unCheckedChildren="False"
        />
      </div>
    </div>
  )
}
