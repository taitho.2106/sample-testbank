import { useRef, useState } from 'react'

import { convertStrToHtml } from '@/utils/string'

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { CustomText } from '../../../../../practiceTest/components/atoms/text'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { formatDateTime } from '../../../../../practiceTest/utils/functions'

export const Game15Audio = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const choices = answers[0].answerList.split('/')

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)

  const defaultRadio = Array.from(Array(answers.length), () => null)
  const [action, setAction] = useState(defaultRadio)

  const audio = useRef(null)

  return (
    <div className="pt-o-game-15 --audio" style={{width:'100%', height:'100%'}}>
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-15__container">
        {isShowTapescript ? (
          <div className="__paper">
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
        ) : (
          <>
            <BlockWave className={`pt-o-game-15__left`}>
              <AudioPlayer
                className="__player"
                isPlaying={isPlaying}
                setIsPlaying={() => setIsPLaying(!isPlaying)}
              />
              <span className="__duration">{countDown}</span>
            </BlockWave>
            <div className="pt-o-game-15__right">
              {(
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                  style={{marginBottom:'3rem'}}
                >
                  Tapescript
                </CustomButton>
              )}
              <BlockBottomGradient className="__container">
                <div className="__header">
                  <CustomHeading tag="h6" className="__description">
                    <FormatText tag="span">{instruction}</FormatText>
                  </CustomHeading>
                </div>
                <div className="__content">
                  <div className="__title">
                    {choices.map((item, i) => (
                      <CustomHeading key={i} className="__heading">
                        <span>{item}</span>
                      </CustomHeading>
                    ))}
                  </div>
                  <div className="__list">
                    {/* <Scrollbars universal={true}> */}
                      {answers.map((item, i) => (
                        <RadioItem
                          key={i}
                          choices={choices}
                          data={item.text}
                          index={i}
                          state={action}
                        />
                      ))}
                    {/* </Scrollbars> */}
                  </div>
                </div>
              </BlockBottomGradient>
            </div>
          </>
        )}
      </div>
    </div>
  )
}

const RadioItem = ({ choices, data, index, trueValue, state, setState }) => {  
  return (
    <div className="__item">
      <div className="__text">
        <CustomText tag="p">
          <span dangerouslySetInnerHTML={{ __html: convertStrToHtml(data) }}>
          </span>
        </CustomText>
      </div>
      <div
        className={`__radio ${
          state[index] === choices[0] ? '--true' : ''
        }`}
        style={{ pointerEvents: 'none' }}
      ></div>
      <div
        className={`__radio ${
          state[index] === choices[1] ? '--false' : ''
        }`}
        style={{ pointerEvents: 'none' }}
      ></div>
      <div
        className={`__radio ${
          state[index] === choices[2] ? '--not-given' : ''
        }`}
        style={{ pointerEvents: 'none' }}
      ></div>
    </div>
  )
}
