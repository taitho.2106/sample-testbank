import { useState } from 'react'

import { convertStrToHtml } from '@/utils/string'

import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { ImageZooming } from '../../../../../practiceTest/components/molecules/modals/imageZooming'
import { BlockBottomGradient } from '../../../../../practiceTest/components/organisms/blockBottomGradient'
import { BlockCloud } from '../../../../../practiceTest/components/organisms/blockCloud'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'

export const Game15 = ({ data }) => {
  const answers = data?.answers || []
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const questionInstruction = data?.questionInstruction || ''

  const choices = answers[0].answerList.split('/')

  const [isZoom, setIsZoom] = useState(false)
  const defaultRadio = Array.from(Array(answers.length), () => null)
  const [action, setAction] = useState(defaultRadio)

  return (
    <div className="pt-o-game-15" style={{width:'100%', height:'100%'}}>
      {imageInstruction ? (
        <BlockCloud className="pt-o-game-15__left">
          <CustomImage
            className="__image"
            alt="instruction"
            yRate={0}
            src={`${imageInstruction}`}
            onClick={() => setIsZoom(true)}
          />
          <ImageZooming
            data={{ alt: 'instruction', src: `${imageInstruction}` }}
            status={{
              state: isZoom,
              setState: setIsZoom,
            }}
          />
        </BlockCloud>
      ) : (
        <div className="pt-o-game-15__left">
          <BlockPaper>
            <div className="__content">
              <FormatText tag="p">{convertStrToHtml(questionInstruction)}</FormatText>
            </div>
          </BlockPaper>
        </div>
      )}
      <div className="pt-o-game-15__right">
        <BlockBottomGradient className="__container">
          <div className="__header">
            <CustomHeading tag="h6" className="__description">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          </div>
          <div className="__content">
            <div className="__title">
              {choices.map((item, i) => (
                <CustomHeading key={i} className="__heading">
                  <span>{item}</span>
                </CustomHeading>
              ))}
            </div>
            <div className="__list">
              {/* <Scrollbars universal={true}> */}
                {answers.map((item, i) => (
                  <RadioItem
                    key={i}
                    choices={choices}
                    data={item.text}
                    index={i}
                    state={action}
                  />
                ))}
              {/* </Scrollbars> */}
            </div>
          </div>
        </BlockBottomGradient>
      </div>
    </div>
  )
}

const RadioItem = ({ choices, data, index, trueValue, state, setState }) => {
  return (
    <div className="__item">
      <div className="__text">
        <FormatText tag="p">{convertStrToHtml(data)}</FormatText>
      </div>
      <div
        className={`__radio ${
          state[index] === choices[0] ? '--true' : ''
        }`}
        style={{ pointerEvents:'none' }}
      ></div>
      <div
        className={`__radio ${
          state[index] === choices[1] ? '--false' : ''
        }`}
        style={{ pointerEvents:'none' }}
      ></div>
      <div
        className={`__radio ${
          state[index] === choices[2] ? '--not-given' : ''
        }`}
        style={{ pointerEvents: 'none' }}
      ></div>
    </div>
  )
}
