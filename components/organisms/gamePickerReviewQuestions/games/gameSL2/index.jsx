import { Fragment, useEffect, useRef, useState } from 'react'

import Dropdown from 'rsuite/Dropdown';

import { convertStrToHtml } from '@/utils/string'
import { ImageZooming } from 'practiceTest/components/molecules/modals/imageZooming';

import { CustomButton } from '../../../../../practiceTest/components/atoms/button'
import { CustomHeading } from '../../../../../practiceTest/components/atoms/heading'
import { CustomImage } from '../../../../../practiceTest/components/atoms/image'
import { AudioPlayer } from '../../../../../practiceTest/components/molecules/audioPlayer'
import { FormatText } from '../../../../../practiceTest/components/molecules/formatText'
import { BlockBottomGradientWithHeader } from '../../../../../practiceTest/components/organisms/blockBottomGradient/BlockBottomGradientWithHeader'
import { BlockPaper } from '../../../../../practiceTest/components/organisms/blockPaper'
import { BlockWave } from '../../../../../practiceTest/components/organisms/blockWave'
import { TEST_MODE } from '../../../../../practiceTest/interfaces/constants'
import { formatDateTime } from '../../../../../practiceTest/utils/functions'

export const GameSL2 = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const question = data?.question || ''

  // const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
  // const questionArr = replaceQuestion.split('%s%')

  const pickers = answers.map((item) => item.answerList.split('/'))

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)
  const [isOpenCollapse, setIsOpenCollapse] = useState(false)
  // const [topDrawer,setTopDrawer]=useState(0)
  const [isZoom, setIsZoom] = useState(false)
  const [zoomUrl, setZoomUrl] = useState('');

  const defaultArray =  Array.from(Array(pickers.length), () => null)
  const [chosenAnswers, setChosenAnswers] = useState(defaultArray)

  const drawer = useRef(null)
  const audio = useRef(null)
  const contentRef = useRef(null)

  useEffect(() => {
    
    window.addEventListener('click', closeCollapse)
    return () => window.removeEventListener('click', closeCollapse)
  }, [])

  useEffect(() => {
    if (drawer?.current && mode.state === TEST_MODE.play)
      drawer.current.scrollTo({ top: 0, behaviour: 'smooth' })
  }, [isOpenCollapse, drawer.current])

  
  const closeCollapse = (e) => {
    if (
      e.target.classList.contains('__space') ||
      e.target.classList.contains('__drawer') ||
      e.target.closest('.__drawer')
    )
      return
    setIsOpenCollapse(false)
    //setTopDrawer(0)
  }

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }
 
  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  function renderDropdown(j){
    return (
      <Dropdown key={j} 
        title={
          chosenAnswers[j] !== null && pickers[j][chosenAnswers[j]]
        }
        placement="bottomStart"
        className={`${chosenAnswers[j] !== null && pickers[j][chosenAnswers[j]] ? '--inline' : ''}`}
      >
        {pickers[j].map((item, index) => 
          <Dropdown.Item key={j + index} eventKey={index}>{item}</Dropdown.Item>,
        )}
      </Dropdown>
    )
  }

  const renderQuestion = (question, j) => {
    // const replaceQuestion = question.replace(/%[0-9]%/g, '%s%')
    const finalSentences = question.split('%s%');
    return finalSentences.map((item, index) => {
      return (
        <Fragment key={j + index + ''}>
            {index !== 0 && renderDropdown(j)}
            <FormatText tag="span">{convertStrToHtml(item) + " "}</FormatText>
        </Fragment>
      )
    })

  }

  return (
    <div className="pt-o-game-sl2-audio" style={{width:'100%', height:'100%'}}>
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-sl2-audio__container">
        {isShowTapescript ? (
          <div className="__paper">
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
        ) : (
          <>
            <BlockWave
              className={`pt-o-game-sl2-audio__left`}
            >
              <AudioPlayer
                className="__player"
                isPlaying={isPlaying}
                setIsPlaying={() => setIsPLaying(!isPlaying)}
              />
              <span className="__duration">{countDown}</span>
            </BlockWave>
            <div
              className={`pt-o-game-sl2-audio__right ${imageInstruction ? '--image' : ''
                }`}
            >
              {(
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                  style={{marginBottom:'3rem'}}
                >
                  Tapescript
                </CustomButton>
              )}
              <BlockBottomGradientWithHeader
                headerChildren={
                  <CustomHeading tag="h6" className="__heading">
                    <FormatText tag="span">{instruction}</FormatText>
                  </CustomHeading>
                }
              >
                {/* <Scrollbars universal={true}> */}
                  <div ref={contentRef} className="__content">
                  { zoomUrl.length > 0 &&
                      <ImageZooming
                        data={{
                          alt: 'Image Instruction',
                          src: `${zoomUrl}`,
                        }}
                        status={{
                          state: isZoom,
                          setState: setIsZoom,
                        }}
                      />
                    }
                    {answers.map((item, j) => {
                      return (
                        <Fragment key={j}>
                          <div
                            className="__item"
                          >
                            <CustomImage
                              alt=""
                              src={item?.imageInstruction}
                              yRate={0}
                              onClick={() => {
                                setIsZoom(true)
                                setZoomUrl(item?.imageInstruction)
                              }}
                            />
                            <div className="select">
                              {renderQuestion(item?.question, j)}
                            </div>
                          </div>
                        </Fragment>
                      )
                    })}
                  </div>
                {/* </Scrollbars> */}
              </BlockBottomGradientWithHeader>
            </div>
          </>
        )}
      </div>
    </div>
  )
}
