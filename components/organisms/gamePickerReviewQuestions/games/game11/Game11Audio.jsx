import { Fragment } from 'react'
import { useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { CustomButton } from 'practiceTest/components/atoms/button'
import { CustomHeading } from 'practiceTest/components/atoms/heading'
import { CustomImage } from 'practiceTest/components/atoms/image'
import { CustomText } from 'practiceTest/components/atoms/text'
import { AudioPlayer } from 'practiceTest/components/molecules/audioPlayer'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from 'practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'
import { BlockWave } from 'practiceTest/components/organisms/blockWave'
import { convertStrToHtml } from 'utils/string'

import { formatDateTime } from '../../../../../practiceTest/utils/functions'

export const Game11Audio = ({ data }) => {
  const instruction = data?.instruction || ''
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const questionsGroup = data?.questionsGroup || []
  const examplesList = []
  const questionList = []
  for (let i = 0; i < questionsGroup.length; i++) {
    const item = questionsGroup[i]
    if (item.isExample) {
      examplesList.push(item)
    } else {
      questionList.push(item)
    }
  }
  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)
  const audio = useRef(null)

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDownValue = audio.current.duration - audio.current.currentTime
    if (countDownValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  return (
    <div
      className="pt-o-game-11-audio"
      style={{ width: '100%', height: '100%' }}
    >
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      {isShowTapescript ? (
        <div className="__paper">
          <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
            <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
          </div>
          <BlockPaper>
            <div className="__content">
              <FormatText>{audioScript}</FormatText>
            </div>
          </BlockPaper>
        </div>
      ) : (
        <>
          <BlockWave className={`pt-o-game-11-audio__left`}>
            <AudioPlayer
              className="__player"
              isPlaying={isPlaying}
              setIsPlaying={() => setIsPLaying(!isPlaying)}
            />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <BlockBottomGradient className="pt-o-game-11-audio__right">
            {
              <CustomButton
                className="__tapescript"
                onClick={() => setIsShowTapescript(true)}
              >
                Tapescript
              </CustomButton>
            }
            <div className="__header">
              <CustomHeading tag="h6" className="__description">
                <FormatText tag="span">{instruction}</FormatText>
              </CustomHeading>
            </div>
            <div className="__content">
            <>
                {examplesList.length > 0 && (
                  <div className='__content_question_group --example'>
                    <div className='question_header'>
                      <span>Example</span>
                    </div>
                    <>
                      {examplesList.map((item, i) => (
                        <div key={i} className="__radio-group">
                          <CustomHeading tag="h6" className="__radio-heading">
                          <span
                              dangerouslySetInnerHTML={{
                                __html: convertStrToHtml(
                                    item.question,
                                ).replaceAll(
                                    '%s%',
                                    `<div class="__space"></div>`,
                                ),
                              }}
                          ></span>
                          </CustomHeading>
                          <div className="__radio-list">
                            {item.answers.map((childItem, j) => (
                              <div
                              key={`question-ans-${i}`}
                              className={`mc56-a-multichoiceanswers answer`}
                            >
                              <div className="mc56-a-multichoiceanswers__answer">
                                <ul className="mc56-a-multichoiceanswers__input ">
                                  <li>
                                    <input
                                      type="checkbox"
                                      name={`mc56-id${i}`}
                                      className="option-input checkbox"
                                      defaultChecked={childItem.isCorrect}
                                      style={{ pointerEvents: 'none' }}
                                    />
                                    <label
                                      dangerouslySetInnerHTML={{
                                        __html: convertStrToHtml(
                                          childItem.text,
                                        ),
                                      }}
                                    ></label>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            ))}
                          </div>
                        </div>
                      ))}
                    </>
                  </div>
                )}
                {questionList.length > 0 && (
                  <div className='__content_question_group'>
                    <div className='question_header'>
                      <span>
                        {questionList.length > 1 ? 'Questions' : 'Question'}
                      </span>
                    </div>
                    <div>
                      {questionList.map((item, i) => (
                        <div key={i} className="__radio-group">
                          <CustomHeading tag="h6" className="__radio-heading">
                          <span
                              dangerouslySetInnerHTML={{
                                __html: convertStrToHtml(
                                    item.question,
                                ).replaceAll(
                                    '%s%',
                                    `<div class="__space"></div>`,
                                ),
                              }}
                          ></span>
                          </CustomHeading>
                          <div className="__radio-list">
                            {item.answers.map((childItem, j) => (
                              <div
                                key={j}
                                className={`__radio-item`}
                                style={{
                                  pointerEvents: 'none',
                                }}
                              >
                                <label
                                    dangerouslySetInnerHTML={{
                                      __html: convertStrToHtml(
                                          childItem.text,
                                      ),
                                    }}
                                ></label>
                              </div>
                            ))}
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                )}
              </>
            </div>
          </BlockBottomGradient>
        </>
      )}
    </div>
  )
}
