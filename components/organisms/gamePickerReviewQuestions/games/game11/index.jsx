import Scrollbars from 'react-custom-scrollbars'

import { CustomHeading } from 'practiceTest/components/atoms/heading'
import { FormatText } from 'practiceTest/components/molecules/formatText'
import { BlockBottomGradient } from 'practiceTest/components/organisms/blockBottomGradient'
import { BlockPaper } from 'practiceTest/components/organisms/blockPaper'
import {convertStrToHtml, formatHtmlText} from 'utils/string'




export const Game11 = ({ data }) => {
  const instruction = data?.instruction || ''
  const questionInstruction = data?.questionInstruction || ''
  const questionsGroup = data?.questionsGroup || []
  const examplesList = []
  const questionList = []
  for (let i = 0; i < questionsGroup.length; i++) {
    const item = questionsGroup[i]
    if (item.isExample) {
      examplesList.push(item)
    } else {
      questionList.push(item)
    }
  }
  return (
    <div className="pt-o-game-11" style={{width:'100%', height:'100%'}}>
      <div className="pt-o-game-11__left">
        <BlockPaper>
          <div className="__content">
            <FormatText>{convertStrToHtml(questionInstruction)}</FormatText>
          </div>
        </BlockPaper>
      </div>
      <BlockBottomGradient className="pt-o-game-11__right">
        <div className="__header">
          <CustomHeading tag="h6" className="__description">
            <FormatText tag="span">{instruction}</FormatText>
          </CustomHeading>
        </div>
        <div className="__content">
        <>
            {examplesList.length > 0 && (
              <div className='__content_question_group --example'>
                <div className='question_header'>
                  <span>Example</span>
                </div>
                <>
                  {examplesList.map((item, i) => {
                    return (
                      <div key={i} className="__radio-group">
                        <CustomHeading tag="h6" className="__radio-heading">
                          <span
                            dangerouslySetInnerHTML={{
                              __html: convertStrToHtml(
                                item.question,
                              ).replaceAll(
                                '%s%',
                                `<div class="__space"></div>`,
                              ),
                            }}
                          ></span>
                        </CustomHeading>
                        <div className="__radio-list">
                          {item.answers.map((childItem, j) => (
                            <div
                              key={`question-ans-${i}`}
                              className={`mc56-a-multichoiceanswers answer`}
                            >
                              <div className="mc56-a-multichoiceanswers__answer">
                                <ul className="mc56-a-multichoiceanswers__input ">
                                  <li>
                                    <input
                                      type="checkbox"
                                      name={`mc56-id${i}`}
                                      className="option-input checkbox"
                                      defaultChecked={childItem.isCorrect}
                                      style={{ pointerEvents: 'none' }}
                                    />
                                    <label
                                      dangerouslySetInnerHTML={{
                                        __html: convertStrToHtml(
                                          childItem.text,
                                        ),
                                      }}
                                    ></label>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    )
                  })}
                </>
              </div>
            )}
            {questionList.length > 0 && (
              <div className='__content_question_group'>
                <div className='question_header'>
                  <span>
                    {questionList.length > 1 ? 'Questions' : 'Question'}
                  </span>
                </div>
                <div>
                  {questionList.map((item, i) => {
                    return (
                      <div key={i} className="__radio-group">
                        <CustomHeading tag="h6" className="__radio-heading">
                          <span
                            dangerouslySetInnerHTML={{
                              __html: convertStrToHtml(
                                item.question,
                              ).replaceAll(
                                '%s%',
                                `<div class="__space"></div>`,
                              ),
                            }}
                          ></span>
                        </CustomHeading>
                        <div className="__radio-list">
                          {item.answers.map((childItem, j) => (
                            <div
                              key={j}
                              className={`__radio-item`}
                              style={{
                                pointerEvents: 'none',
                              }}
                            >
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: convertStrToHtml(childItem.text),
                                }}
                              ></span>
                            </div>
                          ))}
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}
          </>
        </div>
      </BlockBottomGradient>
    </div>
  )
}
