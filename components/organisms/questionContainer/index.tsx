import { KeyboardEvent, useCallback, useContext, useEffect, useMemo, useRef, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import { Collapse } from 'react-collapse'
import { useForm } from 'react-hook-form'
import readXlsxFile from 'read-excel-file'
import { Button, toaster, Message, Whisper, Tooltip } from 'rsuite'

import { paths } from 'api/paths'
import { DateInput } from 'components/atoms/dateInput'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { MultiSelectPicker } from 'components/atoms/selectPicker/multiSelectPicker'
import { QuestionTable } from 'components/molecules/questionTable'
import useGrade from 'hooks/useGrade'
import useSeries from 'hooks/useSeries'
import { USER_ROLES } from 'interfaces/constants'
import { QuestionContext, WrapperContext } from 'interfaces/contexts'
import { LEVEL_SELECTIONS, PUBLISHER_SELECTIONS, SKILLS_SELECTIONS, TASK_SELECTIONS } from 'interfaces/struct'
import { QuestionContextType, QuestionSearchType } from 'interfaces/types'
import { userInRight } from 'utils'
import { useMatchMutate } from 'utils/swr'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import { filterSeriesByGrade } from 'utils/series/filterSeriesByGrade'
import useTranslation from "@/hooks/useTranslation";
import { arrayToTranslateWithCode } from '@/utils/array'

export const QuestionContainer = () => {
  const { t } = useTranslation()
  const router = useRouter()
  const { getSeries } = useSeries()
  const { getGrade } = useGrade()
  const [listSeries, setListSeries] = useState([])
  const [session] = useSession()
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const isAuthPage = router.query.m === 'mine'
  const isSystemPage = !router.query.m
  const isCreatable = isAuthPage || (isSystem && isSystemPage)
  const user: any = session?.user

  const { search, setSearch } = useContext<QuestionContextType>(QuestionContext)
  const fileImport = useRef(null)
  const fileUpload = useRef(null)
  const formSearch = useRef(null)
  const matchMutate = useMatchMutate()
  const { register, handleSubmit, setValue } = useForm()

  const { globalModal } = useContext(WrapperContext)

  const [isOpenFilterCollapse, setIsOpenFilterCollapse] = useState(true)
  const [triggerReset, setTriggerReset] = useState(false)
  const [filterBagde, setFilterBagde] = useState(0)
  const filterData = useRef<any>({})
  const [isOnceUpload, setIsOnceUpload] = useState(false)
  const [searching, setSearching] = useState(false)
  useEffect(() => {
    register('publisher')
    register('series')
    register('grade')
    register('skills')
    register('level')
    register('question_type')
    register('question_text')
    register('created_date')
  }, [])
  useEffect(() => {
    const dataSeries = filterSeriesByGrade(getSeries,getGrade,null)
    setListSeries(dataSeries)
  }, [getSeries])

  useEffect(() => {
    onResetForm();
  }, [user?.id, router.asPath])
  const onSubmit = async (formData: any) => {
    setSearch({ ...search, ...formData, page_index: 0 })
  }

  const onChange = (name: string, value: any) => {
    setValue(name, value)
    filterData.current[name] = value
    if (name == 'grade') {
      const list = filterSeriesByGrade(getSeries,getGrade,value)
          setListSeries([...list])
    }
    setFilterBagde(
      Object.keys(filterData.current).filter(
        (m) => filterData.current[m]?.length > 0 || filterData.current[m]?.toString(),
      ).length,
    )
  }

  const onResetForm = () => {
    setValue('question_text', '')
    setValue('publisher', [])
    setValue('series', [])
    setValue('grade', [])
    setValue('skills', [])
    setValue('level', [])
    setValue('question_type', [])
    setValue('created_date', '')
    filterData.current = {}
    setFilterBagde(0)
    setTriggerReset(!triggerReset)
    setTimeout(() => {
      handleSubmit(onSubmit)(formSearch.current)
    }, 0)
  }


  const handlerKeyDown = useCallback((event:KeyboardEvent<HTMLInputElement>)=>{
    const keyPrevent = ['*','#']
    if(event.shiftKey && keyPrevent.some(m => event.key.toLowerCase() === m)){
      event.preventDefault()
    }
  },[])
  // console.log('filterData------------------', filterData)
  return (
    <div style={{ margin: '1.4rem 0 0 0' }}>
      <form ref={formSearch} onSubmit={handleSubmit(onSubmit)} style={{ display: 'flex', flexDirection: 'column' }}>
        <div className="m-unittest-template-table-data__content">
          <Collapse isOpened={isOpenFilterCollapse}>
            <div className="content__sub">
              <InputWithLabel
                className="__filter-box --search --xl"
                label={t('question-page')['search-question-input']}
                placeholder={t('common')['placeholder-search-question']}
                triggerReset={triggerReset}
                type="text"
                //onBlur={handleNameChange}
                icon={<img src="/images/icons/ic-search-dark.png" alt="search" />}
                onChange={onChange.bind(this, 'question_text')}
                onkeyDown={handlerKeyDown}
              />
              {getGrade && (
                <SelectFilterPicker
                  data={getGrade}
                  isMultiChoice={false}
                  inputSearchShow={false}
                  onChange={onChange.bind(this, 'grade')}
                  className={'__filter-box --grade'}
                  triggerReset={triggerReset}
                  label={t('common')['grade']}
                  menuSize='lg'
                ></SelectFilterPicker>
              )}
              {listSeries && isSystemPage && (
                <SelectFilterPicker
                  data={listSeries}
                  isMultiChoice={false}
                  inputSearchShow={true}
                  onChange={onChange.bind(this, 'series')}
                  className={'__filter-box --series'}
                  triggerReset={triggerReset}
                  label={t('common')['series']}
                  menuSize='xxl'
                ></SelectFilterPicker>
              )}
              <MultiSelectPicker
                label={t('common')['skill']}
                data={SKILLS_SELECTIONS}
                className={'__filter-box --skill --sm'}
                onChange={onChange.bind(this, 'skills')}
                style={{ zIndex: 6 }}
                triggerReset={triggerReset}
                menuSize='lg'
              />
              <MultiSelectPicker
                label={t('common')['question-type']}
                data={TASK_SELECTIONS}
                className={'__filter-box'}
                onChange={onChange.bind(this, 'question_type')}
                style={{ zIndex: 5 }}
                menuSize='xl'
                triggerReset={triggerReset}
              />
              <MultiSelectPicker
                label={t('common')['level']}
                data={arrayToTranslateWithCode(LEVEL_SELECTIONS, t)}
                className={'__filter-box'}
                onChange={onChange.bind(this, 'level')}
                style={{ zIndex: 4 }}
                menuSize='lg'
                triggerReset={triggerReset}
              />
              {/* <MultiSelectPicker
                className="__filter-box"
                data={PUBLISHER_SELECTIONS}
                label="NXB"
                triggerReset={triggerReset}
                onChange={onChange.bind(this, 'publisher')}
                style={{ zIndex: 3 }}
              /> */}
              <DateInput
                className="__filter-box"
                defaultValue={null}
                label={t('question-page')['created-date']}
                placement="bottomEnd"
                time={false}
                style={{ zIndex: 3 }}
                onChange={onChange.bind(this, 'created_date')}
                triggerReset={triggerReset}
              />
            </div>
          </Collapse>
          <div className="content__sup">
            <div className="__action-group" style={{ display: 'flex', marginTop: '1rem' }}></div>
            <div className="__input-group">
              {isSystemPage && isSystem && (
                <Button
                  appearance={'primary'}
                  className="__action-btn gray"
                  onClick={() =>
                    globalModal.setState({
                      id: 'btn-uploadfile',
                    })
                  }
                >
                  <img src="/images/icons/ic-upload-purple.png" alt="Import file" />
                  <span>{t('common')['upload']}</span>
                </Button>
              )}
              {isCreatable && (
                <>
                  {/* thêm Button upload đối với người dùng là giáo viên  */}
                  {/*{user?.is_admin !== 1 && !isSystem && (*/}
                  {/*  <Button*/}
                  {/*    appearance={'primary'}*/}
                  {/*    className="__action-btn gray"*/}
                  {/*    onClick={() =>*/}
                  {/*      globalModal.setState({*/}
                  {/*        id: 'btn-uploadfile',*/}
                  {/*      })*/}
                  {/*    }*/}
                  {/*  >*/}
                  {/*    <img src="/images/icons/ic-upload-purple.png" alt="Import file" />*/}
                  {/*    <span>Tải lên</span>*/}
                  {/*  </Button>*/}
                  {/*)}*/}
                  <Button
                    style={{ marginRight: '0.8rem' }}
                    className="__action-btn"
                    onClick={() => {
                      router.push({
                        pathname: '/questions/[questionSlug]',
                        query: { ...router.query, questionSlug: -1 },
                      })
                    }}
                  >
                    <img src="/images/icons/ic-plus-white.png" alt="create" />
                    <span>{t('common')['create']}</span>
                  </Button>
                </>
              )}
              <Whisper placement="bottom" trigger="hover" speaker={<Tooltip>{t('unit-test')['tooltip-reset']}</Tooltip>}>
                <Button className="__sub-btn" onClick={() => {
                  onResetForm()
                  setSearching(false)
                }}>
                  <img src="/images/icons/ic-reset-darker.png" alt="reset" />
                  <span>{t('common')['reset']}</span>
                </Button>
              </Whisper>
              <Button className="__main-btn" type="submit" onClick={() => setSearching(true)}>
                <img src="/images/icons/ic-search-dark.png" alt="find" />
                <span>{t('common')['search']}</span>
              </Button>
            </div>
          </div>
        </div>
      </form>
      <QuestionTable searching={searching} />
    </div>
  )
}

const schema = {
  Publisher: {
    prop: 'publisher',
    type: String,
  },
  'Exam Type': {
    prop: 'test_type',
    type: String,
  },
  Series: {
    prop: 'series',
    type: String,
  },
  Grade: {
    prop: 'grade',
    type: String,
  },
  Format: {
    prop: 'format',
    type: String,
  },
  Types: {
    prop: 'types',
    type: String,
  },
  Skills: {
    prop: 'skills',
    type: String,
  },
  'Question Type': {
    prop: 'question_type',
    type: String,
  },
  Level: {
    prop: 'level',
    type: String,
  },
  Group: {
    prop: 'group',
    type: String,
  },
  'Parent-Question Description': {
    prop: 'parent_question_description',
    type: String,
  },
  'Parent-Question Text': {
    prop: 'parent_question_text',
    type: String,
  },
  'Question Description': {
    prop: 'question_description',
    type: String,
  },
  'Question Text': {
    prop: 'question_text',
    type: String,
  },
  Image: {
    prop: 'image',
    type: String,
  },
  Video: {
    prop: 'video',
    type: String,
  },
  Audio: {
    prop: 'audio',
    type: String,
  },
  Answers: {
    prop: 'answers',
    type: (value: any) => {
      return value.toString()
    },
  },
  'Correct Answers': {
    prop: 'correct_answers',
    type: String,
  },
  Points: {
    prop: 'points',
    type: Number,
  },
  CEFR: {
    prop: 'cerf',
    type: String,
  },
  'Audio script': {
    prop: 'audio_script',
    type: String,
  },
}
