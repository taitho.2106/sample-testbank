import React, { useContext, useState, useEffect } from 'react'

import * as Excel from 'exceljs'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { Modal, Button, Tooltip, Whisper } from 'rsuite'

import { USER_ROLES } from '@/interfaces/constants'
import { SKILLS_SELECTIONS, TASK_SELECTIONS } from '@/interfaces/struct'
import {
  cellArrayObject,
  cellArrayObjectFail,
} from '@/questions_middleware/cellDefine'
import getDisplay from '@/questions_middleware/getDisplay'
import { QuestionContext, WrapperContext } from 'interfaces/contexts'
import { useQuestions } from 'lib/swr-hook'
import { userInRight } from 'utils'

import {
  DefaultPropsType,
  QuestionContextType,
} from '../../../../interfaces/types'

interface Props extends DefaultPropsType {
  isActive?: boolean
}

export const UploadResultModal = ({
  className = '',
  isActive,
  style,
}: Props) => {
  const router = useRouter()
  const { mutateQuestions } = useQuestions(
    0,
    10,
    {},
    router.query.m === 'mine' || router.query.m === 'teacher',
  )
  const { search, setSearch } = useContext<QuestionContextType>(QuestionContext)
  const [session] = useSession()
  const user: any = session?.user
  // is system.
  const isSystem =
    user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
  // const isAdmin = userInRight([], session)
  const { globalModal } = useContext(WrapperContext)

  const handleModalClose = () => {
    globalModal.setState(null)
    if (uploadSuccessResult.length > 0) {
      mutateQuestions()
    }
  }

  const format = globalModal.state?.data?.data?.format
  const grade = globalModal.state?.data?.data?.grade
  const publisher = globalModal.state?.data?.data?.publisher
  const result = globalModal.state?.data?.data?.result || []
  const series = globalModal.state?.data?.data?.series
  const test_type = globalModal.state?.data?.data?.test_type
  const types = globalModal.state?.data?.data?.types
  const totalQuestion = result.countQuestion
  const totalSuccess = result.success
  const totalFail = totalQuestion - totalSuccess

  const [selected, setSelected] = useState(totalFail > 0 ? 2 : 1)
  // console.log("totalQuestion", totalQuestion)
  // console.log("totalSuccess",totalSuccess)
  // console.log("totalFail", totalFail);
  const uploadResultList = result.listResult
  const uploadSuccessResult = uploadResultList.filter(
    (question: any) => question['success'] === true,
  )
  const uploadFailResult = uploadResultList.filter(
    (question: any) => question['success'] === false,
  )

  // console.log("uploadResultList", uploadResultList);
  // console.log('uploadSuccessResult', uploadSuccessResult)
  // console.log('uploadFailResult', uploadFailResult)

  const scrollNext = (scrollId: string, scrollSection: string) => {
    const thList: any = document.querySelectorAll(scrollSection)
    const elScroll = document.querySelector(`#${scrollId}`)
    let totalWidth = 0
    const scrollClientWidth = elScroll.clientWidth
    const scrollLeft = elScroll.scrollLeft
    const scrollWidth = elScroll.scrollWidth
    const len = thList.length
    for (let i = 0; i < len; i++) {
      const element = thList[i]
      if (totalWidth <= scrollClientWidth + scrollLeft + 16) {
        totalWidth += element.offsetWidth
      } else {
        elScroll.scrollLeft = totalWidth - scrollClientWidth
        return
      }
      if (i == len - 1) {
        elScroll.scrollLeft = scrollWidth - scrollClientWidth
      }
    }
  }

  const scrollBack = (scrollId: string, scrollSection: string) => {
    const thList: any = document.querySelectorAll(scrollSection)
    const elScroll = document.querySelector(`#${scrollId}`)
    let totalWidth = 0
    const scrollClientWidth = elScroll.clientWidth
    const scrollLeft = elScroll.scrollLeft

    for (const element of thList) {
      if (totalWidth + element.offsetWidth >= scrollClientWidth + scrollLeft) {
        elScroll.scrollLeft = totalWidth - scrollClientWidth
      } else {
        totalWidth += element.offsetWidth
      }
    }
  }
  const scrollUp = (scrollId: string, scrollSection: string) => {
    // const tdList:any = document.querySelectorAll(scrollSection);

    // const elScroll = document.querySelector(`#${scrollId}`);
    // let totalHeight = 0;
    // const scrollClientHeight =  elScroll.clientHeight;
    // const scrollTop = elScroll.scrollTop;

    // for(const element of tdList){
    //     if((totalHeight + element.offsetHeight) >= scrollClientHeight + scrollTop){
    //         elScroll.scrollTop = (totalHeight - scrollClientHeight)

    //     }else{
    //         totalHeight += element.offsetHeight
    //     }
    // }
    const elScroll = document.querySelector(`#${scrollId}`)
    const scrollHeight = elScroll.scrollHeight
    const clientHeight = elScroll.clientHeight
    const scrollTop = elScroll.scrollTop

    if (scrollTop + clientHeight > 80) {
      elScroll.scrollTop -= 80
    } else {
      elScroll.scrollTop = 0
    }
  }
  const scrollDown = (scrollId: string, scrollSection: string) => {
    // const tdList: any = document.querySelectorAll(scrollSection);
    // const elScroll = document.querySelector(`#${scrollId}`);
    // let totalHeight = 0;
    // const scrollClientHeight =  elScroll.clientWidth;
    // const scrollTop = elScroll.scrollTop;
    // const scrollHeight = elScroll.scrollHeight;
    // for(const element of tdList){
    //     if(totalHeight <= scrollClientHeight + scrollTop){
    //         totalHeight += element.offsetHeight
    //     }else{
    //         elScroll.scrollTop = (totalHeight - scrollClientHeight)
    //     }
    // }
    const elScroll = document.querySelector(`#${scrollId}`)
    const scrollHeight = elScroll.scrollHeight
    const clientHeight = elScroll.clientHeight
    const scrollTop = elScroll.scrollTop

    if (scrollTop + clientHeight < scrollHeight - 80) {
      elScroll.scrollTop += 80
    } else {
      elScroll.scrollTop = scrollHeight
    }
  }

  const onLoadElement = (elementId: string) => {
    const scroll = document.querySelector(`#${elementId}`)
    const clientHeight = scroll.clientHeight
    const scrollHeight = scroll.scrollHeight
    const clientWidth = scroll.clientWidth
    const scrollWidth = scroll.scrollWidth

    if (scrollHeight > clientHeight) {
      scroll.parentElement
        .querySelectorAll(`.btn-scroll-vertical`)
        .forEach((el: any) => {
          el.style.display = 'flex'
        })
      const btn_next: HTMLElement =
        scroll.parentElement.querySelector('.btn-scroll__next')
      btn_next.style.right = '16px'
    } else {
      const btn_next: HTMLElement =
        scroll.parentElement.querySelector('.btn-scroll__next')
      btn_next.style.right = '0px'
    }
    if (scrollWidth > clientWidth) {
      scroll.parentElement
        .querySelectorAll(`.btn-scroll-horizontal`)
        .forEach((el: any) => {
          el.style.display = 'flex'
        })
    }
  }
  const renderBold = (children: any) => {
    return <b>{children}</b>
  }
  const renderItalic = (children: any) => {
    return <i>{children}</i>
  }
  const renderUnderLine = (children: any) => {
    return <u>{children}</u>
  }
  const renderText = (text: any[]) => {
    if (Array.isArray(text)) {
      return text.map((element: any) => {
        let htmlItem = element.text
        if (element.bold == true) {
          htmlItem = renderBold(htmlItem)
        }
        if (element.italic == true) {
          htmlItem = renderItalic(htmlItem)
        }
        if (element.underline == true) {
          htmlItem = renderUnderLine(htmlItem)
        }
        return htmlItem
      })
    } else {
      return text
    }
  }
  let countAns = 0
  const getTitleAnswers = (sampleArr: any[]) => {
    const arr: any = []
    for (const element of sampleArr) {
      const item = element
      let countAnsItem = 10
      while (!item[`answer_${countAnsItem}`] && countAnsItem >= 1) {
        countAnsItem -= 1
      }
      if (countAnsItem > countAns) {
        countAns = countAnsItem
      }
    }
    for (let i = 1; i <= countAns; i++) {
      arr.push(i)
    }
    return arr
  }

  //export success
  const exportSuccessResult = async () => {
    const workbook = new Excel.Workbook()
    const worksheet = workbook.addWorksheet('My Sheet')

    const columns = []
    const lenColumns = cellArrayObject.length
    for (let i = 0; i < lenColumns; i++) {
      const itemCell = cellArrayObject[i]
      columns.push({ header: itemCell.display, key: itemCell.key, width: 30 })
    }
    worksheet.columns = columns
    for (let i = 0; i < uploadSuccessResult.length; i++) {
      const itemResult = uploadSuccessResult[i]
      worksheet.addRow(itemResult)
      for (let j = 0; j < lenColumns; j++) {
        const itemCell = cellArrayObject[j]
        const valueCell: any = itemResult[itemCell.key]
        const cell = worksheet.getCell(`${itemCell.code}${i + 2}`) ///trừ header và bắt đầu từ 1
        cell.border = {
          top: { style: 'thin', color: { argb: 'FF000000' } },
          left: { style: 'thin', color: { argb: 'FF000000' } },
          bottom: { style: 'thin', color: { argb: 'FF000000' } },
          right: { style: 'thin', color: { argb: 'FF000000' } },
        }
        cell.font = {
          size: 12,
        }
        cell.alignment = {
          vertical: 'top',
          horizontal: 'left',
          wrapText: true,
        }
        if (valueCell && Array.isArray(valueCell) == true) {
          const richText = []
          for (let index = 0; index < valueCell.length; index++) {
            if (typeof valueCell[index] == 'string') {
              richText.push({ text: `${valueCell[index]}\r\n` })
            } else {
              const fontStyle = {
                bold: valueCell[index]?.bold,
                underline: valueCell[index]?.underline,
                italic: valueCell[index]?.italic,
              }
              richText.push({
                font: fontStyle,
                text: valueCell[index]?.text,
              })
              // if (valueCell[index]?.bold == true) {
              //   richText.push({
              //     font: { bold: true },
              //     text: valueCell[index].text,
              //   })
              // } else if (valueCell[index]?.underline == true) {
              //   richText.push({
              //     font: { underline: true },
              //     text: valueCell[index].text,
              //   })
              // } else {
              //   richText.push({ text: valueCell[index]?.text })
              // }
            }
          }
          cell.value = {
            richText: richText,
          }
        } else if (valueCell) {
          cell.value = {
            richText: [{ text: valueCell }],
          }
        }
      }
    }
    for (let i = 0; i < lenColumns; i++) {
      const itemCell = cellArrayObject[i]
      const cell = worksheet.getCell(`${itemCell.code}1`)
      const row = worksheet.getRow(1)
      row.height = 30
      cell.fill = {
        type: 'pattern',
        bgColor: {
          argb: '4E81BE',
        },
        pattern: 'solid',
        fgColor: { argb: '4E81BE' },
      }
      cell.font = {
        size: 12,
        bold: true,
      }
      cell.alignment = {
        vertical: 'middle',
        horizontal: 'left',
      }
      cell.border = {
        top: { style: 'thin', color: { argb: 'FF000000' } },
        left: { style: 'thin', color: { argb: 'FF000000' } },
        bottom: { style: 'thin', color: { argb: 'FF000000' } },
        right: { style: 'thin', color: { argb: 'FF000000' } },
      }
    }
    const buffer = await workbook.xlsx.writeBuffer()

    const url = URL.createObjectURL(
      new Blob([buffer], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      }),
    )
    const link = document.createElement('a')
    link.setAttribute('download', 'import_result_success.xlsx')
    link.href = url
    link.click()
  }

  //export error
  const exportFailResult = async () => {
    const workbook = new Excel.Workbook()
    const worksheet = workbook.addWorksheet('My Sheet')

    const columns = []
    const lenColumns = cellArrayObjectFail.length
    for (let i = 0; i < lenColumns; i++) {
      const itemCell = cellArrayObjectFail[i]
      columns.push({ header: itemCell.display, key: itemCell.key, width: 30 })
    }
    worksheet.columns = columns
    for (let i = 0; i < uploadFailResult.length; i++) {
      const itemResult = uploadFailResult[i]
      worksheet.addRow(itemResult)
      for (let j = 0; j < lenColumns; j++) {
        const itemCell = cellArrayObjectFail[j]
        const valueCell: any = itemResult[itemCell.key]
        const cell = worksheet.getCell(`${itemCell.code}${i + 2}`) ///trừ header và bắt đầu từ 1
        cell.border = {
          top: { style: 'thin', color: { argb: 'FF000000' } },
          left: { style: 'thin', color: { argb: 'FF000000' } },
          bottom: { style: 'thin', color: { argb: 'FF000000' } },
          right: { style: 'thin', color: { argb: 'FF000000' } },
        }
        cell.font = {
          size: 12,
        }
        cell.alignment = {
          vertical: 'top',
          horizontal: 'left',
          wrapText: true,
        }
        // console.log("valueCell=====",valueCell)
        if (valueCell && Array.isArray(valueCell) == true) {
          const richText = []
          for (let index = 0; index < valueCell.length; index++) {
            if (typeof valueCell[index] == 'string') {
              richText.push({ text: `${valueCell[index]}\r\n` })
            }else{
              const fontStyle = {
                bold: valueCell[index]?.bold,
                underline: valueCell[index]?.underline,
                italic: valueCell[index]?.italic,
              }
              richText.push({
                font: fontStyle,
                text: valueCell[index]?.text,
              })
            }
          }
          cell.value = {
            richText: richText,
          }
        } else if (valueCell) {
          cell.value = {
            richText: [{ text: valueCell }],
          }
        }
      }
    }
    for (let i = 0; i < lenColumns; i++) {
      const itemCell = cellArrayObjectFail[i]
      const cell = worksheet.getCell(`${itemCell.code}1`)
      const row = worksheet.getRow(1)
      row.height = 30
      cell.fill = {
        type: 'pattern',
        bgColor: {
          argb: '4E81BE',
        },
        pattern: 'solid',
        fgColor: { argb: '4E81BE' },
      }
      cell.font = {
        size: 12,
        bold: true,
      }
      cell.alignment = {
        vertical: 'middle',
        horizontal: 'left',
      }
      cell.border = {
        top: { style: 'thin', color: { argb: 'FF000000' } },
        left: { style: 'thin', color: { argb: 'FF000000' } },
        bottom: { style: 'thin', color: { argb: 'FF000000' } },
        right: { style: 'thin', color: { argb: 'FF000000' } },
      }
    }

    const buffer = await workbook.xlsx.writeBuffer()

    const url = URL.createObjectURL(
      new Blob([buffer], {
        type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
      }),
    )
    const link = document.createElement('a')
    link.setAttribute('download', 'import_result_fail.xlsx')
    link.href = url
    link.click()
  }

  const keyPress = (e: any) => {
    let scrollId: string
    let scrollSectionX: string
    let scrollSectionY: string
    if (selected === 1) {
      scrollId = 'success-result-scroll'
      scrollSectionX = '.result-table__success th'
      scrollSectionY = '.result-table__success tr'
    }
    if (selected === 2) {
      scrollId = 'fail-result-scroll'
      scrollSectionX = '.result-table__fail th'
      scrollSectionY = '.result-table__fail tr'
    }
    // const scrollElement = document.getElementById(scrollId);
    if (e.key === 'ArrowUp') {
      // scrollElement.scrollTop -=80
      scrollUp(scrollId, scrollSectionY)
    }
    if (e.key === 'ArrowDown') {
      // scrollElement.scrollTop +=80
      scrollDown(scrollId, scrollSectionY)
    }
    if (e.key === 'ArrowLeft') {
      scrollBack(scrollId, scrollSectionX)
    }
    if (e.key === 'ArrowRight') {
      scrollNext(scrollId, scrollSectionX)
    }
  }

  useEffect(() => {
    onLoadElement('success-result-scroll')
    onLoadElement('fail-result-scroll')
    window.addEventListener('keydown', keyPress)
    return () => {
      window.removeEventListener('keydown', keyPress)
    }
  }, [selected])

  return (
    <>
      <Modal
        className={`upload-question-result-modal ${className} upload-question-result-modal-custom`}
        size="lg"
        open={isActive}
        style={{
          overflow: 'hidden',
          background: 'rgba(39,44,54,0.3)',
        }}
        onClose={handleModalClose}
        backdrop={false}
      >
        <Modal.Body style={{ overflow: 'hidden' }}>
          <div
            className="modal-header modal-header__success"
            style={{ display: totalFail === 0 ? 'flex' : 'none' }}
          >
            {' '}
            Câu hỏi thành công
            <Button className="option-btn" onClick={handleModalClose}>
              <img src="/images/icons/ic-close-bright.png" alt="close" />
            </Button>
          </div>
          <div
            className="modal-header modal-header__fail"
            style={{ display: totalFail > 0 ? 'flex' : 'none' }}
          >
            {' '}
            Kết quả thất bại
            <Button className="option-btn" onClick={handleModalClose}>
              <img src="/images/icons/ic-close-bright.png" alt="close" />
            </Button>
          </div>
          <div className="modal-sub-content">
            <div className="general-result">
              <div
                className="total-info"
                style={isSystem ? {} : { width: '40%' }}
              >
                <div className="info-col-1">
                  <div
                    className="color-mark"
                    style={{ backgroundColor: '#5DB9D9' }}
                  ></div>
                  <div className="result-section">
                    <span style={{ flex: '1' }}>Tổng câu hỏi:</span>
                    <span className="value-result">{totalQuestion}</span>
                  </div>
                </div>
                <div className="info-col-1">
                  <div
                    className="color-mark"
                    style={{ backgroundColor: '#68AC8E' }}
                  ></div>
                  <div className="result-section">
                    <span style={{ flex: '1' }}>Thành công:</span>
                    <span className="value-result">{totalSuccess}</span>
                  </div>
                </div>
                <div className="info-col-1">
                  <div
                    className="color-mark"
                    style={{ backgroundColor: '#C96953' }}
                  ></div>
                  <div className="result-section">
                    <span style={{ flex: '1' }}>Thất bại:</span>
                    <span className="value-result">{totalFail}</span>
                  </div>
                </div>
              </div>
              <div
                className="total-info"
                style={
                  isSystem
                    ? {}
                    : { width: '40%', padding: '0 4%', border: 'none' }
                }
              >
                <div>
                  <span>Khối lớp:</span>
                  <span>{grade ? grade : '---'}</span>
                </div>

                <div>
                  <span>Môn học:</span>
                  <span>{test_type}</span>
                </div>

                <div style={isSystem ? {} : { display: 'none' }}>
                  <span>Nhà xuất bản:</span>
                  <span>{publisher}</span>
                </div>
              </div>
              {isSystem ? (
                <div className="total-info" style={{ border: 'hidden' }}>
                  <div>
                    <span>Chương trình:</span>
                    <span>{series ? series : '---'}</span>
                  </div>

                  <div>
                    <span>Tiêu chuẩn:</span>
                    <span>{format}</span>
                  </div>

                  <div>
                    <span>Kỳ thi:</span>
                    <span>{types}</span>
                  </div>
                </div>
              ) : (
                ''
              )}
            </div>
            <div className="result-table">
              <div className="select-result">
                <div
                  className="table-header table-header__success"
                  data-active={selected === 1 || totalFail === 0 ? true : false}
                  onClick={() => setSelected(1)}
                  style={{
                    width: totalFail === 0 ? '100%' : '50%',
                    display: totalSuccess === 0 ? 'none' : 'flex',
                  }}
                >
                  {' '}
                  Câu hỏi thành công
                  <Button
                    className="file-download-btn"
                    onClick={exportSuccessResult}
                    style={{
                      display:
                        selected === 1 || totalFail === 0 ? 'flex' : 'none',
                      right: totalFail === 0 ? '0px' : '50%',
                    }}
                  >
                    <img src="/images/icons/ic-download.png" alt="download" />
                    Tải xuống
                  </Button>
                </div>
                <div
                  className="table-header table-header__fail"
                  data-active={selected === 2 ? true : false}
                  onClick={() => setSelected(2)}
                  style={{
                    display: totalFail === 0 ? 'none' : 'flex',
                    width: totalSuccess === 0 ? '100%' : '50%',
                  }}
                >
                  {' '}
                  Kết quả thất bại
                  <Button
                    className="file-download-btn"
                    onClick={exportFailResult}
                    style={{
                      display: selected == 2 ? 'flex' : 'none',
                      right: '0px',
                    }}
                  >
                    <img src="/images/icons/ic-download.png" alt="download" />
                    Tải xuống
                  </Button>
                </div>
              </div>

              <div
                style={{
                  position: 'relative',
                  height: 'calc(100% - 40px)',
                  overflow: 'visible',
                  display: selected == 1 || totalFail === 0 ? 'block' : 'none',
                }}
              >
                <div
                  id="success-result-scroll"
                  style={{
                    height: '100%',
                    overflow: 'auto',
                    scrollBehavior: 'smooth',
                  }}
                >
                  <div className="result-table__success">
                    <table>
                      <thead>
                        <tr>
                          <th></th>
                          <th className="text-center">STT</th>
                          <th className="text-center">LOẠI CÂU HỎI</th>
                          <th className="text-center">KỸ NĂNG</th>
                          <th>NỘI DUNG BÀI ĐỌC/BÀI NGHE</th>
                          <th>YÊU CẦU ĐỀ BÀI</th>
                          <th>CÂU HỎI</th>
                          {getTitleAnswers(uploadSuccessResult).map(
                            (value: number) => (
                              <th key={'succecc-th' + value}>ĐÁP ÁN {value}</th>
                            ),
                          )}
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {uploadSuccessResult.map((item: any, index: number) => {
                          const parent_question_description: any = renderText(
                            item.parent_question_description,
                          )
                          const question_description = renderText(
                            item.question_description,
                          )
                          return (
                            <>
                              <tr key={`success-${index}`}>
                                <td></td>
                                <td>
                                  <div className="text-center">
                                    {item.index}
                                  </div>
                                </td>
                                <td className="text-center">
                                  <Whisper
                                    placement="bottom"
                                    trigger={
                                      item.question_type ? 'hover' : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {getDisplay.handleGetDisplay(
                                          TASK_SELECTIONS,
                                          item.question_type,
                                        )}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {renderText(item.question_type)}
                                    </span>
                                  </Whisper>
                                </td>
                                <td className="text-center">
                                  <Whisper
                                    placement="bottom"
                                    trigger={item.skill ? 'hover' : 'none'}
                                    speaker={
                                      <Tooltip>
                                        {getDisplay.handleGetDisplay(
                                          SKILLS_SELECTIONS,
                                          item.skill,
                                        )}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {renderText(item.skill)}
                                    </span>
                                  </Whisper>
                                </td>
                                <td>
                                  <Whisper
                                    placement="rightStart"
                                    trigger={
                                      item.parent_question_text ||
                                      item.audio_script
                                        ? 'hover'
                                        : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {item.parent_question_text
                                          ? renderText(
                                              item.parent_question_text,
                                            )
                                          : renderText(item.audio_script)}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {item.parent_question_text
                                        ? renderText(item.parent_question_text)
                                        : renderText(item.audio_script)}
                                    </span>
                                  </Whisper>
                                </td>
                                <td>
                                  <Whisper
                                    placement="rightStart"
                                    trigger={
                                      item.parent_question_description ||
                                      item.question_description
                                        ? 'hover'
                                        : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {item.parent_question_description
                                          ? parent_question_description
                                          : question_description}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {item.parent_question_description
                                        ? parent_question_description
                                        : question_description}
                                    </span>
                                  </Whisper>
                                </td>
                                <td>
                                  <Whisper
                                    placement="rightStart"
                                    trigger={
                                      item.question_text ? 'hover' : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {renderText(item.question_text)}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {renderText(item.question_text)}
                                    </span>
                                  </Whisper>
                                </td>
                                {getTitleAnswers(uploadSuccessResult).map(
                                  (value: number, i: any) => (
                                    <td key={'answer_success-' + i}>
                                      <Whisper
                                        placement={
                                          i + 1 <
                                          getTitleAnswers(uploadFailResult)
                                            .length
                                            ? 'rightStart'
                                            : 'leftStart'
                                        }
                                        trigger={
                                          item[`answer_${value}`]
                                            ? 'hover'
                                            : 'none'
                                        }
                                        speaker={
                                          <Tooltip>
                                            <span
                                              style={{ whiteSpace: 'pre-wrap' }}
                                            >
                                              {renderText(
                                                item[`answer_${value}`],
                                              )}
                                            </span>
                                          </Tooltip>
                                        }
                                      >
                                        <span className="wrap-cell">
                                          {renderText(item[`answer_${value}`])}
                                        </span>
                                      </Whisper>
                                    </td>
                                  ),
                                )}
                                <td></td>
                              </tr>
                            </>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="sticky-title">
                  <div style={{ width: 15 }}></div>
                  <div
                    style={{ width: 30, borderLeft: 'none' }}
                    className="sticky-element"
                  >
                    STT
                  </div>
                  <div style={{ width: 120 }} className="sticky-element">
                    LOẠI CÂU HỎI
                  </div>
                </div>
                <div
                  className="btn-scroll btn-scroll__back btn-scroll-horizontal"
                  onClick={() => {
                    scrollBack(
                      'success-result-scroll',
                      '.result-table__success th',
                    )
                  }}
                >
                  <div className="arrow arrow__back"></div>
                </div>
                <div
                  className="btn-scroll btn-scroll__next btn-scroll-horizontal"
                  onClick={() => {
                    scrollNext(
                      'success-result-scroll',
                      '.result-table__success th',
                    )
                  }}
                >
                  <div className="arrow arrow__next"></div>
                </div>
                <div
                  className="btn-scroll btn-scroll__up btn-scroll-vertical"
                  onClick={() => {
                    scrollUp(
                      'success-result-scroll',
                      '.result-table__success tr',
                    )
                  }}
                >
                  <div className="arrow arrow__up"></div>
                </div>
                <div
                  className="btn-scroll btn-scroll__down btn-scroll-vertical"
                  onClick={() => {
                    scrollDown(
                      'success-result-scroll',
                      '.result-table__success tr',
                    )
                  }}
                >
                  <div className="arrow arrow__down"></div>
                </div>
              </div>

              <div
                style={{
                  position: 'relative',
                  height: 'calc(100% - 40px)',
                  overflow: 'visible',
                  display: selected == 2 ? 'block' : 'none',
                }}
              >
                <div
                  id="fail-result-scroll"
                  style={{
                    height: '100%',
                    overflow: 'auto',
                    scrollBehavior: 'smooth',
                  }}
                >
                  <div className="result-table__fail">
                    <table>
                      <thead>
                        <tr>
                          <th></th>
                          <th>STT</th>
                          <th>MÔ TẢ LỖI</th>
                          <th>LOẠI CÂU HỎI</th>
                          <th>KỸ NĂNG</th>
                          <th>NỘI DUNG BÀI ĐỌC/BÀI NGHE</th>
                          <th>YÊU CẦU ĐỀ BÀI</th>
                          <th>CÂU HỎI</th>
                          {getTitleAnswers(uploadFailResult).map(
                            (value: number) => (
                              <th key={'err-th' + value}>ĐÁP ÁN {value}</th>
                            ),
                          )}
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        {uploadFailResult.map((item: any, index: number) => {
                          const parent_question_description: any = renderText(
                            item.parent_question_description,
                          )
                          const question_description = renderText(
                            item.question_description,
                          )
                          return (
                            <>
                              <tr key={`err-${index}`}>
                                <td></td>
                                <td className="text-center">{item.index}</td>
                                <td className="text-center">
                                  <Whisper
                                    placement="rightStart"
                                    trigger={
                                      item.arr_message ? 'hover' : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {item.arr_message.map(
                                          (i: any, index: number) => (
                                            <li key={index}>- {i}</li>
                                          ),
                                        )}
                                      </Tooltip>
                                    }
                                  >
                                    <span
                                      className="wrap-cell"
                                      style={{ display: 'table-cell' }}
                                    >
                                      {item.arr_message.map(
                                        (i: any, index: number) => (
                                          <li key={index}>- {i}</li>
                                        ),
                                      )}
                                    </span>
                                  </Whisper>
                                </td>
                                <td className="text-center">
                                  <Whisper
                                    placement="bottom"
                                    trigger={
                                      item.question_type ? 'hover' : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {getDisplay.handleGetDisplay(
                                          TASK_SELECTIONS,
                                          item.question_type,
                                        )}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {renderText(item.question_type)}
                                    </span>
                                  </Whisper>
                                </td>
                                <td className="text-center">
                                  <Whisper
                                    placement="bottom"
                                    trigger={item.skill ? 'hover' : 'none'}
                                    speaker={
                                      <Tooltip>
                                        {getDisplay.handleGetDisplay(
                                          SKILLS_SELECTIONS,
                                          item.skill,
                                        )}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {renderText(item.skill)}
                                    </span>
                                  </Whisper>
                                </td>
                                <td>
                                  <Whisper
                                    placement="rightStart"
                                    trigger={
                                      item.parent_question_text ||
                                      item.audio_script
                                        ? 'hover'
                                        : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {item.parent_question_text
                                          ? renderText(
                                              item.parent_question_text,
                                            )
                                          : renderText(item.audio_script)}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {item.parent_question_text
                                        ? renderText(item.parent_question_text)
                                        : renderText(item.audio_script)}
                                    </span>
                                  </Whisper>
                                </td>
                                <td>
                                  <Whisper
                                    placement="rightStart"
                                    trigger={
                                      item.parent_question_description ||
                                      item.question_description
                                        ? 'hover'
                                        : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {item.parent_question_description
                                          ? parent_question_description
                                          : question_description}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {item.parent_question_description
                                        ? parent_question_description
                                        : question_description}
                                    </span>
                                  </Whisper>
                                </td>
                                <td>
                                  <Whisper
                                    placement="rightStart"
                                    trigger={
                                      item.question_text ? 'hover' : 'none'
                                    }
                                    speaker={
                                      <Tooltip>
                                        {renderText(item.question_text)}
                                      </Tooltip>
                                    }
                                  >
                                    <span className="wrap-cell">
                                      {renderText(item.question_text)}
                                    </span>
                                  </Whisper>
                                </td>
                                {getTitleAnswers(uploadFailResult).map(
                                  (value: number, i: any) => (
                                    <td key={'answer-err' + i}>
                                      <Whisper
                                        placement={
                                          i + 1 <
                                          getTitleAnswers(uploadFailResult)
                                            .length
                                            ? 'rightStart'
                                            : 'leftStart'
                                        }
                                        trigger={
                                          item[`answer_${value}`]
                                            ? 'hover'
                                            : 'none'
                                        }
                                        speaker={
                                          <Tooltip>
                                            <span
                                              style={{ whiteSpace: 'pre-wrap' }}
                                            >
                                              {renderText(
                                                item[`answer_${value}`],
                                              )}
                                            </span>
                                          </Tooltip>
                                        }
                                      >
                                        <span className="wrap-cell">
                                          {renderText(item[`answer_${value}`])}
                                        </span>
                                      </Whisper>
                                    </td>
                                  ),
                                )}
                                <td></td>
                              </tr>
                            </>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </div>
                <div className="sticky-title">
                  <div style={{ width: 15 }}></div>
                  <div
                    style={{ width: 30, borderLeft: 'none' }}
                    className="sticky-element"
                  >
                    STT
                  </div>
                  <div style={{ width: 200 }} className="sticky-element">
                    MÔ TẢ LỖI
                  </div>
                  <div style={{ width: 120 }} className="sticky-element">
                    LOẠI CÂU HỎI
                  </div>
                </div>
                <div
                  className="btn-scroll btn-scroll__back btn-scroll-horizontal"
                  onClick={() => {
                    scrollBack('fail-result-scroll', '.result-table__fail th')
                  }}
                >
                  <div className="arrow arrow__back"></div>
                </div>
                <div
                  className="btn-scroll btn-scroll__next btn-scroll-horizontal"
                  onClick={() => {
                    scrollNext('fail-result-scroll', '.result-table__fail th')
                  }}
                >
                  <div className="arrow arrow__next"></div>
                </div>
                <div
                  className="btn-scroll btn-scroll__up btn-scroll-vertical"
                  onClick={() => {
                    scrollUp('fail-result-scroll', '.result-table__fail tr')
                  }}
                >
                  <div className="arrow arrow__up"></div>
                </div>
                <div
                  className="btn-scroll btn-scroll__down btn-scroll-vertical"
                  onClick={() => {
                    scrollDown('fail-result-scroll', '.result-table__fail tr')
                  }}
                >
                  <div className="arrow arrow__down"></div>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </>
  )
}
