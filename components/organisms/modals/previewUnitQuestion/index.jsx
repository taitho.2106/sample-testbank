import { useContext } from 'react'

import { Button, Drawer } from 'rsuite'

import { WrapperContext } from 'interfaces/contexts'

import { GamePickerReviewQuestions } from '../../gamePickerReviewQuestions'
import useTranslation from "../../../../hooks/useTranslation";

export const PreviewUnitQuestion = ({ isActive }) => {
    const { t } = useTranslation()
    const { globalModal } = useContext(WrapperContext)

    return (
        <Drawer
            className={`o-preview-unit-question-drawer `}
            full
            placement="bottom"
            open={isActive}
            style={{}}
            onClose={() => globalModal.setState(null)}
        >
            <Drawer.Body>
                <Button
                    className="o-preview-unit-question-drawer__toggle"
                    onClick={() => globalModal.setState(null)}
                >
                    <img src="/images/icons/ic-close-darker.png" alt="close" />
                </Button>
                <div className="o-preview-unit-question-drawer__header">
                    <h5 className="o-preview-unit-question-drawer__title">
                        {t('common')['question-preview']}
                    </h5>
                </div>
                <div className="o-preview-unit-question-drawer__body">
                    <GamePickerReviewQuestions
                        data={globalModal.state.data}
                        type={globalModal.state.questionType}
                        id={'preview-question'}
                    />
                </div>
            </Drawer.Body>
        </Drawer>
    )
}