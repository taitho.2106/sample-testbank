import { useContext, useState } from 'react'

import { Button, Modal } from 'rsuite'

import { callApi } from 'api/utils'
import { CheckBox } from 'components/atoms/checkbox'
import { DateInput } from 'components/atoms/dateInput'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import useNoti from 'hooks/useNoti'
import { WrapperContext } from 'interfaces/contexts'
import { formatDate } from 'utils/string'

import { DefaultPropsType } from '../../../../interfaces/types'

interface Props extends DefaultPropsType {
  isActive: boolean
}

export const ChangeDateModal = ({ className = '', isActive, style }: Props) => {
  const { globalModal } = useContext(WrapperContext)
  const { getNoti } = useNoti()

  const data = globalModal.state?.content?.data
  const id = data?.id
  const time = data?.time
  const start = data?.start_date
  const end = data?.end_date
  const isForceEnd = globalModal.state?.content?.isForceEnd
  const onSubmit = globalModal.state?.onSubmit

  const [modalState, setModalState] = useState({ ...data } as any)
  const [triggerEndDate, setTriggerEndDate] = useState(null as any)

  const handleModalClose = () => globalModal.setState(null)

  const handleModalSubmit = async () => {
    const newStart = modalState?.start_date
      ? isNaN(modalState.start_date)
        ? new Date(modalState.start_date).getTime()
        : modalState.start_date
      : null
    const newEnd = modalState?.end_date
      ? isNaN(modalState.end_date)
        ? new Date(modalState.end_date).getTime()
        : modalState.end_date
      : null
    if (!newStart) {
      getNoti('error', 'topEnd', 'Thời gian bắt đầu không được trống')
      return
    }

    if (!newEnd) {
      getNoti('error', 'topEnd', 'Thời gian kết thúc không được trống')
      return
    }

    if (newStart > newEnd) {
      getNoti(
        'error',
        'topEnd',
        'Thời gian bắt đầu phải trước thời gian kết thúc',
      )
      return
    }

    const newData = { ...modalState, start_date: newStart, end_date: newEnd }
    const response: any = await callApi(
      '/api/unit-test?type=datetime',
      'put',
      'Token',
      newData,
    )
    if (response?.status === 200 && response?.data)
      onSubmit({
        id,
        time: modalState?.time,
        start_date: modalState?.start_date
          ? new Date(modalState.start_date)
          : null,
        end_date: modalState?.start_date ? new Date(modalState.end_date) : null,
      })
    setTimeout(() => {
      getNoti('success', 'topEnd', 'Cập nhật thành công')
      handleModalClose()
    }, 0)
  }

  const checkActive = () => {
    const detail = modalState
    const startDate = detail.start_date
    const endDate = detail?.end_date || null
    if (!startDate) return false
    const d = new Date()
    const currentTime = d.getTime()
    const startStamp = new Date(startDate).getTime()
    if (currentTime < startStamp) return false
    if (endDate) {
      const endStamp = new Date(endDate).getTime()
      if (currentTime > endStamp) return false
    }
    return true
  }

  const handleTimeChange = (str: string) => {
    const parseValue = parseInt(str || '0')
    if (parseValue <= 0) getNoti('error', 'topEnd', 'Thời lượng phải lớn hơn 0')
    let detail = modalState
    if (!detail) detail = {}
    detail.time = parseValue
    setModalState({ ...detail })
    if (isForceEnd)
      if (parseValue && start) {
        handleEndDateChange(
          new Date(detail.start_date).getTime() + parseValue * 60 * 1000,
        )
        setTriggerEndDate(
          new Date(
            new Date(detail.start_date).getTime() + parseValue * 60 * 1000,
          ),
        )
      }
  }

  const handleStartDateChange = (timestamp: number) => {
    let detail = modalState
    if (!detail) detail = {}
    detail.start_date = timestamp
    setModalState({ ...detail })
    if (isForceEnd)
      if (detail.time && timestamp) {
        handleEndDateChange(timestamp + detail.time * 60 * 1000)
        setTriggerEndDate(new Date(timestamp + detail.time * 60 * 1000))
      }
  }

  const handleEndDateChange = (timestamp: number) => {
    let detail = modalState
    if (!detail) detail = {}
    detail.end_date = timestamp
    setModalState({ ...detail })
  }

  return (
    <Modal
      className={`o-share-link-modal ${className}`}
      open={isActive}
      style={style}
      onClose={handleModalClose}
    >
      <Modal.Body>
        <div className="modal-header" style={{ paddingTop: 0 }}>
          <img src="/images/collections/clt-change-date.png" alt="banner" />
          <h5 className="modal-title">Thay đổi thời gian làm bài</h5>
        </div>
        <div className="modal-content input-group">
          <InputWithLabel
            className="input-item"
            decimal={0}
            defaultValue={time}
            label="Thời lượng"
            min={0}
            type="number"
            onBlur={handleTimeChange}
            style={{ zIndex: 5 }}
          />
          <div className="input-item --checkbox">
            <CheckBox checked={checkActive()} disabled={true} />
            <span>Đang mở</span>
          </div>
          <DateInput
            className="input-item"
            defaultValue={start || null}
            label="Ngày bắt đầu"
            toolTip="Nhập ngày giờ mở bài thi"
            placement="topStart"
            time={true}
            style={{ zIndex: 2 }}
            onChange={handleStartDateChange}
          />
          <DateInput
            className="input-item"
            defaultValue={end || null}
            disabled={isForceEnd}
            label="Ngày kết thúc"
            toolTip={isForceEnd ? "Ngày giờ đóng bài thi = Thời gian bắt đầu làm bài + Thời lượng":"Nhập ngày giờ đóng bài thi"}
            placement="topStart"
            time={true}
            triggerSetValue={triggerEndDate}
            style={{ zIndex: 2 }}
            onChange={(timestamp: number) =>
              !isForceEnd && handleEndDateChange(timestamp)
            }
          />
        </div>
        <div className="modal-footer" style={{ display: 'flex' }}>
          <Button className="secondary" onClick={handleModalClose}>
            Quay lại
          </Button>
          <Button className="primary" onClick={handleModalSubmit}>
            Cập nhật
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  )
}
