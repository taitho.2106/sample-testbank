import { useContext, useRef, useState, useCallback, useEffect } from 'react'

import { UseFormRegisterReturn } from 'react-hook-form'
import { useForm } from 'react-hook-form'
import { Button, Modal, toaster } from 'rsuite'

import { USER_GUIDE_SCREEN_ID, USER_ROLES } from '@/interfaces/constants'
import { paths } from 'api/paths'
import { AlertNoti } from 'components/atoms/alertNoti'
import { SelectField } from 'components/atoms/selectField'
import useGrade from 'hooks/useGrade'
import useSeries from 'hooks/useSeries'
import { WrapperContext } from 'interfaces/contexts'
import { FORMAT_SELECTIONS, PUBLISHER_SELECTIONS, TEST_TYPE_SELECTIONS, TYPES_SELECTIONS } from 'interfaces/struct'
import { DefaultPropsType } from 'interfaces/types'
import { formatBytes } from 'utils/string'

import api from '../../../../lib/api'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import { filterSeriesByGrade } from 'utils/series/filterSeriesByGrade'

interface PropsType extends DefaultPropsType {
  isActive?: boolean
  userInfo?: any
  onChange?: any
  register?: UseFormRegisterReturn
  error?: any
  defaultValue?: any
}
function UploadModal({ isActive, className = '', userInfo }: PropsType) {
  const { globalModal } = useContext(WrapperContext)
  const { getGrade } = useGrade()
  const { getSeries } = useSeries()
  const [gradeId, setGradeId] = useState(null)
  const [listSeries, setListSeries] = useState([])
  const {
    register,
    formState: { errors },
    setValue,
    handleSubmit,
  } = useForm<any>()

  const dateNow = new Date()
  const processName = `${userInfo.id}--${dateNow.getSeconds()}-${dateNow.getMilliseconds()}`
  const formRef = useRef(null)
  const data = globalModal.state.data
  const checkError = useCallback((errors: any, prop: string) => {
    return errors[prop] ? 'error' : ''
  }, [])
  console.log("errors---", errors)
  console.log("data---", data)
  // is system.
  const isSystem = userInfo?.is_admin === 1 || userInfo?.user_role_id == USER_ROLES.Operator
  const onSelectChange = (name: string, value: any) => {
    console.log("--------------",{name, value})
    if (name == 'grade') {
      setGradeId(value)
    }
    setValue(name, value, {
      shouldValidate: true,
    })
  }
  const handleOnClose = () => {
    globalModal.setState(null)
  }
  useEffect(() => {
    const dataSeries = filterSeriesByGrade(getSeries,getGrade, gradeId)
    setListSeries(dataSeries)
  }, [getSeries, gradeId])
  const updateStateProcess = (processing: string, percent: number, title?: string, message?: string) => {
    globalModal.setState({
      id: 'loading-modal',
      processing: processing,
      percent: percent,
      title: title,
      message: message,
    })
  }
  const onSubmit = async (formData: any) => {
    const config = {
      onUploadProgress: (progressEvent: any) => {
        const processing = `${formatBytes(progressEvent.loaded)}/${formatBytes(progressEvent.total)}`
        const percent = (progressEvent.loaded / progressEvent.total) * 100
        updateStateProcess(processing, percent, 'Đang tải lên', 'Dữ liệu của bạn đang được tải lên, vui lòng chờ!')
      },
    }
    if (formData.file.size > 100 * 1024 * 1024) {
      const key = toaster.push(<AlertNoti type={'error'} message={'Tập tin quá lớn, dung lượng tối đa là 100MB'} />, {
        placement: 'topCenter',
      })
      setTimeout(() => toaster.remove(key), 2000)
      return
    }

    const data = new FormData()
    data.append('file', formData.file)
    data.append('format', formData.format || 'NA')
    data.append('grade', formData.grade)
    data.append('process_name', formData.process_name)
    data.append('publisher', formData.publisher || 'NA')
    data.append('series', formData.series || 1)
    data.append('test_type', formData.test_type || 'NA')
    data.append('types', formData.types || 'NA')

    let isSuccess = false
    const intervalProcess = setInterval(async () => {
      const resProcess = await api.get(`${paths.api_import_questions_v2}?process_name=${formData.process_name}`)
      if (resProcess.status == 200 && isSuccess == false) {
        const dataProcess: any = resProcess.data
        switch (dataProcess?.status) {
          case 1:
            updateStateProcess('', 100, 'Đang giải nén dữ liệu', 'Hệ thống đang xử lý, vui lòng chờ trong giây lát')
            break
          case 2:
            const { success, total } = dataProcess
            const percent = (success / total) * 100
            updateStateProcess(
              `${success}/${total}`,
              percent,
              'Đang xử lý dữ liệu',
              'Hệ thống đang xử lý, vui lòng chờ trong giây lát',
            )
            break
          default:
            break
        }
      }
    }, 300)
    const reader = new FileReader()
    reader.onload = async function () {
      // console.log("onload!");
      const res = await api.post(paths.api_import_questions_v2, data, config)
      isSuccess = true
      if (intervalProcess) {
        clearInterval(intervalProcess)
      }

      if (res.status == 200) {
        globalModal.setState({
          id: 'upload-question-result',
          data: res.data,
        })
      } else {
        console.log('err-import question----', res)
        const dataRes: any = res.data
        if (dataRes.code !== 1) {
          const key = toaster.push(<AlertNoti type={'error'} message={dataRes.message} />, {
            placement: 'topCenter',
          })
          setTimeout(() => toaster.remove(key), 2000)
        }
        globalModal.setState({
          id: 'btn-uploadfile',
          data: formData,
        })
      }
    }
    reader.readAsDataURL(formData.file)
    reader.onerror = function () {
      if (intervalProcess) {
        clearInterval(intervalProcess)
      }

      const key = toaster.push(<AlertNoti type={'error'} message={'Vui lòng kiểm tra tập tin và thử lại.'} />, {
        placement: 'topCenter',
      })
      setTimeout(() => toaster.remove(key), 2000)
    }
  }
  const renderMessageErr = (error: any) => {
    const keys: any = Object.keys(error)
    const list = []
    const list2 = []
    for (const i in keys) {
      switch (keys[i]) {
        case 'file':
          if (error[keys[i]].type != 'required') {
            list2.push(error[keys[i]].message)
          } else {
            const key1 = 'file upload'
            list.push(key1)
          }
          break
        case 'grade':
          const key2 = 'khối lớp'
          list.push(key2)
          break
        case 'format':
          const key3 = 'tiêu chuẩn'
          list.push(key3)
          break
        case 'publisher':
          const key4 = 'nhà xuất bản'
          list.push(key4)
          break
        case 'series':
          const key5 = 'chương trình'
          list.push(key5)
          break
        case 'test_type':
          const key6 = 'môn học'
          list.push(key6)
          break
        case 'types':
          const key7 = 'kỳ thi'
          list.push(key7)
          break

        default:
          break
      }
    }
    return { require: list.join(', '), another: list2.join(', ') }
  }
  return (
    <Modal
      className={`upload-file-question ${className} upload-file-question-modal-custom`}
      open={isActive}
      style={{
        maxHeight: '100%',
        background: 'rgba(39,44,54,0.3)',
      }}
      onClose={handleOnClose}
      backdrop={false}
      overflow={true}
    >
      <Modal.Body>
        <form ref={formRef} onSubmit={handleSubmit(onSubmit)}>
          <input type={'hidden'} value={processName} name="process_name" {...register('process_name')}></input>
          <div className="modal-header">
            <span>Tải lên danh sách câu hỏi</span>
            <Button className="modal-toggle" onClick={handleOnClose}>
              <img src="/images/icons/ic-close-admin.png" alt="close" />
            </Button>
          </div>
          <div className="modal-sub-content">
            <div
              className="install-guide"
              onClick={() => {
                window.open(`${window.location.origin}/contents/Templates/Import_Template.zip`, '_blank')
              }}
            >
              <span>Tải file mẫu</span>
            </div>
            <DragDropFile
              error={checkError(errors, 'file')}
              register={register('file', {
                required: true,
                validate: {
                  checkFile: (file) => {
                    if (!file) return false
                    const fileName = file.name
                    const fileExt = fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length) || fileName
                    if (['zip', 'rar'].indexOf(fileExt) < 0) {
                      return 'Chỉ hổ trợ các file có định dạng: zip, rar'
                    }
                    return file.size <= 100 * 1024 * 1024 || 'Dung lượng file tối đa là 100MB'
                  },
                },
              })}
              onChange={onSelectChange.bind(null, 'file')}
              className={`${checkError(errors, 'file')}`}
            />
            <div className="divider">
              <span></span>
            </div>
            <div className="title-form">
              <span>Thông tin câu hỏi</span>
            </div>
            <div className="form-container">
              <div className="form-container__item">
                <div className="form-item" style={{ zIndex: '7' }}>
                  <div className="form-item__field">
                    {getGrade.length>0 && (
                      <SelectFilterPicker
                        data={getGrade}
                        isMultiChoice={false}
                        inputSearchShow={false}
                        isShowSelectAll={false}
                        register={register('grade', {
                          required: true,
                        })}
                        style={{
                          width: 'calc(100% - 25px)',
                          maxWidth: 'calc(100% - 25px)',
                          margin: '0',
                        }}
                        onChange={onSelectChange.bind(null, 'grade')}
                        className={`series ${checkError(errors, 'grade')}`}
                        label="Khối lớp"
                        defaultValue={data?.grade || ""}
                      />
                    )}
                    <div
                      style={{
                        width: '15px',
                        display: 'flex',
                        justifyContent: 'flex-end',
                      }}
                    >
                      <img src="/pt/images/icons/ic-required.svg" alt="" />
                    </div>
                  </div>
                </div>
                {isSystem ? (
                  <div className="form-item" style={{ zIndex: '7' }}>
                    <div className="form-item__field">
                      {listSeries.length>0 && (
                        <SelectFilterPicker
                          data={listSeries}
                          isMultiChoice={false}
                          inputSearchShow={true}
                          isShowSelectAll={false}
                          onChange={onSelectChange.bind(null, 'series')}
                          style={{
                            width: 'calc(100% - 25px)',
                            maxWidth: 'calc(100% - 25px)',
                            margin: '0',
                          }}
                          register={register('series', {
                            required: true,
                          })}
                          className={`series ${checkError(errors, 'series')}`}
                          label="Chương trình"
                          menuSize='xxl'
                          defaultValue={data?.series || ''}
                        />
                      )}
                      <div>
                        <img src="/pt/images/icons/ic-required.svg" alt="" />
                      </div>
                    </div>
                  </div>
                ) : (
                  <div className="form-item" style={{ zIndex: '6' }}>
                    <div className="form-item__field">
                      <SelectField
                        defaultValue={data?.test_type || ''}
                        label="Môn học"
                        register={register('test_type', {
                          required: true,
                        })}
                        data={TEST_TYPE_SELECTIONS}
                        style={{
                          width: 'calc(100% - 25px)',
                          maxWidth: 'calc(100% - 25px)',
                        }}
                        onChange={onSelectChange.bind(null, 'test_type')}
                        className={`${checkError(errors, 'test_type')}`}
                      />
                      <div>
                        <img src="/pt/images/icons/ic-required.svg" alt="" />
                      </div>
                    </div>
                  </div>
                )}
              </div>
              {
                <div className="form-container__item">
                  {!isSystem ? (
                    <></>
                  ) : (
                    <div className="form-item" style={{ zIndex: '6' }}>
                      <div className="form-item__field">
                        <SelectField
                          defaultValue={data?.test_type || ''}
                          label="Môn học"
                          register={register('test_type', {
                            required: true,
                          })}
                          data={TEST_TYPE_SELECTIONS}
                          style={{
                            width: 'calc(100% - 25px)',
                            maxWidth: 'calc(100% - 25px)',
                          }}
                          onChange={onSelectChange.bind(null, 'test_type')}
                          className={`${checkError(errors, 'test_type')}`}
                        />
                        <div>
                          <img src="/pt/images/icons/ic-required.svg" alt="" />
                        </div>
                      </div>
                    </div>
                  )}
                  {isSystem && (
                    <div className="form-item" style={{ zIndex: '6' }}>
                      <div className="form-item__field">
                        <SelectField
                          defaultValue={data?.format || ''}
                          label="Tiêu chuẩn"
                          register={register('format', {
                            required: true,
                          })}
                          data={FORMAT_SELECTIONS}
                          style={{
                            width: 'calc(100% - 25px)',
                            maxWidth: 'calc(100% - 25px)',
                          }}
                          onChange={onSelectChange.bind(null, 'format')}
                          className={`${checkError(errors, 'format')}`}
                        />
                        <div>
                          <img src="/pt/images/icons/ic-required.svg" alt="" />
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              }
              {isSystem && (
                <div className="form-container__item">
                  <div className="form-item">
                    <div className="form-item__field">
                      <SelectField
                        defaultValue={data?.publisher || ''}
                        label="Nhà xuất bản"
                        register={register('publisher', {
                          required: true,
                        })}
                        data={PUBLISHER_SELECTIONS}
                        style={{
                          width: 'calc(100% - 25px)',
                          maxWidth: 'calc(100% - 25px)',
                        }}
                        onChange={onSelectChange.bind(null, 'publisher')}
                        className={`${checkError(errors, 'publisher')}`}
                      />
                      <div>
                        <img src="/pt/images/icons/ic-required.svg" alt="" />
                      </div>
                    </div>
                  </div>
                  <div className="form-item">
                    <div className="form-item__field">
                      <SelectField
                        defaultValue={data?.types || ''}
                        label="Kỳ thi"
                        register={register('types', {
                          required: true,
                        })}
                        data={TYPES_SELECTIONS}
                        style={{
                          width: 'calc(100% - 25px)',
                          maxWidth: 'calc(100% - 25px)',
                        }}
                        onChange={onSelectChange.bind(null, 'types')}
                        className={`${checkError(errors, 'types')}`}
                      />
                      <div>
                        <img src="/pt/images/icons/ic-required.svg" alt="" />
                      </div>
                    </div>
                  </div>
                </div>
              )}
            </div>
            <div className="error-message">
              {Object.keys(errors).length > 0 ? (
                <>
                  {renderMessageErr(errors).require.length > 0 ? (
                    <div>
                      <img src="/pt/images/icons/ic-notifi.svg" alt="notifi" />
                      <span>{`Vui lòng điền đầy đủ thông tin ${renderMessageErr(errors).require}`}</span>
                    </div>
                  ) : (
                    ''
                  )}
                  {renderMessageErr(errors).another.length > 0 ? (
                    <div>
                      <img src="/pt/images/icons/ic-notifi.svg" alt="notifi" />
                      <span>{renderMessageErr(errors).another}</span>
                    </div>
                  ) : (
                    ''
                  )}
                </>
              ) : (
                ''
              )}
            </div>
            <div className="action-form">
              <div className="action-form__guide">
                <img
                  onClick={() => {
                    //open help modal
                    ;(window as any).onOpenHelp(USER_GUIDE_SCREEN_ID.My_Questions_Upload)
                  }}
                  src="/images/icons/ic-guide-modal.png"
                  alt="guide"
                />
              </div>
              <div className="action-form__btn">
                <Button type="submit" className="btn-save">
                  Save
                </Button>
                <Button className="btn-close" onClick={handleOnClose}>
                  Close
                </Button>
              </div>
            </div>
          </div>
        </form>
      </Modal.Body>
    </Modal>
  )
}

export default UploadModal

function DragDropFile({ onChange, register, className = '', defaultValue }: PropsType) {
  const [fileName, setFileName] = useState('' as string)
  const [dragActive, setDragActive] = useState(false)
  const handleChange = (e: any) => {
    e.preventDefault()
    const formDate = new FormData()
    if (e.target.files && e.target.files[0]) {
      setFileName(e.target.files[0]?.name)
      let fileUpload = document.getElementById('file-upload-submit') as any
      fileUpload = e.target.files[0]
      formDate.append('file', fileUpload)
      onChange(fileUpload)
    }
  }
  const handleDrag = (e: any) => {
    e.preventDefault()
    e.stopPropagation()
    const el = document.getElementById('label-file-upload')
    if (e.type === 'dragenter') {
      setDragActive(true)
      el.classList.add('hover')
    } else if (e.type === 'dragover') {
      setDragActive(true)
      el.classList.add('hover')
    } else if (e.type === 'dragleave') {
      setDragActive(false)
      el.classList.remove('hover')
    }
  }
  const handleDrop = (e: any) => {
    e.preventDefault()
    e.stopPropagation()
    setDragActive(false)
    const el = document.getElementById('label-file-upload')
    const formDate = new FormData()
    if (e.dataTransfer.files && e.dataTransfer.files[0]) {
      el.classList.remove('hover')
      setFileName(e.dataTransfer.files[0]?.name)
      let fileUpload = document.getElementById('file-upload-submit') as any
      fileUpload = e.dataTransfer.files[0]
      formDate.append('file', fileUpload)
      onChange(fileUpload)
    }
  }
  return (
    <div
      className="form-file-upload"
      id="form-file-upload"
      onDragEnter={handleDrag}
      onDragLeave={handleDrag}
      onDragOver={handleDrag}
      onDrop={handleDrop}
    >
      <input {...register} type="hidden" id="file-upload-submit" />
      <input
        type="file"
        id="input-file-upload"
        multiple={false}
        accept=".zip,.rar"
        onChange={handleChange}
        onClick={(e: any) => {
          e.target.value = null
        }}
      />
      <label
        id="label-file-upload"
        htmlFor="input-file-upload"
        data-active={Boolean(fileName) && className === '' ? '' : className}
      >
        {Boolean(fileName) ? (
          <span className="file-name">{fileName}</span>
        ) : (
          <div className="title-upload-file">
            <div className="title-upload-file__header">
              <img src="/images/icons/ic-upload.png" alt="upload-file" />
              <span>
                Tải file câu hỏi tại đây hoặc <a>Tìm kiếm trong thư mục</a>
              </span>
            </div>
            <span>( Hỗ trợ file zip hoặc rar )</span>
          </div>
        )}
      </label>
    </div>
  )
}
