import { useContext, useState } from 'react'

import dayjs from 'dayjs'
import { useRouter } from 'next/dist/client/router'
import { Button, Modal } from 'rsuite'

import useExportExcelListIDs from '@/hooks/useExportExcelListIDs'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { WrapperContext } from 'interfaces/contexts'
import { DefaultPropsType } from 'interfaces/types'
import { useSession } from 'next-auth/client'
import { userInRight } from '@/utils/index'
import { USER_ROLES } from '@/interfaces/constants'
import RightUpIcon from '@/assets/icons/right-up-arrow.svg'
import useTranslation from "@/hooks/useTranslation";
interface PropsType extends DefaultPropsType {
  isActive?: boolean
}

export const ShareLinkModal = ({
  className = '',
  isActive,
  style,
}: PropsType) => {
  const { t, locale, subPath } = useTranslation()
  const router = useRouter()
  const [session] = useSession()
  const user: any = session.user
  const isAdmin = user?.is_admin === 1
  const { handleExportExcel} = useExportExcelListIDs()
  const isSystem = userInRight([USER_ROLES.Operator],session)
  const { globalModal } = useContext(WrapperContext)

  const id = globalModal.state?.content?.id || null
  const title = globalModal.state?.content?.title || null
  const dataShare = globalModal.state?.content?.dataShare || {}
  const userId = globalModal.state?.content?.userId || null

  const [isShowNoti, setIsShowNoti] = useState(false)
  const getShareUrl = () => {
    const protocol = window.location.protocol
    const hostname = window.location.hostname
    const port = hostname === 'localhost' ? `:${window.location.port}` : ''
    return `${protocol}//${hostname}${port}${subPath}/practice-test/${userId}===${id}`
  }

  const handleModalClose = () => {
    if (globalModal.state?.content?.onClose){
        globalModal.state?.content?.onClose()
    } 
    globalModal.setState(null)
  }

  if (!id || !userId) return <></>

  return (
    <Modal
      className={`o-share-link-modal ${className}`}
      open={isActive}
      style={style}
      backdrop={false}
      onClose={handleModalClose}
    >
      <Modal.Body>
        <div className="modal-header">
          {title && <h5 className="modal-title">{title}</h5>}
          {dataShare && (
            <ul>
              <li className="modal-description">
                <span className="span-title">{t('common')['unit-test-name']}: </span>
                <span>{dataShare?.name}</span>
              </li>
              <li className="modal-description">
                <span className="span-title">{t('drawer-filter-unit-test')['tag']}: </span>
                <span>
                  {dataShare?.unit_type === 0 ? t('unit-type')['practice'] : t('unit-type')['exam']}
                </span>
              </li>
              <li className="modal-description">
                <span className="span-title">{t('common')['start-date']}: </span>
                <span>{dayjs(dataShare?.start_date).format('DD/MM/YYYY HH:mm')}</span>
              </li>
            </ul>
          )}
        </div>
        <div
          className="modal-content"
          onClick={() => {
            navigator.clipboard.writeText(getShareUrl())
            setIsShowNoti(true)
          }}
        >
          <InputWithLabel
            className="input-copy"
            label="URL"
            type="text"
            defaultValue={getShareUrl()}
            readOnly={true}
            iconPlacement="end"
            icon={<img src="/images/icons/ic-copy-active.png" alt="icon" />}
            toolTip={t('unit-test-create')['copy-link']}
          />
          {isShowNoti && <span className="modal-noti">{t('unit-test-create')['copy']}</span>}
        </div>
        <div className='wrapper-btn'>
          {!isSystem && <div className='modal-send-email'>
            <Button
                appearance="primary"
                color="orange"
                onClick={() => 
                  globalModal.setState({
                    id: 'send-email',
                    content: getShareUrl()
                  })}
              >
                <RightUpIcon/>
                <span>{t('unit-test-create')['action-send-mail']}</span>
              </Button>
          </div>}
          {isAdmin && (
            <div className='btn-export-excel'>
              <Button onClick={() => handleExportExcel(user, id)}>
                {t('table-action-popover')['export-list-id']}
              </Button>
            </div>
          )}
          <div className={`modal-footer ${!isSystem && router.query.mode !== 'edit' && router.query.m !== 'mine' ? '' : 'no-back'}`}>
            {(!isSystem && router.query.mode !== 'edit' && router.query.m !== 'mine') && <Button
              appearance="primary"
              color="blue"
              style={{ backgroundColor: '#e4e4e4', color: '#8A8A8A' }}
              onClick={handleModalClose}
            >
              {t('common')['close']}
            </Button>}
              <Button
                appearance="primary"
                color="blue"
                onClick={() => router.push(`/unit-test${isSystem ? '' : '?m=mine'}`)}
              >
                {isSystem ? t('unit-test-create')['back'] : t('left-menu')['my-test-bank']}
              </Button>
          </div>
        </div>
        
      </Modal.Body>
    </Modal>
  )
}
