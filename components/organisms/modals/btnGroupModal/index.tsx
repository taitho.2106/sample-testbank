import { useContext, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import { Button, Modal, Tooltip, Whisper } from 'rsuite'

import { USER_ROLES } from 'interfaces/constants'
import { WrapperContext } from 'interfaces/contexts'
import { DefaultPropsType } from 'interfaces/types'
import { userInRight } from 'utils'

interface PropsType extends DefaultPropsType {
  isActive?: boolean
}

export const BtnGroupModal = ({
  className = '',
  isActive,
  style,
}: PropsType) => {
  const [session] = useSession()

  const { globalModal } = useContext(WrapperContext)
  const defaultIsDisabledMedia = globalModal.state?.isDisabledMedia || true
  const onQuestionUpload = globalModal.state?.onQuestionUpload
  const onMediaUpload = globalModal.state?.onMediaUpload

  const [isDisabledMedia, setIsDisabledMedia] = useState(defaultIsDisabledMedia)

  const handleModalClose = () => {
    globalModal.setState(null)
  }

  return (
    <Modal
      className={`o-btn-group-modal ${className}`}
      open={isActive}
      style={style}
      onClose={handleModalClose}
    >
      <Modal.Body>
        <div className="o-btn-group-modal__container">
          <div className="title">Tải lên</div>
          <Whisper
            placement="top"
            controlId="get-questions"
            trigger="hover"
            speaker={
              <Tooltip>
                <span>Nhấn để tải danh sách câu hỏi lên hệ thống.</span>
                <br />
                <span>Lưu ý: chỉ được chọn file .xlxs, .csv</span>
              </Tooltip>
            }
          >
            <Button
              className="upload-btn"
              onClick={() => {
                onQuestionUpload()
                setIsDisabledMedia(false)
              }}
            >
              Tải danh sách câu hỏi
            </Button>
          </Whisper>
          <span className="break-sentence">
            Tải câu hỏi lên để bắt đầu tải đa phương tiện
          </span>
          <Whisper
            placement="top"
            controlId="control-id-hover"
            trigger="hover"
            speaker={
              <Tooltip>
                Nhấn để tải hình ảnh, video, audio lên hệ thống. Lưu ý: Chỉ được
                chọn file .zip
              </Tooltip>
            }
          >
            <Button
              className="upload-btn"
              disabled={isDisabledMedia}
              onClick={onMediaUpload}
            >
              Tải đa phương tiện
            </Button>
          </Whisper>
            {/* btn dùng để Test Modal Thông báo */}
          <Button 
            className="upload-result"
            onClick={() =>
              globalModal.setState({
                id: 'upload-question-result'

              })
            }
          >
            Thông báo Kết quả (test...)
          </Button>
        </div>
      </Modal.Body>
    </Modal>
  )
}
