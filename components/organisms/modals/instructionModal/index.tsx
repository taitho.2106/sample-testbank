import { useContext, useState } from 'react'

import { useRouter } from 'next/dist/client/router'
import Carousel from 'nuka-carousel'
import { Modal } from 'rsuite'


import { WrapperContext } from 'interfaces/contexts'
import { DefaultPropsType } from 'interfaces/types'

interface PropsType extends DefaultPropsType {
  isActive?: boolean
}

export const InstructionModal = ({
  className = '',
  isActive,
  style,
}: PropsType) => {
  const router = useRouter()

  const [slideIndex, setSlideIndex] = useState(0)
  const [showHand, setShowHand] = useState(true)
  // const [checked, setChecked] = useState(false)

  const { globalModal } = useContext(WrapperContext)

  const arrImages = [
    {uri: '/images/instructions/extra1.png', left: '47%', bottom: '39%', rotate: true, isHide: true},
    {uri: '/images/instructions/extra2.png', left: '47%', bottom: '39%', rotate: true, isHide: true},
    {uri: '/images/instructions/extra3.png', left: '47%', bottom: '39%', rotate: true, isHide: true},
    {uri: '/images/instructions/extra4.png', left: '47%', bottom: '39%', rotate: true, isHide: true},
    {uri: '/images/instructions/extra5.png', left: '47%', bottom: '39%', rotate: true, isHide: true},
    {uri: '/images/instructions/extra6.png', left: '47%', bottom: '39%', rotate: true, isHide: true},
    {uri: '/images/instructions/image1.png', left: '47%', bottom: '39%', rotate: true},
    {uri: '/images/instructions/image2.png', left: '39%', bottom: '49%', rotate: false},
    {uri: '/images/instructions/image3.png', left: '85%', bottom: '16%', rotate: true},
    {uri: '/images/instructions/image4.png', left: '35%', bottom: '43%', rotate: true},
    {uri: '/images/instructions/image5.png', left: '80%', bottom: '46%', rotate: false},
    {uri: '/images/instructions/image6.png', left: '83%', bottom: '16%', rotate: true},
    {uri: '/images/instructions/image7.png', left: '84%', bottom: '16%', rotate: true},
    {uri: '/images/instructions/image8.png', left: '77%', bottom: '70%', rotate: true},
    {uri: '/images/instructions/image9.png', left: '82%', bottom: '17%', rotate: true},
    {uri: '/images/instructions/image10.png', left: '83%', bottom: '16%', rotate: true},
    {uri: '/images/instructions/image11.png', left: '62%', bottom: '45%', rotate: false},
    {uri: '/images/instructions/image12.png', left: '52%', bottom: '35%', rotate: true},
    {uri: '/images/instructions/image13.png', left: '30%', bottom: '65%', rotate: true, isHide: true},
    {uri: '/images/instructions/image14.png', left: '77%', bottom: '50%', rotate: true},
    {uri: '/images/instructions/image15.png', left: '30%', bottom: '65%', rotate: true, isHide: true},
  ]
  const handleModalClose = async() => {
    const ins = await localStorage.getItem('instruction')
    if (!ins) {
      localStorage.setItem('instruction', 'true')
    }
    globalModal.setState(null)
  }

  return (
    <Modal
      backdropClassName="mask-bg-popup-black-80"
      className={`o-instruction-modal ${className}`}
      open={isActive}
      style={{...style, display: "flex"}}
      onClose={() => handleModalClose()}
      size="lg"
    >
      <Modal.Body style={{maxHeight: '560px'}}>
        <div className="modal-toggle" onClick={handleModalClose}>
          <img src="/images/icons/ic-close-dark.png" alt="close" />
        </div>
        <div className="modal-content">
          <Carousel
            swiping={false}
            dragging={false}
            autoplay={true}
            slideIndex={slideIndex}
            beforeSlide={() => {
              setShowHand(false)
            }}
            afterSlide={(slIndex) => {setSlideIndex(slIndex);setShowHand(true)}}
            style={{pointerEvents: 'none'}}
            renderBottomCenterControls={({ currentSlide }) => (
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'row',
                  marginBottom: -16,
                  pointerEvents: 'visible'
                }}
              >
                {arrImages.map((item, index) => (
                  <div
                    style={{
                      marginLeft: 3,
                      cursor: 'pointer',
                      paddingTop: 10,
                      paddingBottom: 10
                    }}
                    key={index}
                    onClick={() => setSlideIndex(index)}
                  > <div style={{
                    height: 6,
                    width: 40,
                    backgroundColor:
                    index === currentSlide ? '#35B9E5' : '#C9ECF8',
                  }}
                  >
                    </div>
                  </div>
                ))}
              </div>
            )}
            renderCenterLeftControls={() => <></>}
            renderCenterRightControls={() => <></>}
          >
            {arrImages.map((item) => (
              <div className="container-item" key={item.uri}>
                <img className="img-item" src={item.uri} alt=""/>
              </div>
            ))}
          </Carousel>
        </div>
        { !arrImages[slideIndex].isHide && showHand &&
        <div className="hand-container" style={{bottom: arrImages[slideIndex].bottom, left: arrImages[slideIndex].left}}>
          <div className={`hand-icon ${arrImages[slideIndex].rotate ? 'rotate' : ''}`}>
            <img src="/images/instructions/handIcon.png" alt="handIcon"/>
          </div>
        </div>
        }
      </Modal.Body>
    </Modal>
  )
}
