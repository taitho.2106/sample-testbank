import { useContext } from "react";

import { useRouter } from "next/router";
import { Button, Modal } from "rsuite";

import { WrapperContext } from "@/interfaces/contexts";
import { DefaultPropsType } from "@/interfaces/types";


interface PropsType extends DefaultPropsType {
    isActive?: boolean
    className?: string
}
function ModalResultIntegrateExam({
    className = '',
    isActive,
}: PropsType) {
    const router = useRouter()
    const { globalModal } = useContext(WrapperContext)
    const onSubmit = globalModal?.state?.content?.onSubmit
    const handleClose = () => {
        globalModal.setState(null)
    }
    return ( 
        <>
            <Modal
                className={`r-integrate-exam-modal ${className} r-integrate-exam-modal-custom`}
                open={isActive}
                style={{
                    overflow: 'hidden',
                    maxHeight: '100%',
                    background: "rgba(39,44,54,0.3)",
                }}
                onClose={handleClose}
                backdrop={false}
            >
                <Modal.Body>
                <div className="modal-container">
                    <div className="modal-header">
                        <Button className="modal-toggle" onClick={handleClose}>
                            <img src="/images/icons/ic-close-admin.png" alt="close" />
                        </Button>
                    </div>

                    <div className="background-image">
                        <img src="/images/collections/clt-cancel-green.png" alt="title" />
                    </div>
                    <div className="title-header">
                        <span>Lưu đề thi thành công</span>
                    </div>
                    <div className="modal-action">
                        <div>
                        <Button onClick={onSubmit}>
                            <span>Sao chép URL Tích hợp</span>
                        </Button>
                        </div>
                        <div className="action-2">
                        <Button onClick={()=>{
                            router.push('/integrate-exam')
                        }}>
                            <span>Đi đến Đề thi tích hợp</span>
                        </Button>
                        </div>
                        <div>
                        <Button onClick={()=>{
                            router.push('/unit-test')
                        }}>
                            <span>Đi đến Danh sách đề thi</span>
                        </Button>
                        </div>
                    </div>
                </div>
                </Modal.Body>
            </Modal>
        </>
     );
}

export default ModalResultIntegrateExam;