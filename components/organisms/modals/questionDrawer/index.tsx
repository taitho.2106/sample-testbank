import { useContext, useState, useEffect, MouseEvent, useRef } from 'react'

import { useSession } from 'next-auth/client'
import { Collapse } from 'react-collapse'
import { FaEye } from 'react-icons/fa'
import { Button, Drawer, Pagination, Tooltip, Whisper } from 'rsuite'

import { USER_ROLES } from '@/interfaces/constants'
import { callApi } from 'api/utils'
import { CheckBox } from 'components/atoms/checkbox'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import { MultiSelectPicker } from 'components/atoms/selectPicker/multiSelectPicker'
import { ActivityPicker } from 'components/organisms/questionPreviewSection'
import useGrade from 'hooks/useGrade'
import useSeries from 'hooks/useSeries'
import { SingleTemplateContext, WrapperContext } from 'interfaces/contexts'
import {
  CERF_SELECTIONS,
  FORMAT_SELECTIONS,
  LEVEL_SELECTIONS,
  PUBLISHER_SELECTIONS,
  QUESTION_CREATED_BY,
  SKILLS_SELECTIONS,
  TASK_SELECTIONS,
  TYPES_SELECTIONS,
} from 'interfaces/struct'
import { userInRight } from 'utils'
import { filterSeriesByGrade } from 'utils/series/filterSeriesByGrade'

import { DefaultPropsType, StructType } from '../../../../interfaces/types'
import useTranslation from "@/hooks/useTranslation";
import { useRouter } from 'next/router'
import useNoti from '@/hooks/useNoti'
import { arrayToTranslateWithCode } from '@/utils/array'


interface Props extends DefaultPropsType {
  isActive: boolean
}

export const QuestionDrawer = ({ className = '', isActive, style }: Props) => {
  const { t, subPath } = useTranslation()
  const { query } = useRouter()
  
  const { globalModal }: any = useContext(WrapperContext)
  const { chosenTemplate, mode } = useContext(SingleTemplateContext)
  const { getSeries } = useSeries()
  const { getGrade } = useGrade()
  const { getNoti } = useNoti()
  const id = globalModal.state?.content?.id || null
  const replaceId = globalModal.state?.content?.replaceId || null
  const disabled = globalModal.state?.content?.isDisabled || false
  const grade = globalModal.state?.content?.grade
  const gradeRef = useRef(grade)
  const skill = globalModal.state?.content?.skill
  const defaultTotalPage = globalModal.state?.content?.data?.totalPage || 1
  const totalQuestion = globalModal.state?.content?.totalQuestion || 0
  const defaultChosenQuestions = globalModal.state?.content?.chosenQuestions || []
  const currentSectionId = globalModal.state?.content?.id || null
  const replaceExcludeId = globalModal.state?.content?.excludeId || null
  const isChangeQuesion = globalModal.state?.content?.isChangeQuesion || null

  // data
  const [session] = useSession()
  const isSystem = userInRight([USER_ROLES.Operator],session)
  const isAdmin = userInRight([], session)
  const user:any = session.user;
  const isOwner = user?.id && user.id == chosenTemplate?.state?.author;
  
  const checkEditAble = user && (isAdmin || isOwner || (chosenTemplate?.state?.scope ==0 && isSystem)) || mode=="create";

  const defaultQuestionList = globalModal.state?.content?.data?.data || []
  const [questionList, setQuestionList] = useState(defaultQuestionList || ([] as any[]))
  const [chosenList, setChosenList] = useState(defaultChosenQuestions as any[])
  // display
  const [isOpenFilterCollapse, setIsOpenFilterCollapse] = useState(false)
  const [triggerReset, setTriggerReset] = useState(false)
  // filter
  const [filterName, setFilterName] = useState('')
  const [filterSkill, setFilterSkill] = useState([] as any[])
  const temp:any[] = isSystem ? QUESTION_CREATED_BY.filter((item)=> isSystem && item.code === '0') : []
  const [filterCreatedBy, setFilterCreatedBy] = useState([...temp])
  const [filterQuestionType, setFilterQuestionType] = useState([] as any[])
  const [filterLevel, setFilterLevel] = useState([] as any[])
  const [filterCEFR, setFilterCEFR] = useState([] as any[])
  const [filterPublisher, setFilterPublisher] = useState([] as any[])
  const [filterFormat, setFilterFormat] = useState([] as any[])
  const [filterSeries, setFilterSeries] = useState([] as any[])
  const [filterType, setFilterType] = useState([] as any[])
  const [isOnly, setIsOnly] = useState(disabled?true:false)
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPage, setTotalPage] = useState(defaultTotalPage)

  const [isFilter, setIsFilter] = useState(false)

  const [listSeries, setListSeries] = useState([])
  let totalChosenSentence = 0
  if (chosenList) {
    chosenList.forEach((item) => {
      if (item?.total_question) totalChosenSentence += item.total_question
    })
  }

  const [filterBadge, setFilterBadge] = useState(0)
  const checkFilterBadge = () => {
    const length = [
      filterName,
      filterSkill.length > 0,
      !isSystem && filterCreatedBy.length > 0,
      filterQuestionType.length > 0,
      filterLevel.length > 0,
      filterCEFR.length > 0,
      filterPublisher.length > 0,
      filterFormat.length > 0,
      filterSeries.length > 0,
      filterType.length > 0,
    ].filter((item) => item === true).length
    setFilterBadge(length)
  }

  const checkExistFilter = () =>
    (filterName ||
      filterSkill.length > 0 ||
      filterCreatedBy.length > 0 ||
      filterQuestionType.length > 0 ||
      filterLevel.length > 0 ||
      filterCEFR.length > 0 ||
      filterPublisher.length > 0 ||
      filterFormat.length > 0 ||
      filterSeries.length > 0 ||
      filterType.length > 0 ||
      isOnly) &&
    isFilter
      ? true
      : false

  const collectData = (isReset: boolean, isFromChecked = false, page: number, sid: number[] = [], body: any = null) => {
    const collection: any = {
      grade,
      page: page,
      skills: filterSkill.length > 0 ? filterSkill : skill,
      sid: sid,
    }
    if (body?.skills) collection['skills'] = body.skills
    if (isFromChecked ? !isOnly : isOnly)
      collection['id'] = chosenList.length > 0 ? chosenList.map((item) => item?.id) : [-1]
    // console.log(body?.name, filterName)
    if (!isReset) {
      if (filterName || body?.name) collection['search'] = body?.name || filterName
      if ((filterCreatedBy && filterCreatedBy.length > 0) || (body?.scope && body.scope.length > 0))
        collection['scope'] = body?.scope || filterCreatedBy
      if ((filterQuestionType && filterQuestionType.length > 0) || (body?.questionType && body.questionType.length > 0))
        collection['question_type'] = body?.questionType || filterQuestionType
      if ((filterLevel && filterLevel.length > 0) || (body?.level && body.level.length > 0))
        collection['level'] = [body?.level || filterLevel]
      if ((filterCEFR && filterCEFR.length > 0) || (body?.cerf && body.cerf.length > 0))
        collection['cerf'] = body?.cerf || filterCEFR
      if ((filterPublisher && filterPublisher.length > 0) || (body?.publisher && body.publisher.length > 0))
        collection['publisher'] = body?.publisher || filterPublisher
      if ((filterFormat && filterFormat.length > 0) || (body?.format && body.format.length > 0))
        collection['format'] = body?.format || filterFormat
      if ((filterSeries && filterSeries.length > 0) || (body?.series && body.series.length > 0))
        collection['series'] = body?.series || filterSeries
      if ((filterType && filterType.length > 0) || (body?.types && body.types.length > 0))
        collection['types'] = body?.types || filterType
    }
    return collection
  }

  const handleNameChange = (val: string) => setFilterName(val)
  const handleNameSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      // handleSearchSubmit(false, false, 1, { name: val })
      e.target.blur()
    }
  }
  const handleSkillChange = (val: any[]) => {
    setFilterSkill([...val])
    // handleSearchSubmit(false, false, 1, { skills: [...val] })
  }
  const handleCreatedByChange = (val: any[]) => {
    setFilterCreatedBy([...val])
    // handleSearchSubmit(false, false, 1, { scope: [...val] })
  }
  const handleQuestionTypeChange = (val: any[]) => {
    setFilterQuestionType([...val])
    // handleSearchSubmit(false, false, 1, { questionType: [...val] })
  }
  const handleLevelChange = (val: any[]) => {
    setFilterLevel([...val])
    // handleSearchSubmit(false, false, 1, { level: [...val] })
  }
  const handleCEFRChange = (val: any[]) => {
    setFilterCEFR([...val])
    // handleSearchSubmit(false, false, 1, { cerf: [...val] })
  }
  const handlePublisherChange = (val: any[]) => {
    setFilterPublisher([...val])
    // handleSearchSubmit(false, false, 1, { publisher: [...val] })
  }
  const handleFormatChange = (val: any[]) => {
    setFilterFormat([...val])
    // handleSearchSubmit(false, false, 1, { format: [...val] })
  }
  const handleSeriesChange = (val: any) => {
    let value:any = []
    if(val){
      value = [val]
    }
    setFilterSeries(value)
    // handleSearchSubmit(false, false, 1, { series: [...val] })
  }
  const handleTypeChange = (val: any[]) => {
    setFilterType([...val])
    // handleSearchSubmit(false, false, 1, { types: [...val] })
  }

  const handleIsOnlyChange = async () => {
    setIsOnly(!isOnly)
  }

  const handleChooseRecord = (id: number) => {
    const currentChosenList = [...chosenList]
    const chosenItem = questionList.find((item: any) => item?.id === id)
    const findItem = currentChosenList.find((item: any) => item?.id === id)
    if (findItem) setChosenList([...currentChosenList.filter((item: any) => item?.id !== id)])
    else {
      if (chosenItem.deleted === 0) {
        setChosenList([...currentChosenList, chosenItem])
      }
    }
  }

  const handlePageChange = async (page: number) => {
    await handleSearchSubmit(false, false, page)
    document.getElementsByClassName('o-question-drawer__table')[0].scrollTop = 0
  }

  const handleResetFilter = () => {
    setFilterName('')
    setFilterSkill([])
    !isSystem && setFilterCreatedBy([])
    setFilterQuestionType([])
    setFilterLevel([])
    setFilterCEFR([])
    setFilterPublisher([])
    setFilterFormat([])
    setFilterSeries([])
    setFilterType([])
    setIsOnly(false)
    setTriggerReset(!triggerReset)
    setFilterBadge(0)
    setQuestionList([...defaultQuestionList])
    setTotalPage(defaultTotalPage)
    setCurrentPage(1)
  }
  const handleSearchSubmit = async (isReset: boolean, isFromChecked = false, page: number, customData: any = null) => {
    const body = collectData(
      isReset,
      isFromChecked,
      page - 1,
      chosenList.map((m) => m.id),
      customData,
    )
    setIsFilter(!isReset)
    const otherQuestions = [] as any[]
    chosenTemplate.state.sections.forEach((section: any) => {
      if (
        section?.id !== currentSectionId &&
        section?.parts &&
        section.parts[0]?.questions &&
        section.parts[0].questions.length > 0
      ) {
        section.parts[0].questions.forEach((question: any) => {
          otherQuestions.push(question?.id)
        })
      }
    })
    const response: any = await callApi('/api/questions/search', 'post', 'Token', {
      ...body,
      excludeId: replaceExcludeId ? replaceExcludeId : otherQuestions.length > 0 ? otherQuestions : null,
      total_question: replaceId ? totalQuestion : undefined,
    })
    if (response) {
      if (response?.data) {
        if (response.data.length > 0) {
          setQuestionList([...response.data])
          setCurrentPage(page)
          setTotalPage(response?.totalPage || 1)
        } else {
          if (isOnly && page > 1) handlePageChange(page - 1)
          else setQuestionList([])
        }
      } else setQuestionList([])
    }
  }
  useEffect(() => {
    const fetchData = async ()=>{
      await handleSearchSubmit(false, false, 1)
    }
    fetchData();
  }, [isOnly])
  useEffect(() => {
    const list = filterSeriesByGrade(getSeries,getGrade, grade)
    setListSeries([...list])
  }, [getSeries,grade,getGrade])
  useEffect(() => {
    checkFilterBadge()
    // const haveFilter = checkExistFilter()
    // if (!haveFilter) {
    //   handleSearchSubmit(true, false, 1)
    // } else {
    //   handleSearchSubmit(false, false, 1)
    // }
  }, [
    filterName,
    filterSkill,
    filterCreatedBy,
    filterQuestionType,
    filterLevel,
    filterCEFR,
    filterPublisher,
    filterFormat,
    filterSeries,
    filterType,
    isOnly,
  ])

  const onClickView = (e: MouseEvent<HTMLOrSVGElement>) => {
    e.stopPropagation()
    let el = (e.target as HTMLElement).parentElement
    while (el.tagName !== 'TR') {
      el = el.parentElement
    }
    if (el.classList.contains('show')) {
      el.classList.remove('show')
    } else {
      document.querySelector('.o-question-drawer__table tr.show')?.classList.remove('show')
      el.classList.add('show')
    }
  }

  const handleSubmit = () => {
    if (totalChosenSentence < totalQuestion) {
      getNoti('error', 'topCenter', 'Vui lòng chọn đủ câu hỏi để tiếp tục')
      return
    }
    if (totalChosenSentence > totalQuestion) {
      getNoti('error', 'topCenter', 'Vượt quá số lượng tổng câu hỏi của phần thi')
      return
    }
    globalModal.state?.content?.onSubmit({ id, data: chosenList, replaceId })
  }

  return (
    <Drawer
      className={`o-question-drawer ${className}`}
      full
      placement="bottom"
      open={isActive}
      style={style}
      onClose={() => globalModal.setState(null)}
    >
      <Drawer.Body>
        <Button className="o-question-drawer__toggle" onClick={() => globalModal.setState(null)}>
          <img src="/images/icons/ic-close-darker.png" alt="close" />
        </Button>
        <div className="o-question-drawer__header">
          <h5 
            className="o-question-drawer__title" 
            dangerouslySetInnerHTML={{
              __html: t(t('common')['select-questions-1'],[`${chosenTemplate.state?.name}`])
            }} 
          ></h5>
        </div>
        <div className="o-question-drawer__body">
          <div className="o-question-drawer__filter">
            <div className="__upper">
              <div className="__input-group">
                <InputWithLabel
                  className="__search-box"
                  label={t('common')['search']}
                  placeholder={t('unit-test')['drawer-search-placeholder']}
                  triggerReset={triggerReset}
                  type="text"
                  onBlur={handleNameChange}
                  onKeyUp={handleNameSubmit}
                  icon={<img src="/images/icons/ic-search-dark.png" alt="search" />}
                />
                <Whisper
                  placement="bottom"
                  trigger="hover"
                  speaker={<Tooltip>{t('common-whisper')['show-more-filter']}</Tooltip>}
                >
                  <Button
                    className="__sub-btn"
                    data-active={isOpenFilterCollapse}
                    onClick={() => setIsOpenFilterCollapse(!isOpenFilterCollapse)}
                  >
                    <div className="__badge" data-value={filterBadge} data-hidden={filterBadge <= 0}>
                      <img src="/images/icons/ic-filter-dark.png" alt="filter" />
                    </div>
                    <span>{t('common')['filter']}</span>
                  </Button>
                </Whisper>
                <Whisper
                  placement="bottom"
                  trigger="hover"
                  speaker={<Tooltip>{t('unit-test')['tooltip-reset']}</Tooltip>}
                >
                  <Button className="__sub-btn" onClick={() => handleResetFilter()}>
                    <img src="/images/icons/ic-reset-darker.png" alt="reset" />
                    <span>{t('common')['reset']}</span>
                  </Button>
                </Whisper>
                <Button className="__main-btn" onClick={() => handleSearchSubmit(false, false, 1)}>
                  <img src="/images/icons/ic-search-dark.png" alt="find" />
                  <span>{t('common')['search']}</span>
                </Button>
              </div>
              <div className="__action-group">
                {!disabled && (
                  <Button className="__action-btn" onClick={() => window.open(`${subPath}/questions/-1${query?.m ? `?m=${query.m}` : ''}`)}>
                    <img src="/images/icons/ic-plus-white.png" alt="create" />
                    <span>{t('common')['create-question']}</span>
                  </Button>
                )}
              </div>
            </div>
            <Collapse isOpened={isOpenFilterCollapse}>
              <div className="__below">
                <MultiSelectPicker
                  className="__filter-box"
                  data={QUESTION_CREATED_BY.filter((item)=> isSystem ? item.code === '0' : item).map(item => ({...item, display: t('unit-test')[item.key]}))}
                  label={t('table')['created-by']}
                  displayAll={!isSystem}
                  defaultValue={QUESTION_CREATED_BY.filter((item)=> isSystem && item.code === '0').map(item => ({...item, display: t('unit-test')[item.key]}))}
                  menuSize="lg"
                  triggerReset={!isSystem && triggerReset}
                  onChange={handleCreatedByChange}
                  disabled={isSystem}
                />
                <MultiSelectPicker
                  className="__filter-box"
                  data={TASK_SELECTIONS}
                  label={t('common')['question-type']}
                  menuSize="xl"
                  triggerReset={triggerReset}
                  onChange={handleQuestionTypeChange}
                />
                <MultiSelectPicker
                  className="__filter-box"
                  data={arrayToTranslateWithCode(LEVEL_SELECTIONS, t)}
                  label={t('common')['level']}
                  menuSize="lg"
                  triggerReset={triggerReset}
                  onChange={handleLevelChange}
                />
                <MultiSelectPicker
                  className="__filter-box"
                  data={SKILLS_SELECTIONS.filter((item: StructType) => skill.includes(item.code))}
                  label={t('common')['skill']}
                  menuSize="lg"
                  triggerReset={triggerReset}
                  onChange={handleSkillChange}
                />
                <MultiSelectPicker
                  className="__filter-box"
                  data={arrayToTranslateWithCode(CERF_SELECTIONS, t)}
                  label="CEFR"
                  menuSize="xl"
                  triggerReset={triggerReset}
                  onChange={handleCEFRChange}
                />
                <MultiSelectPicker
                  className="__filter-box"
                  data={arrayToTranslateWithCode(PUBLISHER_SELECTIONS, t)}
                  label={t('common')['publisher']}
                  triggerReset={triggerReset}
                  onChange={handlePublisherChange}
                />
                <MultiSelectPicker
                  className="__filter-box"
                  data={arrayToTranslateWithCode(FORMAT_SELECTIONS, t)}
                  label={t('common')['format']}
                  triggerReset={triggerReset}
                  onChange={handleFormatChange}
                />
                {listSeries && (
                  <SelectFilterPicker
                    placement='bottomEnd'
                    data={listSeries}
                    isMultiChoice={false}
                    inputSearchShow={true}
                    isShowSelectAll={true}
                    onChange={handleSeriesChange}
                    className="__filter-box"
                    menuSize="xxl"
                    label={t('common')['series']}
                    triggerReset={triggerReset}
                  />
                )}
                <MultiSelectPicker
                  className="__filter-box"
                  data={arrayToTranslateWithCode(TYPES_SELECTIONS, t)}
                  label={t('common')['types']}
                  menuSize="lg"
                  placement="bottomEnd"
                  triggerReset={triggerReset}
                  onChange={handleTypeChange}
                />
              </div>
            </Collapse>
            <div className="__ps">
              <CheckBox className="__checkbox" checked={isOnly} onChange={handleIsOnlyChange} />
              <span>
                {t('unit-test')['show-question']}
                <b style={totalChosenSentence !== totalQuestion ? {color: '#E92C2C'} : { color: '#35b9e5' }}>
                  {' '}
                  ({totalChosenSentence} / {totalQuestion} {t('common')['question']})
                </b>
              </span>
            </div>
          </div>
          {questionList.length > 0 ? (
            <div className="o-question-drawer__table">
              <div className={`__table ${isSystem ? '__system': ''}`}>
                <table>
                  <thead>
                    <tr>
                      <th></th>
                      <th>{isSystem ? 'ID' : t('STT')}</th>
                      {isSystem && <>
                        <th>{t('common')['question-name']}</th>
                        <th>{t('NXB')}</th>
                        <th>{t('common')['format']}</th>
                      </>}
                      <th>{t('common')['series']}</th>
                      <th>{t('common')['level']}</th>
                      <th>{t('common')['skill']}</th>
                      {isSystem && 
                      <>
                        <th>CEFR</th>
                        <th>{t('common')['group']}</th>
                        <th>{t('common')['types']}</th>
                      </>}
                      <th>{t('common')['question-type']}</th>
                      <th>{t('common')['total-question']}</th>
                      <th>{t('common')['instruction']}</th>
                      <th>{t('common')['question']}</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {questionList.map((item: any, i: number) => {
                      const question_description = (
                        item?.parent_question_description || item?.question_description
                      )?.replace(/\%s\%/g, ' ')
                      const question_text = item?.question_text
                        ?.replace(/\%(u|b|i|s)\%/g, ' ')
                        .replace(/(#|\*)/g, ' ')
                        .replace(/\[\]/g, ' ')
                      return [
                        <tr
                          data-id={item.id}
                          data-scope={item.scope}
                          key={item.id}
                          onClick={() =>
                            !disabled &&
                            ((totalQuestion && totalQuestion > totalChosenSentence) ||
                              chosenList.find((chosenItem: any) => chosenItem.id === item.id)) &&
                            handleChooseRecord(item.id)
                          }
                          className={item.deleted ? 'deleted' : ''}
                        >
                          <td style={{ height: '5.6rem' }}>
                            <div
                              style={{
                                height: '100%',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center',
                              }}
                            >
                              <CheckBox
                                toolTip={t('common-whisper')['checkbox-question-drawer']}
                                checked={chosenList.find((chosenItem: any) => chosenItem.id === item.id) ? true : false}
                                disabled={
                                  disabled ||
                                  ((!totalQuestion || totalQuestion <= totalChosenSentence) &&
                                    !chosenList.find((chosenItem: any) => chosenItem.id === item.id))
                                    ? true
                                    : false
                                }
                                onChange={() => handleChooseRecord(item.id)}
                              />
                            </div>
                          </td>
                          <td style={{ height: '5.6rem' }}>{isSystem ? item.id : i + 1 + (currentPage - 1) * 10}</td>
                          {isSystem && 
                            <>
                              <td
                                title={item?.name}
                                style={{ height: '5.6rem' }}
                              >
                                <div className="__elipsis">
                                  {item?.name || '---'}
                                </div>
                              </td>
                              <td
                                title={t('contant')[item?.publisher]}
                                style={{ height: '5.6rem' }}
                              >
                                <div className="__elipsis">
                                  {t('contant')[item?.publisher] || '---'}
                                </div>
                              </td>
                              <td
                                title={t('contant')[item?.format]}
                                style={{ height: '5.6rem' }}
                              >
                                <div className="__elipsis">
                                  {t('contant')[item?.format] ||
                                    '---'}
                                </div>
                              </td>
                            </>
                          }
                          <td
                            title={
                              getSeries.length > 0 &&
                              getSeries.find((find: StructType) => find.code == item?.series)?.display
                            }
                            style={{ height: '5.6rem' }}
                          >
                            <div className="__elipsis">
                              {(getSeries.length > 0 &&
                                getSeries.find((find: StructType) => find.code == item?.series)?.display) ||
                                t('NA')}
                            </div>
                          </td>
                          <td
                            title={t('contant')[item?.level]}
                            style={{ height: '5.6rem' }}
                          >
                            <div className="__elipsis">
                              {t('contant')[item?.level] || '---'}
                            </div>
                          </td>
                          <td
                            title={SKILLS_SELECTIONS.find((find: StructType) => find.code === item?.skills)?.display}
                            style={{ height: '5.6rem' }}
                          >
                            <div className="__elipsis">
                              {SKILLS_SELECTIONS.find((find: StructType) => find.code === item?.skills)?.display ||
                                '---'}
                            </div>
                          </td>
                          
                          {isSystem &&
                            <>
                              <td
                                title={t('contant')[item?.cerf]}
                                style={{ height: '5.6rem' }}
                              >
                                <div className="__elipsis">
                                  {t('contant')[item?.cerf] || '---'}
                                </div>
                              </td>
                              <td title={item?.group} style={{ height: '5.6rem' }}>
                                <div className="__elipsis">{item?.group || '---'}</div>
                              </td>
                              <td
                                title={t('contant')[item?.types]}
                                style={{ height: '5.6rem' }}
                              >
                                <div className="__elipsis">
                                  {t('contant')[item?.types] || '---'}
                                </div>
                              </td>
                            </>
                          }
                          <td
                            title={
                              TASK_SELECTIONS.find((find: StructType) => find.code === item?.question_type)?.display
                            }
                            style={{ height: '5.6rem' }}
                          >
                            <div className="__elipsis">
                              {TASK_SELECTIONS.find((find: StructType) => find.code === item?.question_type)?.display ||
                                '---'}
                            </div>
                          </td>
                          <td title={item?.total_question} style={{ height: '5.6rem' }}>
                            <div className="__elipsis">{item?.total_question || '---'}</div>
                          </td>
                          <td title={question_description} style={{ height: '5.6rem' }}>
                            <div className="__elipsis">{question_description || '---'}</div>
                          </td>
                          <td title={question_text} style={{ height: '5.6rem' }}>
                            <div className="__elipsis">{question_text || '---'}</div>
                          </td>
                          <td>
                            <Whisper
                              placement="left"
                              trigger="hover"
                              speaker={<Tooltip>{t('common-whisper')['question-view']}</Tooltip>}
                            >
                              <span>
                                <FaEye style={{ cursor: 'pointer' }} onClick={onClickView} />
                              </span>
                            </Whisper>
                          </td>
                        </tr>,
                        <tr key={`${item.id}_expand`} className="tr-expand">
                          <td
                            colSpan={15}
                            style={{
                              width: '100%',
                              textAlign: 'unset',
                            }}
                          >
                            <div style={{ width: '800px' }}>
                              <ActivityPicker data={item} type={item.question_type} />
                            </div>
                          </td>
                        </tr>,
                      ]
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          ) : (
            <div className="o-question-drawer__empty">
              <img
                className="__banner"
                src='/images/collections/clt-emty-result.png'
                alt="banner"
              />
              <p className="__description">
                {t('no-field-data')['search-empty']}
              </p>
              
            </div>
          )}
        </div>
        {questionList.length > 0 && (
          <div className="o-question-drawer__footer">
            <div className="__pagination">
              <Pagination
                prev
                next
                ellipsis
                size="md"
                total={totalPage * 10}
                maxButtons={10}
                limit={10}
                activePage={currentPage}
                onChangePage={(page: number) => handlePageChange(page)}
              />
            </div>
            { mode !="detail" && checkEditAble &&
              <Whisper
              placement="top"
              trigger="hover"
              speaker={
                <Tooltip
                  style={{
                    visibility: !totalQuestion || totalQuestion !== totalChosenSentence ? 'visible' : 'hidden',
                  }}
                >
                  {t('common-whisper')['total-question-count']}
                </Tooltip>
              }
            >
              <div className="__submit">
                <Button
                  className="__primary"
                  onClick={handleSubmit}
                >
                  {t('common')['save']}
                </Button>
              </div>
            </Whisper>
            }
          </div>
        )}
      </Drawer.Body>
    </Drawer>
  )
}
