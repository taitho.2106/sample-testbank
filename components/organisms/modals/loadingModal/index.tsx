import { useContext, useState } from "react";

import { Modal } from "rsuite";

import { WrapperContext } from "interfaces/contexts";

function LoadingModal({isActive,className=''}:any) {
    const {globalModal} = useContext(WrapperContext)
    const processing = globalModal.state?.processing||""
    const percent = globalModal.state?.percent||100
    const title = globalModal.state?.title || "Đang xử lý"
    const message = globalModal.state?.message || "Hệ thống đang xử lý, vui lòng không thao tác gì thêm"
    const handleOnClose = () =>{
        globalModal.setState(null)
    }
    return ( 
        <Modal
        className={`loading-modal ${className} loading-modal-custom`}
        open={isActive}
        style={{
            overflowY:'hidden',
            maxHeight:'100%',
            background: "rgba(39,44,54,0.3)",
        }}
        onClose={handleOnClose} 
        backdrop={false}
        >
            <Modal.Body>
                <div className="modal-container">
                    <div className="icon-loading">
                        <img src="/images/icon-loading-modal.png"  alt="loading"/>
                    </div>
                    <div className="background-image">
                        <img src="/images/image-background.png" alt="background"/>
                    </div>
                    <div className="title-header">
                        <span>{title}</span>
                    </div>
                    <div className="title-content">
                        <span>{message}</span>
                    </div>
                    <div className="process-bar">
                        <div className="process-bar__body">
                            <div className="processing" style={{width:`${percent}%`}}></div>
                            <span>{processing}</span>
                        </div>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
     );
}

export default LoadingModal;