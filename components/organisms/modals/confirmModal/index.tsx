import { useContext } from "react";

import { Button, Modal } from "rsuite";

import { WrapperContext } from "interfaces/contexts";
import { DefaultPropsType } from "interfaces/types";

import { confirmModalTemplates } from "./templates";
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import { PACKAGES, USER_ROLES } from "@/interfaces/constants";
import useTranslation from "@/hooks/useTranslation";
import { useSession } from "next-auth/client";
import { userInRight } from "@/utils/index";

interface PropsType extends DefaultPropsType {
    isActive?: boolean;
}

export const ConfirmModal = ({
                                 className = "",
                                 isActive,
                                 style
                             }: PropsType) => {
    const { t } = useTranslation()
    const { globalModal } = useContext(WrapperContext);
    const { dataPlan } = useCurrentUserPackage()
    const [session] = useSession()
    const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const isCreateUnitTest = (dataPlan?.code !== PACKAGES.BASIC.CODE && isTeacher) || isSystem
    const type = globalModal.state?.type || "";
    const translationWithType = t(`modal-${type}`)
    const findingData = confirmModalTemplates.find(
        (item: any) => item.type === type
    );

    const handleClose = () => {
        if (globalModal.state?.content?.onClose) {
            globalModal.state.content.onClose();
        }
        globalModal.setState(null);
    };

    const handleSubmit = () => {
        if (globalModal.state?.content?.onSubmit) {
            globalModal.state.content.onSubmit();
        }
        globalModal.setState(null);
    };

    if (!type || !findingData) return <></>;

    return (
        <Modal
            className={`o-confirm-modal ${className} ${type === "delete-unit-test" ? "new-modal" : ""}`}
            open={isActive}
            style={{ overflow: "hidden", ...style }}
            onClose={handleClose}
        >
            <Modal.Body>
                {type === "delete-unit-test" ? (
                    <div className="new-modal-container">
                        <span className="new-modal-header">{translationWithType['title']}</span>
                        <div className="new-modal-content">
                            <span className="new-modal-content-title">{translationWithType['note']}</span>
                            <ul className="new-modal-content-des">
                                <li>{translationWithType['description-1']}</li>
                                <li>{translationWithType['description-2']}</li>
                            </ul>
                            <span className="new-modal-content-noti">{translationWithType['warm']}</span>
                        </div>
                        <div className="new-modal-button">
                            <Button className="new-modal-button-cancel"
                                    onClick={handleClose}>{globalModal.state?.content?.closeText || t('common')['cancel']}</Button>
                            <Button className="new-modal-button-submit"
                                    onClick={handleSubmit}>{globalModal.state?.content?.submitText || t('common')['submit-btn']}</Button>
                        </div>
                    </div>
                    ): (
                    <>
                        <div className="modal-toggle" onClick={handleClose}>
                            <img src="/images/icons/ic-close-dark.png" alt="close" />
                        </div>
                        <div className="modal-content">
                            <div className="modal-banner">
                                <img src={findingData.banner} alt="delete" />
                            </div>
                            <h5 className="modal-title">{globalModal.state?.content?.isCopy ? translationWithType[findingData.titleCopy] : translationWithType[findingData.title]}</h5>
                            <p className="modal-description"
                               dangerouslySetInnerHTML={{
                                   __html: (['create-template-success','update-template-success'].includes(type) && !isCreateUnitTest) ?
                                        translationWithType['description-not-create-unittest']
                                    : translationWithType[findingData.description]
                            }}></p>
                        </div>
                        <div className="modal-footer">
                            {!(['create-template-success','update-template-success'].includes(type) && !isCreateUnitTest) &&
                                <Button
                                    className="secondary"
                                    appearance="primary"
                                    color="blue"
                                    onClick={handleSubmit}
                                >
                                    {globalModal.state?.content?.submitText || t("common")['submit-btn']}
                                </Button>
                            }
                            <Button
                                className="primary"
                                appearance="primary"
                                color="blue"
                                onClick={handleClose}
                            >
                                {globalModal.state?.content?.closeText || t('common')['cancel']}
                            </Button>
                        </div>
                    </>
                    )}

            </Modal.Body>
        </Modal>
    );
};
