export const confirmModalTemplates = [
  {
    banner: '/images/collections/clt-change-grade.png',
    description: 'description',
    title: 'title',
    type: 'change-grade',
  },
  {
    banner: '/images/collections/clt-change-grade.png',
    description: 'description',
    title: 'title',
    type: 'change-skill',
  },
  {
    banner: '/images/collections/clt-delete-section.png',
    description: 'description',
    title: 'title',
    type: 'delete-skill',
  },
  {
    banner: '/images/collections/clt-cancel.png',
    description: "description",
    title: 'title',
    type: 'delete-unit-test',
  },
  {
    banner: '/images/collections/clt-cancel.png',
    description: "description",
    title: 'title',
    type: 'delete-template',
  },
  {
    banner: '/images/collections/clt-back.png',
    description: "description",
    title: 'title',
    type: 'move-to-first-step',
  },
  {
    banner: '/images/collections/clt-cancel.png',
    description: 'description',
    title: 'title',
    type: 'delete-question',
  },
  // {
  //   banner: '/images/collections/clt-cancel.png',
  //   description: (
  //     <>
  //       Bạn có chắc chắn muốn xoá người dùng này.
  //       <br />
  //       Hành động này không thể hoàn tác
  //     </>
  //   ),
  //   title: 'Xoá',
  //   type: 'delete-user',
  // },
  {
    banner: '/images/collections/clt-cancel-green.png',
    description: 'description',
    title: 'title',
    type: 'update-template-success',
  },
  {
    banner: '/images/collections/clt-cancel-green.png',
    description: 'description',
    title: 'title',
    titleCopy: 'title-copy',
    type: 'create-template-success',
  },
  {
    banner: '/images/collections/clt-cancel-green.png',
    description: 'description',
    title: 'title',
    type: 'update-unittest-success',
  },
  {
    banner: '/images/collections/clt-cancel-green.png',
    description: 'description',
    title: 'title',
    titleCopy: 'title-copy',
    type: 'create-question',
  },
  {
    banner: '/images/collections/clt-cancel-green.png',
    description: 'description',
    title: 'title',
    type: 'update-question',
  },
  {
    banner: '/images/collections/clt-cancel.png',
    description: 'description',
    title: 'title',
    type: 'cancel-unit-test',
  },
  {
    banner: '/images/collections/clt-cancel.png',
    description: 'description',
    title: 'title',
    type: 'cancel-unit-test-update',
  },
]
