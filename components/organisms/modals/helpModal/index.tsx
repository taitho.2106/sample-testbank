import { useState, useEffect } from 'react'

import Carousel from 'nuka-carousel'
import { Modal, Button, Tooltip, Whisper } from 'rsuite'

import { DefaultPropsType } from '../../../../interfaces/types'

interface Props extends DefaultPropsType {
    isActive?: boolean
    closeModal: () => void
    userGuideData: any
}

export const HelpModal = ({
    className = '',
    isActive,
    closeModal,
    userGuideData,
    style,
}: Props) => {
    const [openDetail, setOpenDetail] = useState(false)
    const [openVideo, setOpenVideo] = useState(false)
    const [check, setCheck] = useState(false)
    const [tab, setTab] = useState(1)
    const [selected, setSelected] = useState(1)
    const handleClose = () => {
        setTab(1);
        closeModal()
        setSelected(1)
    }
    const data: any = userGuideData
    // console.log('@@@@@@', data)
    const dataDocument = data.filter((item: any) => {
        return item.type_data === 2
    })

    const listDuration: any = []
    const dataVideo = data.filter((item: any) => {
        const check: boolean = item.type_data === 1
        if (check) {
        listDuration.push(0)
        }
        return check
    })
    const [videoDuration, setVideoDuration] = useState(listDuration)

    let url = ''
    let vUrl = ''
    const title = ''
    const [fileUrl, setFileUrl] = useState(url)
    const [videoUrl, setVideoUrl] = useState(vUrl)
    const [fileTitle, setFileTitle] = useState(title)

    if (dataDocument && dataDocument.length > 0) {
        url = dataDocument[0].link_file
    }
    if (dataVideo && dataVideo.length > 0) {
        vUrl = dataVideo[0].link_file
    }
    useEffect(() => {
        if (isActive && !check) {
            setFileUrl(url)
            // console.log("success");
        }
    })
    const SecondToString = (second: number) => {
        if (second) {
        return new Date(second * 1000).toISOString().substring(11, 19)
        } else {
        return '00:00:00'
        }
    }

    return (
        <>
        <Modal
            className={`help-modal ${className} help-modal-custom`}
            size="lg"
            open={isActive && openDetail !==true && openVideo !== true}
            style={{
                overflow: 'hidden',
                // maxHeight: '100%',
                background: 'rgba(39,44,54,0.3)'
            }}
            onClose={handleClose} 
            backdrop={false}
        >
        <Modal.Body style={{ overflow: 'hidden' }}>
            <div className="modal-header">
                {' '}
                Hướng dẫn sử dụng
                <Button className="option-btn" onClick={handleClose}>
                <img src="/images/icons/ic-close-help.png" alt="close" />
                </Button>
            </div>
            <div className="modal-sub-content">
                <div className="modal-menu">
                <Button
                    data-active={tab === 1 ? true : false}
                    className="btn-select"
                    onClick={() => setTab(1)}
                >
                    Tài liệu hướng dẫn
                </Button>
                <Button
                    data-active={tab === 2 ? true : false}
                    className="btn-select"
                    onClick={() => setTab(2)}
                >
                    Video Hướng dẫn
                </Button>
                </div>
                <div style={{ height: 'calc(100% - 75px' }}>
                <div
                    style={{
                    height: '100%',
                    overflow: 'hidden',
                    display: tab == 1 ? 'block' : 'none',
                    }}
                >
                    {dataDocument && dataDocument.length > 0 ? (
                    <div className="document-guide">
                        <nav className="nav-main" id='scroll-style'>
                        <div className="nav-item">
                            <ul>
                            {dataDocument?.map((guide: any, index: number) => {
                                return (
                                    <button
                                        onClick={() => {
                                        setCheck(true)
                                        setFileTitle(guide.title)
                                        setFileUrl(guide.link_file)
                                        setSelected(index + 1)
                                        }}
                                        key={`document${index}`}
                                        data-active={
                                        selected === index + 1 ? true : false
                                        }
                                    >
                                        <div className="btn-elipsis">{guide.title}</div>
                                </button>
                                )
                            })}
                            </ul>
                        </div>
                        </nav>
                        <div className="nav-content">
                        <Button
                            className="zoom-in-toggle"
                            onClick={() => setOpenDetail(true)}
                        >
                            <img src="/images/icons/ic-zoom-in.png" alt="zoom-in" />
                        </Button>
                        {}
                        {fileUrl && selected ? (
                            <iframe
                            src={fileUrl}
                            width={'100%'}
                            height={'100%'}
                            style={{
                                border: 0,
                                height: '100%',
                                borderRadius: 'inherit',
                                position: 'relative',
                            }}
                            
                            />
                        ) : (
                            <div className="no-data-user-guide">
                        <img src="/images/emty-result.png" alt=""></img>
                        <span>Không có dữ liệu</span>
                    </div>
                        )}
                        </div>
                    </div>
                    ) : (
                    <div className="no-data-user-guide">
                        <img src="/images/emty-result.png" alt=""></img>
                        <span>Không có dữ liệu</span>
                    </div>
                    )}
                </div>

                <div
                    style={{
                    height: '100%',
                    display: tab == 2 ? 'block' : 'none',
                    }}
                >
                    {dataVideo && dataVideo.length > 0 ? (
                    <div className="video-guide">
                        {dataVideo?.map((guide: any, index: number) => {
                        return (
                            <div key={index} className="video-section"
                                onClick={() => {
                                    setFileTitle(guide.title)
                                    setVideoUrl(guide.link_file)
                                    setOpenVideo(true)
                                }}
                            >
                                <video
                                    preload='metadata'
                                    src={guide.link_file}
                                    id="outer"
                                    onLoadedMetadata={(e) => {
                                    const list: any = [...videoDuration]
                                    list[index] = (e.target as any).duration
                                    setVideoDuration(list)
                                    }}
                                />    
                                <div className="video-element"> 
                                    <div className="video-title">
                                        <Button className="play-btn">
                                            <img src="/images/icons/ic-play-btn.png" alt="play"/>
                                        </Button>
                                        <Whisper placement="topStart" trigger="hover" speaker={<Tooltip>{guide.title}</Tooltip>}>
                                        <div className='vid-name'>{guide.title}</div>
                                        </Whisper> 
                                    </div>
                                    
                                    <Whisper placement="topStart" trigger="hover" speaker={<Tooltip>{SecondToString(videoDuration[index])}</Tooltip>}>   
                                    <div className='vid-duration'>
                                        {SecondToString(videoDuration[index])}
                                    </div>
                                    </Whisper>
                                </div>
                            
                            </div>
                        )
                        })}
                    </div>
                    ) : (
                    <div className="no-data-user-guide">
                        <img src="/images/emty-result.png" alt=""></img>
                        <span>Không có dữ liệu</span>
                    </div>
                    )}
                </div>
                </div>
            </div>
        </Modal.Body>
        </Modal>

        {/* Document Detail */}
        <Modal
            size="lg"
            className={`document-detail ${className} document-detail-custom`}
            open={openDetail}
            style={{
                background: 'rgba(39,44,54,0.3)'
            }}
            onClose={() => setOpenDetail(false)}
            backdrop={false}
        >
        <Modal.Body style={{ overflow: 'hidden' }}>
            <div className="modal-detail-header">
                <Button className="option-btn" onClick={() => setOpenDetail(false)}>
                <img src="/images/icons/ic-back-arrow.png" alt="back" />
                </Button>
                {fileTitle}
                <Button
                className="option-btn"
                onClick={() => {
                    setOpenDetail(false)
                }}
                >
                <img src="/images/icons/ic-close-help.png" alt="close" />
                </Button>
            </div>
            <div className="modal-document-content">
                <div style={{ height: '100%', width: '100%' }}>
                {fileUrl ? (
                    <iframe
                    src={fileUrl}
                    width="100%"
                    height="100%"
                    style={{ border: 0 }}
                    />
                ) : (
                    <div className="no-data-user-guide">
                        <img src="/images/emty-result.png" alt=""></img>
                        <span>Không có dữ liệu</span>
                    </div>
                )} 
                </div>
                <div>
                    <Button className="zoom-out-toggle" onClick={() => setOpenDetail(false)}>
                        <img src="/images/icons/ic-zoom-out.png" alt="zoom-out" />
                    </Button>
                </div>
            </div>
        </Modal.Body>
        </Modal>

        {/* Video detail Modal */}
        <Modal
            overflow={false}
            size="lg"
            className={`video-detail ${className} video-detail-custom`}
            open={openVideo}
            style={{
                background: 'rgba(39,44,54,0.3)'
            }}
            onClose={() => setOpenVideo(false)}
            backdrop={false}
        >
        <Modal.Body >
            <div className="modal-detail-header">
                <Button className="option-btn" onClick={() => setOpenVideo(false)}>
                    <img src="/images/icons/ic-back-arrow.png" alt="back" />
                </Button>
                {fileTitle}
                <Button
                    className="option-btn"
                    onClick={() => { setOpenVideo(false) ; handleClose()}}
                >
                    <img src="/images/icons/ic-close-help.png" alt="close" />
                </Button>
            </div>
            <div className='modal-video-content'>
                <div className="select-vid">
                    <video id="select" src={videoUrl} controls preload='metadata' />
                </div>
                <div className="play-list">
                <div className='text'>
                    Danh sách Video
                </div>
                <div >
                    <Carousel
                    defaultControlsConfig={{
                        pagingDotsStyle:{display:"none"}}     
                    }
                    swiping={false}
                    dragging={false}
                    cellSpacing={20}
                    wrapAround={false}
                    scrollMode="remainder"
                    slidesToShow={2.75}
                    renderCenterLeftControls={({ previousSlide }) => (
                        <Button onClick={previousSlide} className="vid-toggle__right">
                            <img src="/images/icons/ic-right-ctrl.png" alt="previous"/>
                        </Button>
                    )}
                    renderCenterRightControls={({ nextSlide }) => (
                        <Button onClick={nextSlide} className="vid-toggle__left">
                            <img src="/images/icons/ic-left-ctrl.png" alt="next" />
                        </Button>
                    )}
                    >
                    {dataVideo?.map((guide: any, index: number) => (
                        <div key={index} className="item-slide">
                            <div className="video-component" 
                                onClick={() => {
                                    setFileTitle(guide.title)
                                    setVideoUrl(guide.link_file)
                                }}
                            >
                                <video
                                preload='metadata'
                                id="outer"
                                onLoadedMetadata={(e) => console.log}
                                src={guide.link_file}
                                />
                                <div className="video-element-mod">
                                <Whisper placement="topStart" trigger="hover" speaker={<Tooltip>{guide.title}</Tooltip>}> 
                                    <div className='text-name'>{guide.title}</div>
                                </Whisper>
                                    <div className='vid-duration'>{SecondToString(videoDuration[index])}</div>
                                </div>
                            </div>
                            
                        </div>
                    ))}
                    </Carousel>
                </div>
                </div>
            </div>
        </Modal.Body>
        </Modal>
    </>
  )
}
