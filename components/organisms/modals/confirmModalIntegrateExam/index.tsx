import { useContext } from "react";

import { Button, Modal } from "rsuite";

import { WrapperContext } from "@/interfaces/contexts";
import { DefaultPropsType } from "@/interfaces/types";

import { confirmModalIntegrateExam } from "./templatesIntegrateExcam";

interface PropsType extends DefaultPropsType {
    isActive?: boolean
    className?: string
    style?: any
}
function ConfirmModalIntegrateExam({
    className = '',
    isActive,
}: PropsType) {
    const { globalModal } = useContext(WrapperContext)

    const type = globalModal.state?.type || ''
    const color = globalModal.state?.color || { colorSubmit: '#5DB8D8', colorClose: '#738396' }
    const third_party_display = globalModal.state?.content?.third_party_display
    const findingData = confirmModalIntegrateExam.find(
        (item: any) => item.type === type,
    )
    const handleClose = () => {
        if (globalModal.state?.content?.onClose) {
            globalModal.state.content.onClose()
        }
        globalModal.setState(null)
    }
    const handleSubmit = () => {
        if (globalModal.state?.content?.onSubmit) {
            globalModal.state.content.onSubmit()
        }
        globalModal.setState(null)
    }

    if (!type || !findingData) return <></>
    
    const descriptionA = [
        `Đề thi tích hợp với đối tác ${third_party_display} đã tồn tại trên hệ thống.`,
        'Bạn vui lòng sử dụng đề thi tích hợp đã tồn tại hoặc chọn đối tác khác.'
    ]
    return (
        <Modal
            className={`integrate-exam-modal ${className} integrate-exam-modal-custom`}
            open={isActive}
            style={{
                overflow: 'hidden',
                maxHeight: '100%',
                background: "rgba(39,44,54,0.3)",
            }}
            onClose={handleClose}
            backdrop={false}
        >
            <Modal.Body>
                <div className="modal-container">
                    <div className="modal-header">
                        <Button className="modal-toggle" onClick={handleClose}>
                            <img src="/images/icons/ic-close-admin.png" alt="close" />
                        </Button>
                    </div>

                    <div className="background-image">
                        <img src={findingData.banner} alt={findingData.title} />
                    </div>
                    <div className="title-header">
                        <span>{findingData.title}</span>
                    </div>
                    {type === "create-integrate-fail" && (<div className="title-content">
                        {findingData.description && findingData.description.map((item: any, index: number) => (<span key={index + 1}>{item}</span>))}
                    </div>)}
                    {type === 'integrate-duplicate'  && (<div className="title-content">
                        {descriptionA.map((item: any, index: number) => (<span key={index + 1}>{item}</span>))}
                    </div>)}
                    {type !== "create-integrate-fail" && type !== 'integrate-duplicate' && (
                    <div className="title-content-action">
                            {findingData.description && findingData.description.map((item:any, index:number)=>(
                                <div key={index+1} className="title-content-action__item">
                                    <div>
                                        <img src="/pt/images/icons/ic-notifi.svg" alt="notifi" />
                                    </div>
                                    <span>{item}</span>
                                </div>
                            ))}
                    </div>
                    )}
                    <div className="modal-action">
                        <Button
                            className="btn-submit"
                            style={{ background: `${color.colorSubmit}` }}
                            onClick={handleSubmit}
                        >
                            {globalModal.state?.content?.submitText || 'Xác nhận'}
                        </Button>
                        <Button
                            className="btn-close"
                            style={{ background: `${color.colorClose}` }}
                            onClick={handleClose}
                        >
                            {globalModal.state?.content?.closeText || 'Hủy'}
                        </Button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    );
}

export default ConfirmModalIntegrateExam;