
export const confirmModalIntegrateExam = [
    {
        banner: '/images/collections/clt-create-integrate-fail.png',
        description: [
            "Vui lòng chọn chương trình cho đề thi trước khi tạo đề thi tích hợp."
        ],
        title: 'Tạo đề thi tích hợp thất bại',
        type: 'create-integrate-fail',
    },
    {
        banner: '/images/collections/clt-create-integrate-fail.png',
        description: [
            "Vui lòng chọn khối lớp cho đề thi trước khi tạo đề thi tích hợp."
        ],
        title: 'Tạo đề thi tích hợp thất bại',
        type: 'create-integrate-fail-for-grade',
    },
    {
        banner: '/images/collections/clt-create-integrate-fail.png',
        description: [
            "Vui lòng chọn khối lớp và chương trình cho đề thi trước khi tạo đề thi tích hợp."
        ],
        title: 'Tạo đề thi tích hợp thất bại',
        type: 'create-integrate-fail-for-grade-and-series',
    },
    {
        banner: '/images/collections/clt-integrate-delete.png',
        description: [
            'Việc xóa đề thi tích hợp với đối tác có thể làm gián đoạn chức năng xem đề thi từ hệ thống của đối tác.',
            'Hành động này không thể hoàn tác.'
        ],
        title: 'Bạn chắc chắn muốn xóa đề thi này?',
        type: 'integrate-delete',
    },
    {
        banner: '/images/collections/clt-integrate-edit.png',
        description: [
            'Việc chỉnh sửa đề thi tích hợp với đối tác có thể làm gián đoạn chức năng xem đề thi từ hệ thống của đối tác.',
            'Hành động này không thể hoàn tác.'
        ],
        title: 'Bạn chắc chắn muốn chỉnh sửa đề thi này?',
        type: 'integrate-edit',
    },
    {
        banner: '/images/collections/clt-integrate-delete.png',
        description: [
        ],
        title: 'Tạo đề thi tích hợp thất bại',
        type: 'integrate-duplicate',
    },
]