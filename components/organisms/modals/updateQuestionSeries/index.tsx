import { useContext, useState, useEffect } from 'react'

import { Button, Drawer, Pagination, toaster } from 'rsuite'

import { paths } from 'api/paths'
import { AlertNoti } from 'components/atoms/alertNoti'
import useGrade from 'hooks/useGrade'
import useSeries from 'hooks/useSeries'
import { SingleTemplateContext } from 'interfaces/contexts'
import {
  LEVEL_SELECTIONS,
  SKILLS_SELECTIONS,
  TASK_SELECTIONS,
} from 'interfaces/struct'
import api from 'lib/api'

import { DefaultPropsType, StructType } from '../../../../interfaces/types'
interface PropsType extends DefaultPropsType {
  isActive?: boolean
  className?: string
  setIsActive: (key: boolean) => void
  style?: any
}
export const UpdateQuestionSeries = ({
  className = '',
  isActive = true,
  setIsActive,
  style,
}: PropsType) => {
  const { chosenTemplate } = useContext(SingleTemplateContext)
  const { getSeries } = useSeries()
  const { getGrade } = useGrade()
  const [testInfo,setTestInfo] = useState(null);
  const id = chosenTemplate.state?.id || null
  const [questionList, setQuestionList] = useState([])
  const [currentPage, setCurrentPage] = useState(1)
  const [totalRecords, setTotalRecords] = useState(1)

  const getSeriesName = (code: string) => {
    const series = getSeries.find((m) => m.code == code)
    if (series) return series.display
    return ''
  }
  const getGradeName = (code: string) => {
    const grade = getGrade.find((m) => m.code == code)
    if (grade) return grade.display
    return '---'
  }
  const handleSearchSubmit = async (pageIndex: number) => {
    const response = await api.get(
      `${paths.api_unit_test_questions}?id=${id}&page=${pageIndex}&limit=${10}`,
    )
    if (response && response.status == 200) {
      const dataRes:any = response.data;
      if (dataRes?.data) {
        setQuestionList(dataRes.data)
        setCurrentPage(dataRes.currentPage)
        setTotalRecords(dataRes.totalRecords)
      }
    }
  }
  const getInfoUnitTest = async ()=>{
    const response = await api.get(
      `${paths.api_unit_test}/${id}?mode=info`,
    )
    if (response && response.status == 200) {
      const dataRes:any = response.data;
      if (dataRes?.data) {
        setTestInfo(dataRes.data)
      }
    }
  }
  useEffect(() => {
      getInfoUnitTest();
      handleSearchSubmit(0)
  }, [isActive])
  const handlePageChange = async (page: number) => {
    await handleSearchSubmit(page - 1)
    document.getElementsByClassName(
      'o-unit-test-question-update-series-drawer__table',
    )[0].scrollTop = 0
  }
  const formatDate = (date: Date) => {
    if (!date) return ''
    const dd = `0${date.getDate()}`.slice(-2)
    const mm = `0${date.getMonth() + 1}`.slice(-2)
    const yy = `${date.getFullYear()}`
    const h = `0${date.getHours()}`.slice(-2)
    const m = `0${date.getMinutes()}`.slice(-2)
    return `${dd}/${mm}/${yy}    ${h}:${m}`
  }
  const updateQuestionSeriesByUnitTestId=async(id:any)=>{
    const response = await api.post(
      `${paths.api_unit_test_questions}/update-series?id=${id}`,{}
    )
    if(response){
      const dataRes:any = response.data;
      if (response.status == 200) {
        await handlePageChange(1)
      }else{
        const key = toaster.push(
          <AlertNoti type={'error'} message={dataRes?.message} />,
          {
            placement: 'topEnd',
          },
        )
        setTimeout(() => toaster.remove(key), 2000)
      }
    }
    
  }
  return (
    <Drawer
      className={`o-unit-test-question-update-series-drawer ${className}`}
      full
      placement="bottom"
      open={isActive}
      style={style}
      onClose={() => setIsActive(false)}
    >
      <Drawer.Body>
        <Button
          className="o-unit-test-question-update-series-drawer__toggle"
          onClick={() => setIsActive(false)}
        >
          <img src="/images/icons/ic-close-admin.png" alt="close" />
        </Button>
        <div className="o-unit-test-question-update-series-drawer__header">
          <h6 className="o-unit-test-question-update-series-drawer__title">
            Danh sách câu hỏi
          </h6>
        </div>
        <div className="o-unit-test-question-update-series-drawer__body">
          {testInfo && (
            <div className="o-unit-test-question-update-series-drawer__info">
              <div className="__content __content-left">
                <div className="__line">
                  <span>Tên đề thi:</span>
                  <span className="text-hight-light">{testInfo.name}</span>
                </div>
                <div className="__line">
                  <span>Khối lớp:</span>
                  <span className="text-hight-light">
                    {getGradeName(testInfo.template_level_id)}
                  </span>
                </div>
                <div className="__line">
                  <span>Loại đề:</span>
                  <span className="text-hight-light">
                    {testInfo.unit_type == '0' ? 'Luyện tập' : 'Kiểm tra'}
                  </span>
                </div>
              </div>
              <div className="__content __content-center">
                <div className="__line">
                  <span>Thời lượng:</span>
                  <span className="text-hight-light">{testInfo.time}</span>
                </div>
                <div className="__line">
                  <span>Số câu hỏi:</span>
                  <span className="text-hight-light">
                    {testInfo.total_question}
                  </span>
                </div>
                <div className="__line">
                  <span>Tổng điểm:</span>
                  <span className="text-hight-light">
                    {testInfo.total_point}
                  </span>
                </div>
              </div>
              <div className="__content __content-right">
                <div className="__line">
                  <span>Thời gian bắt đầu:</span>
                  <span className="text-hight-light" style={{whiteSpace:"pre-wrap"}}>
                    {formatDate(new Date(testInfo.start_date))}
                  </span>
                </div>
                <div className="__line">
                  <span>Thời gian kết thúc:</span>
                  <span className="text-hight-light" style={{whiteSpace:"pre-wrap"}}>
                    {formatDate(new Date(testInfo.end_date))}
                  </span>
                </div>
                <div className="__line">
                  <span>Chương trình:</span>
                  <span className="text-hight-light">
                    {getSeriesName(testInfo.series_id)}
                  </span>
                </div>
              </div>
            </div>
          )}
          {questionList.length > 0 ? (
            <div className="o-unit-test-question-update-series-drawer__table">
              <div className="__table">
                <table>
                  <thead>
                    <tr>
                      <th>STT</th>
                      <th>Khối lớp</th>
                      <th>Kỹ năng</th>
                      <th>Loại câu hỏi</th>
                      <th>Độ khó</th>
                      <th>Yêu cầu đề bài</th>
                      <th>Câu hỏi</th>
                      <th>Chương trình</th>
                    </tr>
                  </thead>
                  <tbody>
                    {questionList.map((item: any, i: number) => {
                      const question_description = (
                        item?.parent_question_description ||
                        item?.question_description
                      )?.replace(/\%s\%/g, ' ')
                      const question_text = item?.question_text
                        ?.replace(/\%(u|b|i|s)\%/g, ' ')
                        .replace(/(#|\*)/g, ' ')
                        .replace(/\[\]/g, ' ')
                      return [
                        <tr key={item.id} data-id={item.id}
                        data-scope={item.scope}>
                          <td>{i + 1 + (currentPage-1) * 10}</td>
                          <td title={getGradeName(item.grade)}>
                          {getGradeName(item.grade)|| "---"}
                          </td>
                          <td
                            title={
                              SKILLS_SELECTIONS.find(
                                (find: StructType) =>
                                  find.code === item?.skills,
                              )?.display
                            }
                          >
                            <div className="__elipsis">
                              {SKILLS_SELECTIONS.find(
                                (find: StructType) =>
                                  find.code === item?.skills,
                              )?.display || '---'}
                            </div>
                          </td>
                          <td
                            title={
                              TASK_SELECTIONS.find(
                                (find: StructType) =>
                                  find.code === item?.question_type,
                              )?.display
                            }
                          >
                            <div className="__elipsis">
                              {TASK_SELECTIONS.find(
                                (find: StructType) =>
                                  find.code === item?.question_type,
                              )?.display || '---'}
                            </div>
                          </td>
                          <td
                            title={
                              LEVEL_SELECTIONS.find(
                                (find: StructType) => find.code === item?.level,
                              )?.display
                            }
                          >
                            <div className="__elipsis">
                              {LEVEL_SELECTIONS.find(
                                (find: StructType) => find.code === item?.level,
                              )?.display || '---'}
                            </div>
                          </td>

                          <td title={question_description}>
                            <div className="__elipsis">
                              {question_description || '---'}
                            </div>
                          </td>
                          <td title={question_text}>
                            <div className="__elipsis">
                              {question_text || '---'}
                            </div>
                          </td>
                          <td
                            title={getSeriesName(item.series)}
                            style={{ height: '5.6rem' }}
                          >
                            <div className="__elipsis">
                              {getSeriesName(item.series) || "---"}
                            </div>
                          </td>
                        </tr>,
                      ]
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          ) : (
            <div className="o-unit-test-question-update-series-drawer__empty">
              <img
                className="__banner"
                src="/images/collections/clt-emty-question.png"
                alt="banner"
              />
              <p className="__description"></p>
            </div>
          )}
        </div>
        {questionList.length > 0 && (
          <div className="o-unit-test-question-update-series-drawer__footer">
            <div className="__pagination">
              <Pagination
                prev
                next
                ellipsis
                size="md"
                total={totalRecords}
                maxButtons={10}
                limit={10}
                activePage={currentPage}
                onChangePage={(page: number) => handlePageChange(page)}
              />
            </div>
            <div style={{ display: 'flex' }}>
              <div className="__submit">
                <Button style={{ marginRight: '1.6rem' }} className="__update" onClick={()=>{
                  updateQuestionSeriesByUnitTestId(id)
                }}>
                  Cập nhật chương trình
                </Button>
              </div>
              <div className="__submit">
                <Button className="__close" onClick={() => setIsActive(false)}>
                  Đóng
                </Button>
              </div>
            </div>
          </div>
        )}
      </Drawer.Body>
    </Drawer>
  )
}
