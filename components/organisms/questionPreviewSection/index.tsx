import { useContext } from 'react'

import { Button, Tooltip, Whisper } from 'rsuite'

import { callApi } from 'api/utils'
import { MatchingQuestion } from 'components/molecules/matchingQuestion'
import { MatchingQuestion4 } from 'components/molecules/matchingQuestion4'
import { MatchingQuestion5 } from 'components/molecules/matchingQuestion5'
import { MatchingQuestion6 } from 'components/molecules/matchingQuestion6'
import { TemplateLinkertScale } from 'components/molecules/templateLinkertScale'
import { TemplateTrueFalse } from 'components/molecules/templateTrueFalse'
import { TemplateTrueFalseType3 } from 'components/molecules/templateTrueFalseType3'
import TemplateTrueFalseType4 from 'components/molecules/templateTrueFalseType4'
import TemplateTrueFalseType5 from 'components/molecules/templateTrueFalseType5'
import { UnitTestTemplateDD1 } from 'components/molecules/unitTestTemplateDD1'
import UnitTestTemplateDD2 from 'components/molecules/unitTestTemplateDD2'
import { UnitTestTemplateDrawLine1 } from 'components/molecules/unitTestTemplateDrawLine1'
import { UnitTestTemplateDrawLine2 } from 'components/molecules/unitTestTemplateDrawLine2'
import { UnitTestTemplateDrawLine3 } from 'components/molecules/unitTestTemplateDrawLine3'
import { UnitTestTemplateFB1 } from 'components/molecules/unitTestTemplateFB1'
import { UnitTestTemplateFB2 } from 'components/molecules/unitTestTemplateFB2'
import { UnitTestTemplateFB3 } from 'components/molecules/unitTestTemplateFB3'
import { UnitTestTemplateFB4 } from 'components/molecules/unitTestTemplateFB4'
import { UnitTestTemplateFB5 } from 'components/molecules/unitTestTemplateFB5'
import { UnitTestTemplateFB6 } from 'components/molecules/unitTestTemplateFB6'
import { UnitTestTemplateFB7_v2 } from 'components/molecules/unitTestTemplateFB7_v2'
import { UnitTestTemplateFB8 } from 'components/molecules/unitTestTemplateFB8'
import { TemplateMIX1 } from 'components/molecules/unitTestTemplateMIX1'
import { TemplateMIX2 } from 'components/molecules/unitTestTemplateMIX2'
import { UnitTestTemplateMR1 } from 'components/molecules/unitTestTemplateMR1'
import { UnitTestTemplateMR2 } from 'components/molecules/unitTestTemplateMR2'
import { UnitTestTemplateMR3 } from 'components/molecules/unitTestTemplateMR3'
import { UnitTestTemplateMultiChoices1 } from 'components/molecules/unitTestTemplateMultiChoices1'
import { UnitTestTemplateMultiChoices10 } from 'components/molecules/unitTestTemplateMultiChoices10'
import { UnitTestTemplateMultiChoices2 } from 'components/molecules/unitTestTemplateMultiChoices2'
import { UnitTestTemplateMultiChoices3 } from 'components/molecules/unitTestTemplateMultiChoices3'
import { UnitTestTemplateMultiChoices4 } from 'components/molecules/unitTestTemplateMultiChoices4'
import { UnitTestTemplateMultiChoices5 } from 'components/molecules/unitTestTemplateMultiChoices5'
import { UnitTestTemplateMultiChoices6 } from 'components/molecules/unitTestTemplateMultiChoices6'
import { UnitTestTemplateMultiChoices7 } from 'components/molecules/unitTestTemplateMultiChoices7'
import { UnitTestTemplateMultiChoices8 } from 'components/molecules/unitTestTemplateMultiChoices8'
import { UnitTestTemplateMultiChoices9 } from 'components/molecules/unitTestTemplateMultiChoices9'
import { UnitTestTemplateMultiChoices11 } from 'components/molecules/unitTestTemplateMultiChoices11'
import { UnitTestTemplateSA1 } from 'components/molecules/unitTestTemplateSA1'
import { UnitTestTemplateSelectObject1 } from 'components/molecules/unitTestTemplateSelectObject1'
import { UnitTestTemplateSL1 } from 'components/molecules/unitTestTemplateSL1'
import { UnitTestTemplateSL2 } from 'components/molecules/unitTestTemplateSL2'
import { SingleTemplateContext, WrapperContext } from 'interfaces/contexts'
import { SKILLS_SELECTIONS } from 'interfaces/struct'

import { DefaultPropsType, StructType } from '../../../interfaces/types'
import { UnitTestTemplateDrawShape1 } from 'components/molecules/unitTestTemplateDrawShape1'
import { UnitTestTemplateFillColour1 } from 'components/molecules/unitTestTemplateFillColour1'
import { UnitTestTemplateFillColour2 } from 'components/molecules/unitTestTemplateFillColour2'
import useTranslation from "@/hooks/useTranslation";
interface Props extends DefaultPropsType {
  data: any
  isShowIconChange?:boolean
}

const skillThumbnails: any = {
  GR: '/images/collections/clt-grammar.png',
  LI: '/images/collections/clt-listening.png',
  PR: '/images/collections/clt-pronunciation.png',
  RE: '/images/collections/clt-reading.png',
  SP: '/images/collections/clt-speaking.png',
  US: '/images/collections/clt-use-of-english.png',
  VO: '/images/collections/clt-vocab.png',
  WR: '/images/collections/clt-writing.png',
}

export const QuestionPreviewSection = ({
  className = '',
  data,
  style,
  isShowIconChange = false
}: Props) => {
  const { t } = useTranslation()
  const { globalModal } = useContext(WrapperContext)
  const { chosenTemplate, mode } = useContext(SingleTemplateContext)

  const sectionArr = Array.isArray(data?.sectionId)
    ? data.sectionId
    : [data?.sectionId]

  const handleReplace = async (
    questionId: number,
    id: number,
    skill: string[],
    totalQuestion: number,
    chosenQuestions: any[],
  ) => {
    const otherQuestions = [] as any[]
    chosenTemplate.state.sections.forEach((section: any) => {
      if (
        section?.parts &&
        section.parts[0]?.questions &&
        section.parts[0].questions.length > 0
      ) {
        section.parts[0].questions.forEach((question: any) => {
          otherQuestions.push(question?.id)
        })
      }
    })
    const grade = chosenTemplate.state?.templateLevelId || null
    const response = await callApi('/api/questions/search', 'post', 'Token', {
      grade: [grade],
      page: 0,
      skills: skill,
      total_question: totalQuestion,
      sid: chosenQuestions.map((m) => m.id),
      excludeId: otherQuestions.length > 0 ? otherQuestions : null,
    })
    if (response)
      globalModal.setState({
        id: 'question-drawer',
        content: {
          id,
          chosenQuestions: [],
          isDisabled: false,
          replaceId: questionId,
          excludeId: otherQuestions.length > 0 ? otherQuestions : null,
          data: response,
          grade: chosenTemplate.state?.templateLevelId || null,
          skill,
          totalQuestion,
          onSubmit: handleSubmitReplace,
          isChangeQuesion: true
        },
      })
  }

  const handleSubmitReplace = (content: any) => {
    const detail = chosenTemplate.state
    if (!content?.replaceId) return
    const findSectionIndex = content?.id
      ? detail.sections.findIndex((item: any) => item?.id === content.id)
      : -1
    if (findSectionIndex === -1) return
    const findQuestionIndex =
      detail.sections[findSectionIndex]?.parts &&
      detail.sections[findSectionIndex].parts[0] &&
      detail.sections[findSectionIndex].parts[0]?.questions
        ? detail.sections[findSectionIndex].parts[0].questions.findIndex(
            (item: any) => item?.id === content.replaceId,
          )
        : null
    if (findQuestionIndex === -1) return
    const first = detail.sections[findSectionIndex].parts[0]?.questions.filter(
      (item: any, i: number) => i < findQuestionIndex,
    )
    const second = detail.sections[findSectionIndex].parts[0]?.questions.filter(
      (item: any, i: number) => i > findQuestionIndex,
    )
    const middle = content?.data || []
    detail.sections[findSectionIndex].parts[0].questions = [
      ...first,
      ...middle,
      ...second,
    ]
    chosenTemplate.setState({ ...detail })
    globalModal.setState(null)
  }

  return (
    <div className={`o-question-preview-section ${className}`} style={style}>
      <div
        className="o-question-preview-section__header"
        title={
          data?.sectionId && data.sectionId.length > 0
            ? sectionArr
                .map(
                  (item: any) =>
                    SKILLS_SELECTIONS.find((skill:StructType) => skill.code === item)
                      ?.display || item,
                )
                .join(', ')
            : null
        }
      >
        {data?.sectionId ? (
          sectionArr
            .filter((item: any, i: number) => i <= 3)
            .map((item: any, i: number) =>
              i === 3 ? (
                <div key={i} className="__banner">
                  +2
                </div>
              ) : (
                <div key={i} className="__banner">
                  <img
                    src={
                      skillThumbnails[item] ||
                      '/images/collections/clt-unknown.png'
                    }
                    alt="banner"
                  />
                </div>
              ),
            )
        ) : (
          <div className="__banner">
            <img src="/images/collections/clt-unknown.png" alt="banner" />
          </div>
        )}
        <h5 className="__title">{data?.parts[0]?.name || t('unit-test')['part-1']}</h5>
      </div>
      <div className="o-question-preview-section__body">
        {data?.parts[0]?.questions.map((item: any) => (
          <div
            key={item.id}
            className="o-question-preview-section__item"
            style={{ paddingRight: '5.2rem' }}
          >
            <ActivityPicker data={item} type={item.question_type} />
            <div className="o-question-preview-section__id">ID: {item.id}</div>
            {['create', 'update'].includes(mode) && isShowIconChange && (
              <Whisper
              placement='top'
              trigger={'hover'}
              speaker={
                <Tooltip>{t('unit-test')['change-unit-test']}</Tooltip>
              }
            >
              <Button
                className="o-question-preview-section__replace"
                onClick={() =>
                  handleReplace(
                    item.id,
                    data?.id,
                    data?.sectionId,
                    item?.total_question || 1,
                    data?.parts[0]?.questions || [],
                  )
                }
              >
                <img src="/images/icons/ic-reload-active.png" alt="replace" />
              </Button>
            </Whisper>
            )}
          </div>
        ))}
      </div>
    </div>
  )
}

export const ActivityPicker = ({ data, type }: any) => {
  switch (type) {
    // drag and drop
    case 'DD1':
      return <UnitTestTemplateDD1 data={data} />
    case 'DD2':
      return <UnitTestTemplateDD2 data={data} />

    // fill in blanks
    case 'FB1':
      return <UnitTestTemplateFB1 data={data} />
    case 'FB2':
      return <UnitTestTemplateFB2 data={data} />
    case 'FB3':
      return <UnitTestTemplateFB3 data={data} />
    case 'FB4':
      return <UnitTestTemplateFB4 data={data} />
    case 'FB5':
      return <UnitTestTemplateFB5 data={data} />
    case 'FB6':
      return <UnitTestTemplateFB6 data={data} />
    case 'FB7':
      return <UnitTestTemplateFB7_v2 data={data} />
    case 'FB8':
      return <UnitTestTemplateFB8 data={data} />

    // likert scale
    case 'LS1':
      return <TemplateLinkertScale data={data} mode="paragraph" />
    case 'LS2':
      return <TemplateLinkertScale data={data} mode="audio" />

    // multiple choices
    case 'MC1':
      return <UnitTestTemplateMultiChoices1 data={data} />
    case 'MC2':
      return <UnitTestTemplateMultiChoices2 data={data} />
    case 'MC3':
      return <UnitTestTemplateMultiChoices3 data={data} />
    case 'MC4':
      return <UnitTestTemplateMultiChoices4 data={data} />
    case 'MC5':
      return <UnitTestTemplateMultiChoices5 data={data} />
    case 'MC6':
      return <UnitTestTemplateMultiChoices6 data={data} />
    case 'MC7':
      return <UnitTestTemplateMultiChoices7 data={data} />
    case 'MC8':
      return <UnitTestTemplateMultiChoices8 data={data} />
    case 'MC9':
      return <UnitTestTemplateMultiChoices9 data={data} />
    case 'MC10':
      return <UnitTestTemplateMultiChoices10 data={data} />
    case 'MC11':
      return <UnitTestTemplateMultiChoices11 data={data} />

    // matching questions
    case 'MG1':
      return <MatchingQuestion data={data} />
    case 'MG2':
      return <MatchingQuestion data={data} />
    case 'MG3':
      return <MatchingQuestion data={data} />
    case 'MG4':
      return <MatchingQuestion4 data={data} />
    case 'MG5':
      return <MatchingQuestion5 data={data} />
    case 'MG6':
      return <MatchingQuestion6 data={data} />

    // multiple responses
    case 'MR1':
      return <UnitTestTemplateMR1 data={data} />
    case 'MR2':
      return <UnitTestTemplateMR2 data={data} />
    case 'MR3':
      return <UnitTestTemplateMR3 data={data} />

    // select from list
    case 'SL1':
      return <UnitTestTemplateSL1 data={data} />
    case 'SL2':
      return <UnitTestTemplateSL2 data={data} />

    // short answer
    case 'SA1':
      return <UnitTestTemplateSA1 data={data} />

    // true false
    case 'TF1':
      return <TemplateTrueFalse data={data} mode="audio" />
    case 'TF2':
      return <TemplateTrueFalse data={data} mode="paragraph" />
    case 'TF3':
      return <TemplateTrueFalseType3 data={data} />
    case 'TF4':
      return <TemplateTrueFalseType4 data={data} mode='image'/>
    case 'TF5':
      return <TemplateTrueFalseType5 data={data} mode='image'/>
    // MIX
    case 'MIX1':
      return <TemplateMIX1 data={data} />
    case 'MIX2':
      return <TemplateMIX2 data={data} />
    case 'DL1':
      return <UnitTestTemplateDrawLine1 data={data} />
    case 'DL3':
      return <UnitTestTemplateDrawLine3 data={data} />     
    case 'SO1':
      return <UnitTestTemplateSelectObject1 data={data} />
    case 'DL2':
      return <UnitTestTemplateDrawLine2 data={data} />
    case 'DS1':
      return <UnitTestTemplateDrawShape1 data={data} />
    case 'FC1':
      return <UnitTestTemplateFillColour1 data={data} />
    case 'FC2':
      return <UnitTestTemplateFillColour2 data={data} />
    // not found
    default:
      return <></>
  }
}
