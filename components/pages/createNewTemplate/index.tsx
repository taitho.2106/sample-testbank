import { useContext, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { Button, Popover, Whisper, Tooltip } from 'rsuite'

import { callApi } from 'api/utils'
import { BreadCrumbNav } from 'components/molecules/breadCrumbNav'
import { StickyFooter } from 'components/organisms/stickyFooter'
import { TemplateInfoCard } from 'components/organisms/templateInfoCard'
import { TemplateSectionCard } from 'components/organisms/templateSectionCard'
import { Wrapper } from 'components/templates/wrapper'
import useNoti from 'hooks/useNoti'
import { PACKAGES, USER_ROLES } from 'interfaces/constants'
import { SingleTemplateContext, WrapperContext } from 'interfaces/contexts'
import { userInRight } from 'utils'

import { DefaultPropsType } from '../../../interfaces/types'
import useTranslation from "@/hooks/useTranslation";
import useCurrentUserPackage from '@/hooks/useCurrentUserPackage'

export const CreateNewTemplate = ({
  className = '',
  style,
}: DefaultPropsType) => {
  const { t } = useTranslation()
  const router = useRouter()

  const { breadcrumbData, templateDetail } = useContext(SingleTemplateContext)

  const checkReady = () => {
    if (!templateDetail.state?.name) return false
    if (!templateDetail.state?.templateLevelId) return false
    if (!templateDetail.state?.time) return false
    if (!templateDetail.state?.totalQuestion) return false
    if (!templateDetail.state?.point) return false
    return true
  }

  return (
    <Wrapper
      className={`p-create-new-template p-detail-format ${className}`}
      pageTitle={
        router.query?.templateSlug === 'create-new-template'
          ? t('unit-test')['create-new-template']
          : templateDetail.state?.name || ''
      }
      style={style}
    >
      <div className="p-detail-format__breadcrumb">
        <BreadCrumbNav data={breadcrumbData} />
      </div>
      <div className="p-detail-format__container">
        <div className="container-info">
          <div className="container-title">
            <h5>{t('unit-test')['info-test-title']}</h5>
          </div>
          <TemplateInfoCard isReady={checkReady()} />
        </div>
        <div
          className="container-section-list"
          style={{ paddingBottom: '4rem' }}
        >
          <div className="container-title">
            <h5>{t('unit-test')['info-parts-title']}</h5>
          </div>
          <TemplateSectionCard isReady={checkReady()} />
        </div>
        <FooterSubmit />
      </div>
    </Wrapper>
  )
}

const FooterSubmit = () => {
  const { t } = useTranslation()
  const router = useRouter()

  const [session] = useSession()
  const isEditAble = userInRight([USER_ROLES.Operator], session)
  const isTeacherRight = userInRight([-1, USER_ROLES.Teacher], session)
  const { dataPlan } = useCurrentUserPackage()
  const isEdit = dataPlan?.code !== PACKAGES.BASIC.CODE

  const { getNoti } = useNoti()

  const [isCopy, setIsCopy] = useState(false)

  const { globalModal } = useContext(WrapperContext)
  const { templateDetail, mode, setMode } = useContext(SingleTemplateContext)

  const checkReadyToFinalSubmit = () => {
    if (
      !templateDetail.state?.sections ||
      templateDetail.state.sections.length <= 0
    )
      return false
    let check = true
    templateDetail.state.sections.forEach((section: any) => {
      if (!section?.sectionId || section.sectionId.length <= 0) check = false
      else if (!section?.parts[0]) check = false
      else {
        if (
          !section.parts[0]?.name ||
          !section.parts[0]?.totalQuestion ||
          !section.parts[0]?.points
        )
          check = false
      }
    })
    return check
  }

  const handleCheckSubmit = () => {
    // thong tin chung

    const templateTime = templateDetail.state?.time || 0
    if (!templateTime) {
      getNoti('error', 'topEnd', t('common-get-noti')['template-time-change-validate'])
      return
    }

    const templateTotalQuestion = templateDetail.state?.totalQuestion || 0
    if (!templateTotalQuestion) {
      getNoti('error', 'topEnd', t('common-get-noti')['total-question-validate'])
      return
    }
    const unitTestTotalPoint = templateDetail.state?.point || 0
    if (!unitTestTotalPoint) {
      getNoti('error', 'topEnd', t('common-get-noti')['point-validate'])
      return
    }

    // thong tin chi tiet vs thong tin chung
    let skillTotalQuestion = 0
    let skillTotalPoint = 0
    if (templateDetail.state?.sections)
      templateDetail.state.sections.forEach((section: any) => {
        if (section?.parts[0] && section.parts[0]?.totalQuestion)
          skillTotalQuestion += section.parts[0]?.totalQuestion

        if (section?.parts[0] && section.parts[0]?.totalQuestion)
          skillTotalPoint += section.parts[0]?.points
      })
    skillTotalPoint = Math.round(skillTotalPoint * 100) / 100
    if (skillTotalQuestion && skillTotalQuestion !== templateTotalQuestion) {
      getNoti(
        'error',
        'topEnd',
        t('common-get-noti')['check-total-question'],
      )
      return
    }
    if (skillTotalPoint && skillTotalPoint !== unitTestTotalPoint) {
      getNoti(
        'error',
        'topEnd',
          t('common-get-noti')['check-total-point'],
      )
      return
    }

    handleSubmit()
  }

  const handleSubmit = async () => {
    const formData = {} as any
    formData.name = templateDetail.state?.name || ''
    formData.template_level_id = templateDetail.state?.templateLevelId || null
    formData.time = templateDetail.state?.time || null
    formData.total_question = templateDetail.state?.totalQuestion || null
    formData.total_point = templateDetail.state?.point || null
    formData.status =
      mode === 'create' ? true : templateDetail.state?.status || false
    const sections = [] as any[]
    if (templateDetail.state?.sections)
      templateDetail.state.sections.forEach((section: any) => {
        const returnSection = {} as any
        returnSection.section_id = section.sectionId
        const parts = [] as any[]
        if (section?.parts)
          section.parts.forEach((part: any) => {
            const returnPart = {} as any
            returnPart.name = part?.name
            returnPart.question_types = part.questionTypes
            returnPart.total_question = part.totalQuestion
            returnPart.points = part.points
            parts.push(returnPart)
          })
        returnSection.parts = parts
        sections.push(returnSection)
      })
    formData.sections = sections
    const response: any = await callApi(
      `/api/templates${
        mode === 'update' ? `/${templateDetail.state?.id || ''}` : ''
      }`,
      mode === 'update' ? 'put' : 'post',
      'Token',
      formData,
    )
    if (response) {
      if (mode === 'update')
        globalModal.setState({
          id: 'confirm-modal',
          type: 'update-template-success',
          content: {
            isCopy: isCopy,
            closeText: t('unit-test')['list-templates'],
            submitText: t('no-field-data')['teacher-btn-action'],
            onClose: () =>
              router.push(`/templates${isEditAble ? '' : '?m=mine'}`),
            onSubmit: () =>
              router.push(
                `/unit-test/[templateSlug]?id=${response.id}${router.query.m?`&m=${router.query.m}`:''}`,
                `/unit-test/create-new-unittest?id=${response.id}${router.query.m?`&m=${router.query.m}`:''}`,
              ),
          },
        })
      else if (mode === 'create')
        globalModal.setState({
          id: 'confirm-modal',
          type: 'create-template-success',
          content: {
            isCopy: isCopy,
            closeText: t('unit-test')['list-templates'],
            submitText: t('no-field-data')['teacher-btn-action'],
            onClose: () =>
              router.push(`/templates${isEditAble ? '' : '?m=mine'}`),
            onSubmit: () =>
              router.push(
                `/unit-test/[templateSlug]?id=${response.id}${router.query.m?`&m=${router.query.m}`:''}`,
                `/unit-test/create-new-unittest?id=${response.id}${router.query.m?`&m=${router.query.m}`:''}`,
              ),
          },
        })
    }
  }

  const onHandleCopy = () => {
    setIsCopy(true)
    setMode('create')
    router.push(
      {pathname: '/templates/[templateSlug]',query: {m: 'mine'}},
      {pathname: '/templates/create-new-template',query: {m: 'mine'}}, 
      {shallow: true}
    )
    getNoti('success', 'topEnd', t('common-get-noti')['copy-succeed'])
  }

  return (
    <div className="container-footer">
      <StickyFooter
        containerStyle={{
          justifyContent: 'space-between',
        }}
      >
        <div>
          {mode === 'detail' && !isEditAble && (
            <Whisper
              placement="top"
              trigger="hover"
              speaker={
                <Tooltip>{t('unit-test')['tooltip-copy-template']}</Tooltip>
              }
            >
              <Button
                className="__submit"
                type="button"
                style={{ backgroundColor: '#71AC5D' }}
                onClick={onHandleCopy}
              >
                <span>{t('common')['copy']}</span>
              </Button>
            </Whisper>
          )}
        </div>
        <div>
          {mode === 'detail' &&
            ((templateDetail.state?.scope === 0 && isEditAble) ||
              (templateDetail.state?.scope === 1 && isTeacherRight && isEdit)) && (
                <Whisper
                placement="top"
                trigger="hover"
                controlId={`control-id-top`}
                speaker={
                  <Tooltip>
                    {t('unit-test')['tooltip-edit-template']}
                  </Tooltip>
                }
              >
                <Button
                  className="__submit"
                  onClick={() => {
                    router.push(
                      // '/templates/[templateSlug]?mode=edit',
                      `/templates/${templateDetail.state?.id}?mode=edit${router.query.m ? `&m=${router.query.m}` : ''}`,
                    )
                    setMode('update')
                  }}
                >
                  <span>{t('common')['edit']}</span>
                </Button>
              </Whisper>
            )}
          {(mode === 'update' || mode === 'create') && (
            <>
              <Whisper
                placement="top"
                trigger="hover"
                controlId={`control-id-top`}
                speaker={
                  <Popover>
                    {mode === 'create' ? t('unit-test')['tooltip-create-template'] : t('unit-test')['tooltip-save-template']}
                  </Popover>
                }
              >
                <div style={{ cursor: !checkReadyToFinalSubmit() ? 'not-allowed' : 'auto'}}>
                  <Button
                    style={{ pointerEvents: !checkReadyToFinalSubmit() ? 'none' : 'auto' }}
                    className="__submit"
                    disabled={!checkReadyToFinalSubmit()}
                    onClick={handleCheckSubmit}
                  >
                    <span>{t('common')['completed']}</span>
                  </Button>
                </div>
              </Whisper>
            </>
          )}
        </div>
      </StickyFooter>
    </div>
  )
}
