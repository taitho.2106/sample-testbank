import { useContext, useEffect, useMemo, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import { Collapse } from 'react-collapse'
import { Button, Pagination, Popover, Tooltip, Whisper } from 'rsuite'

import { templateListTransformData } from 'api/dataTransform'
import { callApi } from 'api/utils'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { MultiSelectPicker } from 'components/atoms/selectPicker/multiSelectPicker'
import { BreadCrumbNav } from 'components/molecules/breadCrumbNav'
import { TemplateTable } from 'components/molecules/templateTable'
import { StickyFooter } from 'components/organisms/stickyFooter'
import { Wrapper } from 'components/templates/wrapper'
import useGrade from 'hooks/useGrade'
import { PACKAGES, USER_ROLES } from "interfaces/constants";
import { TemplateContext } from 'interfaces/contexts'
import { SKILLS_SELECTIONS } from 'interfaces/struct'
import { userInRight } from 'utils'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import useTranslation from "@/hooks/useTranslation";

export const Templates = () => {
  const { t } = useTranslation()
  const router = useRouter()
  const {getGrade, gradeList} = useGrade()
  const [session] = useSession()
  const { dataPlan } = useCurrentUserPackage();
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
  const isAuthPage = router.query.m === 'mine'
  const isQuery = !router.query.m
  const isCreatable = (isSystem && isQuery) || (isTeacher && dataPlan?.code !== PACKAGES.BASIC.CODE && isAuthPage)
  const { breadcrumbData, templateData, totalPages, currentPage, isLoading } =
    useContext(TemplateContext)

  const dataGrade = useMemo(() => {
    return isSystem ? getGrade : gradeList
  } , [isSystem,getGrade, gradeList])

  const [isOpenFilterCollapse, setIsOpenFilterCollapse] = useState(false)
  const [triggerReset, setTriggerReset] = useState(false)

  const [filterName, setFilterName] = useState('')
  const [filterGrade, setFilterGrade] = useState([] as any[])
  const [filterTime, setFilterTime] = useState('')
  const [filterTotalQuestion, setFilterTotalQuestion] = useState('')
  const [filterSkill, setFilterSkill] = useState([] as any[])
  const [filterBagde, setFilterBagde] = useState(
    [
      // filterName ? true : false,
      filterGrade.length > 0,
      filterTime ? true : false,
      filterTotalQuestion ? true : false,
      filterSkill.length > 0,
    ].filter((item) => item === true).length,
  )
  const [searching, setSearching] = useState(false)
  const [page, setPage] = useState(1)

  const handleNameChange = (val: string) => setFilterName(val)
  const handleNameSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      handleSubmit(false, 1, { name: val })
      e.target.blur()
    }
  }
  const handleGradeChange = (val: any) => {
    let value:any[] = []
    if(val){
      value = [val]
    }
    setFilterGrade([...value])
    // handleSubmit(false, 1, { level: [...value] })
  }
  const handleTimeChange = (val: string) => setFilterTime(!isNaN(Number(val)) ? val : '')
  const handleTimeSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      handleSubmit(false, 1, { time: val })
      e.target.blur()
    }
  }
  const handleTotalQuestionChange = (val: string) => setFilterTotalQuestion(!isNaN(Number(val)) ? val : '')
  const handleTotalQuestionSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      handleSubmit(false, 1, { totalQuestions: val })
      e.target.blur()
    }
  }
  const handleSkillChange = (val: any[]) => {
    setFilterSkill([...val])
    // handleSubmit(false, 1, { skill: [...val] })
  }

  const handleDelete = async (id: number) => {
    if (!id) return
    const response: any = await callApi(`/api/templates/${id}`, 'delete')
    if (response?.status === 200) {
      const currentData = templateData.state
      const newData = currentData.filter((item: any) => item?.id !== id)
      if(!newData.length) {
        setPage(Math.max(page - 1, 1))
        handleSubmit(false, Math.max(page - 1, 1))
      }else {
        handleSubmit(false, page)
      }
    }
  }

  const handleStatusToggle = async (id: number, boo: boolean) => {
    if (!id) return
    const response: any = await callApi(
      `/api/templates/${id}`,
      'put',
      'Token',
      {
        isQuickEdit: true,
        status: boo,
      },
    )
    if (response?.status === 200) {
      const currentData = templateData.state
      const findIndex = currentData.findIndex((item: any) => item?.id === id)
      if (findIndex === -1) return
      currentData[findIndex]['status'] = boo ? 1 : 0
      templateData.setState([...currentData])
    }
  }
    const scrollToTop = () =>{
        window.scrollTo({
          top: 0, 
          behavior: 'smooth'
          /* you can also use 'auto' behaviour
             in place of 'smooth' */
        });
      };
  const handleSubmit = async (
    isReset: boolean,
    page: number,
    body: any = null,
  ) => {
    const scope = router.query?.m
      ? ['mine', 'teacher'].findIndex((item) => item === router.query.m)
      : null
    if (isReset) setFilterBagde(0)
    scrollToTop()
    // collect data
    const data = !isReset
      ? {
          name: body?.name || filterName,
          level: body?.level || filterGrade,
          page: page - 1,
          skill: body?.skill || filterSkill,
          scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
          time: body?.time || filterTime,
          totalQuestions: body?.totalQuestions || filterTotalQuestion,
        }
      : { scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null }
    // send request
    const response: any = await callApi(
      '/api/templates/search',
      'post',
      'token',
      data,
    )
    if (response?.templates)
      templateData.setState([
        ...response.templates.map(templateListTransformData),
      ])
    setPage(page)
    if (response?.totalPages) totalPages.setState(response.totalPages)
  }

  const checkFilterBadge = () => {
    const length = [
      // filterName ? true : false,
      filterTime ? true : false,
      filterTotalQuestion ? true : false,
      filterSkill.length > 0,
      filterGrade.length > 0,
    ].filter((item) => item === true).length
    setFilterBagde(length)
  }

  const resetFilterState = () => {
    setFilterName('')
    setFilterGrade([])
    setFilterTime('')
    setFilterTotalQuestion('')
    setFilterSkill([])
  }
  const onResetForm = ()=>{
    setTriggerReset(!triggerReset)
    resetFilterState()
    handleSubmit(true, 1)
  }
  useEffect(
    () => checkFilterBadge(),
    [filterName, filterGrade, filterTime, filterTotalQuestion, filterSkill],
  )
  useEffect(()=>{
    onResetForm()
  },[router.asPath])
  const isSearchEmpty = useMemo(() =>{
    const isSearchingHasValue = filterName || filterGrade.length !== 0 || filterTime || filterTotalQuestion || filterSkill.length !== 0 ? true : false
    const result = (!templateData.state || templateData.state.length === 0) && searching && isSearchingHasValue
    if(!result){
      setSearching(false)
    }
    return result
  }, [templateData, searching, filterName, filterGrade, filterTime, filterTotalQuestion, filterSkill])
  const onKeyDownInputNumber = (e:any,val:any)=>{
    const key = e?.key?.toLowerCase();
    if(["e","+","-"].includes(key)){
      e.preventDefault();
      return;
    }
    if(!val && key == "0"){
      e.preventDefault();
      return;
    }
  }
  return (
    <Wrapper
      className="p-create-new-template p-detail-format"
      pageTitle={
        router.query?.m
          ? router.query.m === 'mine'
            ? t('unit-test')['my-template-1']
            : t('unit-test')["teacher-template"]
          : t('unit-test')['operator-template-1']
      }
    >
      <div className="p-detail-format__breadcrumb">
        <BreadCrumbNav data={breadcrumbData} />
      </div>
      <div className="m-unittest-template-table-data">
        <div className="m-unittest-template-table-data__content">
          <div className="content__sup">
            <div className="__input-group">
              <InputWithLabel
                className="__search-box"
                label={t('table')['template-name']}
                placeholder={t("common")["placeholder-search-template"]}
                triggerReset={triggerReset}
                type="text"
                onBlur={handleNameChange}
                onKeyUp={handleNameSubmit}
                icon={
                  <img src="/images/icons/ic-search-dark.png" alt="search" />
                }
              />
              <Whisper
                placement="bottom"
                trigger="hover"
                speaker={
                  <Tooltip>
                    {t('common-whisper')['show-more-filter']}
                  </Tooltip>
                }
              >
                <Button
                  className="__sub-btn"
                  data-active={isOpenFilterCollapse}
                  onClick={() => setIsOpenFilterCollapse(!isOpenFilterCollapse)}
                  style={{ marginLeft: '0.8rem' }}
                >
                  <div
                    className="__badge"
                    data-value={filterBagde}
                    data-hidden={filterBagde <= 0}
                  >
                    <img src="/images/icons/ic-filter-dark.png" alt="filter" />
                  </div>
                  <span>{t('common')['filter']}</span>
                </Button>
              </Whisper>
              <Whisper
                placement="bottom"
                trigger="hover"
                speaker={<Tooltip>{t('unit-test')['tooltip-reset']}</Tooltip>}
              >
                <Button
                  className="__sub-btn"
                  onClick={() => {
                    onResetForm()
                    setSearching(false)
                  }}
                >
                  <img src="/images/icons/ic-reset-darker.png" alt="reset" />
                  <span>{t('common')['reset']}</span>
                </Button>
              </Whisper>
              <Button
                className="__main-btn"
                onClick={() => {
                  handleSubmit(false, 1)
                  setSearching(true)
                }}
              >
                <img src="/images/icons/ic-search-dark.png" alt="find" />
                <span>{t('common')['search']}</span>
              </Button>
            </div>
            <div className="__action-group">
              {isCreatable && (
                <Button
                  className="__action-btn"
                  onClick={() =>
                    router.push(
                      {
                        pathname: '/templates/[templateSlug]',
                        query: router.query.m ? { m: router.query.m } : {},
                      },
                      {
                        pathname: '/templates/create-new-template',
                        query: router.query.m ? { m: router.query.m } : {},
                      },
                    )
                  }
                >
                  <img src="/images/icons/ic-plus-white.png" alt="create" />
                  <span>{t('common')['create']}</span>
                </Button>
              )}
            </div>
          </div>
          <Collapse isOpened={isOpenFilterCollapse}>
            <div className="content__sub">
              {/* {getGrade.length > 0 && (<MultiSelectPicker
                className="__filter-box"
                data={getGrade}
                label="Khối lớp"
                triggerReset={triggerReset}
                style={{ flex: 'unset' }}
                onChange={handleGradeChange}
              />)} */}
              {dataGrade && (
                <SelectFilterPicker
                  data={dataGrade}
                  isMultiChoice={false}
                  inputSearchShow={false}
                  onChange={handleGradeChange}
                  className={'__filter-box --grade'}
                  triggerReset={triggerReset}
                  label={t('common')['grade']}
                  menuSize='lg'
                  style={{ width: 'calc(25% - 0.8rem)' }}
                ></SelectFilterPicker>
              )}
              <Whisper
                placement="top"
                trigger="hover"
                speaker={
                  <Tooltip>{t("common-whisper")['time-required']}</Tooltip>
                }
              >
                <InputWithLabel
                  className="__filter-box"
                  decimal={0}
                  label={t('common')['time']}
                  min={0}
                  placeholder={t('common')['time-2']}
                  triggerReset={triggerReset}
                  type="number"
                  onkeyDown={onKeyDownInputNumber}
                  onBlur={handleTimeChange}
                  onKeyUp={handleTimeSubmit}
                  style={{ width: 'calc(25% - 0.8rem)' }}
                />
              </Whisper>
              <InputWithLabel
                className="__filter-box"
                decimal={0}
                label={t('common')['total-question']}
                min={0}
                triggerReset={triggerReset}
                type="number"
                onkeyDown={onKeyDownInputNumber}
                onBlur={handleTotalQuestionChange}
                onKeyUp={handleTotalQuestionSubmit}
                style={{ width: 'calc(25% - 0.8rem)' }}
              />
              <MultiSelectPicker
                className="__filter-box"
                data={SKILLS_SELECTIONS}
                label={t('common')['skill']}
                triggerReset={triggerReset}
                style={{ width: 'calc(25% - 0.8rem)' }}
                onChange={handleSkillChange}
              />
            </div>
          </Collapse>
        </div>
        {(templateData.state && templateData.state.length > 0) || isLoading ? (
          <TemplateTable
            currentPage={page}
            data={templateData.state || []}
            isLoading={isLoading}
            onDelete={handleDelete}
            onStatusToggle={handleStatusToggle}
          />
        ) : (
          <div
            className="o-question-drawer__empty"
            style={{ minHeight: '50vh', paddingTop: 32, paddingBottom: 32 }}
          >
              <img
                className="__banner"
                src={isSearchEmpty ? '/images/collections/clt-emty-result.png' : '/images/collections/clt-emty-template.png'}
                alt="banner"
              />
            <p className="__description">
              {isSearchEmpty ? (
                <>{t('no-field-data')['search-empty']}</>
              ) : (
                <>
                  {t('unit-test')['no-template-system']}
                  <br />
                  {isCreatable && t('unit-test')['pls-create']}
                </>
              )}
            </p>
            {isCreatable  && !isSearchEmpty && (
              <Button
                className="__submit"
                onClick={() =>
                  router.push(
                    {
                      pathname: '/templates/[templateSlug]',
                      query: router.query.m ? { m: router.query.m } : {},
                    },
                    {
                      pathname: '/templates/create-new-template',
                      query: router.query.m ? { m: router.query.m } : {},
                    },
                  )
                }
              >
                {t('common')['create']}
              </Button>
            )}
          </div>
        )}
        {templateData.state.length > 0 && (
          <StickyFooter>
            <div className="__pagination">
              <Pagination
                prev
                next
                ellipsis
                size="md"
                total={totalPages.state * 10}
                maxButtons={10}
                limit={10}
                activePage={page}
                onChangePage={(page: number) => handleSubmit(false, page)}
              />
            </div>
          </StickyFooter>
        )}
      </div>
    </Wrapper>
  )
}
