import { Button, Modal } from 'rsuite'

import FormEditSkills from '../FormEditSkills';

export default function ModalEditGrade({onCloseModal, className='', data, isReload, dataAll}:any) {
    const handleOnClose = () => {
        onCloseModal(false)
    }
    const isEdit = (data && data.id) ? true: false;
    return ( 
        <Modal  
            className={`edit-field-modal ${className} edit-field-modal-custom`}
            open={true}
            style={{
                overflowY:'hidden',
                maxHeight:'100%',
                background: "rgba(39,44,54,0.3)"
            }}
            onClose={handleOnClose} 
            backdrop={false}
        >
            <Modal.Body>
                <div className="modal-header" >
                    <span>{isEdit ? "Cập nhật" : "Thêm mới"} kỹ năng</span>               
                    <Button className="modal-toggle" onClick={handleOnClose}>
                        <img src="/images/icons/ic-close-admin.png" alt="close" />
                    </Button>
                </div>
                <div className='modal-sub-content'>
                    <FormEditSkills data={data} dataAll={dataAll} isEdit={isEdit} onCloseModal={onCloseModal} isReload={isReload}/>
                </div>
            </Modal.Body>
        </Modal>
    );
}
