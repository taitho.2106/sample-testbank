import {  Button, Modal } from "rsuite";

function ModalSuccess({data, onCloseModalSuccess}:any) {
    if(!data) return null;
    const{status, mode, message} = data
    return ( 
            <Modal
            className="modal-success-form-user-guide"
            open={true}
            onClose={()=>{onCloseModalSuccess("none")}}
            backdrop={false}
            overflow={true} 
            style={{background:"rgba(39, 44, 54, 0.3)", overflowY:'hidden'}}
            >
                <Modal.Body>
                <div className='modal-sub-content'>
               
                    <div className="title-content">
                        {status?
                            <>
                            <img src="/images/img-success.png" alt=""></img>
                        <span className="title-content-top-success">{mode=="edit"?"Cập nhật hướng dẫn thành công":"Tạo mới hướng dẫn thành công"}</span>
                            </>
                        :
                        <>
                        <img src="/images/img-error.png" alt=""></img>
                        <span className="title-content-top-error">{mode=="edit"?"Cập nhật hướng dẫn thất bại":"Tạo mới hướng dẫn thất bại"}</span>
                        <span className="title-content-message">{message}</span>
                        </>
                        }
                    </div>
                    <div className="action-btn">
                        {status?
                            <>
                            <Button 
                            onClick={()=>onCloseModalSuccess("add")}>Tạo mới hướng dẫn</Button>
                        <Button 
                        onClick={()=>onCloseModalSuccess("refresh")}>Danh sách tài liệu</Button>
                            </>
                        :
                        <>
                        <Button 
                        onClick={()=>onCloseModalSuccess("none")}>Quay lại</Button>
                        <Button
                        onClick={()=>onCloseModalSuccess("refresh")}>Danh sách tài liệu</Button>                
                        </>
                        }
                    </div>
                </div>
                </Modal.Body>
            </Modal>
     );
}

export default ModalSuccess;