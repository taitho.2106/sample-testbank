/* eslint-disable @typescript-eslint/no-unused-vars */
import { useContext, useEffect, useRef, useState } from "react";

import { Message, toaster } from "rsuite";

import { SelectPicker } from "components/atoms/selectPicker";
import { WrapperAdminContext } from "interfaces/contexts";

const dataType = [
    {
        code: 1,
        display:'Video'
    },
    {
        code: 2,
        display:'Document'
    },
]

const initData = {title:'',type:'', linkfile:'', screen:'', titleEn:'', linkfileEn:'',idScreen:''}
const initError = {title:'',type:'', fileName:'', screen:'', titleEn:'', fileNameEn:''}
function FormDataModal({ dataForm , onDeleted, onSuccessForm}:any) {
    // console.log("data2", dataForm)
    let data:any;
    let isEdit = false;
    if(dataForm?.id){
        data = dataForm
        isEdit = true;
    }else{
        data = initData
    }
    // console.log("data-form", data)
    const [dataScreen, setDataScreen] = useState([])
    const userGuideId = data?.id?data.id:-1;

    const fileUpload = useRef(null)
    const [title, setTitle] = useState(data?.title)
    const [titleEn, setTitleEn] = useState(data?.titleEn)
    const [type, setType] = useState({code:data?.type_data || '', display: data?.type_data === 1 ? 'Video' : data?.type_data === 2 ?'Document' : ''})
    const [file,setFile]= useState(null)
    const [fileEn,setFileEn]= useState(null)
    const [fileName, setFileName] = useState(data?.linkfile)
    const [fileNameEn, setFileNameEn] = useState(data?.linkfileEn)
    const [screen, setScreen] = useState(data?.screen)
    const [idScreen, setIdScreen] = useState(data?.screenId || '')
    const [selected, setSelected] = useState(1 as 1|2)
    const [error, setError] = useState({...initError})
    const [screenData, setScreenData] = useState({code:idScreen,display:screen})
    const {handleLoading} = useContext(WrapperAdminContext)
    async function fetchDataScreen() {
        const result = await fetch('/api/admin/user-guide-screen')
        const data = await result.json()
        const arrScreen:any = data.data.map((item:any)=>{
            return {
                code:item.id,
                display:item.name
            }
        })
        setDataScreen(arrScreen)
    }
    useEffect(()=>{
        fetchDataScreen()
    },[])
    //validate dataform
    const validate = () =>{
        let valid = false;
        //validate title
        if(title.length <= 0){
            valid = true
            error.title = "Vui lòng nhập Tên tài liệu"
        }else{
            error.title = ""
        }//validate titleEn
        if(titleEn.length <= 0){
            valid = true
            error.titleEn = "File Name is required"
        }else{
            error.titleEn = ""
        }
        //validate screen
        if(Boolean(idScreen)){
            error.screen = ""
        }else{
            valid = true
            error.screen = "Vui lòng chọn Màn hình"
        }
        //validate Link File and Type
        if(file){
            if(Boolean(type.display)){
                if(type.display === 'Video' && validFileVideo(file)){
                    error.type = ""
                    error.fileName = ""
                }
                if(type.display === "Video" && !validFileVideo(file)){
                    valid = true
                    error.type = ""
                    error.fileName = "File/Video must be a video file"
                }
                if(type.display === 'Document' && validFilePDF(file)){
                    error.type = ""
                    error.fileName = ""
                }
                if(type.display === 'Document' && !validFilePDF(file)){
                    valid = true;
                    error.type = ""
                    error.fileName = "File/Video must be a Pdf file"
                }
            }else{
                valid = true;
                error.type = "Vui lòng chọn Định dạng"
                error.fileName = ""
            }
        }else{
            if(Boolean(type.display)){
                if(type.display === 'Video' && validURLVideo(encodeURI(fileName))){
                    error.type = ""
                    error.fileName = ""
                }
                if(type.display === "Video" && !validURLVideo(encodeURI(fileName))){
                    valid = true
                    error.type = ""
                    error.fileName = "File/Video must be a video URL"
                }
                if(type.display === 'Document' && validURLPDF(encodeURI(fileName))){
                    error.type = ""
                    error.fileName = ""
                }
                if(type.display === 'Document' && !validURLPDF(encodeURI(fileName))){
                    valid = true;
                    error.type = ""
                    error.fileName = "File/Video must be a Pdf URL"
                }
            }else{
                valid = true;
                error.type = "Vui lòng chọn Định dạng"
                error.fileName = ""
            }
        }
        //validate fileEn
        if(fileEn){
            if(Boolean(type.display)){
                if(type.display === "Video" && validFileVideo(fileEn)){
                    error.type = ""
                    error.fileNameEn = ""
                }
                if(type.display === "Video" && !validFileVideo(fileEn)){
                    valid = true
                    error.type = ""
                    error.fileNameEn = "File/Video must be a video file"
                }
                if(type.display === "Document" && validFilePDF(fileEn)){
                    error.type = ""
                    error.fileNameEn = ""
                }
                if(type.display === "Document" && !validFilePDF(fileEn)){
                    valid = true;
                    error.type = ""
                    error.fileNameEn = "File/Video must be a Pdf file"
                }
            }else{
                valid = true;
                error.type = "Vui lòng chọn Định dạng"
                error.fileNameEn = ""
            }
        }else{
            if(Boolean(type.display)){
                if(type.display === 'Video' && validURLVideo(encodeURI(fileNameEn))){
                    error.type = ""
                    error.fileNameEn = ""
                }
                if(type.display === "Video" && !validURLVideo(encodeURI(fileNameEn))){
                    valid = true
                    error.type = ""
                    error.fileNameEn = "File/Video must be a video URL"
                }
                if(type.display === 'Document' && validURLPDF(encodeURI(fileNameEn))){
                    error.type = ""
                    error.fileNameEn = ""
                }
                if(type.display === 'Document' && !validURLPDF(encodeURI(fileNameEn))){
                    valid = true;
                    error.type = ""
                    error.fileNameEn = "File/Video must be a Pdf URL"
                }
            }else{
                valid = true;
                error.type = "Vui lòng chọn Định dạng"
                error.fileNameEn = ""
            }
        }
            setError({...error})
            return valid
        }
    const handleSubmit = async (e:any) => {
        e.preventDefault()
        
        let tv:any;
        
        if(file){
            tv = file;

        }else{
            tv = fileName;
        }
        let en:any;
        if(fileEn){
            en = fileEn;
         }else{
            en = fileNameEn;
         }
       
        if(validate()){
            //focus tab en
            if(error.fileNameEn !== "" || error.titleEn !==""){
                setSelected(2)
            }
            //focus tab vn
            if(error.fileName !== "" || error.title !==""){
                setSelected(1)
            }
           
        }else{
            
            handleLoading(true)
            const body = {title,titleEn,idScreen,linkfile:tv,linkfileEn:en,type:type.code}
            // console.log("body==============",body);
            const data = new FormData()
            data.append('file', body.linkfile)
            data.append('fileEn', body.linkfileEn)
            data.append('title',body.title)
            data.append('titleEn',body.titleEn)
            data.append('idScreen',body.idScreen)
            data.append('type',body.type)

            const origin = window.location.origin
            data.append('origin',origin)
            console.log("form_data=========",data)
            const res:any = await fetch(`/api/admin/user-guide/${userGuideId}`, {
                method: 'POST',
                body: data,
            })
            const json = await res.json()
            handleLoading(false)
            if (!res.ok) {
                // console.log("update user guide err",json.message)
                const result = {
                    status : false,
                    mode:  isEdit?"edit":"add",
                    message: json.message
                }
                onSuccessForm(result)
                return;
            }
                const result = {
                    status : true,
                    mode:  isEdit?"edit":"add",
                    message: json.message
                }
                onSuccessForm(result)
                return;
            }
    }
    const onChangeFile = (e:any) => {
        e.preventDefault();
        // console.log("file", e.target.files[0]);
        setFileName(e.target.files[0].name)
        setFile(e.target.files[0])
    }
    const onChangeFileEn = (e:any) => {
        e.preventDefault();
        // console.log("file", e.target.files[0]);
        setFileNameEn(e.target.files[0].name)
        setFileEn(e.target.files[0])
    }
    
    const handleChangeFilePath = (e:any)=>{
        e.preventDefault()
        setFileName(e.target.value)
        setFile(null)
        error.fileName=""
        setError({...error})
        
    }
    const handleChangeFilePathEn = (e:any)=>{
        e.preventDefault()
        setFileNameEn(e.target.value)
        setFileEn(null)
        error.fileNameEn=""
        setError({...error})
        
    }
    const handleSelectTV = (e:any)=>{
        e.preventDefault()
        setSelected(1)
    }
    const handleSelectEN = (e:any)=>{
        e.preventDefault()
        setSelected(2)
    }
    const handleOnDelete = (e:any,userGuideId:any) => {
        e.preventDefault()
       onDeleted(userGuideId)
    }
    const handleScreenChange = (val:any) =>{
        // console.log("value screen", val);
        setIdScreen(val)
        error.screen=""
        setError({...error})
    }
    const handleTypeChange = (val:any) =>{
        // console.log("value type", val)
        error.type = ''
        setError({...error})
        if(val == 1){
            // console.log("value type ===", val)
            setType({code:1,display:'Video'})
        }else{
            setType({code:2,display:'Document'})
        }
    }
    return (
        <div>
            
            <form
            className="form-data" 
            >
                {/* Thông tin chung */}
                <div className="info-general">
                    <div className="info-general__title">
                        <span>Thông tin chung</span>
                    </div>
                    <div className="info-general__group">
                        {/* Màn hình */}
                        <div className="form-data__item" >
                            <div className="item-title">
                                <span>Màn hình</span>
                            </div>
                            <div className="item-body">
                                <div className="form-data__item__dropdown" style={{zIndex:'6'}}>
                                    <div className="form-data__field-dropdown">
                                        {/* <div className="form-data__field-dropdown__item">
                                            <span>{screen || "Select Screen"}</span>
                                            <img src="/images/icons/ic-chevron-down-dark.png" alt="toggle" />
                                        </div> */}
                                        <SelectPicker 
                                            className="form-data__field-dropdown__item"
                                            data={dataScreen}
                                            label="Màn hình"
                                            defaultValue={screenData}
                                            onChange={handleScreenChange}
                                        />
                                        <div className="icon-required">
                                            <img src="/pt/images/icons/ic-required.svg" alt="required"/>
                                        </div>
                                    </div>
                                    {/* <Dropdown className="screen__dropdown" >
                                        {dataScreen.map(item=>(<Dropdown.Item key={item.code} onSelect={()=>handleSelectScreen(item)}>{item.display}</Dropdown.Item>))}
                                    </Dropdown> */}
                                    
                                    
                                </div>
                                <div className="error-message">
                                    {(<span>{error.screen}</span>)}
                                </div>
                            </div>
                        </div>
                        {/* Định dạng */}
                        <div className="form-data__item">
                            <div className="item-title">
                                <span>Định dạng</span>
                            </div>
                            <div className="item-body">
                                <div className="form-data__item__dropdown">
                                    <div className="form-data__field-dropdown">
                                        {/* <div className="form-data__field-dropdown__item">
                                            <span>{type || "Select Type"}</span>
                                            <img src="/images/icons/ic-chevron-down-dark.png" alt="toggle"/>
                                            
                                        </div> */}
                                        <SelectPicker 
                                            className="form-data__field-dropdown__item"
                                            data={dataType}
                                            label="Định dạng"
                                            defaultValue={type}
                                            onChange={handleTypeChange}
                                        />
                                        <div className="icon-required">
                                            <img src="/pt/images/icons/ic-required.svg" alt="required"/>
                                        </div>
                                    </div>
                                    {/* <Dropdown className="type__dropdown">
                                        {dataType.map(item =>(<Dropdown.Item key={item.value} onSelect={()=>handleSelectType(item)}>{item.label}</Dropdown.Item>))}
                                    </Dropdown> */}
                                </div>
                                <div className="error-message">
                                {(<span>{error.type}</span>)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {/* Thông tin theo loại ngôn ngữ */}
                <div className="info-type-language">
                    <div className="info-type-language__title"><span>Thông tin theo loại ngôn ngữ</span></div>
                    <div>
                        <button style={selected === 1 ? {background:'#fff',color:'#6666BB'} : {background:'#D4D4DD',color:'#606060'}} className="info-btn" onClick={handleSelectTV}>Tiếng Việt</button>
                        <button style={selected === 2 ? {background:'#fff',color:'#6666BB'} : {background:'#D4D4DD',color:'#606060'}} className="info-btn" onClick={handleSelectEN}>Tiếng Anh</button>
                    </div>
                    <div className="info-type-language__group">
                        {selected === 1 ? (
                            <div>
                                {/* Tên tài liệu */}
                                <div className="form-data__item">
                                    <div className="item-title">
                                        <span>Tên tài liệu</span>
                                    </div>
                                    <div className="item-body">
                                        <div className="input-container">
                                            <div className="input-item">
                                                <input value={title} name={title} 
                                                onChange={(e)=>{
                                                    setTitle(e.target.value)
                                                    error.title = ""
                                                    setError({...error})
                                                }} 
                                                maxLength={100}/>
                                            </div>
                                            <div className="input-icon-required">
                                                <img src="/pt/images/icons/ic-required.svg" alt="required"/>
                                            </div>
                                        </div>
                                        <div  className="error-message">
                                            {(<span>{error.title}</span>)}
                                        </div>
                                    </div>
                                </div>
                                {/* File/Video */}
                                <div className="form-data__item">
                                    <div className="item-title">
                                        <span>File/Video</span>
                                    </div>
                                    <div className="item-body">
                                        <div className="upload-container">
                                            <div className="item-upload">
                                                <input value={fileName} name={fileName} onChange={handleChangeFilePath}/>
                                                <div className="btn-upload">
                                                    <img src="/images/icons/ic-upload-admin.png" alt="upload"/>
                                                    <input type='file'
                                                    ref={fileUpload} 
                                                    name={fileName} 
                                                    id='upload-file' 
                                                    accept=".pdf,video/mp4" 
                                                    onChange={onChangeFile} 
                                                    onClick={(e:any)=> { 
                                                        e.target.value = null
                                                    }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="upload-icon-required">
                                                <img src="/pt/images/icons/ic-required.svg" alt="required"/>
                                            </div>
                                        </div>
                                        
                                    
                                        {error.fileName ? 
                                        (
                                        <div  className="error-message last">
                                            <span>{error.fileName}</span>
                                        </div>
                                        ) : 
                                        (
                                            <div className="message-notifi">
                                                <img src="/pt/images/icons/ic-notifi.svg" alt="icon-notifi" />
                                                <span >Post link file hoặc nhấn nút upload để upload file</span>
                                            </div>
                                        )}
                                        
                                    </div>
                                </div>
                            </div>
                        ) : (
                            <div>
                                {/* Tên tài liệu */}
                                <div className="form-data__item">
                                    <div className="item-title">
                                        <span>File Name</span>
                                    </div>
                                    <div className="item-body">
                                        <div className="input-container">
                                            <div className="input-item">
                                                <input value={titleEn} name={titleEn} 
                                                onChange={(e)=>{
                                                    setTitleEn(e.target.value)
                                                    error.titleEn = ""
                                                    setError({...error})
                                                }} 
                                                maxLength={100}/>
                                            </div>
                                            <div className="input-icon-required">
                                                <img src="/pt/images/icons/ic-required.svg" alt="required"/>
                                            </div>
                                        </div>
                                        <div  className="error-message">
                                            {(<span>{error.titleEn}</span>)}
                                        </div>
                                    </div>
                                </div>
                                {/* File/Video */}
                                <div className="form-data__item">
                                    <div className="item-title">
                                        <span>File/Video</span>
                                    </div>
                                    <div className="item-body">
                                        <div className="upload-container">
                                            <div className="item-upload">
                                                <input value={fileNameEn} name={fileNameEn} onChange={handleChangeFilePathEn}/>
                                                <div className="btn-upload">
                                                    <img src="/images/icons/ic-upload-admin.png" alt="upload"/>
                                                    <input type='file' 
                                                    name={fileNameEn} 
                                                    id='upload-file-en' 
                                                    accept=".pdf,video/mp4" 
                                                    onChange={onChangeFileEn} 
                                                    onClick={(e:any)=> { 
                                                        e.target.value = null
                                                    }}
                                                    />
                                                </div>
                                            </div>
                                            <div className="upload-icon-required">
                                                <img src="/pt/images/icons/ic-required.svg" alt="required"/>
                                            </div>
                                        </div>
                                        
                                    
                                        {error.fileNameEn ? (
                                            <div  className="error-message last">
                                                <span>{error.fileNameEn}</span>
                                            </div>
                                        
                                        ) : 
                                        (
                                            <div className="message-notifi">
                                                <img src="/pt/images/icons/ic-notifi.svg" alt="icon-notifi" />
                                                <span >Post the file link or press the upload button to upload the file</span>
                                            </div>
                                        )}
                                        
                                    </div>
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            
                <div className="form-data__action">
                    <button onClick={handleSubmit} className="action__save">Lưu</button>
                   {isEdit &&  <button onClick={(e)=>handleOnDelete(e,data.id)} className="action__close">Xóa</button>}
                </div>
            </form>
        </div>
     );
}

export default FormDataModal;
function validURLVideo(str:string) {
    const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
      
  const allowedExtensions =
  /(\.mp4|\.m4v)$/i;
    if(!!pattern.test(str) && allowedExtensions.exec(str)){
        return true
    }else{
        return false
    }
  }
  function validURLPDF(str:string) {
    const pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|'+ // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
      '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
      
  const allowedExtensions =
  /(\.pdf)$/i;
    if(!!pattern.test(str) && allowedExtensions.exec(str)){
        return true
    }else{
        return false
    }
  }
function validFilePDF(file:any){
    if(file.type === 'application/pdf'){
        return true
    }else{
        return false
    }
}
function validFileVideo(file:any){
    if (file.type === "video/mp4") {
        return true;
    }else{
        return false
    }
}
