import { Button, Modal } from 'rsuite'

import FormDataModal from '../FormData';

function ModalCusTomAdmin({onCloseModal, onDetele, className='', data,onSuccessForm}:any) {
    // console.log("isOpenEdit",isOpenEdit)
    const handleOnClose = () => {
        data = null;
        onCloseModal(false)
    }
    const isEdit = data && data.id>0;
    return ( 
        <Modal  
        className={`add ${className} add-modal-custom`}
        open={true}
        style={{
            overflowY:'hidden',
            maxHeight:'100%',
            background: "rgba(39,44,54,0.3)"
            // zIndex:'1049'
        }}
        onClose={handleOnClose} 
        backdrop={false}
        >
            <Modal.Body>
                <div className="modal-header" >
                    <span>{isEdit?"Cập nhật":"Thêm mới"}</span>               
                    <Button className="modal-toggle" onClick={handleOnClose}>
                    <img src="/images/icons/ic-close-admin.png" alt="close" />
                    </Button>
                </div>
                <div className='modal-sub-content'>
                    <FormDataModal dataForm={data} onDeleted={onDetele} onSuccessForm ={onSuccessForm}/>
                </div>
            </Modal.Body>
        </Modal>
     );
}

export default ModalCusTomAdmin;