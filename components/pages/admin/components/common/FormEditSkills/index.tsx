import { KeyboardEvent, useCallback, useEffect, useRef, useState } from "react";

import { useForm } from "react-hook-form";

import { callApi } from "api/utils";
import useNoti from "hooks/useNoti";
import { removeVNCharacter } from "utils/string";

export default function FormEditSkills({
  data,
  dataAll,
  isEdit,
  onCloseModal,
  isReload,
}: any) {
    const formRef = useRef(null)
    const {getNoti} = useNoti()
    const {
        register, 
        formState: { errors },
        setValue,
        handleSubmit,
    } = useForm<any>()

    const initData = {id: '', name: '', priority: ''}
    const [defaultValue, setDefaultValue] = useState(isEdit? data: initData);

    useEffect (() =>{
        register('id', {
            maxLength: {
                value: 3,
                message: 'Độ dài không được lớn hơn 3 ký tự',
            },
            validate: (value) => {
                if(!value) return 'Vui lòng nhập Mã kỹ năng'
                if(value.length >3 ) return 'Độ dài mã không được lớn hơn 3 ký tự'
                const index = dataAll.findIndex((item: any) => value === item.id && !isEdit)
                return index == -1 || 'Mã kỹ năng này đã có trong hệ thống'
              },
            value: data?.id || ''
        })
        register('name', {
            required: "Vui lòng nhập Tên kỹ năng",
            maxLength: {
                value: 100,
                message: 'Độ dài không được lớn hơn 100 ký tự',
            },
            value: data?.name || ''
        })
        register('priority', {
            required: "Vui lòng nhập Độ ưu tiên",
            min: {
                value: 1,
                message: 'Giá trị Độ ưu tiên phải lớn hơn 0',
            },
            max: {
                value: 1000,
                message: 'Giá trị Độ ưu tiên không được lớn hơn 1000',
            },
            value: data?.priority || ''
        })
    })
    
    const onChangeId = (e: any) => {
        const newId = removeVNCharacter(e.target.value.trim()).toUpperCase()
        e.target.value = newId;
        setValue('id', newId, {shouldValidate:true})
    }

    const onChangeName = (e: any) => {
        const newName = e.target.value.trim()
        setValue('name', newName, {shouldValidate:true})
    }
    const onChangePriority = (e: any) => {
        const newPriority = e.target.value
        setValue('priority', newPriority, {shouldValidate:true})     
    }
    const onSubmit = async (formData: any) =>{
        const response: any = await callApi(
            `/api/admin/skills/${isEdit? data.id : '-1'}`,
            isEdit ? 'put' : 'post',
            'token',
            {
                ...formData
            }
        )
        if(response){
            if(response?.code === 1){
                isReload(true)
                onCloseModal(true)
                getNoti('success', 'topCenter', `${isEdit? 'Cập nhật': 'Tạo mới'} kỹ năng thành công`)
            }
            if(response?.code === 0){
                getNoti('error', 'topCenter', `${response?.message}`)
            }
        }else{
            getNoti('error', 'topCenter', `Có lỗi xảy ra, không thể ${isEdit ? 'cập nhật' : 'tạo mới'} kỹ năng`)
      return
        }
    }
    const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
        const key = event.key
        const keyCode = event.keyCode
        if(key && (key.toLowerCase() == "tab") ){
          return;
        }
        if (
          key === '!' ||
          key === '@' ||
          key === '#' ||
          key === '$' ||
          key === '%' ||
          key === '^' ||
          key === '&' ||
          key === '*' ||
          key === '(' ||
          key === ')' ||
          key === '{' ||
          key === '}' ||
          key === '[' ||
          key === ']'
        ) {
          event.preventDefault()
        }
        if (
          !(
            (keyCode >= 48 && keyCode <= 57) ||
            (keyCode >= 65 && keyCode <= 90) ||
            (keyCode >= 97 && keyCode <= 122) ||
            keyCode == 231
          ) &&
          keyCode != 8 &&
          keyCode != 32
        ) {
          event.preventDefault()
        }
    }, [])

    const onKeyDownPriority = (event: KeyboardEvent<HTMLInputElement>) => {
        const key = event.key;
        if(event.key === 'e' || event.key === '.' || event.key === '+' || event.key === '-'){
            event.preventDefault()
        }
        if( ! key.match(/[a-z0-9]/g) || event.ctrlKey.valueOf()){
            event.preventDefault();
        }
    }

    return (
        <form
            ref={formRef}
            onSubmit={handleSubmit(onSubmit)}
        >
            <div className="form-edit-field-data__input">
                <div className="input-table">
                    <div className="item-field-title">
                        <span>Mã kỹ năng</span>
                    </div>
                    <div className="item-field-body">
                        <div className="field-input-container">
                            <div className="input-section">
                                <input
                                    readOnly={isEdit ? true : false}
                                    type='text'
                                    defaultValue={defaultValue.id}
                                    onKeyDown={onKeyDown}
                                    onChange={onChangeId}
                                    className={`${errors['id']?.message ? 'error' : ''}`}
                                />
                            </div>
                            {!isEdit && (<div className="input-icon-required">
                                <img src="/pt/images/icons/ic-required.svg" alt="required" />
                            </div>)}
                        </div>
                        <div className="error-message">
                            <span>{errors['id']?.message}</span>
                        </div>
                    </div>
                </div>

                <div className="input-table">
                    <div className="item-field-title">
                        <span>Tên kỹ năng</span>
                    </div>
                    <div className="item-field-body">
                        <div className="field-input-container">
                            <div className="input-section">
                                <input
                                    defaultValue={defaultValue.name}
                                    onChange={onChangeName}
                                    className={`${errors['name']?.message ? 'error' : ''}`}
                                />
                            </div>
                            <div className="input-icon-required">
                                <img src="/pt/images/icons/ic-required.svg" alt="required" />
                            </div>
                        </div>
                        <div className="error-message">
                            <span>{errors['name']?.message}</span>
                        </div>
                    </div>
                </div>

                <div className="input-table">
                    <div className="item-field-title">
                        <span>Độ ưu tiên</span>
                    </div>
                    <div className="item-field-body">
                        <div className="field-input-container">
                            <div className="input-section">
                                <input
                                    type='number'
                                    defaultValue={defaultValue.priority}
                                    onKeyDown={onKeyDownPriority}
                                    onChange={onChangePriority}
                                    // maxLength={3}
                                    className={`${errors['priority']?.message ? 'error' : ''}`}
                                />
                            </div>
                            <div className="input-icon-required">
                                <img src="/pt/images/icons/ic-required.svg" alt="required" />
                            </div>
                        </div>
                        <div className="error-message">
                            <span>{errors['priority']?.message}</span>
                        </div>
                    </div>
                </div>
                <div className="confirm__action">
                    <button 
                        type={"submit"}
                        className="action__save"
                    >Lưu</button>
                </div>
            </div>
        </form>
    )
}
