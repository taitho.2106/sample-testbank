import { KeyboardEvent, useCallback, useEffect, useRef, useState } from 'react'

import { useForm } from 'react-hook-form'

import { callApi } from 'api/utils'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import useGrade from 'hooks/useGrade'
import useNoti from 'hooks/useNoti'
import useSeries from 'hooks/useSeries'
import { filterSeriesByGrade } from 'utils/series/filterSeriesByGrade'
import { removeVNCharacter } from 'utils/string'

export default function FormEditThirdParty({ data, itemModify, isEdit, onCloseModal, isReload }: any) {
  const formRef = useRef(null)
  const { getGrade } = useGrade()
  const { getSeries } = useSeries()
  const { getNoti } = useNoti()
  const {
    register,
    formState: { errors },
    setValue,
    handleSubmit,
  } = useForm<any>()
  const [gradeId, setGradeId] = useState('')
  const [listSeries, setListSeries] = useState([])

  const initialData = {
    code_id: '',
    display: '',
    priority: '',
    grade_id: '',
    series_id: '',
    child_app_id: '',
    secret_key: '',
  }
  const defaultValue = isEdit ? itemModify : initialData
  useEffect(() => {
    register('code_id', {
      required: 'Vui lòng nhập Mã đối tác',
      maxLength: {
        value: 10,
        message: 'Độ dài không được lớn hơn 10 ký tự',
      },
      validate: (value: string) => {
        const codeArray = data.map((item: any) => item.code_id)
        if (!isEdit && codeArray.includes(value)) {
          return 'Mã đối tác này đã có trong hệ thống'
        }
        return true
      },
      value: itemModify?.code_id || '',
    })
    register('display', {
      required: 'Vui lòng nhập Tên đối tác tích hợp',
      maxLength: {
        value: 100,
        message: 'Độ dài không được lớn hơn 100 ký tự',
      },
      value: itemModify?.display || '',
    })
    register('priority', {
      required: 'Vui lòng nhập Độ ưu tiên',
      min: {
        value: 1,
        message: 'Giá trị Độ ưu tiên phải lớn hơn 0',
      },
      max: {
        value: 1000,
        message: 'Giá trị Độ ưu tiên không được lớn hơn 1000',
      },
      value: itemModify?.priority || '',
    })
    register('child_app_id', {
      value: itemModify?.child_app_id || '',
      maxLength: {
        value: 20,
        message: 'Mã ứng dụng tích hợp không được lớn hơn 20 ký tự',
      },
    })
    register('secret_key', {
      value: itemModify?.secret_key || '',
      maxLength: {
        value: 255,
        message: 'Khoá bí mật không được lớn hơn 255 ký tự',
      },
    })
  }, [])
  
  useEffect(() => {
    if(getSeries.length ==0 || getGrade.length ==0 ) return;
   const data =  filterSeriesByGrade(getSeries, getGrade, gradeId)
   setListSeries(data)
  }, [gradeId, getSeries, getGrade])
  

  const handleOnChangeId = (e: any) => {
      const text = removeVNCharacter(e.target.value.trim())
      .toUpperCase();
    e.target.value = text
    setValue('code_id', text, { shouldValidate: true })
  }
  const handleOnChangeName = (e: any) => {
    const text = e.target.value.trim()
    setValue('display', text, { shouldValidate: true })
  }
  const handleOnChangePriority = (e: any) => {
    const _value = e.target.value
    setValue('priority', _value, { shouldValidate: true })
  }
  const handleOnChangeChildKey = (e: any) => {
    const _value = e.target.value
    setValue('child_app_id', _value, { shouldValidate: true })
  }
  const handleOnSelectGrade = (value: any) => {
    setGradeId(value)
    setValue('grade_id', value || '', { shouldValidate: value != '' })
  }
  const handleOnSelectSeries = (value: any) => {
    setValue('series_id', value || '', { shouldValidate: true })
  }
  const handleOnChangeSecretKey = (event: any) => {
    const _value = event.target.value
    setValue('secret_key', _value, { shouldValidate: true })
  }

  const onSubmit = async (formData: any) => {
    const response: any = await callApi(
      `/api/admin/third-party/${isEdit ? itemModify.code_id : '-1'}`,
      isEdit ? 'put' : 'post',
      'token',
      { ...formData },
    )
    if (response) {
      if (response?.code === 1) {
        isReload(true)
        onCloseModal(true)
        getNoti('success', 'topCenter', `${isEdit ? 'Cập nhật' : 'Tạo mới'} đối tác thành công`)
      }
      if (response?.code === 0) {
        getNoti('error', 'topCenter', `${response?.message}`)
      }
    }else{
        getNoti('error', 'topCenter', `Có lỗi xảy ra, không thể ${isEdit ? 'Cập nhật' : 'Tạo mới'} đối tác tích hợp`)
    }
  }
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    const key = event.key
    const keyCode = event.keyCode
    if(key && (key == "-" || key == "_" || key.toLowerCase() == "tab") ){
      return;
    }

    if (
      key === '!' ||
      key === '@' ||
      key === '#' ||
      key === '$' ||
      key === '%' ||
      key === '^' ||
      key === '&' ||
      key === '*' ||
      key === '(' ||
      key === ')' ||
      key === '{' ||
      key === '}' ||
      key === '[' ||
      key === ']'
    ) {
      event.preventDefault()
    }
    if (
      !(
        (keyCode >= 48 && keyCode <= 57) ||
        (keyCode >= 65 && keyCode <= 90) ||
        (keyCode >= 97 && keyCode <= 122) ||
        keyCode == 231
      ) &&
      keyCode != 8 &&
      keyCode != 32
    ) {
      event.preventDefault()
    }
  }, [])

  const onKeyDownPriority = (event: KeyboardEvent<HTMLInputElement>) => {
    const key = event.key
    if (event.key === 'e' || event.key === '.' || event.key === '+' || event.key === '-') {
      event.preventDefault()
    }
    if (!key.match(/[a-z0-9]/g) || event.ctrlKey.valueOf()) {
      event.preventDefault()
    }
  }
  const onPasteId = (event: any) => {
    event.preventDefault()
    let data = event.clipboardData.getData('text')
    if (data) {
      data = removeVNCharacter(data).replace(/[^a-zA-Z0-9\_\-]/g, '').toUpperCase()
      const newId = data.substring(0, 10)
      setValue('code_id', newId, {shouldValidate:true})
      event.currentTarget.value = newId
    }
  }
  return (
    <form className={`form-third-party`} ref={formRef} onSubmit={handleSubmit(onSubmit)}>
      <div className="wrapper-form-edit-field-data__input">
        <div className="input-table">
          <div className="item-field-title">
            <span>Mã đối tác</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className="input-section">
                <input
                  readOnly={isEdit ? true : false}
                  type="text"
                  defaultValue={defaultValue.code_id}
                  onKeyDown={onKeyDown}
                  onChange={handleOnChangeId}
                  onPaste={onPasteId}
                  maxLength={10}
                  className={`${errors.code_id?.message ? 'error' : ''}`}
                />
              </div>
              {!isEdit && (
                <div className="input-icon-required">
                  <img src="/pt/images/icons/ic-required.svg" alt="required" />
                </div>
              )}
            </div>
            {errors.code_id?.message ? (
              <div className="error-message">
                <span>{errors.code_id?.message}</span>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Tên đối tác</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className="input-section">
                <input
                  defaultValue={defaultValue.display}
                  onChange={handleOnChangeName}
                  maxLength={100}
                  className={`${errors.display?.message ? 'error' : ''}`}
                />
              </div>
              <div className="input-icon-required">
                <img src="/pt/images/icons/ic-required.svg" alt="required" />
              </div>
            </div>
            {errors.display?.message ? (
              <div className="error-message">
                <span>{errors.display.message}</span>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Độ ưu tiên</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className="input-section">
                <input
                  type="number"
                  defaultValue={defaultValue.priority}
                  onKeyDown={onKeyDownPriority}
                  onChange={handleOnChangePriority}
                  className={`${errors.priority?.message ? 'error' : ''}`}
                />
              </div>
              <div className="input-icon-required">
                <img src="/pt/images/icons/ic-required.svg" alt="required" />
              </div>
            </div>
            {errors.priority?.message ? (
              <div className="error-message">
                <span>{errors.priority.message}</span>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Khối lớp</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <SelectFilterPicker
                data={getGrade}
                isMultiChoice={false}
                inputSearchShow={false}
                isShowSelectAll={false}
                onChange={handleOnSelectGrade}
                className={`form-input col-4 ${errors.grade_id?.message ? 'error' : ''}`}
                label="Khối lớp"
                defaultValue={defaultValue?.grade_id}
                register={register('grade_id', {
                  validate: (value: any) => {
                    return true
                  },
                  required: 'Vui lòng chọn Khối lớp',
                })}
                name="grade"
                style={{ zIndex: 10 }}
              />
              <div className="input-icon-required">
                <img src="/pt/images/icons/ic-required.svg" alt="required" />
              </div>
            </div>
            {errors.grade_id?.message ? (
              <div className="error-message">
                <span>{errors.grade_id.message}</span>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Chương trình</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              {getGrade && getSeries ? (
                <SelectFilterPicker
                  data={listSeries}
                  isMultiChoice={false}
                  inputSearchShow={true}
                  isShowSelectAll={false}
                  onChange={handleOnSelectSeries}
                  className={`form-input col-4 ${errors.series_id?.message ? 'error' : ''}`}
                  label="Chương trình"
                  defaultValue={defaultValue?.series_id}
                  register={register('series_id', {
                    validate: (value: any) => {
                      return true
                    },
                    required: 'Vui lòng chọn Chương trình',
                  })}
                  name="series"
                  menuSize="md"
                />
              ) : (
                ''
              )}
              <div className="input-icon-required">
                <img src="/pt/images/icons/ic-required.svg" alt="required" />
              </div>
            </div>
            {errors.series_id?.message ? (
              <div className="error-message">
                <span>{errors.series_id.message}</span>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Mã ứng dụng tích hợp</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className="input-section">
                <input type={'text'} defaultValue={defaultValue.child_app_id} onChange={handleOnChangeChildKey} />
              </div>
            </div>
            {errors.child_app_id?.message ? (
              <div className="error-message">
                <span>{errors.child_app_id.message}</span>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Khoá bí mật</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className="input-section">
                <textarea rows={4} defaultValue={defaultValue.secret_key} onChange={handleOnChangeSecretKey}></textarea>
              </div>
            </div>
            {errors.secret_key?.message ? (
              <div className="error-message">
                <span>{errors.secret_key.message}</span>
              </div>
            ) : (
              ''
            )}
          </div>
        </div>

        <div className="confirm__action">
          <button type={'submit'} className="action__save">
            Lưu
          </button>
        </div>
      </div>
    </form>
  )
}
