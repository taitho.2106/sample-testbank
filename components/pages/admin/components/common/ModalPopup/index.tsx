import {  Modal } from "rsuite";

function ModalPopupAdmin({isOpenDel,onDelete,onBack,data, className=''}:any) {
    const handleOnClose = () => {
        onBack()
    }
    return ( 
            <Modal
            className={`popup ${className} popup-modal-custom`}
            open={isOpenDel}
            style={{
                overflowY:'hidden',
                background: "rgba(39,44,54,0.3)"
            }}
            onClose={handleOnClose}
            backdrop={false}
            >
                <Modal.Body>
                <div className='modal-sub-content'>
                    <div className="title-content">
                        <span>Bạn chắc chắn muốn xóa hướng dẫn này?</span>
                    </div>
                    <div className="action-btn">
                        <button onClick={()=>onDelete(data)}>Xóa</button>
                        <button onClick={handleOnClose}>Quay lại</button>
                    </div>
                </div>
                </Modal.Body>
            </Modal>
     );
}

export default ModalPopupAdmin;