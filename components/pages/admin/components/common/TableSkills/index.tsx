import { GoKebabVertical } from "react-icons/go";
import { Popover, Whisper } from "rsuite";

export default function TableSkills ({ data,  getDataEdit, dataDelete, isOpenDelete}:any) {
    const handleEdit = (item:any) => {
        getDataEdit(item)
    }
    const handleDelete = (item: any) => {
        isOpenDelete(true)
        dataDelete(item)
    }
    return ( 
        <div className="container-table-of-field">
            <table className="table-field">
                <thead className="table-field__header">
                    <tr>
                        <th>MÃ KỸ NĂNG</th>
                        <th>TÊN KỸ NĂNG</th>
                        <th>ĐỘ ƯU TIÊN</th>
                        <th></th>
                    </tr>
                </thead>
                {data && data.length > 0 ? (
                <tbody className="table-field__body">
                {data.map((item:any, index:number) =>(
                    <tr key={index}>
                        <td>{item.id}</td>
                        <td>{item.name}</td>
                        <td>{item.priority}</td>
                        <td>
                            <div className="btn-container">
                                <Whisper
                                    placement="leftStart"
                                    trigger="click"
                                    speaker={
                                        <Popover>
                                            <div className="table-action-popover">
                                                <div 
                                                    className="__item" 
                                                    onClick={()=>handleEdit(item)}
                                                >
                                                    <img src="/images/icons/ic-edit-admin.png" alt="edit"/>
                                                    <span>Chỉnh sửa</span>
                                                </div>
                                                <div 
                                                    className="__item" 
                                                    onClick={() => handleDelete(item)} 
                                                >
                                                    <img src="/images/icons/ic-delete-admin.png" alt="delete"/>
                                                    <span>Xóa</span>
                                                </div>
                                            </div>
                                        </Popover>
                                    }
                                >
                                    <button className="action-btn">
                                        <GoKebabVertical color="#6262BC" />
                                    </button>
                                </Whisper>
                            </div>
                        </td>
                    </tr>
                ))}
            </tbody>
            ) : (
                <tbody>
                    <tr style={{textAlign:'center'}}>
                        <td colSpan={7}>
                            <div className="no-data-user-guide">
                                <img src="/images/emty-result.png" alt=""></img>
                                <span>Không có dữ liệu</span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            )}
            </table>
        </div>
    )
}
