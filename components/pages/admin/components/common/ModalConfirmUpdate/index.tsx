import {  Modal } from "rsuite";

export default function ModalConfirmUpdate ({isReload, isOpenUpdateConfirm, onUpdateSeries, onClose, className=''}:any) {
    const handleOnClose = () => {
        onClose()
    }
    return ( 
        <Modal
            className={`popup ${className} popup-modal-custom`}
            open={isOpenUpdateConfirm}
            style={{
                overflowY:'hidden',
                background: "rgba(39,44,54,0.3)"
            }}
            onClose={handleOnClose}
            backdrop={false}
        >
            <Modal.Body>
                <div className='modal-sub-content'>
                    <div className="title-content">
                        <span>Bạn chắc chắn muốn cập nhật toàn bộ chương trình?</span>
                    </div>
                    <div className="action-btn">
                        <button 
                            onClick={()=>{
                                isReload(true)
                                onUpdateSeries()
                                handleOnClose()
                            }}
                        >
                            Cập nhật toàn bộ
                        </button>
                        <button onClick={handleOnClose}>
                            Quay lại
                        </button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    );
}
