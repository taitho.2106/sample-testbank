import { Button, Modal } from 'rsuite'

import FormEditSeries from '../FormEditSeries';

export default function ModalEditGrade({onCloseModal, className='', data, dataAll, isReload}:any) {
    const handleOnClose = () => {
        onCloseModal(false)
    }
    const isEdit = (data && data.id) ? true: false;
    return ( 
        <Modal  
            className={`edit-field-modal ${className} edit-field-modal-custom`}
            open={true}
            style={{
                overflowY:'hidden',
                maxHeight:'100%',
                background: "rgba(39,44,54,0.3)"
            }}
            onClose={handleOnClose} 
            backdrop={false}
        >
            <Modal.Body>
                <div className="modal-header" >
                    <span>{isEdit ? "Cập nhật" : "Thêm mới"} chương trình</span>               
                    <Button className="modal-toggle" onClick={handleOnClose}>
                        <img src="/images/icons/ic-close-admin.png" alt="close" />
                    </Button>
                </div>
                <div className='modal-sub-content'>
                    <FormEditSeries data={data} isEdit={isEdit} onCloseModal={onCloseModal} isReload={isReload} dataAll={dataAll}/>
                </div>
            </Modal.Body>
        </Modal>
    );
}
