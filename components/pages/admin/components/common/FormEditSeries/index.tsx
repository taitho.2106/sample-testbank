import { KeyboardEvent, useCallback, useEffect, useRef, useState } from "react";

import { useForm } from "react-hook-form";

import { callApi } from "api/utils";
import useNoti from "hooks/useNoti";
import { removeVNCharacter } from "utils/string";

export default function FormEditSeries({
  data,
  dataAll,
  isEdit,
  onCloseModal,
  isReload,
}: any) {
    const allId = dataAll.map((item: any) => item.id)
    const formRef = useRef(null)
    const {getNoti} = useNoti()
    const {
        register, 
        formState: { errors },
        setValue,
        handleSubmit,
    } = useForm<any>()

    const initData = {id: '', name: '', priority: ''}
    const [defaultValue, setDefaultValue] = useState(isEdit? data: initData)

    useEffect (() =>{
        register('id', {
            required: "Vui lòng nhập Mã chương trình",
            maxLength: {
                value: 3,
                message: 'Độ dài không được lớn hơn 3 ký tự',
            },
            value: data?.id || ''
        })
        register('name', {
            required: "Vui lòng nhập Tên chương trình",
            maxLength: {
                value: 100,
                message: 'Độ dài không được lớn hơn 100 ký tự',
            },
            value: data?.name || ''
        })
        register('priority', {
            required: "Vui lòng nhập Độ ưu tiên",
            min: {
                value: 1,
                message: 'Giá trị Độ ưu tiên phải lớn hơn 0',
            },
            max: {
                value: 1000,
                message: 'Giá trị Độ ưu tiên không được lớn hơn 1000',
            },
            value: data?.priority || ''
        })
    })
    
    const onChangeId = (e: any) => {
        const newId = removeVNCharacter(e.target.value.trim()).toUpperCase()
        e.target.value = newId
        if(newId.length === 0){
            setMessageError('Vui lòng nhập Mã chương trình')
        }
        else setMessageError('')
        for (let i = 0; i < allId.length; i++){
            if(newId === allId[i]){
                setMessageError('Mã chương trình này đã có trong hệ thống')
            }
            if(newId === 'NA'){
                setMessageError(`Mã này đã dùng cho 'Không xác định'`)
            }
            setValue('id', newId)
        }
    }

    const onChangeName = (e: any) => {
        const newName = e.target.value.trim()
        setValue('name', newName)
        if(newName.length === 0){
            setShowNameMsg('Vui lòng nhập Tên chương trình')
        }
        else setShowNameMsg('')
    }
    const onChangePriority = (e: any) => {
        const newPriority = e.target.value
        setValue('priority', newPriority)   
        if(newPriority.length === 0){
            setShowPriorityMsg('Vui lòng nhập Độ ưu tiên')
        }
        else setShowPriorityMsg('')
    }

    const [messageError, setMessageError] = useState('')
    const [showNameMsg, setShowNameMsg] = useState('')
    const [showPriorityMsg, setShowPriorityMsg] = useState('')
    const onSubmit = async (formData: any) =>{
        const response: any = await callApi(
            `/api/admin/series/${isEdit? data.id : '-1'}`,
            isEdit ? 'put' : 'post',
            'token',
            {
                ...formData
            }
        )
        if(response){
            if(response?.code === 1){
                isReload(true)
                onCloseModal(true)
                getNoti('success', 'topCenter', `${isEdit? 'Cập nhật': 'Tạo mới'} chương trình thành công`)
            }
            if(response?.code === 0){
                getNoti('error', 'topCenter', `${response?.message}`)
            }
        }
    }
    const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
        const key = event.key
        const keyCode = event.keyCode 
        setMessageError('')
        if ( key === '!' || key === '@'|| key === '#'|| key === '$'|| key === '%'|| key === '^'|| key === '&' || key === '*' || key === '(' || key === ')' || key === '{' || key === '}'|| key === '['|| key === ']'){
            event.preventDefault()
        }
        if ( !( (keyCode >= 48 && keyCode <= 57) 
        ||(keyCode >= 65 && keyCode <= 90) 
        || (keyCode >= 97 && keyCode <= 122) 
        || keyCode == 231 ) 
        && keyCode != 8 && keyCode != 32) {
            event.preventDefault();
        }
    }, [])

    useEffect(()=> {
        if(errors['id']?.message){
            setMessageError(errors['id']?.message)
        }
        if(errors['name']?.message ) {
            setShowNameMsg(errors['name']?.message) 
        } 
        if(errors['priority']?.message){
            setShowPriorityMsg(errors['priority']?.message)
        }
    },[errors])

    return (
        <form
            ref={formRef}
            onSubmit={handleSubmit(onSubmit)}
        >
            <div className="form-edit-field-data__input">
                <div className="input-table">
                    <div className="item-field-title">
                        <span>Mã chương trình</span>
                    </div>
                    <div className="item-field-body">
                        <div className="field-input-container">
                            <div className="input-section">
                                <input
                                    readOnly={isEdit ? true : false}
                                    type='text'
                                    defaultValue={defaultValue.id}
                                    onKeyDown={onKeyDown}
                                    onChange={onChangeId}
                                    maxLength={3}
                                    className={`${messageError ? 'error' : ''}`}
                                />
                            </div>
                            {!isEdit && (<div className="input-icon-required">
                                <img src="/pt/images/icons/ic-required.svg" alt="required" />
                            </div>)}
                        </div>
                        <div className="error-message">
                            <span>{messageError}</span>
                        </div>
                    </div>
                </div>

                <div className="input-table">
                    <div className="item-field-title">
                        <span>Tên chương trình</span>
                    </div>
                    <div className="item-field-body">
                        <div className="field-input-container">
                            <div className="input-section">
                                <input
                                    defaultValue={defaultValue.name}
                                    onChange={onChangeName}
                                    maxLength={100}
                                    className={`${showNameMsg ? 'error' : ''}`}
                                />
                            </div>
                            <div className="input-icon-required">
                                <img src="/pt/images/icons/ic-required.svg" alt="required" />
                            </div>
                        </div>
                        <div className="error-message">
                            <span>{showNameMsg}</span>
                        </div>
                    </div>
                </div>

                <div className="input-table">
                    <div className="item-field-title">
                        <span>Độ ưu tiên</span>
                    </div>
                    <div className="item-field-body">
                        <div className="field-input-container">
                            <div className="input-section">
                                <input
                                    type='number'
                                    defaultValue={defaultValue.priority}
                                    onKeyDown={(event)=> {
                                        if(event.key === 'e' || event.key === '.' || event.key === '+' || event.key === '-'){
                                            event.preventDefault()
                                        }
                                    }}
                                    onChange={onChangePriority}
                                    // maxLength={3}
                                    className={`${showPriorityMsg ? 'error' : ''}`}
                                />
                            </div>
                            <div className="input-icon-required">
                                <img src="/pt/images/icons/ic-required.svg" alt="required" />
                            </div>
                        </div>
                        <div className="error-message">
                            <span>{showPriorityMsg}</span>
                        </div>
                    </div>
                </div>
                <div className="confirm__action">
                    <button 
                        type={"submit"}
                        className="action__save"
                    >Lưu</button>
                </div>
            </div>
        </form>
    )
}