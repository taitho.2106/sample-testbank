import { KeyboardEvent, useCallback, useEffect, useRef, useState } from 'react'

import { useForm } from 'react-hook-form'

import { callApi } from 'api/utils'
import useNoti from 'hooks/useNoti'
import { removeVNCharacter } from 'utils/string'

export default function FormEditGrade({ data, dataAll, isEdit, onCloseModal, isReload }: any) {
  const formRef = useRef(null)
  const { getNoti } = useNoti()
  const {
    register,
    formState: { errors },
    setValue,
    handleSubmit,
  } = useForm<any>()

  const initData = { id: '', name: '', grade_eduhome_id: '', priority: '' }
  const [defaultValue, setDefaultValue] = useState(isEdit ? data : initData)

  useEffect(() => {
    register('id', {
      required: 'Vui lòng nhập Mã khối lớp',
      validate: (value) => {
        if(!value) return 'Vui lòng nhập Mã khối lớp'
        if(value.length >3 ) return 'Độ dài mã không được lớn hơn 3 ký tự'
        const index = dataAll.findIndex((item: any) => value === item.id && !isEdit)
        return index == -1 || 'Mã khối lớp đã tồn tại'
      },
      maxLength: {
        value: 3,
        message: 'Độ dài mã không được lớn hơn 3 ký tự',
      },
      value: data?.id || '',
    })
    register('name', {
      required: 'Vui lòng nhập Tên khối lớp',
      maxLength: {
        value: 100,
        message: 'Tên khối lớp không được lớn hơn 100 ký tự',
      },
      value: data?.name || '',
    })
    register('grade_eduhome_id', {
      min: {
        value: 1,
        message: 'Giá trị GradeID phải lớn hơn 0',
      },
      max: {
        value: 2147483647,
        message: 'GradeID không được lớn hơn 2147483647',
      },
      validate: (value) => {
        const index = dataAll.findIndex(
          (item: any) => value == item.grade_eduhome_id && (!isEdit || (isEdit && data.id != item.id)),
        )
        return index == -1 || 'GradeId đang được sử dụng'
      },
      value: data?.grade_eduhome_id || '',
    })
    register('priority', {
      required: 'Vui lòng nhập Độ ưu tiên',
      min: {
        value: 1,
        message: 'Giá trị Độ ưu tiên phải lớn hơn 0',
      },
      max: {
        value: 1000,
        message: 'Giá trị Độ ưu tiên không được lớn hơn 1000',
      },
      value: data?.priority || '',
    })
  })

  const onChangeId = (e: any) => {
    const newId = removeVNCharacter(e.target.value.trim()).toUpperCase()
    e.target.value = newId
    setValue('id', newId, {shouldValidate:true})
  }

  const onChangeName = (e: any) => {
    const newName = e.target.value.trim()
    setValue('name', newName, {shouldValidate:true})
  }

  const onChangePriority = (e: any) => {
    const newPriority = e.target.value = e.target.value.replace(/\D/g,'')
    setValue('priority', newPriority, {shouldValidate:true})
  }

  const onChangeGradeID = (e: any) => {
    const newGradeID = e.target.value = e.target.value.replace(/\D/g,'')
    setValue('grade_eduhome_id', newGradeID, {shouldValidate:true})
  }

  const onSubmit = async (formData: any) => {
    const response: any = await callApi(
      `/api/admin/grade/${isEdit ? data.id : '-1'}`,
      isEdit ? 'put' : 'post',
      'token',
      {
        ...formData,
      },
    )
    if (response) {
      if (response?.code === 1) {
        isReload(true)
        onCloseModal(true)
        getNoti('success', 'topCenter', `${isEdit ? 'Cập nhật' : 'Tạo mới'} khối lớp thành công`)
        return
      }
      if (response?.code === 0) {
        getNoti('error', 'topCenter', `${response?.message}`)
        return
      }
    } else {
      getNoti('error', 'topCenter', `Có lỗi xảy ra, không thể ${isEdit ? 'cập nhật' : 'tạo mới'} khối lớp`)
      return
    }
  }
  const onKeyDown = useCallback((event: KeyboardEvent<HTMLInputElement>) => {
    const key = event.key
    const keyCode = event.keyCode
    if(key && (key.toLowerCase() == "tab") ){
      return;
    }
    if (
      key === '!' ||
      key === '@' ||
      key === '#' ||
      key === '$' ||
      key === '%' ||
      key === '^' ||
      key === '&' ||
      key === '*' ||
      key === '(' ||
      key === ')' ||
      key === '{' ||
      key === '}' ||
      key === '[' ||
      key === ']'
    ) {
      event.preventDefault()
    }
    if (
      !(
        (keyCode >= 48 && keyCode <= 57) ||
        (keyCode >= 65 && keyCode <= 90) ||
        (keyCode >= 97 && keyCode <= 122) ||
        keyCode == 231
      ) &&
      keyCode != 8 &&
      keyCode != 32
    ) {
      event.preventDefault()
    }
  }, [])
  const onPasteId = (event: any) => {
    event.preventDefault()
    let data = event.clipboardData.getData('text')
    if (data) {
      data = removeVNCharacter(data).replace(/[^a-zA-Z0-9]/g, '').toUpperCase()
      const newId = data;
      setValue('id', newId, {shouldValidate:true})
      event.currentTarget.value = newId
    }
  }
  const isNumber = (value:string)=>{
    const reg = /^\d+$/;
    return reg.test(value)
  }
  const messageError = errors['id']?.message
  const showNameMsg = errors['name']?.message
  const showPriorityMsg = errors['priority']?.message
  const showGradeIdMsg = errors['grade_eduhome_id']?.message
  return (
    <form ref={formRef} onSubmit={handleSubmit(onSubmit)}>
      <div className="form-edit-field-data__input">
        <div className="input-table">
          <div className="item-field-title">
            <span>Mã khối lớp</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className={`input-section`}>
                <input
                  readOnly={isEdit ? true : false}
                  onKeyDown={onKeyDown}
                  defaultValue={defaultValue.id}
                  onChange={onChangeId}
                  onPaste={onPasteId}
                  className={`${messageError ? 'error' : ''}`}
                />
              </div>
              {!isEdit && (
                <div className="input-icon-required">
                  <img src="/pt/images/icons/ic-required.svg" alt="required" />
                </div>
              )}
            </div>
            <div className="error-message">
              <span>{messageError}</span>
            </div>
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Tên khối lớp</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className={`input-section`}>
                <input
                  defaultValue={defaultValue.name}
                  onChange={onChangeName}
                  maxLength={100}
                  className={`${showNameMsg ? 'error' : ''}`}
                />
              </div>
              <div className="input-icon-required">
                <img src="/pt/images/icons/ic-required.svg" alt="required" />
              </div>
            </div>
            <div className="error-message">
              <span>{showNameMsg}</span>
            </div>
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Độ ưu tiên</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className="input-section">
                <input
                  onKeyDown={(event) => {
                    if (event.key?.toLowerCase() === 'e' || event.key === '.' || event.key === '+' || event.key === '-') {
                      event.preventDefault()
                    }
                  }}
                  defaultValue={defaultValue.priority}
                  onChange={onChangePriority}
                  // max={100}
                  className={`${showPriorityMsg ? 'error' : ''}`}
                />
              </div>
              <div className="input-icon-required">
                <img src="/pt/images/icons/ic-required.svg" alt="required" />
              </div>
            </div>
            <div className="error-message">
              <span>{showPriorityMsg}</span>
            </div>
          </div>
        </div>

        <div className="input-table">
          <div className="item-field-title">
            <span>Grace ID</span>
          </div>
          <div className="item-field-body">
            <div className="field-input-container">
              <div className="input-section">
                <input
                  onKeyDown={(event) => {
                    if (event.key?.toLowerCase() === 'e' || event.key === '.' || event.key === '+' || event.key === '-') {
                      event.preventDefault()
                    }
                  }}
                  
                  defaultValue={defaultValue.grade_eduhome_id}
                  onChange={onChangeGradeID}
                  className={`${showGradeIdMsg ? 'error' : ''}`}
                />
              </div>
              <div className="input-icon-required">
              </div>
            </div>
            <div className="error-message">
              <span>{showGradeIdMsg}</span>
            </div>
          </div>
        </div>

        <div className="confirm__action">
          <button type={'submit'} className="action__save">
            Lưu
          </button>
        </div>
      </div>
    </form>
  )
}
