import {  Modal } from "rsuite";

export default function ModalDeleteSeries ({dataDelete, isOpenDelete, onDelete, onCloseDelete, className=''}:any) {
    const handleOnClose = () => {
        onCloseDelete()
    }
    return ( 
        <Modal
            className={`popup ${className} popup-modal-custom`}
            open={isOpenDelete}
            style={{
                overflowY:'hidden',
                background: "rgba(39,44,54,0.3)"
            }}
            onClose={handleOnClose}
            backdrop={false}
        >
            <Modal.Body>
                <div className='modal-sub-content'>
                    <div className="title-content">
                        <span>Bạn chắc chắn muốn xóa thông tin chương trình này?</span>
                    </div>
                    <div className="action-btn">
                        <button 
                            onClick={()=>{
                                onDelete(dataDelete)
                                handleOnClose()
                            }}
                        >
                            Xóa
                        </button>
                        <button onClick={handleOnClose}>
                            Quay lại
                        </button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
    );
}
