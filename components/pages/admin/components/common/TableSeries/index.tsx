import { useState } from "react";

import { GoKebabVertical } from "react-icons/go";
import { Popover, Whisper } from "rsuite";

import { ImageZooming } from "practiceTest/components/molecules/modals/imageZooming";
import useGrade from "hooks/useGrade";

export default function TableSeries ({ data,  getDataEdit, dataDelete, isOpenDelete}:any) {
    const [isZoomKey, setIsZoomKey] = useState(false)
    const [zoomStrKey, setZoomStrKey] = useState('')
    const {getGrade} = useGrade()
    const seriesData = data.filter((s:any) => s.name !== 'Không xác định')
    const handleEdit = (item:any) => {
        getDataEdit(item)
    }
    const handleDelete = (item: any) => {
        isOpenDelete(true)
        dataDelete(item)
    }
    const getDisplayByCode=(list:any[], code:any)=>{
        const item = list.find(m=>m.code == code);
        if(item) return item.display;
        return ""
    }
    return ( 
        <div className="container-table-of-series">
            <table className="table-series">
                <thead className="table-series__header">
                    <tr>
                        <th>TÊN CHƯƠNG TRÌNH</th>
                        <th>SÁCH MINH HỌA</th>
                        <th>SERIES EDUHOME ID</th>
                        <th>GRADE</th>
                        <th>LOẠI SÁCH</th>
                        <th>ĐỘ ƯU TIÊN</th>
                        <th></th>
                    </tr>
                </thead>
                {seriesData && seriesData.length > 0 ? (
                <tbody className="table-series__body">
                {zoomStrKey.length > 0 && (
                    <ImageZooming
                        data={{
                            alt: 'image_series',
                            src: `${zoomStrKey}`,
                        }}
                        status={{
                            state: isZoomKey,
                            setState: setIsZoomKey,
                        }}
                        />
                )}
                {seriesData.map((item:any, index:number) =>(
                    <tr key={index} data-id={item.id}>
                        <td>{item.name}</td>
                        <td>
                            {item.image_url && (
                                <img 
                                    alt=''
                                    src={item.image_url}
                                    onClick={() => {
                                        setIsZoomKey(true)
                                        setZoomStrKey(item.image_url)
                                    }}
                                /> 
                            )}                               
                        </td>
                        <td>{item.series_eduhome_id}</td>
                        <td>{getDisplayByCode(getGrade,item.grade_id)}</td>
                        <td>{item.series_type === 1 ? 'SGK' : 'Khác'}</td>
                        <td>{item.priority}</td>
                        <td>
                            <div className="btn-container">
                                <Whisper
                                    placement="leftStart"
                                    trigger="click"
                                    speaker={
                                        <Popover>
                                            <div className="table-action-popover">
                                                <div 
                                                    className="__item" 
                                                    onClick={()=>handleEdit(item)}
                                                >
                                                    <img src="/images/icons/ic-edit-admin.png" alt="edit"/>
                                                    <span>Chỉnh sửa</span>
                                                </div>
                                                <div 
                                                    className="__item" 
                                                    onClick={() => handleDelete(item)} 
                                                >
                                                    <img src="/images/icons/ic-delete-admin.png" alt="delete"/>
                                                    <span>Xóa</span>
                                                </div>
                                            </div>
                                        </Popover>
                                    }
                                >
                                    <button className="action-btn">
                                        <GoKebabVertical color="#6262BC" />
                                    </button>
                                </Whisper>
                            </div>
                        </td>
                    </tr>
                ))}
            </tbody>
            ) : (
                <tbody>
                    <tr style={{textAlign:'center'}}>
                        <td colSpan={7}>
                            <div className="no-data-user-guide">
                                <img src="/images/emty-result.png" alt=""></img>
                                <span>Không có dữ liệu</span>
                            </div>
                        </td>
                    </tr>
                </tbody>
            )}
            </table>
        </div>
    )
}
