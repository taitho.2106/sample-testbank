import { GoKebabVertical } from "react-icons/go"
import { Popover, Whisper } from "rsuite"

interface DataTable {
    data: any[]
    openModalDelete: any
    openModalEdit: any
    itemModify: any
}

export default function TableThirdParty( table: DataTable ){
    const handleItemEdit = (item: any) => { 
        table.itemModify(item) 
        table.openModalEdit(true)
    }
    const handleItemDelete = (item: any) => { 
        table.itemModify(item) 
        table.openModalDelete(true)
    }

    return(
    <div className="third-party-page container-table-of-field">
        <table className="table-field">
            <thead className="table-field__header">
                <tr>
                    <th>MÃ ĐỐI TÁC</th>
                    <th>TÊN ĐỐI TÁC</th>
                    <th>ĐỘ ƯU TIÊN</th>
                    <th>KHỐI LỚP</th>
                    <th>TÊN CHƯƠNG TRÌNH</th>
                    <th>MÃ ỨNG DỤNG TÍCH HỢP</th>
                    <th></th>
                </tr>
            </thead>
            <tbody className="table-field__body">
                {table.data ? table.data.map((item, index: number) => (
                    <tr key={index}>
                        <td>{item.code_id ?? '...'}</td>
                        <td>{item.display ?? '...'}</td>
                        <td>{item.priority ?? '...'}</td>
                        <td>{item.grade_display ?? '...'}</td>
                        <td>{item.series_display ?? '...'}</td>
                        <td>{item.child_app_id ?? '...'}</td>
                        <td>
                            <div className="btn-container">
                                <Whisper
                                    placement="leftStart"
                                    trigger="click"
                                    speaker={
                                        <Popover>
                                            <div className="table-action-popover">
                                                <div className="__item" 
                                                    onClick={() => handleItemEdit(item)}>
                                                    <img src="/images/icons/ic-edit-admin.png" alt="edit"/>
                                                    <span>Chỉnh sửa</span>
                                                </div>
                                                <div className="__item"
                                                    onClick={() => handleItemDelete(item)}>
                                                    <img src="/images/icons/ic-delete-admin.png" alt="delete"/>
                                                    <span>Xóa</span>
                                                </div>
                                            </div>
                                        </Popover>
                                    }>
                                    <button className="action-btn">
                                        <GoKebabVertical color="#6262BC" />
                                    </button>
                                </Whisper>
                            </div>
                        </td>
                    </tr>
                )) : <BodyEmptyData />}
            </tbody>
        </table>
    </div>)
}

const BodyEmptyData = () => {
    return(
        <tr style={{textAlign:'center'}}>
            <td colSpan={4}>
                <div className="no-data-user-guide">
                    <img src="/images/emty-result.png" alt=""></img>
                    <span>Không có dữ liệu</span>
                </div>
            </td>
        </tr>
    )
}