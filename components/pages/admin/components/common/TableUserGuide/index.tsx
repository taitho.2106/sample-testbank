import Link from "next/link";
import { GoKebabVertical } from "react-icons/go";
import { Popover, Whisper } from "rsuite";

import {formatDate} from "../../../../../../utils/string"

function TableUserGuide({ data,  getDataEdit, onDeleteData}:any) {
    const handleEdit = (item:any) => {
        getDataEdit(item)
    }
    return ( 
        <div className="container-table">
            <table className="table-user-screen">
            <thead className="table-user-screen__header">
                <tr>
                    <th>STT</th>
                    <th>TÊN TÀI LIỆU</th>
                    <th>MÀN HÌNH</th>
                    <th>ĐỊNH DẠNG</th>
                    <th>FILE/VIDEO</th>
                    <th>NGÀY TẠO</th>
                    <th></th>
                </tr>
            </thead>
            {data && data.length > 0 ? (
                <tbody className="table-user-screen__body">
                {data.map((item:any, index:number) =>(
                    <tr key={index}>
                    <td>{index+1}</td>
                    <td>{item.title}</td>
                    <td>{item.screen}</td>
                    <td>{item.type_data===1?"Video":"Document"}</td>
                    <td><Link href={item.linkfile || ""} passHref><a target="_blank" title={item.linkfile || ""} rel="noopener noreferrer">{item.linkfile}</a></Link></td>
                    <td>{item.created_date? formatDate(item.created_date,true):""}</td>
                    <td>
                        <div className="btn-container">
                            <Whisper
                            placement="leftStart"
                            trigger="click"
                            speaker={
                                <Popover>
                                    <div className="table-action-popover">
                                        <div className="__item" onClick={()=>handleEdit(item)}>
                                            <img src="/images/icons/ic-edit-admin.png" alt="edit"/>
                                            <span>Chỉnh sửa</span>
                                        </div>
                                        <div className="__item" onClick={()=>onDeleteData(item.id)}>
                                            <img src="/images/icons/ic-delete-admin.png" alt="delete"/>
                                            <span>Xóa</span>
                                        </div>
                                    </div>
                                </Popover>
                            }
                            >
                            <button className="action-btn">
                              <GoKebabVertical color="#6262BC" />
                            </button>
                            </Whisper>
                        </div>
                    </td>
                </tr>
                ))}
            </tbody>
            ) : (
                <tbody>
                <tr style={{textAlign:'center'}}>
                       <td colSpan={7}>
                       <div className="no-data-user-guide">
                        <img src="/images/emty-result.png" alt=""></img>
                        <span>Không có dữ liệu</span>
                    </div>
                       </td>
                </tr>
                </tbody>
            )}
            
            </table>
        </div>
     );
}

export default TableUserGuide;