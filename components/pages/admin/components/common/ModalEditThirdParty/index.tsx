import { Button, Modal } from 'rsuite'
import FormEditThirdParty from '../FormEditThirdParty';

export default function ModalEditThirdParty({onCloseModal, className='', data, isReload, itemModify}:any) {
    const handleOnClose = () => { onCloseModal(false) }
    const isEdit = (itemModify && itemModify.code_id) ? true: false;

    return ( 
        <Modal  
            className={`edit-field-modal-third-party ${className} edit-field-modal-third-party-custom`}
            open={true}
            style={{ background: "rgba(39,44,54,0.3)" }}
            onClose={handleOnClose} 
            backdrop={false}>
            <Modal.Body>
                <div className="modal-header" >
                    <span>{isEdit ? "Cập nhật" : "Thêm mới"} đối tác</span>               
                    <Button className="modal-toggle" onClick={handleOnClose}>
                        <img src="/images/icons/ic-close-admin.png" alt="close" />
                    </Button>
                </div>
                <div className='modal-sub-content'>
                    <FormEditThirdParty 
                        data={data} 
                        itemModify={itemModify} 
                        isEdit={isEdit} 
                        onCloseModal={onCloseModal} 
                        isReload={isReload}/>
                </div>
            </Modal.Body>
        </Modal>
    );
}
