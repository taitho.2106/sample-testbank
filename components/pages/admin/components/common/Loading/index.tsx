import { useEffect, useState } from "react";

import { Modal } from "rsuite";

function LoadingAdmin() {
    const [width, setWidth] = useState(0)
    useEffect(()=>{
        setWidth(window.innerWidth)
    },[])
    const arr = []
    const b:any = width/14 + 1
    for(let i =1; i <= b ; i++){
        arr.push(i)
    }
    return ( 
        <Modal
            className="modal-loading"
            open={true}
            style={{
                overflowY:'hidden',
                // zIndex:'1049'
            }}
            >
            <div className="loading">
                <div id="noTrespassingOuterBarG">
                    <div id="noTrespassingFrontBarG" className="noTrespassingAnimationG">
                        {arr.map(item => (
                        <div key={item} className="noTrespassingBarLineG">
                        </div>
                        ))}
                    </div>
                </div>
            </div>
        </Modal>

     );
}

export default LoadingAdmin;