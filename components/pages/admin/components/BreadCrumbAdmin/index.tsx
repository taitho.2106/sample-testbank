import Link from 'next/link'
function BreadCrumbAdmin({data}:any) {
    return ( 
        <div className='a-breadcrumb-admin'>
            {data && data.map((item:any,i:number)=>(
                <div key={i} className='a-breadcrumb-admin__item'>
                    {item.url ? (
                        <Link href={item.url}>
                            <a><img src='/images/icons/ic-header-admin.png' alt='icon breadcrumb'/>{item.name}</a>
                        </Link>
                    ) : (
                        <span>{item.name}</span>
                    )}
                </div>
            ))}
        </div>
     );
}

export default BreadCrumbAdmin;