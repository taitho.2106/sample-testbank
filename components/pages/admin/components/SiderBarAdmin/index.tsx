
import { useState } from "react";

import Link from "next/link";
import { useRouter } from "next/router";

import { SIDENAV_ITEM_ADMIN, SIDENAV_ITEM_ICONS_ADMIN } from "interfaces/constants";


export default function SiderBarAdmin() {
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [selected, setSelected] = useState(1)
    const router = useRouter()

    const checkActive=(listPathName:any[])=>{
        return listPathName.indexOf(router.pathname)>=0
    }
    return ( 
    <>
        <aside className="o-sidebar__admin">
            <div className="o-sidebar__brand">
                <Link href="/admin">
                    <a>
                    <img
                        className="brand-logo"
                        src="/images/collections/clt-logo.png"
                        alt="brand logo testbank"
                    />
                    </a>
                </Link>
            </div>
            <nav>
                <div className="o-sidebar__sidenav">
                    {SIDENAV_ITEM_ADMIN && SIDENAV_ITEM_ADMIN.length > 0 && (
                        <>
                            <ul className="o-sidenav__list">
                                {SIDENAV_ITEM_ADMIN.map((item:any)=>{
                                    return (
                                    <li key={item.key} 
                                    className="list-item" 
                                    data-active={checkActive(item.pathName)} 
                                    onChange={()=> {
                                        return setSelected(item.key)
                                    }} 
                                    >
                                        <Link href={item.url}>
                                                <a style={{width:'100%'}} >
                                                    <NavbarItem data={item}/>
                                                </a>
                                        </Link>
                                    </li>
                                )})}
                            </ul>
                        </>
                    )}   
                </div> 
            </nav>
        </aside>
    </> 
    );
}

function NavbarItem({data}:any) {
    const router = useRouter()
    const [status, setStatus] = useState('light' as 'active' | "light"|'white')
    return ( 
    <div
        style={{display:'flex'}}
        onMouseEnter={() => {
            setStatus('white')
        }}
        onMouseLeave={() => {
            setStatus('light')
        }}
    >
        <img src={SIDENAV_ITEM_ICONS_ADMIN[data.icon][router.asPath === data.url ? "active" : status]} alt={data.label} className="list-item__icon"/>
        <div className="list-item__name">{data.label}</div>
    </div> 
    );
}


