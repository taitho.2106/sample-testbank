import { useContext, useEffect, useRef, useState } from "react";

import { signOut } from "next-auth/client";
import { useRouter } from "next/router";
import { Dropdown } from "rsuite";

import { WrapperAdminContext } from "interfaces/contexts";



const language = [
    {
        key: 1,
        label: 'VN',
        title:'VietNamese'
    },
    {
        key:2,
        label:'EN',
        title:'English'
    }
]

function HeaderAdmin() {
    const {pageTitle, userInfo} = useContext(WrapperAdminContext)
    const router = useRouter()
    const [selectLang, setSelectLang] = useState('VN')
    const imgRef = useRef(null);
    const avatar = userInfo?.avatar ? `/upload/${userInfo?.avatar}`:'/images/collections/clt-default-avatar.png';
    // console.log("avatar ===== ",avatar)
    useEffect(()=>{
        if(userInfo?.avatar){
            imgRef.current.setAttribute('src',avatar)
        }
    },[userInfo])

    const onLogOut = () => {
        signOut({ callbackUrl: '/', redirect: false })
      }
    
      const onUserInfo = () => {
        router.push('/users/info', '/users/info')
      }
     
    return ( 
    <header className="a-header" >
        {/* title page */}
        <div className="a-header__title">
            <img src="/images/icons/ic-header-admin.png" alt="title header admin"/>
            <h1>{pageTitle}</h1>
        </div>
        <div className="a-header__action">
            {/* Language */}
            <div className="action-language">
                <div className="action-langue-cover" >
                    <img src="/images/icons/ic-language-header.png" 
                    alt="languages" 
                    className="icon-language"
                    />
                    <span className="title-lang">{selectLang}</span>
                    <img src="/images/icons/ic-chevron-down.png" 
                    alt="icon-toggle-language" 
                    className='toggle-language'/>
                </div>
                <Dropdown className="action-language-dropdown" title="language" placement='bottomEnd'>
                    {language.map(item =>(<Dropdown.Item  key={item.key} onSelect={()=>setSelectLang(item.label)}>{item.title}</Dropdown.Item>))}
                </Dropdown>

            </div>
            {/* Profile */}
            <div className="action-profile" >
                <div className="action-profile-cover" >
                    <img
                    ref={imgRef}
                    src={avatar}
                    alt=""
                    style={{height:'35px', width:'35px'}}
                    className='icon-avatar-person'
                    />
                    <div className="person-info">
                        <div className="title-person-info">
                            <span className="title-person">{userInfo.user_name}</span>
                            <span className="role-person">{userInfo.is_admin === 1 ? 'Admin' : userInfo.user_role_name}</span>
                        </div>
                        <div>
                            <img
                            className="upper-toggle-admin"
                            src="/images/icons/ic-chevron-down.png"
                            alt="toggle"
                            />
                            
                        </div>
                    </div>
                </div>
                <Dropdown className="action-person-dropdown" title="default" placement='bottomEnd'>
                    <Dropdown.Item onSelect={onUserInfo}>Thông tin tài khoản</Dropdown.Item>
                    <Dropdown.Item onSelect={onLogOut}>Đăng xuất</Dropdown.Item>
                </Dropdown>
            </div>
        </div>
    </header>
    );
}

export default HeaderAdmin;