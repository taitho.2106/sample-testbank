import { WrapperAdminContext } from "@/interfaces/contexts";
import { callApi } from "api/utils";
import { useRouter } from "next/router"
import { useContext, useEffect, useState } from "react";
import { Button, Col, FlexboxGrid, Tooltip, Whisper } from "rsuite";
import ModalEditThirdParty from "../components/common/ModalEditThirdParty";
import ModalDeleteThirdParty from "../components/common/ModalDeleteThirdParty";
import TableThirdParty from "../components/common/TableThirdParty";
import useNoti from "hooks/useNoti";
import useSeries from "hooks/useSeries";
import useGrade from "hooks/useGrade";

export default function ThirdPartyPage(){
    let flagFetchData = true
    const rounter = useRouter()
    const {getNoti} = useNoti()
    const {getGrade} = useGrade()
    const {getSeries} = useSeries()
    const [dataTable, setDataTable] = useState([])
    const [dataModify, setDataModify] = useState<any>({})
    const [reloadEditModal, setReloadEditModal] = useState<boolean>(true)
    const [openEditModal, setOpenEditModal] = useState<boolean>(false)
    const [openDeleteModal, setOpenDeleteModal] = useState<boolean>(false)
    const {handleLoading} = useContext(WrapperAdminContext) 

    const fetchData = async () => {
        const response: any = await callApi('/api/admin/third-party/', 'GET', 'Token')
        if( response ) {
            [...response.data].map((item: any) => {
                item['series_display'] = getSeries.filter((i: any) => i.code == item.series_id)[0]?.display ?? '...'
                item['grade_display'] = getGrade.filter((i: any) => i.code == item.grade_id)[0]?.display ?? '...'
            })
            setDataTable([...response.data])
        }
        handleLoading(false)
    }
    useEffect(() => {
        if( flagFetchData ){
            handleLoading(true)
            fetchData()
            console.log('Fetch already!')
        }
        if( reloadEditModal ){
            fetchData()
            setReloadEditModal(false)
        }

        return () => {
            setDataTable([])
            flagFetchData = false
            console.log('Destroy fetching!')
        }
    },[rounter.asPath, reloadEditModal, getSeries, getGrade])

    const handleItemModify = ( data: any) => {
        if( data ){
            setDataModify(() => data)
        }
    }
    const handleOnCloseModalEdit = () => {setOpenEditModal(() => false)}
    const handleOnOpenModalEdit = () => {
        setDataModify(() => {})
        setOpenEditModal(() => true)
    }
    const handleOnCloseModalDelete = () => { 
        setDataModify(() => {})
        setOpenDeleteModal(() => false) 
    }
    
    const handleOnModalDelete = async () => { 
        if( dataModify ){
            let code_id = dataModify.code_id
            const result = await callApi(`/api/admin/third-party/${code_id}`,
                'DELETE',
                'token')
            if(result){
                setReloadEditModal(true)
                getNoti('success', 'topCenter', `Xóa đối tác thành công`)
            }
            else{
                getNoti('success', 'topCenter', `Chưa xóa được đối tác ${code_id}`)
            }
        }
    }

    return(<div className={`admin-thirdparty`}>
        <FlexboxGrid>
            {openEditModal ? 
                <ModalEditThirdParty 
                    onCloseModal={handleOnCloseModalEdit}
                    itemModify={dataModify} 
                    data={dataTable} 
                    isReload={setReloadEditModal}/> : ''}
            {openDeleteModal ? 
                <ModalDeleteThirdParty 
                    dataDelete={dataModify} 
                    isOpenDelete={openDeleteModal} 
                    onDelete={handleOnModalDelete} 
                    onCloseDelete={handleOnCloseModalDelete}/> : ''}
            <div className='action-field'>                        
                <div className="action-field__action">
                    <div className="action-field__action-btn" >
                        <Whisper
                            placement="bottom"
                            trigger="hover"
                            speaker={<Tooltip>Nhấn để thêm mới đối tác</Tooltip>}>
                            <Button
                                className="btn__main-btn"
                                style={{background:"#6868ac", width: 'max-content'}}
                                    onClick={() => handleOnOpenModalEdit()}>
                                <img src="/images/icons/ic-add-new.png" 
                                    alt="add-new"/>
                                    <span>Tạo mới</span>
                            </Button>
                        </Whisper>
                    </div>
                </div>
            </div>
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='table-field'>
                { getGrade.length > 0 && getSeries.length > 0 ? <TableThirdParty 
                    data={dataTable} 
                    itemModify={handleItemModify}
                    openModalEdit={setOpenEditModal}
                    openModalDelete={setOpenDeleteModal}/> : ''}
            </FlexboxGrid.Item>
        </FlexboxGrid>
    </div>)
}
