/* eslint-disable @typescript-eslint/no-unused-vars */

import { useEffect, useState } from "react";

import { useSession } from "next-auth/client";
import { useRouter } from "next/router";


import {WrapperAdminContext} from "interfaces/contexts";

import LoadingAdmin from "../components/common/Loading";
import HeaderAdmin from "../components/HeaderAdmin";
import SiderBarAdmin from "../components/SiderBarAdmin";

function WrapperAdmin({className='',userInfo,pageTitle='',children}:any) { 
  const [session] = useSession()
  const router = useRouter()
  const [loading, setLoading] = useState(false)
  const handleLoading = (value:boolean) => {
    setLoading(value)
  }
  useEffect(() => { 
    if (session === null) {
      router.push('/login')
    } else if (session) {
      const user:any = session.user;
      if(user.is_admin !=1){
        router.push(user.redirect)
      }
    } 
  }, [session])
  if (!session || ((session?.user)as any)?.is_admin !=1) return null
    return (
      <WrapperAdminContext.Provider value={{pageTitle, userInfo:session?.user, loading, handleLoading}}>
        <div className={`t-wrapper-admin ${className}`}>
        {loading ? <LoadingAdmin /> : null}
          <div className="t-wrapper-admin__sidebar">
            <SiderBarAdmin />
          </div>
          <div className="t-wrapper-admin__header">
            <HeaderAdmin />
          </div>
          <main className='t-wrapper-admin__main'>
            <div className="main-content">{children}</div>
          </main>
        </div>
        
      </WrapperAdminContext.Provider>
      );
  }
  
  export default WrapperAdmin
