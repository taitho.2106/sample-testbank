import { useContext, useEffect, useState } from "react";

import { Button, Col, FlexboxGrid, Tooltip, Whisper } from "rsuite";

import { callApi } from "api/utils";
import useNoti from "hooks/useNoti";
import { WrapperAdminContext } from "interfaces/contexts";

import ModalDeleteGrade from "../components/common/ModalDeleteGrade";
import ModalEditGrade from "../components/common/ModalEditGrade";
import TableGrade from "../components/common/TableGrade";

export default function GradePage() {
    const {getNoti} = useNoti()
    const [dataEdit, setDataEdit] = useState(null)

    const [isReload, setIsReload] = useState(false)
    const {handleLoading} = useContext(WrapperAdminContext)
    
    const [dataTable, setDataTable] = useState([])
    const [dataDelete, setDataDelete] = useState()
    const [isOpenEdit, setIsOpenEdit]= useState(false)
    const [isOpenDelete, setIsOpenDelete] = useState(false)

    const fetchData = async () => {
        const response: any = await callApi(
            `/api/admin/grade`,
            'get',
            'Token',
        )
        if (response) {
            setDataTable([...response.data])
            handleLoading(false)
        }
    }
        
    useEffect(() => {
        handleLoading(true)
        fetchData()
    }, [])
    useEffect(() =>{
        if(isReload){
            fetchData()
            setIsReload(false)
        }
    }, [isReload])
    
    const handleAddNewGrade = () => {
        setIsOpenEdit(true)
        setDataEdit(null)
    }

    const handleEditGrade = (dataEdit:any)=>{
        if(dataEdit){
            setIsOpenEdit(true)
            setDataEdit(dataEdit)
        }
    }

    const onCloseModal = ()=>{
        setIsOpenEdit(false)
    }

    const handleDeleteGrade = async (data:any)=>{
        if(data){
            const result = await callApi(`/api/admin/grade/${data.id}`,
                'DELETE',
                'token',
            )
            if(result){
                getNoti('success', 'topCenter', `Xóa khối lớp thành công `)
                setIsReload(true)
            }
            else{
                getNoti('success', 'topCenter', `Chưa xóa được khối lớp`)
            }
        }   
    }

    return (
        <FlexboxGrid justify="start">
            {isOpenEdit && <ModalEditGrade onCloseModal={onCloseModal} data={dataEdit} dataAll={dataTable} isReload={setIsReload} />}
            {isOpenDelete && <ModalDeleteGrade dataDelete={dataDelete} isOpenDelete={isOpenDelete} onDelete={handleDeleteGrade} onCloseDelete={setIsOpenDelete} />}
            <div className='action-field'>                        
                <div className="action-field__action">
                    <div className="action-field__action-btn" >
                        <Whisper
                            placement="bottom"
                            trigger="hover"
                            speaker={<Tooltip>Nhấn để tạo khối lớp mới</Tooltip>}
                        >
                            <Button
                                className="btn__main-btn"
                                style={{background:"#6868ac"}}
                                    onClick={() => {
                                    handleAddNewGrade()
                                }}
                            >
                                <img src="/images/icons/ic-add-new.png" 
                                    alt="add-new"
                                />
                                    <span>Tạo mới</span>
                            </Button>
                        </Whisper>
                    </div>
                </div>
            </div>

            {/* Table data */}
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='table-field'>
                {dataTable.length > 0 && (<TableGrade data={dataTable} getDataEdit={handleEditGrade} dataDelete={setDataDelete} isOpenDelete={setIsOpenDelete} isReload={setIsReload}/>)}
            </FlexboxGrid.Item>

        </FlexboxGrid> 
    )
}