import { useContext, useEffect, useState } from "react";

import { Button, Col, FlexboxGrid, Tooltip, Whisper } from "rsuite";

import { callApi } from "api/utils";
import useNoti from "hooks/useNoti";
import { WrapperAdminContext } from "interfaces/contexts";

import ModalEditSkills from "../components/common/ModalEditSkills";
import ModalDeleteSkills from "../components/common/ModalDeleteSkills";
import TableSkills from "../components/common/TableSkills";


export default function SkillsPage() {
    const {getNoti} = useNoti()
    const [dataEdit, setDataEdit] = useState(null)

    const [isReload, setIsReload] = useState(false)
    const {handleLoading} = useContext(WrapperAdminContext)
    
    const [dataTable, setDataTable] = useState([])
    const [dataDelete, setDataDelete] = useState()
    const [isOpenEdit, setIsOpenEdit]= useState(false)
    const [isOpenDelete, setIsOpenDelete] = useState(false)

    const fetchData = async () => {
        const response: any = await callApi(
            `/api/admin/skills`,
            'get',
            'Token',
        )
        if (response) {
            setDataTable([...response.data])
            handleLoading(false)
        }
    }
    
    useEffect(() => {
        handleLoading(true)
        fetchData()
    }, [])
    useEffect(() =>{
        if(isReload){
            fetchData()
            setIsReload(false)
        }
    }, [isReload])

    const handleAddNewSkills = () => {
        setIsOpenEdit(true)
        setDataEdit(null)
    }

    const handleEditSkills = (dataEdit:any)=>{
        if(dataEdit){
            setIsOpenEdit(true)
            setDataEdit(dataEdit)
        }
    }

    const onCloseModal = ()=>{
        setIsOpenEdit(false)
    }

    const handleDeleteSkills = async (data:any)=>{
        if(data){
            const result = await callApi(`/api/admin/skills/${data.id}`,
                'DELETE',
                'token',
            )
            if(result){
                setIsReload(true)
                getNoti('success', 'topCenter', `Xóa kỹ năng thành công`)
            }
            else{
                getNoti('success', 'topCenter', `Chưa xóa được kỹ năng ${data.id}`)
            }
        }   
    }

    return (
        <FlexboxGrid justify="start">
            {isOpenEdit && <ModalEditSkills onCloseModal={onCloseModal} data={dataEdit} isReload={setIsReload} dataAll={dataTable} />}
            {isOpenDelete && <ModalDeleteSkills dataDelete={dataDelete} isOpenDelete={isOpenDelete} onDelete={handleDeleteSkills} onCloseDelete={setIsOpenDelete}  />}
            <div className='action-field'>                        
                <div className="action-field__action">
                    <div className="action-field__action-btn" >
                        <Whisper
                            placement="bottom"
                            trigger="hover"
                            speaker={<Tooltip>Nhấn để tạo kỹ năng mới</Tooltip>}
                        >
                            <Button
                                className="btn__main-btn"
                                style={{background:"#6868ac"}}
                                    onClick={() => {
                                    handleAddNewSkills()
                                }}
                            >
                                <img src="/images/icons/ic-add-new.png" 
                                    alt="add-new"
                                />
                                    <span>Tạo mới</span>
                            </Button>
                        </Whisper>
                    </div>
                </div>
            </div>

            {/* Table data */}
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='table-field'>
                {dataTable.length > 0 && (<TableSkills data={dataTable} getDataEdit={handleEditSkills} dataDelete={setDataDelete} isOpenDelete={setIsOpenDelete} isReload={setIsReload}/>)}
            </FlexboxGrid.Item>
        </FlexboxGrid> 
    )
}