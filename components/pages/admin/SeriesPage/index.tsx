import { useContext, useEffect, useState } from "react";

import { Button, Col, FlexboxGrid, Pagination, Row, Tooltip, Whisper } from "rsuite";

import { callApi } from "api/utils";
import { InputWithLabel } from "components/atoms/inputWithLabel";
import { MultiSelectPicker } from "components/atoms/selectPicker/multiSelectPicker";
import { StickyFooter } from "components/organisms/stickyFooter";
import useNoti from "hooks/useNoti";
import { WrapperAdminContext } from "interfaces/contexts";

import ModalConfirmUpdate from "../components/common/ModalConfirmUpdate";
import ModalDeleteSeries from "../components/common/ModalDeleteSeries";
import ModalEditSeries from "../components/common/ModalEditSeries";
import TableSeries from "../components/common/TableSeries";
import useGrade from "hooks/useGrade";

export default function SeriesPage() {
    const {getNoti} = useNoti()
    const [dataEdit, setDataEdit] = useState(null)

    const [isReload, setIsReload] = useState(false)
    const {handleLoading} = useContext(WrapperAdminContext)
    
    const [totalItem, setTotalItem] = useState(0)
    const [totalPage,setTotalPage] = useState(0)
    const limit:any = 10;
    const [page, setPage] = useState(1)

    const [dataTable, setDataTable] = useState([])
    const [dataDelete, setDataDelete] = useState()
    const [isOpenEdit, setIsOpenEdit]= useState(false)
    const [isOpenDelete, setIsOpenDelete] = useState(false)
    const [updateAll, setUpdateAll] = useState(false)
    const [isOpenUpdateConfirm, setIsOpenUpdateConfirm] = useState(false)

    const [title, setTitle] = useState("")
    const [type, setType] = useState([])
    const [grade, setGrade] = useState([])
    const {getGrade} = useGrade()
    const [isFilter, setIsFilter] = useState(false)
    const [triggerReset, setTriggerReset] = useState(false)
    const dataType = [
        {
            code:'1',
            display:'SGK'
        },
        {
            code:'2',
            display:'Khác'
        },
    ]
    async function fetchData(page: any, limit: any) {
        const response: any = await callApi(
            `/api/admin/series?page=${page}&limit=${limit}`,
            'POST',
            'Token',
        )
        if (response) {
            setDataTable(response.data)
            setTotalItem(response.totalRecords)
            setTotalPage(response.totalPage)
            handleLoading(false)
        }
    }
    
    useEffect(() => {
        handleLoading(true)
        fetchData(1,10)
    }, [])
    useEffect(() =>{
        if(isReload){
            fetchData(1,10)
            setIsReload(false)
            setPage(1)
            setTriggerReset(!triggerReset)
            handleResetFilter()
        }
    }, [isReload])

    const handleUpdateAllSeries = async () => {
        const response = await callApi(
            `/api/admin/seriesEduHome`,
            'POST',
            'token',
        )
        if(response){
            getNoti('success', 'topCenter', `Cập nhật toàn bộ chương trình thành công`)
            setIsReload(true)
            setPage(1)
        }else{
            getNoti('error', 'topCenter', `Cập nhật toàn bộ chương trình thất bại`)
        }

    }

    const handleEditSeries = (dataEdit:any)=>{
        if(dataEdit){
            setIsOpenEdit(true)
            setDataEdit(dataEdit)
        }
    }

    const onCloseModal = ()=>{
        setIsOpenEdit(false)
    }

    const handleDeleteSeries = async (data:any)=>{
        if(data){
            const result = await callApi(`/api/admin/series/${data.id}`,
                'DELETE',
                'token',
            )
            if(result){
                setIsReload(true)
                getNoti('success', 'topCenter', `Xóa chương trình thành công`)
            }
            else{
                getNoti('success', 'topCenter', `Chưa xóa được chương trình`)
            }
        }   
    }

    const scrollToTop = () =>{
        window.scrollTo({
            top: 0, 
            behavior: 'smooth'
        });
    };

    const handlePageChange = async (page:number) =>{       
        const body = collectData(false, page)
        setIsFilter(true)
        scrollToTop()
        const result:any = await callApi(
            `/api/admin/series?page=${page}&limit=10`,
            'post',
            'Token',
            body,
        )
        if(result){
            setDataTable(result.data)
            setPage(page)
            handleLoading(false)
        }
    }
    
    const handleChangeTitle = (val:any) => {
        setTitle(val)
    }
    const handleChangeType = (val:any) => {
        setType([...val])
        handleFilterSubmit(false,{type: [...val]})
    }
    const handleChangeGradeId = (val:any) => {
        setGrade([...val])
        handleFilterSubmit(false,{gradeId:[...val]})
    }
    const handleResetFilter = () => {
        if(Boolean(title)) setTitle("")
        if(Boolean(type)) setType([])
        if(Boolean(grade)) setGrade([])
    }

    const collectData = (isReset:boolean, page = 1, body:any = null) => {
        const collection: any = {page}
        if(!isReset){
            if(title || body?.title){
                collection['title'] = body?.title?.trim() || title?.trim()
            }
            if((type && type.length > 0) || (body?.type && body?.type.length > 0)){
                collection['typeArr'] = body?.type || type
            }
            if((grade && grade.length > 0) || (body?.gradeId && body.gradeId.length > 0)){
                collection['grade'] = body?.gradeId || grade
            }
        }
        return collection
    }

    const handleFilterSubmit = async (isReset:boolean, customData:any = null) =>{
        const body = collectData(isReset,1,customData)
        if(isReset === false){
            handleLoading(true)
        }
        setIsFilter(!isReset)
        const result: any = await callApi(
            `/api/admin/series?page=${body?.page}&limit=10`,
            'post',
            'Token',
            body,
        )
        if(result){
            setDataTable(result.data)
            setTotalItem(result.totalRecords)
            setTotalPage(result.totalPage)
            setPage(body.page > 0 ? body.page : 1)
            handleLoading(false)
        }
    }

    return (
        <FlexboxGrid justify="start">
            {isOpenEdit && <ModalEditSeries onCloseModal={onCloseModal} data={dataEdit} dataAll={dataTable} isReload={setIsReload} />}
            {isOpenDelete && <ModalDeleteSeries dataDelete={dataDelete} isOpenDelete={isOpenDelete} onDelete={handleDeleteSeries} onCloseDelete={setIsOpenDelete} />}
            {updateAll && <ModalConfirmUpdate isOpenUpdateConfirm={isOpenUpdateConfirm} onClose={() => setIsOpenUpdateConfirm(false)} onUpdateSeries={handleUpdateAllSeries} isReload={setIsReload}/>}
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='action-series'>  
                <FlexboxGrid.Item as={Col} colspan={24} md={24}>
                    <FlexboxGrid.Item as={Row} colspan={24} md={24} style={{margin:'0'}}></FlexboxGrid.Item>                      
                        <FlexboxGrid.Item as={Col} colspan={24} md={5}>
                            <InputWithLabel 
                                label="Tên chương trình"
                                triggerReset={triggerReset}
                                type='text'
                                maxLength={100}
                                onChange={handleChangeTitle}
                            />
                        </FlexboxGrid.Item>
                        <FlexboxGrid.Item as={Col} colspan={24} md={5}>
                            <MultiSelectPicker 
                                data={[...dataType]}
                                label='Loại sách'
                                triggerReset={triggerReset}
                                onChange={handleChangeType}
                            />
                        </FlexboxGrid.Item>
                        <FlexboxGrid.Item as={Col} colspan={24} md={5}>
                            <MultiSelectPicker 
                                data={getGrade}
                                label="Grade"
                                triggerReset={triggerReset}
                                onChange={handleChangeGradeId}
                            />
                            </FlexboxGrid.Item>
                    <FlexboxGrid.Item as={Col} colspan={24} md={9} className="action-series__action">
                        <div className="action-series__action-btn" >
                            <Whisper
                                placement="bottom"
                                trigger="hover"
                                speaker={<Tooltip>Nhấn để xóa bỏ bộ lọc đang dùng</Tooltip>}
                            >
                                <Button
                                    className="btn__main-btn"
                                    style={{background:'#fff'}}
                                    onClick={() => {
                                        setTriggerReset(!triggerReset)
                                        handleResetFilter()
                                        handleFilterSubmit(true)
                                    }}
                                >
                                    <img src="/images/icons/ic-reset-darker.png" alt="reset" />
                                    <span style={{color:"#526375"}}>Mặc định</span>
                                </Button>
                            </Whisper>
                            <Whisper
                                placement="bottom"
                                trigger="hover"
                                speaker={<Tooltip>Nhấn để tìm kiếm theo bộ lọc</Tooltip>}
                            >
                                <Button
                                    className="btn__main-btn__find"
                                    onClick={() => {
                                        handleFilterSubmit(false)
                                    }}
                                >
                                    <img 
                                        src="/images/icons/ic-search-dark.png" 
                                        alt="find" 
                                    />
                                        <span>Tìm kiếm</span>
                                </Button>
                            </Whisper>
                            <Whisper
                                placement="bottom"
                                trigger="hover"
                                speaker={<Tooltip>Nhấn để cập nhật lại danh sách các chương trình đã có</Tooltip>}
                            >
                                <Button
                                    className="btn__main-btn"
                                    style={{background:"#6868ac"}}
                                        onClick={() => {                                        
                                            setUpdateAll(true)
                                            setIsOpenUpdateConfirm(true)
                                    }}
                                >
                                    <img src="/images/icons/ic-download-white.png" 
                                        alt="reload"
                                    />
                                        <span>Cập nhật</span>
                                </Button>
                            </Whisper>
                        </div>
                    </FlexboxGrid.Item>
                </FlexboxGrid.Item>
            </FlexboxGrid.Item>

            {/* Table data */}
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='table-series'>
                {dataTable && (
                <TableSeries data={dataTable} getDataEdit={handleEditSeries} dataDelete={setDataDelete} isOpenDelete={setIsOpenDelete} isReload={setIsReload}/>
                )}
            </FlexboxGrid.Item>

            {/* pagination */}
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='pagination-series'>
                <FlexboxGrid.Item as={Col} colspan={24} md={24}>
                {dataTable.length > 0 && (
                    <StickyFooter  className="pagination-admin">
                        <div className="admin-pagination">
                            <span>Trang {page}/{totalPage}</span>
                            <div className="admin-pagination-item">
                            <Pagination
                                prev
                                next
                                ellipsis
                                size="md"
                                total={totalItem}
                                maxButtons={10}
                                limit={limit}
                                activePage={page}
                                onChangePage={(page: number) => handlePageChange(page)}
                            />
                            </div>
                        </div>
                        <div><span>{totalItem} kết quả</span></div>
                    </StickyFooter>
                )}
                </FlexboxGrid.Item>
            </FlexboxGrid.Item>
        </FlexboxGrid> 
    )
}