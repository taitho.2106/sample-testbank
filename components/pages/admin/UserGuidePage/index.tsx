import { useContext, useEffect, useState } from "react";

import { Button, Col, FlexboxGrid, Message, Pagination, Row, toaster, Tooltip, Whisper } from "rsuite";

import { callApi } from "api/utils";
import { InputWithLabel } from "components/atoms/inputWithLabel";
import { MultiSelectPicker } from "components/atoms/selectPicker/multiSelectPicker";
import { StickyFooter } from "components/organisms/stickyFooter";
import { WrapperAdminContext } from "interfaces/contexts";

// import LoadingAdmin from "../components/common/Loading";
import ModalCusTomAdmin from "../components/common/Modal";
import ModalPopupAdmin from "../components/common/ModalPopup";
import ModalSuccess from "../components/common/ModalSuccess";
import TableUserGuide from "../components/common/TableUserGuide";



const dataType = [
    {
        code:'1',
        display:'Video'
    },
    {
        code:'2',
        display:'Document'
    }
]
function UserGuidePage() {
    const [dataEdit, setDataEdit] = useState(null)
    const [triggerReset, setTriggerReset] = useState(false)
    const [title, setTitle] = useState("")
    const [screen, setScreen] = useState([])
    const [type, setType] = useState([])
    const [isOpenEdit, setIsOpenEdit]= useState(false)
    const [isOpenDel, setIsOpenDel]= useState(false)
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    const [isFilter, setIsFilter] = useState(false)
    const [idDelete, setIdDelete] = useState(-1)
    const [dataScreen, setDataScreen] = useState([])
    const [dataTable, setDataTable] = useState([])
    const limit:any = 10;
    const [page,setPage] = useState(1)
    const [totalItem, setTotalItem] = useState(0)
    const [totalPage,setTotalPage] = useState(0)
    const [isOpenModalSuccess,setIsOpenModalSuccess] = useState(false)
    const [dataModalSuccess,setDataModalSuccess] = useState(null)
    const {handleLoading} = useContext(WrapperAdminContext)
    
    async function fetchDataScreen() {
        handleLoading(true)
        const result = await fetch('/api/admin/user-guide-screen')
        const data = await result.json()
        const arrScreen = data.data.map((item:any)=>{
            return {
                code:item.id,
                display:item.name
            }
        })
        handleLoading(false)
        setDataScreen(arrScreen)
    }
    async function fetchData(page:any, limit:any) {
        handleLoading(true)
        const result: any = await callApi(
            `/api/admin/user-guide/search?page=${page}&limit=${limit}`,
            'post',
            "Token"
          )
          if(result){
              setDataTable(result.data)
              setTotalItem(result.totalRecords)
              setTotalPage(result.totalPage)
              handleLoading(false)
          }

    }
    
    useEffect(()=>{
        fetchData(1,10)
        fetchDataScreen()
    },[])
    const collectData = (isReset:boolean,page = 1,body:any = null) =>{
        const collection: any = {page}
        if(!isReset){
            if(title || body?.title){
                collection['title'] = body?.title.trim() || title.trim()
            }
            if((screen && screen.length > 0) || (body?.screenId && body.screenId.length > 0)){
                collection['idScreenArr'] = body?.screenId || screen
            }
            if((type && type.length > 0) || (body?.type && body?.type.length > 0)){
                collection['typeArr'] = body?.type || type
            }
        }
        // console.log("colecction", collection)
        return collection
    }
    const handleAddNew = () => {
        setIsOpenEdit(true)
        setDataEdit(null)
    }
    const handleChangeTitle = (val:any) => {
        setTitle(val)
    }
    const handleChangeScreen = (val:any) => {
        setScreen([...val])
        handleFilterSubmit(false,{screenId:[...val]})
    }
    const handleChangeType = (val:any) => {
        setType([...val])
        handleFilterSubmit(false,{type: [...val]})
    }
    const handleFilterSubmit = async (isReset:boolean, customData:any = null) =>{
        const body = collectData(isReset,1,customData)
        if(isReset === false){
            handleLoading(true)
        }
        setIsFilter(!isReset)
        const result: any = await callApi(
            `/api/admin/user-guide/search?page=${body?.page}&limit=10`,
            'post',
            "Token",
           body,
          )
        if(result){
            setDataTable(result.data)
            setTotalItem(result.totalRecords)
            setTotalPage(result.totalPage)
            setPage(body.page > 0 ? body.page : 1)
            handleLoading(false)
        }
    }
    const handleResetFilter = () => {
        if(Boolean(title)) setTitle("")
        if(Boolean(screen)) setScreen([])
        if(Boolean(type)) setType([])
    }
    const handlerEdit = (dataEdit:any)=>{
       if(dataEdit && dataEdit.id>0){
        setIsOpenEdit(true)
        setDataEdit(dataEdit)
       }
    }
    const handleOnConfirm = async (userGuideId:any)=>{
        console.log("confirm delete userGuideId= ",userGuideId)
        if(userGuideId>0){
            // delete user_guide
            handleLoading(true)
            const result = await fetch(`/api/admin/user-guide/${userGuideId}`,{
                method:'DELETE'
            })
            if(result.status === 200){
                onCloseModal(true)
                handleLoading(false)
                const message = (
                    <Message showIcon type='success' >Xoá dữ liệu thành công</Message>
                )
                toaster.push(message,{placement:'topCenter'})
            }
        }
        
    }
    const handleOnBack = (dataEdit:any)=>{
        console.log("dataEdit",dataEdit)
        setIdDelete(-1)
        setIsOpenDel(false);
    }
    const handleOnDelete = (userGuideId:any)=>{
        console.log("deleted userGuideId",userGuideId)
        if(userGuideId>0){
            setIdDelete(userGuideId)
            setIsOpenDel(true);
        }
    }
    const onCloseModal = (status:any)=>{
        console.log("close modal",status)
        setIsOpenEdit(false)
        setIsOpenDel(false)
        if(status === true){
            handleFilterSubmit(true)
        }
    }
    const scrollToTop = () =>{
        window.scrollTo({
          top: 0, 
          behavior: 'smooth'
          /* you can also use 'auto' behaviour
             in place of 'smooth' */
        });
      };
    const handlePageChange =async (page:number) =>{
        const body = collectData(false, page)
        handleLoading(true)
        setIsFilter(true)
        scrollToTop()
        const result:any = await callApi(`/api/admin/user-guide/search?page=${page}&limit=10`,'post','Token',body)
       if(result){
        setDataTable(result.data)
        setPage(page)
        handleLoading(false)
       }
    }

    const onSuccessForm = (result:any)=>{
        setIsOpenModalSuccess(true);
        setDataModalSuccess(result);
        if(result.status===true){ //success
            handleFilterSubmit(true)
            setIsOpenEdit(false);
        }
    }
    const onCloseModalSuccess = (result:any)=>{
        setIsOpenModalSuccess(false);
        setDataModalSuccess(null);
        switch (result){
            case "add":
                setDataEdit(null);
                setIsOpenEdit(true);
                break;
            case "refresh":
                setDataEdit(null);
                setIsOpenEdit(false);
                handleFilterSubmit(true)
                break;
            default:
                break;
        }
    }
    return (
        <FlexboxGrid justify="space-around">
             
            {isOpenEdit && <ModalCusTomAdmin  onCloseModal={onCloseModal} data={dataEdit} onDetele={handleOnDelete} onSuccessForm={onSuccessForm} />}
            {isOpenDel && <ModalPopupAdmin data={idDelete}  isOpenDel={isOpenDel} onDelete={handleOnConfirm} onBack={handleOnBack}  />}
            {isOpenModalSuccess && <ModalSuccess data={dataModalSuccess} onCloseModalSuccess={onCloseModalSuccess}></ModalSuccess>}
            {/* Action Add and Search */}
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='action-user-guide'>
                <FlexboxGrid.Item as={Col} colspan={24} md={24}>
                    <FlexboxGrid.Item as={Row} colspan={24} md={24} style={{margin:'0'}}>
                        <FlexboxGrid.Item as={Col} colspan={24} md={5}>
                                <InputWithLabel 
                                    label="Tên tài liệu"
                                    triggerReset={triggerReset}
                                    type='text'
                                    maxLength={100}
                                    onChange={handleChangeTitle}
                                />
                            </FlexboxGrid.Item>
                            <FlexboxGrid.Item as={Col} colspan={24} md={5}>
                                <MultiSelectPicker 
                                        data={[...dataType]}
                                        label='Định dạng'
                                        triggerReset={triggerReset}
                                        onChange={handleChangeType}
                                />
                            </FlexboxGrid.Item>
                            <FlexboxGrid.Item as={Col} colspan={24} md={5}>
                                <MultiSelectPicker 
                                    data={[...dataScreen]}
                                    label="Màn hình"
                                    triggerReset={triggerReset}
                                    onChange={handleChangeScreen}
                                />
                            </FlexboxGrid.Item>
                            <FlexboxGrid.Item as={Col} colspan={24} md={9} className="action-user-guide__action">
                                <div className="action-user-guide__action-btn" >
                                    <Whisper
                                    placement="bottom"
                                    trigger="hover"
                                    speaker={<Tooltip>Nhấn để xóa bỏ bộ lọc đang dùng</Tooltip>}
                                    >
                                        <Button
                                            className="btn__main-btn"
                                            style={{background:'#fff'}}
                                            onClick={() => {
                                            setTriggerReset(!triggerReset)
                                            handleResetFilter()
                                            handleFilterSubmit(true)
                                            }}
                                        >
                                            <img src="/images/icons/ic-reset-darker.png" alt="reset" />
                                            <span style={{color:"#526375"}}>Mặc định</span>
                                        </Button>
                                    </Whisper>
                                    <Whisper
                                    placement="bottom"
                                    trigger="hover"
                                    speaker={<Tooltip>Nhấn để tìm kiếm theo bộ lọc</Tooltip>}
                                    >
                                        <Button
                                            className="btn__main-btn__find"
                                            onClick={() => {
                                                handleFilterSubmit(false)
                                            }}
                                        >
                                            <img 
                                            src="/images/icons/ic-search-dark.png" 
                                            alt="find" />
                                            <span>Tìm kiếm</span>
                                        </Button>
                                    </Whisper>
                                    <Whisper
                                    placement="bottom"
                                    trigger="hover"
                                    speaker={<Tooltip>Nhấn để tạo mới một tài liệu</Tooltip>}
                                    >
                                        <Button
                                        className="btn__main-btn"
                                        style={{background:"#6868ac"}}
                                        onClick={() => {
                                            handleAddNew()
                                        }}>
                                        <img 
                                            src="/images/icons/ic-add-new.png" 
                                            alt="add-new" />
                                            <span>Tạo mới</span>
                                        </Button>
                                    </Whisper>
                                </div>
                            </FlexboxGrid.Item>
                        </FlexboxGrid.Item>
                </FlexboxGrid.Item>
            </FlexboxGrid.Item>
            
            {/* Table data */}
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='table-user-guide'>
                {/* <div className="table-user-guide__title"><span>DANH SÁCH</span></div> */}
                <FlexboxGrid.Item as={Col} colspan={24} md={24}>
                    <TableUserGuide data={dataTable} getDataEdit={handlerEdit} onDeleteData={handleOnDelete}  />
                </FlexboxGrid.Item>
            </FlexboxGrid.Item>

            {/* pagination */}
            <FlexboxGrid.Item as={Col} colspan={24} md={24} className='pagination-user-guide'>
                <FlexboxGrid.Item as={Col} colspan={24} md={24}>
                {dataTable.length > 0 && (
                    <StickyFooter  className="pagination-admin">
                        <div className="admin-pagination">
                            <span>Trang {page}/{totalPage}</span>
                            <div className="admin-pagination-item">
                            <Pagination
                            prev
                            next
                            ellipsis
                            size="md"
                            total={totalItem}
                            maxButtons={10}
                            limit={limit}
                            activePage={page}
                            onChangePage={(page: number) => handlePageChange(page)}
                            />
                        </div>
                        </div>
                        
                        <div><span>{totalItem} kết quả</span></div>
                    </StickyFooter>
               
                )}
                </FlexboxGrid.Item>
                </FlexboxGrid.Item>


        {/* modal success */}
        
        </FlexboxGrid> 
    );
}

export default UserGuidePage;
