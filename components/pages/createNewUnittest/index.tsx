import { SetStateAction, useContext, useEffect, useMemo, useState } from 'react'

import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { Button, Tooltip, Whisper } from 'rsuite'

import { UnittestInfoCard } from 'components/atoms/unittestInfoCard'
import { BreadCrumbNav } from 'components/molecules/breadCrumbNav'
import { ProgressUnittest } from 'components/molecules/progressUnittest'
import { UnittestTemplateTableData } from 'components/molecules/unittestTemplateTableData'
import { UpdateQuestionSeries } from 'components/organisms/modals/updateQuestionSeries'
import { QuestionPreviewSection } from 'components/organisms/questionPreviewSection'
import { StickyFooter } from 'components/organisms/stickyFooter'
import { Wrapper } from 'components/templates/wrapper'
import useNoti from 'hooks/useNoti'
import { PACKAGES, USER_ROLES } from 'interfaces/constants'
import { AppContext, SingleTemplateContext, WrapperContext } from 'interfaces/contexts'
import { delayResult, userInRight } from 'utils'
import ConfirmLeave from './ConfirmLeave'

import { templateDetailTransform } from '../../../api/dataTransform'
import { callApi } from '../../../api/utils'
import { UnitSectionCard } from '../../molecules/unitSectionCard/index'
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";
import useTranslation from "@/hooks/useTranslation";
import IconInfoTest from '@/assets/icons/info-create-test.svg';

export const CreateNewUnittest = ({
  className = '',
  title = '',
  style,
}: any) => {
  const { t } = useTranslation();
  const ctx = useContext(WrapperContext)
  const router = useRouter()
  const [isCreateSuccess, setIsCreateSuccess] = useState(false);
  const [isShow, setIsShow] = useState(false)
  const { getNoti } = useNoti()

  const {
    mode,
    setMode,
    breadcrumbData,
    chosenTemplate,
    templateData,
    totalPages,
    currentPage,
  } = useContext(SingleTemplateContext)
  const sectionData = chosenTemplate.state?.sections || []
  const totalPoints =
    chosenTemplate.state?.totalPoints || chosenTemplate.state?.point || ''

  const [session] = useSession()
  // const isEditAble = dataPlan?.code !== PACKAGES.BASIC.CODE
  const isEditAble = userInRight([USER_ROLES.Operator], session)
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
  const { dataPlan } = useCurrentUserPackage()
  const unitTestEI = chosenTemplate.state?.is_international_exam === 1
  const isChangeData = useMemo(() => {
    if (isEditAble) return true
    if (isTeacher && dataPlan?.code === PACKAGES.BASIC.CODE) return false
    if (isTeacher && dataPlan?.code !== PACKAGES.INTERNATIONAL.CODE && unitTestEI) return false
    return true
  }, [isTeacher, isEditAble, dataPlan?.code, chosenTemplate])

  const isNotCopy = useMemo(() => {
    if (isEditAble) return false
    if (!isEditAble && !isTeacher) return true
    if (router?.query?.m !== 'mine') return false
    if (dataPlan?.code === PACKAGES.BASIC.CODE) return true
    if (unitTestEI) return true
    return false
  } , [isTeacher, router.query, dataPlan, unitTestEI])

  const [isCopy, setIsCopy] = useState(false)
  const [slider, setSlider] = useState(mode === 'create' ? 0 : 1)
  const [biggestStep, setBiggestStep] = useState(0)
  const [isReady, setIsReady] = useState(false)
  const user: any = session?.user
  const enableUpdateQuestionSeries =
    chosenTemplate.state?.id > 0 &&
    user?.is_admin === 1 &&
    chosenTemplate.state?.scope == 0

  const isOwner = user?.id && user.id == chosenTemplate?.state?.author;
  const [isActiveModalUpdateSeries, setIsActiveModalUpdateSeries] =
    useState(false)
  const getTotalSentence = (arr: any[]) => {
    let totalChosenSentence = 0
    if (arr) {
      arr.forEach((item) => {
        if (item?.total_question) totalChosenSentence += item.total_question
      })
    }
    return totalChosenSentence
  }
  const isPointQuestion = (value: number) => {
    if (isNaN(value)) {
      return false
    }
    if (!value) {
      return false
    }
    return true
  }

  const checkReady = () => {
    if (
      !chosenTemplate.state?.date[0] ||
      !chosenTemplate.state?.date[1] ||
      !chosenTemplate.state?.name ||
      !chosenTemplate.state?.totalPoints ||
      !chosenTemplate.state?.templateLevelId ||
      !chosenTemplate.state?.time ||
      !chosenTemplate.state?.totalQuestion
    ){
      return false
    }
    if (isEditAble && ((chosenTemplate.state.is_international_exam === 0 &&
      !chosenTemplate.state?.series_id) ||
    !chosenTemplate.state.unit_test_type )) {
      return false
    }
    return true
  }

  const checkReadyToFinalSubmit = () => {
    let check = true
    if (mode !== 'detail' && isEditAble && ((chosenTemplate.state?.is_international_exam === 0 &&
        !chosenTemplate.state?.series_id) ||
      !chosenTemplate.state?.unit_test_type)) {
        return false
    }

    if (
      !chosenTemplate.state?.name ||
      !chosenTemplate.state?.time ||
      !chosenTemplate.state?.date[0] ||
      !chosenTemplate.state?.date[1] ||
      !chosenTemplate.state?.totalQuestion ||
      !chosenTemplate.state?.totalPoints ||
      !chosenTemplate.state?.sections ||
      chosenTemplate.state.sections.length <= 0 
    ){
      return false
    }
    const startDate = new Date(chosenTemplate.state?.date[0])
    const endDate = new Date(chosenTemplate.state?.date[1])
    if(endDate.getTime() < startDate.getTime()){
      return false;
    }
    chosenTemplate.state.sections.forEach((section: any) => {
      if (
        !section?.parts[0] ||
        !section.parts[0]?.name ||
        !section.parts[0]?.questions ||
        !section.parts[0]?.totalQuestion ||
        getTotalSentence(section.parts[0].questions) !==
          section.parts[0].totalQuestion ||
        !isPointQuestion(section?.parts[0]?.points)
      )
        check = false
    })
    return check
  }

  const passValidation = () => {
    if (slider !== 1 || !['update', 'create'].includes(mode)) return true
    if(!chosenTemplate.state) return false
    if(!chosenTemplate.state?.name || !chosenTemplate.state?.name.trim().length){
      getNoti('error', 'topCenter', t('common-get-noti')['template-name-validate'])
      return false
    }
    const unitTestTotalQuestion = chosenTemplate.state?.totalQuestion || 0
    if (!unitTestTotalQuestion) {
      getNoti('error', 'topCenter', t('common-get-noti')['template-total-question-validate'])
      return false
    }
    const unitTestTotalPoint = totalPoints || 0
    if (!unitTestTotalPoint) {
      getNoti('error', 'topCenter', t('common-get-noti')['template-point-validate'])
      return false
    }

    if (!chosenTemplate.state.date[0]) {
      getNoti('error', 'topCenter', t('common-get-noti')['start-date-validate'])
      return false
    }

    if (!chosenTemplate.state.date[1]) {
      getNoti('error', 'topCenter', t('common-get-noti')['end-date-validate'])
      return false
    }

    const date = chosenTemplate.state.date.map((item: number) =>
      item ? new Date(item).getTime() : null,
    )

    if (date[0] > date[1]) {
      getNoti('error', 'topCenter', t('common-get-noti')['date-time'])
      return false
    }

    if(!chosenTemplate.state?.sections || chosenTemplate.state.sections.length <= 0){
        getNoti('error', 'topCenter', t('common-get-noti')['add-part'])
        return false
    }

    // thong tin chi tiet vs thong tin chung
    let skillTotalQuestion = 0
    let skillTotalPoint = 0
    let existEmptyName = false
    let existEmptySkill = false
    let existEmptyQuestion = false
    let notEqualTotalQuestion = 0
    if (chosenTemplate.state?.sections){
      chosenTemplate.state.sections.forEach((section: any) => {
        if (section?.parts[0]) {
          if (!section.parts[0]?.name && !existEmptyName) existEmptyName = true
          if (section.parts[0]?.totalQuestion) skillTotalQuestion += section.parts[0]?.totalQuestion
          if (section.parts[0]?.points) skillTotalPoint += section.parts[0]?.points
          if (!section.parts[0]?.questions || !section.parts[0]?.questions?.length) existEmptyQuestion = true
          if(section.parts[0]?.questions){
            notEqualTotalQuestion += getTotalSentence(section.parts[0]?.questions)
          }
        }
        if(!section?.sectionId || !section?.sectionId?.length){
          existEmptySkill = true
        }
      })
    }

    if(existEmptyName || existEmptySkill || existEmptyQuestion || !skillTotalQuestion || !skillTotalPoint) {
      getNoti('error', 'topCenter', "Vui lòng nhập đầy đủ thông tin để tiếp tục")
      return false
    }

    if ((skillTotalQuestion && skillTotalQuestion !== unitTestTotalQuestion) || (notEqualTotalQuestion !== unitTestTotalQuestion)) {
      getNoti('error', 'topCenter', t('common-get-noti')['template-check-total-question'])
      return false
    }
    skillTotalPoint = Math.round(skillTotalPoint * 100) / 100
    if (skillTotalPoint && skillTotalPoint !== unitTestTotalPoint) {
      getNoti('error', 'topCenter', t('common-get-noti')['template-check-total-point'])
      return false
    }

    return true
  }

  useEffect(() => {
    const valid = passValidation()
    setIsReady(valid)
  }, [])
  
  const handleMoveToThirdStep = () => {
    const valid = passValidation()
    if (!valid) return
    setSlider(2)
    setBiggestStep(2)
  }
  const onToggle = (value: boolean) => {
    setIsShow(value)
  }

  const onHandleCopy = () => {
    if(router.query.m === 'mine'){
      setIsCopy(true)
      setMode('create')
      router.push(
        { pathname: '/unit-test/[templateSlug]', query: { m: 'mine' } },
        { pathname: '/unit-test/create-new-unittest', query: { m: 'mine' } },
        { shallow: true },
      )
      setTimeout(() => {
        setSlider(1)
        setBiggestStep(2)
        getNoti('success', 'topCenter',  t('common-get-noti')['copy-succeed'])
      }, 0)
    }else{
      onToggle(true)
    }
  }

  useEffect(() => {
    const fecthDefaultTemplate = async (id: string) => {
      const response: any = await callApi(`/api/templates/${id}`, 'get')
      const data = response?.data
        ? templateDetailTransform(response.data)
        : null
      if (data) {
        chosenTemplate.setState({
          ...data,
          date: [null, null],
          scope: isEditAble ? 0 : 1,
          unitType: '0',
        })
        setSlider(1)
        setBiggestStep(1)
      }
    }

    const defaultTemplateId = `${router.query?.id}`
    if (!defaultTemplateId) return
    fecthDefaultTemplate(defaultTemplateId)
  }, [])

  useEffect(() => {
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }, [slider])

  const viewQuestionList = async () => {
    setIsActiveModalUpdateSeries(true)
  }
  const onClickStep = (value:any)=>{
    if(value == slider) return;
    switch(value){
      case 2:
        if(!checkReadyToFinalSubmit()){
          setBiggestStep(slider)
          return;
        }
        if(!isReady){
          setBiggestStep(slider)
        }
        return;
      default:
        setSlider(value)
        break;
    }
  }
  return (
    <Wrapper
      className={`p-create-new-template p-detail-format ${className}`}
      pageTitle={title}
      style={style}
    >
      {chosenTemplate?.state && (
        <ShowModalCopyShortStep isShow={isShow} data={chosenTemplate?.state} onClose={onToggle}/>
      )}
      <ConfirmLeave mode={mode} isCreateSuccess={isCreateSuccess} />
      <div className="p-detail-format__breadcrumb">
        <BreadCrumbNav data={breadcrumbData} />
      </div>
      {mode === 'create' && 
        <div className="p-detail-format__sticky-progress">
          <div className="p-detail-format__progress">
            <ProgressUnittest
              max={biggestStep}
              setMax={setBiggestStep}
              value={slider}
              onClickStep={onClickStep}
              setValue={setSlider}
            />
          </div>
        </div>
      }
      {['update', 'detail'].includes(mode) && 
        <div
          className="p-detail-format__progress"
          style={{ margin: '2.4rem 0 0 0' }}
        >
          <Button
              className="__progress-btn"
              data-active={slider === 1}
              onClick={() => setSlider(1)}
          >
            {t('common')['general-view']}
          </Button>
          <Button
            className="__progress-btn"
            data-active={slider === 2}
            onClick={handleMoveToThirdStep}
          >
            {t('common')['preview-unit-test']}
          </Button>
        </div>
      }
      {slider === 0 && mode === 'create' && (
        <>
          {templateData.length > 0 ? (
            <UnittestTemplateTableData
              className="templates-table"
              title={t('unit-test')['chosen-template']}
              data={templateData}
              total={totalPages}
              pageIndex={currentPage}
              nextStep={() => {
                if (biggestStep < 1) setBiggestStep(1)
                setSlider(1)
              }}
            />
          ) : (
            <div
              className="o-question-drawer__empty"
              style={{ minHeight: '50vh', paddingTop: 20, paddingBottom: 20 }}
            >
              <img
                className="__banner"
                src="/images/collections/clt-emty-template.png"
                alt="banner"
              />
              <p className="__description">
                {t('unit-test')['no-data-test']}
                <br />
                {t('unit-test')['pls-create']}
              </p>
              <Button
                className="__submit"
                onClick={() =>
                  router.push(
                    '/unit-test/[templateSlug]',
                    '/questions/create-new-unittest',
                  )
                }
              >
                {t('common')['create']}
              </Button>
            </div>
          )}
        </>
      )}
      {slider === 1 && (
        <div className="p-detail-format__container">
          <div className="container-info">
            <div className="container-title">
              <h5>{t('unit-test')['info-test-title']}</h5>
            </div>
            <UnittestInfoCard isCopy={!isEditAble} isChangeData={isChangeData}/>
          </div>
          <div className="container-section-list">
            <div className="container-title">
              <h5>{t('unit-test')['info-parts-title']}</h5>
            </div>
            {checkReady() && ['update', 'create'].includes(mode) && <div className="description-create-part">
              <IconInfoTest />
              <span>Vui lòng chọn câu hỏi cho từng phần thi để hoàn tất tiến trình tạo đề</span>
            </div>}
            <UnitSectionCard isReady={checkReady()} isChangeData={isChangeData}/>
          </div>
          <StickyFooter>
            <div
              className="container-footer"
              style={{ width: '100%', justifyContent: 'space-between' }}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  width: '100%',
                }}
              >
                <div>
                  {mode === 'detail' && !isEditAble && isOwner && !isNotCopy && (
                    <Button
                      className="__submit --green"
                      type="button"
                      onClick={onHandleCopy}
                    >
                      <span>
                        {t('common')['copy']}
                      </span>
                    </Button>
                  )}
                  {mode === 'detail' && !isEditAble && chosenTemplate?.state?.scope == 0 && !router?.query?.m && (
                    <Button
                      className="__submit --green"
                      type="button"
                      onClick={onHandleCopy}
                    >
                      <span>
                        {t('table-action-popover')['copy-unit-test']}
                      </span>
                    </Button>
                  )}
                </div>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'flex-end',
                    width: '100%',
                  }}
                >
                  {enableUpdateQuestionSeries && (
                    <Whisper
                      placement="top"
                      trigger="hover"
                      speaker={
                        <Tooltip>{t('unit-test')['tooltip-test']}</Tooltip>
                      }
                    >
                      <div
                        style={{
                          cursor: !checkReadyToFinalSubmit()
                            ? 'not-allowed'
                            : 'auto',
                          marginRight: '1.6rem',
                        }}
                      >
                        <Button
                          className="__submit"
                          style={{ background: '#6262BC' }}
                          onClick={viewQuestionList}
                        >
                          <span>{t('unit-test')['lists-question']}</span>
                        </Button>
                      </div>
                    </Whisper>
                  )}
                  <Whisper
                    placement="top"
                    trigger="hover"
                    speaker={<Tooltip>{t('unit-test')['next-step']} 3</Tooltip>}
                  >
                    <div
                      style={{
                        cursor: !checkReadyToFinalSubmit()
                          ? 'not-allowed'
                          : 'auto',
                      }}
                    >
                      <Button
                        className="__submit"
                        // style={{
                        //   pointerEvents: !checkReadyToFinalSubmit()
                        //     ? 'none'
                        //     : 'auto',
                        // }}
                        // disabled={!checkReadyToFinalSubmit()}
                        onClick={handleMoveToThirdStep}
                      >
                        <span>
                          {isCopy ? t('common')['next-btn'] : t('common')['detailed-unit-test']}
                        </span>
                      </Button>
                    </div>
                  </Whisper>
                </div>
              </div>
            </div>
          </StickyFooter>
        </div>
      )}
      {slider === 2 && (
        <div className="p-detail-format__preview">
          {sectionData.map((item: any) => (
            <QuestionPreviewSection
              key={item.id}
              data={item}
              isShowIconChange={!(dataPlan?.code !== PACKAGES.INTERNATIONAL.CODE && unitTestEI) || isEditAble}
            />
          ))}
          <FinalSubmit
            setSlider={setSlider}
            setBiggestStep={setBiggestStep}
            isNotCopy={isNotCopy}
            setIsCreateSuccess={setIsCreateSuccess}
            setIsShow={setIsShow}
          />
        </div>
      )}
      {enableUpdateQuestionSeries && (
        <UpdateQuestionSeries
          isActive={isActiveModalUpdateSeries}
          setIsActive={setIsActiveModalUpdateSeries}
        />
      )}
    </Wrapper>
  )
}

const FinalSubmit = ({ setSlider, setBiggestStep, isNotCopy, setIsCreateSuccess, setIsShow }: any) => {
  const { t, subPath } = useTranslation()
  const router = useRouter()
  const { fullScreenLoading } = useContext(AppContext)
  const [session] = useSession()
  const checkScope = userInRight([USER_ROLES.Operator], session)
  const isAdmin = userInRight([], session)
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
  const user:any = session.user;
  const { getNoti } = useNoti()
  
  const { chosenTemplate, mode, setMode } = useContext(SingleTemplateContext)
  const isOwner = user?.id && user.id == chosenTemplate?.state?.author;
  const checkEditAble = user && (isAdmin || isOwner || (chosenTemplate?.state?.scope ==0 && checkScope));
  const { globalModal }: any = useContext(WrapperContext)

  const totalPoints =
    chosenTemplate.state?.totalPoints || chosenTemplate.state?.point || ''

  const handleFinalSubmit = async () => {
    const formData = {} as any
    formData.id = mode === 'update' ? chosenTemplate.state.id : null
    formData.template_id = chosenTemplate.state.templateId // || chosenTemplate.state.id
    formData.name = chosenTemplate.state?.name || ''
    formData.template_level_id = chosenTemplate.state?.templateLevelId || null
    formData.time = chosenTemplate.state?.time || null
    formData.total_question = chosenTemplate.state?.totalQuestion || null
    formData.total_point = totalPoints || null
    formData.privacy =  chosenTemplate.state?.privacy ?? (isTeacher ? 0 : 1)
    const startDate = chosenTemplate.state?.date[0]
    formData.start_date = startDate
      ? isNaN(startDate)
        ? Math.floor(new Date(startDate).getTime() / 1000) * 1000
        : Math.floor(startDate / 1000) * 1000
      : null
    const endDate = chosenTemplate.state?.date[1]
    formData.end_date = endDate
      ? isNaN(endDate)
        ? Math.floor(new Date(endDate).getTime() / 1000) * 1000
        : Math.floor(endDate / 1000) * 1000
      : null
    formData.scope = checkScope ? 0 : 1
    formData.unit_type = ['0', '1'].includes(chosenTemplate.state?.unitType)
      ? parseInt(chosenTemplate.state.unitType)
      : 0
    formData.series_id = chosenTemplate.state.series_id || null
    const sections = [] as any[]
    if (chosenTemplate.state?.sections)
      chosenTemplate.state.sections.forEach((section: any) => {
        const returnSection = {} as any
        let formSkills = []
        if (Array.isArray(section?.sectionId)) formSkills = section.sectionId
        else
          formSkills = section?.sectionId
            ? `${section.sectionId}`.split(',')
            : []
        returnSection.section = formSkills
        returnSection.section_old_id = section?.id

        const parts = [] as any[]
        if (section?.parts)
          section.parts.forEach((part: any) => {
            const returnPart = {} as any
            returnPart.id = part?.id
            returnPart.name = part?.name
            returnPart.question_types = part.questionTypes
            returnPart.total_question = part.totalQuestion
            returnPart.points = part.points
            returnPart.questions = part?.questions
              ? part.questions.map((question: any) => question.id)
              : []
            parts.push(returnPart)
          })
        returnSection.parts = parts
        sections.push(returnSection)
      })
    formData.sections = sections
    formData.unit_test_type = chosenTemplate.state.unit_test_type || null
    formData.package_level = chosenTemplate.state.package_level
    formData.is_international_exam = chosenTemplate.state.is_international_exam || null

    // console.log('formData-------', formData);
    fullScreenLoading.setState(true)
    const response: any = await delayResult(callApi(
      '/api/unit-test',
      mode === 'update' ? 'put' : 'post',
      'Token',
      formData,
    ),300)
    fullScreenLoading.setState(false)
    if (response?.id && (response?.userId || chosenTemplate.state?.author)) {
        setIsCreateSuccess(true);
        globalModal.setState({
          id: 'share-link',
          content: {
            id: response.id,
            dataShare: formData,
            title: t('unit-test-create')['title'],
            userId: response.userId || chosenTemplate.state?.author,
          },
        })
    }
  }
  const onToggle = (val: boolean) => {
    setIsShow(val)
  }

  const onHandleCopy = () => {
    if(router.query.m === 'mine'){
      setMode('create')
      router.push(
        { pathname: '/unit-test/[templateSlug]', query: { m: 'mine' } },
        { pathname: '/unit-test/create-new-unittest', query: { m: 'mine' } },
        { shallow: true },
      )
      setTimeout(() => {
        setSlider(1)
        setBiggestStep(2)
        getNoti('success', 'topCenter', t("common-get-noti")['copy-succeed'])
      }, 0)
    }else{
      onToggle(true)
    }
  }
  if (mode === "update" && (!checkEditAble)){
    console.log("403----", "Bạn không có quyền chỉnh sửa đề thi này")
    return <></>
  }
  if (mode === 'detail')
    return (
      <StickyFooter>
        <div
          className="preview-footer"
          style={{ width: '100%', justifyContent: 'space-between' }}
        >
          <div>
          {mode === 'detail' && !checkScope && isOwner && !isNotCopy && (
            <Button
              className="__submit --green"
              type="button"
              onClick={onHandleCopy}
            >
              <span>
                {t('common')['copy']}
              </span>
            </Button>
          )}
          {mode === 'detail' && !checkScope && chosenTemplate?.state?.scope == 0 && !router?.query?.m && (
            <Button
              className="__submit --green"
              type="button"
              onClick={onHandleCopy}
            >
              <span>
                {t('table-action-popover')['copy-unit-test']}
              </span>
            </Button>
          )}
          </div>
          <div>
            {(isAdmin ||
              (checkScope && chosenTemplate?.state?.scope ==0) ||
              (router?.query.m === 'mine' && !checkScope) && isOwner) && (
              <Button
                className="__submit"
                onClick={() =>
                  (window.location.href = `${subPath}/unit-test/${
                    router.query?.templateSlug
                  }?mode=edit${router.query.m ? `&m=${router.query.m}` : ''}`)
                }
              >
                <span>{t('common')['edit']}</span>
              </Button>
            )}
          </div>
        </div>
      </StickyFooter>
    )
  else
    return (
      <StickyFooter>
        <div className="preview-footer" style={{ width: '100%' }}>
          <Whisper
            placement="top"
            trigger={'hover'}
            speaker={<Tooltip>{t('unit-test')['tooltip-save']}</Tooltip>}
          >
            <Button className="__submit" onClick={handleFinalSubmit}>
              <span>{t('common')['completed']}</span>
            </Button>
          </Whisper>
        </div>
      </StickyFooter>
    )
    
}
type propShortStep = {
  isShow?: boolean
  onClose?: (value:boolean) => void
  data?: any
}
const ShowModalCopyShortStep = ({ isShow = false, data, onClose }: propShortStep) :any => {
  const { globalModal } = useContext(WrapperContext)
  const { tempData } = useContext(SingleTemplateContext)

  useEffect(() => {
    if (isShow && tempData.state) {
      globalModal.setState({
        id: 'copy-unit-test-short-step',
        dataUnitTest: tempData.state,
        onClose: () => onClose(false),
      })
    }
  }, [isShow, tempData.state])
  
  return null
}