import { useContext, useEffect } from 'react';
import { useRouter } from 'next/router';

import { WrapperContext } from '@/interfaces/contexts';
import useTranslation from "@/hooks/useTranslation";

const ConfirmLeave = ({ mode, isCreateSuccess }: any): any => {
    const { t } = useTranslation()
    const router = useRouter();
    const { globalModal } = useContext(WrapperContext);

    useEffect(() => {
        const confirmationMessage = 'Changes you made may not be saved.';

        const beforeUnloadHandler = (e: BeforeUnloadEvent) => {
            (e || window.event).returnValue = confirmationMessage;
            return confirmationMessage; // Gecko + Webkit, Safari, Chrome etc.
        };

        const beforeRouteHandler = (url: string) => {
            if (router.pathname !== url) {
                globalModal.setState({
                    id: 'confirm-modal',
                    type: mode === 'create' ? 'cancel-unit-test' : 'cancel-unit-test-update',
                    content: {
                        closeText: t('common')['cancel'],
                        submitText: t('common')['submit-btn'],
                        onSubmit: () => {
                            window.removeEventListener('beforeunload', beforeUnloadHandler);
                            router.events.off('routeChangeStart', beforeRouteHandler);
                            router.push(url);
                        },
                    },
                });

                // to inform NProgress or something ...
                router.events.emit('routeChangeError');
                // tslint:disable-next-line: no-string-throw
                throw `Route change to "${url}" was aborted (this error can be safely ignored). See https://github.com/zeit/next.js/issues/2476.`;
            }
        };

        if ((mode === 'create' || mode === 'update') && !isCreateSuccess) {
            window.addEventListener('beforeunload', beforeUnloadHandler);
            router.events.on('routeChangeStart', beforeRouteHandler);
        }

        return () => {
            window.removeEventListener('beforeunload', beforeUnloadHandler);
            router.events.off('routeChangeStart', beforeRouteHandler);
        };
    }, [mode, isCreateSuccess]);

    return null;
};

export default ConfirmLeave;