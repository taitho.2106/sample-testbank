import { createContext } from 'react'

export const AppContext = createContext(null)

export const TemplateContext = createContext(null)

export const TemplateDetailContext = createContext(null)

export const UnitTestContext = createContext(null)

export const TemplateSectionsContext = createContext(null)

export const WrapperContext = createContext(null)

export const WrapperAdminContext = createContext(null)

export const QuestionContext = createContext(null)

export const PreviewResultContext = createContext(null)

export const LoginContext = createContext(null)

export const SingleTemplateContext = createContext(null)

export const UnitTestV1Context = createContext(null)

export const UnitTestWrapper = createContext(null)

export const ClassesContext = createContext(null)