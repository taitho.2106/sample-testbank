import { StructType } from './types'

export const PUBLISHER_SELECTIONS: StructType[] = [
  { code: 'NA', display: 'undefined' },
  { code: 'DTP', display: 'DTP' },
  { code: 'EX', display: 'EXPRESS' },
  { code: 'GE', display: 'Global ELT' },
  { code: 'IN', display: 'INNOVA' },
  { code: 'RMD', display: 'Richmond' },
]

// export const SERIES_SELECTIONS: StructType[] = [
//   { code: 'NA', display: 'Undefined' },
//   { code: 'IW', display: 'I-learn Smart World' },
//   { code: 'IW1', display: 'I-learn Smart World 1' },
//   { code: 'MS', display: 'Math & Science' },
//   { code: 'RO', display: 'Right-on!' },
//   { code: 'IS', display: 'i-learn smart start' },
// ]

export const LEVEL_SELECTIONS: StructType[] = [
  { code: 'NA', display: 'undefined' },
  { code: 'M1', display: 'M1 - Nhận biết' },
  { code: 'M2', display: 'M2 - Hiểu' },
  { code: 'M3', display: 'M3 - Vận dụng' },
  { code: 'M4', display: 'M4 - Vận dụng cao' },
]

export const TEMPLATE_LEVEL_SELECTIONS: StructType[] = [
  { code: 'M1', display: 'M1 - Nhận biết' },
  { code: 'M2', display: 'M2 - Hiểu' },
  { code: 'M3', display: 'M3 - Vận dụng' },
  { code: 'M4', display: 'M4 - Vận dụng cao' },
]

// export const GRADE_SELECTIONS: StructType[] = [
//   { code: 'G6', display: 'Lớp 6' },
//   { code: 'G7', display: 'Lớp 7' },
//   { code: 'G8', display: 'Lớp 8' },
//   { code: 'G9', display: 'Lớp 9' },
//   { code: 'G10', display: 'Lớp 10' },
// ]

export const CERF_SELECTIONS: StructType[] = [
  { code: 'NA', display: 'undefined' },
  { code: 'PA1Y', display: 'Pre A1 (starters)' },
  { code: 'A1Y', display: 'A1 (Movers)' },
  { code: 'A2Y', display: 'A2 (Flyers)' },
  { code: 'A1', display: 'A1 (Beginner)' },
  { code: 'A1+', display: 'A1+ (False beginner)' },
  { code: 'A2', display: 'A2 (Elementary)' },
  { code: 'B1', display: 'B1 (Pre-intermediate)' },
  { code: 'B1+', display: 'B1+ (Intermediate)' },
  { code: 'B2', display: 'B2 (Upper-intermediade)' },
  { code: 'C1', display: 'C1 (Advanced)' },
  { code: 'C2', display: 'C2 (Proficiency)' },
]

export const FORMAT_SELECTIONS: StructType[] = [
  { code: 'NA', display: 'undefined' },
  { code: 'DO', display: 'DOET' },
  { code: 'CA', display: 'CAMBRIDGE' },
  { code: 'IELTS', display: 'IELTS' },
  { code: 'APTIS', display: 'APTIS' },
  { code: 'TOEIC', display: 'TOEIC' },
  { code: 'TOFLE', display: 'TOEFL' },
]

export const TEST_TYPE_SELECTIONS: StructType[] = [
  // { code: 'NA', display: 'Undefined' },
  { code: 'EN', display: 'Tiếng Anh' },
  { code: 'MA', display: 'Toán' },
]

export const TYPES_SELECTIONS: StructType[] = [
  { code: 'NA', display: 'Undefined' },
  { code: 'PT', display: 'Xếp lớp' },
  { code: 'UT', display: 'Unit tests' },
  { code: 'PGT', display: 'Thường xuyên HK1' },
  { code: 'M1T', display: 'Giữa HK1' },
  { code: 'T1T', display: 'Cuối HK1' },
  { code: 'PGT2', display: 'Thường xuyên HK2' },
  { code: 'M2T', display: 'Giữa HK2' },
  { code: 'T2T', display: 'Cuối HK2' },
  { code: 'AM10', display: 'Tuyển sinh 10' },
  { code: 'E10M', display: 'Tuyển sinh 10 chuyên' },
  { code: 'HSG', display: 'Tốt nghiệp THPT' },
  { code: 'RC', display: 'Đánh giá năng lực' },
]

export const SKILLS_SELECTIONS: any = Object.freeze([
  { code: 'PR', display: 'Pronunciation' },
  { code: 'VO', display: 'Vocab' },
  { code: 'GR', display: 'Grammar' },
  { code: 'RE', display: 'Reading' },
  { code: 'WR', display: 'Writing' },
  { code: 'LI', display: 'Listening' },
  { code: 'SP', display: 'Speaking' },
  //{ code: 'UE', display: 'Use of English' },
])

export const TASK_SELECTIONS: StructType[] = [
  { code: 'MC1', display: 'Multiple Choice - Type 1' },
  { code: 'MC2', display: 'Multiple Choice - Type 2' },
  { code: 'MC3', display: 'Multiple Choice - Type 3' },
  { code: 'MC4', display: 'Multiple Choice - Type 4' },
  { code: 'MC5', display: 'Multiple Choice - Type 5' },
  { code: 'MC6', display: 'Multiple Choice - Type 6' },
  { code: 'MC7', display: 'Multiple Choice - Type 7' },
  { code: 'MC8', display: 'Multiple Choice - Type 8' },
  { code: 'MC9', display: 'Multiple Choice - Type 9' },
  { code: 'MC10', display: 'Multiple Choice - Type 10' },
  { code: 'MC11', display: 'Multiple Choice - Type 11' },
  { code: 'FB1', display: 'Fill In The Blanks - Type 1' },
  { code: 'FB2', display: 'Fill In The Blanks - Type 2' },
  { code: 'FB3', display: 'Fill In The Blanks - Type 3' },
  { code: 'FB4', display: 'Fill In The Blanks - Type 4' },
  { code: 'FB5', display: 'Fill In The Blanks - Type 5' },
  { code: 'FB6', display: 'Fill In The Blanks - Type 6' },
  { code: 'FB7', display: 'Fill In The Blanks - Type 7' },
  { code: 'FB8', display: 'Fill In The Blanks - Type 8' },
  { code: 'SL1', display: 'Select from list - Type 1' },
  { code: 'SL2', display: 'Select from list - Type 2' },
  { code: 'DD1', display: 'Drag and Drop - Type 1' },
  { code: 'DD2', display: 'Drag and Drop - Type 2' },
  { code: 'DL1', display: 'Draw Line - Type 1' },
  { code: 'DL2', display: 'Draw Line - Type 2' },
  { code: 'DL3', display: 'Draw Line - Type 3' },
  { code: 'SA1', display: 'Short Answer - Type 1' },
  { code: 'TF1', display: 'True False - Type 1' },
  { code: 'TF2', display: 'True False - Type 2' },
  { code: 'TF3', display: 'True False - Type 3' },
  { code: 'TF4', display: 'True False - Type 4' },
  { code: 'TF5', display: 'True False - Type 5' },
  { code: 'MG1', display: 'Matching - Type 1' },
  { code: 'MG2', display: 'Matching - Type 2' },
  { code: 'MG3', display: 'Matching - Type 3' },
  { code: 'MG4', display: 'Matching - Type 4' },
  { code: 'MG5', display: 'Matching - Type 5' },
  { code: 'MG6', display: 'Matching - Type 6' },
  { code: 'MR1', display: 'Multiple Response - Type 1' },
  { code: 'MR2', display: 'Multiple Response - Type 2' },
  { code: 'MR3', display: 'Multiple Response - Type 3' },
  { code: 'LS1', display: 'Liker Scale - Type 1' },
  { code: 'LS2', display: 'Liker Scale - Type 2' },
  { code: 'MIX1', display: 'TF2 AND MC5' },
  { code: 'MIX2', display: 'LS1 AND MC5' },
  { code: 'SO1', display: 'Select Object - Type 1' },
  { code: 'DS1', display: 'Draw Shape - Type 1' },
  { code: 'FC1', display: 'Fill Color - Type 1' },
  { code: 'FC2', display: 'Fill Color - Type 2' },
]

export const PRACTICE_TEST_ACTIVITY: any = {
  // MC1: 1,
  // MC2: 1,
  MC3: 1,
  MC4: 4,
  MC5: 11,
  MC6: 11,
  FB1: 2,
  FB2: 3,
  FB3: 3,
  // FB4: 3,
  FB5: 14,
  FB6: 3,
  // FB7: 3,
  SL1: 5,
  SL2: 16,
  DD1: 6,
  SA1: 7,
  TF1: 8,
  TF2: 8,
  TF3: 19,
  TF4: 8,
  TF5: 29,
  MG1: 9,
  MG2: 9,
  MG3: 9,
  MR1: 12,
  MR2: 12,
  MR3: 12,
  LS1: 15,
  LS2: 15,
  MIX1: 17,
  MIX2: 18,
  MC1:20,
  MC2:21,
  MC7:22,
  MG4:23,
  MC9:24,
  FB4:25,
  FB7:26,
  FB8:27,
  DL1: 28,
  MC8: 30, 
  MG6: 33,
  SO1: 31,
  MC10: 32,  
  DD2: 36,
  DL2: 34,
  DS1:38,
  MG5:37,
  FC1:39,
  FC2:42,
  DL3:40,
  MC11: 41,
}

export const QUESTION_CREATED_BY: typeUnitType[] = [
  { code: '0', display: 'Câu hỏi của hệ thống', key: "system-question"},
  { code: '1', display: 'Câu hỏi của tôi', key: "my-question" },
]

export const TEMPLATE_CREATED_BY: typeUnitType[] = [
  { code: '0', display: 'Cấu trúc của hệ thống', key: 'operator-template' },
  { code: '1', display: 'Cấu trúc của tôi', key: 'my-template'},
]

export const ACTIVE_STATUS: StructType[] = [
  { code: 'AC', display: 'Hoạt động' },
  { code: 'DI', display: 'Không hoạt động' },
]

export const TASK_DESCRIPTION: any = {
  MC1: 'Câu hỏi dạng text điền vào chỗ trống gợi ý',
  MC2: 'Câu hỏi dạng có hình gợi ý',
  MC3: 'Câu hỏi dạng text gợi ý',
  MC4: 'Chọn đáp án, có từ gạch chân (biến thể của dạng 3)',
  MC5: 'Đọc đoạn văn và trả lời câu hỏi (theo group)',
  MC6: 'Nghe và trả lời câu hỏi (theo group)',
  MC7: 'Nghe và chọn ra hình ảnh đúng',
  MC8: 'Xem và chọn ra đáp án đúng',
  MC9: 'Xem hình ảnh minh họa và chọn đáp án đúng',
  MC10: 'Nghe audio và xem hình ảnh minh họa để chọn đáp án đúng',
  MC11: 'Nghe audio và xem hình ảnh minh họa để chọn đáp án đúng',
  FB1: 'Điền vào chỗ trống với từ gợi ý sẵn',
  FB2: 'Điền từ thích hợp vào chỗ trống',
  FB3: 'Nghe và điền từ bất kỳ vào chỗ trống (Fill in the blanks) cho phù hợp với đoạn văn',
  FB4: 'Điền vào chỗ trống cho đoạn văn theo gợi ý từ hình ảnh',
  FB5: 'Điền vào chỗ trống cho các câu dựa vào hình ảnh gợi ý',
  SL1: 'Điền vào ô trống bằng cách lựa chọn các đáp án cho sẵn',
  DD1: 'Sắp xếp các từ thành câu hoàn chỉnh',
  DD2: 'Nghe audio và sắp xếp các câu trả lời với hình ảnh đúng',
  SA1: 'Viết lại câu theo gợi ý (nếu có) để không thay đổi nghĩa của câu cho trước',
  TF1: 'Nghe audio và trả lời câu hỏi True-False',
  TF2: 'Đọc đoan văn và trả lời câu hỏi True-False',
  TF3: 'Xem hình minh họa và trả lời câu hỏi True-False',
  TF4: 'Xem hình ảnh và trả lời câu hỏi True-False',
  TF5: 'Nghe audio và trả lời câu hỏi True-False',
  MG1: 'Đọc đoạn văn và nối các câu ở cột A với câu trả lời đúng ở cột B (cột A cố định)',
  MG2: 'Nối các câu ở cột trái với câu trả lời đúng ở cột bên phải',
  MG3: 'Nghe và nối các câu ở cột trái với câu trả lời đúng ở cột bên phải',
  MG4: 'Xem hình minh họa và nối các câu ở cột trái với câu trả lời đúng ở cột bên phải',
  MG5: 'Nối các câu ở cột trái với câu trả lời đúng ở cột bên phải',
  MR1: 'Lựa chọn các đáp án đúng từ các từ cho sẵn',
  MR2: 'Nghe audio và trả lời câu hỏi bằng cách lựa chọn các từ cho sẵn (dành cho câu hỏi ngắn)',
  MR3: 'Nghe audio và trả lời câu hỏi theo yêu cầu (dành cho hỏi dài)',
  LS1: 'Đoạn đoạn văn và trả lời các câu hỏi',
  LS2: 'Nghe audio và trả lời các câu hỏi',
  SO1: 'Nghe audio và chọn hình ảnh đúng',
  DL1: '',
  DL2: ''
}

export interface typeUnitType extends StructType {
  key?: string
}

export const UNIT_TYPE: typeUnitType[] = [
  { code: '0', display: 'Luyện tập', key: 'practice' }, // admin | operator | teacher -> end_date can be selected
  { code: '1', display: 'Kiểm tra', key: 'exam' }, // only teacher -> end_date = start_date + time
]
