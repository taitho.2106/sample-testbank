import {
  DisplayTemplateTableDataType,
  FootnavItemType,
  SidenavItemType,
} from './types'

export const USER_ROLES = {
  Operator: 1,
  Teacher: 2,
  Student: 4,
  Sale: 5,
}

export const FOOTNAV_ITEMS: FootnavItemType[] = [
  {
    name: 'Contact Us',
    url: '/',
  },
  {
    name: 'Help',
    url: '/',
  },
  {
    name: 'Privacy & Policy',
    url: '/',
  },
]

export const RightIndexes={
  Home:0,
  Menu_General:1, //Tổng quan

  Menu_MyExamBank:2, //Ngân hàng đề thi của tôi
  Menu_Mine_ExamFormat:3,
  Menu_Mine_QuestionBank:4,
  Menu_Mine_Exams:5,

  Menu_TeacherExamBank:6, //Ngân hàng đề thi của giáo viên
  Menu_Teacher_ExamFormat:7,
  Menu_Teacher_QuestionBank:8,
  Menu_Teacher_Exams:9,

  Menu_SystemExamBank:10, //Ngân hàng đề thi của hệ thống
  Menu_System_ExamFormat:11,
  Menu_System_QuestionBank:12,
  Menu_System_Exams:13,

  UploadQuestions:14, //tải lên (question)
  Mine_EditExamFormat:15,
  Mine_DeleteExamFormat:16,
  Mine_EditQuestion:17,
  Mine_DeleteQuestion:18,
  Mine_EditExam:19,
  Mine_DeleteExam:20,

  Teacher_EditExamFormat:21,
  Teacher_DeleteExamFormat:22,
  Teacher_EditQuestion:23,
  Teacher_DeleteQuestion:24,
  Teacher_EditExam:25,
  Teacher_DeleteExam:26,


  System_EditExamFormat:21,
  System_DeleteExamFormat:22,
  System_EditQuestion:23,
  System_DeleteQuestion:24,
  System_EditExam:25,
  System_DeleteExam:26,

  AccountSetting:27, // cài đặt tài khoản (backend)
}

export const SIDENAV_ITEMS: SidenavItemType[] = [
  {
    id: 0,
    name: 'Tổng quan',
    icon: 'general',
    url: '/',
  },
  {
    id: 1,
    name: 'Ngân hàng đề thi của tôi',
    icon: 'template',
    access_role: [-1, USER_ROLES.Teacher],
    list: [
      {
        id: 1,
        name: 'Định dạng đề thi',
        url: '/templates?m=mine',
        baseUrl: [{ url: '/templates/[templateSlug]', query: { m: 'mine' } }],
        active_role: [-1, USER_ROLES.Teacher],
      },
      {
        id: 2,
        name: 'Ngân hàng câu hỏi',
        url: '/questions?m=mine',
        baseUrl: [{ url: '/questions/[questionSlug]', query: { m: 'mine' } }],
        active_role: [-1, USER_ROLES.Teacher],
      },
      {
        id: 3,
        name: 'Danh sách đề thi',
        url: '/unit-test?m=mine',
        baseUrl: [{ url: '/unit-test/[templateSlug]', query: { m: 'mine' } },{ url: '/unit-test/[templateSlug]/export-zip',query: { m: 'mine' } }],
        active_role: [-1, USER_ROLES.Teacher],
      },
    ],
  },
  {
    id: 2,
    name: 'Ngân hàng đề thi của giáo viên',
    icon: 'template',
    access_role: [],
    list: [
      {
        id: 1,
        name: 'Định dạng đề thi',
        url: '/templates?m=teacher',
        baseUrl: [
          { url: '/templates/[templateSlug]', query: { m: 'teacher' } },
        ],
      },
      {
        id: 2,
        name: 'Ngân hàng câu hỏi',
        url: '/questions?m=teacher',
        baseUrl: [
          { url: '/questions/[questionSlug]', query: { m: 'teacher' } },
        ],
      },
      {
        id: 3,
        name: 'Danh sách đề thi',
        url: '/unit-test?m=teacher',
        baseUrl: [
          { url: '/unit-test/[templateSlug]', query: { m: 'teacher' } },
          { url: '/unit-test/[templateSlug]/export-zip',query: { m: 'teacher' } }
        ],
      },
    ],
  },
  {
    id: 3,
    name: 'Ngân hàng đề thi của hệ thống',
    icon: 'question',
    list: [
      {
        id: 1,
        name: 'Định dạng đề thi',
        url: '/templates',
        baseUrl: [{ url: '/templates/[templateSlug]' }],
        active_role: [USER_ROLES.Operator,USER_ROLES.Teacher],
      },
      {
        id: 2,
        name: 'Ngân hàng câu hỏi',
        url: '/questions',
        baseUrl: [{ url: '/questions/[questionSlug]' }],
        active_role: [USER_ROLES.Operator,USER_ROLES.Teacher],
      },
      {
        id: 3,
        name: 'Danh sách đề thi',
        url: '/unit-test',
        baseUrl: [{ url: '/unit-test/[templateSlug]' },{ url: '/unit-test/[templateSlug]/export-zip' }],
        active_role: [USER_ROLES.Operator,USER_ROLES.Teacher],
        regexUrls:[
          /(\?g=\w+)*(\&s=\w)/gm, //open by grade and series
        ]
      },
      {
        id: 4,
        name: 'Đề thi tích hợp với đối tác',
        url: '/integrate-exam',
        baseUrl: [{ url: '/integrate-exam/[integrateExamSlug]' }],
        active_role: [USER_ROLES.Operator],
      },
    ],
  }

  // {
  //   id: 4,
  //   name: 'Quản lý người dùng',
  //   icon: 'user',
  //   url: '/users',
  //   access_role: [],
  // },
]

export const SIDENAV_ITEM_ICONS: any = {
  general: {
    active: '/images/icons/ic-general-active.png',
    light: '/images/icons/ic-general-light.png',
    white: '/images/icons/ic-general-white.png',
  },
  question: {
    active: '/images/icons/ic-question-active.png',
    light: '/images/icons/ic-question-light.png',
    white: '/images/icons/ic-question-white.png',
  },
  template: {
    active: '/images/icons/ic-template-active.png',
    light: '/images/icons/ic-template-light.png',
    white: '/images/icons/ic-template-white.png',
  },
  unitTest: {
    active: '/images/icons/ic-unit-test-active.png',
    light: '/images/icons/ic-unit-test-light.png',
    white: '/images/icons/ic-unit-test-white.png',
  },
  user: {
    active: '/images/icons/ic-user-active.png',
    light: '/images/icons/ic-user-light.png',
    white: '/images/icons/ic-user-white.png',
  }
}

export const TEMPLATE_TABLE_COL_DATA: DisplayTemplateTableDataType[] = [
  { id: 1, name: 'Name', width: 40, align: 'left', property: 'name' },
  {
    id: 2,
    name: 'Level',
    width: 15,
    align: 'center',
    property: 'templateLevelId',
  },
  { id: 3, name: 'Time', width: 10, align: 'center', property: 'time' },
  {
    id: 4,
    name: 'Total questions',
    width: 15,
    align: 'center',
    property: 'totalQuestions',
  },
]

export const TEMPLATE_UNITTEST_TABLE_COL_DATA: DisplayTemplateTableDataType[] =
  [
    { id: 1, name: 'Tên cấu trúc', width: 40, align: 'left', property: 'name' },
    {
      id: 2,
      name: 'Khối lớp',
      width: 15,
      align: 'center',
      property: 'templateLevelId',
    },
    {
      id: 3,
      name: 'Thời gian (phút)',
      width: 10,
      align: 'center',
      property: 'time',
    },
    {
      id: 4,
      name: 'Tổng số câu hỏi',
      width: 15,
      align: 'center',
      property: 'totalQuestions',
    },
    {
      id: 5,
      name: 'Số lượng kỹ năng',
      width: 15,
      align: 'center',
      property: 'totalQuestions',
    },
  ]

//server test
//export const URL_FROM_EDUHOME = "http://eduhome.dtpds.com/BookDetail?idSubject=1&idSupplement=1011&idTheme=2214";
//server release
export const URL_FROM_EDUHOME = "https://eduhome.com.vn/BookDetail?idGrade=0&idSubject=1&idSeries=0&idSupplement=1011&idTheme=1260";

export const USER_GUIDE_VIDEO_SFTP_PATH = "/opt/video/userguide_testbank"

export const USER_GUIDE_SCREEN_ID={

  Home:1,
  Register:2,
  Login:3,
  ForgotPassWord:4,
  General:5,

  My_Templates:6,
  My_Templates_Create:7,
  My_Templates_Edit:8,
  My_Templates_View:9,

  My_Questions:10,
  My_Questions_Create:11,
  My_Questions_Edit:12,
  My_Questions_View:13,
  My_Questions_Upload:14,

  My_UnitTests:15,
  My_UnitTests_Create:16,
  My_UnitTests_Edit:17,
  My_UnitTests_View:18,


  System_Templates:19,
  System_Templates_View:20,

  System_Questions:21,
  System_Questions_View:22,

  System_UnitTests:23,
  System_UnitTests_View:24,

  // integrate
  Integration_Create:25,
  Integration_Update:26,
  Integration_View:27,

  //export
  Export_UnitTest:28
}
// #region admin page
export const SIDENAV_ITEM_ICONS_ADMIN:any={
  userManagement:{
    active:'/images/icons/ic-1-selected.png',
    light:'/images/icons/ic-1-default.png',
    white:'/images/icons/ic-1-hover.png'
  },
  decentralization:{
    active:'/images/icons/ic-2-selected.png',
    light:'/images/icons/ic-2-default.png',
    white:'/images/icons/ic-2-hover.png'
  },
  userGuideScreen:{
    active:'/images/icons/ic-1-selected.png',
    light:'/images/icons/ic-1-default.png',
    white:'/images/icons/ic-1-hover.png'
  },
  userGuideManagement:{
    active:'/images/icons/ic-3-selected.png',
    light:'/images/icons/ic-3-default.png',
    white:'/images/icons/ic-3-hover.png'
  },
  gradeManagement:{
    active:'/images/icons/ic-3-selected.png',
    light:'/images/icons/ic-3-default.png',
    white:'/images/icons/ic-3-hover.png'
  },
  seriesManagement:{
    active:'/images/icons/ic-3-selected.png',
    light:'/images/icons/ic-3-default.png',
    white:'/images/icons/ic-3-hover.png'
  },
  skillsManagement:{
    active:'/images/icons/ic-3-selected.png',
    light:'/images/icons/ic-3-default.png',
    white:'/images/icons/ic-3-hover.png'
  },
}
export const SIDENAV_ITEM_ADMIN = [
  {
    key:1,
    label:'Quản lý người dùng',
    url:'/users',
    icon:'userManagement',
    pathName: ["/users","/users/[userSlug]"]
  },
  {
    key:2,
    label:'Phân quyền',
    url:'/admin/decentralization',
    icon:'decentralization',
    pathName: ["/admin/decentralization"]
  },
  // {
  //   key:3,
  //   label:'Quản lý màn hình',
  //   url:'/admin/user-guide-screen',
  //   icon:'userGuideScreen',
  //   pathName: ["/admin/user-guide-screen"]
  // },
  {
    key:4,
    label:'Quản lý hướng dẫn sử dụng',
    url:'/admin/user-guide',
    icon:'userGuideManagement',
    pathName: ["/admin/user-guide"]
  },
  {
    key:5,
    label:'Quản lý khối lớp',
    url:'/admin/grade',
    icon:'gradeManagement',
    pathName: ["/admin/grade"]
  },
  {
    key:6,
    label:'Quản lý chương trình',
    url:'/admin/series',
    icon:'seriesManagement',
    pathName: ["/admin/series"]
  },
  {
    key:7,
    label:'Quản lý kỹ năng',
    url:'/admin/skills',
    icon:'skillsManagement',
    pathName: ["/admin/skills"]
  },
  {
    key:8,
    label:'Quản lý tích hợp',
    url:'/admin/third-party',
    icon:'skillsManagement',
    pathName: ["/admin/third-party"]
  },
]
// #endregion

//email config
export const EMAIL_CONFIG={
  user: process.env.NEXT_PUBLIC_SMTP_EMAIL,
  password: process.env.NEXT_PUBLIC_SMTP_PASSWORD,
  host: process.env.NEXT_PUBLIC_SMTP_HOST,
  port: +process.env.NEXT_PUBLIC_SMTP_PORT,
  [process.env.NEXT_PUBLIC_SMTP_ENCRYPTION]: true,
}
    // const client = new SMTPClient({
    //   user: 'no_reply@dtp-education.com',
    //   password: 'Dtponline@2012',
    //   host: 'smtp.office365.com',
    //   port: 25,
    //   tls: true,
    // })
export const COLOR_SHAPE=[
  {color:"fb2410", display:"red"},
  {color:"fcfb2d", display:"yellow"},
  {color:"62aa2d", display:"green"},
  {color:"038af9", display:"blue"},
  {color:"8400ab", display:"purple"},
  {color:"000000", display:"black"},
]
export const SHAPE_CODE=[
  'circle',
  'triangle',
  'square',
  'star',
  'rectangle',
  'oval'
]

export const ssrMode = typeof window === "undefined";


export const metaDefaults = (data: { href: string }, t:any) => ({
    description: "",
    image: "",
    title: `i-Test - ${t('title')}`,
    type: "website",
    url: ssrMode ? data.href : window.location.origin,
});

export const TABS = {
  OVERVIEW: 'overview',
  STUDENT: 'student',
  TEACHER: 'teacher',
  PARENTS: 'parents',
  SCHOOL: 'school'
}

const BASE_PATH = "/"


export const paths = {
    home: BASE_PATH,
    signIn: `${BASE_PATH}login`,
    signUp: `${BASE_PATH}register`,
    forgotPassword: `${BASE_PATH}forgot-password`,
    overview: `${BASE_PATH}home`,
    policySecurity: `${BASE_PATH}page/security-policy`,
    paymentPolicy: `${BASE_PATH}page/payment-policy`,
    feedbackPolicy: `${BASE_PATH}page/feedback-policy`,
    aboutUs: `${BASE_PATH}page/about-us`,
    shoppingGuide: `${BASE_PATH}page/shopping-guide`,
    createUnitTest: `${BASE_PATH}unit-test/create-new-unittest`,
    createTemplate: `${BASE_PATH}templates/create-new-template`,
    userInfo: `${BASE_PATH}users/info`,
    transactionHistory: `${BASE_PATH}users/info/transaction-history`,
    unitTest: `${BASE_PATH}unit-test`,
    unitTestSlug: `${BASE_PATH}unit-test/[templateSlug]`,
    detailSeries: `${BASE_PATH}unit-test/series-detail`,
    questions: `${BASE_PATH}questions`,
    questionSlug: `${BASE_PATH}questions/[questionSlug]`,
    templates: `${BASE_PATH}templates`,
    templateSlug: `${BASE_PATH}templates/[templateSlug]`,
    integrateExam: `${BASE_PATH}integrate-exam`,
    integrateExamSlug: `${BASE_PATH}integrate-exam/[integrateExamSlug]`,
    classes: `${BASE_PATH}classes`,
    classeSlug: `${BASE_PATH}classes/[classeSlug]`,
    studentSlug: `${BASE_PATH}students/[studentSlug]`,
    dashboard: `${BASE_PATH}dashboard`,
    dashboardTurnover: `${BASE_PATH}dashboard/turnover`,
    dashboardUser: `${BASE_PATH}dashboard/user`,
    dashboardServicePackage: `${BASE_PATH}dashboard/service-package`,
    dashboardClassRanking: `${BASE_PATH}dashboard/class-ranking`,
    testSchedule: `${BASE_PATH}test-schedule`,
    payment: `${BASE_PATH}payment`,
    paymentResults: `${BASE_PATH}payment/results`,
    learningResult: `${BASE_PATH}learning`,
    learningResultSlug: `${BASE_PATH}learning/[learningSlug]`,
    studentInfo: `${BASE_PATH}students/info`,
    studentAddClass: `${BASE_PATH}classes/add-new-student`,
    studentChosen: `${BASE_PATH}classes/choosen-students`,
    assignedUnitTest: `${BASE_PATH}assigned-unit-test`,
    assignedUnitTestSlug: `${BASE_PATH}assigned-unit-test/[assignSlug]`,
    studentDetail: `${BASE_PATH}students/details`,
    practiceTest: `${BASE_PATH}practice-test`,
    student_unit_test_result: `${BASE_PATH}unit-test/student-unit-test-result`,
    practiceTestSlug: `${BASE_PATH}practice-test/[practiceTestSlug]`,
}

export const phoneRegExp = /^(84|0|02)([35789])([0-9]{8})$/;
export const phoneRegExp1 = /^0([3|5|7|8|9]([0-9]{8}))$/;
export const numberRegex = /^\d+$/;
export const emailRegExp = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;

export const passwordRegex = /^[-a-zA-Z0-9-~!@#$%^&*()+/?\\_`=;:"'><.,|{}\[\]]+(\s+[-a-zA-Z0-9-~!@#$%^&*()+/?\\_`=;:"'><.,|{}\[\]]+)*$/;
export const whiteSpaceRegex = /^[^\s]+(\s+[^\s]+)*$/g;

export const ORDER_STATUS = {
    NEW: 0,
    DONE: 1,
    CANCEL: 2,
}

export const USER_ASSET_TYPE = {
    SERVICE_PACKAGE: 'SERVICE_PACKAGE',
    UNIT_TEST: 'UNIT_TEST',
    COMBO_SERVICE_PACKAGE: 'COMBO_SERVICE_PACKAGE'
}

export const STEPS = {
  INIT: "0",
  EDIT: "1",
  DONE: "2"
}

export const VNPAY_STATUS_CODE = {
    "00": "Giao dịch thành công",
    "07": "Trừ tiền thành công. Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường).",
    "09": "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.",
    "10": "Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần",
    "11": "Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.",
    "12": "Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.",
    "13": "Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.",
    "24": "Giao dịch không thành công do: Khách hàng hủy giao dịch",
    "51": "Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch.",
    "65": "Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày.",
    "75": "Ngân hàng thanh toán đang bảo trì.",
    "79": "Giao dịch không thành công do: KH nhập sai mật khẩu thanh toán quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch",
}

export const FORMAT_DATE = {
  ISO_DATE: "DD/MM/YYYY"
}

export const PACKAGES: any = {
  BASIC: {
    CODE: 'BASIC',
    NAME: 'Gói phổ thông',
  },
  TRIAL: {
    CODE: 'TRIAL',
    NAME: 'Gói dùng thử',
  },
  STANDARD: {
    CODE: 'STANDARD',
    NAME: 'Gói tiêu chuẩn',
  },
  ADVANCE: {
    CODE: 'ADVANCE',
    NAME: 'Gói nâng cao',
  },
  INTERNATIONAL: {
    CODE: 'INTERNATIONAL',
    NAME: 'Gói quốc tế',
  },
  COMBO: {
    CODE: 'COMBO',
    NAME: 'Gói Hot Deal Back to school',
  },
}

export const PACKAGES_FOR_STUDENT: any = {
  BEAT_FREE_EXAM: {
    CODE: 'BEAT_FREE_EXAM',
    NAME: 'Phá đảo đề thi miễn phí',
  },
  PREMIUM_PACKAGE: {
    CODE: 'PREMIUM_PACKAGE',
    NAME: 'Gói xịn hời',
  },
  SUPER_INTELLIGENCE_CHALLENGE: {
    CODE: 'SUPER_INTELLIGENCE_CHALLENGE',
    NAME: 'Thử thách siêu trí tuệ',
  },
  CERTIFICATE_MASTER: {
    CODE: 'CERTIFICATE_MASTER',
    NAME: 'Bá chủ mọi chứng chỉ',
  },
}

export const comboLevel = [
    PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE,
    PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE,
    PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE,
    PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE,
]

export const BANK_CODES = {
    VNPAYQR: 'VNPAYQR',
    VNBANK: 'VNBANK',
    INTCARD: 'INTCARD',
}

// index tương đương level, level cao sẽ bao gồm level thấp
export const PACKAGE_LEVEL_CONFIG = [
    PACKAGES.BASIC.CODE,
    PACKAGES.TRIAL.CODE,
    PACKAGES.ADVANCE.CODE,
    PACKAGES.INTERNATIONAL.CODE,
];

export const PACKAGE_LEVEL_FOR_STUDENT_CONFIG = [
  PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE,
  PACKAGES_FOR_STUDENT.PREMIUM_PACKAGE.CODE,
  PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE,
  PACKAGES_FOR_STUDENT.CERTIFICATE_MASTER.CODE
]

export const NUMBER_MONTH_PAYMENT = 12;

export const PAYMENT_TYPE = {
  RENEW: 'renew',
  UPGRADE: 'upgrade',
  BUYCOMBO: 'buy-combo',
}

export const ACCOUNT_STATUS = {
  ACTIVE: 'ACTIVE',
  LOCK: 'LOCK',
  INACTIVE: 'INACTIVE',
}

export const STATUS_CODE_EXAM = {
    UPCOMING: 1,
    ONGOING: 2,
    ENDED: 3,
}

export  const CODE_ASSIGN_OPTION = [
    {
      label: 'upcoming',
      value: STATUS_CODE_EXAM.UPCOMING
    },{
      label: 'ongoing',
      value: STATUS_CODE_EXAM.ONGOING
    },{
      label: 'ended',
      value: STATUS_CODE_EXAM.ENDED
    },
]

export const optionRole = [
  { label: 'teacher', value: USER_ROLES.Teacher },
  { label: 'student', value: USER_ROLES.Student },
]

export const VOUCHER_VALIDATE_ERROR_CODE = {
  1: "Thiếu voucher_code trong request body",
  2: "Mã giảm giá không tồn tại hoặc không thuộc phạm vi sử dụng",
  3: "Bạn đã hết lượt sử dụng mã giảm giá này",
  4: "Mã giảm giá đã hết lượt sử dụng",
  5: "Lỗi không xác định"
}