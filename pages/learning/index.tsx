import React from 'react'

import LearningContainer from '@/componentsV2/LearningResultPage/LearningContainer'
import { Wrapper } from 'components/templates/wrapper'
import useTranslation from "@/hooks/useTranslation";

export default function ClassesManagement() {
    const { t } = useTranslation()
    const pageTitle = t('learring-result')['title']

    return (
        <Wrapper pageTitle={pageTitle} hasStickyFooter={false} hiddenOverflow={true}>
            <LearningContainer />
        </Wrapper>
    )
}
