import React from "react";

import AssignResultsPage from "@/componentsV2/LearningResultPage/AssignResultsPage";
import { GetServerSideProps } from "next";

const LearningSlug = ({query}:any) => {
    return (
        <>
            <AssignResultsPage query={query}/>
        </>
    );
};

export default LearningSlug;

export const getServerSideProps: GetServerSideProps = async (context) => {
    const { query } = context
    return { props: { query } }
}