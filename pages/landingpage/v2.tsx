import Home from "@/componentsV2/Home";
import MasterLayout from "@/componentsV2/Layout";

const LandingPageV2 = () => {
    return (
        <MasterLayout>
            <Home />
        </MasterLayout>
    )
}

export function getServerSideProps() {
    return {
        redirect: {
            destination: '/',
            permanent: true,
        },
    };
}

export default LandingPageV2;
