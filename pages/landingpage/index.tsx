import { ThemeLandingPage } from 'components/landingpage'
import ChatHelp from 'components/chatHelp'

import { USER_GUIDE_SCREEN_ID } from '../../interfaces/constants'

export default function LandingPage() {
  return <>
    <ThemeLandingPage />
    <ChatHelp screenId={USER_GUIDE_SCREEN_ID.Home}></ChatHelp>
  </>
}


export function getServerSideProps() {
  return {
    redirect: {
      destination: '/',
      permanent: true,
    },
  };
}