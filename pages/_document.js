import Document, { Html, Head, Main, NextScript } from 'next/document'
import { googleTagManagerId } from '../constants'

export default class MyDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    {googleTagManagerId && (
                        <>
                            <script async src={`https://www.googletagmanager.com/gtag/js?id=${googleTagManagerId}`}></script>
                            <script
                                dangerouslySetInnerHTML={{
                                    __html: `
                                        window.dataLayer = window.dataLayer || [];
                                        function gtag(){dataLayer.push(arguments);}
                                        gtag('js', new Date());

                                        gtag('config', '${googleTagManagerId}', {
                                            send_page_view: false,
                                        });
                                    `
                                }}
                            />
                        </>
                    )}
                    <script
                        dangerouslySetInnerHTML={{
                            __html: `
                            function loadJsAsync(t,e){
                            var n=document.createElement("script");
                            n.type="text/javascript",
                            n.src=t,
                            n.addEventListener("load",function(t){
                            e(null,t)
                            },!1);
                            var a=document.getElementsByTagName("head")[0];
                            a.appendChild(n)
                            }
                            window.addEventListener("DOMContentLoaded",function(){
                            loadJsAsync("https://webchat.caresoft.vn:8090/js/CsChat.js?v=3.0",
                            function(){
                            var t={"domain":"DTPeducation_iTest","domainId":9450};
                            embedCsChat(t)
                            }
                            )},!1);
                            `
                        }}
                    />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}