import { BreadCrumbNav } from 'components/molecules/breadCrumbNav'
import { UserContainer } from 'components/organisms/UserContainer'
import { BreadcrumbItemType } from 'interfaces/types'

import WrapperAdmin from '../../components/pages/admin/WrapperAdmin'

const breadcrumbData: BreadcrumbItemType[] = [
  { name: 'Tổng quan', url: '/' },
  { name: 'Quản lý người dùng', url: null },
]

export default function UsersPage() {
  return (
    <WrapperAdmin
      className="p-users"
      pageTitle="Danh sách người dùng"
    >
      <div className="p-users__container" style={{ marginBottom: '2rem' }}>
        <div className="p-detail-format__breadcrumb">
          <BreadCrumbNav data={breadcrumbData} />
        </div>
        <UserContainer />
      </div>
    </WrapperAdmin>
  )
}