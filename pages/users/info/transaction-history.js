import React from "react";
import ComingSoonComponent from "../../../components-v2/Common/ComingSoonPage";

import {USER_ROLES} from "../../../interfaces/constants";
import {Wrapper} from "../../../components/templates/wrapper";
import ProfileLayout from "../../../components-v2/Admin/UserInfo";
import useTranslation from "../../../hooks/useTranslation";

const TransactionHistoryPage = () => {
    const { t } = useTranslation()
    const pageTitle = t('personal')['page-title']

    return (
        <Wrapper
            className="p-questions"
            pageTitle={pageTitle}
            access_role={[USER_ROLES.Operator, USER_ROLES.Teacher, USER_ROLES.Student]}
            userInfo={''}
            hasStickyFooter={false}
        >
            <ProfileLayout>
                <div style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    height: "50dvh",
                    color: 'orange',
                    fontSize: 40
                }}>
                    <ComingSoonComponent/>
                </div>
            </ProfileLayout>
        </Wrapper>
    )
}

export default TransactionHistoryPage;