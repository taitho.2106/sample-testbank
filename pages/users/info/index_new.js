import React from "react";

import {USER_ROLES} from "../../../interfaces/constants";
import {Wrapper} from "../../../components/templates/wrapper";
import ProfileLayout from "../../../components-v2/Admin/UserInfo";
import AccountInfo from "../../../components-v2/Admin/UserInfo/AccountInfo";

const UserInfoPage = () => {
    return (
        <Wrapper
            className="p-questions"
            pageTitle={''}
            access_role={[USER_ROLES.Operator, USER_ROLES.Teacher]}
            userInfo={''}
            hasStickyFooter={false}
        >
            <ProfileLayout>
                <AccountInfo/>
            </ProfileLayout>
        </Wrapper>
    )
}

export default UserInfoPage;