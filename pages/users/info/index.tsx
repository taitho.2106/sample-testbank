import { useSession } from 'next-auth/client';

import { USER_ROLES } from '@/interfaces/constants'
import { useUserInfo } from 'lib/swr-hook'

import ProfileLayout from "../../../components-v2/Admin/UserInfo";
import AccountInfo from "../../../components-v2/Admin/UserInfo/AccountInfo";
import { Wrapper } from '../../../components/templates/wrapper'
import useTranslation from "@/hooks/useTranslation";

export default function QuestionsPage() {
  const { t } = useTranslation()
  const [session] = useSession()
  const { user, mutateUserInfo } = useUserInfo()
  const pageTitle = t('personal')['page-title']

  return (
    <Wrapper
      className="p-questions"
      pageTitle={pageTitle}
      access_role={[USER_ROLES.Operator, USER_ROLES.Teacher, USER_ROLES.Student]}
      hasStickyFooter={false}
    >
      <ProfileLayout>
        {user && (
          <AccountInfo session={session} user={user} mutateUserInfo={mutateUserInfo}/>
        )}
      </ProfileLayout>
    </Wrapper>
  )
}