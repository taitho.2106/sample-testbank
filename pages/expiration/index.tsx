import {  useEffect } from 'react'
import { signOut,useSession } from 'next-auth/client'
import Link from 'next/link'
import { Button } from 'rsuite'
import { URL_FROM_EDUHOME } from '../../interfaces/constants'

async function LogOut(){
    console.log("signout");
    await signOut({ callbackUrl: '/expiration', redirect: false })
}
export default function Expiration(){
    const [session] = useSession();
    useEffect(() => {
        if (session !== null) {
            LogOut();
        }
      }, [session])
    
     

    return (
        <>
        <div className="p-expiration">
            <div className="bg-image-right">
            <img 
                src="/images/expiration-page/bg-image-right.png"
                alt="bg-image-right"
            />
            </div>
            <div className="bg-image-left">
            <img 
                src="/images/expiration-page/bg-image-left.png"
                alt="bg-image-left"
            />
            </div>
            <div className="bg-image-footer-right">
            <img 
                src="/images/expiration-page/bg-image-footer-right.png"
                alt="bg-image-footer-right"
            /> 
            </div>
            <div className="bg-image-footer-left">
            <img 
                src="/images/expiration-page/bg-image-footer-left.png"
                alt="bg-image-footer-left"
            />
            </div>     
           
            <div className="box">
                <div className="p-expiration__brand">
                    <Link href="/">
                    <a>
                        <img
                        className="brand-logo"
                        src="/images/collections/clt-logo.png"
                        alt=""
                        />
                    </a>
                    </Link>
                </div>
                <div className="icon">
                    <img 
                        src="/images/expiration-page/icon.png"
                        alt="expire-icon"
                    />
                </div>
                <div className="instruction">
                    <div className="instruction__big-warning">
                        Đã hết hạn dùng thử!
                    </div>
                    <div className="instruction_small-warning">
                        Vui lòng truy cập vào EduHome để gia hạn
                    </div>
                </div>
                <Link href={URL_FROM_EDUHOME}>
                <Button className="btn-access" >Truy cập EduHome ngay</Button>
                </Link>
                
            </div>
        </div>

        </>
    )
}