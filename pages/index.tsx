import Home from '@/componentsV2/Home'
import MasterLayout from '@/componentsV2/Layout'

const LandingPage = () => {
  return (
    <MasterLayout>
      <Home />
    </MasterLayout>
  )
}

export default LandingPage
