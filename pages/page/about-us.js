import React from "react";
import MasterLayout from "../../components-v2/Layout";
import AboutUs from "../../components-v2/Page/AboutUs";

const FeedbackPolicyPage = () => {
    return (
        <MasterLayout contentStyle={{background: '#F5F5F5'}}>
            <AboutUs/>
        </MasterLayout>
    )
}

export default FeedbackPolicyPage;