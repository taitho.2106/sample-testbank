import React from "react";
import MasterLayout from "../../components-v2/Layout";
import ShoppingGuide from "../../components-v2/Page/ShoppingGuide";

const FeedbackPolicyPage = () => {
    return (
        <MasterLayout contentStyle={{background: '#F5F5F5'}}>
            <ShoppingGuide/>
        </MasterLayout>
    )
}

export default FeedbackPolicyPage;