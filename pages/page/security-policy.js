import React from "react";
import MasterLayout from "../../components-v2/Layout";
import SecurityPolicy from "../../components-v2/Page/SecurityPolicy";

const PolicySecurityPage = () => {
    return (
        <MasterLayout contentStyle={{background: '#F5F5F5'}}>
            <SecurityPolicy/>
        </MasterLayout>
    )
}

export default PolicySecurityPage;