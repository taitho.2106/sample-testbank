import React from "react";
import MasterLayout from "../../components-v2/Layout";
import FeedbackPolicy from "../../components-v2/Page/FeedbackPolicy";

const FeedbackPolicyPage = () => {
    return (
        <MasterLayout contentStyle={{background: '#F5F5F5'}}>
            <FeedbackPolicy/>
        </MasterLayout>
    )
}

export default FeedbackPolicyPage;