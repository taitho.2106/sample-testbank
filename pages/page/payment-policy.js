import React from "react";
import MasterLayout from "../../components-v2/Layout";
import PaymentPolicy from "../../components-v2/Page/PaymentPolicy";

const PaymentPolicyPage = () => {
    return (
        <MasterLayout contentStyle={{background: '#F5F5F5'}}>
            <PaymentPolicy/>
        </MasterLayout>
    )
}

export default PaymentPolicyPage;