import Payment from '@/componentsV2/Payment/Main'
import MasterLayout from '@/componentsV2/Layout'

const PaymentPage = () => {
    return (
        <MasterLayout contentStyle={{}} footerStyle={{ marginTop: 0 }}>
            <Payment />
        </MasterLayout>
    )
}

export default PaymentPage
