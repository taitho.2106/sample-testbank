import PaymentResults from '@/componentsV2/Payment/Results'
import MasterLayout from '@/componentsV2/Layout'

const PaymentResultsPage = () => {
    return (
        <MasterLayout contentStyle={{}} footerStyle={{ marginTop: 0 }}>
            <PaymentResults />
        </MasterLayout>
    )
}

export default PaymentResultsPage
