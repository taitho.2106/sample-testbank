import { useEffect } from 'react'

import Cookies from 'js-cookie'
import { useRouter } from 'next/dist/client/router'

import { paths } from 'api/paths'
import api from 'lib/api'
import { isPositiveInteger } from 'utils/number'
export default function Page({ query }: any) {
  const router = useRouter()
  useEffect(() => {
    if (query) {
      const slug: string = query.slug as string
      const subSlug: string = query.subSlug as string
      // check positive integer
      if (subSlug && slug && isPositiveInteger(subSlug)) {
        //check code third_party
        const fetchData = async () => {
          const res = await api.get(paths.api_get_third_party)
          if (res.status == 200) {
            const dataRes: any = res.data
            if (dataRes.code == 1) {
              if (dataRes.data.find((m: any) => m.id == slug.toUpperCase())) {
                Cookies.set('third_party_code', slug.toUpperCase(), {
                  secure: true
                })
                router.push(`/practice-test/===${subSlug}`)
                return
              }
            }
          }
          router.push('/')
        }
        fetchData()
      } else {
        console.log('ddddddddd')
        router.push('/')
        return
      }
    }
  }, [query])
  return <></>
}
Page.getInitialProps = async ({ query }: any) => {
  return {
    query,
  }
}
