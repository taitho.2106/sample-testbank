import { useState } from 'react'

import { useRouter } from 'next/router'

import { BreadCrumbNav } from 'components/molecules/breadCrumbNav'
import { USER_GUIDE_SCREEN_ID, USER_ROLES } from 'interfaces/constants'
import { QuestionContext } from 'interfaces/contexts'

import { QuestionContainer } from '../../components/organisms/questionContainer'
import { Wrapper } from '../../components/templates/wrapper'
import { BreadcrumbItemType, QuestionSearchType } from '../../interfaces/types'
import ChatHelp from 'components/chatHelp'
import useTranslation from "@/hooks/useTranslation";

export default function QuestionsPage() {
  const { t } = useTranslation();
  const router = useRouter()
  //userGuideScreenID
  const owned = router.query.m;
  const userGuideScreenId = owned=="mine"?USER_GUIDE_SCREEN_ID.My_Questions:(owned=='teacher'?
  -1:USER_GUIDE_SCREEN_ID.System_Questions)
  
  const pageTitle = router.query.m
    ? `${router.query.m === 'mine' ? t('question-page')['my-question'] : t('question-page')['teacher-question']}`
    : t('question-page')['system-question']

  const breadcrumbData: BreadcrumbItemType[] = [
    { name: t('left-menu')['overview'], url: '/' },
    {
      name: pageTitle,
      url: null,
    },
  ]

  const [search, setSearch] = useState<QuestionSearchType>({ page_index: 0 })

  return (
    <>
     <QuestionContext.Provider value={{ search, setSearch }}>
      <Wrapper
        className="p-questions"
        pageTitle={pageTitle}
        access_role={[USER_ROLES.Operator, USER_ROLES.Teacher]}
      >
        <div className="p-questions__container">
          <div className="p-detail-format__breadcrumb">
            <BreadCrumbNav data={breadcrumbData} />
          </div>
          <QuestionContainer />
        </div>
        
      </Wrapper>
    </QuestionContext.Provider>
    <ChatHelp screenId={userGuideScreenId}></ChatHelp>
    </>
   
  )
}
