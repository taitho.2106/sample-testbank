import { useCallback, useContext, useEffect, useMemo, useRef, useState } from "react";

import { GetServerSideProps } from 'next'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import { useForm } from 'react-hook-form'
import { Button, toaster, Tooltip, Whisper, Modal } from 'rsuite'
import { mutate } from 'swr'

import useNoti from "@/hooks/useNoti";
import { paths } from 'api/paths'
import { AlertNoti } from 'components/atoms/alertNoti'
import { Card } from 'components/atoms/card'
import { CheckBox } from 'components/atoms/checkbox'
import { InputField } from "components/atoms/inputField";
import { SelectField } from 'components/atoms/selectField'
import { SelectField2 } from 'components/atoms/selectField2'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import { BreadCrumbNav } from 'components/molecules/breadCrumbNav'
import DragAndDrop from 'components/molecules/templateQuestion/DragAndDrop'
import DragAndDrop2 from 'components/molecules/templateQuestion/DragAndDrop2'
import DrawLine1 from 'components/molecules/templateQuestion/DrawLine1'
import DrawLine2 from 'components/molecules/templateQuestion/DrawLine2'
import DrawLine3 from 'components/molecules/templateQuestion/DrawLine3'
import DrawShape1 from 'components/molecules/templateQuestion/DrawShape1'
import FillInColour1 from 'components/molecules/templateQuestion/FillColor1'
import FillInColour2 from 'components/molecules/templateQuestion/FillColor2'
import FillInBlank from 'components/molecules/templateQuestion/FillInBlank'
import FillInBlank5 from 'components/molecules/templateQuestion/FillInBlank5'
import FillInBlankGroup from 'components/molecules/templateQuestion/FillInBlankGroup'
import FillInBlankGroup4 from 'components/molecules/templateQuestion/FillInBlankGroup4'
import FillInBlankGroup7 from 'components/molecules/templateQuestion/FillInBlankGroup7'
import FillInBlankGroup8 from 'components/molecules/templateQuestion/FillInBlankGroup8'
import LikertScale from 'components/molecules/templateQuestion/LikertScale'
import MatchingGame from 'components/molecules/templateQuestion/MatchingGame'
import MatchingGame4 from 'components/molecules/templateQuestion/MatchingGame4'
import MatchingGameType5 from 'components/molecules/templateQuestion/MatchingGame5'
import MatchingGame6 from 'components/molecules/templateQuestion/MatchingGame6'
import MixGame1 from 'components/molecules/templateQuestion/MixGame1'
import MixGame2 from 'components/molecules/templateQuestion/MixGame2'
import MultiChoice from 'components/molecules/templateQuestion/MultiChoice'
import MultiChoice4 from 'components/molecules/templateQuestion/MultiChoice4'
import MultiChoiceGroupExample from 'components/molecules/templateQuestion/MultiChoiceGroupExample'
import MultiChoiceGroupMC1 from 'components/molecules/templateQuestion/MultiChoiceGroupMC1'
import MultiChoiceGroupMC10 from 'components/molecules/templateQuestion/MultiChoiceGroupMC10'
import MultiChoiceGroupMC11 from 'components/molecules/templateQuestion/MultiChoiceGroupMC11'
import MultiChoiceGroupMC2 from 'components/molecules/templateQuestion/MultiChoiceGroupMC2'
import MultiChoiceGroupMC7 from 'components/molecules/templateQuestion/MultiChoiceGroupMC7'
import MultiChoiceGroupMC8 from 'components/molecules/templateQuestion/MultiChoiceGroupMC8'
import MultiChoiceGroupMC9 from 'components/molecules/templateQuestion/MultiChoiceGroupMC9'
import MultiResponse from 'components/molecules/templateQuestion/MultiResponse'
import SelectFromList from 'components/molecules/templateQuestion/SelectFromList'
import SelectFromList2 from 'components/molecules/templateQuestion/SelectFromList2'
import SelectObjectType1 from 'components/molecules/templateQuestion/SelectObjectType1'
import ShortAnswer from 'components/molecules/templateQuestion/ShortAnswer'
import TrueFalse from 'components/molecules/templateQuestion/TrueFalse'
import TrueFalseType3 from 'components/molecules/templateQuestion/TrueFalseType3'
import TrueFalseType5 from 'components/molecules/templateQuestion/TrueFalseType5'
import { ActivityPicker } from 'components/organisms/questionPreviewSection'
import { StickyFooter } from 'components/organisms/stickyFooter'
import { Wrapper } from 'components/templates/wrapper'
import useGrade from 'hooks/useGrade'
import useSeries from 'hooks/useSeries'
import { PACKAGES, USER_ROLES } from 'interfaces/constants'
import { AppContext, WrapperContext } from 'interfaces/contexts'
import {
  CERF_SELECTIONS,
  FORMAT_SELECTIONS,
  LEVEL_SELECTIONS,
  PRACTICE_TEST_ACTIVITY,
  PUBLISHER_SELECTIONS,
  SKILLS_SELECTIONS,
  TASK_SELECTIONS,
  TYPES_SELECTIONS,
} from 'interfaces/struct'
import { BreadcrumbItemType } from 'interfaces/types'
import { useQuestion } from 'lib/swr-hook'
import { userInRight } from 'utils'
import { filterSeriesByGrade } from 'utils/series/filterSeriesByGrade'
import SortExampleQuestion from 'utils/sortExampleQuestion/index'
import useTranslation from "@/hooks/useTranslation";
import useCurrentUserPackage from "@/hooks/useCurrentUserPackage";

type PropsType = {
  idQues: string
}

export default function QuestionUpdatePage({ idQues }: PropsType) {
  const { t } = useTranslation()
  const [idQuestion, setIdQuestion] = useState(idQues)
  const router = useRouter()
  const viewMode = router.query.mode === 'view'

  return (
    <>
      <Wrapper
        className="p-question-update"
        pageTitle={`${viewMode ? t('common')['view-questions'] : idQuestion !== '-1' ? t('question-page')['update-question'] : t('question-page')['create-question']}`}
      >
        <QuestionContent
          idQuestionData={idQues}
          idQuestion={idQuestion}
          setIdQuestion={setIdQuestion}
          viewMode={viewMode}
        />
      </Wrapper>
    </>
  )
}

function QuestionContent({ idQuestionData, idQuestion, setIdQuestion, viewMode }: any) {
  const { t } = useTranslation()
  const router = useRouter()
  const [session] = useSession()
  const user: any = session?.user
  const { globalModal } = useContext(WrapperContext)

  const [isCopy, setIsCopy] = useState(false)

  const breadcrumbData: BreadcrumbItemType[] = [
    { name: t('left-menu')['overview'], url: '/' },
    { name: t('left-menu')['questions-bank'], url: '/questions' },
    {
      name: viewMode ? t('common')['view-questions'] : idQuestion !== '-1' ? t('question-page')['update-question'] : t('question-page')['create-question'],
      url: null,
    },
  ]

  const {
    register,
    formState: { errors },
    setValue,
    handleSubmit,
    getValues,
    clearErrors,
    reset,
  } = useForm<any>()
  const [isReset, setIsReset] = useState(false)
  const { getGrade, gradeList } = useGrade()
  const { getSeries } = useSeries()
  const { getNoti } = useNoti()
  const [dataReview, setDataReview] = useState(null)
  const { question } = useQuestion(idQuestionData)
  const [questionType, setQuestionType] = useState(question?.question_type)
  const isEditable =
    user?.is_admin ||
    idQuestion == -1 ||
    user?.id === (question as any)?.created_by ||
    ((question as any)?.scope === 0 && userInRight([USER_ROLES.Operator], session))
  const initQuestion = useRef(null)
  const { dataPlan } = useCurrentUserPackage()
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const dataGrade = useMemo(() => {
    return (isSystem || (question?.grade === 'IE')) ? getGrade : gradeList
  }, [dataPlan?.code, isSystem, getGrade, gradeList, question?.grade])

  const [gradeId, setGradeId] = useState(question?.grade)
  const [privacy, setPrivacy] = useState(question?.privacy)
  const formRef = useRef(null)
  const isCopyAble = isTeacher && ((dataPlan?.code !== PACKAGES.INTERNATIONAL.CODE && question?.grade !== 'IE') || (dataPlan?.code === PACKAGES.INTERNATIONAL.CODE)) || isSystem

  const { fullScreenLoading } = useContext(AppContext)

  useEffect(() => {
    if (question) {
      setQuestionType(question.question_type)
      console.debug('questionSlug====', question)
      initQuestion.current = question
    }
    setPrivacy(question?.privacy)
  }, [question])
  useEffect(() => {
    setValue('privacy', privacy)
  }, [privacy])

  const onSubmit = async (formData: any) => {
    console.debug('formData', formData)
    if (!isEditable) return
    try {
      formData.id = idQuestion
      if(!isShowPrivacy){
        formData.privacy = null
      }
      switch (formData.question_type) {
        case 'MC1':
        case 'MC5':
        case 'MC6':
          formData = SortExampleQuestion.sortExampleQuestionMC1(formData)
          break
        case 'MC2':
          formData = SortExampleQuestion.sortExampleQuestionMC2(formData)
          break
        case 'DD1':
          formData = SortExampleQuestion.sortExampleQuestionDD1(formData)
          break
        case 'DD2':
          formData = SortExampleQuestion.sortExampleQuestionDD2(formData)
          break
        case 'MG4':
          formData = SortExampleQuestion.sortExampleQuestionMG4(formData)
          break
        case 'MC7':
          formData = SortExampleQuestion.sortExampleQuestionMC7(formData)
          break
        case 'MC8':
          formData = SortExampleQuestion.sortExampleQuestionMC8(formData)
          break
        case 'MC9':
          formData = SortExampleQuestion.sortExampleQuestionMC9(formData)
          break
        case 'MC10':
          formData = SortExampleQuestion.sortExampleQuestionMC10(formData)
          break
        case 'TF5':
          formData = SortExampleQuestion.sortExampleQuestionTF5(formData)
          break
        case 'MG5':
          formData = SortExampleQuestion.sortExampleQuestionMG5(formData)
          break
        case 'DS1':
          formData = SortExampleQuestion.sortExampleQuestionDS1(formData)
          break
        case 'MG6':
          formData = SortExampleQuestion.sortExampleQuestionMG6(formData)
          break
        case 'MC11':
          formData = SortExampleQuestion.sortExampleQuestionMC11(formData)
          break
        default:
          break
      }
      const formData1 = new FormData()
      Object.keys(formData).forEach((key) => {
        if (key !== 'image_files') {
          if (
            (key === 'mc7_image_files' && formData.question_type == 'MC7' && formData[key]) ||
            (key === 'mc8_image_files' && formData.question_type == 'MC8' && formData[key]) ||
            (key === 'mc11_image_files' && formData.question_type == 'MC11' && formData[key])
          ) {
            for (let i = 0; i < formData[key].length; i++) {
              if (formData[key][i] != undefined) {
                for (let j = 0; j < formData[key][i].length; j++) {
                  formData1.append(`${key}_${i + 1}_${j + 1}`, formData[key][i][j] ?? new File([''], 'empty'))
                }
              }
            }
          } else if (key === 'image_file_group2' && formData[key] != undefined) {
            for (let i = 0; i < formData[key].length; i++) {
              formData1.append(`${key}_${i}`, formData[key][i] ?? new File([''], 'empty'))
            }
          } else if (key === 'image_file_group1' && formData[key] != undefined) {
            for (let i = 0; i < formData[key].length; i++) {
              if (formData[key][i] != undefined) {
                for (let j = 0; j < formData[key][i].length; j++) {
                  formData1.append(`${key}_${i + 1}_${j + 1}`, formData[key][i][j] ?? new File([''], 'empty'))
                }
              }
            }
          } else if ((key === 'mg6_images_left' || key === 'mg6_images_right') && formData[key] != undefined) {
            for (let i = 0; i < formData[key].length; i++) {
              formData1.append(`${key}_${i}`, formData[key][i] ?? new File([''], 'empty'))
            }
          } else {
            formData[key] != undefined && formData1.append(key, formData[key])
          }
        } else {
          if (formData[key] != undefined) {
            for (let i = 0; i < formData[key].length; i++) {
              const indexKey = `0${i}`.slice(-2)
              formData1.append(`${key}_${indexKey}`, formData[key][i] ?? new File([''], 'empty'))
            }
          }
        }
      })
      fullScreenLoading.setState(true)
      const res = await fetch(paths.api_questions, {
        method: idQuestion === '-1' ? 'POST' : 'PUT',
        body: formData1,
      })
      const json = await res.json()
      fullScreenLoading.setState(false)
      if (!res.ok) {
        console.debug("question---", json)
        const key = toaster.push(
          <AlertNoti type={'error'} message={json.message|| t('common-get-noti')['error'] } />,
          {
            placement: 'topCenter',
          },
        )
        setTimeout(() => toaster.remove(key), 2000)
        return;
      }

      if (idQuestion !== '-1') {
        await mutate(`${paths.api_questions}/${idQuestion}`, json, false)
        globalModal.setState({
          id: 'confirm-modal',
          type: 'update-question',
          content: {
            isCopy: isCopy,
            closeText: t('unit-test')["lists-question"],
            onClose: () => router.push(`/questions${isOperator ? '' : '?m=mine'}`),
            submitText: t('question-update-container')['create-more'],
            onSubmit: onContinueCreate,
          },
        })
      } else {
        globalModal.setState({
          id: 'confirm-modal',
          type: 'create-question',
          content: {
            isCopy: isCopy,
            closeText: t('unit-test')["lists-question"],
            onClose: () => router.push(`/questions${isOperator ? '' : '?m=mine'}`),
            submitText: t('question-update-container')['create-more'],
            onSubmit: onContinueCreate,
          },
        })
      }
    } catch (e: any) {
      throw Error(e.message)
    } finally {
      fullScreenLoading.setState(false)
    }
  }

  // const checkDataGroupChange = useCallback(
  //   (oldData: any, newData: any, properties: string[]) => {
  //     for (const property of properties) {
  //       if ((oldData[property] ?? '') != newData[property]) return true
  //     }
  //     return false
  //   },
  //   [],
  // )

  const renderError = useCallback((errors: any) => {
    const list = []
    for (const prop in errors) {
      if (['required', 'validate'].includes(errors[prop].type) || prop === 'name') continue
      list.push(<p key={prop} className="form-error-message">{`* ${errors[prop].message}`}</p>)
    }
    return list
  }, [])

  const checkError = useCallback((errors: any, prop: string) => {
    return errors[prop] ? 'error' : ''
  }, [])

  const refNameInput = useRef<NodeJS.Timeout>()
  const renderErrorNoti = useCallback((errors: any) => {
    if (!!errors['name']) {
      if (refNameInput.current) {
        clearTimeout(refNameInput.current);
      }

      refNameInput.current = setTimeout(() => {
        getNoti('error', 'topEnd', t('common-get-noti')['question-name-limit']);
      }, 0);
    }
  }, []);

  const onSelectChange = (name: string, value: any) => {
    initQuestion.current = question
    if (name === 'question_type') {
      setValue(name, value, {
        shouldValidate: value !== '',
      })
      if (value != questionType) {
        setQuestionType('')
        reset()
        setTimeout(() => {
          clearErrors()
          const values = getValues()
          console.debug('values-----', values)
          setQuestionType(value)
        }, 0)
      }
    } else {
      setValue(name, value, {
        shouldValidate: true,
      })
    }
  }
  type hasData = {
    hasReading: boolean
    hasAudio: boolean
    hasImage: boolean
  }
  const clearDataWithQuestionType = (questionTypeSelected: string, {
    hasReading = false,
    hasAudio = false,
    hasImage = false
  }: hasData) => {
    const typeSelected = questionTypeSelected?.substring(0, 2);
    const questionType = question?.question_type?.substring(0, 2);
    if (questionTypeSelected && typeSelected !== questionType && initQuestion.current !== null) {
      initQuestion.current = {
        ...initQuestion.current,
        question_type: questionTypeSelected,
        parent_question_description: question.question_description || question.parent_question_description,
        question_description: question.question_description || question.parent_question_description,
        correct_answers: null,
        answers: null,
        audio: hasAudio ? question.audio : null,
        image: hasImage ? question.image : null,
        audio_script: hasAudio ? question.audio_script : null,
        ...convertReadingText({
          isReading: hasReading,
          currentQuestionText: questionType,
          selectedQuestionText: typeSelected
        })
      };
    }
  };
  const convertReadingText = ({ isReading = false, currentQuestionText = "", selectedQuestionText = "" }: {
    isReading: boolean,
    currentQuestionText: string,
    selectedQuestionText: string
  }): any => {
    if (!isReading) {
      return {
        parent_question_text: null,
        question_text: null,
      };
    }

    const questionTypeUseParentQuestionText = new Set(["FB", "MC", "MI"]);

    if (questionTypeUseParentQuestionText.has(currentQuestionText) && !questionTypeUseParentQuestionText.has(selectedQuestionText)) {
      return {
        question_text: question.parent_question_text,
        parent_question_text: null,
      };
    }

    if (!questionTypeUseParentQuestionText.has(currentQuestionText) && questionTypeUseParentQuestionText.has(selectedQuestionText)) {
      return {
        parent_question_text: question.question_text,
        question_text: null
      };
    }

    return {
      question_text: question.question_text || question.parent_question_text,
      parent_question_text: question.parent_question_text || question.question_text
    };
  };
  const isOperator = userInRight([USER_ROLES.Operator], session)
  const isShowPrivacy = user?.is_admin == 1 && !router?.query?.m;
  const renderQuestionType = useMemo(() => {
    const getImage = new Set(questionImage)
    const getAudio = new Set(questionAudio)
    const getReading = new Set(questionReading)

    const hasImage = getImage.has(questionType)
    const hasAudio = getAudio.has(questionType)
    const hasReading = getReading.has(questionType)
    let QuestionTemplate = MultiChoice
    switch (PRACTICE_TEST_ACTIVITY[questionType]) {
      case 1:
        QuestionTemplate = MultiChoice
        break
      case 4:
        QuestionTemplate = MultiChoice4
        break
      case 11:
        QuestionTemplate = MultiChoiceGroupExample
        break
      case 2:
        QuestionTemplate = FillInBlank
        break
      case 3:
        QuestionTemplate = FillInBlankGroup
        break
      case 5:
        QuestionTemplate = SelectFromList
        break
      case 6:
        QuestionTemplate = DragAndDrop
        break
      case 7:
        QuestionTemplate = ShortAnswer
        break
      case 8:
        QuestionTemplate = TrueFalse
        break
      case 19:
        QuestionTemplate = TrueFalseType3
        break
      case 9:
        QuestionTemplate = MatchingGame
        break
      case 12:
        QuestionTemplate = MultiResponse
        break
      case 14:
        QuestionTemplate = FillInBlank5
        break
      case 15:
        QuestionTemplate = LikertScale
        break
      case 16:
        QuestionTemplate = SelectFromList2
        break
      case 17:
        QuestionTemplate = MixGame1
        break
      case 18:
        QuestionTemplate = MixGame2
        break
      case 20:
        QuestionTemplate = MultiChoiceGroupMC1
        break
      case 21:
        QuestionTemplate = MultiChoiceGroupMC2
        break
      case 22:
        QuestionTemplate = MultiChoiceGroupMC7
        break
      case 23:
        QuestionTemplate = MatchingGame4
        break
      case 24:
        QuestionTemplate = MultiChoiceGroupMC9
        break
      case 25:
        QuestionTemplate = FillInBlankGroup4
        break
      case 26:
        QuestionTemplate = FillInBlankGroup7
        break
      case 27:
        QuestionTemplate = FillInBlankGroup8
        break
      case 28:
        QuestionTemplate = DrawLine1
        break
      case 29:
        QuestionTemplate = TrueFalseType5
        break
      case 30:
        QuestionTemplate = MultiChoiceGroupMC8
        break
      case 33:
        QuestionTemplate = MatchingGame6
        break
      case 31:
        QuestionTemplate = SelectObjectType1
        break
      case 32:
        QuestionTemplate = MultiChoiceGroupMC10
        break
      case 36:
        QuestionTemplate = DragAndDrop2
        break
      case 37:
        QuestionTemplate = MatchingGameType5
        break
      case 34:
        QuestionTemplate = DrawLine2
        break
      case 38:
        QuestionTemplate = DrawShape1
        break
      case 39:
        QuestionTemplate = FillInColour1
        break
      case 40:
        QuestionTemplate = DrawLine3
        break
      case 42:
        QuestionTemplate = FillInColour2
        break
      case 41:
        QuestionTemplate = MultiChoiceGroupMC11
        break
    }
    if (initQuestion.current) {
      initQuestion.current.id = idQuestion
      initQuestion.current.audio_script = initQuestion.current.audio_script ?? ''
      clearDataWithQuestionType(questionType, { hasImage, hasAudio, hasReading });
    }
    return (
      <QuestionTemplate
        key={`${questionType}_${idQuestion}_${viewMode}_${isReset}`}
        question={initQuestion.current}
        register={register}
        setValue={setValue}
        isAudio={hasAudio}
        isReading={hasReading}
        isImage={hasImage}
        errors={errors}
        viewMode={viewMode}
        questionType={questionType}
      />
    )
  }, [viewMode, questionType, Object.keys(errors).length])

  const onClickReview = () => {
    handleSubmit((data) => {
      console.debug('dataa---------', data)
      onPauseAudio()
      setDataReview(null)
      setTimeout(() => {
        setDataReview({ ...data })
      }, 0)
    })(formRef.current)
  }
  const onPauseAudio = () => {
    const el = document.querySelector('audio')
    if (el) {
      el.pause()
      el.currentTime = 0
    }
  }

  console.debug('errors ====', errors)
  const onContinueCreate = () => {
    setQuestionType(null)
    setTimeout(() => {
      reset()
      setValue('name', '')
      setValue('question_type', '')
      router.push(
        `/questions/[questionSlug]${!isOperator ? '?m=mine' : ''}`,
        `/questions/-1${!isOperator ? '?m=mine' : ''}`,
      )
      setIdQuestion('-1')

      setIsReset(!isReset)
    }, 0)
  }

  const onHandleCopy = () => {
    setIsCopy(true)
    onPauseAudio()
    setIdQuestion('-1')
    router.push(
      `/questions/[questionSlug]${!isOperator ? '?m=mine' : ''}`,
      `/questions/-1${!isOperator ? '?m=mine' : ''}`,
      {
        shallow: true,
      },
    )
  }
  const onSelectChangeSeries = (value: any) => {
    setValue('series', value || '')
  }
  const onSelectChangeGrade = (value: any) => {
    setGradeId(value)
    setValue('grade', value || '', { shouldValidate: value != '' })
  }
  const handlePrivacyChange = ()=>{
    let value = null;
    if(privacy == 1){
      value = null;
    }else{
      value = 1
    }
    setPrivacy(value)
  }
  setValue('test_type', question?.test_type || 'EN')
  return (
    <>
      <div className="p-detail-format__breadcrumb">
        <BreadCrumbNav data={breadcrumbData} />
      </div>
      {(idQuestion === '-1' || question) && (
        <form ref={formRef} onSubmit={handleSubmit(onSubmit)} style={{ display: 'flex', flexDirection: 'column' }}>
          <Card title={t('question-page')['question-information']} style={{ marginTop: '1.8rem', zIndex: 3 }}>
            <div className="p-question-update__container">
              <div
                className="__form-control __form-container"
                style={{
                  display: 'flex',
                  flexWrap: 'wrap',
                  pointerEvents: viewMode ? 'none' : 'auto',
                }}
              >
                <InputField
                  className={`form-input col-4 ${checkError(errors, 'name')}`}
                  defaultValue={question?.name}
                  disabled={false}
                  label={t('common')['question-name']}
                  type="text"
                  setValue={setValue}
                  register={register('name', {
                    maxLength: {
                      value: 255,
                      message: t('common-get-noti')['question-name-limit'],
                    }
                  })}
                />
                <SelectField2
                  key={`${question?.id}`}
                  label={t('common')['question-type']}
                  register={register('question_type', {
                    required: t('question-validation-msg')['question-type-required'],
                  })}
                  defaultValue={question?.question_type}
                  data={TASK_SELECTIONS}
                  onChange={onSelectChange.bind(null, 'question_type')}
                  className={`form-input col-4 ${checkError(errors, 'question_type')}`}
                  disabled={idQuestion != -1}
                  style={{ zIndex: 11 }}
                  triggerReset={isReset}
                />
                {getGrade && (
                  <SelectFilterPicker
                    data={dataGrade}
                    isMultiChoice={false}
                    inputSearchShow={false}
                    isShowSelectAll={false}
                    onChange={onSelectChangeGrade}
                    className={`form-input col-4 ${checkError(errors, 'grade')}`}
                    label={t('common')['grade']}
                    defaultValue={question?.grade}
                    register={register('grade', {
                      required: t(t('formikForm')['required'], [t('common')['grade']]),
                    })}
                    name="grade"
                    style={{ zIndex: 10 }}
                    disabled={gradeId === 'IE' && isTeacher && router.query.m === 'mine'}
                  />
                )}
                {isOperator && getSeries && (
                  <SelectFilterPicker
                    data={filterSeriesByGrade(getSeries, getGrade, gradeId)}
                    isMultiChoice={false}
                    inputSearchShow={true}
                    isShowSelectAll={false}
                    onChange={onSelectChangeSeries}
                    className={`form-input col-4 ${checkError(errors, 'series')}`}
                    label={t('common')['series']}
                    register={register('series', {
                      validate: (value: any) => {
                        return true
                      },
                      required: t(t('formikForm')['required'], [t('common')['series']]),
                    })}
                    name="series"
                    menuSize="xxl"
                    defaultValue={question?.series}
                    placement="bottomEnd"
                  />
                )}
                <SelectField
                  label={t('common')['skill']}
                  register={register('skills', {
                    required: t(t('formikForm')['required'], [t('common')['skill']]),
                  })}
                  defaultValue={question?.skills.toString()}
                  data={SKILLS_SELECTIONS}
                  onChange={onSelectChange.bind(null, 'skills')}
                  className={`form-input col-4 ${checkError(errors, 'skills')}`}
                  style={{ zIndex: 8 }}
                />
                <SelectField
                  label={t('common')['level']}
                  register={register('level', {
                    required: t(t('formikForm')['required'], [t('common')['level']]),
                  })}
                  defaultValue={question?.level.toString()}
                  data={LEVEL_SELECTIONS.map(item => ({...item, display: t('contant')[item.code]}))}
                  onChange={onSelectChange.bind(null, 'level')}
                  className={`form-input col-4 ${checkError(errors, 'level')}`}
                  style={{ zIndex: 4 }}
                />
                {isOperator && (
                  <>
                    <SelectField
                      label={t('common')['publisher']}
                      register={register('publisher', {
                        required: t(t('formikForm')['required'], [t('common')['publisher']]),
                      })}
                      defaultValue={question?.publisher}
                      data={PUBLISHER_SELECTIONS}
                      onChange={onSelectChange.bind(null, 'publisher')}
                      className={`form-input col-4 ${checkError(errors, 'publisher')}`}
                      style={{ zIndex: 6 }}
                    />
                    {/* {getSeries && (<SelectField
                      label="Chương trình"
                      register={register('series', {
                        required: 'Chương trình không được để trống',
                      })}
                      defaultValue={question?.series}
                      data={getSeries}
                      onChange={onSelectChange.bind(null, 'series')}
                      className={`form-input col-4 ${checkError(
                        errors,
                        'series',
                      )}`}
                      style={{ zIndex: 5 }}
                    />
                    )} */}
                    <SelectField
                      label={t('common')['format']}
                      register={register('format', {
                        required: t(t('formikForm')['required'], [t('common')['format']]),
                      })}
                      defaultValue={question?.format}
                      data={FORMAT_SELECTIONS}
                      onChange={onSelectChange.bind(null, 'format')}
                      className={`form-input col-4 ${checkError(errors, 'format')}`}
                      style={{ zIndex: 3 }}
                    />
                    <SelectField
                      label={t('common')['types']}
                      register={register('types', {
                        required: t(t('formikForm')['required'], [t('common')['types']]),
                      })}
                      defaultValue={question?.types}
                      data={TYPES_SELECTIONS.map(item => ({...item, display: t('contant')[item.code]}))}
                      onChange={onSelectChange.bind(null, 'types')}
                      className={`form-input col-4 ${checkError(errors, 'types')}`}
                      style={{ zIndex: 2 }}
                    />
                    <SelectField
                      label="CEFR"
                      register={register('cerf', {
                        required: t(t('formikForm')['required'], ['CEFR']),
                      })}
                      defaultValue={question?.cerf}
                      data={CERF_SELECTIONS}
                      onChange={onSelectChange.bind(null, 'cerf')}
                      className={`form-input col-4 ${checkError(errors, 'cerf')}`}
                      style={{ zIndex: 1 }}
                    />
                    {isShowPrivacy && (
                      <Whisper
                        placement="top"
                        trigger="hover"
                        speaker={
                          <Tooltip>
                            {t('common-whisper')['privacy-question']}
                          </Tooltip>
                        }
                      >
                        <div className="a-select-picker __checkbox" style={{display:"flex", alignItems:"center", marginLeft:"1rem"}}>
                          <CheckBox
                            checked={privacy == 1}
                            onChange={handlePrivacyChange}
                          />
                          <span style={{paddingLeft:"1rem"}}>{t('common')['private']}</span>
                        </div>
                      </Whisper>
                    )}
                  </>
                )}
              </div>
            </div>
          </Card>
          {renderErrorNoti(errors)}
          {questionType && (
            <Card
              title={TASK_SELECTIONS.find((m) => m.code === questionType)?.display.toUpperCase()}
              style={{
                marginTop: '1.8rem',
                backgroundColor: '#C9ECF8',
                pointerEvents: viewMode ? 'none' : 'auto',
                zIndex: 2,
              }}
            >
              <div className="p-question-update__container">
                <div className="__form-control __form-container">{renderQuestionType}</div>
                <div className={'group-error'}>{renderError(errors)}</div>
              </div>
            </Card>
          )}
          <StickyFooter containerStyle={{ justifyContent: 'space-between' }}>
            <div style={{ display: 'flex' }}>
              <Whisper
                placement="top"
                trigger={'hover'}
                speaker={<Tooltip>{t('common-whisper')['preview']}</Tooltip>}
              >
                <Button className="__main-btn" onClick={onClickReview}>
                  <img src="/images/icons/ic-show-white.png" alt="Review" />
                  <span>{t('common')['preview']}</span>
                </Button>
              </Whisper>
              {idQuestion !== '-1' && viewMode && isCopyAble && (
                <Whisper
                  placement="top"
                  trigger={'hover'}
                  speaker={<Tooltip>{t('common-whisper')['copy']}</Tooltip>}
                >
                  <Button
                    className="__main-btn"
                    type="button"
                    style={{ backgroundColor: '#71AC5D' }}
                    onClick={onHandleCopy}
                  >
                    <span>{t('common')['copy']}</span>
                  </Button>
                </Whisper>
              )}
            </div>
            <div style={{ display: 'flex' }}>
              {isEditable && (
                <>
                  {!viewMode && (
                    <Whisper
                      placement="top"
                      trigger={'hover'}
                      speaker={
                        <Tooltip>
                          {idQuestion !== '-1'
                            ? t('common-whisper')['submit-create']
                            : t('common-whisper')['submit-edit']}
                        </Tooltip>
                      }
                    >
                      <Button className="__main-btn" type="submit" style={{ backgroundColor: '#6868AC' }}>
                        <span>{t('common')['completed']}</span>
                      </Button>
                    </Whisper>
                  )}
                  {viewMode && (
                    <Whisper
                      placement="top"
                      trigger={'hover'}
                      speaker={<Tooltip>{t('common-whisper')['edit']}</Tooltip>}
                    >
                      <Button
                        className="__main-btn"
                        type="button"
                        style={{ backgroundColor: '#6868AC' }}
                        onClick={() => {
                          onPauseAudio()
                          const query1 = router.query ?? {}
                          router.push(
                            `/questions/[questionSlug]${query1.hasOwnProperty('m') ? `?m=${query1['m']}` : ''}`,
                            `/questions/${question.id}${query1.hasOwnProperty('m') ? `?m=${query1['m']}` : ''}`,
                          )
                        }}
                      >
                        <span>{t('common')['edit']}</span>
                      </Button>
                    </Whisper>
                  )}
                </>
              )}
            </div>
          </StickyFooter>
        </form>
      )}
      <Modal
        className={`review-modal review-modal-${dataReview?.question_type}`}
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          margin: 0,
        }}
        open={dataReview != null}
        onClose={() => setDataReview(null)}
      >
        <Modal.Header>
          <Modal.Title>{t('question-page')['preview-question']}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <ActivityPicker data={dataReview} type={dataReview?.question_type} />
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setDataReview(null)} appearance="subtle">
            {t('common')['close']}
          </Button>
        </Modal.Footer>
      </Modal>
      {/* <Modal
        className={`review-question-modal review-question-modal-custom`}
        size='lg'
        style={{
          overflow: 'hidden',
          background: 'rgba(39,44,54,0.3)'
        }}
        open={dataReview != null}
        onClose={() => setDataReview(null)}
        backdrop={false}
      >
        <Modal.Body 
          style={{ overflow: 'hidden' }}
        >
          <div className='modal-header'>
            {' '}
            Xem trước câu hỏi
            <Button className="option-btn" onClick={() => setDataReview(null)}>
                <img src="/images/icons/ic-close-purple.png" alt="close" />
            </Button>
          </div>
          <div className="modal-sub-content">
            <ActivityPicker data={dataReview} type={dataReview?.question_type} />
          </div>
          
        </Modal.Body>
      </Modal> */}
    </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context
  return { props: { idQues: query.questionSlug } }
}

const questionReading = ['MC5', 'FB6', 'TF2', 'MG1', 'LS1', 'MIX1', 'MIX2']

const questionImage = ['MC2', 'FB4', 'FB5', 'FB7', 'FB8', 'SL2', 'DL1', 'DL2', 'DL3', 'TF3', 'MG4', 'SO1', 'FC1', 'FC2']

const questionAudio = ['MC6', 'MC7', 'MC10', 'MC11', 'FB3', 'FB4', 'FB5', 'SL2', 'DL1', 'DL2', 'TF1',  'TF5', 'MG3', 'MR2', 'MR3', 'LS2', 'SO1', 'DS1', 'FC1']
