import { useSession } from 'next-auth/client';

import DashboardTeacher from '@/componentsV2/Dashboard/Teacher';
import useTranslation from '@/hooks/useTranslation';
import { USER_ROLES } from '@/interfaces/constants';
import DashboardMain from 'components-v2/Dashboard/Main';
import { Wrapper } from 'components/templates/wrapper';

export default function DashboardPage() {
    const {t} = useTranslation()
    const [session] = useSession()
    const user: any = session?.user || null

    const isAdmin = user?.is_admin === 1
    const userRole = user?.user_role_id

    if (userRole === USER_ROLES.Teacher) {
        return (
            <Wrapper hasStickyFooter={false} pageTitle={t("overview")}>
                <DashboardTeacher />
            </Wrapper>
        );
    }

    if (isAdmin) {
        return (
            <Wrapper hasStickyFooter={false} pageTitle={t("overview")}>
                <DashboardMain />
            </Wrapper>
        );
    }

    return null
}