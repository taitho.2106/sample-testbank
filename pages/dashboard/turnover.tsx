import useTranslation from '@/hooks/useTranslation';
import TurnoverDetail from 'components-v2/Dashboard/Main/TurnoverDetail';
import { Wrapper } from 'components/templates/wrapper';

export default function DashboardPage() {
    const { t } = useTranslation()
    return (
        <Wrapper hasStickyFooter={false} pageTitle={t("dashboard")["turnover-report"]}>
            <TurnoverDetail />
        </Wrapper>
    );
}