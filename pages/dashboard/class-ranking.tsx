import useTranslation from '@/hooks/useTranslation';
import ClassRankingDetail from 'components-v2/Dashboard/Teacher/ClassRankingDetail';
import { Wrapper } from 'components/templates/wrapper';

export default function DashboardPage() {
    const { t } = useTranslation()
    return (
        <Wrapper hasStickyFooter={false} pageTitle={t("overview")}>
            <ClassRankingDetail />
        </Wrapper>
    );
}