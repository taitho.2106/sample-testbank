import useTranslation from '@/hooks/useTranslation';
import UserDetail from 'components-v2/Dashboard/Main/UserDetail';
import { Wrapper } from 'components/templates/wrapper';

export default function DashboardPage() {
    const { t } = useTranslation()
    return (
        <Wrapper hasStickyFooter={false} pageTitle={t("dashboard")["user-report"]}>
            <UserDetail />
        </Wrapper>
    );
}