import useTranslation from '@/hooks/useTranslation';
import ServicePackageDetail from 'components-v2/Dashboard/Main/ServicePackageDetail';
import { Wrapper } from 'components/templates/wrapper';

export default function DashboardPage() {
    const { t } = useTranslation()
    return (
        <Wrapper hasStickyFooter={false} pageTitle={t("dashboard")["package-report"]}>
            <ServicePackageDetail />
        </Wrapper>
    );
}