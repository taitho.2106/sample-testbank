import { useEffect, useState } from 'react'

import { useRouter } from 'next/dist/client/router'
import { Collapse } from 'react-collapse'
import { Button, Pagination, Tooltip, Whisper } from 'rsuite'

import { callApi } from 'api/utils'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { MultiSelectPicker } from 'components/atoms/selectPicker/multiSelectPicker'
import useGrade from 'hooks/useGrade'
import useSeries from 'hooks/useSeries'
import useThirdParty from 'hooks/useThirdParty'

import { BreadCrumbNav } from '../../components/molecules/breadCrumbNav'
import { IntegrateExamTemplate } from '../../components/organisms/integrateExamTemplate'
import { StickyFooter } from '../../components/organisms/stickyFooter'
import { Wrapper } from '../../components/templates/wrapper'
import { BreadcrumbItemType } from '../../interfaces/types'
import ChatHelp from 'components/chatHelp'
import { USER_GUIDE_SCREEN_ID } from '@/interfaces/constants'
import { SelectFilterPicker } from 'components/atoms/selectFilterPicker'
import { filterSeriesByGrade } from 'utils/series/filterSeriesByGrade'

function IntegrateExam() {
  const router = useRouter()
  const { getGrade } = useGrade()
  const { getSeries } = useSeries()
  const { getThirdParty } = useThirdParty()

  const [integrateExamList, setIntegrateExamList] = useState([] as any[])
  const [currentPage, setCurrentPage] = useState(1)
  const [totalPage, setTotalPage] = useState(1)

  const [isOpenFilterCollapse, setIsOpenFilterCollapse] = useState(false)
  const [triggerReset, setTriggerReset] = useState(false)

  const [filterName, setFilterName] = useState('')
  const [filterThirdParty, setFilterThirdParty] = useState([] as any[])
  const [filterSeries, setFilterSeries] = useState([] as any[])
  const [filterGrade, setFilterGrade] = useState([] as any[])

  const [gradeId, setGradeId] = useState(null)
  const [listSeries, setListSeries] = useState([])

  const [isFilter, setIsFilter] = useState(false)
  const [isLoading, setIsLoading] = useState(true)

  const filterBagde = [filterSeries.length > 0, filterThirdParty.length > 0, filterGrade.length > 0].filter(
    (item) => item === true,
  ).length
  const userGuideScreenId = USER_GUIDE_SCREEN_ID.Integration_View

  useEffect(() => {
    const fetchData = async () => {
      const response: any = await callApi(`/api/integrate-exam/unit-test/search`, 'post', 'Token')
      if (response) {
        setIntegrateExamList([...response.data])
        setCurrentPage(1)
        setTotalPage(response.totalPages)
        setIsLoading(false)
      }
      setIsLoading(false)
    }
    fetchData()
  }, [router.asPath])
  useEffect(() => {
    const list = filterSeriesByGrade(getSeries, getGrade, gradeId)
    setListSeries([...list])
  }, [getSeries,gradeId])
  const checkExistFilter = () =>
    (filterName || filterGrade.length > 0 || filterSeries.length > 0 || filterThirdParty.length > 0) && isFilter
      ? true
      : false

  const collectData = (isReset: boolean, page = 0, body: any = null) => {
    const collection: any = { page }
    if (!isReset) {
      if (filterName || body?.name) collection['name'] = body?.name || filterName
      if ((filterGrade && filterGrade.length > 0) || (body?.templateLevelId && body.templateLevelId.length > 0))
        collection['template_level_id'] = body?.templateLevelId || filterGrade

      if ((filterSeries && filterSeries.length > 0) || (body?.series && body.series.length > 0))
        collection['series_id'] = body?.series || filterSeries
      if ((filterThirdParty && filterThirdParty.length > 0) || (body?.integrate && body.integrate.length > 0))
        collection['third_party_code'] = body?.integrate || filterThirdParty
    }
    return collection
  }

  const handleNameChange = (val: string) => setFilterName(val)
  const handleNameSubmit = (e: any, val: string) => {
    if (e.keyCode === 13) {
      handleFilterSubmit(false, { name: val })
      e.target.blur()
    }
  }

  const handleSeriesChange = (val: any) => {
    let value: any[] = []
    if (val) {
      value = [val]
    }
    setFilterSeries([...value])
    handleFilterSubmit(false, { series: [...value] })
  }
  const handleIntegrateChange = (val: any[]) => {
    setFilterThirdParty([...val])
    handleFilterSubmit(false, { integrate: [...val] })
  }
  const handleGradeChange = (val: any) => {
    setGradeId(val)
    const listGrade: any[] = []
    if (val) {
      setFilterGrade([val])
    }
    handleFilterSubmit(false, { templateLevelId: listGrade })
  }

  const handleItemDelete = async (id: number) => {
    const body = collectData(false, currentPage - 1)
    setIsFilter(true)
    const response: any = await callApi(`/api/unit-test-third-party/${id}`, 'delete')
    if (response?.data?.affectedRows === 1) {
      const response1: any = await callApi(`/api/integrate-exam/unit-test/search`, 'post', 'Token', {
        ...body,
      })
      if (response1) {
        if (response1?.data) setIntegrateExamList([...response1.data])
        if (response1?.totalPages && response1.totalPages < totalPage && response1.totalPage > 0)
          setCurrentPage(currentPage - 1)
        setTotalPage(response1?.totalPages)
      }
    }
  }

  const handleFilterSubmit = async (isReset: boolean, customData: any = null) => {
    const body = collectData(isReset, 0, customData)
    setIsFilter(!isReset)
    const response: any = await callApi(`/api/integrate-exam/unit-test/search`, 'post', 'Token', {
      ...body,
    })
    if (response) {
      if (response?.data) setIntegrateExamList([...response.data])
      if (response?.currentPage) setCurrentPage(response.currentPage + 1 > 0 ? response.currentPage + 1 : 1)
      if (response?.totalPages) setTotalPage(response.totalPages)
    }
  }

  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
      /* you can also use 'auto' behaviour
             in place of 'smooth' */
    })
  }
  const handlePageChange = async (page: number) => {
    const body = collectData(false, page - 1)
    setIsFilter(true)
    scrollToTop()
    const response: any = await callApi(`/api/integrate-exam/unit-test/search`, 'post', 'Token', {
      ...body,
    })
    if (response) {
      if (response?.data) setIntegrateExamList([...response.data])
      if (response?.currentPage || response.currentPage === 0) setCurrentPage(response.currentPage + 1)
      if (response?.totalPages) setTotalPage(response.totalPages)
    }
  }

  const resetFilterState = () => {
    if (filterGrade.length > 0) setFilterGrade([])
    if (filterSeries.length > 0) setFilterSeries([])
    if (filterThirdParty.length > 0) setFilterThirdParty([])
    setCurrentPage(1)
  }

  const breadcrumbData: BreadcrumbItemType[] = [
    { name: 'Tổng quan', url: '/' },
    { name: 'Đề thi tích hợp với đối tác', url: null },
  ]

  return (
    <>
      <Wrapper pageTitle="Đề thi dùng cho tích hợp" className="p-integrate-exam__container">
        <div className="p-detail-format__breadcrumb" style={{ fontFamily: 'Montserrat Regular' }}>
          <BreadCrumbNav data={breadcrumbData} />
        </div>

        <div className="m-unittest-template-table-data">
          <div className="m-unittest-template-table-data__content">
            <div className="content__sup">
              <div className="__input-group">
                <InputWithLabel
                  className="__search-box"
                  label="Tên đề thi"
                  placeholder="Tìm kiếm đề thi"
                  triggerReset={triggerReset}
                  type="text"
                  onBlur={handleNameChange}
                  onKeyUp={handleNameSubmit}
                  icon={<img src="/images/icons/ic-search-dark.png" alt="search" />}
                />
                <Whisper
                  placement="bottom"
                  trigger="hover"
                  speaker={<Tooltip>Nhấn để chọn lọc dữ liệu theo nhiều điều kiện</Tooltip>}
                >
                  <Button
                    className="__sub-btn"
                    data-active={isOpenFilterCollapse}
                    onClick={() => setIsOpenFilterCollapse(!isOpenFilterCollapse)}
                    style={{ marginLeft: '0.8rem' }}
                  >
                    <div className="__badge" data-value={filterBagde} data-hidden={filterBagde <= 0}>
                      <img src="/images/icons/ic-filter-dark.png" alt="filter" />
                    </div>
                    <span>Lọc</span>
                  </Button>
                </Whisper>
                <Whisper
                  placement="bottom"
                  trigger="hover"
                  speaker={<Tooltip>Nhấn để xóa bỏ bộ lọc đang dùng</Tooltip>}
                >
                  <Button
                    className="__sub-btn"
                    onClick={() => {
                      setTriggerReset(!triggerReset)
                      resetFilterState()
                      handleFilterSubmit(true)
                    }}
                  >
                    <img src="/images/icons/ic-reset-darker.png" alt="reset" />
                    <span>Mặc định</span>
                  </Button>
                </Whisper>
                <Button
                  className="__main-btn"
                  onClick={() => {
                    handleFilterSubmit(false)
                    setCurrentPage(1)
                  }}
                >
                  <img src="/images/icons/ic-search-dark.png" alt="find" />
                  <span>Tìm kiếm</span>
                </Button>
              </div>
            </div>
            <Collapse isOpened={isOpenFilterCollapse}>
              <div className="content__sub">
                {/* {getGrade.length > 0 && (<MultiSelectPicker
                                    className="__filter-box"
                                    data={getGrade}
                                    label="Khối lớp"
                                    triggerReset={triggerReset}
                                    onChange={handleGradeChange}
                                />)} */}
                {getGrade.length > 0 && (
                  <SelectFilterPicker
                    data={getGrade}
                    isMultiChoice={false}
                    inputSearchShow={false}
                    isShowSelectAll={false}
                    className="__filter-box"
                    label="Khối lớp"
                    onChange={handleGradeChange}
                    style={{ zIndex: 6 }}
                    triggerReset={triggerReset}
                  />
                )}
                {getSeries.length > 0 && (
                  <SelectFilterPicker
                    data={listSeries}
                    isMultiChoice={false}
                    inputSearchShow={true}
                    onChange={handleSeriesChange}
                    className="__filter-box"
                    label="Chương trình"
                    triggerReset={triggerReset}
                    menuSize='xxl'
                  />
                )}
                {getThirdParty.length > 0 && (
                  <MultiSelectPicker
                    className="__filter-box"
                    data={getThirdParty}
                    label="Đối tác"
                    triggerReset={triggerReset}
                    onChange={handleIntegrateChange}
                  />
                )}
              </div>
            </Collapse>
          </div>
          {integrateExamList.length > 0 || isLoading ? (
            <IntegrateExamTemplate
              currentPage={currentPage}
              data={integrateExamList}
              isLoading={isLoading}
              setData={setIntegrateExamList}
              onDelete={handleItemDelete}
            />
          ) : (
            <div className="o-question-drawer__empty" style={{ minHeight: '50vh', paddingTop: 32, paddingBottom: 32 }}>
              <img
                className="__banner"
                src={
                  checkExistFilter()
                    ? '/images/collections/clt-emty-result.png'
                    : '/images/collections/clt-emty-template.png'
                }
                alt="banner"
              />
              <p className="__description">
                {checkExistFilter() ? (
                  <>Không có kết quả nào được tìm thấy.</>
                ) : (
                  <>
                    Chưa có đề thi nào trong danh sách.
                    <br />
                    Vui lòng tạo mới để quản lý
                  </>
                )}
              </p>
            </div>
          )}
          {integrateExamList.length > 0 && (
            <StickyFooter>
              <div className="__pagination">
                <Pagination
                  prev
                  next
                  ellipsis
                  size="md"
                  total={totalPage * 10}
                  maxButtons={10}
                  limit={10}
                  activePage={currentPage}
                  onChangePage={(page: number) => handlePageChange(page)}
                />
              </div>
            </StickyFooter>
          )}
        </div>
      </Wrapper>
      <ChatHelp screenId={userGuideScreenId}></ChatHelp>
    </>
  )
}

export default IntegrateExam
