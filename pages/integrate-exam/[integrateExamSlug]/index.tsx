import { useEffect, useState } from 'react'

import { callApi } from 'api/utils'
import useGrade from 'hooks/useGrade'
import useSeries from 'hooks/useSeries'
import useThirdParty from 'hooks/useThirdParty'

import { BreadCrumbNav } from '../../../components/molecules/breadCrumbNav'
import IntegrateExamContainer from '../../../components/organisms/integrateExamContainer'
import { Wrapper } from '../../../components/templates/wrapper'
import { BreadcrumbItemType } from '../../../interfaces/types'
import ChatHelp from 'components/chatHelp'
import { USER_GUIDE_SCREEN_ID } from '@/interfaces/constants'

function IntegrateExamSlug({ id, viewMode }: any) {
  //viewmode : edit or create
  const arrViewMode = ['edit', 'create']

  const [unitTest, setUnitTest] = useState(null)
  const [loading, setLoading] = useState(false)
  const { getSeries } = useSeries()
  const { getThirdParty } = useThirdParty()
  const { getGrade } = useGrade()

  if (!arrViewMode.find((m) => m == viewMode)) {
    return <></>
  }

  const breadcrumbData: BreadcrumbItemType[] = [
    { name: 'Đề thi tích hợp với đối tác', url: '/integrate-exam' },
    {
      name:
        viewMode === 'create'
          ? 'Tạo đề thi tích hợp'
          : 'Cập nhật đề thi tích hợp',
      url: null,
    },
  ]
  const userGuideScreenId =
    viewMode == 'create'
      ? USER_GUIDE_SCREEN_ID.Integration_Create
      : USER_GUIDE_SCREEN_ID.Integration_Update
  useEffect(() => {
    const fetchData = async () => {
      setLoading(true)
      if (viewMode === 'create') {
        const response: any = await callApi(
          `/api/unit-test/${id}?mode=${viewMode === 'edit' ? 'edit' : 'view'}`,
          'GET',
        )
        if (response) {
          setUnitTest({ ...response?.data })
          setLoading(false)
        }
      } else {
        const response: any = await callApi(
          `/api/unit-test-third-party/${id}`,
          'GET',
        )
        if (response) {
          setUnitTest(response?.data)
          setLoading(false)
        }
      }
    }
    fetchData()
  }, [viewMode])

  const updateSuccess = async () => {
    const response: any = await callApi(
      `/api/unit-test-third-party/${id}`,
      'GET',
    )
    if (response) {
      setUnitTest(response?.data)
      setLoading(false)
    }
  }
  return (
    <>
      {!loading &&
        unitTest &&
        getGrade.length > 0 &&
        getSeries.length > 0 &&
        getThirdParty.length > 0 && (
          <Wrapper
            pageTitle={unitTest?.name}
            className="p-integrate-exam__container"
          >
            <div
              className="p-detail-format__breadcrumb"
              style={{ fontFamily: 'Montserrat Regular' }}
            >
              <BreadCrumbNav data={breadcrumbData} />
            </div>
            {
              <IntegrateExamContainer
                updateSuccess={updateSuccess}
                viewMode={viewMode}
                data={unitTest}
                getThirdParty={getThirdParty}
                getSeries={getSeries}
                getGrade={getGrade}
              />
            }
          </Wrapper>
        )}
      <ChatHelp screenId={userGuideScreenId}></ChatHelp>
    </>
  )
}

export default IntegrateExamSlug
export async function getServerSideProps(context: any) {
  const id = context.query.integrateExamSlug
  const viewMode = context.query.mode
  return {
    props: { id, viewMode },
  }
}
