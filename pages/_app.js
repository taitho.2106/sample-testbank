import { Provider } from 'next-auth/client';
import Head from 'next/head';
import React, { useEffect } from 'react';

import ModalShowNotiForStudent from "../components-v2/App/ModalShowNotiForStudent";
import usePageTracking from '../hooks/usePageTracking';
import { fnBrowserDetect, isIOS } from '../utils/log';

import ModalStudentGift from '../components-v2/App/ModalStudentGift';
import UserPackage from '../components-v2/App/UserPackage';
import FullScreenLoading from "../components-v2/Common/FullScreenLoading";
import AppProvider from '../components-v2/Providers/AppProvider';
import RouteGuard from '../components-v2/RouteGuard';

import "@/assets/scss/index.scss";
import { useRouter } from "next/router";
import 'rsuite/dist/rsuite.min.css';
import useTranslation from '../hooks/useTranslation';
import { paths } from "../interfaces/constants";
import '../styles/app.scss';

export default function App({ Component, pageProps }) {
    const {t} = useTranslation()
    usePageTracking();
    const { pathname, isReady } = useRouter();
    useEffect(async () => {
        const check = isIOS()
        const browser = fnBrowserDetect()
        document.body.setAttribute('data-ios', check)
        document.body.setAttribute('data-browser', browser)
        
    }, [])
    
    useEffect(() => {
        if (isReady) {
            const bodyElement = document.body
            const listPathnameShowLiveChat = [paths.home, paths.payment, paths.aboutUs, paths.shoppingGuide, paths.paymentPolicy, paths.paymentResults, paths.feedbackPolicy, paths.policySecurity]
            const setPathname = new Set(listPathnameShowLiveChat)
            if (setPathname.has(pathname)) {
                document.body.classList.remove("hide-cs-chat");
            } else {
                if (!bodyElement.classList.contains("hide-cs-chat")) {
                    document.body.classList.add("hide-cs-chat");
                }
            }
        }
    }, [isReady, pathname])

    return (
        <Provider
            options={{
                clientMaxAge: 0,
                keepAlive: 0,
            }}
            session={pageProps.session}
        >
            <Head>
                <title>i-Test - {t('title')}</title>
                <meta
                    name="viewport"
                    content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0"
                />
            </Head>
            <AppProvider>
                <FullScreenLoading />
                <UserPackage />
                <ModalShowNotiForStudent />
                <ModalStudentGift />
                <RouteGuard><Component {...pageProps} /></RouteGuard>
            </AppProvider>
        </Provider>
    )
}
