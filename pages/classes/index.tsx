import React from 'react'

import ClassList from '@/componentsV2/ClassesPage/ClassList'
import { Wrapper } from 'components/templates/wrapper'
import useTranslation from "@/hooks/useTranslation";

export default function ClassesManagement() {
  const { t } = useTranslation();
  const pageTitle = t('classes')['list-of-classes']
  return (
    <Wrapper
      pageTitle={pageTitle}
      hasStickyFooter={true}
    >
      <ClassList/>
    </Wrapper>
  )
}
