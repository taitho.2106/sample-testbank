import React, { useEffect, useState } from 'react'

import { GetServerSideProps } from 'next'
import { useRouter } from 'next/router'

import StudentsContainer from '@/componentsV2/StudentsPages'
import AddNewStudent from '@/componentsV2/StudentsPages/AddNewStudent'
import ChoosenStudents from '@/componentsV2/StudentsPages/ChoosenStudents'
import { ClassesContext } from '@/interfaces/contexts'
import { callApiConfigError } from 'api/utils'
import { Wrapper } from 'components/templates/wrapper'
import { Loader } from "rsuite";

export default function ListStudents({query}:any) {
    const {push} = useRouter()
    const [dataClasses, setDataClasses] = useState(null)
    const [isLoading, setIsLoading] = useState(true)
    const idClass = query.idClass || query.classeSlug
    const fetcher = async () => {
        try {
            const response: any = await callApiConfigError(`/api/classes/${idClass}`)
            setIsLoading(false)
            if(response.result){
                if (response.data){
                    setDataClasses(response.data)
                }else{
                    push('/classes')
                }
            }
        }catch (e) {
            setIsLoading(false)
            push('/classes')
        }

    }
    useEffect(() => {
      if (!isNaN(Number(idClass))) {
        fetcher()
      }
    }, [idClass])
    const renderUIWithSlug = () => {
        switch (query.classeSlug) {
          case 'choosen-students':
            return <ChoosenStudents />
          case 'add-new-student':
            return <AddNewStudent />
          default:
            return <StudentsContainer />
        }
    }
    if(dataClasses !== null && !isLoading){
      return (
        <Wrapper
          className="new-styles-css"
          pageTitle={dataClasses?.name}
          subTitle={`(${dataClasses?.start_year} - ${dataClasses?.end_year})`}
        >
          <ClassesContext.Provider
            value={{
              dataClasses,
            }}
          >
            {renderUIWithSlug()}
          </ClassesContext.Provider>
        </Wrapper>
      )
    }
    return (
        <Wrapper
            className="new-styles-css"
        >
            <Loader backdrop vertical/>
        </Wrapper>
    )
}
export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context

  return { props: { query } }
}