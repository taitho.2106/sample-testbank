import { useEffect } from 'react'

import Cookies from 'js-cookie'
import { Session } from 'next-auth'
import { getSession, signIn, signOut } from 'next-auth/client'
import { useRouter } from 'next/dist/client/router'
import Loader from 'rsuite/Loader'

import { paths } from 'api/paths'

import { cryptoDecrypt } from '../../utils/crypto'
import api from 'lib/api'

const cookieKey = '_extension_source_' + process.env.NEXT_PUBLIC_APP_ID

export default function Eduhome({ data }: any) {
  console.debug('Eduhome', data)
  const router = useRouter()
  useEffect(() => {
    // const data1 = "SCsxHXpvNgf5u9Gow8Z5+v68Spn1algCdotiZZPgGcHDYUDah6UvIYE+SLnJDh26Ny9i0RSAmgUHLEnZvzrzvZ3ue1Q1BoMWwsIke4UDT4MBIsJtx31Ol235nqZgfoXj8r21T+brxGKnjvpaY+35CpeUVeVmMVOxyaYE+PXAUnEKcG1eNfcEyFB3VHe5I6GDufyI685CoHYgxiRdMqzz5gljgf0gKMWK8abnbMBkst+iWxwC9KajTLbUol+s1gAlEI12VwjB5sk="
    // Cookies.set('_extension_source_' + process.env.NEXT_PUBLIC_APP_ID, data1)
    // Cookies.remove(cookieKey)
    console.debug('client_date_now', new Date())
    if (data) {
      if (data.mode == ModeVerify.Success) {
        const login = async () => {
          const sessionCurrent : Session = await getSession()
          //check has user login
          if(sessionCurrent){
           const resCurrent = await signOut({callbackUrl:"/", redirect:false})
            console.debug("logout---",resCurrent)
          }
          const result = await signIn('credentials', {
            redirect: false,
            email: data.phone,
            eduhome: true,
          })
          console.debug('result-----------', result)
          if (result.ok) {
            if (!result.error) {
              const extendParams = data.extend_params
              let session: Session = await getSession()
              console.debug('session1-----------', session)
              if (session && session.user) {
                const user: any = session.user
                checkUnitTestAndRedirect(user,extendParams?.IdGrade, extendParams?.IdSeries)
                return
              }
              // alert('session null')
              let count = 0
              const interval = setInterval(async () => {
                count++
                session = await getSession()
                console.debug('session2-----------', session)
                if (session && session.user) {
                  const user: any = session.user
                  console.debug('count-----', count)
                  clearInterval(interval)
                  checkUnitTestAndRedirect(user,extendParams?.IdGrade, extendParams?.IdSeries)
                  return;
                } else if (count > 5) {
                  clearInterval(interval)
                  router.reload()
                }
              }, 100)
            }
          }
        }
        login()
      } else {
        if (data.mode == ModeVerify.Expired) {
          router.push('/expiration')
          return
        }
        router.push('/')
        return
      }
    } else {
      console.log('data-null')
    }
  }, [data])
  const checkUnitTestAndRedirect = async(user:any,gradeId:number, seriesId:number)=>{
    let urlRedirect = user.redirect || 'unit-test';
    if(gradeId && seriesId){
      const res:any = await api.get(`/api/unit-test/eduhome/check-by-grade-series?g=${gradeId}&s=${seriesId}`)
          const gradeRes = res.data?.data?.grade||null
          const seriesRes = res.data?.data?.series||0
          console.log("res================",res?.data)
          if(gradeRes){
            urlRedirect =  `${urlRedirect}?g=${gradeRes}`
            urlRedirect =  `${urlRedirect}&s=${seriesRes}`
          }
    }
    console.log("urlRedirect===",urlRedirect)
    router.replace(urlRedirect)
  }
  return <Loader size="lg" backdrop content="Loading..." vertical />
}

type PropsType = {
  req: any
}
Eduhome.getInitialProps = async ({ req }: PropsType) => {
  console.log('init')
  //  process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  // await delay(3000);
  console.debug(cookieKey)
  const eduCookie = req.cookies[cookieKey]
  // console.log("eduCookie=====",eduCookie)
  const parseCookie = eduCookie ? cryptoDecrypt(eduCookie) : null
  console.debug('data=====', parseCookie)
  if (parseCookie) {
    if (parseCookie?.ExpireTime && parseCookie?.Mode) {
      const startDate = new Date(
        parseInt(
          parseCookie?.StartTime.replace('/Date(', '').replace(')/', ''),
        ),
      )
      const endDate = new Date(
        parseInt(
          parseCookie?.ExpireTime.replace('/Date(', '').replace(')/', ''),
        ),
      )
      // startDate.setTime(startDate.getTime() - 7*60*60*1000);
      // endDate.setTime(endDate.getTime() - 7*60*60*1000);

      const endTime = endDate.getTime()
      const startTime = startDate.getTime()
      const dataBody = {
        phone: parseCookie.phone ?? '',
        start_date: startTime ? startTime : 0,
        end_date: endTime ? endTime : 0,
        mode: parseCookie.Mode.toLowerCase(),
      }
      dataBody.phone = dataBody.phone.trim()
      if (dataBody.phone.length == 0) {
        return {
          data: {
            mode: ModeVerify.Error,
          },
        }
      }
      const dataSend = JSON.stringify(dataBody)
      const res = await fetch(
        process.env.NEXT_PUBLIC_WEB_URL +
          paths.api_eduhome_user,
        {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: dataSend,
        },
      )
      //ExtendParams:  { IdGrade: 0, IdSeries: 0 }
      const extendParams = parseCookie.ExtendParams;

      const json = await res.json()
      if (!res.ok) {
        return {
          data: {
            mode: ModeVerify.Error,
          },
        }
      }
      return {
        data: {
          mode: ModeVerify.Success,
          phone: dataBody.phone,
          extend_params:extendParams
        },
      }
    }
  }
  return {
    data: {
      mode: ModeVerify.Error,
    },
  }
}
function delay(delayInms: number) {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(2)
    }, delayInms)
  })
}
const ModeVerify = {
  Success: 1,
  Error: 2,
  Expired: 3,
}
