import { GetServerSideProps } from 'next'
import { getCsrfToken, getSession } from 'next-auth/client'

import ChatHelp from 'components/chatHelp'
import { Login } from 'components/pages/login'
import SignIn from "../../components-v2/Authentication/SignIn"
import { LoginContext } from 'interfaces/contexts'

import {USER_GUIDE_SCREEN_ID} from'../../interfaces/constants'
type PropsType = {
  csrfToken: string
}

const LoginPage = ({ csrfToken }: PropsType) => {
  return (
    <>
      <LoginContext.Provider value={{ csrfToken }}>
        <SignIn/>
      </LoginContext.Provider>
      <ChatHelp screenId={USER_GUIDE_SCREEN_ID.Login}/>
    </>
  
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  //process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
  const session = await getSession(context)
  const csrfToken = await getCsrfToken(context)
  if (session) {
    const user: any = session.user
    return {
      redirect: {
        destination: user.redirect ?? '/questions',
        permanent: false,
      },
    }
  }

  return { props: { csrfToken: csrfToken || null } }
}

export default LoginPage
