import React from 'react'

import classNames from 'classnames'
import { GetServerSideProps } from 'next'

import StudentSlugContainer from '@/componentsV2/StudentsPages/StudentTeacher/StudentSlugContainer'
import { Wrapper } from 'components/templates/wrapper'
import useTranslation from "@/hooks/useTranslation";

export default function PageStudentSlug({ query }:any) {
    const { t } = useTranslation();
    return (
      <Wrapper
        className={classNames('new-styles-css')}
        pageTitle={
          query.studentSlug == 'details'
            ? t('student-management')['title']
            : query.studentSlug == 'info' ? t('student-info')['title'] : t('add-new-student')['title']
        }
      >
        <StudentSlugContainer query={query} />
      </Wrapper>
    )
}
export const getServerSideProps: GetServerSideProps = async (context) => {
    const { query } = context

    return { props: { query } }
}