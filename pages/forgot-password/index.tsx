import { GetServerSideProps } from 'next'
import { getCsrfToken, getSession } from 'next-auth/client'

import ChatHelp from 'components/chatHelp'
// import { FogotPassword } from 'components/pages/forgot-password'
import { USER_GUIDE_SCREEN_ID } from 'interfaces/constants'
import { LoginContext } from 'interfaces/contexts'
import ForgotPassword from "../../components-v2/Authentication/ForgotPassword"

type PropsType = {
  csrfToken: string
}

const ForgotPasswordPage = ({ csrfToken }: PropsType) => {
  return (
    <>
    <LoginContext.Provider value={{ csrfToken }}>
      <ForgotPassword />
    </LoginContext.Provider>
     <ChatHelp  screenId={USER_GUIDE_SCREEN_ID.ForgotPassWord}></ChatHelp>
     </>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context)
  const csrfToken = await getCsrfToken(context)

  if (session) {
    const user: any = session.user
    return {
      redirect: {
        destination: user.redirect ?? '/questions',
        permanent: false,
      },
    }
  }

  return { props: { csrfToken: csrfToken || null } }
}

export default ForgotPasswordPage
