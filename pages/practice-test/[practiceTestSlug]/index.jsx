import { useEffect, useState } from 'react'

import Cookies from 'js-cookie'
import { useRouter } from 'next/router'

import { paths } from '../../../api/paths'

import { practiceTestDataTransform } from '../../../api/dataTransform'
import api from '../../../lib/api'
import { questionListTransform } from '../../../practiceTest/api/dataTransform'
import { DingSound } from '../../../practiceTest/components/molecules/dingSound'
import { GamePicker } from '../../../practiceTest/components/organisms/gamePicker'
import { OffLineScreen } from '../../../practiceTest/components/organisms/offlineScreen'
import { StartTestScreen } from '../../../practiceTest/components/organisms/startTestScreen'
import { SummaryBoard } from '../../../practiceTest/components/organisms/summaryBoard'
import { ResultWrapper } from '../../../practiceTest/components/templates/resultWrapper'
import { TestWrapper } from '../../../practiceTest/components/templates/testWrapper'
import { Wrapper } from '../../../practiceTest/components/templates/wrapper'
import { TEST_MODE } from '../../../practiceTest/interfaces/constants'
import { PageTestContext } from '../../../practiceTest/interfaces/contexts'
import { callApi } from '../../../practiceTest/utils/fetch'
import { HistoryBoard } from '../../../practiceTest/components/organisms/testHistoryBoard'
import { useSession } from "next-auth/client";
import useTranslation from "../../../hooks/useTranslation";

const cookieKeyCode = 'third_party_code'
const cookieKeyStart = '_extension_source_'
export default function Test({ query, third_party_code }) {
  const { t, locale } = useTranslation()
  //remove cookie
  // Cookies.remove(cookieKeyCode)
  const [session] = useSession()
  const router = useRouter()
  const user = session?.user

  const [currentGroupBtn, setCurrentGroupbtn] = useState(0)
  const [currentGroupBtn6, setCurrentGroupBtn6] = useState(0)
  const [currentPart, setCurrentPart] = useState(0)
  const [currentQuestion, setCurrentQuestion] = useState([0])
  const [mode, setMode] = useState(TEST_MODE.play) // check | play | result | review
  const [partData, setPartData] = useState([])
  const [progress, setProgress] = useState([])
  const [thirdPartyData, setThirdPartyData] = useState(null)
  // const [testName, setTestName] = useState('')
  // const [testTime, setTestTime] = useState('')
  const [isOpenSummaryBoard, setIsOpenSummaryBoard] = useState(false)

  //history
  const [isOpenHistoryBoard, setIsOpenHistoryBoard] = useState(true)
  const [dataHistoryBoard, setDataHistoryBoard] = useState(null)

  const [testInfo, setTestInfo] = useState({
    testName: '',
    testTime: '',
    gradeName: '',
  })
  const [studentInfo, setStudentInfo] = useState({
    name: '',
    class: '',
    email: '',
  })

  let isShowStartScreen = true

  //don't show form input student info
  if (third_party_code) {
    isShowStartScreen = false
  }
  const [showStartScreen, setShowStartScreen] = useState(isShowStartScreen)

  const [isOnline, setNetwork] = useState(true)
  
  const [ classUnitTestInfo, setClassUnitTestInfo ] = useState(null)

  const pageData = { id: 'test' }

  const handleRouteComplete = () => setMode(TEST_MODE.play)

  useEffect(() => {
    const fetchData = async () => {
      const queryParams = query?.practiceTestSlug
        ? query.practiceTestSlug.split('===') || []
        : []
      const id = queryParams[1] || null
      const teacherId = queryParams[0] || null
      let resultId = queryParams[2] || null
      if (!id) return

      console.log('teacherid', teacherId)
      //get data third_party.
      if (third_party_code && !teacherId) {
        const childAppIdRes = await api.post(
          paths.api_child_app_id_integration,
          { third_party_code: third_party_code, unit_test_id: id },
        )

        if (childAppIdRes.status == 200) {
          const childAppId = childAppIdRes.data.data
          const cookieKeyApp = `${cookieKeyStart}${childAppId}`
          const thirdPartyCookieValue = Cookies.get(cookieKeyApp)
          // remove cookie
          // Cookies.remove(cookieKeyApp)
          if (!thirdPartyCookieValue) {
            console.log('home--1')
            router.push('/')
            return
          }

          //get value parse
          const thirdPartyCookieParseRes = await api.post(
            paths.api_data_integration,
            {
              third_party_code: third_party_code,
              unit_test_id: id,
              value: thirdPartyCookieValue,
            },
          )
          if (
            thirdPartyCookieParseRes.status == 200 &&
            thirdPartyCookieParseRes?.data.code == 1
          ) {
            const thirdPartyCookieParse = thirdPartyCookieParseRes.data.data
            // console.log("thirdPartyCookieParse-----", thirdPartyCookieParse)
            // Khác với mã trong config.
            if (childAppId != thirdPartyCookieParse.AppId) {
              console.log('home--2')
              router.push('/')
              return
            }
            setThirdPartyData({
              info: thirdPartyCookieParse,
              code: third_party_code,
            })
          } else {
            console.log('home--3')
            router.push('/')
            return
          }
        } else {
          console.log('home--4')
          router.push('/')
          return
        }
      } else {
        if (!teacherId) {
          console.log('home--5')
          router.push('/')
          return
        }
        //show form input student info
        setShowStartScreen(true)
      }
      const response = await callApi(`/api/unit-question/${id}`, 'get')
      if (!response) {
        router.push('/_error')
        return;
      }
      const skillListData = response?.sections
        ? response.sections.map(practiceTestDataTransform)
        : []
      // setTestName(response?.name || '')
      // setTestTime(response?.time)
      setTestInfo({
        ...response,
        testName: response?.name || '',
        testTime: response?.time || 0,
        gradeName: response?.template_level_id || '',
        id: response?.id || 0,
      })
      setPartData([...skillListData])

      //get data resultfrom db
      let dataResult = null;
      if(resultId){
        const dataResultRes = await api.get(
          `${paths.api_unit_test_result}?id=${resultId}&unit_test_id=${id}`
        )
        if(dataResultRes && dataResultRes.status == 200 && dataResultRes.data?.code==1 ){
          dataResult = dataResultRes.data.data;
          // console.log(dataResult)
        }else{
          resultId = null;
        }
      }
      const arrUnitTestSectionId = []
      const tq = skillListData.map((item, i) =>
        {
         arrUnitTestSectionId.push(item.id)
         return questionListTransform(skillListData, i)
        },
      )
      // console.log("skillListData======",skillListData)
      // console.log("tq======",tq)
     
      if(resultId){  //mapdata result
        setDataHistoryBoard(dataResult);
        setMode(TEST_MODE.review);
        setShowStartScreen(false)
        setProgress(
          tq.map((questions, index) =>
            {
              const unitTestSectionId = arrUnitTestSectionId[index];
              const sectionQuestionResult = dataResult.section_data.filter(m=> m.unit_test_section_id == unitTestSectionId)
              let currentQuestionId = 0;
              return questions.map((item) => {
                let  user_answer = {status:false, value:null};
                if(currentQuestionId != item.id){
                  const questionResult = sectionQuestionResult.find(m=> m.question_id == item.id);
                  user_answer = questionResult?JSON.parse(questionResult.user_answer):user_answer

                  //convert data DD1 old.
                  if(item.activityType == 6){ //DD1
                    if(!Array.isArray(user_answer.status)){
                      let value = user_answer.value;
                      if(value !== null && value !==undefined){
                        value = [value]
                      }
                      user_answer = {status: [user_answer.status], value:value}
                    }
                  }
                }
                currentQuestionId = item.id;
                return {
                  id: item.id,
                  group: item.group,
                  point: item.point,
                  question: item,
                  activityType: item.activityType,
                  skill: item.skill,
                  ...user_answer
                }
              })
            },
          ),
        )
      }else{
        setProgress(
          tq.map((questions) =>
            questions.map((item) => {
              return {
                id: item.id,
                group: item.group,
                point: item.point,
                skill: item.skill,
                question: item,
                activityType: item.activityType,
                status: false,
                value: null,
              }
            }),
          ),
        )
      }
    }

    if (window.navigator.onLine) {
      fetchData()
    } else {
      setNetwork(false)
    }
  }, [])

  useEffect(() => {
    router.events.on('routeChangeComplete', handleRouteComplete)
    return () => router.events.off('routeChangeComplete', handleRouteComplete)
  }, [])

  useEffect(() => {
    window.addEventListener('offline', updateNetwork)
    window.addEventListener('online', updateNetwork)
    return () => {
      window.removeEventListener('offline', updateNetwork)
      window.removeEventListener('online', updateNetwork)
    }
  }, [])

  const updateNetwork = () => {
    setNetwork(window.navigator.onLine)
  }

  if (partData.length <= 0 || progress.length <= 0) return <></>

  const onStart = (data) => {
    // setStudentInfo(data)
    setShowStartScreen(false)
  }
  return (
    <Wrapper isHavingHeader={false} isLoading={false} page={pageData}>
      <PageTestContext.Provider
        value={{
          currentGroupBtn: {
            index: currentGroupBtn,
            setIndex: setCurrentGroupbtn,
          },
          currentGroupBtn6: {
            index: currentGroupBtn6,
            setIndex: setCurrentGroupBtn6
          },
          currentPart: { index: currentPart, setIndex: setCurrentPart },
          currentQuestion: {
            index: currentQuestion,
            setIndex: setCurrentQuestion,
          },
          mode: { state: mode, setState: setMode },
          partData,
          progress: { state: progress, setState: setProgress},
          testInfo,
          studentInfo: studentInfo,
          isOpenSummaryBoard: {
            state: isOpenSummaryBoard,
            setState: setIsOpenSummaryBoard,
          },
          isOpenHistoryBoard: {
            state: isOpenHistoryBoard,
            setState: setIsOpenHistoryBoard,
          },
          //third_party_data
          pageMode:dataHistoryBoard?PageMode.ViewHistory:PageMode.DoingTest,
          thirdPartyData,
        }}
      >
        {isOnline ? (
          <>
            {showStartScreen ? (
              <StartTestScreen
                onStart={onStart}
                gradeName={testInfo?.gradeName ? testInfo?.gradeName : '---'}
                testName={testInfo?.testName}
                testTime={testInfo?.testTime}
                user={user}
                setClassUnitTestInfo={setClassUnitTestInfo}
              />
            ) : (
              <div>
                {[TEST_MODE.check, TEST_MODE.play, TEST_MODE.review].includes(
                  mode,
                ) && (
                  <TestWrapper>
                    <GamePicker />
                    <DingSound />
                  </TestWrapper>
                )}
                {mode === TEST_MODE.result && (
                  <ResultWrapper data={partData} classUnitTestInfo={classUnitTestInfo}/>
                )}
                {(mode === TEST_MODE.play || mode !== TEST_MODE.result) && (
                  <SummaryBoard />
                )}
                {[TEST_MODE.check, TEST_MODE.play, TEST_MODE.review].includes(
                  mode,
                ) && dataHistoryBoard && (
                  <HistoryBoard data={dataHistoryBoard}/>
                )}
              </div>
            )}
          </>
        ) : (
          <OffLineScreen />
        )}
      </PageTestContext.Provider>
    </Wrapper>
  )
}
const PageMode = {
  DoingTest:1,
  ViewHistory:2
}
export const getServerSideProps = async (context) => {
  const { query, req } = context
  const third_party_code = req.cookies[cookieKeyCode]
  if (third_party_code) {
    return { props: { query, third_party_code } }
  } else {
    return { props: { query } }
  }
}
