import { useEffect,useRef } from 'react'
import * as Excel from "exceljs";
import { CERF_SELECTIONS, LEVEL_SELECTIONS, SKILLS_SELECTIONS, TASK_SELECTIONS } from '@/interfaces/struct';
import { StructType } from '@/interfaces/types';
import api from 'lib/api';
import { cellArrayObject } from '@/questions_middleware/cellDefine';
const Home = () => {
  
    const fileImport:any = useRef(null)
    const onChangeReadFile=async()=>{
      if (fileImport.current.files.length > 0) {
        const file = fileImport.current.files[0]
        const data = new FormData()
        const question_type = (document.getElementById("question_type") as HTMLSelectElement).value
        const cefr = (document.getElementById("cefr") as HTMLSelectElement).value
        const skill = (document.getElementById("skill") as HTMLSelectElement).value
        const level = (document.getElementById("level") as HTMLSelectElement).value
        const key =(document.getElementById("key") as HTMLSelectElement).value
           data.append('file', file)
           data.append("question_type",question_type)
           data.append("skill",skill)
           data.append("cefr",cefr)
           data.append("level",level)
           data.append("key",key)
           const res =await api.post("/api/handle-pdf-question", data)
           if(res){
            const dataRes:any = res.data;
            if(res.status==200 ){
                if(dataRes.code==1){
                    exportSuccessResult(dataRes.data);
                }else{
                    alert(dataRes.message)
                }
                
            }else{
                alert(dataRes.message)
            }
           }
           
       }
    }
    const exportSuccessResult=async(uploadSuccessResult:any[])=>{
      const workbook = new Excel.Workbook();
      const worksheet = workbook.addWorksheet("My Sheet");

      const columns = []
      const lenColumns = cellArrayObject.length;
      for(let i=0; i<lenColumns;i++){
          const itemCell = cellArrayObject[i];
          columns.push({header: itemCell.display, key:itemCell.key, width:30})
      }
      worksheet.columns = columns
      for(let i=0; i<uploadSuccessResult.length;i++){
          const itemResult = uploadSuccessResult[i];
          worksheet.addRow(itemResult);
          for(let j=0; j<lenColumns;j++){
              const itemCell = cellArrayObject[j];
              const valueCell:any = itemResult[itemCell.key]
              const cell = worksheet.getCell(`${itemCell.code}${i+2}`); ///trừ header và bắt đầu từ 1
              cell.border = {
                  top: {style:'thin',color:{argb:'FF000000'}},
                  left: {style:'thin',color:{argb:'FF000000'}},
                  bottom: {style:'thin',color:{argb:'FF000000'}},
                  right: {style:'thin',color:{argb:'FF000000'}}
              }
              cell.font={
                  size: 12,
              }
              cell.alignment = {
                  vertical: 'top',
                  horizontal:'left',
                  wrapText: true,
              }
              if( valueCell && Array.isArray(valueCell) ==true){
                  const richText = [];
                  for(let index =0; index<valueCell.length;index++){
                      if(typeof valueCell[index] == "string"){
                          richText.push( {text: `${valueCell[index]}\r\n`})
                      }else
                      if(valueCell[index]?.bold == true){
                          richText.push( {font: {bold: true}, text: valueCell[index].text})
                      }else if(valueCell[index]?.underline == true){
                      richText.push( {font: {underline: true}, text: valueCell[index].text})
                      }else{
                          richText.push( {text: valueCell[index]?.text})
                      }
                  }
                  cell.value = {
                      richText :richText
                  }
              }else if(valueCell){
                  cell.value = {
                      richText :[{text:valueCell}]
                  }
              }
          }
      }
      for(let i=0; i<lenColumns;i++){
          const itemCell = cellArrayObject[i];
          const cell = worksheet.getCell(`${itemCell.code}1`);
          const row = worksheet.getRow(1)
          row.height = 30
          cell.fill = {
              type: "pattern",
              bgColor: {
                  argb: i<12?"4E81BE":"888888"
              },
              pattern:'solid',
        fgColor:{ argb:i<12?"4E81BE":"888888" }
          }
          cell.font={
              size: 12,
              bold: true
          }
          cell.alignment = {
              vertical: 'middle',
              horizontal:'left'
          }
          cell.border = {
              top: {style:'thin',color:{argb:'FF000000'}},
              left: {style:'thin',color:{argb:'FF000000'}},
              bottom: {style:'thin',color:{argb:'FF000000'}},
              right: {style:'thin',color:{argb:'FF000000'}}
          }
      }
    const buffer = await workbook.xlsx.writeBuffer();


    const url =  URL.createObjectURL(new Blob([buffer] ,{type:"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"}))
    const link = document.createElement('a');
    link.setAttribute('download', "result-pdf.xlsx");
    link.href = url;
    link.click();
  }
    return (
        <div className='handle-pdf-question-page'>

<div className='content-input'>
<label htmlFor="question_type">Từ khóa bắt đầu câu hỏi:</label>
    <input style={{background:"#ff0"}} defaultValue="choose the option that best completes the" name="key" id="key" ></input>
</div>
<div className='content-input'>
<label htmlFor="question_type">Loại câu hỏi:</label>
<select name="question_type" id="question_type">
{
        TASK_SELECTIONS.map((item:StructType)=>{
            return (
                <option key={`cefr-${item.code}`} value={item.code}>{item.display}</option>
            )
        })
    }
</select>
</div>
<div className='content-input'>
<label htmlFor="level">Độ khó:</label>
<select name="level" id="level">
{
        LEVEL_SELECTIONS.map((item:StructType)=>{
            return (
                <option key={`cefr-${item.code}`} value={item.code}>{item.display}</option>
            )
        })
    }
</select>
</div>
<div className='content-input'>
<label htmlFor="cefr">CEFR:</label>
<select name="cefr" id="cefr">
    {
        CERF_SELECTIONS.map((item:StructType)=>{
            return (
                <option key={`cefr-${item.code}`} value={item.code}>{item.display}</option>
            )
        })
    }
</select>
</div>
<div className='content-input'>
<label htmlFor="skill">Kỹ năng:</label>
<select name="skill" id="skill">
{
        SKILLS_SELECTIONS.map((item:StructType)=>{
            return (
                <option key={`cefr-${item.code}`} value={item.code}>{item.display}</option>
            )
        })
    }
</select>
</div>

<div className='content-input'>
        <span>Chọn file tải lên:</span>
        <input
                      type="file"
                      ref={fileImport}
                      accept=".pdf"
                      onChange={onChangeReadFile}
                    ></input>
      </div>
        </div>
  )
}
export default Home
