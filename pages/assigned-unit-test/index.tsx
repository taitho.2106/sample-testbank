import React from 'react'

import AssignPage from '@/componentsV2/AssignedUnitTest/AssignPage'
import { Wrapper } from 'components/templates/wrapper'
import useTranslation from '@/hooks/useTranslation'

export default function AssignedUnitTest() {
  const { t } = useTranslation()
  const pageTitle = `${t('left-menu')['unit-test-assigned']}`
    return (
        <Wrapper
            pageTitle={pageTitle}
            hasStickyFooter={false}
        >
            <AssignPage />
        </Wrapper>
    )
}