import React from 'react'

import SchedulesContainer from "@/componentsV2/Schedules";
import useTranslation from '@/hooks/useTranslation';
import { Wrapper } from 'components/templates/wrapper'

export default function ClassesManagement() {
    const {t} = useTranslation()
    const pageTitle = t("left-menu")["test-schedule"]
    return (
        <Wrapper
            pageTitle={pageTitle}
            hasStickyFooter={false}
        >
            <SchedulesContainer />
        </Wrapper>
    )
}
