import { GetServerSideProps } from 'next'
import { getCsrfToken, getSession } from 'next-auth/client'

import ChatHelp from 'components/chatHelp'
import { Register } from 'components/pages/register'
import { USER_GUIDE_SCREEN_ID } from 'interfaces/constants'
import { LoginContext } from 'interfaces/contexts'
import SignUpProcess from '../../components-v2/Authentication/SignUp'

type PropsType = {
  csrfToken: string
}

const RegisterPage = ({ csrfToken }: PropsType) => {
  return (
    <>
      <LoginContext.Provider value={{ csrfToken }}>
        <SignUpProcess />
      </LoginContext.Provider>
      <ChatHelp screenId={USER_GUIDE_SCREEN_ID.Register} />
    </>

  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context)
  const csrfToken = await getCsrfToken(context)

  if (session) {
    const user: any = session.user
    return {
      redirect: {
        destination: user.redirect ?? '/questions',
        permanent: false,
      },
    }
  }

  return { props: { csrfToken: csrfToken || null } }
}

export default RegisterPage
