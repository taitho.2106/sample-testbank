import { useEffect, useState } from 'react'

import { GetServerSideProps } from 'next'
import { getSession, useSession } from 'next-auth/client'
import Error from 'next/error'
import { useRouter } from 'next/router'

import UnitTestPage from '@/componentsV2/TeacherPages/UnitTestPage'
import { userInRight } from '@/utils/index'
import { paths } from 'api/paths'
import ChatHelp from 'components/chatHelp'
import { UnitTestContainer } from 'components/organisms/unitTestContainer'
import { USER_GUIDE_SCREEN_ID, USER_ROLES } from 'interfaces/constants'
import { BreadcrumbItemType } from 'interfaces/types'

import { Wrapper } from '../../components/templates/wrapper'
import { callApi } from 'api/utils'
import useTranslation from "@/hooks/useTranslation";

export default function UnitTestListPage() {
  const router = useRouter()
  const { t } = useTranslation();
  const [session] = useSession()
  const user: any = session?.user || null

  const [errCode, setErrCode] = useState(null)

  const pageTitle = router.query.m
    ? `${router.query.m === 'mine' ? t('page-title')['my-test-bank'] : t('page-title')['teacher-test-bank']}`
    : t('left-menu')['list-unit-tests']
  //userGuideScreenID
  const owned = router.query.m;
  const userGuideScreenId = owned=="mine"?USER_GUIDE_SCREEN_ID.My_UnitTests:(owned=='teacher'?
  -1:USER_GUIDE_SCREEN_ID.System_UnitTests)

  const breadcrumbData: BreadcrumbItemType[] = [
    { name: 'Tổng quan', url: '/' },
    { name: pageTitle, url: null },
  ]
  const isNewUI = router?.query?.m === 'mine' || router?.query?.m === 'teacher'
  useEffect(() => {
    let err = -1
    if (user) {
      if (router.query?.m) {
        switch (router.query.m) {
          case 'mine':
            err =
              user?.is_admin === 0 && (user?.user_role_id === USER_ROLES.Teacher || user?.user_role_id === USER_ROLES.Student)
                ? 0
                : 403
            break
          case 'teacher':
            err = user?.is_admin === 1 ? 0 : 403
            break
          default:
            err = 404
            break
        }
      } else err = 0
    }

    switch (err) {
      case -1:
        break
      case 0:
        setErrCode(null)
        break
      default:
        setErrCode(err)
        break
    }
  }, [user, router.asPath])

  // error
  if ([404, 403].includes(errCode)) return <Error statusCode={errCode} />
  // success
  else
    return (
        <> 
              <Wrapper
                className={`p-create-new-template p-detail-format ${isNewUI ? '' : 'new-styles-css'}`}
                pageTitle={pageTitle}
              >
                {/* <div className="p-detail-format__breadcrumb">
                  <BreadCrumbNav data={breadcrumbData} />
                </div>
                <UnitTestContainer /> */}
                
                {isNewUI ? (<><UnitTestContainer /></>) : (<><UnitTestPage /></>)}
              </Wrapper>
              <ChatHelp screenId={userGuideScreenId}></ChatHelp>
        </>
    )
}
