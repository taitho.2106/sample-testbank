import { Fragment, useEffect, useRef, useState } from 'react'

import { GetServerSideProps } from 'next'
import { getTextWithoutFontStyle } from 'utils/string'

type PropsType = {
  id: string
}

export default function UnitTestExportPage({ id }: PropsType) {
  const [data, setData] = useState(null)
  const idInterval: any = useRef(0)
  let questionIndex = 0
  useEffect(() => {
    fetch(`/api/unit-test/${id}`, {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
    })
      .then((res) => res.json())
      .then((resp) => {
        setData(resp.data)
        idInterval.current = setInterval(() => {
          if (
            document.querySelector(
              `.unit-test-section[data-id='${
                resp.data.sections[resp.data.sections.length - 1].id
              }']`,
            )
          ) {
            clearInterval(idInterval.current)
            setTimeout(() => {
              window.print()
            }, 1000)
          }
        }, 100)
      })
  }, [])

  const renderQuestion = (question: any, index: number) => {
    let Template = MultiChoiceQuestion1
    switch (question.question_type) {
      case 'MC1':
        Template = MultiChoiceQuestion1_v2
        break
      case 'MC3':
        Template = MultiChoiceQuestion1
        break
      case 'MC4':
        Template = MultiChoiceQuestion4
        break
      case 'MC2':
        Template = MultiChoiceQuestion2
        break
      case 'MC5':
      case 'MC6':
        Template = MultiChoiceQuestion5
        break
      case 'FB1':
        Template = FillInBlankQuestion1
        break
      case 'FB2':
      case 'FB3':
      case 'FB4':
      case 'FB6':
      case 'FB7':
        Template = FillInBlankQuestion2
        break
      case 'FB5':
        Template = FillInBlankQuestion5
        break
      case 'SL1':
        Template = SelectFromListQuestion1
        break
      case 'SL2':
        Template = SelectFromListQuestion2
        break
      case 'DD1':
        Template = DragAndDropQuestion
        break
      case 'SA1':
        Template = ShortAnswerQuestion
        break
      case 'TF1':
      case 'TF2':
        Template = TrueFalseQuestion
        break
      case 'MG1':
      case 'MG2':
      case 'MG3':
        Template = MatchingGameQuestion
        break
      case 'MR1':
      case 'MR2':
        Template = MultiResponseQuestion1
        break
      case 'MR3':
        Template = MultiResponseQuestion3
        break
      case 'LS1':
      case 'LS2':
        Template = LikertScaleQuestion
        break
      case 'MIX1':
        Template = MIXQuestion1
        break
      case 'MIX2':
        Template = MIXQuestion2
        break
      case 'TF3':
      case 'TF4':
        Template = TrueFalseQuestion3_4
        break
      default:
        Template = null
    }
    if (Template === null) return null
    return <Template question={question} index={index} />
  }

  if (data === null) return <></>

  return (
    <div className="p-unit-test-export">
      <p className="unit-test-title">{data.name}</p>
      {data.sections.map((section: any, sIndex: number) => (
        <div
          key={section.parts[0].name}
          className="unit-test-section"
          data-id={section.id}
          style={{
            width: '50%',
            float: 'left',
            verticalAlign: 'top',
            ...(sIndex > 1 && sIndex % 2 === 0 ? { clear: 'both' } : {}),
          }}
        >
          <div className="unit-test-part prevent-page-break">
            <div>PART {sIndex + 1}</div>
            {section.parts[0].name}
          </div>
          {section.parts[0].questions.map((question: any, qIndex: number) => {
            const currentQuestion = questionIndex + 1
            if (excludeType.includes(question.question_type)) {
              questionIndex += 1
            } else {
              questionIndex += question.total_question
            }
            return (
              <Fragment key={`${section.parts[0].name}${qIndex}`}>
                {renderQuestion(question, currentQuestion)}
              </Fragment>
            )
          })}
        </div>
      ))}
      <span className="end-page">---THE END---</span>
    </div>
  )
}

const excludeType = ['MR1', 'MR2', 'MR3']

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context

  return { props: { id: query.templateSlug } }
}

const correctText: any = { T: 'True', F: 'False', NI: 'Not given' }

const MultiChoiceQuestion1_v2 = ({ question, index }: any) => {
  const listAnswers = question.answers.split('#')
  const listQuestionTexts = question.question_text.split('#')
  const listCorrectAnswers = question.correct_answers.split('#')
  return (
    <Fragment>
      {listQuestionTexts.map((question_text: string, indexItem: number) => {
        const answers = listAnswers[indexItem].split('*')
        return (
          <div
            key={`${index}-${indexItem}`}
            className="box-question"
            style={{
              marginTop: '5px',
              width: '30%',
              float: 'left',
            }}
          >
            <span className="instruction" style={{ fontWeight: 'normal' }}>
              <span>{index + indexItem}.</span>
              {String.fromCharCode(
                65 +
                  answers.findIndex(
                    (m: string) =>
                      getTextWithoutFontStyle(m) ===
                      listCorrectAnswers[indexItem],
                  ),
              )}
            </span>
          </div>
        )
      })}
    </Fragment>
  )
}
const MultiChoiceQuestion4 = ({ question, index }: any) => {
  const answers = question.answers.split('*')

  return (
    <div
      className="box-question"
      style={{
        marginTop: '5px',
        width: '30%',
        float: 'left',
      }}
    >
      <span className="instruction" style={{ fontWeight: 'normal' }}>
        <span>{index}.</span>
        {String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string) =>
                getTextWithoutFontStyle(m) === question.correct_answers,
            ),
        )}
      </span>
    </div>
  )
}
const MultiChoiceQuestion1 = ({ question, index }: any) => {
  const answers = question.answers
    .split('*')
    .map((m: string) => m.replace(/\%/g, ''))

  return (
    <div
      className="box-question"
      style={{
        marginTop: '5px',
        width: '30%',
        float: 'left',
      }}
    >
      <span className="instruction" style={{ fontWeight: 'normal' }}>
        <span>{index}.</span>
        {String.fromCharCode(
          65 + answers.findIndex((m: string) => m === question.correct_answers),
        )}
      </span>
    </div>
  )
}

const MultiChoiceQuestion2 = ({ question, index }: any) => {
  const listAnswers = question.answers.split('#')
  const listQuestionTexts = question.question_text.split('#')
  const listCorrectAnswers = question.correct_answers.split('#')
  while(listQuestionTexts.length>0){
    if(listQuestionTexts[0].startsWith("*")){
      listQuestionTexts.shift();
      listCorrectAnswers.shift();
      listAnswers.shift();
    }else{
      break;
    }
  }
  return (
    <Fragment>
      {listQuestionTexts.map((question_text: string, indexItem: number) => {
        const answers = listAnswers[indexItem].split('*')
        return (
          <div
            key={`${index}-${indexItem}`}
            className="box-question"
            style={{
              marginTop: '5px',
              width: '30%',
              float: 'left',
            }}
          >
            <span className="instruction" style={{ fontWeight: 'normal' }}>
              <span>{index + indexItem}.</span>
              {String.fromCharCode(
                65 +
                  answers.findIndex(
                    (m: string) =>
                      getTextWithoutFontStyle(m) ===
                      listCorrectAnswers[indexItem],
                  ),
              )}
            </span>
          </div>
        )
      })}
    </Fragment>
  )
}

const MultiChoiceQuestion5 = ({ question, index }: any) => {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const correctList = question.correct_answers?.split('#')
  const groupKey = useRef(Math.random()).current
  return (
    <Fragment>
      {questionList.map((ques: any, qIndex: number) => {
        const answers = answerList[qIndex].split('*')
        return (
          <div
            key={`${groupKey}_${qIndex}`}
            className="box-question"
            style={{
              marginTop: '5px',
              width: '30%',
              float: 'left',
            }}
          >
            <span className="instruction" style={{ fontWeight: 'normal' }}>
              <span>{index + qIndex}.</span>
              {String.fromCharCode(
                65 +
                  answers.findIndex(
                    (m: string) =>
                      getTextWithoutFontStyle(m) === correctList[qIndex],
                  ),
              )}
            </span>
          </div>
        )
      })}
    </Fragment>
  )
}

const FillInBlankQuestion1 = ({ question, index }: any) => {
  return (
    <div
      className="box-question"
      style={{
        marginTop: '5px',
        width: '100%',
        float: 'left',
      }}
    >
      <span className="instruction" style={{ fontWeight: 'normal' }}>
        <span>{index}.</span>
        {question.correct_answers.replace(/\%\/\%/g, '/')}
      </span>
    </div>
  )
}

const FillInBlankQuestion2 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const correctList = question.correct_answers
    .split('#')
    .map((m: string) => m.replace(/\%\/\%/g, '/'))
  return (
    <Fragment>
      {correctList.map((part: string, partIndex: number) => (
        <div
          className="box-question"
          key={`${groupKey}_${partIndex}`}
          style={{
            marginTop: '5px',
            width: '100%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + partIndex}.</span>
            {part}
          </span>
        </div>
      ))}
    </Fragment>
  )
}

const FillInBlankQuestion5 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const correctList = question.correct_answers
    .split('#')
    .map((m: string) => m.replace(/\%\/\%/g, '/'))
  return (
    <Fragment>
      {correctList.map((part: string, partIndex: number) => (
        <div
          className="box-question"
          key={`${groupKey}_${partIndex}`}
          style={{
            marginTop: '5px',
            width: '100%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + partIndex}.</span>
            {part}
          </span>
        </div>
      ))}
    </Fragment>
  )
}

const SelectFromListQuestion1 = ({ question, index }: any) => {
  const listAnswers = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const listCorrect = question.correct_answers.split('#')
  const groupKey = useRef(Math.random()).current

  return (
    <Fragment>
      {listAnswers.map((answers: string[], answersIndex: number) => (
        <div
          className="box-question"
          key={`${groupKey}_${answersIndex}`}
          style={{
            marginTop: '5px',
            width: '30%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + answersIndex}.</span>
            {String.fromCharCode(
              65 +
                answers.findIndex(
                  (m: string) => m.trim() === listCorrect[answersIndex].trim(),
                ),
            )}
          </span>
        </div>
      ))}
    </Fragment>
  )
}

const SelectFromListQuestion2 = ({ question, index }: any) => {
  const listCorrect: string[] = question.correct_answers.split('#')
  const groupKey = useRef(Math.random()).current
  return (
    <Fragment>
      {listCorrect.map((correct: string, _index) => (
        <div
          className="box-question"
          key={`${groupKey}_${_index}`}
          style={{
            marginTop: '5px',
            width: '30%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + _index}.</span>
            {correct}
          </span>
        </div>
      ))}
    </Fragment>
  )
}

const DragAndDropQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const answers = question.answers.split('#')
  const correctAnswers = question.correct_answers.split('#')
  const correctList: string[] = []
  const checkExam = question.question_text?.split('#') || ''

  for (let i = 0; i < correctAnswers.length; i++) {
    if (answers[i]) {
      if (!checkExam || checkExam[i] === '') {
        correctList.push(correctAnswers[i])
      }
    }
  }

  return (
    <Fragment>
      {correctList.map((answer: any, answerIndex: number) => (
        <div
          className="box-question"
          key={`${groupKey}_${answerIndex}`}
          style={{
            marginTop: '5px',
            width: '100%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + answerIndex}.</span>
            {answer
              .replace(/\*/g, ' ')
              .replace(/\%\/\%/g, '\/\n')}
          </span>
        </div>
      ))}
    </Fragment>
  )
}

const ShortAnswerQuestion = ({ question, index }: any) => {
  return (
    <div
      className="box-question"
      style={{
        marginTop: '5px',
        width: '100%',
        float: 'left',
      }}
    >
      <span className="instruction" style={{ fontWeight: 'normal' }}>
        <span>{index}.</span>
        {question.correct_answers.replace(/\%\/\%/g, '/')}
      </span>
    </div>
  )
}

const TrueFalseQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const correctList: string[] = question.correct_answers.split('#')
  return (
    <Fragment>
      {correctList.map((answer, answerIndex) => (
        <div
          className="box-question"
          key={`${groupKey}_${answerIndex}`}
          style={{
            marginTop: '5px',
            width: '30%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + answerIndex}.</span>
            {correctText[answer]}
          </span>
        </div>
      ))}
    </Fragment>
  )
}
const TrueFalseQuestion3_4 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const answers = question.answers.split('#')
  const correctAnswers = question.correct_answers.split('#')
  const correctList: string[] = []
  //remove example
  for (let i = 0; i < correctAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        correctList.push(correctAnswers[i])
      }
    }
  }
  return (
    <Fragment>
      {correctList.map((answer, answerIndex) => (
        <div
          className="box-question"
          key={`${groupKey}_${answerIndex}`}
          style={{
            marginTop: '5px',
            width: '30%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + answerIndex}.</span>
            {correctText[answer]}
          </span>
        </div>
      ))}
    </Fragment>
  )
}
const MatchingGameQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const correctList: string[] = question.correct_answers.split('#')
  return (
    <Fragment>
      {listAnswer[0].map((_, mIndex: number) => (
        <div
          className="box-question"
          key={`${groupKey}_${mIndex}`}
          style={{
            marginTop: '5px',
            width: '30%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + mIndex}.</span>
            {String.fromCharCode(
              97 +
                listAnswer[1].findIndex(
                  (m: string) => m === correctList[mIndex],
                ),
            )}
          </span>
        </div>
      ))}
    </Fragment>
  )
}

const MultiResponseQuestion1 = ({ question, index }: any) => {
  const answers: string[] = question.answers.split('#')
  const correctList: string[] = question.correct_answers.split('#')

  return (
    <div
      className="box-question"
      style={{
        marginTop: '5px',
        width: '30%',
        float: 'left',
      }}
    >
      <span className="instruction" style={{ fontWeight: 'normal' }}>
        <span>{index}.</span>
        {answers
          .map((answer, answerIndex) =>
            correctList.includes(answer)
              ? String.fromCharCode(65 + answerIndex)
              : null,
          )
          .filter((m) => m !== null)
          .join(', ')}
      </span>
    </div>
  )
}

const MultiResponseQuestion3 = ({ question, index }: any) => {
  const answers: string[] = question.answers.split('#')
  const correctList: string[] = question.correct_answers.split('#')
  return (
    <div
      className="box-question"
      style={{
        marginTop: '5px',
        width: '30%',
        float: 'left',
      }}
    >
      <span className="instruction" style={{ fontWeight: 'normal' }}>
        <span>{index}.</span>
        {answers
          .map((answer, answerIndex) =>
            correctList.includes(answer)
              ? String.fromCharCode(65 + answerIndex)
              : null,
          )
          .filter((m) => m !== null)
          .join(', ')}
      </span>
    </div>
  )
}

const LikertScaleQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const correctList: string[] = question.correct_answers.split('#')

  return (
    <Fragment>
      {correctList.map((answer, answerIndex) => (
        <div
          className="box-question"
          key={`${groupKey}_${answerIndex}`}
          style={{
            marginTop: '5px',
            width: '30%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + answerIndex}.</span>
            {correctText[answer]}
          </span>
        </div>
      ))}
    </Fragment>
  )
}

const MIXQuestion1 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const listAnswers: string[] = question.answers.split('[]')
  const tfAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction: string[] = question.question_description.split('[]')
  const mcQuestions: string[] = question.question_text.split('[]')[1].split('#')
  const listCorrect: string[] = question.correct_answers.split('[]')
  const tfCorrects: string[] = listCorrect[0].split('#')
  const mcCorrects: string[] = listCorrect[1].split('#')
  return (
    <div
      className="box-question"
      style={{
        marginTop: '5px',
        width: '100%',
        float: 'left',
      }}
    >
      <span
        className="instruction"
        style={{ display: 'inline-block', marginRight: 10 }}
      >{`Task 1: ${listInstruction[0]}`}</span>
      {tfCorrects.map((answer, answerIndex) => (
        <div
          className="box-question"
          key={`tf${groupKey}_${answerIndex}`}
          style={{
            marginTop: '5px',
            width: '30%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + answerIndex}.</span>
            {correctText[answer]}
          </span>
        </div>
      ))}
      <span
        className="instruction"
        style={{ marginTop: '20px', display: 'inline-block', marginRight: 10 }}
      >{`Task 2: ${listInstruction[1]}`}</span>
      {mcQuestions.map((ques: any, qIndex: number) => {
        const answers = mcAnswers[qIndex].split('*')
        return (
          <div
            key={`${groupKey}_${qIndex}`}
            className="box-question"
            style={{
              marginTop: '5px',
              width: '30%',
              float: 'left',
            }}
          >
            <span className="instruction" style={{ fontWeight: 'normal' }}>
              <span>{index + tfAnswers.length + qIndex}.</span>
              {String.fromCharCode(
                65 + answers.findIndex((m: string) => m === mcCorrects[qIndex]),
              )}
            </span>
          </div>
        )
      })}
    </div>
  )
}

const MIXQuestion2 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const listAnswers: string[] = question.answers.split('[]')
  const lsAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction: string[] = question.question_description.split('[]')
  const mcQuestions: string[] = question.question_text.split('[]')[1].split('#')
  const listCorrect: string[] = question.correct_answers.split('[]')
  const lsCorrects: string[] = listCorrect[0].split('#')
  const mcCorrects: string[] = listCorrect[1].split('#')
  return (
    <div
      className="box-question"
      style={{
        marginTop: '5px',
      }}
    >
      <span
        className="instruction"
        style={{ display: 'inline-block', marginRight: 10 }}
      >{`Task 1: ${listInstruction[0]}`}</span>
      {lsCorrects.map((answer, answerIndex) => (
        <div
          className="box-question"
          key={`${groupKey}_${answerIndex}`}
          style={{
            marginTop: '5px',
            width: '30%',
            float: 'left',
          }}
        >
          <span className="instruction" style={{ fontWeight: 'normal' }}>
            <span>{index + answerIndex}.</span>
            {correctText[answer]}
          </span>
        </div>
      ))}
      <span
        className="instruction"
        style={{ marginTop: '20px', display: 'inline-block', marginRight: 10 }}
      >{`Task 2: ${listInstruction[1]}`}</span>
      {mcQuestions.map((ques: any, qIndex: number) => {
        const answers = mcAnswers[qIndex].split('*')
        return (
          <div
            key={`${groupKey}_${qIndex}`}
            className="box-question"
            style={{
              marginTop: '5px',
              width: '30%',
              float: 'left',
            }}
          >
            <span className="instruction" style={{ fontWeight: 'normal' }}>
              <span>{index + lsAnswers.length + qIndex}.</span>
              {String.fromCharCode(
                65 + answers.findIndex((m: string) => m === mcCorrects[qIndex]),
              )}
            </span>
          </div>
        )
      })}
    </div>
  )
}
