import { useEffect, useState } from 'react'

import { GetServerSideProps } from 'next'
import { useRouter } from 'next/dist/client/router'
import Error from 'next/error'

import { templateListTransformData } from 'api/dataTransform'
import { callApi } from 'api/utils'
import ChatHelp from 'components/chatHelp'
import { CreateNewUnittest } from 'components/pages/createNewUnittest'
import { PACKAGES, USER_GUIDE_SCREEN_ID } from 'interfaces/constants'

import { SingleTemplateContext } from '../../../interfaces/contexts'
import { BreadcrumbItemType } from '../../../interfaces/types'
import useTranslation from "@/hooks/useTranslation";

export default function TemplateDetailPage({ id }: any) {
  const { t } = useTranslation()
  const router = useRouter()
  const defaultMode =
    router.query.templateSlug === 'create-new-unittest'
      ? 'create'
      : router.query?.mode === 'edit'
      ? 'update'
      : 'detail'
  //userGuideScreenID
  const owned = router.query.m;
  let userGuideScreenId = -1;
  switch(owned){
    case "mine":
      switch(defaultMode){
        case "create":
          userGuideScreenId = USER_GUIDE_SCREEN_ID.My_UnitTests_Create;
          break;
          case "update":
          userGuideScreenId = USER_GUIDE_SCREEN_ID.My_UnitTests_Edit;
          break;
          case "detail":
            userGuideScreenId = USER_GUIDE_SCREEN_ID.My_UnitTests_View;
            break;
            default:
              userGuideScreenId = -1;
              break;
      }
      break;
      case "teacher":
        userGuideScreenId = -1;
        break;
      default :
      switch(defaultMode){
          case "detail":
            userGuideScreenId = USER_GUIDE_SCREEN_ID.System_UnitTests_View;
            break;
            default:
              userGuideScreenId = -1;
              break;
      }
      break;
  }
  ///end user_guid_id

  const [mode, setMode] = useState(defaultMode)

  const [chosenTemplate, setChosenTemplate] = useState(null)
  const [tempData, setTempData] = useState(null)
  const [templateData, setTemplateData] = useState([])
  const [isReady, setIsReady] = useState(
    !['update', 'detail'].includes(mode) ? true : null,
  )
  const [isLoading, setIsLoading] = useState(true)

  const [currentPage, setCurrentPage] = useState(1)
  const [totalPage, setTotalPage] = useState(1)

  const breadcrumbData: BreadcrumbItemType[] = [
    { name: t('left-menu')['overview'], url: '/' },
    { name: t('left-menu')['list-unit-tests'], url: '/unit-test' },
    {
      name:
        router.query.templateSlug === 'create-new-unittest'
          ? t('unit-test')['title-page']
          : chosenTemplate?.name || '',
      url: null,
    },
  ]

  useEffect(() => {
    const fetchData = async () => {
      if (id === 'create-new-unittest') {
      } else {
        const response: any = await callApi(
          `/api/unit-test/${id}?mode=${mode === 'update' ? 'edit' : 'view'}`,
          'GET',
        )
        if (response?.status !== 200) {
          setIsReady(response?.status || 500)
          setIsLoading(false)
          return
        }
        if (response) {
          // console.log("response?.data",response?.data)
          const apiData = response?.data
          const originData = {
            id: apiData?.id || null,
            templateId: apiData?.template_id || null,
            author: apiData?.created_by || null,
            date: [apiData?.start_date || null, apiData?.end_date || null],
            name: apiData?.name || '',
            totalPoints: apiData?.total_point || 0,
            privacy: apiData?.privacy,
            sections: apiData?.sections
              ? apiData.sections.map((section: any) => ({
                  id: section?.id || null,
                  parts: section?.parts
                    ? section.parts.map((part: any) => ({
                        id: part?.id || null,
                        name: part?.name || '',
                        points: part?.points || 0,
                        questions: part?.questions || [],
                        questionTypes: part?.question_types || '',
                        sectionId: part?.unit_test_section_id || null,
                        totalQuestion: part?.total_question || 0,
                      }))
                    : [],
                  sectionId: section?.section ? section.section.split(',') : [],
                }))
              : [],
            status: apiData?.status || false,
            templateLevelId: apiData?.template_level_id || '',
            time: apiData?.time || 0,
            totalQuestion: apiData?.total_question || 0,
            scope: apiData?.scope || 0,
            unitType: [0, 1].includes(apiData?.unit_type)
              ? `${apiData.unit_type}`
              : '0',
            series_id: apiData?.series_id || '',
            unit_test_type: apiData?.unit_test_type || null,
            package_level: apiData?.package_level,
            is_international_exam: apiData?.is_international_exam || 0
          }
          setChosenTemplate({ ...originData })
          setTempData({ ...response?.data })
          setIsReady(true)
        }
      }
    }
    fetchData()
  }, [])
  useEffect(() => {
    const fetchData = async () => {
      // console.log("router.asPath----",{path:router, id})
      if (router.query.templateSlug === 'create-new-unittest') {
        setIsLoading(true)
        const response: any = await callApi(
          '/api/templates/search',
          'post',
          'Token',
          {
            page: 0,
            scope: 3,
            status: 1
          },
        )
        if (response?.templates) setTemplateData([...response.templates])
        if (response?.currentPage || response.currentPage === 0)
          setCurrentPage(response.currentPage + 1)
        if (response?.totalPages) setTotalPage(response.totalPages)
        if (response) {
          setIsReady(true)
          setIsLoading(false)
        }
      }
    }
    fetchData()
  }, [router.asPath])

  if (isReady !== true && !isLoading)
    return <Error statusCode={isReady || 500} />

  return (
    <>
    <SingleTemplateContext.Provider
      value={{
        breadcrumbData,
        mode,
        setMode,
        templateDetailOrigin: null,
        templateData: templateData.map(templateListTransformData),
        setTemplateData: setTemplateData,
        totalPages: totalPage,
        currentPage,
        chosenTemplate: { state: chosenTemplate, setState: setChosenTemplate },
        tempData: { state: tempData, setState: setTempData },
      }}
    >
      <CreateNewUnittest
        title={
          router.query.templateSlug === 'create-new-unittest'
            ? t('unit-test')['title-page']
            : chosenTemplate?.name || ''
        }
      />
    </SingleTemplateContext.Provider>
    <ChatHelp screenId={userGuideScreenId}></ChatHelp>
    </>
    
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context
  return { props: { id: query.templateSlug } }
}