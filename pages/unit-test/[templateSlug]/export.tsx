import { CSSProperties, Fragment, useEffect, useRef, useState } from 'react'

import { GetServerSideProps } from 'next'

import { formatHtmlText, getTextWithoutFontStyle } from 'utils/string'

type PropsType = {
  id: string
}

export default function UnitTestExportPage({ id }: PropsType) {
  const [data, setData] = useState(null)
  const idInterval: any = useRef(0)
  let questionIndex = 0
  useEffect(() => {
    fetch(`/api/unit-test/${id}`, {
      method: 'get',
      headers: { 'Content-Type': 'application/json' },
    })
      .then((res) => res.json())
      .then((resp) => {
        setData(resp.data)
        idInterval.current = setInterval(() => {
          if (
            document.querySelector(
              `.unit-test-section[data-id='${
                resp.data.sections[resp.data.sections.length - 1].id
              }']`,
            )
          ) {
            clearInterval(idInterval.current)
            setTimeout(() => {
              window.print()
            }, 1000)
          }
        }, 100)
      })
  }, [])

  const renderQuestion = (question: any, index: number) => {
    let Template = MultiChoiceQuestion1
    switch (question.question_type) {
      case 'MC1':
        Template = MultiChoiceQuestion1_v2
        break
      case 'MC3':
        Template = MultiChoiceQuestion1
        break
      case 'MC4':
        Template = MultiChoiceQuestion4
        break
      case 'MC2':
        Template = MultiChoiceQuestion2
        break
      case 'MC5':
      case 'MC6':
        Template = MultiChoiceQuestion5
        break
      case 'FB1':
        Template = FillInBlankQuestion1
        break
      case 'FB2':
      case 'FB3':
      case 'FB4':
        Template = FillInBlankQuestion2_3_4_v2
        break
      case 'FB6':
      case 'FB7':
        Template = FillInBlankQuestion2
        break
      case 'FB5':
        Template = FillInBlankQuestion5
        break
      case 'SL1':
        Template = SelectFromListQuestion1
        break
      case 'SL2':
        Template = SelectFromListQuestion2
        break
      case 'DD1':
        Template = DragAndDropQuestion
        break
      case 'SA1':
        Template = ShortAnswerQuestion
        break
      case 'TF1':
      case 'TF2':
        Template = TrueFalseQuestion
        break
      case 'MG1':
      case 'MG2':
      case 'MG3':
        Template = MatchingGameQuestion
        break
      case 'MR1':
      case 'MR2':
        Template = MultiResponseQuestion1
        break
      case 'MR3':
        Template = MultiResponseQuestion3
        break
      case 'LS1':
      case 'LS2':
        Template = LikertScaleQuestion
        break
      case 'MIX1':
        Template = MIXQuestion1
        break
      case 'MIX2':
        Template = MIXQuestion2
        break
      case 'TF3':
        Template = TrueFalseQuestion3
        break
      case 'TF4':
        Template = TrueFalseQuestion4
        break
      default:
        Template = null
    }
    if (Template === null) return null
    return <Template question={question} index={index} />
  }

  if (data === null) return <></>

  return (
    <div className="p-unit-test-export">
      <table className="info">
        <tbody>
          <tr>
            <td
              align="center"
              className="vertical-top"
              style={{ width: '50%' }}
            >
              UBND QUẬN _________ <br /> PHÒNG GIÁO DỤC VÀ ĐÀO TẠO <br /> ĐỀ
              CHÍNH THỨC
            </td>
            <td align="center" colSpan={2} style={{ width: '50%' }}>
              ĐỀ KIỂM TRA __________ <br /> Năm học: 20__ - 20__ <br /> [MÔN
              HỌC] -{' '}
              {
                data.grade_name
              }{' '}
              <br />
              Thời gian làm bài: {data.time} phút
            </td>
          </tr>
          <tr>
            <td>
              <div className="user-info">
                Trường: _______________________________________
              </div>
              <div className="user-info">
                Họ tên: _______________________________________
              </div>
              <div className="user-info">Lớp: _________________</div>
            </td>
            <td align="center" className="vertical-top">
              Điểm
            </td>
            <td align="center" className="vertical-top">
              Nhận xét của giáo viên
            </td>
          </tr>
        </tbody>
      </table>
      <p className="unit-test-title">{data.name}</p>
      <p className="unit-test-time">Time allotted: {data.time} phút</p>
      {data.sections.map((section: any, sIndex: number) => (
        <div
          key={section.parts[0].name}
          className="unit-test-section"
          data-id={section.id}
        >
          <div className="unit-test-part prevent-page-break">
            <div>PART {sIndex + 1}</div>
            {section.parts[0].name}
          </div>
          {section.parts[0].questions.map((question: any, qIndex: number) => {
            const currentQuestion = questionIndex + 1
            if (excludeType.includes(question.question_type)) {
              questionIndex += 1
            } else {
              questionIndex += question.total_question
            }
            return (
              <Fragment key={`${section.parts[0].name}${qIndex}`}>
                {renderQuestion(question, currentQuestion)}
              </Fragment>
            )
          })}
        </div>
      ))}
      <span className="end-page">---THE END---</span>
    </div>
  )
}

const excludeType = ['MR1', 'MR2', 'MR3']

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context

  return { props: { id: query.templateSlug } }
}

const MultiChoiceQuestion1 = ({ question, index }: any) => {
  const answers = question.answers.split('*')
  const groupKey = useRef(Math.random()).current
  const formatAnswer = (answer: string) => {
    const ansl = answer.split('%')
    if (ansl.length === 3) {
      return ansl.map((m, mIndex) =>
        mIndex === 1 ? (
          <b key={groupKey + m}>
            <u>{m}</u>
          </b>
        ) : (
          m
        ),
      )
    }
    return answer
  }

  return (
    <div className="box-question">
      <span className="instruction">
        <span>{index}.</span>
        {question.question_description}
      </span>
      {question.question_text && (
        <span className="question">
          {question.question_text?.replace(/\%s\%/g, '______________')}
        </span>
      )}
      <div className="box-answers">
        {answers.map((answer: string, answerIndex: number) => (
          <div key={`${groupKey}_${answerIndex}`} className="answer">
            {`${String.fromCharCode(65 + answerIndex)}. `}
            {formatAnswer(answer)}
          </div>
        ))}
      </div>
    </div>
  )
}

const MultiChoiceQuestion1_v2 = ({ question, index }: any) => {
  const answers = question.answers.split('#')
  const questionTexts: string[] = question.question_text.split('#')
  const groupKey = useRef(Math.random()).current

  if (questionTexts.length == 1) {
    return (
      <div className="box-question">
        <span className="instruction">
          <span>{index}.</span>
          {question.question_description}
        </span>
        {questionTexts[0] && (
          <span
            className="question"
            dangerouslySetInnerHTML={{
              __html: formatHtmlText(questionTexts[0])?.replaceAll(
                /\%s\%/g,
                '______________',
              ),
            }}
          ></span>
        )}
        <div className="box-answers">
          {answers[0]
            .split('*')
            ?.map((answerItem: string, answerIndex: number) => (
              <div key={`${groupKey}_${answerIndex}`} className="answer">
                {`${String.fromCharCode(65 + answerIndex)}. `}
                <span
                  dangerouslySetInnerHTML={{
                    __html: formatHtmlText(answerItem),
                  }}
                ></span>
              </div>
            ))}
        </div>
      </div>
    )
  }
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {answers.map((answerQuestion: string, answerItemIndex: number) => {
        const questionTextItem =
          `<span style="width:32px; display: inline-block">${
            answerItemIndex + index
          }.</span>` +
          formatHtmlText(questionTexts[answerItemIndex])?.replaceAll(
            /\%s\%/g,
            '______________',
          )
        const hasQuestionText =
          questionTexts[answerItemIndex] &&
          questionTexts[answerItemIndex].trim() != ''
        return (
          <>
            {hasQuestionText && (
              <span
                className="question"
                dangerouslySetInnerHTML={{
                  __html: questionTextItem,
                }}
              ></span>
            )}
            <div className="box-answers">
              {!hasQuestionText ? (
                <span className="question-text-empty-mc1">
                  {answerItemIndex + index}.
                </span>
              ) : (
                <span className="question-text-empty-mc1"></span>
              )}
              {answerQuestion
                .split('*')
                ?.map((answerItem: string, answerIndex: number) => (
                  <div
                    key={`${groupKey}_ques_ans_${answerIndex}`}
                    className="answer"
                  >
                    {`${String.fromCharCode(65 + answerIndex)}. `}
                    <span
                      dangerouslySetInnerHTML={{
                        __html: formatHtmlText(answerItem),
                      }}
                    ></span>
                  </div>
                ))}
            </div>
          </>
        )
      })}
    </div>
  )
}
const MultiChoiceQuestion4 = ({ question, index }: any) => {
  const answer = question.answers
  const questionText = question.question_text
  const groupKey = useRef(Math.random()).current
  return (
    <div className="box-question">
      <span className="instruction">
        <span>{index}.</span>
        {question.question_description}
      </span>
      {questionText && (
        <span
          className="question"
          dangerouslySetInnerHTML={{
            __html: formatHtmlText(questionText)?.replaceAll(
              /\%s\%/g,
              '______________',
            ),
          }}
        ></span>
      )}
      <div className="box-answers">
        {answer.split('*')?.map((answerItem: string, answerIndex: number) => (
          <div key={`${groupKey}_${answerIndex}`} className="answer">
            {`${String.fromCharCode(65 + answerIndex)}. `}
            <span
              dangerouslySetInnerHTML={{
                __html: formatHtmlText(answerItem),
              }}
            ></span>
          </div>
        ))}
      </div>
    </div>
  )
}
const MultiChoiceQuestion2 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  type dataViewModel = {
    questions: string[]
    answers: string[]
    corrects: string[]
  }
  const listAnswer: string[] = question.answers.split('#')
  const listQuestionText: string[] = question.question_text.split('#')
  const listCorrectAnswer: string[] = question.correct_answers.split('#')

  const image = question.image
  const imageUrl = `/upload/${image}`
  const example: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
  }
  const mainQuestion: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
  }
  for (let i = 0; i < listQuestionText.length; i++) {
    if (listQuestionText[i]?.startsWith('*')) {
      example.questions.push(listQuestionText[i].substring(1))
      example.answers.push(listAnswer[i])
      example.corrects.push(listCorrectAnswer[i])
    } else {
      mainQuestion.questions.push(listQuestionText[i])
      mainQuestion.answers.push(listAnswer[i])
      mainQuestion.corrects.push(listCorrectAnswer[i])
    }
  }
  return (
    <div className="box-question box-question-export-mc2">
      <span className="instruction">
        {question.parent_question_description}
      </span>
      <div>
        <div className="box-image prevent-page-break">
          <img alt="image question" src={imageUrl} />
        </div>
        {example.questions.length > 0 && (
          <>
            <span className="instruction text-underline">Examples:</span>
            <div>
              {example.questions.map(
                (question: string, questionIndex: number) => {
                  const answers = example.answers[questionIndex]
                  const questionTextItem = formatHtmlText(question)?.replaceAll(
                    /\%s\%/g,
                    '______________',
                  )
                  return (
                    <div key={`${groupKey}_ex_${questionIndex}`}>
                      <span
                        className="question"
                        dangerouslySetInnerHTML={{
                          __html: questionTextItem,
                        }}
                      ></span>
                      <div className="box-answers">
                        <span className="question-text-empty-mc1"></span>
                        {answers
                          .split('*')
                          ?.map((answerItem: string, answerIndex: number) => {
                            const isCorrect =
                              example.corrects[questionIndex] ===
                              getTextWithoutFontStyle(answerItem)
                            const styleAns: CSSProperties = isCorrect
                              ? {
                                  color: '#009d12',
                                  textDecoration: 'underline',
                                }
                              : {}
                            return (
                              <div
                                key={`${groupKey}_ex_ans_${answerIndex}`}
                                className="answer"
                              >
                                <span style={styleAns}>{`${String.fromCharCode(
                                  65 + answerIndex,
                                )}. `}</span>
                                <span
                                  dangerouslySetInnerHTML={{
                                    __html: formatHtmlText(answerItem),
                                  }}
                                ></span>
                              </div>
                            )
                          })}
                      </div>
                    </div>
                  )
                },
              )}
            </div>
          </>
        )}
        {mainQuestion.questions.length > 0 && (
          <>
            <span className="instruction text-underline">Questions:</span>
            <div>
              {mainQuestion.questions.map(
                (question: string, questionIndex: number) => {
                  const answers = mainQuestion.answers[questionIndex]
                  const questionTextItem =
                    `<span style="width:32px; display: inline-block">${
                      index + questionIndex
                    } .</span>` +
                    formatHtmlText(question)?.replaceAll(
                      /\%s\%/g,
                      '______________',
                    )
                  return (
                    <div key={`${groupKey}_ques_${questionIndex}`}>
                      <span
                        className="question"
                        dangerouslySetInnerHTML={{
                          __html: questionTextItem,
                        }}
                      ></span>
                      <div className="box-answers">
                        <span className="question-text-empty-mc1"></span>
                        {answers
                          .split('*')
                          ?.map((answerItem: string, answerIndex: number) => (
                            <div
                              key={`${groupKey}_ques_ans_${answerIndex}`}
                              className="answer"
                            >
                              <span>
                                {`${String.fromCharCode(65 + answerIndex)}. `}
                              </span>
                              <span
                                dangerouslySetInnerHTML={{
                                  __html: formatHtmlText(answerItem),
                                }}
                              ></span>
                            </div>
                          ))}
                      </div>
                    </div>
                  )
                },
              )}
            </div>
          </>
        )}
      </div>
    </div>
  )
}
const MultiChoiceQuestion5 = ({ question, index }: any) => {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const groupKey = useRef(Math.random()).current
  return (
    <div className="box-question">
      <span className="instruction">
        {question.parent_question_description}
      </span>
      {question.parent_question_text && (
        <div
          className="reading-script prevent-page-break"
          dangerouslySetInnerHTML={{
            __html: formatHtmlText(question.parent_question_text),
          }}
        ></div>
      )}
      {questionList.map((ques: any, qIndex: number) => {
        const answers = answerList[qIndex].split('*')
        const question: string = ques.replaceAll(/%s%/g, '__________')
        return (
          <div key={`${groupKey}_${qIndex}`} className="multi-question">
            <span className="instruction">
              <span>{index + qIndex}.</span>
              <span
                className="instruction_question"
                dangerouslySetInnerHTML={{
                  __html: formatHtmlText(question),
                }}
              ></span>
            </span>
            <div className="box-answers vertical">
              {answers.map((answer: string, answerIndex: number) => (
                <div
                  key={`${groupKey}_${qIndex}_${answerIndex}`}
                  className="answer"
                >
                  {`${String.fromCharCode(65 + answerIndex)}. `}
                  <span
                    dangerouslySetInnerHTML={{
                      __html: formatHtmlText(answer),
                    }}
                  ></span>
                </div>
              ))}
            </div>
          </div>
        )
      })}
    </div>
  )
}

const FillInBlankQuestion1 = ({ question, index }: any) => {
  return (
    <div className="box-question">
      <span className="instruction">
        <span>{index}.</span>
        {question.question_description}
      </span>
      <span className="question">
        {question.question_text?.replace(/\%s\%/g, '___________')}
      </span>
      <span className="hint-text">{`(${question.answers})`}</span>
    </div>
  )
}

const FillInBlankQuestion2 = ({ question, index }: any) => {
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {question.image && (
        <div className="box-image prevent-page-break">
          <img src={`/upload/${question.image}`} />
        </div>
      )}
      {question.parent_question_text && (
        <div className="reading-script">{question.parent_question_text}</div>
      )}
      <span className="question">
        {question.question_text
          ?.split(/\%s\%/g)
          .map((part: string, partIndex: number) =>
            partIndex > 0
              ? `(${index + partIndex - 1})___________${part}`
              : part,
          )}
      </span>
    </div>
  )
}
const FillInBlankQuestion2_3_4_v2 = ({ question, index }: any) => {
  let questionTextHtml = ''
  question.question_text
    ?.split(/\%s\%/g)
    ?.forEach((element: string, partIndex: number) => {
      if (partIndex != 0) {
        questionTextHtml += `(${
          index + partIndex - 1
        })___________${formatHtmlText(element)}`
      } else {
        questionTextHtml += formatHtmlText(element)
      }
    })
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {question.image && (
        <div className="box-image prevent-page-break">
          <img alt="image question" src={`/upload/${question.image}`} />
        </div>
      )}
      {question.parent_question_text && (
        <div className="reading-script">{question.parent_question_text}</div>
      )}
      <span
        className="question"
        dangerouslySetInnerHTML={{
          __html: questionTextHtml,
        }}
      ></span>
    </div>
  )
}

const FillInBlankQuestion5 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {question.image && (
        <div className="box-image prevent-page-break">
          <img src={`/upload/${question.image}`} />
        </div>
      )}
      <div className="box-answers vertical">
        {Array.from(
          { length: question.total_question },
          (_, mIndex: number) => (
            <div key={`${groupKey}_${mIndex}`} className="answer">{`(${
              index + mIndex
            })___________________`}</div>
          ),
        )}
      </div>
    </div>
  )
}

const SelectFromListQuestion1 = ({ question, index }: any) => {
  const listAnswers = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const groupKey = useRef(Math.random()).current
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {question.image && (
        <div className="box-image prevent-page-break">
          <img src={`/upload/${question.image}`} />
        </div>
      )}
      <div className="question">
        {question.question_text
          ?.split(/\%s\%/g)
          .map((part: string, partIndex: number) =>
            partIndex > 0
              ? `(${index + partIndex - 1})___________${part}`
              : part,
          )}
      </div>
      {listAnswers.map((answers: string[], answersIndex: number) => (
        <div key={`${groupKey}_${answersIndex}`} className="box-answers">
          <div style={{ marginRight: '10px' }}>{`(${
            index + answersIndex
          })`}</div>
          {answers.map((answer: string, answerIndex: number) => (
            <div key={`${groupKey}_${answerIndex}`} className="answer">
              {`${String.fromCharCode(65 + answerIndex)}. ${answer}`}
            </div>
          ))}
        </div>
      ))}
    </div>
  )
}

const SelectFromListQuestion2 = ({ question, index }: any) => {
  const listImage = question.image.split('#')
  const listQuestion: string[] = question.question_text.split('#')
  const groupKey = useRef(Math.random()).current
  const listAnswers = question.answers
  .split('#')
  .map((m: string) => m.split('*'))
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {listQuestion.map((m, mIndex) => (
        <div key={`${groupKey}_${mIndex}`} style={{ marginTop: '25px' }}>
          <div className="box-image prevent-page-break">
            <img src={`/upload/${listImage[mIndex]}`} />
          </div>
          <div
            className="question"
            style={{ display: 'flex', justifyContent: 'flex-start', marginLeft: '32px' }}
          >
            <div style={{ display: 'flex' }}>
              <div>{`${index + mIndex}. `}</div>
              {m
                .split(/\%s\%/g)
                .map((part: string, partIndex: number) =>
                  partIndex > 0 ? `___________${part}` : part,
                )}
            </div>
          </div>
          <div className='answer_lists'>
          {listAnswers[mIndex].map((answer:any, aIndex:number) => (
            <div
              key={`${groupKey}_${aIndex}`}
              className="answer_item"
            >
              {`${String.fromCharCode(65 + aIndex)}. ${answer}`}
            </div>
          ))}
          </div>
        </div>
      ))}
    </div>
  )
}

const DragAndDropQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const answers = question.answers.split('#')
  const correctAnswers = question.correct_answers.split('#')
  const images = question.image?.split('#') || ''
  const checkExam = question.question_text?.split('#') || ''

  const example: any = {
    answers: [],
    corrects: [],
    images: [],
  }
  const mainQuestion: any = {
    answers: [],
    corrects: [],
    images: [],
  }
  for (let i = 0; i < answers.length; i++) {
    if (answers[i]) {
      if (!checkExam ||checkExam[i] === '') {
        mainQuestion.corrects.push(correctAnswers[i])
        mainQuestion.answers.push(answers[i])
        mainQuestion.images.push(images[i])
        continue
      } else {
        example.corrects.push(correctAnswers[i])
        example.answers.push(answers[i])
        example.images.push(images[i])
        continue
      }
    }
  }

  return (
    <div className="box-question box-question-export-dd1">
      <span className="instruction">
        {/* <span>{index}.</span> */}
        {question.parent_question_description}
      </span>
      {example.answers.length > 0 && (
        <>
          <span className="instruction text-underline">Examples:</span>
          <div>
            {example.answers.map((answer: string, answerIndex: number) => {
              const correct = example.corrects[answerIndex]
              const imageUrl = `/upload/${example.images[answerIndex]}`
              return (
                <div
                  className="line-a-question prevent-page-break"
                  key={`${groupKey}_${answerIndex}_ex`}
                >
                  <div className="question ">
                    <div className="line-a-question__content">
                      <span className="line-a-question__content__text-index"></span>
                      <div
                        className={`box-image-export-dd1 ${example.images[answerIndex] ? '' : 'no-image-data'} `}
                      >
                        <img alt="" src={imageUrl} />
                      </div>
                      <div className="answer-export-dd1">
                        <span className="question">
                          {`Keywords: ${answer?.replace(/\*/g, '/')}`}
                        </span>
                        <div className="box-answers">
                          {`→ ${correct?.replace(/\*/g, ' ')}`}
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </>
      )}
      {mainQuestion.answers.length > 0 && (
        <>
          <span className="instruction text-underline">Questions:</span>
          <div>
            {mainQuestion.answers.map((answer: string, answerIndex: number) => {
              const imageUrl = `/upload/${mainQuestion.images[answerIndex]}`
              return (
                <div
                  className="line-a-question prevent-page-break"
                  key={`${groupKey}_${answerIndex}_ex`}
                >
                  <div className="question ">
                    <div className="line-a-question__content">
                      <span className="line-a-question__content__text-index">{`${
                        index + answerIndex
                      }.`}</span>
                      <div className={`box-image-export-dd1 ${example.images[answerIndex] ? '' : 'no-image-data'} `}>
                        <img alt="" src={imageUrl} />
                      </div>
                      <div className="answer-export-dd1">
                        <span className="question">
                          {`Keywords: ${answer?.replace(/\*/g, '/')}`}
                        </span>
                        <div className="box-answers">
                          → _______________________________________
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )
            })}
          </div>
        </>
      )}
    </div>
  )
}

const ShortAnswerQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  return (
    <div className="box-question">
      <span className="instruction">
        <span>{index}.</span>
        {question.question_description}
      </span>
      <span className="question">
        {question.question_text
          ?.split(/\%u\%/g)
          .map((part: string, partIndex: number) => {
            if (partIndex % 2 === 1) {
              return <u key={`${groupKey}_${partIndex}`}>{part}</u>
            } else {
              return part
            }
          })}
      </span>
      <div className="box-answers">
        → {`${question.answers ?? '...........'}....................`}
      </div>
    </div>
  )
}
const TrueFalseQuestion3 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const answers = question.answers.split('#')
  const correctAnswers = question.correct_answers.split('#')
  const image = question.image
  const imageUrl = `/upload/${image}`
  const example: any = {
    answers: [],
    corrects: [],
  }
  const mainQuestion: any = {
    answers: [],
    corrects: [],
  }

  //cup example
  for (let i = 0; i < correctAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        mainQuestion.corrects.push(correctAnswers[i])
        mainQuestion.answers.push(answers[i])
        continue
      } else {
        example.corrects.push(correctAnswers[i])
        example.answers.push(answers[i].replace('*', ''))
        continue
      }
    }
  }
  return (
    <div className="box-question box-question-export-tf34">
      <span className="instruction">{question.question_description}</span>
      <div>
        <div className="box-image prevent-page-break">
          <img alt="" src={imageUrl} />
        </div>
        {example.answers.length > 0 && (
          <>
            <span className="instruction text-underline">Examples:</span>
            <div>
              {example.answers.map((answer: string, answerIndex: number) => {
                const correct = example.corrects[answerIndex]
                return (
                  <div
                    className="line-a-question prevent-page-break"
                    key={`${groupKey}_${answerIndex}_ex`}
                  >
                    <div className="question ">
                      <span></span>
                      <span>{answer}</span>
                    </div>
                    <div className="line-a-question__true-false">
                      <span className="line-a-question__true-false__checkbox">{`${
                        correct == 'T' ? '☑' : '☐'
                      }`}</span>
                      <label className="text-tf">True</label>
                      <span className="line-a-question__true-false__checkbox">{`${
                        correct == 'F' ? '☑' : '☐'
                      }`}</span>
                      <label className="text-tf">False</label>
                    </div>
                  </div>
                )
              })}
            </div>
          </>
        )}
        {mainQuestion.answers.length > 0 && (
          <>
            <span className="instruction text-underline">Questions:</span>
            <div>
              {mainQuestion.answers.map(
                (answer: string, answerIndex: number) => {
                  return (
                    <div
                      className="line-a-question prevent-page-break"
                      key={`${groupKey}_${answerIndex}_ex`}
                    >
                      <div className="question">
                        <span></span>
                        <span>{`${index + answerIndex}. ${answer}`}</span>
                      </div>
                      <div className="line-a-question__true-false">
                        <span className="line-a-question__true-false__checkbox">{`☐`}</span>
                        <label className="text-tf">True</label>
                        <span className="line-a-question__true-false__checkbox">{`☐`}</span>
                        <label className="text-tf">False</label>
                      </div>
                    </div>
                  )
                },
              )}
            </div>
          </>
        )}
      </div>
    </div>
  )
}
const TrueFalseQuestion4 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const answers = question.answers.split('#')
  const correctAnswers = question.correct_answers.split('#')
  const images = question.image.split('#')
  const example: any = {
    answers: [],
    corrects: [],
    images: [],
  }
  const mainQuestion: any = {
    answers: [],
    corrects: [],
    images: [],
  }

  //cup example
  for (let i = 0; i < correctAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        mainQuestion.corrects.push(correctAnswers[i])
        mainQuestion.answers.push(answers[i])
        mainQuestion.images.push(images[i])
        continue
      } else {
        example.corrects.push(correctAnswers[i])
        example.answers.push(answers[i].replace('*', ''))
        example.images.push(images[i])
        continue
      }
    }
  }
  return (
    <div className="box-question box-question-export-tf34">
      <span className="instruction">{question.question_description}</span>
      <div>
        {example.answers.length > 0 && (
          <>
            <span className="instruction text-underline">Examples:</span>
            <div>
              {example.answers.map((answer: string, answerIndex: number) => {
                const correct = example.corrects[answerIndex]
                const imageUrl = `/upload/${example.images[answerIndex]}`
                return (
                  <div
                    className="line-a-question prevent-page-break"
                    key={`${groupKey}_${answerIndex}_ex`}
                  >
                    <div className="question ">
                      <div className="line-a-question__content">
                        <span className="line-a-question__content__text-index"></span>
                        <div className="box-image-export-tf">
                          <img alt="" src={imageUrl} />
                        </div>
                        <span>{answer}</span>
                      </div>
                    </div>
                    <div className="line-a-question__true-false">
                      <span className="line-a-question__true-false__checkbox">{`${
                        correct == 'T' ? '☑' : '☐'
                      }`}</span>
                      <label className="text-tf">True</label>
                      <span className="line-a-question__true-false__checkbox">{`${
                        correct == 'F' ? '☑' : '☐'
                      }`}</span>
                      <label className="text-tf">False</label>
                    </div>
                  </div>
                )
              })}
            </div>
          </>
        )}
        {mainQuestion.answers.length > 0 && (
          <>
            <span className="instruction text-underline">Questions:</span>
            <div>
              {mainQuestion.answers.map(
                (answer: string, answerIndex: number) => {
                  const imageUrl = `/upload/${mainQuestion.images[answerIndex]}`
                  return (
                    <div
                      className="line-a-question prevent-page-break"
                      key={`${groupKey}_${answerIndex}_ex`}
                    >
                      <div className="question ">
                        <div className="line-a-question__content">
                          <span className="line-a-question__content__text-index">{`${
                            index + answerIndex
                          }.`}</span>
                          <div className="box-image-export-tf">
                            <img alt="" src={imageUrl} />
                          </div>
                          <span>{`${answer}`}</span>
                        </div>
                      </div>
                      <div className="line-a-question__true-false">
                        <span className="line-a-question__true-false__checkbox">{`☐`}</span>
                        <label className="text-tf">True</label>
                        <span className="line-a-question__true-false__checkbox">{`☐`}</span>
                        <label className="text-tf">False</label>
                      </div>
                    </div>
                  )
                },
              )}
            </div>
          </>
        )}
      </div>
    </div>
  )
}
const TrueFalseQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const listAnswer: string[] = question.answers.split('#')
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {question.question_text && (
        <div className="reading-script prevent-page-break">
          {question.question_text}
        </div>
      )}
      <div className="answer-table">
        <table>
          <thead>
            <tr>
              <th>Answers</th>
              <th>True</th>
              <th>False</th>
            </tr>
          </thead>
          <tbody>
            {listAnswer.map((answer, answerIndex) => [
              <tr key={`${groupKey}_${answerIndex}`}>
                <td>{`${index + answerIndex}. ${answer}`}</td>
                <td>
                  <div className="radio"></div>
                </td>
                <td>
                  <div className="radio"></div>
                </td>
              </tr>,
              answerIndex < listAnswer.length - 1 && (
                <tr
                  key={`${groupKey}_${answerIndex}_divide`}
                  className="divide"
                >
                  <td colSpan={3}>
                    <div></div>
                  </td>
                </tr>
              ),
            ])}
          </tbody>
        </table>
      </div>
    </div>
  )
}

const MatchingGameQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length)
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {question.question_text && (
        <div className="reading-script prevent-page-break">
          {question.question_text}
        </div>
      )}
      <table className="matching-table">
        <tbody>
          {Array.from({ length: maxLength }, (_, _index) => (
            <tr key={`${groupKey}_${_index}`}>
              <td>
                {listAnswer[0][_index] &&
                  `${index + _index}. ${listAnswer[0][_index]}`}
              </td>
              <td>
                {listAnswer[1][_index] &&
                  `${String.fromCharCode(97 + _index)}. ${
                    listAnswer[1][_index]
                  }`}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

const MultiResponseQuestion1 = ({ question, index }: any) => {
  const answers: string[] = question.answers.split('#')
  const groupKey = useRef(Math.random()).current

  return (
    <div className="box-question">
      <span className="instruction">
        <span>{index}.</span>
        {question.question_description}
      </span>
      {question.question_text && (
        <span className="question">
          {question.question_text?.replace(/\%s\%/g, '______________')}
        </span>
      )}
      <div className="box-answers">
        {answers.map((answer, answerIndex) => (
          <div
            key={`${groupKey}_${answerIndex}`}
            style={{ width: '25%', padding: 5 }}
          >{`${String.fromCharCode(65 + answerIndex)}. ${answer}`}</div>
        ))}
      </div>
    </div>
  )
}

const MultiResponseQuestion3 = ({ question, index }: any) => {
  const answers: string[] = question.answers.split('#')
  const groupKey = useRef(Math.random()).current

  return (
    <div className="box-question">
      <span className="instruction">
        <span>{index}.</span>
        {question.question_description}
      </span>
      {question.question_text && (
        <span className="question">
          {question.question_text?.replace(/\%s\%/g, '______________')}
        </span>
      )}
      <div className="box-answers vertical">
        {answers.map((answer, answerIndex) => (
          <div
            key={`${groupKey}_${answerIndex}`}
            style={{ padding: 5 }}
          >{`${String.fromCharCode(65 + answerIndex)}. ${answer}`}</div>
        ))}
      </div>
    </div>
  )
}

const LikertScaleQuestion = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const listAnswer: string[] = question.answers.split('#')
  return (
    <div className="box-question">
      <span className="instruction">{question.question_description}</span>
      {question.question_text && (
        <div className="reading-script prevent-page-break">
          {question.question_text}
        </div>
      )}
      <div className="answer-table">
        <table>
          <thead>
            <tr>
              <th>Answers</th>
              <th>True</th>
              <th>False</th>
              <th>Not given</th>
            </tr>
          </thead>
          <tbody>
            {listAnswer.map((answer, answerIndex) => [
              <tr key={`${groupKey}_${answerIndex}`}>
                <td>{`${index + answerIndex}. ${answer}`}</td>
                <td>
                  <div className="radio"></div>
                </td>
                <td>
                  <div className="radio"></div>
                </td>
                <td>
                  <div className="radio"></div>
                </td>
              </tr>,
              answerIndex < listAnswer.length - 1 && (
                <tr
                  key={`${groupKey}_${answerIndex}_divide`}
                  className="divide"
                >
                  <td colSpan={4}>
                    <div></div>
                  </td>
                </tr>
              ),
            ])}
          </tbody>
        </table>
      </div>
    </div>
  )
}

const MIXQuestion1 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const listAnswers: string[] = question.answers.split('[]')
  const tfAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction: string[] = question.question_description.split('[]')
  const mcQuestions: string[] = question.question_text.split('[]')[1].split('#')
  return (
    <div className="box-question">
      <span className="instruction">
        {question.parent_question_description}
      </span>
      <div className="reading-script prevent-page-break">
        {question.parent_question_text}
      </div>
      <span
        className="instruction"
        style={{ marginTop: '20px' }}
      >{`Task 1: ${listInstruction[0]}`}</span>
      <div className="answer-table">
        <table>
          <thead>
            <tr>
              <th>Answers</th>
              <th>True</th>
              <th>False</th>
            </tr>
          </thead>
          <tbody>
            {tfAnswers.map((answer, answerIndex) => [
              <tr key={`${groupKey}_${answerIndex}`}>
                <td>{`${index + answerIndex}. ${answer}`}</td>
                <td>
                  <div className="radio"></div>
                </td>
                <td>
                  <div className="radio"></div>
                </td>
              </tr>,
              answerIndex < tfAnswers.length - 1 && (
                <tr
                  key={`${groupKey}_${answerIndex}_divide`}
                  className="divide"
                >
                  <td colSpan={3}>
                    <div></div>
                  </td>
                </tr>
              ),
            ])}
          </tbody>
        </table>
      </div>
      <span
        className="instruction"
        style={{ marginTop: '20px' }}
      >{`Task 2: ${listInstruction[1]}`}</span>
      {mcQuestions.map((ques: any, qIndex: number) => {
        const answers = mcAnswers[qIndex].split('*')
        return (
          <div key={`${groupKey}_${qIndex}`} className="multi-question">
            <span className="instruction">
              <span>{index + tfAnswers.length + qIndex}.</span>
              {ques.replace(/\%s\%/g, '______________')}
            </span>
            <div className="box-answers vertical">
              {answers.map((answer: string, answerIndex: number) => (
                <div
                  key={`${groupKey}_${qIndex}_${answerIndex}`}
                  className="answer"
                >{`${String.fromCharCode(65 + answerIndex)}. ${answer}`}</div>
              ))}
            </div>
          </div>
        )
      })}
    </div>
  )
}

const MIXQuestion2 = ({ question, index }: any) => {
  const groupKey = useRef(Math.random()).current
  const listAnswers: string[] = question.answers.split('[]')
  const lsAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction: string[] = question.question_description.split('[]')
  const mcQuestions: string[] = question.question_text.split('[]')[1].split('#')
  return (
    <div className="box-question">
      <span className="instruction">
        {question.parent_question_description}
      </span>
      <div className="reading-script prevent-page-break">
        {question.parent_question_text}
      </div>
      <span
        className="instruction"
        style={{ marginTop: '20px' }}
      >{`Task 1: ${listInstruction[0]}`}</span>
      <div className="answer-table">
        <table>
          <thead>
            <tr>
              <th>Answers</th>
              <th>True</th>
              <th>False</th>
              <th>Not given</th>
            </tr>
          </thead>
          <tbody>
            {lsAnswers.map((answer, answerIndex) => [
              <tr key={`${groupKey}_${answerIndex}`}>
                <td>{`${index + answerIndex}. ${answer}`}</td>
                <td>
                  <div className="radio"></div>
                </td>
                <td>
                  <div className="radio"></div>
                </td>
                <td>
                  <div className="radio"></div>
                </td>
              </tr>,
              answerIndex < lsAnswers.length - 1 && (
                <tr
                  key={`${groupKey}_${answerIndex}_divide`}
                  className="divide"
                >
                  <td colSpan={4}>
                    <div></div>
                  </td>
                </tr>
              ),
            ])}
          </tbody>
        </table>
      </div>
      <span
        className="instruction"
        style={{ marginTop: '20px' }}
      >{`Task 2: ${listInstruction[1]}`}</span>
      {mcQuestions.map((ques: any, qIndex: number) => {
        const answers = mcAnswers[qIndex].split('*')
        return (
          <div key={`${groupKey}_${qIndex}`} className="multi-question">
            <span className="instruction">
              <span>{index + lsAnswers.length + qIndex}.</span>
              {ques.replace(/\%s\%/g, '______________')}
            </span>
            <div className="box-answers vertical">
              {answers.map((answer: string, answerIndex: number) => (
                <div
                  key={`${groupKey}_${qIndex}_${answerIndex}`}
                  className="answer"
                >{`${String.fromCharCode(65 + answerIndex)}. ${answer}`}</div>
              ))}
            </div>
          </div>
        )
      })}
    </div>
  )
}
