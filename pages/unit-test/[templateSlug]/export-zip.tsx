import { useState } from 'react'

import { GetServerSideProps } from 'next'
import { Wrapper } from 'components/templates/wrapper'
import { BreadCrumbNav } from 'components/molecules/breadCrumbNav'
import { UnitTestExportContainer } from 'components/organisms/UnitTestExportContainer'
import ChatHelp from 'components/chatHelp'
import { USER_GUIDE_SCREEN_ID } from '@/interfaces/constants'

export default function TemplateDetailPage({ id }: any) {
  const userGuideScreenId = USER_GUIDE_SCREEN_ID.Export_UnitTest;
  const [unitTestName, setUnitTestName] = useState("Xuất đề thi")
  const [breadcrumbData, SetBreadcrumbData] = useState([
    { name: 'Tổng quan', url: '/' },
    { name: 'Danh sách đề thi', url: '/unit-test' },
    {
      name: 'Xuất đề thi',
      url: null,
    },
  ])
  
  return (
    <>
        <Wrapper
      pageTitle={unitTestName}
      className="p-export-unit-test__container"
    >
      <div
        className="p-detail-format__breadcrumb"
        style={{ fontFamily: 'Montserrat Regular' }}
      >
        <BreadCrumbNav data={breadcrumbData} />
      </div>
      <UnitTestExportContainer id={id} 
      setUnitTestName={setUnitTestName}
      SetBreadcrumbData={SetBreadcrumbData} 
      breadcrumbData={breadcrumbData}></UnitTestExportContainer>
    </Wrapper>
     <ChatHelp screenId={userGuideScreenId}></ChatHelp></>
  )
}

export const getServerSideProps: GetServerSideProps = async (context) => {
  const { query } = context

  return { props: { id: query.templateSlug } }
}
