import React, { useContext, useEffect, useMemo, useRef, useState } from 'react'

import classNames from 'classnames'
import { useSession } from 'next-auth/client'
import { useRouter } from 'next/router'
import { Collapse } from 'react-collapse'
import { Button, Tooltip, Whisper } from 'rsuite'

import ArrowLeftBtn from '@/assets/icons/arrow-left-v2.svg'
import BackgroundBlurBottom from '@/componentsV2/Common/BackgroundBlurBottom'
import DrawerFilter from '@/componentsV2/Dashboard/FilterSidebar/DrawerFilter'
import NoFieldData from '@/componentsV2/NoDataSection/NoFieldData'
import { IS_INTERNATIONAL_EXAM } from '@/constants/index'
import useTranslation from '@/hooks/useTranslation'
import { USER_ROLES } from '@/interfaces/constants'
import { AppContext } from '@/interfaces/contexts'
import { SKILLS_SELECTIONS, UNIT_TYPE } from '@/interfaces/struct'
import { delayResult, userInRight } from '@/utils/index'
import { callApi } from 'api/utils'
import { InputWithLabel } from 'components/atoms/inputWithLabel'
import { SelectPicker } from 'components/atoms/selectPicker'
import { MultiSelectPicker } from 'components/atoms/selectPicker/multiSelectPicker'
import UnitTestGrid from 'components/molecules/unitTestTable/UnitTestGrid'
import { Wrapper } from 'components/templates/wrapper'

import styles from './SeriesDetailPage.module.scss';

export default function SeriesDetailPage() {
	const router = useRouter()
	const {t} = useTranslation()
	const pageTitle = router.query.m
    ? `Đề thi của ${router.query.m === 'mine' ? 'tôi' : 'giáo viên'}`
    : t(`${t('left-menu')["list-unit-tests"]}`)

	return (
		<Wrapper
			pageTitle={pageTitle}
			hasStickyFooter={false}
		>
			{/* <WrapperUnitTestV2 childrens={[RenderUI()]}/> */}
			<RenderUI />
		</Wrapper>
	)
}
const RenderUI = () => {
	const router = useRouter()
	const contentRef = useRef<HTMLDivElement>(null)
	const [title, setTitle] = useState('')
    
	const { isReady, query, push, back } = router;

	const handleBack = () => {
		back()
	}
	const filter = useMemo(() => {
		if(isReady){
			setTitle(query?.display?.toString())
		}
	}, [ isReady, query])
	

	return (
		<div className={styles.wrapperSeriesDetail}>
			<div className={styles.titlePage}>
				<div onClick={handleBack}>
					<ArrowLeftBtn />
				</div>
				<span>{title}</span>
			</div>
			<div className={styles.boxContent} ref={contentRef}>
				<BackUpUI />
			</div>
		</div>
	)
}

const BackUpUI = () => {
	const router = useRouter()
	const {t} = useTranslation()
	const { query } = router;
	const seriesStart =useRef( query.series_id ? parseInt(query.series_id as string):null);
	const InExamMode = isNaN(Number(query.idI))
	const idInExam = InExamMode ? [] as number[] : [Number(query.idI)] 
	const [session] = useSession()
	const isImportable = userInRight([USER_ROLES.Operator], session)
	const isSystem = !router.query.m
	const isCreatable = isSystem === isImportable

	const [unitTestList, setUnitTestList] = useState([] as any[])
	const [currentPage, setCurrentPage] = useState(1)
	const [totalPage, setTotalPage] = useState(1)

	const [isOpenFilterCollapse, setIsOpenFilterCollapse] = useState(true)
	const [triggerReset, setTriggerReset] = useState(false)

	const [filterName, setFilterName] = useState('')
	const [filterTime, setFilterTime] = useState('')
	const [filterTotalQuestion, setFilterTotalQuestion] = useState('')
	const [filterSkill, setFilterSkill] = useState([] as any[])
	const [filterStartDate, setFilterStartDate] = useState(null as any)
	const [filterEndDate, setFilterEndDate] = useState(null as any)
	const [filterStatus, setFilterStatus] = useState('' as '' | 'AC' | 'DI')
	const [filterUnitType, setFilterUnitType] = useState('')
    const [filterUnitTestTypes, setFilterUnitTestTypes] = useState(idInExam);

	const [searching, setSearching] = useState(false)

	const { isExpandSidebar } = useContext(AppContext)
	const unitLimit = 60
	const [isFilter, setIsFilter] = useState(false)
	const [isLoading, setIsLoading] = useState(true)
	const handleNameChange = (val: string) => setFilterName(val)
	const handleNameSubmit = (e: any, val: string) => {
		if (e.keyCode === 13) {
			// handleFilterSubmit(false, { name: val })
			e.target.blur()
		}
	}
	const handleUnitTypeChange = (val:string) => setFilterUnitType(val)

	const handleTimeChange = (val: string) => setFilterTime(!isNaN(Number(val)) ? val : '')
	const handleTimeSubmit = (e: any, val: string) => {
		if (e.keyCode === 13) {
			// handleFilterSubmit(false, { time: val })
			e.target.blur()
		}
	}
	const handleTotalQuestionChange = (val: string) => setFilterTotalQuestion(!isNaN(Number(val)) ? val : '')
	const handleTotalQuestionSubmit = (e: any, val: string) => {
		if (e.keyCode === 13) {
			// handleFilterSubmit(false, { totalQuestion: val })
			e.target.blur()
		}
	}
	const handleSkillChange = (val: any[]) => {
		setFilterSkill([...val])
		// handleFilterSubmit(false, { skills: [...val] })
	}
	
	const collectData = (isReset: boolean, page = 0, body: any = null) => {
		const collection: any = { page, series_id: [seriesStart.current], limit: unitLimit }
		if(!InExamMode){
			collection['is_international_exam'] = IS_INTERNATIONAL_EXAM
			collection['unit_test_types'] = idInExam || filterUnitTestTypes
			delete collection.series_id
		}
		if (!isReset) {
			if (filterName || body?.name) collection['name'] = body?.name || filterName
			
			if(filterTime !== '' || body?.time) 
				collection['time'] = body?.time || filterTime
			if (filterTotalQuestion !== '' || body?.totalQuestion)
				collection['total_question'] = body?.totalQuestion || filterTotalQuestion
			if ((filterSkill && filterSkill.length > 0) || (body?.skills && body.skills.length > 0))
				collection['skills'] = body?.skills || filterSkill
			if (filterStartDate || body?.startDate) collection['start_date'] = body?.startDate || filterStartDate
			if (filterEndDate || body?.endDate) collection['end_date'] = body?.endDate || filterEndDate
			if (filterStatus || body?.status) collection['status'] = body?.status === '' ? '' : body?.status || filterStatus
			if(filterUnitType || body?.unit_type) collection['unit_type'] = body?.unit_type === '' ? '' : body?.unit_type || filterUnitType
			if (filterUnitTestTypes.length || body?.unit_test_types) {
                collection['unit_test_types'] = body?.unit_test_types === '' ? '' : body?.unit_test_types || filterUnitTestTypes
            }
		}
		return collection
	}

	const handleItemDelete = async (id: number) => {
		const body = collectData(false, currentPage - 1)
		setIsFilter(true)
		const response = await callApi(`/api/unit-test/${id}`, 'delete')
		if (response) {
			const scope = router.query?.m ? ['mine', 'teacher'].findIndex((item) => item === router.query.m) : null
			const newUnitTest: any = await callApi(`/api/unit-test/search?page=${body.page}&limit=${unitLimit}`, 'post', 'Token', {
				...body,
				scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
			})
			if (newUnitTest?.unitTests?.length > 0) {
				setUnitTestList([...newUnitTest.unitTests])
				setTotalPage(newUnitTest?.totalPages)
			} else {
				handlePageChange(newUnitTest?.currentPage > 0 ? newUnitTest?.currentPage : 1)
			}
		}
	}
	const wrapperDiv: HTMLDivElement = document.querySelector('.t-wrapper__main')
	const scrollToTop = () => {
		if (wrapperDiv){
			wrapperDiv.scrollTop = 0
			wrapperDiv.style.scrollBehavior = 'smooth'
		}
      
	}

	const handleFilterSubmit = async (isReset: boolean, customData: any = null) => {
		const body = collectData(isReset, 0, customData)
		setIsFilter(!isReset)
		setIsLoading(true)
		const scope = router.query?.m ? ['mine', 'teacher'].findIndex((item) => item === router.query.m) : null
		const response: any = await delayResult(callApi(`/api/unit-test/search?page=${body.page}&limit=${unitLimit}`, 'post', 'Token', {
			...body,
			scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
		}), 400)
		setIsLoading(false)
		if (response) {
			scrollToTop()
			if (response?.unitTests) setUnitTestList([...response.unitTests])
			if (response?.currentPage) setCurrentPage(response.currentPage + 1 > 0 ? response.currentPage + 1 : 1)
			if (response?.totalPages) setTotalPage(response.totalPages)
			setSearching(!isReset)
		}
	}

	const handlePageChange = async (page: number) => {
		const body = collectData(false, page - 1)
		setIsLoading(true)
		setIsFilter(true)
		const scope = router.query?.m ? ['mine', 'teacher'].findIndex((item) => item === router.query.m) : null
		const response: any = await delayResult(callApi(`/api/unit-test/search?page=${body.page}&limit=${unitLimit}`, 'post', 'Token', 
			{
				...body,
				scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
			}), 400)
		setIsLoading(false)
		if (response) {
			scrollToTop()
			if (response?.unitTests) setUnitTestList([...response.unitTests])
			if (response?.currentPage || response.currentPage === 0) setCurrentPage(response.currentPage + 1)
			if (response?.totalPages) setTotalPage(response.totalPages)
		} 
	}

	const resetFilterState = () => {
		setFilterTime('')
		setFilterTotalQuestion('')
		setFilterSkill([])
		setFilterStartDate('')
		setFilterEndDate('')
		setFilterStatus('')
		setFilterName('')
		setFilterUnitType('')
		handleFilterSubmit(true)
		setCurrentPage(1)
        setFilterUnitTestTypes([]);
	}
	
	useEffect(() => {
		const fetchData = async () => {
			const scope = router.query?.m ? ['mine', 'teacher'].findIndex((item) => item === router.query.m) : null
			const bodyData:any = {
				page: 0,
				scope: (scope && scope !== -1) || scope === 0 ? scope + 1 : null,
				isSystem: router.query?.m === 'mine' ? false : true,
			}
			if(seriesStart.current){
				bodyData.series_id = [seriesStart.current]
			}
			if(!InExamMode){
				bodyData.is_international_exam = IS_INTERNATIONAL_EXAM
				bodyData.unit_test_types = filterUnitTestTypes
				delete bodyData.series_id
			}
			const response: any = await callApi(`/api/unit-test/search?page=${bodyData.page}&limit=${unitLimit}`, 'post', 'Token', bodyData)
			if (response?.unitTests) setUnitTestList([...response.unitTests])
			setCurrentPage(1)
			if (response?.totalPages) setTotalPage(response.totalPages)
			if (response) setIsLoading(false)
			setSearching(false)
		}
		setTriggerReset(!triggerReset)
		resetFilterState()
		fetchData()
	}, [router.asPath])

	const isSearchEmpty = useMemo(() => {
		if (!unitTestList || unitTestList.length === 0) {
			return searching
		}
		return true
	}, [searching])

	const onKeyDownInputNumber = (e:any,val:any)=>{
		const key = e?.key?.toLowerCase();
		if(["e","+","-"].includes(key)){
			e.preventDefault();
			return;
		}
		if(!val && key == "0"){
			e.preventDefault();
			return;
		}
	}

	const submitSearch = () => {
        setCurrentPage(1)
        setSearching(true)
        handleFilterSubmit(false)
    }

	const submitRightFilterSearch = (unit_test_types: any[]) => {
        setCurrentPage(1)
        setSearching(true)
        setFilterUnitTestTypes(unit_test_types)
        handleFilterSubmit(false, { unit_test_types: unit_test_types })
    }

	return (
        <>
            {InExamMode && <DrawerFilter
                filterUnitTestTypes={filterUnitTestTypes}
                onSubmit={submitRightFilterSearch}
				onReset={resetFilterState}
            />}
            <div className={`m-unittest-template-table-data`}>
				<div className="m-unittest-template-table-data__content">
					<Collapse isOpened={isOpenFilterCollapse}>
						<div className="content__sub">
							<InputWithLabel
								className="__filter-box --search --xl"
								label={t('common')['unit-test-name']}
								placeholder={t('common')["placeholder-unitTest-name"]}
								triggerReset={triggerReset}
								type="text"
								onBlur={handleNameChange}
								onKeyUp={handleNameSubmit}
								icon={<img src="/images/icons/ic-search-dark.png" alt="search" />}
							/>
							<InputWithLabel
								className="__filter-box"
								decimal={0}
								label={t('common')['time']}
								min={0}
								triggerReset={triggerReset}
								type="number"
								onkeyDown={onKeyDownInputNumber}
								onBlur={handleTimeChange}
								onKeyUp={handleTimeSubmit}
							/>
							<InputWithLabel
								className="__filter-box"
								decimal={0}
								label={t('common')['total-question']}
								min={0}
								triggerReset={triggerReset}
								type="number"
								onkeyDown={onKeyDownInputNumber}
								onBlur={handleTotalQuestionChange}
								onKeyUp={handleTotalQuestionSubmit}
							/>
							<MultiSelectPicker
								className="__filter-box"
								data={SKILLS_SELECTIONS}
								label={t('common')['skill']}
								triggerReset={triggerReset}
								onChange={handleSkillChange}
							/>
							<SelectPicker
								className="__filter-box"
								data={UNIT_TYPE.map((item)=> ({...item, display: t("unit-type")[item.key]}))}
								label={t('common')["unit-type"]}
								triggerReset={triggerReset}
								onChange={handleUnitTypeChange}
							/>
						</div>
					</Collapse>
					<div className="content__sup">
					<div className="__action-group"></div>
					<div className="__input-group">
						{isCreatable && (
							<Button
								className="__action-btn"
								style={{ margin: '0 0.8rem 0 0.8rem' }}
								onClick={() => {
									router.push({
										pathname: '/unit-test/create-new-unittest',
										query: router.query.m ? { m: router.query.m } : {},
									})
								}}
							>
								<img src="/images/icons/ic-plus-white.png" alt="create" />
								<span>{t(`${t('common')['create']}`)}</span>
							</Button>
						)}
						<Whisper placement="bottom" trigger="hover" speaker={<Tooltip>{t(`${t('common-whisper')['default']}`)}</Tooltip>}>
							<Button
								className="__sub-btn"
								onClick={() => {
									setTriggerReset(!triggerReset)
									resetFilterState()
									setSearching(false)
								}}
							>
								<img src="/images/icons/ic-reset-darker.png" alt="reset" />
								<span>{t('common')['default']}</span>
							</Button>
						</Whisper>
						<Button
							className="__main-btn"
							onClick={submitSearch}
						>
							<img src="/images/icons/ic-search-dark.png" alt="find" />
							<span>{t('common')['search']}</span>
						</Button>
					</div>
					</div>
				</div>
				{unitTestList.length > 0 || isLoading ? (
					<UnitTestGrid 
						currentPage={currentPage}
						totalPage={totalPage}
						onChangePage={(page: number) => handlePageChange(page)}
						data={unitTestList}
						isLoading={isLoading}
						setData={setUnitTestList}
						onDelete={handleItemDelete}
					/>		
				) : (
					<NoFieldData
						field='series_detail'
						hasAction={false}
						isSearchEmpty={isSearchEmpty}
					/>
            )}
			{!isLoading && unitTestList.length > 0 &&
				<BackgroundBlurBottom 
					defaultRef={wrapperDiv}
					paddingBottom={50}
					className={classNames(styles.wrapperBlur, {
						[styles.compact]: !isExpandSidebar.state,
					})}
				/>
			}
            </div>
        </>
	)
}
