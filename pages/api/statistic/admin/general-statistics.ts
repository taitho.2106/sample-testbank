import { NextApiRequest, NextApiResponse } from "next";

import { commonDeleteStatus } from '@/constants/index';
import { USER_ROLES } from "@/interfaces/constants";
import createHandler, { apiResponseDTO, roleConfig } from "lib/apiHandle";
import prisma from 'lib/prisma';

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [roleConfig.admin],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {

            const [totalTeachers, totalStudents, totalUnitTestNotIE, totalUnitTestIE, totalUnitTestNotIEPublished, totalUnitTestIEPublished] = await prisma.$transaction([
                prisma.user.count({
                    where: {
                        deleted: commonDeleteStatus.ACTIVE,
                        user_role_id: USER_ROLES.Teacher
                    }
                }),
                prisma.user.count({
                    where: {
                        deleted: commonDeleteStatus.ACTIVE,
                        user_role_id: USER_ROLES.Student
                    }
                }),
                prisma.unit_test.count({
                    where: {
                        deleted: commonDeleteStatus.ACTIVE,
                        scope: 0,
                        unit_test_origin_id: null,
                        is_international_exam: 0,
                        series_id: {
                            not: null
                        }
                    }
                }),
                prisma.unit_test.count({
                    where: {
                        deleted: commonDeleteStatus.ACTIVE,
                        scope: 0,
                        unit_test_origin_id: null,
                        is_international_exam: 1
                    }
                }),
                prisma.unit_test.count({
                    where: {
                        deleted: commonDeleteStatus.ACTIVE,
                        scope: 0,
                        privacy: 0,
                        unit_test_origin_id: null,
                        is_international_exam: 0,
                        series_id: {
                            not: null
                        }
                    }
                }),
                prisma.unit_test.count({
                    where: {
                        deleted: commonDeleteStatus.ACTIVE,
                        scope: 0,
                        privacy: 0,
                        unit_test_origin_id: null,
                        is_international_exam: 1
                    }
                })
            ])
            return res.status(200).json(apiResponseDTO(true, {
                user: {
                    totalTeachers,
                    totalStudents,
                },
                unit_test: {
                    totalUnitTestNotIE,
                    totalUnitTestNotIEPublished,
                    totalUnitTestIE,
                    totalUnitTestIEPublished
                }
            }));
        }
    },
});

export default handler