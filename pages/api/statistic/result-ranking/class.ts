import { Prisma } from "@prisma/client";
import createHandler, { apiResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";

export const getPointRate = (testItem: any) => {
    if (!testItem?.point) {
        return 0
    }
    return testItem.point / testItem.max_point
}

export const getStudentTestKey = (studentItem: any) => {
    if (!studentItem.id || !studentItem.user_id) {
        return "error_key"
    }

    if (studentItem.only_student_id) {
        return `${studentItem.user_id}::student::${studentItem.id}`
    }

    if (studentItem.only_class_group_id) {
        return `${studentItem.user_id}::${studentItem.only_class_group_id}::${studentItem.id}`
    }

    return `${studentItem.user_id}::${studentItem.id}`
}

export const sumValues: any = (obj: any) => Object.values(obj).reduce((a: any, b: any) => (a + b), 0);

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher = session.user;
            const unit_type = req?.body?.unit_type === 0 ? 0 : req?.body?.unit_type || null;
            const grade = req?.body?.grade;
            const resultQueryClasses: any[] = [];

            try {
                const classList = await prisma.classes.findMany({
                    where: {
                        created_by: teacher?.id,
                        grade: grade,
                        status: 1,
                        deleted: 0
                    },
                    orderBy: [
                        { created_date: 'desc' }
                    ]
                });

                classList.map(item => resultQueryClasses.push({
                    id: item.id,
                    name: item.name,
                    correctRate: 0
                }))
            } catch (error) {
                console.error('Error while get classes for teacher:', error);
                return res.status(400).json({ message: error })
            }

            try {
                const resultQueryClassesTestResult = await Promise.all(
                    resultQueryClasses.map(item => {
                        return prisma.$queryRaw`SELECT DISTINCT utr.user_id, cut.id, cut.unit_test_id, utr.point, utr.max_point, utr.created_date, cs.class_id
                        FROM class_unit_test cut
                        LEFT JOIN unit_test_result utr ON  utr.class_unit_test_id = cut.id
                        JOIN class_student cs ON cs.student_id = utr.user_id and cut.class_id = cs.class_id
                        WHERE cut.class_id = ${item.id}
                        AND cut.class_group_id IS NULL
                        AND cut.student_id IS NULL
                        AND utr.user_id IS NOT NULL
                        AND cut.end_date < UTC_TIMESTAMP()
                        AND cs.STATUS = 1
                        ${unit_type !== null ? Prisma.sql`AND cut.unit_type = ${unit_type}` : Prisma.empty}`
                    })
                );

                const classStudentRes = await Promise.all(
                    resultQueryClasses.map((item) => {
                        item.testResult = {};

                        return prisma.$queryRaw`SELECT distinct cs.student_id as user_id, cut.id
                        FROM itest.class_unit_test cut
                        LEFT JOIN itest.class_student cs ON cut.class_id = cs.class_id
                        WHERE cut.class_id = ${item.id}
                        AND cut.class_group_id IS NULL
                        AND cut.student_id IS NULL
                        AND cut.end_date < UTC_TIMESTAMP()
                        AND cs.STATUS = 1
                        ${unit_type !== null ? Prisma.sql`AND cut.unit_type = ${unit_type}` : Prisma.empty}`
                    })
                );

                resultQueryClassesTestResult.forEach((testItem: any, testIndex: number) => {
                    if (!testItem?.id) {
                        resultQueryClasses[testIndex].testResult = {}
                    }

                   testItem.forEach((testItem2: any) => {
                        if (resultQueryClasses[testIndex].testResult[getStudentTestKey(testItem2)] && resultQueryClasses[testIndex].testResult[getStudentTestKey(testItem2)] > getPointRate(testItem2))
                        {
                            return
                        }

                        resultQueryClasses[testIndex].testResult[getStudentTestKey(testItem2)] = getPointRate(testItem2);
                    })
                })

                classStudentRes.forEach((studentItem: any, studentIndex: number) => {
                    studentItem.forEach((studentItem2: any) => {
                        if (!resultQueryClasses[studentIndex].testResult.hasOwnProperty(getStudentTestKey(studentItem2))) {
                            resultQueryClasses[studentIndex].testResult[getStudentTestKey(studentItem2)] = 0
                        }
                    })
                })

            } catch (error) {
                console.error('Error when query:', error);
                return res.status(400).json({ message: error })
            }

            resultQueryClasses.forEach((item) => {
                item.correctRate = sumValues(item.testResult) / Object.keys(item.testResult).length;
            })

            return res.status(200).json(apiResponseDTO(true, resultQueryClasses));
        }
    },
});

export default handler