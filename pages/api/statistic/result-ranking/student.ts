import { Prisma } from "@prisma/client";
import createHandler, { apiResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";
import { getPointRate, getStudentTestKey, sumValues } from "./class";

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher = session.user;
            const body = req?.body;

            const class_id = body?.class_id;
            const unit_type = body?.unit_type === 0 ? 0 : body?.unit_type || null;
            const get_all = body?.get_all || false;

            let resultQueryStudent: any[] = []

            if (!class_id) {
                return res.status(400).json({ message: '`class_id` required' })
            }

            try {
                resultQueryStudent = await prisma.classStudent.findMany({
                    where: {
                        status: 1,
                        class_id: class_id,
                    },
                    select: {
                        student: {
                            select: {
                                full_name: true,
                                phone: true,
                            }
                        },
                        class_group_id: true,
                        class_id: true,
                        created_by: true,
                        created_date: true,
                        student_id: true,
                        student_code: true,
                    }
                })

                resultQueryStudent.forEach(item => {
                    item.testResult = {};
                    item.name = item?.student?.full_name || "";
                    item.phone = item?.student?.phone || "";
                })

            } catch (error) {
                console.error('Error:', error);
                return res.status(400).json({ message: error })
            }

            try {
                const studentTestResult: any[] = await prisma.$queryRaw`SELECT DISTINCT utr.user_id, cut.id, cut.unit_test_id, utr.point, utr.max_point, utr.created_date, cs.class_id, cut.student_id as only_student_id, cut.class_group_id as only_class_group_id
                FROM itest.class_unit_test cut
                LEFT JOIN itest.unit_test_result utr ON  utr.class_unit_test_id = cut.id
                JOIN itest.class_student cs ON cs.student_id = utr.user_id and cs.class_id = cut.class_id
                WHERE cut.created_by = ${teacher.id}
                AND cut.class_id = ${class_id} 
                AND utr.user_id IS NOT NULL
                ${get_all ? Prisma.sql`AND cut.class_group_id IS NULL AND cut.student_id IS NULL` : Prisma.empty}
                AND cut.end_date < UTC_TIMESTAMP()
                AND cs.STATUS = 1
                ${unit_type !== null ? Prisma.sql`AND cut.unit_type = ${unit_type}` : Prisma.empty}`

                const classStudentRes: any[] = await prisma.$queryRaw`SELECT distinct cs.student_id as user_id, cut.id, cut.student_id as only_student_id, cut.class_group_id as only_class_group_id
                FROM itest.class_unit_test cut
                LEFT JOIN itest.class_student cs ON cut.class_id = cs.class_id
                WHERE cut.class_id = ${class_id}
                AND cut.end_date < UTC_TIMESTAMP()
                AND cs.STATUS = 1
                ${unit_type !== null ? Prisma.sql`AND cut.unit_type = ${unit_type}` : Prisma.empty}
                ${get_all ? Prisma.sql`AND cut.class_group_id IS NULL AND cut.student_id IS NULL` : 
                Prisma.sql`AND (cs.student_id = cut.student_id 
                OR cs.class_group_id = cut.class_group_id
                OR (cut.class_group_id IS NULL AND cut.student_id IS NULL))`}`

                studentTestResult.forEach((testItem: any) => {
                    const findStudent = resultQueryStudent.find(student => {
                        return student.student_id === testItem.user_id
                    })

                    if (findStudent.testResult[getStudentTestKey(testItem)] && findStudent.testResult[getStudentTestKey(testItem)] > getPointRate(testItem)) {
                        return
                    }

                    findStudent.testResult[getStudentTestKey(testItem)] = getPointRate(testItem);
                })

                classStudentRes.forEach((studentItem: any) => {
                    const findStudent = resultQueryStudent.find(student => {
                        return student.student_id === studentItem.user_id
                    })

                    if (!findStudent.testResult.hasOwnProperty(getStudentTestKey(studentItem))) {
                        findStudent.testResult[getStudentTestKey(studentItem)] = 0
                    }
                })

            } catch (error) {
                console.error('Error when query:', error);
                return res.status(400).json({ message: error })
            }

            const resultCountRating = {
                1: 0,
                2: 0,
                3: 0,
                4: 0,
                5: 0,
            }

            resultQueryStudent.forEach((item) => {
                const correctRate = sumValues(item.testResult) / Object.keys(item.testResult).length;
                item.correctRate = correctRate || 0;

                if (correctRate >= 0.8) {
                    item.rating = 5;
                    resultCountRating[5]++;
                } else if (correctRate >= 0.65) {
                    item.rating = 4
                    resultCountRating[4]++;
                } else if (correctRate >= 0.5) {
                    item.rating = 3
                    resultCountRating[3]++;
                } else if (correctRate >= 0.35) {
                    item.rating = 2
                    resultCountRating[2]++;
                } else {
                    item.rating = 1
                    resultCountRating[1]++;
                }
            })

            resultQueryStudent.sort((studentA: any, studentB: any) => {
                if (studentB.correctRate > studentA.correctRate) {
                    return 1
                }

                if (studentB.correctRate < studentA.correctRate) {
                    return -1
                }

                return studentA?.name.localeCompare(studentB?.name)
            })

            let previousCorrectRate = 0;

            resultQueryStudent.forEach((student: any, studentIndex: number) => {
                if (student.correctRate == previousCorrectRate && studentIndex != 0) {
                    student.rank = resultQueryStudent[studentIndex - 1].rank;
                } else {
                    previousCorrectRate = student.correctRate;
                    student.rank = studentIndex + 1;
                }

            })

            return res.status(200).json(apiResponseDTO(true, {
                count: resultCountRating,
                detail: resultQueryStudent
            }));
        }
    },
});

export default handler