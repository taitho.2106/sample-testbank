import { UNIT_TYPE } from "@/interfaces/struct";
import { getNDayFromNow, isEqualDate, yyyymmdd } from "@/utils/string";
import createHandler, { apiResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";



const handler: any = createHandler({
    'POST': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher = session.user;
            const grade = req?.body?.grade;
            const class_id = req?.body?.class_id;
            const start_date = req?.body?.start_date ? new Date(req?.body?.start_date) : getNDayFromNow(30);
            const end_date = req?.body?.end_date ? new Date(req?.body?.end_date) : new Date();

            const result: any[] = []

            const listUnitTest = await prisma.classUnitTest.findMany({
                where: {
                    created_by: teacher?.id,
                    status: 1,
                    class: {
                        status: 1,
                        deleted: 0,
                        grade: grade,
                        id: class_id
                    },
                    student_id: null,
                    class_group_id: null,
                    start_date: {
                        gte: start_date,
                        lte: end_date,
                    },
                }
            })

            for (let i = new Date(start_date); i <= end_date; i.setDate(i.getDate() + 1)) {
                result.push({
                    start_date: new Date(i),
                    count: UNIT_TYPE.map(t => ({
                        unit_type: t.code,
                        value: 0
                    }))
                })
            }

            listUnitTest.forEach(test => {
                result.find((item) => {
                    return isEqualDate(item.start_date, test.start_date)
                }).count.find((c: any) => {
                    return c.unit_type == test.unit_type;
                }).value++
            })

            return res.status(200).json(apiResponseDTO(true, result));
        }
    },
});

export default handler