
import { NextApiRequest, NextApiResponse } from 'next'

import { query } from 'lib/db'
import { UNIT_TEST_GROUP } from '@/constants/index'
const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    switch (req.method) {
        case 'GET':
          await getListSeries(req, res)
          break
        default:
          res.status(405).json({ code: 0, message: 'Method Not Allowed' })
      }
}

const getListSeries = async(req: NextApiRequest, res: NextApiResponse)=>{
    try {
        let { grade, groups = UNIT_TEST_GROUP.TEXTBOOK + '' } = req.query;
        let groupArr = null;
        if (groups) {
            groupArr = (groups as string).split(',').filter(Boolean);

            if (!groupArr.length) {
                groupArr = null;
            }
        }

        const list = await query<any[]>(
            `SELECT id, name, image_url as image, series_type as type, grade_eduhome_id, grade_id, series_group as \`group\` 
            FROM series 
            WHERE 
                deleted = 0 
                ${grade ? `AND grade_id = ? ` : ''}
                ${groupArr ? `AND series_group IN (?) AND series_group IS NOT NULL ` : ''}
            ORDER BY priority`,
            [
                grade,
                groupArr,
            ].filter(Boolean),
        )
        return res.status(200).json({
            code:1,
            data:list,
        })
    }
    catch (e: any) {
        console.log("e",e);
        res.status(500).json({code:0, message: e.message })
    }
}

export default handler