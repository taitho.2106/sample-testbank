import { NextApiRequest, NextApiResponse } from 'next'

import { query } from 'lib/db'
const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    switch (req.method) {
        case 'GET':
          await getListSkills(req, res)
          break
        default:
          res.status(405).json({ code: 0, message: 'Method Not Allowed' })
      }
}

const getListSkills = async(req: NextApiRequest, res: NextApiResponse)=>{
    try {
        const list = await query<any[]>(
            'SELECT id, name FROM skills where deleted =0 order by priority',
            [],
        )
        return res.status(200).json({code:1, data:list})
    }
    catch (e: any) {
        console.log("e",e);
        res.status(500).json({code:0, message: e.message })
    }
}

export default handler