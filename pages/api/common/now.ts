import { NextApiRequest, NextApiResponse } from 'next'
const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const date = Date.now();
    return res.status(200).json(date);
}
export default handler