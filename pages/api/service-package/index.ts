import { query } from "lib/db";
import { NextApiRequest, NextApiResponse } from "next"

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    switch (req.method) {
        case 'GET':
            return getServicePackage(req, res);
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}

const getServicePackage = async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        const results: any[] = await query<any[]>(
            `SELECT * FROM service_package WHERE status = 1`,
            [],
        )
        return res.json({
            data: results,
        })
    } catch (e: any) {
        res.status(500).json({ code: 0, message: e.message })
    }
}

export default handler