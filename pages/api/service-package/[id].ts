import { query } from "lib/db";
import { NextApiRequest, NextApiResponse } from "next"
import { getSession } from "next-auth/client"

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req });
    if (!session) {
        return res.status(401).end('Unauthorized')
    }

    switch (req.method) {
        case 'GET':
            return getServicePackageDetails(req, res);
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}

const getServicePackageDetails = async (req: NextApiRequest, res: NextApiResponse) => {
    try {
        const { id } = req.query;

        const results: any[] = await query<any[]>(
            `SELECT * FROM service_package WHERE id = ?`,
            [id],
        )
        const data = results[0] || null;
        return res.json({
            data,
            result: !!data,
        })
    } catch (e: any) {
        res.status(500).json({ code: 0, message: e.message })
    }
}

export default handler