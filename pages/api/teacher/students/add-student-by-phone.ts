import { ClassError } from "@/constants/errorCodes";
import { commonDeleteStatus, commonStatus, LIMIT_CLASS_STUDENT_PACKAGE_BASIC } from "@/constants/index";
import { PACKAGES, USER_ROLES } from "@/interfaces/constants";
import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle";
import prisma from 'lib/prisma';
import { NextApiRequest, NextApiResponse } from "next";
import { countClassActiveStudent, getActiveClass } from "services/classes";
import * as yup from "yup";

const addStudentSchema = yup.object().shape({
    phone: yup.string().trim().required(),
    classId: yup.number(),
});

const handleAddStudent = async (req: NextApiRequest, res: NextApiResponse, session: any) => {
    const { phone, classId } = req.body;
    const student = await prisma.user.findFirst({
        where: {
            phone,
            deleted: commonDeleteStatus.ACTIVE,
        }
    });
    if (!student) {
        return res.status(400).json(errorResponseDTO(ClassError.STUDENT_NOT_FOUND, 'Student not found', null));
    }
    if (USER_ROLES.Student !== student.user_role_id) {
        return res.status(400).json(errorResponseDTO(ClassError.USER_NOT_STUDENT, 'User not student', null));
    }

    try {
        const teacher: any = session.user;
        try {
            const teacherStudent = await prisma.teacherStudent.findUnique({
                where: {
                    teacher_id_student_id: {
                        teacher_id: teacher.id,
                        student_id: student.id,
                    }
                }
            });

            if (teacherStudent) {
                if (teacherStudent.status === commonStatus.ACTIVE) {
                    throw new Error('Student already added!');
                } else {
                    await prisma.teacherStudent.update({
                        where: {
                            teacher_id_student_id: {
                                teacher_id: teacher.id,
                                student_id: student.id,
                            }
                        },
                        data: {
                            ...getAuditData(session),
                            status: commonStatus.ACTIVE,
                        }
                    })
                }
            } else {
                await prisma.teacherStudent.create({
                    data: {
                        ...getAuditData(session, true),
                        teacher_id: teacher.id,
                        student_id: student.id,
                    }
                });
            }
        } catch (error: any) {
            if (!classId) {
                return res.status(400).json(errorResponseDTO(400, error.message || 'Error undefined!'));
            }
        }

        if (classId) {
            const classInfo = await getActiveClass(classId, teacher.id);
            if (!classInfo) {
                return res.status(400).json(errorResponseDTO(400, 'Class not found'));
            }

            // Giới hạn gói "phổ thông" của giáo viên chỉ thêm 60 học viên vào lớp
            const servicePackage: any = session.servicePackage;
            if (servicePackage.code === PACKAGES.BASIC.CODE) {
                const currentClassStudentCount = await countClassActiveStudent(classId);

                if (currentClassStudentCount >= LIMIT_CLASS_STUDENT_PACKAGE_BASIC) {
                    return res.status(400).json(errorResponseDTO(ClassError.IMPORT_STUDENT_REACH_LIMIT, 'Reach limit class student!'));
                }
            }

            const classStudent = await prisma.classStudent.findFirst({
                where: {
                    class_id: classId,
                    student_id: student.id,
                }
            });

            if (classStudent) {
                if (classStudent.status === commonStatus.ACTIVE) {
                    throw new Error('Student already added!');
                } else {
                    await prisma.classStudent.update({
                        where: {
                            class_id_student_id: {
                                class_id: classId,
                                student_id: student.id,
                            }
                        },
                        data: {
                            ...getAuditData(session),
                            status: commonStatus.ACTIVE,
                        }
                    });
                }
            } else {
                await prisma.classStudent.create({
                    data: {
                        ...getAuditData(session, true),
                        class_id: classId,
                        student_id: student.id,
                    }
                });
            }
        }

        return res.status(200).json(apiResponseDTO(true, null));
    } catch (error: any) {
        return res.status(400).json(errorResponseDTO(400, error.message || 'Error undefined!'));
    }
}

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        bodySchema: addStudentSchema,
        requireServicePackage: [],
        handler: handleAddStudent,
    }
});

export default handler