import { commonStatus } from "@/constants/index";
import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            let resultCount: any = 0;
            const teacher: any = session?.user;
            
            try {
                resultCount = await prisma.teacherStudent.count({
                    where: {
                        teacher_id: teacher?.id,
                        status: commonStatus.ACTIVE
                    },
                });
            } catch (error) {
                console.error(error)
                return res.status(400).json(errorResponseDTO(400, "err when count"));
            } finally {
                await prisma.$disconnect();
            }


            return res.status(200).json(apiResponseDTO(true, {
                count: resultCount
            }));
        }
    },
});

export default handler