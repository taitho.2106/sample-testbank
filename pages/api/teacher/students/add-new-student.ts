import bcrypt from 'bcrypt';
import { NextApiRequest, NextApiResponse } from "next";

import { getContentEmailAccountCreated, titleAccountCreated } from "@/constants/email-template";
import { ClassError } from "@/constants/errorCodes";
import { DATE_DISPLAY_FORMAT, LIMIT_CLASS_STUDENT_PACKAGE_BASIC, commonDeleteStatus, commonStatus } from "@/constants/index";
import { PACKAGES, USER_ROLES } from "@/interfaces/constants";
import { convertDateStringToDate } from "@/utils/date";
import { randomString } from "@/utils/index";
import { generateUsername } from '@/utils/user';
import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle";
import prisma from 'lib/prisma';
import { countClassActiveStudent } from 'services/classes';
import { sendEmail } from "services/email";
import { addStudentSchema } from "validations";

const getClassWillAddValidLimit = async (classIds: number[]) => {
    const classWillAdd = [];
    const classCount = await Promise.all(classIds.map(countClassActiveStudent));
    console.log({ classCount });
    for (let i = 0; i < classCount.length; i++) {
        if (classCount[i] < LIMIT_CLASS_STUDENT_PACKAGE_BASIC) {
            classWillAdd.push(classIds[i]);
        }
    }

    return classWillAdd;
}

const handleAddNewStudent = async (req: NextApiRequest, res: NextApiResponse, session: any) => {
    const { phone, email, fullName, studentCode, classId, birthday, gender } = req.body;
    const { user: teacher, servicePackage }: any = session;
    const classIds = typeof classId === 'number' ? [classId] : (classId || []);
    if (classIds.length) {
        const teacherClass = await prisma.classes.findMany({
            where: {
                id: {
                    in: classIds,
                },
                owner_id: teacher.id,
                status: commonStatus.ACTIVE,
                deleted: commonDeleteStatus.ACTIVE,
            }
        });
        if (teacherClass.length !== classIds.length || teacherClass.some((cls: any) => cls.owner_id !== teacher.id)) {
            return res.status(400).json(errorResponseDTO(ClassError.CLASS_NOT_FOUND, 'Class not found', null));
        }
    }

    const student = await prisma.user.findFirst({
        where: {
            OR: [
                { phone },
                { email },
                { user_name: phone },
            ],
            deleted: commonDeleteStatus.ACTIVE,
        }
    });
    if (student) {
        return res.status(400).json(errorResponseDTO(ClassError.STUDENT_EXISTED, 'Student existed', null));
    }

    if (studentCode) {
        const studentInClass = await prisma.classStudent.findMany({
            where: {
                student_code: studentCode,
                class_id: {
                    in: classIds
                },
            }
        });

        if (studentInClass.length) {
            return res.status(400).json(errorResponseDTO(ClassError.STUDENT_EXISTED_CODE, 'Student code existed', null));
        }
    }

    const randomPassword = randomString(8);
    const salt = await bcrypt.genSalt(10)
    const password = await bcrypt.hash(randomPassword, salt);
    try {
        const createUserData: any = {
            phone,
            user_name: generateUsername(phone),
            email: email ? email.toLowerCase() : email,
            full_name: fullName,
            birthday: birthday ? convertDateStringToDate(birthday, DATE_DISPLAY_FORMAT, true).local().toDate() : undefined,
            gender,
            user_role: {
                connect: { id: USER_ROLES.Student }
            },
            password,
            salt,
            created_date: new Date(),
            teachers: {
                create: {
                    ...getAuditData(session, true),
                    teacher_id: teacher.id,
                }
            }
        }
        if (classIds.length) {
            let classWillAdd = [];
            if (servicePackage.code === PACKAGES.BASIC.CODE) {
                classWillAdd = await getClassWillAddValidLimit(classIds);
            } else {
                classWillAdd = classIds;
            }

            if (classWillAdd.length) {
                const createAddToClass = (class_id: number) => ({
                    student_code: studentCode,
                    ...getAuditData(session, true),
                    class_id,
                })
                createUserData.classStudent = {
                    create: classWillAdd.map(createAddToClass)
                };
            }
        }
        const user = await prisma.user.create({ data: createUserData });

        try {
            sendEmail({
                to: email,
                subject: titleAccountCreated,
                content: getContentEmailAccountCreated({ username: user.phone, password: randomPassword }),
            });
        } catch (error) {
        }

        return res.status(200).json(apiResponseDTO(true, null));
    } catch (error: any) {
        console.log({ error });
        return res.status(400).json(errorResponseDTO(400, error.message || 'Error undefined!'));
    }
}

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        bodySchema: addStudentSchema,
        requireServicePackage: [],
        handler: handleAddNewStudent,
    }
});

export default handler