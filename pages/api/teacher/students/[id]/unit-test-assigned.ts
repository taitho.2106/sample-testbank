import createHandler, { apiResponseDTO, errorResponseDTO, transformToArray } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import { STATUS_CODE_EXAM } from "@/interfaces/constants";
import { getDateRangeConditionByStatus } from "@/utils/date";
import { mapListClassUnitTestToDTO } from "dto/unitTest";
import prisma from 'lib/prisma';
import { getStudentUnitTestAvailable } from "services/unitTest";

const getParamsSchema = yup.object().shape({
    studentId: yup.number(),
    classId: yup.number().required(),
    examStatus: yup.array().of(yup.number().oneOf(Object.values(STATUS_CODE_EXAM))).transform(value => {
        if (!Array.isArray(value)) {
            return [value];
        }

        return value;
    }),
});

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        pageable: true,
        querySchema: getParamsSchema,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher: any = session.user;
            const { id: studentId, classId, examStatus }: any = req.query;
            const examStatusListFilter = transformToArray(examStatus, (el: any) => +el);
            const dateCondition = examStatusListFilter.map((status: any) => getDateRangeConditionByStatus(status));

            const classStudent = await prisma.classStudent.findFirst({
                where: {
                    student_id: studentId,
                    classes: {
                        id: +classId,
                        owner_id: teacher.id,
                    }
                }
            })
            if (!classStudent) {
                return res.status(400).json(errorResponseDTO(400, 'Bad request!'));
            }

            const unitTest: any = await getStudentUnitTestAvailable(+classId, studentId, dateCondition);
            if (!unitTest) {
                return res.status(400).json(errorResponseDTO(400, 'Student invalid!'));
            }

            return res.status(200).json(apiResponseDTO(
                true,
                mapListClassUnitTestToDTO(unitTest.map((el: any) => ({
                    ...el,
                    unit_test_result: el.unit_test_result[0] ?? null,
                }))),
            ))
        }
    },
});

export default handler