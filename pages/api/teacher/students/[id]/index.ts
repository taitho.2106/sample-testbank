import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";

import { commonDeleteStatus, commonStatus } from "@/constants/index";
import studentDTO from "dto/studentDTO";
import prisma from 'lib/prisma';

const handler: any = createHandler({
    'DELETE': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher: any = session.user;
            const { id }: any = req.query;

            const teacherStudent = await prisma.teacherStudent.findFirst({
                where: {
                    teacher_id: teacher.id,
                    student_id: id,
                }
            });
            if (!teacherStudent) {
                return res.status(400).json(errorResponseDTO(400, 'Student not found'));
            }

            try {
                const [result] = await prisma.$transaction([
                    prisma.teacherStudent.update({
                        where: {
                            teacher_id_student_id: {
                                teacher_id: teacher.id,
                                student_id: id,
                            }
                        },
                        data: {
                            status: commonStatus.INACTIVE,
                        },
                    }),
                    prisma.classStudent.updateMany({
                        where: {
                            student_id: id,
                            classes: {
                                owner_id: teacher.id,
                            }
                        },
                        data: {
                            status: commonStatus.INACTIVE,
                            class_group_id: null,
                            student_code: null,
                        },
                    })
                ]);
                return res.status(200).json(apiResponseDTO(true, result));
            } catch (error: any) {
                return res.status(200).json(apiResponseDTO(false, null, error?.meta?.cause || '', 400));
            }
        }
    },
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher: any = session.user;
            const { id }: any = req.query;

            const [result, classes] = await Promise.all([
                prisma.teacherStudent.findFirst({
                    where: {
                        teacher_id: teacher.id,
                        student_id: id,
                    },
                    select: {
                        student: {
                            select: studentDTO
                        }
                    }
                }),
                prisma.classStudent.findMany({
                    select: {
                        classes: {
                            select: {
                                id: true,
                                name: true,
                            }
                        },
                    },
                    where: {
                        classes: {
                            owner_id: teacher.id,
                            deleted: commonDeleteStatus.ACTIVE,
                        },
                        student_id: id,
                    },
                    orderBy: {
                        created_date: 'desc',
                    }
                })
            ]);

            return res.status(200).json(apiResponseDTO(true, {
                student: result.student,
                classes: (classes || []).map(el => el.classes),
            }));
        }
    },
});

export default handler