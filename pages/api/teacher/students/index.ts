import createHandler, { apiResponseDTO, paginationResponse } from "lib/apiHandle"
import { NextApiRequest, NextApiResponse } from "next"
import prisma from 'lib/prisma';
import { commonDeleteStatus, commonStatus } from "@/constants/index";
import studentDTO from "dto/studentDTO";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        pageable: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any, paginationData) => {
            const teacher: any = session.user;
            let { search, classIds }: any = req.query;

            let result = [];

            if (classIds) {
                if (typeof classIds === 'string') {
                    classIds = [+classIds];
                } else {
                    classIds = classIds.map((el: string) => +el).filter(Boolean)
                }

                const studentCondition = {
                    status: commonStatus.ACTIVE,
                    classes: {
                        id: {
                            in: classIds
                        },
                        owner_id: teacher.id,
                    },
                    student: search ? {
                        OR: [
                            { phone: { contains: search as string } },
                            { full_name: { contains: search as string } },
                        ]
                    } : undefined
                }

                result[0] = await prisma.classStudent.findMany({
                    distinct: ['student_id'],
                    where: studentCondition,
                    select: {
                        student: {
                            select: {
                                ...studentDTO,
                            },
                        },
                        status: true,
                        created_date: true,
                    },
                    orderBy: {
                        student: {
                            full_name: 'asc',
                        }
                    },
                    ...paginationData,
                })

                const total = await prisma.classStudent.groupBy({
                    by: ['student_id'],
                    where: studentCondition,
                });

                result[1] = total.length;
            } else {
                const studentCondition = {
                    status: commonStatus.ACTIVE,
                    teacher_id: teacher.id,
                    student: search ? {
                        OR: [
                            { phone: { contains: search as string } },
                            { full_name: { contains: search as string } },
                        ]
                    } : undefined
                }

                result = await prisma.$transaction([
                    prisma.teacherStudent.findMany({
                        where: studentCondition,
                        select: {
                            student: {
                                select: studentDTO,
                            },
                            status: true,
                            created_date: true,
                        },
                        orderBy: {
                            student: {
                                full_name: 'asc',
                            }
                        },
                        ...paginationData,
                    }),
                    prisma.teacherStudent.count({
                        where: studentCondition,
                    })
                ])
            }

            const [ data, total ] = result;

            return res.status(200).json(apiResponseDTO(
                true,
                paginationResponse(data.map(({ student, ...rest }) => ({ ...student, ...rest })), total, paginationData),
            ));
        }
    },
});

export default handler