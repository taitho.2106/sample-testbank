import { NextApiRequest, NextApiResponse } from "next";

import { DATE_DISPLAY_FORMAT } from "@/constants/index";
import { getFormDataFromRequest } from "@/utils/api";
import { convertDateStringToDate } from "@/utils/date";
import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import {
    addStudentsToTeacherIfNotExist,
    createUsersAndSendMail,
    verifyStudentData,
} from 'services/student';
import fileUtils from 'utils/file';

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any, p: any, { t }: any) => {
            const { files }: any = await getFormDataFromRequest(req);
            const teacher: any = session.user;

            if (!files?.file) {
                return res.status(400).json(errorResponseDTO(400, 'File invalid'));
            }

            try {
                const studentData = (await fileUtils.readExcelFile(files.file, [
                    'fullName',
                    'phone',
                    'email',
                    'birthday',
                    'gender',
                ]));
                const studentDataVerified = await verifyStudentData(studentData, t);

                const studentWillCreate = studentDataVerified
                    .filter((stu: any) => !stu.status && !stu.id)
                    .map(({ birthday, gender, ...rest}: any) => ({
                        ...rest,
                        birthday: birthday ? convertDateStringToDate(birthday, DATE_DISPLAY_FORMAT, true).local().toDate() : undefined,
                        gender: gender ? +gender : gender,

                    }));
                const studentCreateResult: any = await createUsersAndSendMail(studentWillCreate);
                for (const newStu of studentCreateResult) {
                    const stu = studentDataVerified.find((stu: any) => !stu.status && stu.phone === newStu.phone)
                    if (stu) {
                        stu.id = newStu.id;
                    }
                }

                const studentsAdd = studentDataVerified.filter((stu: any) => !!stu.id);

                const { errors } = await addStudentsToTeacherIfNotExist(studentsAdd, teacher.id, session);

                for (const stuError of errors) {
                    const stu = studentDataVerified.find((stu: any) => !stu.status && stu.id === stuError.id)
                    if (stu) {
                        stu.status = stuError.status;
                    }
                }

                return res.status(200).json(apiResponseDTO(true, studentDataVerified));
            } catch (error) {
                console.log({ error });
                return res.status(400).json(errorResponseDTO(400, 'Error Undefined'));
            }
        }
    },
});

export const config = {
    api: {
        bodyParser: false,
    },
};

export default handler