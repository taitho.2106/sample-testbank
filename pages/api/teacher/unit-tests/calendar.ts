import * as yup from 'yup';
import createHandler, { apiResponseDTO } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";
import prisma from 'lib/prisma';
import { dayjs } from '@/utils/date';
import { classUnitTestSelect } from 'dto/entitiesSelect';
import { commonDeleteStatus } from '@/constants/index';
import { mapListClassUnitTestToDTO } from 'dto/unitTest';

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            year: yup.number().required(),
            month: yup.number().min(1).max(12).required(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher: any = session.user;
            const { year, month } = req.query;
            const filterMonth = dayjs(`${year}-${month}-01`);

            const result = await prisma.classUnitTest.findMany({
                where: {
                    class: {
                        deleted: commonDeleteStatus.ACTIVE,
                        owner_id: teacher.id,
                    },
                    start_date: {
                        gte: filterMonth.startOf('M').toDate(),
                        lte: filterMonth.endOf('M').toDate(),
                    }
                },
                select: classUnitTestSelect,
                orderBy: {
                    start_date: 'asc',
                }
            });

            return res.status(200).json(apiResponseDTO(true, mapListClassUnitTestToDTO(result)));
        }
    },
});

export default handler