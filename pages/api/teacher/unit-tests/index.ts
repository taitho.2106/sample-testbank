import { commonDeleteStatus } from "@/constants/index";
import createHandler, { apiResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher: any = session.user;
            const { grade } = req.query;

            const unitTests = await prisma.unit_test.findMany({
                where: {
                    scope: 1,
                    created_by: teacher.id,
                    template_level_id: grade as string,
                    deleted: commonDeleteStatus.ACTIVE,
                }
            });

            return res.status(200).json(apiResponseDTO(true, unitTests));
        }
    },
});

export default handler