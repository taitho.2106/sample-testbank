import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { SALE_ITEM_TYPE } from '@/constants/index'
import { PACKAGE_LEVEL_CONFIG, USER_ROLES } from '@/interfaces/constants'
import { escapeSearchString, query } from 'lib/db'

// const limit = 10

const handler = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session?.user
  const isAdmin = user?.is_admin ==1;
  const queryRequest:any = req.query
  const limit: number = queryRequest.limit ?? 10
  switch (req.method) {
    case 'POST':
      return filterUnittest(req, res)
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function filterUnittest(req: NextApiRequest, res: NextApiResponse) {
    try {
      // data
      //privacy: quyền riêng tư : 1: riêng tư, !=1: công khai
      //đối với role khác admin: k lấy đề thi riêng tư

      const page: any = req?.body?.page || 0
      const name = req?.body?.name || ''
      const level = req?.body?.template_level_id || []
      const time = req?.body?.time
      const total_question = req?.body?.total_question
      const skill = req?.body?.skills || []
      const start_date = req?.body?.start_date
        ? new Date(req.body.start_date)
        : null
      const end_date = req?.body?.end_date ? new Date(req.body.end_date) : null
      const status = req?.body?.status || 'AL'
      const scope = req?.body?.scope || 0
      const series_id = req?.body?.series_id || []
      const unit_type = req?.body?.unit_type;
      const unit_test_types = req?.body?.unit_test_types;
      const isInternationalExam = req?.body?.is_international_exam;
      // filter conditions
      let unittestCondition = '' 
      if (name){ 
        if(name.includes('\\')){
          const nameArray = name.toString().split('')
          for (let i = 0; i < nameArray.length; i++) {
            if(nameArray[i] === '\\'){
              nameArray[i] = `\\\\\\${nameArray[i]}`
            }
          }
          const newFormat = `${nameArray.join('')}`
          unittestCondition += " AND (ut.name LIKE '%" + newFormat + "%')"
        }else{
          const nameString = escapeSearchString(name.trim())
          unittestCondition += " AND (ut.name LIKE '%" + nameString + "%')"
        }
      }
      if (level.length > 0) {
        let levelStrArr = ''
        level.forEach(
          (item: string, i: number) =>
            (levelStrArr += `${i !== 0 ? ',' : ''}"${item}"`),
        )
        unittestCondition += ` AND ut.template_level_id IN (${levelStrArr})`
      }
      if(!isNaN(time)){
        unittestCondition += ` AND ut.time = ${time}`
      }
      if(!isNaN(total_question)){
        unittestCondition += ` AND ut.total_question = '${total_question}'`
      }
      if (skill.length > 0) {
        let skillStrArr = ''
        skill.forEach(
          (item: string, i: number) =>
            (skillStrArr += `${i !== 0 ? ',' : ''}"${item}"`),
        )
        unittestCondition += ` AND uts.section LIKE "%${skillStrArr}%"`
      }
      if(series_id.length > 0){
        let series_id_string = ''
        series_id.forEach((item:string, i:number)=>(series_id_string += `${i !== 0 ? ',' : ''}"${item}"`))
        unittestCondition += ` AND ut.series_id IN (${series_id_string})`
      }
      if(unit_type !== undefined) {
        unittestCondition += ` AND ut.unit_type = ${unit_type}`
      }
      if(unit_test_types?.length) {
        unittestCondition += ` AND ut.unit_test_type IN (${unit_test_types})`
      }
      if(isInternationalExam !== undefined) {
        unittestCondition += ` AND ut.is_international_exam = ${isInternationalExam}`
      }
      const d = new Date()
      const currentDate =
        [d.getFullYear(), d.getMonth() + 1, d.getDate()].join('-') +
        ' ' +
        [d.getHours(), d.getMinutes(), d.getSeconds()].join(':')
      const startDate = start_date
        ? [
            start_date.getFullYear(),
            start_date.getMonth() + 1,
            start_date.getDate(),
          ].join('-') +
          ' ' +
          [
            start_date.getHours(),
            start_date.getMinutes(),
            start_date.getSeconds(),
          ].join(':')
        : null
      const endDate = end_date
        ? [
            end_date.getFullYear(),
            end_date.getMonth() + 1,
            end_date.getDate(),
          ].join('-') +
          ' ' +
          [
            end_date.getHours(),
            end_date.getMinutes(),
            end_date.getSeconds(),
          ].join(':')
        : null
      if (startDate)
        unittestCondition += ` AND DATE(ut.start_date) = '${startDate}'`
      if (endDate) unittestCondition += ` AND DATE(ut.end_date) = '${endDate}'`
      switch (status) {
        case 'AC':
          unittestCondition += ` AND ut.start_date <= '${currentDate}' AND ut.end_date >= '${currentDate}'`
          break
        case 'DI':
          unittestCondition += ` AND (ut.start_date > '${currentDate}' OR ut.end_date < '${currentDate}')`
          break
        default:
          break
      }
      switch (scope) {
        case 0:
          unittestCondition += ` AND ut.scope = 0`
          break
        case 1: {
            if (user.user_role_id !== USER_ROLES.Student) {
                unittestCondition += ` AND ut.scope = 1 AND ut.created_by = '${user.id}'`
            }
            break
        }
        case 2:
          unittestCondition += ` AND ut.scope = 1`
          break
        default:
          break
      }
      const studentCondition = {
        select: '',
        join: '',
        whereCondition: '',
        groupBy: '',
      };
      if (user.user_role_id === USER_ROLES.Student) {
        if (scope === 1) {
            studentCondition.join = ' JOIN user_asset ua ON ut.id = ua.asset_id';
            unittestCondition += ` AND ua.user_id = '${user.id}' AND ua.asset_type = '${SALE_ITEM_TYPE.UNIT_TEST}'`;
        } else {
            studentCondition.select = ', si.id as sale_item_id, si.exchange_price';
            studentCondition.join = ' LEFT JOIN sale_item si ON ut.id = si.item_id';
            studentCondition.groupBy = ', si.id';
            unittestCondition += ` AND ut.id NOT IN (SELECT ua.asset_id FROM user_asset ua WHERE ua.user_id = '${user.id}' AND ua.asset_type = '${SALE_ITEM_TYPE.UNIT_TEST}')`;
        }
      }
      // templates
      // order null last -> -ut.unit_test_type DESC: https://stackoverflow.com/questions/2051602/mysql-orderby-a-number-nulls-last
      const unitTests: any[] = await query(
        `SELECT ut.*, count(*) as totalSection, GROUP_CONCAT(uts.section) as sections, u.user_name ${studentCondition.select} FROM unit_test ut
        LEFT JOIN user u ON ut.created_by = u.id
        JOIN unit_test_section uts ON ut.id = uts.unit_test_id ${studentCondition.join}
        WHERE ut.deleted = 0  ${isAdmin?"":"and (ut.privacy is null or ut.privacy !=1)"} AND ut.unit_test_origin_id is null ${unittestCondition} 
        GROUP BY ut.id ${studentCondition.groupBy}
        ORDER BY FIELD(ut.package_level, "${[...PACKAGE_LEVEL_CONFIG].reverse().join('", "')}") DESC, ut.name, ut.id DESC
        LIMIT ${limit} OFFSET ${parseInt(page) * limit}`,
        [],
      )
      for (let i = 0; i < unitTests.length; i++) {
        const currentUnitTest = unitTests[i]
        const sections = await query<any>(
          `SELECT * FROM unit_test_section
          WHERE unit_test_id = ${currentUnitTest.id} AND deleted = 0`,
        )
        currentUnitTest.sections = sections.map((item: any) => ({
          id: item.id,
          name: item?.section ? item.section.split(',') : [],
        }))
      }

      // total templates
      const totalUnitTests: any[] = await query(
        `SELECT DISTINCT ut.id FROM unit_test ut
              JOIN unit_test_section uts ON ut.id = uts.unit_test_id ${studentCondition.join}
              WHERE ut.deleted = 0 ${isAdmin?"":"and (ut.privacy is null or ut.privacy !=1)"} AND ut.unit_test_origin_id is null ${unittestCondition}`,
      )

      // return data
      return res.status(200).json({
        unitTests,
        totalPages: Math.ceil(totalUnitTests.length / limit),
        currentPage: parseInt(page),
      })
    } catch (e: any) {
      console.log("e--------------",e)
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
