import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query, queryWithTransaction } from 'lib/db'
import { unitTestSources } from '@/constants/index'
import { PACKAGES } from '@/interfaces/constants'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  if (!session) {
    return res.status(403).end('Forbidden')
  }
  const user: any = session?.user
  if (user?.is_admin !=1) {
    return res.status(403).end('Forbidden')
  }
  //const user = { id: 'be6af6a4-42be-11ec-82a9-d8bbc10864fc' }

  switch (req.method) {
    case 'GET':
      return createUnitTestSystem()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function createUnitTestSystem() {
    const { id } = req.query
    if (!id) return res.status(400).json({ message: '`id` required', code:0 })
    try {
      const unitTests: any[] = await query<any[]>(
        `SELECT u.*FROM unit_test as u 
        WHERE u.deleted = 0 AND scope = 1 and u.id = ? LIMIT 1`,
        [id],
      )
      const data = unitTests[0]
      if (unitTests.length === 0) {
        return res.status(400).json({code:0, message: 'data not found' })
      }
      const sections: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section WHERE deleted = 0 AND unit_test_id = ?',
        [id],
      )
      if (sections.length > 0) {
        data.sections = sections
      }
      const sectionIds = sections.map((m) => m.id)
      const parts: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section_part WHERE deleted = 0 AND unit_test_section_id IN (?)',
        [sectionIds],
      )
      if (parts.length > 0) {
        for (const item of sections) {
          item.parts = parts.filter((m) => m.unit_test_section_id === item.id)
        }
      }
      const partIds = parts.map((m) => m.id)
      const questions: any[] = await query<any[]>(
        `SELECT utspq.unit_test_section_part_id, q.* FROM unit_test_section_part_question utspq
         LEFT JOIN question q ON utspq.question_id = q.id
         WHERE utspq.deleted = 0 AND utspq.unit_test_section_part_id IN (?)`,
        [partIds],
      )
      if (questions.length > 0) {
        for (const item of parts) {
          item.questions = questions.filter(
            (m) => m.unit_test_section_part_id === item.id,
          )
        }
      }
      const dateNow = new Date()
    const startDate = data?.start_date ? new Date(data.start_date) : null
    const endDate = data?.end_date ? new Date(data.end_date) : null
      const listIndex: any = { index: 0, sections: [] }
      let index = 0
      const listQ: any[] = [
        () => [
          `INSERT INTO unit_test(
            template_id, 
            name, 
            template_level_id, 
            total_question, 
            time, 
            is_publish, 
            created_by, 
            created_date, 
            total_point, 
            start_date, 
            end_date, 
            scope, 
            unit_type,
            series_id,
            unit_test_origin_id,
            unit_test_type,
            package_level,
            is_international_exam,
            source
          ) 
          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [
            data.template_id,
            data.name,
            data.template_level_id,
            data.total_question ?? 0,
            data.time,
            data.is_publish ?? 0,
            user.id,
            dateNow,
            data.total_point,
            startDate,
            endDate,
            0,
            [0,1].includes(data?.unit_type) ? parseInt(data.unit_type) : 0,
            data.series_id || null,
            data.unit_test_origin_id || null,
            data.unit_test_type ?? null,
            data.package_level,
            data.is_international_exam || 0,
            unitTestSources.I_TEST,
          ],
        ],
      ]
      listQ.push((r1: any, r2: any) => {
        return [
          `DELETE FROM unit_test_section_mapping WHERE unit_test_origin_id = ?`,
          [
            data.id
          ]
        ]
      })
      index ++ ;
      data.sections.forEach((section: any, sectionIndex: number) => {
        listQ.push((r1: any, r2: any) => {
          return [
            `INSERT INTO unit_test_section(unit_test_id, section, modified_date) VALUES(?,?,?)`,
            [
              r2[listIndex.index].insertId,
              section?.section ? section.section : '',
              dateNow,
            ],
          ]
        })
        index++
        listIndex.sections.push({ index, parts: [] })
        listQ.push((r1: any, r2: any) => {
          return [
            `INSERT INTO unit_test_section_mapping(unit_test_id,unit_test_section_id,unit_test_origin_id, unit_test_section_origin_id) VALUES(?,?,?,?)`,
            [
              r2[listIndex.index].insertId,
              r2[listIndex.sections[sectionIndex].index].insertId,
              id,
              section.id,
            ],
          ]
        })
        index++
        section.parts.forEach((part: any, partIndex: number) => {
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section_part(unit_test_section_id, name, question_types, total_question, points, modified_date) 
               VALUES(?,?,?,?,?,?)`,
              [
                r2[listIndex.sections[sectionIndex].index].insertId,
                part.name,
                part.question_types,
                part.total_question,
                part.points,
                dateNow,
              ],
            ]
          })
          index++
          listIndex.sections[sectionIndex].parts.push({ index})
          
          //clone question;
          part.questions.forEach((questionClone: any, questionCloneIndex: number) => {
            if(questionClone.scope==1){ //create by teacher
              listQ.push((r1: any, r2: any) => {
                console.log("r2---", r2.length)
                return [
                  `INSERT INTO question(
                    publisher,
                    test_type,
                    series,
                    grade,
                    cerf,
                    format,
                    types,
                    skills,
                    question_type,
                    level,
                    \`group\`,
                    parent_question_description,
                    parent_question_text,
                    question_description,
                    question_text,
                    image,
                    video,
                    audio,
                    answers,
                    correct_answers,
                    audio_script,
                    points,
                    status,
                    deleted,
                    created_date,
                    created_by,
                    scope,
                    total_question,
                    question_origin_id
                    ) 
                   VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
                  [
                    questionClone.publisher,
                    questionClone.test_type,
                    questionClone.series,
                    questionClone.grade,
                    questionClone.cerf,
                    questionClone.format,
                    questionClone.types,
                    questionClone.skills,
                    questionClone.question_type,
                    questionClone.level,
                    questionClone.group,
                    questionClone.parent_question_description,
                    questionClone.parent_question_text,
                    questionClone.question_description,
                    questionClone.question_text,
                    questionClone.image,
                    questionClone.video,
                    questionClone.audio,
                    questionClone.answers,
                    questionClone.correct_answers,
                    questionClone.audio_script,
                    questionClone.points,
                    questionClone.status,
                    0,
                    dateNow,
                    user.id,
                    0,
                    questionClone.total_question,
                    questionClone.id
                  ],
                ]
              })
              index++
              questionClone.indexId = index;
              console.log("questionClone.indexId---", index)
            }

          })
          // console.log("part.questions-------",part.questions)
          listQ.push((r11: any, r22: any) => {
              return [
              `INSERT INTO unit_test_section_part_question(unit_test_section_part_id, question_id, modified_date) VALUES ?`,
              [
                part.questions.map((question: any) => {
                  const questionId = (question.indexId?r22[question.indexId].insertId:question?.id);
                  return [
                    r22[listIndex.sections[sectionIndex].parts[partIndex].index]
                      .insertId,
                      questionId
                    ,
                    dateNow,
                  ]
                }),
              ],
            ]
          })
          index++
        })
      })
      const result: any[] = await queryWithTransaction<any>(listQ)
      if (result[0].affectedRows === 0) {
        return res
          .status(400)
          .json({ code: 0, message: `Create unsuccessfull` })
      }
      return res
        .status(200)
        .json({ code: 1, id: result[0].insertId, userId: user.id })

    } catch (e: any) {
      res.status(500).json({ message: e.message, code:0 })
    }
  }
}

export default handler
