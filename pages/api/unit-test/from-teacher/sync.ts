import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query, queryWithTransaction } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  if (!session) {
    return res.status(403).end('Forbidden')
  }
  const user: any = session?.user
  if (user?.is_admin != 1) {
    return res.status(403).end('Forbidden')
  }
  //const user = { id: 'be6af6a4-42be-11ec-82a9-d8bbc10864fc' }

  switch (req.method) {
    case 'GET':
      return syncUnitTestSystem()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function syncUnitTestSystem() {
    const { id } = req.query
    if (!id) return res.status(400).json({ message: '`id` required', code: 0 })
    try {
      //unit_test
      const unitTests: any[] = await query<any[]>(
        `SELECT u.*FROM unit_test as u 
        WHERE u.deleted = 0 AND scope = 1 and u.id = ? LIMIT 1`,
        [id],
      )
      const data = unitTests[0]
      if (unitTests.length === 0) {
        return res.status(400).json({ code: 0, message: 'data not found' })
      }

      //check mapping.
      const section_mapping: any[] = await query<any[]>(
        `SELECT * FROM unit_test_section_mapping as m
        join unit_test as u on m.unit_test_id = u.id
        WHERE u.deleted = 0 and m.unit_test_origin_id = ?`,
        [id],
      )
      if (!section_mapping || section_mapping.length == 0) {
        return res
          .status(400)
          .json({
            code: 0,
            message: 'Không tìm thấy đề thi hệ thống',
          })
      }
      const unit_test_system_id = section_mapping[0].unit_test_id
      const unitTestsSystems: any[] = await query<any[]>(
        `SELECT u.*FROM unit_test as u 
        WHERE u.deleted = 0 AND scope = 0 and u.id = ? LIMIT 1`,
        [unit_test_system_id],
      )
      if (unitTestsSystems.length === 0) {
        return res.status(400).json({ code: 0, message: 'Không tìm thấy đề thi hệ thống!' })
      }
      const dataSystem = unitTestsSystems[0]

      // section
      // teacher
      const sections: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section WHERE deleted = 0 AND unit_test_id = ?',
        [id],
      )
      if (sections.length > 0) {
        data.sections = sections
      }
      const sectionIds = sections.map((m) => m.id)
      const parts: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section_part WHERE deleted = 0 AND unit_test_section_id IN (?)',
        [sectionIds],
      )
      if (parts.length > 0) {
        for (const item of sections) {
          item.parts = parts.filter((m) => m.unit_test_section_id === item.id)
        }
      }
      const partIds = parts.map((m) => m.id)
      const questions: any[] = await query<any[]>(
        `SELECT utspq.unit_test_section_part_id, q.* FROM unit_test_section_part_question utspq
         LEFT JOIN question q ON utspq.question_id = q.id
         WHERE utspq.deleted = 0 AND utspq.unit_test_section_part_id IN (?)`,
        [partIds],
      )
      if (questions.length > 0) {
        for (const item of parts) {
          item.questions = questions.filter(
            (m) => m.unit_test_section_part_id === item.id,
          )
        }
      }

      //system
      const sectionSystem: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section WHERE deleted = 0 AND unit_test_id = ?',
        [unit_test_system_id],
      )
      // console.log("sectionSystem------------------------------------------------",sectionSystem)
      if (sectionSystem.length > 0) {
        dataSystem.sections = sectionSystem
      }
      const sectionSystemIds = sectionSystem.map((m) => m.id)
      const partSystem: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section_part WHERE deleted = 0 AND unit_test_section_id IN (?)',
        [sectionSystemIds],
      )
      // console.log("partSystem-----------------------",partSystem)
      if (partSystem.length > 0) {
        for (const item of sectionSystem) {
          item.parts = partSystem.filter(
            (m) => m.unit_test_section_id === item.id,
          )
        }
      }
      const partSystemIds = partSystem.map((m) => m.id)
      const questionSystem: any[] = await query<any[]>(
        `SELECT utspq.id as unit_test_section_part_question_id, utspq.unit_test_section_part_id, q.* FROM unit_test_section_part_question utspq
         LEFT JOIN question q ON utspq.question_id = q.id
         WHERE utspq.deleted = 0 AND utspq.unit_test_section_part_id IN (?)`,
        [partSystemIds],
      )
      if (questionSystem.length > 0) {
        for (const item of partSystem) {
          item.questions = questionSystem.filter(
            (m) => m.unit_test_section_part_id === item.id,
          )
        }
      }
      const dateNow = new Date()
      const startDate = data?.start_date ? new Date(data.start_date) : null
      const endDate = data?.end_date ? new Date(data.end_date) : null
      // update unit_test
      const listIndex: any = { index: 0, sections: [] }
      let index = 0
      const listQ: any[] = [
        () => [
          `UPDATE unit_test SET 
            template_id = ?, 
            name = ?, 
            template_level_id = ?, 
            total_question = ?, 
            time = ?, 
            is_publish = ?, 
            total_point = ?, 
            start_date = ?, 
            end_date = ?, 
            unit_type = ?,
            series_id = ?,
            updated_by = ?,
            updated_date = ?
            WHERE id = ? `,
          [
            data.template_id,
            data.name,
            data.template_level_id,
            data.total_question ?? 0,
            data.time,
            data.is_publish ?? 0,
            data.total_point,
            startDate,
            endDate,
            [0, 1].includes(data?.unit_type) ? parseInt(data.unit_type) : 0,
            data.series_id || null,
            user.id,
            dateNow,
            dataSystem.id,
          ],
        ],
      ]

      //delete mapping
      listQ.push((r1: any, r2: any) => {
        return [
          `DELETE FROM unit_test_section_mapping WHERE unit_test_origin_id = ? `,
          [
            id
          ]
        ]
      })
      index++
      const listSectionSystemHandle:any[] = []
      data.sections.forEach((section: any, sectionIndex: number) => {
        const sectionSystemMapId = section_mapping.find(m=>m.unit_test_section_origin_id == section.id)?.unit_test_section_id;
        const sectionSystem = dataSystem.sections.find(
          (m: any) => m.id == sectionSystemMapId,
        )
        //have section
        if (sectionSystem) {
          listSectionSystemHandle.push(sectionSystem.id)
          listQ.push((r1: any, r2: any) => {
            return [
              `UPDATE unit_test_section SET section = ?, modified_date = ? WHERE id = ?`,
              [section.section, dateNow, sectionSystem.id],
            ]
          })
          index++
          listIndex.sections.push({ index, parts: [] })

          //mapping
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section_mapping(unit_test_id,unit_test_section_id,unit_test_origin_id, unit_test_section_origin_id) VALUES(?,?,?,?)`,
              [
                unit_test_system_id,
                sectionSystemMapId,
                id,
                section.id,
              ],
            ]
          })
          index++


          const partSection =section.parts[0];
          const partSectionSystem =sectionSystem.parts[0];
          if (partSection) {
            listQ.push((r1: any, r2: any) => {
              return [
                `UPDATE unit_test_section_part SET name = ?, question_types = ?, total_question = ?, points = ?, modified_date =?  WHERE id = ?`,
                [
                  partSection.name,
                  partSection.question_types,
                  partSection.total_question,
                  partSection.points,
                  dateNow,
                  partSectionSystem?.id,
                ],
              ]
            })
            index++
            listQ.push((r1: any, r2: any) => {
              return [
                `UPDATE unit_test_section_part_question SET deleted = 1 WHERE unit_test_section_part_id = ?`,
                [partSectionSystem?.id],
              ]
            })
            index++
            const questionSystem = partSectionSystem.questions
            const questions = partSection.questions
            const questionsAddIds: any[] = []
            const questionsUpdate: any[] = []
            const questionWillBeClone: any[] = []
            questions.forEach((question: any, indexQuestion:number) => {
              //question teacher
              if (question.scope == 1) {
                const questionClone = questionSystem.find(
                  (m: any) =>
                    m.scope == 0 && m.question_origin_id == question.id,
                )
                if (questionClone) {
                  questionsUpdate.push({
                    ...question,
                    id: questionClone.id,
                    prioritySort:indexQuestion
                  })
                  questionsAddIds.push({id:questionClone.id, prioritySort:indexQuestion})
                } else {
                  questionWillBeClone.push({ ...question, prioritySort:indexQuestion })
                }
              } else {
                // question system create
                // question not found
                // if (
                //   questionSystem.findIndex((m: any) => m.id == question.id) ==
                //   -1
                // ) {
                //   questionsAddIds.push(question.id)
                // }
                questionsAddIds.push({id:question.id, prioritySort:indexQuestion})
              }
            })

            //update question content
            questionsUpdate.forEach((item: any) => {
              listQ.push((r1: any, r2: any) => {
                return [
                  `UPDATE question SET publisher = ?, test_type = ?, series = ?, grade = ?, cerf = ?,
         format = ?, types = ?, skills = ?, question_type = ?, level = ?, \`group\` = ?, 
         question_description = ?, question_text = ?, image = ?, video = ?, audio = ?, 
         answers = ?, correct_answers = ?, points = ?, parent_question_description = ?, 
         parent_question_text = ?, audio_script = ?, total_question = ?, updated_date= ?, updated_by = ? WHERE id = ?`,
                  [
                    item.publisher ?? 'NA',
                    item.test_type,
                    item.series ?? 1,
                    item.grade,
                    item.cerf ?? 'NA',
                    item.format ?? 'NA',
                    item.types ?? 'NA',
                    item.skills,
                    item.question_type,
                    item.level ?? 'NA',
                    item.group,
                    item.question_description,
                    item.question_text,
                    item.image,
                    item.video,
                    item.audio,
                    item.answers,
                    item.correct_answers,
                    item.points ?? 0,
                    item.parent_question_description,
                    item.parent_question_text,
                    item.audio_script,
                    item.total_question,
                    dateNow,
                    user.id,
                    item.id,
                  ],
                ]
              })
              index++
            })
            // add to unit_test_section_part_question

            //clone question 
            questionWillBeClone.forEach(
              (questionClone: any, questionCloneIndex: number) => {
                listQ.push((r1: any, r2: any) => {
                  return [
                    `INSERT INTO question(
                    publisher,
                    test_type,
                    series,
                    grade,
                    cerf,
                    format,
                    types,
                    skills,
                    question_type,
                    level,
                    \`group\`,
                    parent_question_description,
                    parent_question_text,
                    question_description,
                    question_text,
                    image,
                    video,
                    audio,
                    answers,
                    correct_answers,
                    audio_script,
                    points,
                    status,
                    deleted,
                    created_date,
                    created_by,
                    scope,
                    total_question,
                    question_origin_id) 
                   VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
                    [
                      questionClone.publisher,
                      questionClone.test_type,
                      questionClone.series,
                      questionClone.grade,
                      questionClone.cerf,
                      questionClone.format,
                      questionClone.types,
                      questionClone.skills,
                      questionClone.question_type,
                      questionClone.level,
                      questionClone.group,
                      questionClone.parent_question_description,
                      questionClone.parent_question_text,
                      questionClone.question_description,
                      questionClone.question_text,
                      questionClone.image,
                      questionClone.video,
                      questionClone.audio,
                      questionClone.answers,
                      questionClone.correct_answers,
                      questionClone.audio_script,
                      questionClone.points,
                      questionClone.status,
                      0,
                      dateNow,
                      user.id,
                      0,
                      questionClone.total_question,
                      questionClone.id,
                    ],
                  ]
                })
                index++
                questionClone.indexId = index
              },
            )

            questionsAddIds.push(... questionWillBeClone.map((question: any) => {
              return {prioritySort:question.prioritySort, indexId:question.indexId}
            }),)
            questionsAddIds.sort((a, b)=> a.prioritySort - b.prioritySort)

            if(questionsAddIds.length>0){
              listQ.push((r11: any, r22: any) => {
                return [
                  `INSERT INTO unit_test_section_part_question (unit_test_section_part_id, question_id, modified_date ) VALUES ? `,
                  [
                    questionsAddIds.map((question: any) => {
                      const questionId = question.indexId
                        ? r22[question.indexId].insertId
                        : question?.id
                    return [
                      partSectionSystem.id,
                      questionId,
                      dateNow,
                    ]
                    })
                  ],
                ]
              })
              index ++;
            }
            
          }
        } else {
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section(unit_test_id, section, modified_date) VALUES(?,?,?)`,
              [
                unit_test_system_id,
                section?.section ? section.section : '',
                dateNow,
              ],
            ]
          })
          index++
          listIndex.sections.push({ index, parts: [] })
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section_mapping(unit_test_id,unit_test_section_id,unit_test_origin_id, unit_test_section_origin_id) VALUES(?,?,?,?)`,
              [
                unit_test_system_id,
                r2[listIndex.sections[sectionIndex].index].insertId,
                section.unit_test_id,
                section.id,
              ],
            ]
          })
          index++
          section.parts.forEach((part: any, partIndex: number) => {
            listQ.push((r1: any, r2: any) => {
              return [
                `INSERT INTO unit_test_section_part(unit_test_section_id, name, question_types, total_question, points, modified_date) 
                 VALUES(?,?,?,?,?,?)`,
                [
                  r2[listIndex.sections[sectionIndex].index].insertId,
                  part.name,
                  part.question_types,
                  part.total_question,
                  part.points,
                  dateNow,
                ],
              ]
            })
            index++
            listIndex.sections[sectionIndex].parts.push({ index })
  
            //clone question;
            part.questions.forEach(
              (questionClone: any, questionCloneIndex: number) => {
                if (questionClone.scope == 1) {
                  //create by teacher
                  listQ.push((r1: any, r2: any) => {
                    return [
                      `INSERT INTO question(
                      publisher,
                      test_type,
                      series,
                      grade,
                      cerf,
                      format,
                      types,
                      skills,
                      question_type,
                      level,
                      \`group\`,
                      parent_question_description,
                      parent_question_text,
                      question_description,
                      question_text,
                      image,
                      video,
                      audio,
                      answers,
                      correct_answers,
                      audio_script,
                      points,
                      status,
                      deleted,
                      created_date,
                      created_by,
                      scope,
                      total_question,
                      question_origin_id
                      ) 
                     VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
                      [
                        questionClone.publisher,
                        questionClone.test_type,
                        questionClone.series,
                        questionClone.grade,
                        questionClone.cerf,
                        questionClone.format,
                        questionClone.types,
                        questionClone.skills,
                        questionClone.question_type,
                        questionClone.level,
                        questionClone.group,
                        questionClone.parent_question_description,
                        questionClone.parent_question_text,
                        questionClone.question_description,
                        questionClone.question_text,
                        questionClone.image,
                        questionClone.video,
                        questionClone.audio,
                        questionClone.answers,
                        questionClone.correct_answers,
                        questionClone.audio_script,
                        questionClone.points,
                        questionClone.status,
                        0,
                        dateNow,
                        user.id,
                        0,
                        questionClone.total_question,
                        questionClone.id,
                      ],
                    ]
                  })
                  index++
                  questionClone.indexId = index
                }
              },
            )
            // console.log("part.questions-------",part.questions)
            if(part.questions.length>0){
              listQ.push((r11: any, r22: any) => {
                return [
                  `INSERT INTO unit_test_section_part_question ( unit_test_section_part_id, question_id, modified_date) VALUES ?`,
                  [
                    part.questions.map((question: any) => {
                      const questionId = question.indexId
                        ? r22[question.indexId].insertId
                        : question?.id
                      return [
                        r22[listIndex.sections[sectionIndex].parts[partIndex].index]
                          .insertId,
                        questionId,
                        dateNow,
                      ]
                    }),
                  ],
                ]
              })
              index++
            }
            
          })
        }
      })
      const listSectionDelete = dataSystem.sections.filter((m:any)=> listSectionSystemHandle.findIndex((x:any)=> m.id == x) == -1  ).map((m:any)=> m.id);
      if(listSectionDelete && listSectionDelete.length>0){
        listQ.push((r1: any, r2: any) => {
          return [
            `UPDATE unit_test_section SET deleted = 1 WHERE id IN (?)`,
            [listSectionDelete],
          ]
        })
        index++
      }
      const result: any[] = await queryWithTransaction<any>(listQ)
      if (result[0].affectedRows === 0) {
        return res
          .status(400)
          .json({ code: 0, message: `Create unsuccessfull` })
      }
      return res
        .status(200)
        .json({ code: 1, id: unit_test_system_id, userId: user.id })
    } catch (e: any) {
      console.log("synce unit test from teacher =============", e)
      res.status(500).json({ message: e.message, code: 0 })
    }
  }
}

export default handler
