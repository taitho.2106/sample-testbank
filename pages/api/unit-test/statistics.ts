import { NextApiRequest, NextApiResponse } from "next";

import { commonDeleteStatus } from '@/constants/index';
import { dayjs } from '@/utils/date';

import createHandler, { apiResponseDTO } from "../../../lib/apiHandle";
import prisma from '../../../lib/prisma';

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse) => {
            const currentDate = dayjs()
            const oldDate = dayjs().subtract(7,'d')
            
            const totalUnitTest = await prisma.unit_test.count({
                where: {
                    deleted: commonDeleteStatus.ACTIVE,
                    scope: 0,
                    privacy: 0,
                    unit_test_origin_id: null,
                    OR: [
                        {
                            is_international_exam: 1,
                            series_id: null,
                        },
                        {
                            is_international_exam: 0,
                            series_id: {
                                not: null
                            }
                        }
                    ]
                }
            });

            const totalNewCreatedUnitTest = await  prisma.unit_test.count({
                where: {
                    deleted: commonDeleteStatus.ACTIVE,
                    scope: 0,
                    privacy: 0,
                    unit_test_origin_id: null,
                    OR: [
                        {
                            is_international_exam: 1,
                            series_id: null,
                        },
                        {
                            is_international_exam: 0,
                            series_id: {
                                not: null
                            }
                        }
                    ],
                    updated_date: {
                        gte: oldDate.startOf('d').toDate(),
                        lte: currentDate.endOf('d').toDate(),
                    }

                }
            });

            return res.status(200).json(apiResponseDTO(true, {total: totalUnitTest, totalNewUnitTest: totalNewCreatedUnitTest}));
        }
    },
});

export default handler