import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
import { query, queryWithTransaction } from 'lib/db'
import { userInRight } from 'utils'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session?.user
  const isAdmin = userInRight([], session)

  switch (req.method) {
    case 'POST':
      return updatePrivacy()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function updatePrivacy() {
    if (!isAdmin) {
      return res.status(403).end('Forbidden')
    }
    const data: any = req.body
    const id = data?.id
    if (!id) return res.status(400).json({ message: 'data not found' })
    const privacy = data?.privacy || 0
    const dateNow = new Date()
    try {
      const listQ: any[] = [
        (r1: any, r2: any) =>  [`UPDATE unit_test SET privacy = ?, updated_by = ?, updated_date = ? WHERE id = ?`,
          [privacy, user.id, dateNow, id]],
      ]
      let questionIds: any[] = []
      const questions: any[] = await query<any[]>(
        `select q.id from unit_test u
        join unit_test_section uts on u.id = uts.unit_test_id
        join unit_test_section_part  utsp on uts.id = utsp.unit_test_section_id
        join unit_test_section_part_question utspq on utsp.id = utspq.unit_test_section_part_id 
        join question q on utspq.question_id = q.id 
        where u.id = ? and u.deleted = 0 and uts.deleted = 0 and utsp.deleted = 0 and utspq.deleted = 0 and q.deleted = 0 and q.scope= 0`,
        [id],
      )
      if (questions.length > 0) {
        questionIds = questions.map((m) => m.id)
        listQ.push(() => [
          `UPDATE question SET privacy = ? WHERE id in (?)`,
          [privacy, questionIds],
        ])
      }
      const result: any[] = await queryWithTransaction<any>(listQ)
      if (result[0].affectedRows === 0) {
        return res.status(400).json({ message: `udate unsuccess` })
      }
      return res.status(200).json({ message: 'success', data: { ...data } })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
