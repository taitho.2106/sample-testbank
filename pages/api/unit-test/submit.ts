import { NextApiRequest, NextApiResponse } from "next";

import { commonDeleteStatus } from "@/constants/index";
import { STATUS_CODE_EXAM } from "@/interfaces/constants";
import { getDateStatusFromStartEnd } from "@/utils/date";
import createHandler, { apiResponseDTO, errorResponseDTO, roleConfig } from "lib/apiHandle";
import prisma from "lib/prisma";
import { checkStudentAccessUnitTestAssigned } from "services/unitTest";

const handler: any = createHandler({
    'POST': {
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session?.user;
            const data = req.body;

            const unitTest = await prisma.unit_test.findFirst({
                where: {
                    id: data.unit_test_id,
                    deleted: commonDeleteStatus.ACTIVE,
                }
            });
            if (!unitTest) {
                return res.status(404).json(errorResponseDTO(404, 'Unit test not found!'));
            }

            if (data.third_party_code) {
                const thirdParty = await prisma.third_party.findFirst({ where: { id: data.third_party_code } });
                if (!thirdParty) {
                    return res.status(400).json(errorResponseDTO(400, 'third_party_code is incorrect'));
                }
            } else {
                if (!student) {
                    return res.status(401).json(errorResponseDTO(401, 'Unauthorized'));
                }
            }

            if (data.class_unit_test_id) {
                const unitTestAssign = await prisma.classUnitTest.findUnique({
                    where: { id: data.class_unit_test_id }
                });

                if (!(await checkStudentAccessUnitTestAssigned(student?.id, unitTestAssign))) {
                    return res.status(400).json(errorResponseDTO(400, 'Unit test not assign for student'));
                }

                const examStatus = getDateStatusFromStartEnd(unitTestAssign.start_date, unitTestAssign.end_date);
                if (examStatus !== STATUS_CODE_EXAM.ONGOING) {
                    return res.status(400).json(errorResponseDTO(400, 'Invalid submit time!'));
                }
            }

            const dateNow = new Date()
            const unitTestResult = await prisma.unit_test_result.create({
                data: {
                    unit_test_id: data.unit_test_id,
                    unit_test_name: unitTest.name,
                    point: data.point,
                    max_point: data.max_point,
                    test_time: data.time,
                    third_party_code: data.third_party_code,
                    third_party_user_id: data.third_party_user_id,
                    third_party_device_id: data.third_party_device_id,
                    deleted: 0,
                    created_by: student?.id,
                    created_date: dateNow,
                    class_unit_test_id: data.class_unit_test_id,
                    user_id: student?.id,
                    user_name: student?.full_name, // save duplicate data for old logic display name of user
                    unit_test_section_result: {
                        create: data.sections.map((section: any) => ({
                            unit_test_section_id: section.unit_test_section_id,
                            name: section.section_name,
                            point: section.point,
                            max_point: section.max_point,
                            deleted: 0,
                            created_by: student?.id,
                            created_date: dateNow,
                            question_result: {
                                createMany: {
                                    data: section.questions.map((question: any) => ({
                                        question_id: question.question_id,
                                        total_question: question.total_question,
                                        point: question.point,
                                        max_point: question.max_point,
                                        user_answer: question.user_answer,
                                        deleted: 0,
                                        created_by: student?.id,
                                        created_date: dateNow,
                                    }))
                                }
                            }
                        }))
                    }
                }
            });

            return res.status(200).json(apiResponseDTO(true, unitTestResult));
        }
    },
});

export default handler