import { NextApiRequest, NextApiResponse } from "next";
import { sendEmail } from "services/email";

import { getContentEmailUnitTestInfo } from '@/constants/email-template/index';
import { initResource } from "@/hooks/useTranslation";
import { emailRegExp } from "@/interfaces/constants";
import dayjs from "dayjs";
import { getSession } from "next-auth/client";

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const locale = req.cookies.locale
    const { t } = initResource(locale);
    const session = await getSession({ req })
    if (!session) {
        return res.status(403).end('Forbidden')
    }
    switch (req.method) {
        case 'POST':
            const { unitTestURL, emails } = req.body;

            const emailList = (emails || '')
                        .split(',')
                        .filter((email: string) => !!email && emailRegExp.test(email.trim()));

            try {
                if (emailList.length) {
                    const { error }: any = await sendEmail({
                        to: emailList,
                        subject: `${dayjs().format('DD/MM/YYYY HH:mm')} - ${t('mess-handler')['send-test-desc']}`,
                        content: getContentEmailUnitTestInfo({ unitTestURL }),
                    });
                    if (error) {
                        return res.status(200).json({
                            error,
                            message: t('mess-handler')['send-test-fail'],
                            code: 400,
                        });
                    }
                }

                return res.json({
                    message: t('mess-handler')['send-test-success'],
                });
            } catch (error) {
                return res.status(500).json({
                    error,
                    message: t('mess-handler')['send-test-fail'],
                });
            }
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
}

export default handler;