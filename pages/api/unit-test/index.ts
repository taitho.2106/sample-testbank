import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { PACKAGES, USER_ROLES } from 'interfaces/constants'
import { query, queryWithTransaction } from 'lib/db'
import { userInRight } from 'utils'
import { IS_INTERNATIONAL_EXAM, unitTestSources, UNIT_TEST_TYPE } from '@/constants/index'
const checkIntergerGreaterThan0 = (num:any)=>{
  return  Number.isInteger(num) && num >0
}

const getDefaultPackageLevelForUnitTest = (isInternationalExam: boolean, unitTestType: number) => {
    if (isInternationalExam) {
        return PACKAGES.INTERNATIONAL.CODE;
    }

    if ([UNIT_TEST_TYPE.PRACTICE_UNIT_TEST, UNIT_TEST_TYPE.PRACTICE_LANGUAGE, UNIT_TEST_TYPE.PRACTICE_SKILL].includes(unitTestType)) {
        return PACKAGES.BASIC.CODE;
    }

    return PACKAGES.ADVANCE.CODE;
}

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session?.user
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const isAdmin = userInRight([], session)
  // if (!userInRight([USER_ROLES.Teacher], session)) {
  //   return res.status(403).end('Forbidden')
  // }
  // const user = { id: 'be6af6a4-42be-11ec-82a9-d8bbc10864fc' }

  switch (req.method) {
    case 'GET':
      return getUnitTest()
    case 'POST':
      return createUnitTest()
    case 'PUT':
      if (req.query?.type === 'datetime') return updateDateTimeUnitTest()
      return updateUnitTest()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getUnitTest() {
    const { page, limit } = req.query
    try {
      const results: any[] = await query<any[]>(
        `SELECT * FROM unit_test WHERE deleted = 0 
         ${user.is_admin ? '' : 'AND privacy !=1 AND created_by = ?'} 
         ORDER BY id desc
         LIMIT ${limit} OFFSET ${
          parseInt(page.toString()) * parseInt(limit.toString())
        }`,
        [user.id],
      )
      const totals = await query<any[]>(
        `SELECT COUNT(*) as total FROM unit_test WHERE deleted = 0 
         ${user.is_admin ? '' : 'AND privacy !=1 AND created_by = ?'}`,
        [user.id],
      )
      return res.json({
        data: results,
        totalRecords: totals[0].total,
      })
    } catch (e: any) {
      res.status(500).json({code:0, message: e.message })
    }
  }

  async function createUnitTest() {
    const data = req.body
    const dateNow = new Date()
    const startDate = data?.start_date ? new Date(data.start_date) : null
    const endDate = data?.end_date ? new Date(data.end_date) : null
    try {
      const listIndex: any = { index: 0, sections: [] }
      let index = 0
      const listQ: any[] = [
        () => [
          `INSERT INTO unit_test(
            template_id, 
            name, 
            template_level_id, 
            total_question, 
            time, 
            is_publish, 
            created_by, 
            created_date, 
            total_point, 
            start_date, 
            end_date, 
            scope, 
            unit_type,
            series_id,
            unit_test_origin_id,
            privacy,
            unit_test_type,
            package_level,
            is_international_exam,
            source
          ) 
          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [
            data.template_id,
            data.name,
            data.template_level_id,
            data.total_question ?? 0,
            data.time,
            data.is_publish ?? 0,
            user.id,
            dateNow,
            data.total_point,
            startDate,
            endDate,
            data?.scope || 0,
            [0, 1].includes(data?.unit_type) ? parseInt(data.unit_type) : 0,
            data.series_id || null,
            data.unit_test_origin_id || null,
            data.privacy||0,
            data.unit_test_type ?? null,
            data.package_level || getDefaultPackageLevelForUnitTest(data.is_international_exam === IS_INTERNATIONAL_EXAM, data.unit_test_type),
            data.is_international_exam || 0,
            unitTestSources.I_TEST,
          ],
        ],
      ]
      data.sections.forEach((section: any, sectionIndex: number) => {
        listQ.push((r1: any, r2: any) => {
          return [
            `INSERT INTO unit_test_section(unit_test_id, section, modified_date) VALUES(?,?,?)`,
            [
              r2[listIndex.index].insertId,
              section?.section ? section.section.join(',') : '',
              dateNow,
            ],
          ]
        })
        index++
        listIndex.sections.push({ index, parts: [] })
        section.parts.forEach((part: any, partIndex: number) => {
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section_part(unit_test_section_id, name, question_types, total_question, points, modified_date) 
               VALUES(?,?,?,?,?,?)`,
              [
                r2[listIndex.sections[sectionIndex].index].insertId,
                part.name,
                part.question_types,
                part.total_question,
                part.points,
                dateNow,
              ],
            ]
          })
          index++
          listIndex.sections[sectionIndex].parts.push({ index })
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section_part_question(unit_test_section_part_id, question_id, modified_date) VALUES ?`,
              [
                part.questions.map((question: any) => [
                  r2[listIndex.sections[sectionIndex].parts[partIndex].index]
                    .insertId,
                  question,
                  dateNow,
                ]),
              ],
            ]
          })
          index++
        })
      })

      const result = await queryWithTransaction<any>(listQ)

      return res.json({code:1, id: result[0].insertId, userId: user.id })
    } catch (e: any) {
      res.status(500).json({code:0, message: e.message })
    }
  }

  async function updateDateTimeUnitTest() {
    const data: any = req.body
    const id = data?.id
    if (!id) return res.status(400).json({ message: 'data not found' })
    const time = data?.time
    const startDate = data?.start_date ? new Date(data.start_date) : null
    const endDate = data?.end_date ? new Date(data.end_date) : null
    try {
      await query<any>(
        `
        UPDATE unit_test 
        SET time = ?, start_date = ?, end_date = ?
        WHERE id = ?
      `,
        [time, startDate, endDate, id],
      )
      return res.status(200).json({ status: 200, data: { ...data } })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }

  async function updateUnitTest() {
    const data: any = req.body
    const dateNow = new Date()
    const startDate = data?.start_date ? new Date(data.start_date) : null
    const endDate = data?.end_date ? new Date(data.end_date) : null
    const unitType = [0, 1].includes(data?.unit_type) ? data.unit_type : 0
    try {
      const unitTest = await query<any>(
        `SELECT 1 FROM unit_test WHERE deleted = 0 ${
          isAdmin
            ? ''
            : isSystem
            ? ` AND scope = 0`
            : `AND created_by = '${user.id}'`
        } AND id = ? LIMIT 1`,
        [data.id],
      )
      if (unitTest.length === 0) {
        return res.status(400).json({ message: 'data not found' })
      }
      const listIndex: any = { index: 0, sections: [] }
      let index = 1
      const listQ: any[] = [
        () => [
          `UPDATE unit_test SET name = ?, template_level_id = ?, total_question = ?, time = ?, total_point = ?, start_date = ?, end_date = ?, unit_type = ?, series_id = ?, privacy = ?, unit_test_type = ?, is_international_exam = ?
           WHERE id = ?`,
          [
            data.name,
            data.template_level_id,
            data.total_question ?? 0,
            data.time,
            data.total_point,
            startDate,
            endDate,
            unitType,
            data.series_id,
            data.privacy||0,
            data.unit_test_type ?? null,
            data.is_international_exam || 0,
            data.id,
          ],
        ],
      ]
      const listSectionId:any[] = data.sections.map((m:any)=>m.section_old_id).filter((m:any)=> checkIntergerGreaterThan0(m)) || []
      if(listSectionId.length >0){
        listQ.push(() => {
          return [
            `DELETE FROM unit_test_section WHERE unit_test_id = ? and id not in(?)`,
            [data.id,listSectionId],
          ]
        })
      }else{
        listQ.push(() => {
          return [
            `DELETE FROM unit_test_section WHERE unit_test_id = ?`,
            [data.id],
          ]
        })
      }
      
      data.sections.forEach((section: any, sectionIndex: number) => {

        //update section
        if(section.section_old_id !=0 && Number.isInteger(section.section_old_id)){
          listQ.push((r1: any, r2: any) => {
            return [
              `UPDATE unit_test_section SET section = ?, modified_date = ? WHERE id = ?`,
              [section?.section ? section.section.join(',') : '', dateNow, section.section_old_id],
            ]
          })
          index++
          listIndex.sections.push({ index, parts: [] })
          section.parts.forEach((part: any, partIndex: number) => {
            listQ.push((r1: any, r2: any) => {
              return [
                `UPDATE unit_test_section_part SET name = ?, question_types = ?, total_question = ?, points = ?, modified_date =?  WHERE id = ?`,
                [
                  part.name,
                  part.question_types,
                  part.total_question,
                  part.points,
                  dateNow,
                  part.id,
                ],
              ]
            })
            index++
            listIndex.sections[sectionIndex].parts.push({ index })
            listQ.push((r1: any, r2: any) => {
              return [
                `UPDATE unit_test_section_part_question SET deleted = 1 WHERE unit_test_section_part_id = ?`,
                [part.id],
              ]
            })
            index++
            listQ.push((r1: any, r2: any) => {
              return [
                `INSERT INTO unit_test_section_part_question(unit_test_section_part_id, question_id, modified_date) VALUES ?`,
                [
                  part.questions.map((question: any) => [
                    part.id,
                    question,
                    dateNow,
                  ]),
                ],
              ]
            })
            index++
          })
        }else{ //insert section
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section(unit_test_id, section, modified_date) VALUES(?,?,?)`,
              [
                data.id,
                section?.section ? section.section.join(',') : '',
                dateNow,
              ],
            ]
          })
          index++
          listIndex.sections.push({ index, parts: [] })
          section.parts.forEach((part: any, partIndex: number) => {
            listQ.push((r1: any, r2: any) => {
              return [
                `INSERT INTO unit_test_section_part(unit_test_section_id, name, question_types, total_question, points, modified_date) 
                 VALUES(?,?,?,?,?,?)`,
                [
                  r2[listIndex.sections[sectionIndex].index].insertId,
                  part?.name || '',
                  part.question_types,
                  part.total_question,
                  part.points,
                  dateNow,
                ],
              ]
            })
            index++
            listIndex.sections[sectionIndex].parts.push({ index })
            listQ.push((r1: any, r2: any) => {
              return [
                `INSERT INTO unit_test_section_part_question(unit_test_section_part_id, question_id, modified_date) VALUES ?`,
                [
                  part.questions.map((question: any) => [
                    r2[listIndex.sections[sectionIndex].parts[partIndex].index]
                      .insertId,
                    question,
                    dateNow,
                  ]),
                ],
              ]
            })
            index++
          })
        }
        
      })
      const result = await queryWithTransaction<any>(listQ)
      return res.json({ ...data, id: data.id, userId: user.id, code:1 })
    } catch (e: any) {
      console.log("er------",e)
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
