import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })

  const user: any = session?.user
  if (user.is_admin !== 1) {
    return res.status(403).end('Forbidden')
  }
  //const user = { id: 'be6af6a4-42be-11ec-82a9-d8bbc10864fc' }

  switch (req.method) {
    case 'POST':
      return updateQuestionSeries()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function updateQuestionSeries() {
    const { id } = req.query
    if (!id) return res.status(400).json({ message: '`id` required' })
    try {
      // const unitTests: any[] = await query<any[]>(
      //     // `SELECT * FROM unit_test as u
      //     // WHERE u.deleted = 0 AND u.id = ? and scope = 0`,
      //     `SELECT s.deleted as seriesDeleted, u.* FROM unit_test as u
      //     left join series as s on u.series_id = s.id
      //     WHERE u.deleted = 0 AND u.id = ? and scope = 0`,
      //     [id],
      // )
      const unitTests: any[] = await query<any[]>(
        `SELECT u.* FROM unit_test as u
            WHERE u.deleted = 0 AND u.id = ? and scope = 0`,
        [id],
      )
      if (unitTests.length == 0) {
        return res.status(400).json({
          code: 0,
          message: 'Không tìm thấy đề thi',
        })
      }
      const unitTest = unitTests[0]
      if (!unitTest.series_id || unitTest.series_id == 1) {
        return res.status(400).json({
          code: 0,
          message: 'Vui lòng cập nhật chương trình cho đề thi trước',
        })
      }
      const listSeries: any[] = await query<any[]>(
        `SELECT * FROM series
          WHERE deleted = 0 AND id = ?`,
        [unitTest.series_id],
      )
      if (listSeries.length == 0) {
        return res.status(400).json({
          code: 0,
          message: 'Vui lòng cập nhật chương trình cho đề thi trước',
        })
      }
      const listGrade: any[] = await query<any[]>(
        `SELECT * FROM grade
          WHERE deleted = 0 AND id = ?`,
        [unitTest.template_level_id],
      )
      if (listGrade.length == 0) {
        return res.status(400).json({
          code: 0,
          message: 'Vui lòng cập nhật khối lớp cho đề thi trước',
        })
      }
      const results: any[] = await query<any[]>(
        `select q.id from unit_test u
        join unit_test_section uts on u.id = uts.unit_test_id
        join unit_test_section_part  utsp on uts.id = utsp.unit_test_section_id
        join unit_test_section_part_question utspq on utsp.id = utspq.unit_test_section_part_id 
        join question q on utspq.question_id = q.id 
        where u.id = ? and u.deleted = 0 and uts.deleted = 0 and utsp.deleted = 0 and utspq.deleted = 0 and q.deleted = 0 and q.scope= 0 and (q.series = 1 or q.series is null)
        `,
        [id],
      )
      if (results.length > 0) {
        const ids = results.map((m) => m.id)
        const updateRes: any = await query(
          `UPDATE question SET series = '${unitTest.series_id}', updated_date =?, updated_by = ? WHERE id in (?)`,
          [new Date(), user.id, ids],
        )
        console.log('updateRes----', updateRes)
        if (updateRes.affectedRows != 0) {
          return res.status(200).json({
            code: 1,
            data: ids,
          })
        }
        return res.status(500).json({
          code: 0,
          message: 'Cập nhật không thành công',
        })
      } else {
        return res.status(400).json({
          code: 0,
          message: 'Không có câu hỏi có thể cập nhật',
        })
      }
    } catch (e: any) {
      console.log('exx -- updateQuestionSeries', e)
      res.status(500).json({ message: e.message, data: [], total: 0, code: 0 })
    }
  }
}

export default handler
