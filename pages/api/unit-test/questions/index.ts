import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })

  const user: any = session?.user
  if (user.is_admin !==1) {
    return res.status(403).end('Forbidden')
  }
  //const user = { id: 'be6af6a4-42be-11ec-82a9-d8bbc10864fc' }

  switch (req.method) {
    case 'GET':
      return getListQuestion()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getListQuestion() {
    console.log("req----", req.query)
    const { id,page, limit } = req.query
    if (!id) return res.status(400).json({code:0, message: '`id` required' })
    try {
      const pageInt = parseInt(page.toString())
      const limitInt = parseInt(limit.toString())
      const results: any[] = await query<any[]>(
        `select q.* from unit_test u
        join unit_test_section uts on u.id = uts.unit_test_id
        join unit_test_section_part  utsp on uts.id = utsp.unit_test_section_id
        join unit_test_section_part_question utspq on utsp.id = utspq.unit_test_section_part_id 
        join question q on utspq.question_id = q.id 
        where u.id = ? and u.deleted = 0 and uts.deleted = 0 and utsp.deleted = 0 and utspq.deleted = 0 and q.deleted = 0
        LIMIT ${limit} OFFSET ${
          pageInt * limitInt
          }
        `,
        [id],
      )
      const totals = await query<any[]>(
        `select COUNT(*) as total from unit_test u
        join unit_test_section uts on u.id = uts.unit_test_id
        join unit_test_section_part  utsp on uts.id = utsp.unit_test_section_id
        join unit_test_section_part_question utspq on utsp.id = utspq.unit_test_section_part_id 
        join question q on utspq.question_id = q.id 
        where u.id = ? and u.deleted = 0 and uts.deleted = 0 and utsp.deleted = 0 and utspq.deleted = 0 and q.deleted =0`,
        [id],
      )
      return res.status(200).json({
        code:1,
        data: results,
        currentPage:pageInt + 1,
        limit:limitInt,
        totalRecords: totals[0]?.total
      })
    } catch (e: any) {
      res.status(500).json({code:0, message: e.message, data:[], total:0 })
    }
  }
}

export default handler
