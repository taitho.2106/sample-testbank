import { NextApiRequest, NextApiResponse } from 'next'
import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case 'GET':
      return checkByGradeAndSeries()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function checkByGradeAndSeries() {
    const { g,s} = req.query
    try {
      const grades = await query<any[]>(
        `select id from grade where deleted = 0 and grade_eduhome_id = ?`,
        [g],
      )
      //if no grade.
      if(grades.length ==0){
        return res.status(200).json({
          code:0
        })
      }
      const series = await query<any[]>(
        `select id from series where deleted = 0 and series_eduhome_id =? and grade_id = ?`,
        [s,grades[0].id],
      )
      //if no series.
      if(series.length >0){
        const checkGradeSeriesUnitTest = await query<any[]>(
          `select count(*) as total from unit_test where deleted = 0 and series_id = ? and template_level_id = ?`,
          [series[0].id,grades[0].id],
        )
        //if has data by grade and series
        if(checkGradeSeriesUnitTest.length >0 && checkGradeSeriesUnitTest[0].total>0){
          return res.status(200).json({
            code:1,
            data:{grade:grades[0].id, series:series[0].id}
          })
        }
      }
      
      const checkGradeUnitTest = await query<any[]>(
        `select count(*) as total from unit_test where deleted = 0 and template_level_id = ?`,
        [grades[0].id],
      )
      //if has data by grade
      if(checkGradeUnitTest.length >0 && checkGradeUnitTest[0].total>0){
        return res.status(200).json({
          code:1,
          data:{grade:grades[0].id}
        })
      }
      return res.status(200).json({
        code:0
      })
    } catch (e: any) {
      res.status(500).json({code:0, message: e.message, data:[], total:0 })
    }
  }
}

export default handler
