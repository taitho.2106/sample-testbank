import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
import { query } from 'lib/db'

import {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  BorderStyle,
  WidthType,
  AlignmentType,
  VerticalAlign,
  HeightRule,
  HeadingLevel,
  ImageRun,
  UnderlineType,
} from 'docx'
import { replaceSpecialCharacter } from 'api/utils'
import { getTextWithoutFontStyle } from 'utils/string'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })

  if (!session) {
    return res.status(403).end('Forbidden')
  }

  switch (req.method) {
    case 'GET':
      return getUnitTestById()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getUnitTestById() {
    const { id } = req.query
    if (!id) return res.status(400).json({ message: '`id` required' })

    try {
      const unitTests: any[] = await query<any[]>(
        'SELECT * FROM unit_test WHERE deleted = 0 AND id = ? LIMIT 1',
        [id],
      )
      const unitTest = unitTests[0]
      if (unitTests.length === 0) {
        return res.status(400).json({ message: 'data not found' })
      }
      const sections: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section WHERE deleted = 0 AND unit_test_id = ?',
        [id],
      )
      if (sections.length > 0) {
        unitTest.sections = sections
      }
      const sectionIds = sections.map((m) => m.id)
      const parts: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section_part WHERE deleted = 0 AND unit_test_section_id IN (?)',
        [sectionIds],
      )
      if (parts.length > 0) {
        for (const item of sections) {
          item.parts = parts.filter((m) => m.unit_test_section_id === item.id)
        }
      }
      const partIds = parts.map((m) => m.id)
      const questions: any[] = await query<any[]>(
        `SELECT utspq.unit_test_section_part_id, q.* FROM unit_test_section_part_question utspq
         LEFT JOIN question q ON utspq.question_id = q.id
         WHERE utspq.deleted = 0 AND utspq.unit_test_section_part_id IN (?)`,
        [partIds],
      )
      if (questions.length > 0) {
        for (const item of parts) {
          item.questions = questions.filter(
            (m) => m.unit_test_section_part_id === item.id,
          )
        }
      }
      const doc = new Document({
        sections: [
          {
            properties: {
              page: {
                margin: {
                  top: '0.5 in',
                  bottom: '0.5 in',
                  left: '0.5 in',
                  right: '0.5 in',
                },
              },
            },
            children: [
              new Paragraph({
                alignment: AlignmentType.CENTER,
                heading: HeadingLevel.HEADING_2,
                children: [
                  new TextRun({
                    text: unitTest.name,
                    break: 1,
                    color: '#188fba',
                    bold: true,
                  }),
                ],
              }),
              ...renderSection(unitTest),
              new Paragraph({
                spacing: { before: 200 },
                alignment: AlignmentType.CENTER,
                children: [
                  new TextRun({
                    text: '---THE END---',
                    color: '#188fba',
                    bold: true,
                  }),
                ],
              }),
            ],
          },
        ],
      })

      res.writeHead(200, {
        'Content-Type':
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'Content-Disposition': `attachment; filename=${replaceSpecialCharacter(
          unitTest.name,
        )}_answer_key.docx`,
      })

      Packer.toBuffer(doc).then((buffer) => {
        res.write(buffer, 'binary')
        res.end()
      })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

function renderSection(data: any) {
  const list: any[] = []

  let currentQuestion = 1

  const sections: any[][] = []
  const sectionList = [...data.sections]

  while (sectionList.length > 0) {
    sections.push(sectionList.splice(0, 2))
  }
  list.push(new Paragraph(''))
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, color: '#ffffff' },
        insideHorizontal: { style: BorderStyle.SINGLE, color: '#ffffff' },
        insideVertical: { style: BorderStyle.SINGLE, color: '#ffffff' },
      },
      margins: { top: 100, right: 100, left: 100, bottom: 100 },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: sections.map(
        (section2: any[], sIndex: number) =>
          new TableRow({
            children: section2.map(
              (section, s2Index) =>
                new TableCell({
                  width: { size: 50, type: WidthType.PERCENTAGE },
                  children: [
                    new Table({
                      borders: {
                        top: { style: BorderStyle.SINGLE, color: '#188fba' },
                        left: { style: BorderStyle.SINGLE, color: '#188fba' },
                        bottom: { style: BorderStyle.SINGLE, color: '#188fba' },
                        right: { style: BorderStyle.SINGLE, color: '#188fba' },
                        insideHorizontal: {
                          style: BorderStyle.SINGLE,
                          color: '#188fba',
                        },
                        insideVertical: {
                          style: BorderStyle.SINGLE,
                          color: '#188fba',
                        },
                      },
                      margins: { top: 100, right: 100, left: 100, bottom: 100 },
                      alignment: AlignmentType.LEFT,
                      rows: [
                        new TableRow({
                          children: [
                            new TableCell({
                              children: [
                                new Paragraph({
                                  children: [
                                    new TextRun({
                                      text: `PART ${sIndex * 2 + s2Index + 1} `,
                                      color: '#188fba',
                                      bold: true,
                                    }),
                                  ],
                                }),
                              ],
                            }),
                            new TableCell({
                              borders: {
                                top: {
                                  style: BorderStyle.SINGLE,
                                  color: '#ffffff',
                                },
                                right: {
                                  style: BorderStyle.SINGLE,
                                  color: '#ffffff',
                                },
                                bottom: {
                                  style: BorderStyle.SINGLE,
                                  color: '#ffffff',
                                },
                              },
                              children: [
                                new Paragraph({
                                  children: [
                                    new TextRun({
                                      text: `${section.parts[0].name}`,
                                      color: '#188fba',
                                      bold: true,
                                    }),
                                  ],
                                }),
                              ],
                            }),
                          ],
                        }),
                      ],
                    }),
                    ...renderAnswer(section.parts[0].questions),
                  ],
                }),
            ),
          }),
      ),
    }),
  )

  function renderAnswer(questions: any[]) {
    const list1: any[] = []
    questions.forEach((question: any, qIndex: number) => {
      switch (question.question_type) {
        case 'MC1':
          MultiChoiceQuestion1_v2(question, currentQuestion, list1)
          break
        case 'MC2':
          MultiChoiceQuestion2_MC2(question, currentQuestion, list1)
          break
        case 'MC3':
          MultiChoiceQuestion1(question, currentQuestion, list1)
          break
        case 'MC4':
          MultiChoiceQuestion4(question, currentQuestion, list1)
          break
        case 'MC5':
        case 'MC6':
          MultiChoiceQuestion5(question, currentQuestion, list1)
          break
        case 'DD1':
          DragAndDropQuestion(question, currentQuestion, list1)
          break
        case 'FB1':
          FillInBlankQuestion1(question, currentQuestion, list1)
          break
        case 'FB2':
        case 'FB3':
        case 'FB4':
        case 'FB5':
        case 'FB6':
        case 'FB7':
          FillInBlankQuestion2(question, currentQuestion, list1)
          break
        case 'LS1':
        case 'LS2':
          LikertScaleQuestion(question, currentQuestion, list1)
          break
        case 'MG1':
        case 'MG2':
        case 'MG3':
          MatchingGameQuestion(question, currentQuestion, list1)
          break
        case 'SA1':
          ShortAnswerQuestion(question, currentQuestion, list1)
          break
        case 'TF1':
        case 'TF2':
          TrueFalseQuestion(question, currentQuestion, list1)
          break
        case 'MR1':
        case 'MR2':
        case 'MR3':
          MultiResponseQuestion1(question, currentQuestion, list1)
          break
        case 'SL1':
          SelectFromListQuestion1(question, currentQuestion, list1)
          break
        case 'SL2':
          SelectFromListQuestion2(question, currentQuestion, list1)
          break
        case 'MIX1':
          MIXQuestion1(question, currentQuestion, list1)
          break
        case 'MIX2':
          MIXQuestion2(question, currentQuestion, list1)
          break
        case 'TF3':
        case 'TF4':
          TrueFalseQuestion3_4(question, currentQuestion, list1)
          break
      }

      if (excludeType.includes(question.question_type)) {
        currentQuestion += 1
      } else {
        currentQuestion += question.total_question
      }
    })
    return list1
  }

  return list
}
const excludeType = ['MR1', 'MR2', 'MR3']
const correctText: any = { T: 'True', F: 'False', NI: 'Not given' }

function MultiChoiceQuestion1(question: any, index: number, list: any[]) {
  const answers = question.answers
    .split('*')
    .map((m: string) => m.replace(/\%/g, ''))
  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${String.fromCharCode(
        65 + answers.findIndex((m: string) => m === question.correct_answers),
      )}`,
    }),
  )
}
function MultiChoiceQuestion4(question: any, index: number, list: any[]) {
  const answers = question.answers
    .split('*')
    .map((m: string) => getTextWithoutFontStyle(m))
  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${String.fromCharCode(
        65 + answers.findIndex((m: string) => m === question.correct_answers),
      )}`,
    }),
  )
}
function MultiChoiceQuestion2_MC2(question: any, index: number, list: any[]) {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const correctList = question.correct_answers?.split('#')
  while(questionList.length>0){
    if(questionList[0].startsWith("*")){
      questionList.shift();
      answerList.shift();
      correctList.shift();
    }else{
      break;
    }
  }
  questionList.forEach((ques: any, qIndex: number) => {
    const answers = answerList[qIndex].split('*')
      list.push(
        new Paragraph({
          alignment: AlignmentType.LEFT,
          spacing: { before: 150 },
          text: `${index + qIndex}. ${String.fromCharCode(
            65 +
              answers.findIndex(
                (m: string) => getTextWithoutFontStyle(m) === correctList[qIndex],
              ),
          )}`,
        }),
      )
  })
}
function MultiChoiceQuestion1_v2(question: any, index: number, list: any[]) {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const correctList = question.correct_answers?.split('#')
  questionList.forEach((ques: any, qIndex: number) => {
    const answers = answerList[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + qIndex}. ${String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string) => getTextWithoutFontStyle(m) === correctList[qIndex],
            ),
        )}`,
      }),
    )
  })
}
function MultiChoiceQuestion5(question: any, index: number, list: any[]) {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const correctList = question.correct_answers?.split('#')
  questionList.forEach((ques: any, qIndex: number) => {
    const answers = answerList[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + qIndex}. ${String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string) => getTextWithoutFontStyle(m) === correctList[qIndex],
            ),
        )}`,
      }),
    )
  })
}

function DragAndDropQuestion(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correctAnswers = question.correct_answers.split('#')
  const checkExam = question.question_text?.split('#') || ''
  const correctList: string[] = []

  for (let i = 0; i < correctAnswers.length; i++) {
    if (answers[i]) {
      if (!checkExam || checkExam[i] !== '*') {
        correctList.push(correctAnswers[i])
      }
    }
  }

  correctList.forEach((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${answer
          .replace(/\*/g, ' ')
          .replace(/\%\/\%/g, '\/\n')}`,
      }),
    )
  })
}

function FillInBlankQuestion1(question: any, index: number, list: any[]) {
  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${question.correct_answers.replace(/\%\/\%/g, '/')}`,
    }),
  )
}

function FillInBlankQuestion2(question: any, index: number, list: any[]) {
  const correctList = question.correct_answers
    .split('#')
    .map((m: string) => m.replace(/\%\/\%/g, '/'))
  correctList.forEach((part: string, partIndex: number) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + partIndex}. ${part}`,
      }),
    )
  })
}

function LikertScaleQuestion(question: any, index: number, list: any[]) {
  const correctList: string[] = question.correct_answers.split('#')

  correctList.forEach((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })
}

function MatchingGameQuestion(question: any, index: number, list: any[]) {
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const correctList: string[] = question.correct_answers.split('#')

  listAnswer[0].forEach((_, mIndex: number) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + mIndex}. ${String.fromCharCode(
          97 +
            listAnswer[1].findIndex((m: string) => m === correctList[mIndex]),
        )}`,
      }),
    )
  })
}

function ShortAnswerQuestion(question: any, index: number, list: any[]) {
  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${question.correct_answers.replace(/\%\/\%/g, '/')}`,
    }),
  )
}
function TrueFalseQuestion(question: any, index: number, list: any[]) {
  const correctList: string[] = question.correct_answers.split('#')
  correctList.forEach((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })
}
//template TF3_4
function TrueFalseQuestion3_4(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correcrAnswers = question.correct_answers.split('#')
  const correctList: string[] = []
  //remove example
  for (let i = 0; i < correcrAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        correctList.push(correcrAnswers[i])
      }
    }
  }
  correctList.forEach((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })
}

function MultiResponseQuestion1(question: any, index: number, list: any[]) {
  const answers: string[] = question.answers.split('#')
  const correctList: string[] = question.correct_answers.split('#')

  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${answers
        .map((answer, answerIndex) =>
          correctList.includes(answer)
            ? String.fromCharCode(65 + answerIndex)
            : null,
        )
        .filter((m) => m !== null)
        .join(', ')}`,
    }),
  )
}

function SelectFromListQuestion1(question: any, index: number, list: any[]) {
  const listAnswers = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const listCorrect = question.correct_answers.split('#')

  listAnswers.forEach((answers: string[], answersIndex: number) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answersIndex}. ${String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string) => m.trim() === listCorrect[answersIndex].trim(),
            ),
        )}`,
      }),
    )
  })
}

function SelectFromListQuestion2(question: any, index: number, list: any[]) {
  const listCorrect: string[] = question.correct_answers.split('#')

  listCorrect.forEach((correct: string, _index) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + _index}. ${correct}`,
      }),
    )
  })
}

function MIXQuestion1(question: any, index: number, list: any[]) {
  const listAnswers: string[] = question.answers.split('[]')
  const tfAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction: string[] = question.question_description.split('[]')
  const mcQuestions: string[] = question.question_text.split('[]')[1].split('#')
  const listCorrect: string[] = question.correct_answers.split('[]')
  const tfCorrects: string[] = listCorrect[0].split('#')
  const mcCorrects: string[] = listCorrect[1].split('#')

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 150,
      },
      children: [
        new TextRun({
          text: `Task 1: ${listInstruction[0]}`,
          bold: true,
        }),
      ],
    }),
  )

  tfCorrects.map((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 150,
      },
      children: [
        new TextRun({
          text: `Task 2: ${listInstruction[1]}`,
          bold: true,
        }),
      ],
    }),
  )

  mcQuestions.forEach((ques: any, qIndex: number) => {
    const answers = mcAnswers[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + tfAnswers.length + qIndex}. ${String.fromCharCode(
          65 + answers.findIndex((m: string) => m === mcCorrects[qIndex]),
        )}`,
      }),
    )
  })
}

function MIXQuestion2(question: any, index: number, list: any[]) {
  const listAnswers: string[] = question.answers.split('[]')
  const lsAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction: string[] = question.question_description.split('[]')
  const mcQuestions: string[] = question.question_text.split('[]')[1].split('#')
  const listCorrect: string[] = question.correct_answers.split('[]')
  const lsCorrects: string[] = listCorrect[0].split('#')
  const mcCorrects: string[] = listCorrect[1].split('#')

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 150,
      },
      children: [
        new TextRun({
          text: `Task 1: ${listInstruction[0]}`,
          bold: true,
        }),
      ],
    }),
  )

  lsCorrects.map((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 150,
      },
      children: [
        new TextRun({
          text: `Task 2: ${listInstruction[1]}`,
          bold: true,
        }),
      ],
    }),
  )

  mcQuestions.forEach((ques: any, qIndex: number) => {
    const answers = mcAnswers[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + lsAnswers.length + qIndex}. ${String.fromCharCode(
          65 + answers.findIndex((m: string) => m === mcCorrects[qIndex]),
        )}`,
      }),
    )
  })
}

export default handler
