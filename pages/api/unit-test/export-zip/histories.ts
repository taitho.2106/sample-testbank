import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { USER_ROLES } from 'interfaces/constants'
import { query, queryWithTransaction } from 'lib/db'
import { userInRight } from 'utils'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  if (!session) {
    return res.status(403).end('Forbidden')
  }
  const user: any = session?.user
  const isSystem = userInRight([USER_ROLES.Operator], session)

  switch (req.method) {
    case 'GET':
      return getExportHistories()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getExportHistories() {
    const {id, type } = req.query
    try {
      const unitTestR: any[] = await query<any[]>(
        `SELECT * FROM unit_test WHERE  
        id = ? 
        AND deleted = 0
        LIMIT 1`,
        [id],
      )
      if(!unitTestR || unitTestR.length==0){
        return res.status(400).json({code:0, message: 'Data not found' })
      }
      let queryStr = `SELECT id,unit_test_id,type,code,scope,created_date,created_by,updated_date,updated_by,mix_questions FROM unit_test_export WHERE deleted = 0 
      AND unit_test_id = ? 
      AND type= ?
      AND deleted = 0`
      const queryParams = [id,type]
      if(isSystem){
        queryStr += ` AND scope = 0 order by created_date desc`
      }else{
        queryStr += ` AND created_by = ? order by created_date desc`
        queryParams.push(user.id)
      }
      const historiesR: any[] = await query<any[]>(
        queryStr,
        queryParams
      )
      return res.json({
        code:1,
        data: {
          unit_test_name:unitTestR[0].name,
          export_histories:historiesR
        },
      })
    } catch (e: any) {
      res.status(500).json({code:0, message: e.message })
    }
  }

}

export default handler
