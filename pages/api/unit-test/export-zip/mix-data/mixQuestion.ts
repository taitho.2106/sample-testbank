import shuffleArray from './shuffleArray'
export default function mixQuestion(question: any) {
  switch (question.question_type) {
    case 'MC1':
      return mixQuestionText_Answer_Correct_MC(question)
    case 'MC5':
    case 'MC6':
      return mixSubAnswer_MC(question)
    case 'MC7':
      return mixSubAnswer_MC7(question)
    case 'MC2':
      return mixQuestionText_Answer_Correct_HasExample_MC(question)
    case 'MC3':
    case 'MC4':
      return { ...question, answers: mixStringSplitKey(question.answers, '*') }
    case 'TF3':
    case 'TF4':
      return mixAnswer_Correct_HasExample_TF(question)
    case 'MG1':
    case 'MG2':
    case 'MG3':
      return mixAnswer_Correct_MG(question)
    case 'MR1':
    case 'MR2':
    case 'MR3':
      return { ...question, answers: mixStringSplitKey(question.answers, '#') }
    case 'SL1':
    case 'SL2':
      return mixAnswer_SL(question)
    case 'DD1':
      return mixDD1(question)
    case 'MIX1':
      return mixAnswer_Correct_MIX1(question)
    case 'MIX2':
      return mixAnswer_Correct_MIX2(question)
    case 'MC8':
      return mixQuestionText_Answer_Correct_HasExample_MC8(question)
    case 'MC9':
      return mixQuestionText_Answer_Correct_HasExample_MC9(question)
    case 'MG4':
    case 'MG6':
      return mixAnswer_Correct_MG4_MG6(question)
    default:
      return question
  }
}
const mixQuestionText_Answer_Correct_MC = (question: any) => {
  try {
    const questionTexts = question.question_text?.split('#') || []
    const answers = question.answers?.split('#')||[]
    const corrects = question.correct_answers?.split('#')||[]

    const questionTextsR = []
    const answersR = []
    const correctsR = []
    let randomIndexs = Array.from({ length: answers.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)
    for (let i = 0; i < randomIndexs.length; i++) {
      const index = randomIndexs[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        const strSubAnswer = mixStringSplitKey(answers[index], '*')
        answersR.push(strSubAnswer)
      }
    }
    const strQuestion =
      questionTextsR.length > 0 ? questionTextsR.join('#') : null
    const strAnswer = answersR.length > 0 ? answersR.join('#') : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      question_text: strQuestion,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixQuestionText_Answer_Corect--', err)
    return question
  }
}
//k trộn câu hỏi, chỉ trộn đáp án của từng câu con, lấy lại corect_answer theo index;
const mixSubAnswer_MC7 = (question: any) => {
  try {
    const answers = question.answers?.split('#') //tách các lựa chọn đáp án của từng chổ trống
    const correctAnswerOld = question.correct_answers?.split("#"); //mảng chứa index đáp án đúng
    const correctAnswerNew = []
    for (let i = 0; i < answers.length; i++) {
      const itemAnswerOldArr = answers[i].split("*");
      const itemCorrect = itemAnswerOldArr[parseInt(correctAnswerOld[i])]
      const itemAnswerNewArr = shuffleArray(itemAnswerOldArr);
      answers[i] = itemAnswerNewArr.join("*")
      const newIndex = itemAnswerNewArr.findIndex(m=>m == itemCorrect)
      correctAnswerNew.push(`${newIndex}`)
    }
    const strCorrect =correctAnswerNew.length >0? correctAnswerNew.join("#"):null
    const strAnswer = answers.length > 0 ? answers.join('#') : null
    return {
      ...question,
      answers: strAnswer,
      correct_answers:strCorrect
    }
  } catch (err) {
    console.log('mixSubAnswer_MC7--', err)
    return question
  }
}
//k trộn câu hỏi, chỉ trộn đáp án của từng câu con
const mixSubAnswer_MC = (question: any) => {
  try {
    const answers = question.answers?.split('#') //tách các lựa chọn đáp án của từng chổ trống
    for (let i = 0; i < answers.length; i++) {
      answers[i] = mixStringSplitKey(answers[i], '*')
    }
    const strAnswer = answers.length > 0 ? answers.join('#') : null
    return {
      ...question,
      answers: strAnswer,
    }
  } catch (err) {
    console.log('mixSubAnswer_MC--', err)
    return question
  }
}
const mixQuestionText_Answer_Correct_HasExample_MC = (question: any) => {
  try {
    const questionTexts = question.question_text?.split('#') || []
    const answers = question.answers?.split('#')||[]
    const corrects = question.correct_answers.split('#')||[]

    const questionTextsR = []
    const answersR = []
    const correctsR = []
    let randomIndexs = Array.from({ length: questionTexts.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)

    let randomIndexsExample = []
    const randomIndexsQuestion = []
    for (let i = 0; i < randomIndexs.length; i++) {
      if (questionTexts[randomIndexs[i]]?.startsWith('*')) {
        randomIndexsExample.push(randomIndexs[i])
      } else {
        randomIndexsQuestion.push(randomIndexs[i])
      }
    }
    //example
    randomIndexsExample  = randomIndexsExample.sort((a, b) => a - b)
    for (let i = 0; i < randomIndexsExample.length; i++) {
      const index = randomIndexsExample[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        const strSubAnswer = answers[index]
        answersR.push(strSubAnswer)
      }
    }
    //question
    for (let i = 0; i < randomIndexsQuestion.length; i++) {
      const index = randomIndexsQuestion[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        const strSubAnswer = mixStringSplitKey(answers[index], '*')
        answersR.push(strSubAnswer)
      }
    }
    const strQuestion =
      questionTextsR.length > 0 ? questionTextsR.join('#') : null
    const strAnswer = answersR.length > 0 ? answersR.join('#') : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      question_text: strQuestion,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixQuestionText_Answer_Corect_HasExample--', err)
    return question
  }
}
const mixQuestionText_Answer_Correct_HasExample_MC8 = (question: any) => {
  try {
    const questionTexts = question.question_text?.split('#') || []
    const answers = question.answers?.split('#')||[]
    const corrects = question.correct_answers.split('#')||[]

    const questionTextsR = []
    const answersR = []
    const correctsR = []
    let randomIndexs = Array.from({ length: questionTexts.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)

    let randomIndexsExample = []
    const randomIndexsQuestion = []
    for (let i = 0; i < randomIndexs.length; i++) {
      if (questionTexts[randomIndexs[i]]?.startsWith('*')) {
        randomIndexsExample.push(randomIndexs[i])
      } else {
        randomIndexsQuestion.push(randomIndexs[i])
      }
    }
    //example
    randomIndexsExample  = randomIndexsExample.sort((a, b) => a - b)
    for (let i = 0; i < randomIndexsExample.length; i++) {
      const index = randomIndexsExample[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        const strSubAnswer = answers[index]
        answersR.push(strSubAnswer)
      }
    }
    //question
    for (let i = 0; i < randomIndexsQuestion.length; i++) {
      const index = randomIndexsQuestion[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      // if (corrects.length > index) {
      //   correctsR.push(corrects[index])
      // }
      const fileCorrect = answers[index]?.split('*')[parseInt(corrects[index])]
      if (answers.length > index) {
        const strSubAnswer = answers[index];
        const arrSubAnswer = shuffleArray(strSubAnswer.split("*"));
        const newIndexCorrect = arrSubAnswer.indexOf(fileCorrect);
        correctsR.push(newIndexCorrect)
        answersR.push(arrSubAnswer.join("*"))
      }
    }
    const strQuestion =
      questionTextsR.length > 0 ? questionTextsR.join('#') : null
    const strAnswer = answersR.length > 0 ? answersR.join('#') : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      question_text: strQuestion,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixQuestionText_Answer_Correct_HasExample_MC8--', err)
    return question
  }
}

const mixQuestionText_Answer_Correct_HasExample_MC9 = (question: any) => {
  try {
    const questionTexts = question.question_text?.split('#') || []
    const images = question.image?.split('#') || []
    const answers = question.answers?.split('#')||[]
    const corrects = question.correct_answers.split('#')||[]
    
    const questionTextsR = []
    const imagesR = []
    const answersR = []
    const correctsR = []
    
    let randomIndexs = Array.from({ length: questionTexts.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)

    let randomIndexsExample = []
    const randomIndexsQuestion = []
    for (let i = 0; i < randomIndexs.length; i++) {
      if (questionTexts[randomIndexs[i]]?.startsWith('*')) {
        randomIndexsExample.push(randomIndexs[i])
      } else {
        randomIndexsQuestion.push(randomIndexs[i])
      }
    }

    //example
    randomIndexsExample  = randomIndexsExample.sort((a, b) => a - b)
    for (let i = 0; i < randomIndexsExample.length; i++) {
      const index = randomIndexsExample[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      if (images.length > index && images[index]) {
        imagesR.push(images[index])
      }
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        answersR.push(answers[index])
      }
    }

    //question
    for (let i = 0; i < randomIndexsQuestion.length; i++) {
      const index = randomIndexsQuestion[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      if (images.length > 0 && images[index]) {
        imagesR.push(images[index])
      }
      if (corrects.length > 0) {
        correctsR.push(corrects[index])
      }
      if (answers.length > 0) {
        const strSubAnswer = answers[index];
        const arrSubAnswer = shuffleArray(strSubAnswer.split("*"));
        answersR.push(arrSubAnswer.join("*"))
      }
    }
    const strQuestion = questionTextsR.length > 0 ? questionTextsR.join('#') : null
    const strImage = imagesR.length > 0 ? imagesR.join('#') : null
    const strAnswer = answersR.length > 0 ? answersR.join('#') : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      question_text: strQuestion,
      image: strImage,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixQuestionText_Answer_Correct_HasExample_MC9--', err)
    return question
  }
}

const mixStringSplitKey = (
  strAnswerOfSubQuestion: string,
  splitKey: string,
) => {
  try {
    const answers = strAnswerOfSubQuestion?.split(splitKey)
    if (answers) {
      return shuffleArray(answers).join(splitKey)
    } else {
      return strAnswerOfSubQuestion
    }
  } catch (err) {
    return strAnswerOfSubQuestion
  }
}
const mixAnswer_Correct_TF = (question: any) => {
  try {
    const answers = question.answers?.split('#')||[]
    const corrects = question.correct_answers.split('#')||[]

    const answersR = []
    const correctsR = []
    let randomIndexs = Array.from({ length: answers.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)
    for (let i = 0; i < randomIndexs.length; i++) {
      const index = randomIndexs[i]
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        answersR.push(answers[index])
      }
    }

    const strAnswer = answersR.length > 0 ? answersR.join('#') : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixAnswer_Correct_TF--', err)
    return question
  }
}
const mixAnswer_Correct_HasExample_TF = (question: any) => {
  try {
    const images = question.image?.split('#') || []
    const answers = question.answers?.split('#') || []
    const corrects = question.correct_answers?.split('#') || []

    const imagesR = []
    const answersR = []
    const correctsR = []
    let randomIndexs = Array.from({ length: answers.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)

    let randomIndexsExample = []
    const randomIndexsQuestion = []
    for (let i = 0; i < randomIndexs.length; i++) {
      const index = randomIndexs[i]
      if (answers[index]?.startsWith('*')) {
        randomIndexsExample.push(index)
      } else {
        randomIndexsQuestion.push(index)
      }
    }
    //example
    randomIndexsExample  = randomIndexsExample.sort((a, b) => a - b)
    for (let i = 0; i < randomIndexsExample.length; i++) {
      const index = randomIndexsExample[i]
      if (images.length > index && images[index]) {
        imagesR.push(images[index])
      }
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        answersR.push(answers[index])
      }
    }
    //question
    for (let i = 0; i < randomIndexsQuestion.length; i++) {
      const index = randomIndexsQuestion[i]
      if (images.length > 0 && images[index]) {
        imagesR.push(images[index])
      }
      if (corrects.length > 0) {
        correctsR.push(corrects[index])
      }
      if (answers.length > 0) {
        answersR.push(answers[index])
      }
    }
    const strImage = imagesR.length > 0 ? imagesR.join('#') : null
    const strAnswer = answersR.length > 0 ? answersR.join('#') : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      image: strImage,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixAnswer_Correct_HasExample_TF--', err)
    return question
  }
}
const mixAnswer_Correct_LS = (question: any) => {
  try {
    const answers = question.answers?.split('#')||[]
    const corrects = question.correct_answers.split('#')||[]

    const answersR = []
    const correctsR = []
    let randomIndexs = Array.from({ length: answers.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)
    for (let i = 0; i < randomIndexs.length; i++) {
      const index = randomIndexs[i]
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        answersR.push(answers[index])
      }
    }

    const strAnswer = answersR.length > 0 ? answersR.join('#') : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixAnswer_Correct_LS--', err)
    return question
  }
}
const mixAnswer_Correct_MG4_MG6 = (question: any) => {
  try {
    const leftRights = question.answers?.split('#')||[] //tách left và right hiển thị
    const corrects = question.correct_answers.split('#')||[] // thứ tự đáp án đúng theo left
    const answers = leftRights[0].split('*') //
    const answersR = []
    const correctsR = []
    const arrExample = leftRights[2]?.split("*")||[]
    const checkExample = arrExample && arrExample[0] && arrExample[0]=='ex'
    let randomIndexs = Array.from({ length: answers.length }, (v, i) => i)
    if(checkExample){
      randomIndexs.shift();
      answersR.push(answers[0])
      correctsR.push(corrects[0])
    }
    randomIndexs = shuffleArray(randomIndexs)
    for (let i = 0; i < randomIndexs.length; i++) {
      const index = randomIndexs[i]
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        answersR.push(answers[index])
      }
    }

    const rightAnswers = shuffleArray(leftRights[1].split("*"));
    if(checkExample){
      const indexRightAnswerExample = rightAnswers.indexOf(correctsR[0])
      if(indexRightAnswerExample !=-1){
        rightAnswers[indexRightAnswerExample] = rightAnswers[0];
        rightAnswers[0] = correctsR[0];
      }
    }
    const strAnswer =
      answersR.length > 0
        ? `${answersR.join('*')}#${rightAnswers.join("*")}${checkExample?`#${leftRights[2]}`:""}`
        : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixAnswer_Correct_MG4_MG6--', err)
    return question
  }
}
const mixAnswer_Correct_MG = (question: any) => {
  try {
    const leftRights = question.answers?.split('#')||[] //tách left và right hiển thị
    const corrects = question.correct_answers.split('#')||[] // thứ tự đáp án đúng theo left
    const answers = leftRights[0].split('*') //
    const answersR = []
    const correctsR = []
    let randomIndexs = Array.from({ length: answers.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)
    for (let i = 0; i < randomIndexs.length; i++) {
      const index = randomIndexs[i]
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (answers.length > index) {
        answersR.push(answers[index])
      }
    }

    const strAnswer =
      answersR.length > 0
        ? `${answersR.join('*')}#${mixStringSplitKey(leftRights[1], '*')}`
        : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    return {
      ...question,
      answers: strAnswer,
      correct_answers: strCorrect,
    }
  } catch (err) {
    console.log('mixAnswer_Correct_MG--', err)
    return question
  }
}

const mixAnswer_SL = (question: any) => {
  try {
    const answers = question.answers?.split('#')||[] //tách các lựa chọn đáp án của từng chổ trống
    for (let i = 0; i < answers.length; i++) {
      answers[i] = mixStringSplitKey(answers[i], '*')
    }
    const strAnswer = answers.length > 0 ? answers.join('#') : null
    return {
      ...question,
      answers: strAnswer,
    }
  } catch (err) {
    console.log('mixAnswer_SL--', err)
    return question
  }
}
const mixAnswer_Correct_MIX1 = (question: any) => {
  try {
    const answerTF2andMC5 = question.answers?.split('[]')||[] //tách ddáp án thành từng phần
    const answerMC5 = answerTF2andMC5[1].split('#')
    for(let i=0; i< answerMC5.length; i++){
      //mix item answer.
      answerMC5[i] =  mixStringSplitKey(answerMC5[i], '*')
    }
    const textAnswerTF2 =  answerTF2andMC5[0]
    //mix MC5
    const strAnswer =`${textAnswerTF2}[]${answerMC5.join("#")}`;
    return {
      ...question,
      answers: strAnswer,
    }
  } catch (err) {
    console.log('mixAnswer_Correct_MIX1--', err)
    return question
  }
}
const mixAnswer_Correct_MIX2 = (question: any) => {
  try {
    const answerLS1andMC5 = question.answers?.split('[]')||[] //tách ddáp án thành từng phần
    const answerMC5 = answerLS1andMC5[1].split('#')
    for(let i=0; i< answerMC5.length; i++){
      //mix item answer.
      answerMC5[i] =  mixStringSplitKey(answerMC5[i], '*')
    }
    const textAnswerLS1 =  answerLS1andMC5[0]
    //mix MC5
    const strAnswer =`${textAnswerLS1}[]${answerMC5.join("#")}`;
    return {
      ...question,
      answers: strAnswer,
    }
  } catch (err) {
    console.log('mixAnswer_Correct_MIX2--', err)
    return question
  }
}

const mixDD1 = (question:any)=>{
  try {
    const questionTexts = question.question_text?.split('#') || []
    const answers = question.answers?.split('#')||[]
    const corrects = question.correct_answers.split('#')||[]
    const images = question.image?.split('#')||[]
    const questionTextsR = []
    const answersR = []
    const correctsR = []
    const imagesR = []
    let randomIndexs = Array.from({ length: answers.length }, (v, i) => i)
    randomIndexs = shuffleArray(randomIndexs)

    let randomIndexsExample = []
    const randomIndexsQuestion = []
    for (let i = 0; i < randomIndexs.length; i++) {
      if (questionTexts.length>randomIndexs[i] && questionTexts[randomIndexs[i]]?.startsWith('*')) {
        randomIndexsExample.push(randomIndexs[i])
      } else {
        randomIndexsQuestion.push(randomIndexs[i])
      }
    }
    //example
    randomIndexsExample  = randomIndexsExample.sort((a, b) => a - b)
    for (let i = 0; i < randomIndexsExample.length; i++) {
      const index = randomIndexsExample[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (images.length > index) {
        imagesR.push(images[index])
      }
      if (answers.length > index) {
        const strSubAnswer = answers[index]
        answersR.push(strSubAnswer)
      }
    }
    //question
    for (let i = 0; i < randomIndexsQuestion.length; i++) {
      const index = randomIndexsQuestion[i]
      if (questionTexts.length > index) {
        questionTextsR.push(questionTexts[index])
      }
      if (corrects.length > index) {
        correctsR.push(corrects[index])
      }
      if (images.length > index) {
        imagesR.push(images[index])
      }
      if (answers.length > index) {
        const strSubAnswer = mixStringSplitKey(answers[index], '*')
        answersR.push(strSubAnswer)
      }
    }
    const strQuestion =
      questionTextsR.length > 0 ? questionTextsR.join('#') : null
    const strAnswer = answersR.length > 0 ? answersR.join('#') : null
    const strCorrect = correctsR.length > 0 ? correctsR.join('#') : null
    const strImage = imagesR.length > 0 ? imagesR.join('#') : null
    return {
      ...question,
      question_text: strQuestion,
      answers: strAnswer,
      correct_answers: strCorrect,
      image:strImage
    }
  } catch (err) {
    console.log('mixDD1--', err)
    return question
  }
}