import mixQuestion from './mixQuestion'
import shuffleArray from './shuffleArray'
export default function mixData(data:any) {
  // console.log("mix------------------",data)
 try{
  console.log("mix data----")
  const dataResutl = {...data}
  const sections = dataResutl.sections;
  // console.log("mix-----sections-------------",sections[0].questions)
  for(let index=0; index < sections.length;index++){
    const section = sections[index]
    if(!section.parts[0]){
      continue;
    }
    if(!Array.isArray(section.parts[0].questions)){
        continue;
    }
    
    // console.log("mix1111------------------")
    section.parts[0].questions = shuffleArray(section.parts[0].questions)
    const questions = [...section.parts[0].questions];
    const lenQuestion = questions.length;
    for(let indexQuestion=0; indexQuestion < lenQuestion; indexQuestion++){
      // console.log("question-before--",questions[indexQuestion])
      questions[indexQuestion] = mixQuestion(questions[indexQuestion]);
      // console.log("question-after--",questions[indexQuestion])
    }
    section.parts[0].questions = questions
    // console.log("mix2222------------------")
  }
  // console.log("mix333------------------")
  return dataResutl;
 }catch(err){
  console.log("mixData--",err)
  return data;
 }
}
