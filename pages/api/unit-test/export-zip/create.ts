import fs from 'fs'

import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { unitTestExportDir } from '@/constants/index'
import { USER_ROLES } from '@/interfaces/constants'
import { query } from 'lib/db'
import { userInRight } from 'utils'
import Guid from 'utils/guid'

import createWordAnswer from './export-data/createWordAnswer'
import createWordAudioScript from './export-data/createWordAudioScript'
import createWordExam from './export-data/createWordExam'
import mixData from './mix-data'

export const config = {
  api: {
    responseLimit: false,
  },
}
const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  if (!session) {
    return res.status(403).end('Forbidden')
  }
  const isSystem = userInRight([USER_ROLES.Operator], session)
  const user: any = session.user
  switch (req.method) {
    case 'GET':
      return getUnitTestById()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }
  async function getUnitTestById() {
    const { id,mix } = req.query
    const isMixQuestion = mix == "1"?true:false;
    const type = 'word'
    if (!id) return res.status(400).json({ code: 0, message: 'id required' })

    try {
      const unitTests: any[] = await query<any[]>(
        `SELECT u.*FROM unit_test as u
        WHERE u.deleted = 0 AND u.id = ? LIMIT 1`,
        [id],
      )
      if (unitTests.length === 0) {
        return res.status(400).json({ code: 0, message: 'data not found' })
      }
      let unitTest = unitTests[0]
      const grades: any[] = await query<any[]>(
        `SELECT g.name FROM grade as g 
        WHERE g.deleted = 0 AND g.id = ? LIMIT 1`,
        [unitTest.template_level_id],
      )
      unitTest.grade_name = "---"
      if(grades.length >0){
        unitTest.grade_name = grades[0].name
      }
      const sections: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section WHERE deleted = 0 AND unit_test_id = ?',
        [id],
      )
      if (sections.length > 0) {
        unitTest.sections = sections
      }
      const sectionIds = sections.map((m) => m.id)
      const parts: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section_part WHERE deleted = 0 AND unit_test_section_id IN (?)',
        [sectionIds],
      )
      if (parts.length > 0) {
        for (const item of sections) {
          item.parts = parts.filter((m) => m.unit_test_section_id === item.id)
        }
      }
      const partIds = parts.map((m) => m.id)
      const questions: any[] = await query<any[]>(
        `SELECT utspq.unit_test_section_part_id, q.* FROM unit_test_section_part_question utspq
         LEFT JOIN question q ON utspq.question_id = q.id
         WHERE utspq.deleted = 0 AND utspq.unit_test_section_part_id IN (?)`,
        [partIds],
      )
      if (questions.length > 0) {
        for (const item of parts) {
          item.questions = questions.filter(
            (m) => m.unit_test_section_part_id === item.id,
          )
        }
      }
      const exportFolder = `${unitTestExportDir}/${id}/${
        user?.id
      }/${Guid.newGuid()}`
      //create folder
      if (!fs.existsSync(exportFolder)) {
        fs.mkdirSync(exportFolder, { recursive: true })
      }

      //trộn câu hỏi, đáp án
      if(isMixQuestion){
        unitTest = mixData({ ...unitTest })
      }

      let fileNameExam;

      if (unitTest.name === 'The Answers') {
        fileNameExam = 'The Answers(1)';
      } else if (unitTest.name === 'Audio Script') {
        fileNameExam = 'Audio Script(1)';
      } else {
        fileNameExam = unitTest.name;
      }
        
      //replace special filename.
      fileNameExam = fileNameExam.replace(
        /(\*|\/|\\|\:|\?|\"|\'|\.|\<|\>|\|)/g,
        '',
      )
      const filePathExam = `${exportFolder}/${fileNameExam}`
      const fileNameAnswer = `${`The Answers`}`
      const filePathAnswer = `${exportFolder}/${fileNameAnswer}`
      const fileNameAudioScript = `${`Audio Script`}`
      const filePathAudioScript = `${exportFolder}/${fileNameAudioScript}`

      const createExamResult = await createWordExam(
        unitTest,
        filePathExam + '.docx',
      )
      
      if (createExamResult.code == 1) {
        //success
        const examBff = fs.readFileSync(createExamResult.filepath)
        if(!examBff || examBff.length==0){
          return res.status(500).json({code:0, message: "create-exam-failed" })
        }
        const createAnswerResult = await createWordAnswer(
          unitTest,
          filePathAnswer + '.docx',
        )
        if (createAnswerResult.code == 1) {
          const answerBff = fs.readFileSync(createAnswerResult.filepath)
          if(!answerBff || answerBff.length==0){
              return res.status(500).json({code:0, message: "create-answers-failed" })
          }
        }else{
          return res.status(500).json({code:0, message: "create-answers-failed" })
        }
      } else {
        return res
          .status(500)
          .json({
            code: 0,
            message:"create-exam-failed",
          })
      }
      let audio_str = null;
      if (
        createExamResult.audioFile &&
        createExamResult.audioFile.length > 0 
      ) {
        const temp = createExamResult.audioFile
        audio_str = temp.map(m=> `${m.path}:${m.index}`).join("*")
        // create file word contain all audio script
        const createAudioScript = await createWordAudioScript(
          unitTest,
          filePathAudioScript + '.docx',
        )
        if (createAudioScript.code == 1) {
          const audioScriptBff = fs.readFileSync(createAudioScript.filepath)
          if (!audioScriptBff || audioScriptBff.length == 0) {
            return res.status(500).json({
              code: 0,
              message: "create-audio-failed"
            })
          }
        } else {
          return res.status(500).json({
            code: 0,
            message: "create-audio-failed"
          })
        }
      }
      const addDbResult = await AddExportHistories(
        user.id,
        type,
        id as string,
        exportFolder,
        isMixQuestion,
        audio_str
      )
      if (addDbResult.code == 1) {
        return res.status(200).json({ code: 1, data: addDbResult.data })
      } else {
        return res.status(500).json({ code: 0, message: 'have an error' })
      }
    } catch (e: any) {
      console.log('create exam api------', e)
      return res.status(500).json({ code: 0, message: e.message })
    }
  }
  async function AddExportHistories(
    user_id: string,
    type: string,
    unit_test_id: string,
    link_file: string,
    mixQuestion:boolean,
    audio_str:string
  ) {
    try {
      const dateNow = new Date()
      const results: any = await query<any>(
        `INSERT INTO unit_test_export(
          unit_test_id, 
          type,
          link_file,
          scope,
          mix_questions,
          audio_file,
          deleted,
          created_date,
          created_by
        ) 
        VALUES(?,?,?,?,?,?,?,?,?)`,
        [
          unit_test_id,
          type,
          link_file,
          isSystem ? 0 : 1,
          mixQuestion?1:0,
          audio_str,
          0,
          dateNow,
          user_id,
        ],
      )
      if (results.affectedRows === 0) {
        return { code: 0, message: 'Insert fail' }
      } else {
        return { code: 1, data: results.insertId, message: 'Insert success' }
      }
    } catch (error: any) {
      console.log('AddExportHistories--err', error)
      return { code: -1, message: error.message }
    }
  }
}

//delete directory
const deleteDir = async (folder: string) => {
  try {
    if (folder) {
      fs.rmSync(folder, { recursive: true })
    }
  } catch (e) {
    console.error(
      `Remove fail: ${folder}. Please remove it manually. Error: ${e}`,
    )
  }
}

export default handler
