import fs from 'fs'

import {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  BorderStyle,
  WidthType,
  AlignmentType,
  VerticalAlign,
  HeightRule,
  HeadingLevel,
  ImageRun,
  UnderlineType,
} from 'docx'

import { COLOR_SHAPE } from '@/interfaces/constants'
import { sizeOfImage } from 'utils/image/size-of-image'
import { getTextWithoutFontStyle } from 'utils/string'
import { uploadDir } from '@/constants/index'

type TypeResolve={
    code:number,
    filepath?:any,
    message?:string,
  }
export default async function createWordAnswer(
  unitTest: any,
  filePath: string,
) {
  return new Promise<TypeResolve>((resolve, reject) => {
    try {
      const doc = new Document({
        sections: [
          {
            properties: {
              page: {
                margin: {
                  top: '0.5 in',
                  bottom: '0.5 in',
                  left: '0.5 in',
                  right: '0.5 in',
                },
              },
            },
            children: [
              new Table({
                width: { size: 100, type: WidthType.PERCENTAGE },
                borders: {
                  top: { style: BorderStyle.SINGLE },
                  left: { style: BorderStyle.SINGLE },
                  bottom: { style: BorderStyle.SINGLE },
                  right: { style: BorderStyle.SINGLE },
                  insideHorizontal: { style: BorderStyle.SINGLE },
                  insideVertical: { style: BorderStyle.SINGLE },
                },
                rows: [
                  new TableRow({
                    children: [
                      new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                          size: 4,
                          type: WidthType.PERCENTAGE,
                        },
                        borders: {
                          top: { style: BorderStyle.SINGLE, color: '#ffffff' },
                          right: {
                            style: BorderStyle.SINGLE,
                            color: '#ffffff',
                          },
                          bottom: {
                            style: BorderStyle.SINGLE,
                            color: '#ffffff',
                          },
                          left: { style: BorderStyle.SINGLE, color: '#ffffff' },
                        },
                        children: [
                          new Paragraph({
                            alignment: AlignmentType.CENTER,
                            heading: HeadingLevel.HEADING_2,
                            children: [
                              new TextRun({
                                text: unitTest.name,
                                color: '#188fba',
                                bold: true,
                              }),
                              new TextRun({
                                text: `Time allotted: ${unitTest.time}`,
                                break: 1,
                                color: '#188fba',
                                bold: true,
                              }),
                            ],
                          }),
                        ],
                      }),
                    ],
                  }),
                ],
              }),
              ...renderSection(unitTest),
              new Paragraph({
                spacing: { before: 200 },
                alignment: AlignmentType.CENTER,
                children: [
                  new TextRun({
                    text: '---THE END---',
                    color: '#188fba',
                    bold: true,
                  }),
                ],
              }),
            ],
          },
        ],
      })
      Packer.toBuffer(doc).then((buffer) => {
        fs.writeFileSync(filePath, buffer)
        resolve({ code: 1, filepath: filePath})
      })
    } catch (err) {
      console.log('createWordAnswer---', err)
      resolve({ code: 0, message: 'Create Answer: have an error' })
    }
  })
}
function renderSection(data: any) {
  const list: any[] = []

  let currentQuestion = 1

  const sections: any[][] = []
  const sectionList = [...data.sections]
  const lenthSection = sectionList.length;
  while (sectionList.length > 0) {
    sections.push(sectionList.splice(0, 2))
  }
  list.push(new Paragraph(''))
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, color: '#ffffff' },
        insideHorizontal: { style: BorderStyle.SINGLE, color: '#ffffff' },
        insideVertical: { style: BorderStyle.SINGLE, color: '#ffffff' },
      },
      margins: { top: 100, right: 100, left: 100, bottom: 100 },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: sections.map(
        (section2: any[], sIndex: number) =>
          new TableRow({
            children: section2.map(
              (section, s2Index) =>
                new TableCell({
                  width: { size: 50, type: WidthType.PERCENTAGE },
                  children: [
                    new Table({
                      borders: {
                        top: { style: BorderStyle.SINGLE, color: '#188fba' },
                        left: { style: BorderStyle.SINGLE, color: '#188fba' },
                        bottom: { style: BorderStyle.SINGLE, color: '#188fba' },
                        right: { style: BorderStyle.SINGLE, color: '#188fba' },
                        insideHorizontal: {
                          style: BorderStyle.SINGLE,
                          color: '#188fba',
                        },
                        insideVertical: {
                          style: BorderStyle.SINGLE,
                          color: '#188fba',
                        },
                      },
                      margins: { top: 100, right: 100, left: 100, bottom: 100 },
                      alignment: AlignmentType.LEFT,
                      rows: [
                        new TableRow({
                          children: [
                            new TableCell({
                              children: [
                                new Paragraph({
                                  children: [
                                    new TextRun({
                                      text: `PART ${sIndex * 2 + s2Index + 1} `,
                                      color: '#188fba',
                                      bold: true,
                                    }),
                                  ],
                                }),
                              ],
                            }),
                            new TableCell({
                              borders: {
                                top: {
                                  style: BorderStyle.SINGLE,
                                  color: '#ffffff',
                                },
                                right: {
                                  style: BorderStyle.SINGLE,
                                  color: '#ffffff',
                                },
                                bottom: {
                                  style: BorderStyle.SINGLE,
                                  color: '#ffffff',
                                },
                              },
                              children: [
                                new Paragraph({
                                  children: [
                                    new TextRun({
                                      text: `${section.parts[0].name}`,
                                      color: '#188fba',
                                      bold: true,
                                    }),
                                  ],
                                }),
                              ],
                            }),
                          ],
                        }),
                      ],
                    }),
                    ...renderAnswer(section.parts[0].questions),
                  ],
                }),
            ),
          }),
      ),
    }),
  )

  function renderAnswer(questions: any[]) {
    const list1: any[] = []
    questions.forEach((question: any, qIndex: number) => {
      switch (question.question_type) {
        case 'MC1':
          MultiChoiceQuestion1_v2(question, currentQuestion, list1)
          break
        case 'MC2':
          MultiChoiceQuestion2_MC2(question, currentQuestion, list1)
          break
        case 'MC3':
          MultiChoiceQuestion1(question, currentQuestion, list1)
          break
        // case 'MC3':
        case 'MC4':
          MultiChoiceQuestion4(question, currentQuestion, list1)
          break
        case 'MC5':
        case 'MC6':
          MultiChoiceQuestion5(question, currentQuestion, list1)
          break
        case 'MC7':
        case 'MC8':
          MultiChoiceQuestion7(question, currentQuestion, list1)
          break
        case 'MC9':
        case 'MC10':
          MultiChoiceQuestion9(question, currentQuestion, list1)
          break
        case 'MC11':
          MultiChoiceQuestion11(question, currentQuestion, list1)
          break
        case 'DD1':
          DragAndDropQuestion(question, currentQuestion, list1)
          break
        case 'FB1':
          FillInBlankQuestion1(question, currentQuestion, list1)
          break
        case 'FB5':
          FillInBlankQuestion2(question, currentQuestion, list1)
          break
        case 'FB2':
        case 'FB3':
        case 'FB4':
        case 'FB6':
        case 'FB7':
        case 'FB8':
          FillInBlankQuestionHasExample(question, currentQuestion, list1)
          break
        case 'LS1':
        case 'LS2':
          LikertScaleQuestion(question, currentQuestion, list1)
          break
        case 'MG1':
        case 'MG2':
        case 'MG3':
          MatchingGameQuestion(question, currentQuestion, list1)
          break
        case 'SA1':
          ShortAnswerQuestion(question, currentQuestion, list1)
          break
        case 'TF1':
        case 'TF2':
          TrueFalseQuestion(question, currentQuestion, list1)
          break
        case 'MR1':
        case 'MR2':
        case 'MR3':
          MultiResponseQuestion1(question, currentQuestion, list1)
          break
        case 'SL1':
          SelectFromListQuestion1(question, currentQuestion, list1)
          break
        case 'SL2':
          SelectFromListQuestion2(question, currentQuestion, list1)
          break
        case 'MIX1':
          MIXQuestion1(question, currentQuestion, list1)
          break
        case 'MIX2':
          MIXQuestion2(question, currentQuestion, list1)
          break
        case 'TF3':
        case 'TF4':
          TrueFalseQuestion3_4(question, currentQuestion, list1)
          break
        case 'TF5':
          TrueFalseQuestion5(question, currentQuestion, list1)
          break
        case 'MG4':
          MatchingGameQuestion4(question, currentQuestion, list1)
          break
        case 'MG5':
          MatchingGameQuestion5(question, currentQuestion, list1)
          break
        case 'DL1':
        case 'DL3':
          DrawLine1(question, currentQuestion, list1,lenthSection)
        break
        case 'SO1':
          SelectObject1(question, currentQuestion, list1,lenthSection)
        break
        case 'DL2':
          DrawLine2(question, currentQuestion, list1,lenthSection)
        break
        case 'DS1':
          DrawShape1(question, currentQuestion, list1,lenthSection)
          break;
        case 'DD2':
          DragDrop2(question, currentQuestion, list1,lenthSection)
        break
        case 'MG6':
          MatchingGameQuestion6(question, currentQuestion, list1)
          break
        case 'FC1':
          FillInColourType1(question, currentQuestion, list1,lenthSection)
          break
        case 'FC2':
          FillInColourType2(question, currentQuestion, list1,lenthSection)
          break
      }

      if (excludeType.includes(question.question_type)) {
        currentQuestion += 1
      } else {
        currentQuestion += question.total_question
      }
    })
    return list1
  }

  return list
}
const excludeType = ['MR1', 'MR2', 'MR3']
const correctText: any = { T: 'True', F: 'False', NI: 'Not given' }
function DragDrop2(question: any, index: number, list: any[], lenthSection:number) {
  const listCorrect:string[] = question.correct_answers
    .split('#') ?? [];
  const widthImage = lenthSection> 1 ? 32 : 17 ;
  const listImage:string[] = question.image.split("#") ?? []
  // if(listCorrect.length < listImage.length){
  //   listImage.shift()
  // }
  let indexAnswer = -1;
  listCorrect.forEach((item:any, indexItem:number)=>{
    // not example
    if(!item.startsWith("*")){
      indexAnswer +=1;
      const itemImage = listImage[indexItem] ?? ''
      const maxWidth = 80
      const imageUrlAnswer = `${uploadDir}/${itemImage}`
      const imageSizeAnswer = sizeOfImage(imageUrlAnswer)
      let imgHeightAnswer = 80
      let imgWidthAnswer = (imageSizeAnswer.width * maxWidth) / imageSizeAnswer.height
      if (imgWidthAnswer> maxWidth) {
        imgWidthAnswer = maxWidth
        imgHeightAnswer = (imageSizeAnswer.height * maxWidth) / imageSizeAnswer.width
      }
  
      list.push(
        new Paragraph({
          alignment: AlignmentType.LEFT,
          spacing: { before: 150 },
          text: `${index+indexAnswer}.`,
        }),
      )
      list.push(
             new Table({
          margins: { top: 150 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 96, type: WidthType.PERCENTAGE },
          alignment:AlignmentType.RIGHT,
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: {size: widthImage, type: WidthType.PERCENTAGE },
                  verticalAlign:VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      spacing: { before: 50, after: 50 },
                      alignment: AlignmentType.LEFT,
                      children: [
                        new ImageRun({
                          data: fs.readFileSync(imageUrlAnswer),
                          transformation: {
                            width: imgWidthAnswer,
                            height: imgHeightAnswer,
                          },
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 100-widthImage, type: WidthType.PERCENTAGE },
                  verticalAlign:VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      spacing: { before: 50, after: 50 },
                      alignment: AlignmentType.LEFT,
                      children: [
                        new TextRun({
                          text:item
                        }),
                      ],
                    }),
                  ],
                })
              ],
            }),
          ],
        }),
      )
    }
    
  });

}
function DrawLine2(question: any, index: number, list: any[], lenthSection:number) {
  const listCorrect = question.correct_answers
    .split('#').filter((m:string)=> !m.startsWith("ex|"));
  const widthImage = lenthSection>1?32:17;
  listCorrect.forEach((item:any, indexItem:number)=>{
    const itemName = item.split("*")||[]
    const maxWidth = 80
    const imageUrlAnswer = `${uploadDir}/${itemName[0]}`
    const imageSizeAnswer = sizeOfImage(imageUrlAnswer)
    let imgHeightAnswer = 80
    let imgWidthAnswer = (imageSizeAnswer.width * maxWidth) / imageSizeAnswer.height
    if (imgWidthAnswer> maxWidth) {
      imgWidthAnswer = maxWidth
      imgHeightAnswer = (imageSizeAnswer.height * maxWidth) / imageSizeAnswer.width
    }

    const imageUrlCorrect = `${uploadDir}/${itemName[2]}`
    const imageSizeCorrect = sizeOfImage(imageUrlCorrect)
    let imgHeightCorrrect = 80
    let imgWidthCorrect = (imageSizeCorrect.width * maxWidth) / imageSizeCorrect.height
    if (imgWidthCorrect> maxWidth) {
      imgWidthCorrect = maxWidth
      imgHeightCorrrect = (imageSizeCorrect.height * maxWidth) / imageSizeCorrect.width
    }
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index+indexItem}.`,
      }),
    )
    list.push(
           new Table({
        margins: { top: 150 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 96, type: WidthType.PERCENTAGE },
        alignment:AlignmentType.RIGHT,
        rows: [
          new TableRow({
            children: [
              new TableCell({
                width: {size: widthImage, type: WidthType.PERCENTAGE },
                verticalAlign:VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    spacing: { before: 50, after: 50 },
                    alignment: AlignmentType.LEFT,
                    children: [
                      new ImageRun({
                        data: fs.readFileSync(imageUrlAnswer),
                        transformation: {
                          width: imgWidthAnswer,
                          height: imgHeightAnswer,
                        },
                      }),
                    ],
                  }),
                ],
              }),
              new TableCell({
                width: { size: 100-widthImage, type: WidthType.PERCENTAGE },
                verticalAlign:VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    spacing: { before: 50, after: 50 },
                    alignment: AlignmentType.LEFT,
                    children: [
                      new ImageRun({
                        data: fs.readFileSync(imageUrlCorrect),
                        transformation: {
                          width: imgWidthCorrect,
                          height: imgHeightCorrrect,
                        },
                      }),
                    ],
                  }),
                ],
              })
            ],
          }),
        ],
      }),
    )
  });

}
function DrawLine1(question: any, index: number, list: any[], lenthSection:number) {
  const listCorrect = question.correct_answers
    .split('#').filter((m:string)=> !m.startsWith("ex|"));
  const widthImage = lenthSection>1?30:16;
  listCorrect.forEach((item:any, indexItem:number)=>{
    const itemName = item.split("*")||[]
                const imageUrl = `${uploadDir}/${itemName[0]}`
                const imageSize = sizeOfImage(imageUrl)
                const maxWidth = 80
                let imgHeight = 80
                let imgWidth = (imageSize.width * maxWidth) / imageSize.height
                if (imgWidth > maxWidth) {
                  imgWidth = maxWidth
                  imgHeight = (imageSize.height * maxWidth) / imageSize.width
                }
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index+indexItem}.`,
      }),
    )
    list.push(
           new Table({
        margins: { top: 150 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 96, type: WidthType.PERCENTAGE },
        alignment:AlignmentType.RIGHT,
        rows: [
          new TableRow({
            children: [
              new TableCell({
                width: {size: widthImage, type: WidthType.PERCENTAGE },
                verticalAlign:VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    spacing: { before: 50, after: 50 },
                    alignment: AlignmentType.LEFT,
                    children: [
                      new ImageRun({
                        data: fs.readFileSync(imageUrl),
                        transformation: {
                          width: imgWidth,
                          height: imgHeight,
                        },
                      }),
                    ],
                  }),
                ],
              }),
              new TableCell({
                width: { size: 100-widthImage, type: WidthType.PERCENTAGE },
                verticalAlign:VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                      alignment: AlignmentType.LEFT,
                      children: [
                        new TextRun(
                          `${itemName[2]}`,
                        ),
                      ],
                    }),
                ],
              })
            ],
          }),
        ],
      }),
    )
  });
  // list.push(
  //   new Paragraph({
  //     alignment: AlignmentType.LEFT,
  //     spacing: { before: 150 },
  //     text: `${index}.`,
  //   }),
  // )
  // let divide = 5;
  // const lenAnswer = listCorrect.length;
  // if(lenAnswer <=4){
  //   divide = lenAnswer
  // }else if(lenAnswer <=8){
  //   divide = 4
  // }
  // const listItemTop = []
  // const listItemBottom = []

  // for(let i=0; i< lenAnswer; i++){
  //   if(i<divide){
  //     listItemTop.push(listCorrect[i])
  //   }else{
  //     listItemBottom.push(listCorrect[i])
  //   }
  // }
  // if(listItemTop.length>0){
  //   list.push(
  //     new Table({
  //       margins: { top: 150 },
  //       borders: {
  //         top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
  //         left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
  //         bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
  //         right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
  //         insideHorizontal: {
  //           style: BorderStyle.SINGLE,
  //           size: 1,
  //           color: '#ffffff',
  //         },
  //         insideVertical: {
  //           style: BorderStyle.SINGLE,
  //           size: 1,
  //           color: '#ffffff',
  //         },
  //       },
  //       width: { size: 100, type: WidthType.PERCENTAGE },
  //       rows: [
  //         new TableRow({
  //           children: [
  //             ...listItemTop.map((item: any, itemIndex: number) => {
  //               const itemName = item.split("*")||[]
  //               const imageUrl = `${uploadDir}/${itemName[0]}`
  //               const imageSize = sizeOf(imageUrl)
  //               const maxWidth = 90
  //               let imgHeight = 90
  //               let imgWidth = (imageSize.width * maxWidth) / imageSize.height
  //               if (imgWidth > maxWidth) {
  //                 imgWidth = maxWidth
  //                 imgHeight = (imageSize.height * maxWidth) / imageSize.width
  //               }
  //               return new TableCell({
  //                 width: { size: 1, type: WidthType.PERCENTAGE },
  //                 verticalAlign:VerticalAlign.BOTTOM,
  //                 children: [
  //                   new Paragraph({
  //                     spacing: { before: 50, after: 50 },
  //                     alignment: AlignmentType.CENTER,
  //                     children: [
  //                       new ImageRun({
  //                         data: fs.readFileSync(imageUrl),
  //                         transformation: {
  //                           width: imgWidth,
  //                           height: imgHeight,
  //                         },
  //                       }),
  //                     ],
  //                   }),
  //                 ],
  //               })
  //             }),
  //           ],
  //         }),
  //       ],
  //     }),
  //   )
  //   list.push(
  //     new Table({
  //       margins: { top: 100 },
  //       borders: {
  //         top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
  //         left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
  //         bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
  //         right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
  //         insideHorizontal: {
  //           style: BorderStyle.SINGLE,
  //           size: 1,
  //           color: '#ffffff',
  //         },
  //         insideVertical: {
  //           style: BorderStyle.SINGLE,
  //           size: 1,
  //           color: '#ffffff',
  //         },
  //       },
  //       width: { size: 100, type: WidthType.PERCENTAGE },
  //       rows: [
  //         new TableRow({
  //           children: [
  //             ...listItemTop.map((item: any, itemIndex: number) => {
  //               const itemName = item.split("*")||[]
  //               return new TableCell({
  //                 width: { size: 1, type: WidthType.PERCENTAGE },
  //                 verticalAlign:VerticalAlign.TOP,
  //                 children: [
  //                   new Paragraph({
  //                     alignment: AlignmentType.CENTER,
  //                     children: [
  //                       new TextRun(
  //                         `${itemName[2]}`,
  //                       ),
  //                     ],
  //                   }),
  //                 ],
  //               })
  //             }),
  //           ],
  //         }),
  //       ],
  //     }),
  //   )
  // }

}
//select object type 1
function SelectObject1(question: any, index: number, list: any[], lenthSection:number) {
  const listCorrect = question.correct_answers
    .split('#').filter((m:string)=> !m.startsWith("ex|") && m.split('*')[2] === 'True');
  const widthImage = lenthSection>1?30:16;
  listCorrect.forEach((item:any, indexItem:number)=>{
    const itemName = item.split("*")||[]
                const imageUrl = `${uploadDir}/${itemName[0]}`
                const imageSize = sizeOfImage(imageUrl)
                const maxWidth = 80
                let imgHeight = 80
                let imgWidth = (imageSize.width * maxWidth) / imageSize.height
                if (imgWidth > maxWidth) {
                  imgWidth = maxWidth
                  imgHeight = (imageSize.height * maxWidth) / imageSize.width
                }
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index+indexItem}.`,
      }),
    )
    list.push(
           new Table({
        margins: { top: 150 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 96, type: WidthType.PERCENTAGE },
        alignment:AlignmentType.RIGHT,
        rows: [
          new TableRow({
            children: [
              new TableCell({
                width: {size: widthImage, type: WidthType.PERCENTAGE },
                verticalAlign:VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    spacing: { before: 50, after: 50 },
                    alignment: AlignmentType.LEFT,
                    children: [
                      new ImageRun({
                        data: fs.readFileSync(imageUrl),
                        transformation: {
                          width: imgWidth,
                          height: imgHeight,
                        },
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
    )
  });
}
//fill in colour type 1
function FillInColourType1(question: any, index: number, list: any[], lenthSection:number) {
  const listCorrect = question?.answers?.split('*')[1]
  let tempArr:any[] = []
  if(listCorrect){
    const file = fs.readFileSync(`${uploadDir.replace('/upload', '')}/${listCorrect}`,'utf8')
    tempArr = JSON.parse(file)
    const widthImage = lenthSection>1?30:16;
  tempArr?.forEach((item:any, indexItem:number)=>{
                const imageUrl = `${uploadDir}/${item}`
                const imageSize = sizeOfImage(imageUrl)
                const maxWidth = 80
                let imgHeight = 80
                let imgWidth = (imageSize.width * maxWidth) / imageSize.height
                if (imgWidth > maxWidth) {
                  imgWidth = maxWidth
                  imgHeight = (imageSize.height * maxWidth) / imageSize.width
                }
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index+indexItem}.`,
      }),
    )
    list.push(
           new Table({
        margins: { top: 150 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 96, type: WidthType.PERCENTAGE },
        alignment:AlignmentType.RIGHT,
        rows: [
          new TableRow({
            children: [
              new TableCell({
                width: {size: widthImage, type: WidthType.PERCENTAGE },
                verticalAlign:VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    spacing: { before: 50, after: 50 },
                    alignment: AlignmentType.LEFT,
                    children: [
                      new ImageRun({
                        data: fs.readFileSync(imageUrl),
                        transformation: {
                          width: imgWidth,
                          height: imgHeight,
                        },
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
    )
  });
  }
  
  
}
//fill in colour type 2
function FillInColourType2(question: any, index: number, list: any[], lenthSection:number) {
  const listCorrect = question?.answers?.split('*')[1]
  let tempArr:any[] = []
  if(listCorrect){
    const file = fs.readFileSync(`${uploadDir.replace('/upload', '')}/${listCorrect}`,'utf8')
    tempArr = JSON.parse(file)
    const widthImage = lenthSection>1?30:16;
  tempArr?.forEach((item:any, indexItem:number)=>{
                const imageUrl = `${uploadDir}/${item}`
                const imageSize = sizeOfImage(imageUrl)
                const maxWidth = 80
                let imgHeight = 80
                let imgWidth = (imageSize.width * maxWidth) / imageSize.height
                if (imgWidth > maxWidth) {
                  imgWidth = maxWidth
                  imgHeight = (imageSize.height * maxWidth) / imageSize.width
                }
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index+indexItem}.`,
      }),
    )
    list.push(
           new Table({
        margins: { top: 150 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 96, type: WidthType.PERCENTAGE },
        alignment:AlignmentType.RIGHT,
        rows: [
          new TableRow({
            children: [
              new TableCell({
                width: {size: widthImage, type: WidthType.PERCENTAGE },
                verticalAlign:VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    spacing: { before: 50, after: 50 },
                    alignment: AlignmentType.LEFT,
                    children: [
                      new ImageRun({
                        data: fs.readFileSync(imageUrl),
                        transformation: {
                          width: imgWidth,
                          height: imgHeight,
                        },
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
    )
  });
  }
  
  
}
//draw shape type 1
function DrawShape1(question: any, index: number, list: any[], lenthSection:number) {
  const listCorrect = question.answers
    .split('#').filter((m:string)=> !m.startsWith("ex|"));
  
  const widthImage = lenthSection > 1 ? 30 : 16;
  
  listCorrect.forEach((item:string, indexItem:number)=>{
    const listPropertyItem = item.split('*')
    const colorName = COLOR_SHAPE.find((item:{color:string, display:string}) => item?.color === listPropertyItem[2])?.display
    const imageUrl = `public/images/shapes/${listPropertyItem[1]}-${colorName}.png`
    const imageSize = sizeOfImage(imageUrl)
    const maxWidth = 80
    let imgHeight = 80
    let imgWidth = (imageSize.width * maxWidth) / imageSize.height
    if (imgWidth > maxWidth) {
      imgWidth = maxWidth
      imgHeight = (imageSize.height * maxWidth) / imageSize.width
    }
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index+indexItem}.`,
      }),
    )
    list.push(
           new Table({
        margins: { top: 150 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 96, type: WidthType.PERCENTAGE },
        alignment:AlignmentType.RIGHT,
        rows: [
          new TableRow({
            children: [
              new TableCell({
                width: {size: widthImage, type: WidthType.PERCENTAGE },
                verticalAlign:VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    spacing: { before: 50, after: 50 },
                    alignment: AlignmentType.LEFT,
                    children: [
                      new ImageRun({
                        data: fs.readFileSync(imageUrl),
                        transformation: {
                          width: imgWidth,
                          height: imgHeight,
                        },
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
    )
  });
}

function MultiChoiceQuestion1(question: any, index: number, list: any[]) {
  const answers = question.answers.split('*')
  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${String.fromCharCode(
        65 + answers.findIndex((m: string) => m === question.correct_answers),
      )}`,
    }),
  )
}
function MultiChoiceQuestion4(question: any, index: number, list: any[]) {
  const answers = question.answers
    .split('*')
    .map((m: string) => getTextWithoutFontStyle(m))
  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${String.fromCharCode(
        65 + answers.findIndex((m: string) => m === question.correct_answers),
      )}`,
    }),
  )
}
function MultiChoiceQuestion2_MC2(question: any, index: number, list: any[]) {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const correctList = question.correct_answers?.split('#')
  while (questionList.length > 0) {
    if (questionList[0].startsWith('*')) {
      questionList.shift()
      answerList.shift()
      correctList.shift()
    } else {
      break
    }
  }
  questionList.forEach((ques: any, qIndex: number) => {
    const answers = answerList[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + qIndex}. ${String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string) => getTextWithoutFontStyle(m) === correctList[qIndex],
            ),
        )}`,
      }),
    )
  })
}
function MultiChoiceQuestion1_v2(question: any, index: number, list: any[]) {

  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const correctList = question.correct_answers?.split('#')
  let i = 0;
  while (i < questionList.length) {
    if (questionList[i].startsWith('*')) {
      questionList.splice(i, 1);
      answerList.splice(i, 1);
      correctList.splice(i, 1);
    } else {
      i+=1;
    }
  }
  questionList.forEach((ques: string, qIndex: number) => {
    const answers = answerList[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + qIndex}. ${String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string) => getTextWithoutFontStyle(m) === correctList[qIndex],
            ),
        )}`,
      }),
    )
    
  })
}
function MultiChoiceQuestion5(question: any, index: number, list: any[]) {
  const questionList:string[] = question.question_text?.split('#') ?? []
  const answerList:string[] = question.answers?.split('#') ?? []
  const correctList:string[] = question.correct_answers?.split('#') ?? []
  while (questionList.length > 0) {
    if (questionList[0].startsWith('*')) {
      questionList.shift()
      answerList.shift()
      correctList.shift()
    } else {
      break
    }
  }
  questionList.forEach((ques: string, qIndex: number) => {
    const answers = answerList[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + qIndex}. ${String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string) => getTextWithoutFontStyle(m) === correctList[qIndex],
            ),
        )}`,
      }),
    )
  })
}
function MultiChoiceQuestion7(question: any, index: number, list: any[]) {
  const questionList = question.question_text?.split('#') || []
  const answerList = question.answers?.split('#') || []
  const correctList = question.correct_answers?.split('#') || []
  while (questionList.length > 0) {
    if (questionList[0].startsWith('*')) {
      questionList.shift()
      answerList.shift()
      correctList.shift()
    } else {
      break
    }
  }
  questionList.forEach((ques: any, qIndex: number) => {
    const answers = answerList[qIndex].split('*') || []
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + qIndex}. ${String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string,mIndex:number) => mIndex.toString() === correctList[qIndex],
            ),
        )}`,
      }),
    )
  })
}

function MultiChoiceQuestion9(question: any, index: number, list: any[]) {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const correctList = question.correct_answers?.split('#')
  while (questionList.length > 0) {
    if (questionList[0].startsWith('*')) {
      questionList.shift()
      answerList.shift()
      correctList.shift()
    } else {
      break
    }
  }
  questionList.forEach((ques: any, qIndex: number) => {
    const answers = answerList[qIndex].split('*') || []
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + qIndex}. ${String.fromCharCode(
          65 +
            answers.findIndex(
              (m: string) => getTextWithoutFontStyle(m) === correctList[qIndex],
            ),
        )}`,
      }),
    )
  })
}

function MultiChoiceQuestion11(question: any, index: number, list: any[]) {
  let answerString = question.answers?.split('#')
  let correctString = question.correct_answers?.split('#')
  // let exmapleLength = answerString.filter((item: string) => item.startsWith('ex')).length ?? 0
  const answerGroup: any = []
  const correctGroup: any = []
  
  answerString.map((item: string, _indexCurrent: number) => {
    if ( ! item.startsWith('ex') ){
      answerGroup.push(item)
      correctGroup.push(correctString[_indexCurrent])
    }
  })

  answerGroup.map((item: string, _indexCurrent: number) => {
    let itemArray = item.split('*') || []
    let questionNumber = _indexCurrent + index
    let letterChar: any [] = [] 

    itemArray.map((v: string, i: number) => {
      const correctArr = correctGroup[_indexCurrent].split("*")||[]
      if(correctArr.findIndex((m:any)=>m==i) !=-1){
        letterChar.push( String.fromCharCode( 65 + i ) )
      }
    })
    list.push(new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${questionNumber}. ${letterChar.join(', ')}`,
    }))
  })

}

function DragAndDropQuestion(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correctAnswers = question.correct_answers.split('#')
  const checkExam = question.question_text?.split('#') || ''
  const correctList: string[] = []

  for (let i = 0; i < correctAnswers.length; i++) {
    if (answers[i]) {
      if (!checkExam || checkExam[i] !== '*') {
        correctList.push(correctAnswers[i])
      }
    }
  }

  correctList.forEach((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${answer
          .replace(/\*/g, ' ')
          .replace(/\%\/\%/g, '/')}`,
      }),
    )
  })
}

function FillInBlankQuestion1(question: any, index: number, list: any[]) {
  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${question.correct_answers.replace(/\%\/\%/g, '/')}`,
    }),
  )
}

function FillInBlankQuestion2(question: any, index: number, list: any[]) {
  const correctList = question.correct_answers
    .split('#')
    .map((m: string) => m.replace(/\%\/\%/g, '/'))
  correctList.forEach((part: string, partIndex: number) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + partIndex}. ${part}`,
      }),
    )
  })
}

function FillInBlankQuestionHasExample(question: any, index: number, list: any[]) {
  const correctAnswers = question.correct_answers.split('#')
  const totalLength = correctAnswers.length
  const correctList: string[] = []
  for(let i = 0; i < totalLength; i++){
    if (!correctAnswers[i].startsWith('*')){
      correctList.push(correctAnswers[i].replace(/\%\/\%/g, '/'))
    }
  }

  correctList.forEach((part: string, partIndex: number) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + partIndex}. ${part}`,
      }),
    )
  })
}

function LikertScaleQuestion(question: any, index: number, list: any[]) {
  const correctList: string[] = question.correct_answers.split('#')

  correctList.forEach((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })
}
function MatchingGameQuestion4(question: any, index: number, list: any[]) {
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const correctList: string[] = question.correct_answers.split('#')

  //check and remove example
  if(listAnswer[2] && listAnswer[2][0]=="ex"){
    // const indexRight = listAnswer[1].indexOf(correctList[0]);
    // if (indexRight > -1) {
    //   listAnswer[1].splice(indexRight, 1); 
    // }
    listAnswer[0].shift();
    correctList.shift();
  }
  listAnswer[0].forEach((_, mIndex: number) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + mIndex}. ${String.fromCharCode(
          65 +
            listAnswer[1].findIndex((m: string) => m === correctList[mIndex]),
        )}`,
      }),
    )
  })
}
function MatchingGameQuestion5(question: any, index: number, list: any[]) {
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const correctList: string[] = question.correct_answers.split('#')

  //check and remove example
  if(listAnswer[2] && listAnswer[2][0]=="ex"){
    // const indexRight = listAnswer[1].indexOf(correctList[0]);
    // if (indexRight > -1) {
    //   listAnswer[1].splice(indexRight, 1); 
    // }
    listAnswer[0].shift();
    correctList.shift();
  }
  listAnswer[0].forEach((_, mIndex: number) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + mIndex}. ${String.fromCharCode(
          65 +
            listAnswer[1].findIndex((m: string) => m === correctList[mIndex]),
        )}`,
      }),
    )
  })
}
function MatchingGameQuestion6(question: any, index: number, _paragraph: any[]){
  const answerGroup: string[][] =  question.answers.split('#')
    .map((item: string) => item.split('*'))
  const correctArray: string[] = question.correct_answers.split('#')
  const maxExample: number = answerGroup[2] ? answerGroup[2].filter((item) => item.startsWith('ex')).length : 0

  if( maxExample > 0 ){
    answerGroup[0].splice(0, maxExample)
    correctArray.splice(0, maxExample)
  }

  answerGroup[0].forEach((_: string, i: number) =>{
    const indexCharacter = answerGroup[1]
      .findIndex((item: string) => item === correctArray[i])
    _paragraph.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${i + index}. ${String.fromCharCode( 65 + indexCharacter ).toUpperCase()}`
      })
    )
  })

}
function MatchingGameQuestion(question: any, index: number, list: any[]) {
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const correctList: string[] = question.correct_answers.split('#')

  listAnswer[0].forEach((_, mIndex: number) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + mIndex}. ${String.fromCharCode(65 +
            listAnswer[1].findIndex((m: string) => m === correctList[mIndex]),
        )}`,
      }),
    )
  })
}

function ShortAnswerQuestion(question: any, index: number, list: any[]) {
  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${question.correct_answers.replace(/\%\/\%/g, '/')}`,
    }),
  )
}
function TrueFalseQuestion(question: any, index: number, list: any[]) {
  const correctList: string[] = question.correct_answers.split('#')
  correctList.forEach((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })
}
//template TF3_4
function TrueFalseQuestion3_4(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correcrAnswers = question.correct_answers.split('#')
  const correctList: string[] = []
  //remove example
  for (let i = 0; i < correcrAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        correctList.push(correcrAnswers[i])
      }
    }
  }
  correctList.forEach((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })
}

function TrueFalseQuestion5(question: any, index: number, _export: any[]){
  const answers = question.answers.split('#')
  const corrects = question.correct_answers.split('#')
  const list: string[] = []
  for( let _index = 0; _index < corrects.length; _index++ ){
    if( ! answers[_index].startsWith('*')  ) 
      list.push( corrects[_index] )
  }
  list.forEach((value, _index)=>{
    _export.push( new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index + _index}. ${correctText[value]} `
    }))
  })
}

function MultiResponseQuestion1(question: any, index: number, list: any[]) {
  const answers: string[] = question.answers.split('#')
  const correctList: string[] = question.correct_answers.split('#')

  list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index}. ${answers
        .map((answer, answerIndex) =>
          correctList.includes(answer)
            ? String.fromCharCode(65 + answerIndex)
            : null,
        )
        .filter((m) => m !== null)
        .join(', ')}`,
    }),
  )
}

function SelectFromListQuestion1(question: any, index: number, list: any[]) {
  const listAnswers = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const listCorrect = question.correct_answers.split('#')
  let  answersIndex = 0;

  for(let i=0; i< listAnswers.length;i++){
   const answers = listAnswers[i];
   const isExample = listCorrect[i].startsWith("*")
   if(isExample) continue;
   list.push(
    new Paragraph({
      alignment: AlignmentType.LEFT,
      spacing: { before: 150 },
      text: `${index + answersIndex}. ${String.fromCharCode(
        65 +
          answers.findIndex(
            (m: string) => m.trim() === listCorrect[i].trim(),
          ),
      )}`,
    }))
    answersIndex +=1;
  }
}

function SelectFromListQuestion2(question: any, index: number, list: any[]) {
  const listCorrect: string[] = question.correct_answers.split('#')
  const listAnswers = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  listCorrect.forEach((correct: string, _index) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + _index}. ${String.fromCharCode(
          65 + listAnswers[_index].findIndex((m: string) => m?.trim() === correct?.trim()),
        )}`,
      }),
    )
  })
}

function MIXQuestion1(question: any, index: number, list: any[]) {
  const listAnswers: string[] = question.answers.split('[]')
  const tfAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction: string[] = question.question_description.split('[]')
  const mcQuestions: string[] = question.question_text.split('[]')[1].split('#')
  const listCorrect: string[] = question.correct_answers.split('[]')
  const tfCorrects: string[] = listCorrect[0].split('#')
  const mcCorrects: string[] = listCorrect[1].split('#')

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 150,
      },
      children: [
        new TextRun({
          text: `Task 1: ${listInstruction[0]}`,
          bold: true,
        }),
      ],
    }),
  )

  tfCorrects.map((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 150,
      },
      children: [
        new TextRun({
          text: `Task 2: ${listInstruction[1]}`,
          bold: true,
        }),
      ],
    }),
  )

  mcQuestions.forEach((ques: any, qIndex: number) => {
    const answers = mcAnswers[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + tfAnswers.length + qIndex}. ${String.fromCharCode(
          65 + answers.findIndex((m: string) => getTextWithoutFontStyle(m) === mcCorrects[qIndex]),
        )}`,
      }),
    )
  })
}

function MIXQuestion2(question: any, index: number, list: any[]) {
  const listAnswers: string[] = question.answers.split('[]')
  const lsAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction: string[] = question.question_description.split('[]')
  const mcQuestions: string[] = question.question_text.split('[]')[1].split('#')
  const listCorrect: string[] = question.correct_answers.split('[]')
  const lsCorrects: string[] = listCorrect[0].split('#')
  const mcCorrects: string[] = listCorrect[1].split('#')

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 150,
      },
      children: [
        new TextRun({
          text: `Task 1: ${listInstruction[0]}`,
          bold: true,
        }),
      ],
    }),
  )

  lsCorrects.map((answer, answerIndex) => {
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + answerIndex}. ${correctText[answer]}`,
      }),
    )
  })

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 150,
      },
      children: [
        new TextRun({
          text: `Task 2: ${listInstruction[1]}`,
          bold: true,
        }),
      ],
    }),
  )

  mcQuestions.forEach((ques: any, qIndex: number) => {
    const answers = mcAnswers[qIndex].split('*')
    list.push(
      new Paragraph({
        alignment: AlignmentType.LEFT,
        spacing: { before: 150 },
        text: `${index + lsAnswers.length + qIndex}. ${String.fromCharCode(
          65 + answers.findIndex((m: string) => getTextWithoutFontStyle(m) === mcCorrects[qIndex]),
        )}`,
      }),
    )
  })
}
