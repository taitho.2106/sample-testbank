import fs from 'fs'

import {
    Document,
    Packer,
    Paragraph,
    TextRun,
    Table,
    TableCell,
    TableRow,
    BorderStyle,
    WidthType,
    AlignmentType,
    VerticalAlign,
    HeadingLevel,
} from 'docx'

type TypeResolve = {
    code: number
    filepath?: any,
    message?: string,
}

const excludeType = ['MR1', 'MR2', 'MR3']

export default async function createWordAudioScript(
    unitTest: any,
    filePath: string,
) {
    return new Promise<TypeResolve>((resolve, reject) => {
        try {
            const doc = createDocument(unitTest);
            Packer.toBuffer(doc).then((buffer) => {
                fs.writeFileSync(filePath, buffer)
                resolve({
                    code: 1,
                    filepath: filePath
                })
            })
        } catch (err) {
            resolve({
                code: 0,
                message: 'Create Word Audio Script: have an error'
            })
        }
    })
}

function createDocument(unitTest: any): Document {
    const doc = new Document({
        sections: [
            {
                properties: {
                    page: {
                        margin: {
                            top: '0.5 in',
                            bottom: '0.5 in',
                            left: '0.5 in',
                            right: '0.5 in',
                        },
                    },
                },
                children: [
                    new Table({
                        width: { size: 100, type: WidthType.PERCENTAGE },
                        borders: {
                            top: { style: BorderStyle.SINGLE },
                            left: { style: BorderStyle.SINGLE },
                            bottom: { style: BorderStyle.SINGLE },
                            right: { style: BorderStyle.SINGLE },
                            insideHorizontal: { style: BorderStyle.SINGLE },
                            insideVertical: { style: BorderStyle.SINGLE },
                        },
                        rows: [
                            new TableRow({
                                children: [
                                    new TableCell({
                                        verticalAlign: VerticalAlign.CENTER,
                                        width: {
                                            size: 4,
                                            type: WidthType.PERCENTAGE,
                                        },
                                        borders: {
                                            top: { style: BorderStyle.SINGLE, color: '#ffffff' },
                                            right: {
                                                style: BorderStyle.SINGLE,
                                                color: '#ffffff',
                                            },
                                            bottom: {
                                                style: BorderStyle.SINGLE,
                                                color: '#ffffff',
                                            },
                                            left: { style: BorderStyle.SINGLE, color: '#ffffff' },
                                        },
                                        children: [
                                            new Paragraph({
                                                alignment: AlignmentType.CENTER,
                                                heading: HeadingLevel.HEADING_2,
                                                children: [
                                                    new TextRun({
                                                        text: unitTest.name,
                                                        color: '#188fba',
                                                        bold: true,
                                                    }),
                                                    new TextRun({
                                                        text: `Time allotted: ${unitTest.time}`,
                                                        break: 1,
                                                        color: '#188fba',
                                                        bold: true,
                                                    }),
                                                ],
                                            }),
                                        ],
                                    }),
                                ],
                            }),
                        ],
                    }),
                    ...renderSection(unitTest),
                    new Paragraph({
                        spacing: { before: 200 },
                        alignment: AlignmentType.CENTER,
                        children: [
                            new TextRun({
                                text: '---THE END---',
                                color: '#188fba',
                                bold: true,
                            }),
                        ],
                    }),
                ],
            },
        ],
    })
    return doc;
}

function renderSection(data: any) {
    const list: any[] = []
    let currentQuestion = 1

    data.sections.forEach((section: any, sIndex: number) => {
        const partHasAudio = section.parts[0].questions.some((item: any) => item.audio);
        list.push(new Paragraph(''))
        {
            partHasAudio && (
                list.push(
                    new Table({
                        borders: {
                            top: { style: BorderStyle.SINGLE, color: '#188fba' },
                            left: { style: BorderStyle.SINGLE, color: '#188fba' },
                            bottom: { style: BorderStyle.SINGLE, color: '#188fba' },
                            right: { style: BorderStyle.SINGLE, color: '#188fba' },
                            insideHorizontal: { style: BorderStyle.SINGLE, color: '#188fba' },
                            insideVertical: { style: BorderStyle.SINGLE, color: '#188fba' },
                        },
                        margins: { top: 100, right: 100, left: 100, bottom: 100 },
                        alignment: AlignmentType.LEFT,
                        rows: [
                            new TableRow({
                                children: [
                                    new TableCell({
                                        children: [
                                            new Paragraph({
                                                children: [
                                                    new TextRun({
                                                        text: `PART ${sIndex + 1} `,
                                                        color: '#188fba',
                                                        bold: true,
                                                    }),
                                                ],
                                            }),
                                        ],
                                    }),
                                    new TableCell({
                                        borders: {
                                            top: { style: BorderStyle.SINGLE, color: '#ffffff' },
                                            right: { style: BorderStyle.SINGLE, color: '#ffffff' },
                                            bottom: { style: BorderStyle.SINGLE, color: '#ffffff' },
                                        },
                                        children: [
                                            new Paragraph({
                                                children: [
                                                    new TextRun({
                                                        text: section.parts[0].name,
                                                        color: '#188fba',
                                                        bold: true,
                                                    }),
                                                ],
                                            }),
                                        ],
                                    }),
                                ],
                            }),
                        ],
                    }),
                )
            )
        }

        section.parts[0].questions.forEach((question: any, qIndex: number) => {
            switch (question.question_type) {
                case 'MC6':
                case 'MC7':
                case 'MC10':
                case 'MC11':
                case 'FB3':
                case 'FB4':
                case 'FB5':
                case 'SL2':
                case 'DD2':
                case 'DL1':
                case 'DL2':
                case 'TF1':
                case 'TF5':
                case 'MG3':
                case 'MR2':
                case 'MR3':
                case 'LS2':
                case 'SO1':
                case 'DS1':
                case 'FC1':
                    RenderAudioScript(question, currentQuestion, list)
                    break
                default: break
            }

            if (excludeType.includes(question.question_type)) {
                currentQuestion += 1
            } else {
                currentQuestion += question.total_question
            }
        })
    })

    return list
}

function RenderAudioScript(question: any, index: number, list: any[]) {
    const totalQuestion = question.total_question
    list.push(
        new Paragraph({
            spacing: {
                before: 200,
            },
            children: [
                new TextRun({
                    text: totalQuestion > 1 ? `${index} - ${index + totalQuestion - 1}` : `${index}`,
                    bold: true,
                }),
            ],
        }),
    )

    const TextRunAudio = question?.audio_script ? question.audio_script.split('\n') : []
    list.push(
        new Paragraph({
            children: TextRunAudio.map(
                (item: string, mIndex: number) =>
                    new TextRun({
                        text: item,
                        break: mIndex > 0 ? 1 : 0,
                    }),
            ),
        }),
    )

    return list
}