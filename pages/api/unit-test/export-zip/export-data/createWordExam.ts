import fs from 'fs'
import { type } from 'os'

import {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  BorderStyle,
  WidthType,
  AlignmentType,
  VerticalAlign,
  HeightRule,
  HeadingLevel,
  ImageRun,
  UnderlineType,
  TabStopType,
} from 'docx'

import { uploadDir } from '@/constants/index'
import { COLOR_SHAPE } from '@/interfaces/constants'
import convertTextCell from '@/questions_middleware/convertTextCell'
import { sizeOfImage } from 'utils/image/size-of-image'
import { formatTextStyleObject, getFileExtension } from 'utils/string'

import shuffleArray from '../mix-data/shuffleArray'

const trueColor = '#009d12'
const falseColor = '#d25045'
type TypeResolve = {
  code: number
  filepath?: any
  message?: string
  fileAudios?: FileAudioType[]
  audioFile?: AudioInfo[]
}
type FileAudioType = {
  filePath: string
  newName: string
}
type AudioInfo = {
  path: string
  index: number
}
const space = '______________'
export default async function createWordExam(
  unitTest: any,
  filePath: string,
) {

  const listAudioInfo: AudioInfo[] = []
  function getAudioPath(question: any, indexQuestion: number) {
    try {
      if (question.audio) {
        const filePath = `${uploadDir}/${question.audio}`
        const fileExt = getFileExtension(filePath)
        if (fs.existsSync(filePath)) {
          listAudioInfo.push({
            path: question.audio,
            index: indexQuestion
          })
        } else {
          console.log('question.audio---not found', filePath)
        }
      }
    } catch (err) {}
  }
  function renderSection(data: any) {
    const list: any[] = []
    let currentQuestion = 1
    data.sections.forEach((section: any, sIndex: number) => {

      let totalQuestionPart = 0;
      section.parts[0].questions.forEach((item:any, indexItem:any)=>{
        if (excludeType.includes(item.question_type)) {
          totalQuestionPart += 1
        } else {
          totalQuestionPart += item.total_question;
        }
      })
      list.push(new Paragraph(''))
      list.push(
        new Table({
          borders: {
            top: { style: BorderStyle.SINGLE, color: '#188fba' },
            left: { style: BorderStyle.SINGLE, color: '#188fba' },
            bottom: { style: BorderStyle.SINGLE, color: '#188fba' },
            right: { style: BorderStyle.SINGLE, color: '#188fba' },
            insideHorizontal: { style: BorderStyle.SINGLE, color: '#188fba' },
            insideVertical: { style: BorderStyle.SINGLE, color: '#188fba' },
          },
          margins: { top: 100, right: 100, left: 100, bottom: 100 },
          alignment: AlignmentType.LEFT,
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: `PART ${sIndex + 1} `,
                          color: '#188fba',
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  borders: {
                    top: { style: BorderStyle.SINGLE, color: '#ffffff' },
                    right: { style: BorderStyle.SINGLE, color: '#ffffff' },
                    bottom: { style: BorderStyle.SINGLE, color: '#ffffff' },
                  },
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: `${section.parts[0].name}${totalQuestionPart>1?` (${totalQuestionPart} questions)`:""}`,
                          color: '#188fba',
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
  
      section.parts[0].questions.forEach((question: any, qIndex: number) => {
        getAudioPath(question, currentQuestion)
        switch (question.question_type) {
          case 'MC1':
            MultiChoiceQuestion1_v2(question, currentQuestion, list)
            break
          case 'MC2':
            MultiChoiceQuestion2_v2(question, currentQuestion, list)
            break
          // case 'MC3':
          //   MultiChoiceQuestion1(question, currentQuestion, list)
          //   break
          case 'MC3':
          case 'MC4':
            MultiChoiceQuestion4(question, currentQuestion, list)
            break
          case 'MC5':
          case 'MC6':
            MultiChoiceQuestion5_v2(question, currentQuestion, list)
            break
          // case 'MC6':
          //   MultiChoiceQuestion5(question, currentQuestion, list)
          //   break
          case 'MC7':
          case 'MC8':
            MultiChoiceQuestion7(question, currentQuestion, list)
            break
          case 'MC9':
          case 'MC10':
            MultiChoiceQuestion9(question, currentQuestion, list)
            break
          case 'MC11':
            MultiChoiceQuestion11(question, currentQuestion, list)
            break
          case 'DD1':
            DragAndDropQuestion(question, currentQuestion, list)
            break
          case 'FB1':
            FillInBlankQuestion1(question, currentQuestion, list)
            break
          case 'FB2':
          case 'FB3':
            FillInBlankQuestion2_3_v2(question, currentQuestion, list)
            break
          case 'FB4':
            FillInBlankQuestion4_v2(question, currentQuestion, list)
            break
          case 'FB6':
            FillInBlankQuestion6(question, currentQuestion, list)
            break
          case 'FB7':
            FillInBlankQuestion7_v2(question, currentQuestion, list)
            break
          case 'FB8':
            FillInBlankQuestion8(question, currentQuestion, list)
            break
          case 'FB5':
            FillInBlankQuestion5(question, currentQuestion, list)
            break
          case 'LS1':
          case 'LS2':
            LikertScaleQuestion(question, currentQuestion, list)
            break
          case 'MG1':
          case 'MG2':
          case 'MG3':
            MatchingGameQuestion(question, currentQuestion, list)
            break
          case 'SA1':
            ShortAnswerQuestion(question, currentQuestion, list)
            break
          case 'TF1':
          case 'TF2':
            TrueFalseQuestion(question, currentQuestion, list)
            break
          case 'MR1':
          case 'MR2':
            MultiResponseQuestion1(question, currentQuestion, list)
            break
          case 'MR3':
            MultiResponseQuestion3(question, currentQuestion, list)
            break
          case 'SL1':
            SelectFromListQuestion1(question, currentQuestion, list)
            break
          case 'SL2':
            SelectFromListQuestion2(question, currentQuestion, list)
            break
          case 'MIX1':
            MIXQuestion1(question, currentQuestion, list)
            break
          case 'MIX2':
            MIXQuestion2(question, currentQuestion, list)
            break
          case 'TF5':
            TrueFalseQuestion5(question, currentQuestion, list)
            break
          case 'TF4':
            TrueFalseQuestion4(question, currentQuestion, list)
            break
          case 'TF3':
            TrueFalseQuestion3(question, currentQuestion, list)
            break
          case 'MG4':
            MatchingGameQuestion4(question, currentQuestion, list)
            break
          case 'MG5':
            MatchingGameQuestion5(question, currentQuestion, list)
            break
          case 'DL1':
          case 'DL3':
            DrawLine1(question, currentQuestion, list)
            break
          case 'SO1':
            SelectObject1(question, currentQuestion, list)
            break
          case 'DL2':
                DrawLine2(question, currentQuestion, list)
                break
          case 'DS1':
                DrawShape1(question, currentQuestion, list)
                break
          case 'DD2':
                DragDrop2(question, currentQuestion, list)
                break
          case 'MG6':
            MatchingGameQuestion6(question, currentQuestion, list)
            break;
          case 'FC1':
            FillInColourType1(question, currentQuestion, list)
            break
          case 'FC2':
            FillInColourType2(question, currentQuestion, list)
            break
        }
  
        if (excludeType.includes(question.question_type)) {
          currentQuestion += 1
        } else {
          currentQuestion += question.total_question
        }
      })
    })
  
    return list
  }
  return new Promise<TypeResolve>((resolve, reject) => {
    try {
      
      const doc = new Document({
        sections: [
          {
            properties: {
              page: {
                margin: {
                  top: '0.5 in',
                  bottom: '0.5 in',
                  left: '0.5 in',
                  right: '0.5 in',
                },
              },
            },
            children: [
              new Table({
                width: { size: 100, type: WidthType.PERCENTAGE },
                rows: [
                  new TableRow({
                    children: [
                      new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                          size: 50,
                          type: WidthType.PERCENTAGE,
                        },
                        children: [
                          new Paragraph({
                            spacing: {
                              line: 300,
                            },
                            alignment: AlignmentType.CENTER,
                            children: [
                              new TextRun('UBND QUẬN _________'),
                              new TextRun({
                                text: 'PHÒNG GIÁO DỤC VÀ ĐÀO TẠO',
                                break: 1,
                              }),
                              new TextRun({
                                text: 'ĐỀ CHÍNH THỨC',
                                break: 1,
                              }),
                            ],
                          }),
                        ],
                      }),
                      new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                          size: 50,
                          type: WidthType.PERCENTAGE,
                        },
                        columnSpan: 2,
                        children: [
                          new Paragraph({
                            spacing: {
                              line: 300,
                              before: 200,
                              after: 200,
                            },
                            alignment: AlignmentType.CENTER,
                            children: [
                              new TextRun('ĐỀ KIỂM TRA __________'),
                              new TextRun({
                                text: 'Năm học: 20__ - 20__',
                                break: 1,
                              }),
                              new TextRun({
                                text: `Tiếng Anh - ${unitTest.grade_name}`,
                                break: 1,
                              }),
                              new TextRun({
                                text: `Thời gian làm bài: ${unitTest.time} phút`,
                                break: 1,
                              }),
                            ],
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableRow({
                    children: [
                      new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                          size: 50,
                          type: WidthType.PERCENTAGE,
                        },
                        children: [
                          new Paragraph({
                            indent: {
                              left: '0.2 in',
                            },
                            spacing: {
                              line: 300,
                              before: 200,
                              after: 200,
                            },
                            alignment: AlignmentType.START,
                            children: [
                              new TextRun('Trường: ________________________'),
                              new TextRun({
                                text: 'Họ tên: _______________________',
                                break: 1,
                              }),
                              new TextRun({
                                text: 'Lớp: _____________',
                                break: 1,
                              }),
                            ],
                          }),
                        ],
                      }),
                      new TableCell({
                        verticalAlign: VerticalAlign.TOP,
                        width: {
                          size: 12,
                          type: WidthType.PERCENTAGE,
                        },
                        children: [
                          new Paragraph({
                            spacing: {
                              before: 200,
                            },
                            alignment: AlignmentType.CENTER,
                            children: [new TextRun('Điểm')],
                          }),
                        ],
                      }),
                      new TableCell({
                        verticalAlign: VerticalAlign.TOP,
                        width: {
                          size: 38,
                          type: WidthType.PERCENTAGE,
                        },
                        children: [
                          new Paragraph({
                            spacing: {
                              before: 200,
                            },
                            alignment: AlignmentType.CENTER,
                            children: [new TextRun('Nhận xét của giáo viên')],
                          }),
                        ],
                      }),
                    ],
                  }),
                ],
              }),
              new Paragraph(''),
              new Table({
                width: { size: 100, type: WidthType.PERCENTAGE },
                borders: {
                  top: { style: BorderStyle.SINGLE },
                  left: { style: BorderStyle.SINGLE },
                  bottom: { style: BorderStyle.SINGLE },
                  right: { style: BorderStyle.SINGLE },
                  insideHorizontal: { style: BorderStyle.SINGLE },
                  insideVertical: { style: BorderStyle.SINGLE },
                },
                rows: [
                  new TableRow({
                    children: [
                      new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                          size: 4,
                          type: WidthType.PERCENTAGE,
                        },
                        borders: {
                          top: { style: BorderStyle.SINGLE, color: '#ffffff' },
                          right: {
                            style: BorderStyle.SINGLE,
                            color: '#ffffff',
                          },
                          bottom: {
                            style: BorderStyle.SINGLE,
                            color: '#ffffff',
                          },
                          left: { style: BorderStyle.SINGLE, color: '#ffffff' },
                        },
                        children: [
                          new Paragraph({
                            alignment: AlignmentType.CENTER,
                            heading: HeadingLevel.HEADING_2,
                            children: [
                              new TextRun({
                                text: unitTest.name,
                                color: '#188fba',
                                bold: true,
                              }),
                              new TextRun({
                                text: `Time allotted: ${unitTest.time}`,
                                break: 1,
                                color: '#188fba',
                                bold: true,
                              }),
                            ],
                          }),
                        ],
                      }),
                    ],
                  }),
                ],
              }),
              ...renderSection(unitTest),
              new Paragraph({
                spacing: { before: 200 },
                alignment: AlignmentType.CENTER,
                children: [
                  new TextRun({
                    text: '---THE END---',
                    color: '#188fba',
                    bold: true,
                  }),
                ],
              }),
            ],
          },
        ],
      })
      Packer.toBuffer(doc).then((buffer) => {
        fs.writeFileSync(filePath, buffer)
        resolve({ code: 1, filepath: filePath, audioFile: listAudioInfo })
      })
    } catch (err) {
      console.log('createWordExam---', err)
      resolve({ code: 0, message: 'have an error' })
    }
  })
}


const excludeType = ['MR1', 'MR2', 'MR3']

const getArrTextRunFromArrTextStyleObject = (data: any[]) => {
  const arrItem: any = []
  data.forEach((item: any) => {
    arrItem.push(
      new TextRun({
        text: item.text.replaceAll(/\%s\%/g, '______________'),
        bold: item.bold,
        italics: item.italic,
        ...(item.underline
          ? {
              underline: {
                type: UnderlineType.SINGLE,
              },
            }
          : {}),
      }),
    )
  })
  return arrItem
}
function DrawShape1(question: any, index: number, list: any[]) {
  const listAnswer = (question?.answers as string).split('#') ?? []
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after:50
      },
      children: [new TextRun({
        text: `${question.question_description} ${question.total_question> 1 ? `(${question.total_question} questions)`:""}`,
        bold: true,
      })],
    }),
  )
  const lengthAnswer = listAnswer.length
  const tempRowData:any[] = []
  const obj:any[] =[]
  for(let i = 0; i < lengthAnswer; i++){
    const div = Math.floor(i/4);
    if(!obj[div]){
    obj.push([])
    }
    obj[div].push(listAnswer[i])
  }
  let indexAns = -1
  for(let i = 0; i < obj.length; i++){
    const rowData = [
      new TableRow({
        children: obj[i].map((item:string, mIndex:number)=>{
          const listPropertyItem = item.split('*')
          const colorName = COLOR_SHAPE.find((item:{color:string, display:string}) => item?.color === listPropertyItem[2])?.display
          const imageUrl = `public/images/shapes/default-${listPropertyItem[0].startsWith("ex|") ? `ex-${listPropertyItem[1]}-${colorName}`:'location'}.png`
          const imageSize = sizeOfImage(imageUrl)
          const maxWidth = 110
          let imgHeight = 110
          let imgWidth = (imageSize.width * maxWidth) / imageSize.height
          if (imgWidth > maxWidth) {
            imgWidth = maxWidth
            imgHeight = (imageSize.height * maxWidth) / imageSize.width
          }
          return new TableCell({
            children:[
              new Paragraph({
                spacing: { before: 50, after: 50 },
                alignment: AlignmentType.CENTER,
                children: [
                  new ImageRun({
                    data: fs.readFileSync(imageUrl),
                    transformation: {
                      width: imgWidth,
                      height: imgHeight,
                    },
                  }),
                ],
              }),
            ]
          })
        })
      }),
      new TableRow({
        height:{value:500, rule: HeightRule.EXACT},
        children: obj[i].map((item:string, mIndex:number)=>{
          const listPropertyItem = item.split('*')
          const isExample = listPropertyItem[0].startsWith("ex|")
          if(!isExample){
            indexAns++
          }
          return new TableCell({
            verticalAlign: VerticalAlign.TOP,
            children:[
              new Paragraph({
                alignment: AlignmentType.CENTER,
                children:[new TextRun({
                  text:`${isExample ? 'Example' : `${index + indexAns}`}`,
                  bold:isExample,
                  ...(isExample?{underline: {
                    type: UnderlineType.SINGLE,
                  }}:{})
                })]
              }),
            ]
          })
        })
      })
    ]
    tempRowData.push(rowData)
  }
  list.push(
    new Table({
      margins: { top: 200,left:150 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [...tempRowData.reduce((a,b)=>[...a,...b],[])]
    }),
  )
}
function DragDrop2(question: any, index: number, list: any[]) {
  const listCorrect: string[] = question.correct_answers.split('#') ?? []
  const listImage: string[] = question.image.split("#") ?? []
  const indexExample = listCorrect.findIndex((m:string)=> m.startsWith("*"))
 
  if(indexExample !=-1){
    const itemExample = listCorrect[indexExample]
    listCorrect.splice(indexExample, 1)
    listCorrect.splice(0, 0, itemExample)

    const itemImage = listImage[indexExample]
    listImage.splice(indexExample, 1)
    listImage.splice(0, 0, itemImage)
  }

  const temps:any[] =[]
  for(let i = 0; i < listCorrect.length; i++){
    const div = Math.floor(i/4);
    if(!temps[div]){
      temps.push([])
    }
    const isExample = listCorrect[i].startsWith("*");
    temps[div].push({image:listImage[i], text: listCorrect[i],
    isExample:isExample})
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after:50
      },
      children: [new TextRun({
        text: `${index}. ${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`,
        bold: true,
      })],
    }),
  )
  temps.forEach((groupList:any[])=>{
    const totalItem = groupList.length
    const listCellTop:TableCell[] = []
    const listCellBottom:TableCell[] = []
    groupList.forEach((item, indexItem)=> {
      const itemImage:string = item.image ?? ''
      const imageUrlCorrect = `${uploadDir}/${itemImage}`
      const maxWidth = 100
      const imageSizeCorrect = sizeOfImage(imageUrlCorrect)
      let imgHeightCorrrect = 100
      let imgWidthCorrect = (imageSizeCorrect.width * maxWidth) / imageSizeCorrect.height
      if (imgWidthCorrect> maxWidth) {
        imgWidthCorrect = maxWidth
        imgHeightCorrrect = (imageSizeCorrect.height * maxWidth) / imageSizeCorrect.width
      }
      const newCellTop = new TableCell({
         width: { size: 5, type: WidthType.PERCENTAGE },
        verticalAlign:VerticalAlign.BOTTOM,
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        },
        children: [
          new Paragraph({
            alignment: AlignmentType.CENTER,
            spacing: {
              after:150
            },
            children: [
              new ImageRun({
                data: fs.readFileSync(imageUrlCorrect),
                transformation: {
                  width: imgWidthCorrect,
                  height: imgHeightCorrrect,
                },
              }),
            ],
          }),
        ],
      })
      const newCellBottom = new TableCell({
        width: { size: 5, type: WidthType.PERCENTAGE },
       verticalAlign:VerticalAlign.TOP,
       borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
      },
       children: [
         new Paragraph({
           alignment: AlignmentType.CENTER,
           spacing: {
            after:150
          },
           children: [
            new TextRun({
              text:item.isExample?item.text.replace("*",""):"",
            }),
           ],
         }),
       ],
     })

      listCellTop.push(newCellTop)
      listCellBottom.push(newCellBottom)
      if(indexItem < totalItem-1){
        const newCellTempTop = new TableCell({
          width: { size: 1, type: WidthType.PERCENTAGE },
         verticalAlign:VerticalAlign.TOP,
         borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        },
         children: [
           new Paragraph({
            spacing:{after:50},
             alignment: AlignmentType.CENTER,
             text:""
           }),
         ],
        })
        const newCellTempBottom= new TableCell({
          width: { size: 1, type: WidthType.PERCENTAGE },
         verticalAlign:VerticalAlign.TOP,
         borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
        },
         children: [
           new Paragraph({
            spacing:{after:50},
             alignment: AlignmentType.CENTER,
             text:""
           }),
         ],
        })
        listCellTop.push(newCellTempTop)
        listCellBottom.push(newCellTempBottom)
      }
    })
    if(groupList.length < 4){
      while(groupList.length <4){
        groupList.push(null);
        const newCellTempTop = new TableCell({
          width: { size: 1, type: WidthType.PERCENTAGE },
         verticalAlign:VerticalAlign.TOP,
         borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        },
         children: [
           new Paragraph({
            spacing:{after:50},
             alignment: AlignmentType.CENTER,
             text:""
           }),
         ],
        })
        const newCellEmpltyTop = new TableCell({
          width: { size: 5, type: WidthType.PERCENTAGE },
         verticalAlign:VerticalAlign.TOP,
         borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        },
         children: [
           new Paragraph({
            spacing:{after:50},
             alignment: AlignmentType.CENTER,
             text:""
           }),
         ],
        })
        const newCellTempBottom= new TableCell({
          width: { size: 1, type: WidthType.PERCENTAGE },
         verticalAlign:VerticalAlign.TOP,
         borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        },
         children: [
           new Paragraph({
            spacing:{after:50},
             alignment: AlignmentType.CENTER,
             text:""
           }),
         ],
        })
        const newCellEmpltyBottom = new TableCell({
          width: { size: 5, type: WidthType.PERCENTAGE },
         verticalAlign:VerticalAlign.TOP,
         borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        },
         children: [
           new Paragraph({
            spacing:{after:50},
             alignment: AlignmentType.CENTER,
             text:""
           }),
         ],
        })
        listCellTop.push(newCellTempTop)
        listCellBottom.push(newCellTempBottom)
        listCellTop.push(newCellEmpltyTop)
        listCellBottom.push(newCellEmpltyBottom)
      }
    }
    list.push(
      new Table({
        margins: { top: 200,left:150 },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: [
              ...listCellTop
            ],
          }),
          new TableRow({
            children: [
              ...listCellBottom
            ],
          }),
        ],
      }),
    )
  })
}
function DrawLine2(question: any, index: number, list: any[]) {
  const listCorrect = question.correct_answers.split('#')||[]
   const listImage = question.image.split("*")||[]
   const image = listImage[1]
  const indexExample = listCorrect.findIndex((m:string)=> m.startsWith("ex|"))
  let itemExample =""
  if(indexExample !=-1){
    itemExample = listCorrect[indexExample]
    listCorrect.splice(indexExample,1)
    listCorrect.splice(0,0,itemExample)
  }
  let divide = 5;
  const lenAnswer = listCorrect.length;
  if(lenAnswer <=4){
    divide = lenAnswer
  }else if(lenAnswer <=8){
    divide = 4
  }
  const listItemTop = []
  const listItemBottom = []

  for(let i=0; i< lenAnswer; i++){
    if(i<divide){
      listItemTop.push(listCorrect[i])
    }else{
      listItemBottom.push(listCorrect[i])
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after:50
      },
      children: [new TextRun({
        text: `${index}. ${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`,
        bold: true,
      })],
    }),
  )
  if(listItemTop.length>0){
    list.push(
      new Table({
        margins: { top: 200,left:150 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: [
              ...listItemTop.map((item: any, itemIndex: number) => {
                const itemName = item.split("*")||[]
                const imageUrlCorrect = `${uploadDir}/${itemName[2]}`
                const maxWidth = 60
                const imageSizeCorrect = sizeOfImage(imageUrlCorrect)
                let imgHeightCorrrect = 60
                let imgWidthCorrect = (imageSizeCorrect.width * maxWidth) / imageSizeCorrect.height
                if (imgWidthCorrect> maxWidth) {
                  imgWidthCorrect = maxWidth
                  imgHeightCorrrect = (imageSizeCorrect.height * maxWidth) / imageSizeCorrect.width
                }
                return new TableCell({
                  width: { size: 1, type: WidthType.PERCENTAGE },
                  verticalAlign:VerticalAlign.BOTTOM,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.CENTER,
                      children: [
                        new ImageRun({
                          data: fs.readFileSync(imageUrlCorrect),
                          transformation: {
                            width: imgWidthCorrect,
                            height: imgHeightCorrrect,
                          },
                        }),
                      ],
                    }),
                  ],
                })
              }),
            ],
          }),
        ],
      }),
    )
  }
  if(image){
    const imageUrl = `${uploadDir}/${image}`
    const imageSize = sizeOfImage(imageUrl)
    const imgHeight = imageSize.height
    const imgWidth = imageSize.width
    list.push(
      new Paragraph({
        spacing: { before: 50, after: 50 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(imageUrl),
            transformation: {
              width: imgWidth,
              height: imgHeight,
            },
          }),
        ],
      }),
    )
  }
  if(listItemBottom.length>0){
    list.push(
      new Table({
        margins: { top: 50 , left:150},
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: [
              ...listItemBottom.map((item: any, itemIndex: number) => {
                const itemName = item.split("*")||[]
                const imageUrlCorrect = `${uploadDir}/${itemName[2]}`
                const maxWidth = 60
                const imageSizeCorrect = sizeOfImage(imageUrlCorrect)
                let imgHeightCorrrect = 60
                let imgWidthCorrect = (imageSizeCorrect.width * maxWidth) / imageSizeCorrect.height
                if (imgWidthCorrect> maxWidth) {
                  imgWidthCorrect = maxWidth
                  imgHeightCorrrect = (imageSizeCorrect.height * maxWidth) / imageSizeCorrect.width
                }
                return new TableCell({
                  width: { size: 1, type: WidthType.PERCENTAGE },
                  verticalAlign:VerticalAlign.TOP,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.CENTER,
                      children: [
                        new ImageRun({
                          data: fs.readFileSync(imageUrlCorrect),
                          transformation: {
                            width: imgWidthCorrect,
                            height: imgHeightCorrrect,
                          },
                        }),
                      ],
                    }),
                  ],
                })
              }),
            ],
          }),
        ],
      }),
    )
  }
}
function DrawLine1(question: any, index: number, list: any[]) {
  const listCorrect = question.correct_answers.split('#')||[]
   const listImage = question.image.split("*")||[]
   const image = listImage[1]
  const indexExample = listCorrect.findIndex((m:string)=> m.startsWith("ex|"))
  let itemExample =""
  if(indexExample !=-1){
    itemExample = listCorrect[indexExample]
    listCorrect.splice(indexExample,1)
    listCorrect.splice(0,0,itemExample)
  }
  let divide = 5;
  const lenAnswer = listCorrect.length;
  if(lenAnswer <=4){
    divide = lenAnswer
  }else if(lenAnswer <=8){
    divide = 4
  }
  const listItemTop = []
  const listItemBottom = []

  for(let i=0; i< lenAnswer; i++){
    if(i<divide){
      listItemTop.push(listCorrect[i])
    }else{
      listItemBottom.push(listCorrect[i])
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after:50
      },
      children: [new TextRun({
        text: `${index}. ${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`,
        bold: true,
      })],
    }),
  )
  if(listItemTop.length>0){
    list.push(
      new Table({
        margins: { top: 200,left:150 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: [
              ...listItemTop.map((item: any, itemIndex: number) => {
                const itemName = item.split("*")||[]
                return new TableCell({
                  width: { size: 1, type: WidthType.PERCENTAGE },
                  verticalAlign:VerticalAlign.BOTTOM,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.CENTER,
                      children: [
                        new TextRun(
                          `${itemName[2]}`,
                        ),
                      ],
                    }),
                  ],
                })
              }),
            ],
          }),
        ],
      }),
    )
  }
  if(image){
    const imageUrl = `${uploadDir}/${image}`
    const imageSize = sizeOfImage(imageUrl)
    const imgHeight = imageSize.height
    const imgWidth = imageSize.width
    list.push(
      new Paragraph({
        spacing: { before: 50, after: 50 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(imageUrl),
            transformation: {
              width: imgWidth,
              height: imgHeight,
            },
          }),
        ],
      }),
    )
  }
  if(listItemBottom.length>0){
    list.push(
      new Table({
        margins: { top: 50 , left:150},
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: [
              ...listItemBottom.map((item: any, itemIndex: number) => {
                const itemName = item.split("*")||[]
                return new TableCell({
                  width: { size: 1, type: WidthType.PERCENTAGE },
                  verticalAlign:VerticalAlign.TOP,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.CENTER,
                      children: [
                        new TextRun(
                          `${itemName[2]}`,
                        ),
                      ],
                    }),
                  ],
                })
              }),
            ],
          }),
        ],
      }),
    )
  }
}
function FillInColourType1(question: any, index: number, list: any[]) {
  const listImage = question.image.split("*")||[]
  const image = listImage[1]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after:50
      },
      children: [new TextRun({
        text: `${index}. ${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`,
        bold: true,
      })],
    }),
  )
  if(image){
    const imageUrl = `${uploadDir}/${image}`
    const imageSize = sizeOfImage(imageUrl)
    const imgHeight = imageSize.height
    const imgWidth = imageSize.width
    list.push(
      new Paragraph({
        spacing: { before: 50, after: 50 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(imageUrl),
            transformation: {
              width: imgWidth,
              height: imgHeight,
            },
          }),
        ],
      }),
    )
  }
}
function FillInColourType2(question: any, index: number, list: any[]) {
  const listImage = question.image.split("*")||[]
  const image = listImage[1]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after:50
      },
      children: [new TextRun({
        text: `${index}. ${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`,
        bold: true,
      })],
    }),
  )
  if(image){
    const imageUrl = `${uploadDir}/${image}`
    const imageSize = sizeOfImage(imageUrl)
    const imgHeight = imageSize.height
    const imgWidth = imageSize.width
    list.push(
      new Paragraph({
        spacing: { before: 50, after: 50 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(imageUrl),
            transformation: {
              width: imgWidth,
              height: imgHeight,
            },
          }),
        ],
      }),
    )
  }
}
function SelectObject1(question: any, index: number, list: any[]) {
  const listImage = question.image.split("*")||[]
  const image = listImage[1]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after:50
      },
      children: [new TextRun({
        text: `${index}. ${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`,
        bold: true,
      })],
    }),
  )
  if(image){
    const imageUrl = `${uploadDir}/${image}`
    const imageSize = sizeOfImage(imageUrl)
    const imgHeight = imageSize.height
    const imgWidth = imageSize.width
    list.push(
      new Paragraph({
        spacing: { before: 50, after: 50 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(imageUrl),
            transformation: {
              width: imgWidth,
              height: imgHeight,
            },
          }),
        ],
      }),
    )
  }
}
function MatchingGameQuestion4(question: any, index: number, list: any[]) {
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
    const correctList: string[] = question.correct_answers.split('#')

  const hasExample = listAnswer[2] && listAnswer[2][0] =="ex"
  const example = {
    left:listAnswer[0][0],
    right:correctList[0]
  }
  let firstCellWidth = 0;
  if(hasExample){
    const indexRight = listAnswer[1].indexOf(correctList[0]);
    if (indexRight > -1) {
      listAnswer[1].splice(indexRight, 1); 
    }
    listAnswer[0].shift();
    correctList.shift();
    firstCellWidth = 6
  }
  const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length)
  const qdArr = `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n')
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: qdArr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
 
  const image = question.image
  if(image){
    const imageUrl = `${uploadDir}/${image}`
    const imageSize = sizeOfImage(imageUrl)
    const maxWidth = 450
    let imgHeight = 136
    let imgWidth = (imageSize.width * 136) / imageSize.height
    if (imgWidth > maxWidth) {
      imgWidth = 450
      imgHeight = (imageSize.height * 450) / imageSize.width
    }
    list.push(
      new Paragraph({
        spacing: { before: 200, after: 200 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(imageUrl),
            transformation: {
              width: imgWidth,
              height: imgHeight,
            },
          }),
        ],
      }),
    )
    if(question.audio_script){
      const audioScriptArr = question.audio_script.split('\n')
      list.push(
        new Paragraph({
          spacing: { before: 0, after: 200 },
          alignment: AlignmentType.CENTER,
          children: audioScriptArr.map(
            (item: string, aIndex: number) =>
              new TextRun({
                text: item,
                bold: true,
                break: aIndex > 0 ? 1 : 0,
              }),
          ),
        })
      )
    }
  }
  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [...(hasExample?[new TableRow({
        children: [
          new TableCell({
            width: { size: 3 + firstCellWidth, type: WidthType.PERCENTAGE },
           children: [
             new Paragraph(
               {
                 children:[new TextRun({
                   text:`Example: `,
                   bold:true,
                   underline: {
                    type: UnderlineType.SINGLE,
                  },
                 })]
               }
             ),
           ],
         }),
          new TableCell({
             width: { size: 32.5 - (firstCellWidth/2), type: WidthType.PERCENTAGE },
            children: [
              new Paragraph(
                  {
                    children: [
                      ...getArrTextRunFromArrTextStyleObject(formatTextStyleObject(example.left))
                    ]
                  }
              )
            ],
          }),
          new TableCell({
             width: { size: 24, type: WidthType.PERCENTAGE },
             margins:{
              bottom:10
             },
            children: [
              new Paragraph(
                {
                  alignment: AlignmentType.CENTER,
                  children:[
                    new TextRun({
                      text:"●  ",
                    }),
                    // new TextRun({
                    //   text:"-------------------------------------",
                    //   color:"#188fba",
                    // }),
                    new ImageRun({
                      data: fs.readFileSync('public/images/icons/line-export.png'),
                      transformation: {
                        width: 125,
                        height: 4.5,
                      },
                    }),
                    new TextRun({
                      text:"  ●",
                    }),
                  ]
                }
              ),
            ],
          }),
          new TableCell({
            width: { size: 40.5 - (firstCellWidth/2), type: WidthType.PERCENTAGE },
            children: [
              new Paragraph(
                  {
                    children: [
                      new TextRun(`A. `),
                      ...getArrTextRunFromArrTextStyleObject(formatTextStyleObject(example.right))
                    ]
                  }
              )
            ],
          }),
        ],
      })]:[]),...Array.from(
        { length: maxLength },
        (_, _index) =>
          new TableRow({
            children: [
              new TableCell({
                width: { size: 3 + firstCellWidth , type: WidthType.PERCENTAGE },
               children: [
                 new Paragraph(
                   {
                     children:[new TextRun({
                       text:`${listAnswer[0][_index]?(index + _index) + ". ":""}`
                     })]
                   }
                 ),
               ],
             }),
              new TableCell({
                 width: { size: 32.5 - (firstCellWidth/2), type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph(
                      {
                        children: listAnswer[0][_index] ? [
                          ...getArrTextRunFromArrTextStyleObject(formatTextStyleObject(listAnswer[0][_index])),
                        ] : [
                          new TextRun('')
                        ],
                      }
                  ),
                ],
              }),
              // new TableCell({
              //    width: { size: 35, type: WidthType.PERCENTAGE },
              //   children: [
              //     new Paragraph(
              //       listAnswer[0][_index]
              //         ? `${index + _index}.${(index+ _index)>=10?space.substring(2):space} ${listAnswer[0][_index]}`
              //         : '',
              //     ),
              //   ],
              // }),
              new TableCell({
                 width: { size: 24, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph(""),
                ],
              }),
              new TableCell({
                width: { size: 40.5 - (firstCellWidth/2), type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph(
                      {
                        children: listAnswer[1][_index] ? [
                          new TextRun(`${String.fromCharCode((hasExample ? 66 : 65) + _index)}. `),
                          ...getArrTextRunFromArrTextStyleObject(formatTextStyleObject(listAnswer[1][_index]))
                        ] : [
                          new TextRun("")
                        ]
                      }
                  )
                ],
              }),
            ],
          }),
      )],
    }),
  )

  return list
}
function MatchingGameQuestion5(question: any, index: number, list: any[]) {
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const correctList: string[] = question.correct_answers.split('#') ?? []
  const imageList :string[] = question.image.split('#') ?? []
  const hasExample = listAnswer[2] && listAnswer[2][0] =="ex"
  const example = {
    left:listAnswer[0][0],
    right:correctList[0],
    image:imageList[0]
  }
  let firstCellWidth = 0;
  let imgWidthEx = 136
  const maxHeight = 250
  let imgHeightEx = 250
  let imageUrlEx = ''
  if(hasExample){
    imageUrlEx = `${uploadDir}/${example.image}`
    const imageSize = sizeOfImage(imageUrlEx)
    imgHeightEx = (imageSize.height * 136) / imageSize.width
    if (imgHeightEx > maxHeight) {
      imgHeightEx = 250
      imgWidthEx = (imageSize.width * 250) / imageSize.height
    }
    const indexRight = listAnswer[1].indexOf(correctList[0]);
    if (indexRight > -1) {
      listAnswer[1].splice(indexRight, 1); 
    }
    listAnswer[0].shift();
    correctList.shift();
    imageList.shift()
    firstCellWidth = 6
  }
  const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length)
  const qdArr = `${question.question_description} ${question.total_question > 1 ? `(${question.total_question} questions)`:""}`.split('\n')
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after:150,
      },
      children: qdArr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
 
  
  
  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [...(hasExample ? 
        [new TableRow({
        children: [
          new TableCell({
            width: { size: 3 + firstCellWidth, type: WidthType.PERCENTAGE },
           children: [
             new Paragraph(
               {
                 children:[new TextRun({
                   text:`Example: `,
                   bold:true,
                   underline: {
                    type: UnderlineType.SINGLE,
                  },
                 })]
               }
             ),
           ],
         }),
          new TableCell({
            width:{size: 32.5 - (firstCellWidth/2), type: WidthType.PERCENTAGE},
            children:[
              new Paragraph({
                spacing: { before: 200, after: 200 },
                alignment: AlignmentType.CENTER,
                children: [
                  new ImageRun({
                    data: fs.readFileSync(imageUrlEx),
                    transformation: {
                      width: imgWidthEx,
                      height: imgHeightEx,
                    },
                  }),
              ],
              }),
            ]
          }),
          new TableCell({
            width: { size: 12, type: WidthType.PERCENTAGE },
            verticalAlign: VerticalAlign.CENTER,
            children: [
              new Paragraph(
                {
                  alignment: AlignmentType.CENTER,
                  children:[new TextRun({
                    text:`_ ${example.left}`
                  })]
                }
              ),
            ],
          }),
          new TableCell({
             width: { size: 30, type: WidthType.PERCENTAGE },
             verticalAlign: VerticalAlign.CENTER,
              children: [
              new Paragraph(
                {
                  alignment: AlignmentType.LEFT,
                  children:[
                    new TextRun({
                      text:"●     ",
                    }),
                    new ImageRun({
                      data: fs.readFileSync('public/images/icons/line-export.png'),
                      transformation: {
                        width: 170,
                        height: 4.5,
                      },
                    }),
                  ]
                }
              ),
            ],
          }),
          new TableCell({
            width: { size: 22.5 - (firstCellWidth/2), type: WidthType.PERCENTAGE },
            verticalAlign: VerticalAlign.CENTER,
            children: [
              new Paragraph({
                alignment: AlignmentType.LEFT,
                children:[
                  new TextRun({text:"●    "}),
                  new TextRun({text:`A. ${example.right}`})
                ]
              }),
            ],
          }),
        ],
      })] : []),...Array.from(
        { length: maxLength },
        (_, _index) =>{
          const imageAns:string = imageList[_index] ?? ''
          let imageUrl = ''
          const maxHeight = 250
          let imgWidth = 136
          let imgHeight = 250
          if(imageAns.length > 0){
            imageUrl = `${uploadDir}/${imageAns}`
            const imageSize = sizeOfImage(imageUrl)
            imgHeight = (imageSize.height * 136) / imageSize.width
            if (imgHeight > maxHeight) {
              imgHeight = 250
              imgWidth = (imageSize.width * 250) / imageSize.height
            }
          }
          
          return new TableRow({
            height: !listAnswer[0][_index]  && {value:2000, rule: HeightRule.EXACT},
            children: [
              new TableCell({
                width: { size: 3 + firstCellWidth , type: WidthType.PERCENTAGE },
               children: [
                 new Paragraph(
                   {
                     children:[new TextRun({
                       text:`${listAnswer[0][_index] ? (index + _index) + ". ":""}`
                     })]
                   }
                 ),
               ],
             }),
             new TableCell({
              width:{size: 32.5 - (firstCellWidth/2), type: WidthType.PERCENTAGE},
              children: imageUrl.length > 0 ? [
                new Paragraph({
                  spacing: { before: 200, after: 200 },
                  alignment: AlignmentType.CENTER,
                  children: [
                    new ImageRun({
                      data: fs.readFileSync(imageUrl),
                      transformation: {
                        width: imgWidth,
                        height: imgHeight,
                      },
                    }),
                ],
                }),
              ] : []
            }),
              new TableCell({
                 width: { size: 12, type: WidthType.PERCENTAGE },
                 verticalAlign: VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    alignment: AlignmentType.CENTER,
                    text:listAnswer[0][_index] ? `_ ${listAnswer[0][_index]}` : ''
                  }),
                ],
              }),
              
              new TableCell({
                 width: { size: 30, type: WidthType.PERCENTAGE },
                 verticalAlign: VerticalAlign.CENTER,
                children: [
                  new Paragraph(
                    {
                      alignment: AlignmentType.LEFT,
                      children:[
                        new TextRun({
                          text:listAnswer[0][_index] ? "● " : "",
                        }),
                      ]
                    }
                  ),
                ],
              }),
              new TableCell({
                width: { size: 22.5 - (firstCellWidth/2), type: WidthType.PERCENTAGE },
                verticalAlign: VerticalAlign.CENTER,
                children: [
                  new Paragraph({
                    alignment: AlignmentType.LEFT,
                    children:[
                      new TextRun({text:"●    "}),
                      new TextRun({
                        text:listAnswer[1][_index]
                        ? `${String.fromCharCode((hasExample?66:65) + _index)}. ${
                            listAnswer[1][_index]
                          }`
                        : '',
                      })
                    ]
                  }),
                ],
              }),
            ],
          })},
      )],
    }),
  )

  return list
}
function MatchingGameQuestion6(question: any, index: number, _paragraph: any[]){
  const answerGroup: string[][] = question.answers.split('#')
    .map((value: string) => value.split('*')) ?? [[],[],[]]  
  const maxLength: number = Math.max(answerGroup[0].length, answerGroup[1].length)
  const maxExample =  answerGroup[2] ? answerGroup[2].filter(item => item.startsWith('ex')).length : 0
  const pathUpload = `${uploadDir}/`

  const duplicateSameTable = ( firstData: string[], 
      secondData: string[], startLoop: number, endLoop: number, _isQuestion: boolean) =>{

    for( let i = startLoop; i < endLoop; i++ ){
      const leftImageURL = firstData[i] ? pathUpload +  firstData[i] : ''
      const leftSize = firstData[i] ? sizeOfImage(leftImageURL) : null
      const rightImageURL = secondData[i] ? pathUpload +  secondData[i] : ''
      const rightSize = sizeOfImage(rightImageURL)
      const _setTextOrNumbering = _isQuestion && firstData[i] 
        ? new TextRun({ text: `${(index + i - maxExample)}.` })
        : ( i == 0 ? new TextRun({
            text: 'Example',
            bold: true,
            underline: { type: UnderlineType.SINGLE}
          }) : new TextRun({ text: '' }) )

      _paragraph.push(new Table({
        margins: { top: 200 },
        alignment: AlignmentType.END,
        width: {size: 100, type: WidthType.PERCENTAGE},
        borders: {
          top: { size: 1, style: BorderStyle.SINGLE, color: '#ffffff' },
          left: { size: 1, style: BorderStyle.SINGLE, color: '#ffffff' }, 
          right: { size: 1, style: BorderStyle.SINGLE, color: '#ffffff' }, 
          bottom: { size: 1, style: BorderStyle.SINGLE, color: '#ffffff' },
          insideVertical: { size: 1, style: BorderStyle.SINGLE, color: '#ffffff' },
          insideHorizontal: { size: 1, style: BorderStyle.SINGLE, color: '#ffffff' } },
        rows: [new TableRow({
          children: [
            new TableCell({
              width: { size: 9, type: WidthType.PERCENTAGE },
              verticalAlign: VerticalAlign.TOP,
              children: [ new Paragraph({
                spacing: { after: 10 },
                children: [ _setTextOrNumbering ]
              })]
            }),
            new TableCell({
              width: { size: 26, type: WidthType.PERCENTAGE },
              verticalAlign: VerticalAlign.CENTER,
              children: [
                new Paragraph({
                  spacing: { before: 200, after: 200 },
                  alignment: AlignmentType.CENTER,
                  children: [
                    leftImageURL ? new ImageRun({
                      data: fs.readFileSync(leftImageURL),
                      transformation:{
                        width: 96,
                        height: ( leftSize.height * 96 ) / leftSize.width
                      }
                    }) : new TextRun('')]
                })]
            }),
            new TableCell({
              width: { size: 39 , type: WidthType.PERCENTAGE },
              verticalAlign: VerticalAlign.CENTER,
              children: [ new Paragraph({
                alignment: AlignmentType.LEFT,
                children: [ 
                  new TextRun({ text: `${ firstData[i] ? '●    ' : ''}` }),
                  _isQuestion ? new TextRun({ text: '' }) : new ImageRun({
                    data: fs.readFileSync('public/images/icons/line-export.png'),
                    transformation: {
                      width: 240,
                      height: 4.5, 
                }})]
            })]}),
            new TableCell({
              width: { size: 3, type: WidthType.PERCENTAGE },
              verticalAlign: VerticalAlign.CENTER,
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  children:[
                    new TextRun({ text: `${ secondData[i] ? '●' : ''}` })]
                })]
            }),
            new TableCell({
              width: { size: 3, type: WidthType.PERCENTAGE },
              verticalAlign: VerticalAlign.CENTER,
              children: [
                new Paragraph({
                  alignment: AlignmentType.CENTER,
                  children:[
                    new TextRun({
                      text: `${String.fromCharCode(65 + i).toUpperCase()+'.'}`
                    })]
                })]
            }),
            new TableCell({
              width: { size: 26, type: WidthType.PERCENTAGE },
              verticalAlign: VerticalAlign.CENTER,
              children: [
                new Paragraph({
                  spacing: { before: 200, after: 200 },
                  alignment: AlignmentType.CENTER,
                  children: [
                    rightImageURL ? new ImageRun({
                      data: fs.readFileSync(rightImageURL),
                      transformation:{
                        width: 96,
                        height: ( rightSize.height * 96 ) / rightSize.width
                      }
                    }) : new TextRun(''),]
              })]
            })
          ]
        })]
      }))
    }
  }

  const _tailQuestion = answerGroup[0].length - maxExample > 1 ? 'questions' : 'question'
  const _countQuestion = `(${answerGroup[0].length - maxExample} ${_tailQuestion})`

  _paragraph.push(new Paragraph({
    spacing: { 
      before: 200,
      after: question.question_text ? 200 : 0},
    children: [new TextRun({ text: `${question.question_description} ${_countQuestion}`, bold: true })]
  }))
  
  if( maxExample > 0 ){
    duplicateSameTable( answerGroup[0], answerGroup[1], 0, maxExample, false)
  }

  duplicateSameTable( answerGroup[0], answerGroup[1], maxExample, maxLength, true)

  return _paragraph
}
//mc1 multi question
function MultiChoiceQuestion1_v2(question: any, index: number, list: any[]) {
  const listQuestionText:string[] = question.question_text.split('#') ?? []
  const listAnswer:string[] = question.answers.split('#') ?? []
  const listCorrectAnswer:string[] = question.correct_answers.split('#') ?? []
  const listQuestionTextExample:string[] = []
  const listQuestionTextQuestions:string[] = []
  const listAnswerExample:string[] = []
  const listAnswerQuestions: string[] = []
  const listCorrectExample:string[] = []
  for(let i = 0; i< listQuestionText.length; i++){
    const item:string = listQuestionText[i]
    if(item.startsWith('*')){
      listQuestionTextExample.push(item.substring(1))
      listAnswerExample.push(listAnswer[i])
      listCorrectExample.push(listCorrectAnswer[i])
    }else{
      listQuestionTextQuestions.push(item)
      listAnswerQuestions.push(listAnswer[i])
    }
  }
  const qdArr = `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n')
  const qdParagraphMC = qdArr.map((item: string, mIndex: number) => {
    return new Paragraph({
      spacing: {
        before: 200,
      },
      children: [new TextRun({ text: item, bold: true })],
    })
  })
  list.push(...qdParagraphMC)
  if(listQuestionTextExample.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    listQuestionTextExample.map((questionText:string, indexItem:number)=>{
      const answers = listAnswerExample[indexItem]
      ?.split('*')
      .map((answer: string) => formatTextStyleObject(answer))
      
    // write index if multiple question
    const hasQuestionText = questionText.replaceAll('*', '').trim().length > 0
    if (hasQuestionText) {
      const questionTextRun = new Paragraph({
        spacing:{before:150},
        indent: { left: '0.2 in' },
        children: RenderTextNewLineAndFontStyle(questionText.replaceAll(/\%s\%/g,space),space)
      })  
      list.push(questionTextRun)
    }

    list.push(
      new Table({
        margins: { top: 200 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: [
              ...answers.map((answer: any, answerIndex: number) => {
                return new TableCell({
                  width: { size: 1, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      indent: { left: '0.15 in' },
                      children: [
                        new TextRun({
                          text: `${String.fromCharCode(65 + answerIndex)}. `,
                          ...(listCorrectExample[indexItem] ===
                          convertTextCell(answer)
                            ? {
                                underline: {
                                  type: UnderlineType.SINGLE,
                                  color: trueColor,
                                },
                                color: trueColor,
                              }
                            : {}),
                        }),
                        ...getArrTextRunFromArrTextStyleObject(answer),
                      ],
                    }),
                  ],
                })
              }),
            ],
          }),
        ],
      }),
    )
    })
  }
  if(listQuestionTextQuestions.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: listQuestionTextQuestions.length > 1 ? 'Questions:' : 'Question:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    listQuestionTextQuestions.map((questionText: string, indexItem: number) => {
      //get object style arr
      const answers = listAnswerQuestions[indexItem]
        ?.split('*')
        .map((answer: string) => formatTextStyleObject(answer))
      // write index if multiple question
      const hasQuestionText = questionText && questionText.trim() != ''
      if (hasQuestionText) {
 
        const questionTextRun = new Paragraph({
          spacing:{before:150},
          indent: { left: '0.2 in' },
          children: RenderTextNewLineAndFontStyle(`${index + indexItem}. ${questionText.replaceAll(/\%s\%/g,space)}`,space)
        })   
        list.push(questionTextRun)
      }
  
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 100, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  return new TableCell({
                    width: { size: 1, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        indent: { left: '0.15 in' },
                        children: [
                          answerIndex == 0
                            ? !hasQuestionText
                              ? new TextRun({
                                  text: `${indexItem + index}. `,
                                })
                              : new TextRun({ text: `    ` })
                            : {},
                          new TextRun(
                            `${String.fromCharCode(65 + answerIndex)}. `,
                          ),
                          ...getArrTextRunFromArrTextStyleObject(answer),
                        ],
                      }),
                    ],
                  })
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  return list
}
//mc2 multi question
function MultiChoiceQuestion2_v2(question: any, index: number, list: any[]) {
  type dataViewModel = {
    questions: string[]
    answers: string[]
    corrects: string[]
  }
  const listAnswer: string[] = question.answers.split('#')
  const listQuestionText: string[] = question.question_text.split('#')
  const listCorrectAnswer: string[] = question.correct_answers.split('#')
  const qdArr = question.parent_question_description? `${question.parent_question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n'): []

  const image = question.image
  const imageUrl = `${uploadDir}/${image}`
  const imageSize = sizeOfImage(imageUrl)

  const example: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
  }
  const mainQuestion: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
  }
  for (let i = 0; i < listQuestionText.length; i++) {
    if (listQuestionText[i]?.startsWith('*')) {
      example.questions.push(listQuestionText[i].substring(1))
      example.answers.push(listAnswer[i])
      example.corrects.push(listCorrectAnswer[i])
    } else {
      mainQuestion.questions.push(listQuestionText[i])
      mainQuestion.answers.push(listAnswer[i])
      mainQuestion.corrects.push(listCorrectAnswer[i])
    }
  }
  const qdParagraphMC = qdArr.map((item: string, mIndex: number) => {
    return new Paragraph({
      spacing: {
        before: 200,
      },
      children: [new TextRun({ text: item, bold: true })],
    })
  })
  list.push(...qdParagraphMC)
  const maxWidth = 450
  let imgHeight = 136
  let imgWidth = (imageSize.width * 136) / imageSize.height
  if (imgWidth > maxWidth) {
    imgWidth = 450
    imgHeight = (imageSize.height * 450) / imageSize.width
  }
  list.push(
    new Paragraph({
      spacing: { before: 200, after: 200 },
      alignment: AlignmentType.CENTER,
      children: [
        new ImageRun({
          data: fs.readFileSync(imageUrl),
          transformation: {
            width: imgWidth,
            height: imgHeight,
          },
        }),
      ],
    }),
  )
  if (example.questions.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.questions.forEach((questionText: string, indexItem: number) => {
      const answers = example.answers[indexItem]
        ?.split('*')
        .map((answer: string) => formatTextStyleObject(answer))
      const questionTextRun = new Paragraph({
        spacing:{before:150},
        indent: { left: '0.2 in' },
        children: RenderTextNewLineAndFontStyle(questionText.replaceAll(/\%s\%/g,space),space)
      })   
      list.push(questionTextRun)
      list.push(
        new Table({
          margins: { top: 150 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 96, type: WidthType.PERCENTAGE },
          alignment: AlignmentType.END,
          rows: [
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  return new TableCell({
                    width: { size: 1, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        children: [
                          new TextRun({
                            text: `${String.fromCharCode(65 + answerIndex)}. `,
                            ...(example.corrects[indexItem] ===
                            convertTextCell(answer)
                              ? {
                                  underline: {
                                    type: UnderlineType.SINGLE,
                                    color: trueColor,
                                  },
                                  color: trueColor,
                                }
                              : {}),
                          }),
                          ...getArrTextRunFromArrTextStyleObject(answer),
                        ],
                      }),
                    ],
                  })
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  if (mainQuestion.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: mainQuestion.questions.length > 1 ? 'Questions:' : 'Question:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    mainQuestion.questions.forEach(
      (questionText: string, indexItem: number) => {
        const answers = mainQuestion.answers[indexItem]
          ?.split('*')
          .map((answer: string) => formatTextStyleObject(answer))
        const questionTextRun = new Paragraph({
          spacing:{before:150},
          indent: { left: '0.2 in' },
          children: RenderTextNewLineAndFontStyle(`${index + indexItem}. ${questionText.replaceAll(/\%s\%/g,space)}`,space)
        })   
        list.push(questionTextRun)
        list.push(
          new Table({
            margins: { top: 150 },
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            width: { size: 96, type: WidthType.PERCENTAGE },
            alignment: AlignmentType.END,
            rows: [
              new TableRow({
                children: [
                  ...answers.map((answer: any, answerIndex: number) => {
                    return new TableCell({
                      width: { size: 1, type: WidthType.PERCENTAGE },
                      children: [
                        new Paragraph({
                          children: [
                            new TextRun(
                              `${String.fromCharCode(65 + answerIndex)}. `,
                            ),
                            ...getArrTextRunFromArrTextStyleObject(answer),
                          ],
                        }),
                      ],
                    })
                  }),
                ],
              }),
            ],
          }),
        )
      },
    )
  }

  return list
}
function MultiChoiceQuestion4(question: any, index: number, list: any[]) {
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: [
        new TextRun({
          text: `${question.question_description}`,
          bold: true,
        }),
      ],
    }),
  )
  const answers = question.answers
    ?.split('*')
    .map((answer: string) => formatTextStyleObject(answer))

  const columnWidth = 100.0 / answers.length

  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          children: [
            new TableCell({
              width: { size: 4, type: WidthType.PERCENTAGE },
              verticalAlign: VerticalAlign.CENTER,
              children: [
                new Paragraph({
                  alignment: AlignmentType.START,
                  children: [
                    new TextRun({
                      text: `${index}. `,
                    }),
                  ],
                }),
              ],
            }),
            ...answers.map((answer: any, answerIndex: number) => {
              return new TableCell({
                width: { size: columnWidth, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph({
                    //  ...(hasQuestionText)? {}:{indent: { left: '0.11 in' }},
                    indent: { left: '0.2 in' },
                    children: [
                      new TextRun(`${String.fromCharCode(65 + answerIndex)}. `),
                      ...getArrTextRunFromArrTextStyleObject(answer),
                    ],
                  }),
                ],
              })
            }),
          ],
        }),
      ],
    }),
  )
  return list
}
function MultiChoiceQuestion1(question: any, index: number, list: any[]) {
  const answers = question.answers.split('*')
  const columnWidth = 100.0 / answers.length
  const isMC4 = question.question_type === 'MC4'

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: [
        new TextRun({
          text: `${question.question_description}`,
          bold: true,
        }),
      ],
    }),
  )
  if (question.question_text) {
    list.push(
      new Paragraph({
        spacing: {
          before: 150,
        },
        indent: {
          left: '0.2 in',
        },
        children: [
          new TextRun({
            text: `${question.question_text.replace(
              /\%s\%/g,
              '______________',
            )}`,
          }),
        ],
      }),
    )
  }
  if (question.image) {
    const imageSize =  sizeOfImage(`${uploadDir}/${question.image}`)
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: (imageSize.width * 136) / imageSize.height,
              height: 136,
            },
          }),
        ],
      }),
    )
  }
  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          children: [
            new TableCell({
              width: { size: 4, type: WidthType.PERCENTAGE },
              verticalAlign: VerticalAlign.CENTER,
              children: [
                new Paragraph({
                  alignment: AlignmentType.START,
                  children: [
                    new TextRun({
                      text: `${index}. `,
                    }),
                  ],
                }),
              ],
            }),
            ...answers.map(
              (answer: string, answerIndex: number) =>
                new TableCell({
                  width: { size: columnWidth, type: WidthType.PERCENTAGE },
                  children: [
                    !isMC4
                      ? new Paragraph({
                          indent: { left: '0.2 in' },
                          text: `${String.fromCharCode(
                            65 + answerIndex,
                          )}. ${answer}`,
                        })
                      : new Paragraph({
                          indent: { left: '0.2 in' },
                          children: [
                            new TextRun(
                              `${String.fromCharCode(65 + answerIndex)}. `,
                            ),
                            ...answer.split('%').map(
                              (part: string, partIndex: number) =>
                                new TextRun({
                                  text: part,
                                  bold: partIndex === 1,
                                  ...(partIndex === 1
                                    ? {
                                        underline: {
                                          type: UnderlineType.SINGLE,
                                        },
                                      }
                                    : {}),
                                }),
                            ),
                          ],
                        }),
                  ],
                }),
            ),
          ],
        }),
      ],
    }),
  )
  return list
}
function RenderTextNewLineAndFontStyle(text:string, textNoStyle = "______________" ){
  const textArrObj = formatTextStyleObject(text)||[];
  const newTextRunArr:TextRun[] = [];
  const lenArr = textArrObj.length;
  for(let i=0; i< lenArr; i++){
    if(textArrObj[i].text.includes("\n")){
      const subItemArr = textArrObj[i].text?.split('\n')||[];
      subItemArr.forEach((item: string, index:number) =>{
        item.split(textNoStyle).forEach((m,indexSplit)=> {
          if(indexSplit>0){
            newTextRunArr.push(new TextRun({text:textNoStyle}))
            newTextRunArr.push(new TextRun({...textArrObj[i],italics: 
              textArrObj[i].italic,
              text:m}))
          }else{
            newTextRunArr.push(new TextRun({...textArrObj[i],italics: 
              textArrObj[i].italic,
              break:index>0?1:0,
              text:m}))
          }
        })
      })
    }else{
      textArrObj[i].text.split(textNoStyle).forEach((m:string,indexSplit:number)=> {
        if(indexSplit>0){
          newTextRunArr.push(new TextRun({text:textNoStyle}))
          newTextRunArr.push(new TextRun({...textArrObj[i],italics: 
            textArrObj[i].italic,
            text:m}))
        }else{
          newTextRunArr.push(new TextRun({...textArrObj[i],italics: 
            textArrObj[i].italic,
            text:m}))
        }
      })
    }
  }
  return newTextRunArr;
}

function RenderTextNewLineAndFontStyleFB(text:string, indexNumber:number, exampleIndex: number, exampleText: string){
  const newTextRunArr: TextRun[] = [];
  const txt = text.replaceAll("%s%","###")
  let count = 0
  const newData = formatTextStyleObject(txt)
  const result:any[] = []
  newData.forEach((obj: any, indexObj: number) => {
    const textArr = obj.text.split('###')
    const len =  textArr.length
    textArr.map((item:any, aIndex:number) => {
      result.push({
        ...obj,
        text: item,
      })
      if(aIndex < len - 1){
        result.push({
          text: '%s%',
          bold: undefined,
          underline: undefined,
          italic: undefined,
        })
      }
    })
  })

  const allTab = result.filter((item: any, ) => item.text === '%s%')
  for(let i = 0; i < allTab.length; i++){
    if(i === exampleIndex){
      allTab[i].text = ` ${exampleText} `
      allTab[i].italic = true
    }
  }
  result.forEach((item, i) => {
    if(item.text === '%s%'){
      newTextRunArr.push(new TextRun({
        text: ` (${indexNumber + count})___________`,
        bold: undefined, 
        underline: undefined, 
        italics: undefined
      }))
      count = count + 1
    }
    else{
      const arrNewLine = item.text.split('\n') || [];
      const isSpaceNext = result[i+1]?.text == "%s%"
      arrNewLine.forEach((n:any, m:number)=>{
        newTextRunArr.push(new TextRun({
          text:isSpaceNext?n.substring(n.length - 1) === ' ' && n !== ' ' ? n.slice(0, n.length - 1) : n:n,
          bold: item.bold, 
          underline: item.underline, 
          italics: item.italic,
          break: m > 0 ? 1 : 0
        }))
      })
    }
  })
  return newTextRunArr;  
}

function ReadingScript(script: string, list: any[]) {
  const textRunArr = formatTextStyleObject(script)
  const tempTextWithObjectStyle: any[] = []
  const regex = /\n/g
  textRunArr.forEach((item: any, index: number) => {
    if (item.text.match(regex)) {
      const arrItem = item.text.split('\n')
      arrItem.forEach((m: any, mIndex: number)=> {
        tempTextWithObjectStyle.push({
          ...item,
          text: m,
          break: mIndex > 0 ? 1 : 0
        })
      })
    }else {
      tempTextWithObjectStyle.push(item)
    }
  })
  const textRunParagraph = tempTextWithObjectStyle.map((item: any, mIndex: number) => {
    return new TextRun({
      ...item,
      italics: item.italic,
    })
  })
  list.push(
    new Table({
      width: { size: 100, type: WidthType.PERCENTAGE },
      borders: {
        top: { color: '#35b9e6', style: BorderStyle.DASHED },
        left: { color: '#35b9e6', style: BorderStyle.DASHED },
        right: { color: '#35b9e6', style: BorderStyle.DASHED },
        bottom: { color: '#35b9e6', style: BorderStyle.DASHED },
      },
      rows: [
        new TableRow({
          children: [
            new TableCell({
              shading: { fill: '#edf9fd' },
              children: [
                new Paragraph({
                  keepLines: false,
                  spacing: {
                    before: 60,
                    after: 60,
                    line: 300,
                  },
                  indent: {
                    left: '0.1 in',
                    right: '0.1 in',
                  },
                  children: [...textRunParagraph],
                })
              ],
            }),
          ],
        }),
      ],
    }),
  )
}

function MultiChoiceQuestion5(question: any, index: number, list: any[]) {
  const questionList:string[] = question.question_text?.split('#')
  const answerList:string[] = question.answers?.split('#') ?? []
    const listCorrectAnswer:string[] = question.correct_answers?.split('#') ?? []
  const columnWidth =
    100.0 / Math.max(...answerList.map((m: string) => m?.split('*').length))
  const pqdArr = `${question.parent_question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n')

  const listQuestionTextExample:string[] = []
  const listQuestionTextQuestions:string[] = []
  const listAnswerExample:string[] = []
  const listAnswerQuestions: string[] = []
  const listCorrectExample:string[] = []
  for(let i = 0; i< questionList.length; i++){
    const item:string = questionList[i]
    if(item.startsWith('*')){
      listQuestionTextExample.push(item.replace('*',''))
      listAnswerExample.push(answerList[i])
      listCorrectExample.push(listCorrectAnswer[i])
    }else{
      listQuestionTextQuestions.push(item)
      listAnswerQuestions.push(answerList[i])
    }
  }
  const pqdParagraph = pqdArr.map((item: string, mIndex: number) => {
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === pqdArr.length - 1 ? 200 : 0,
      },
      children: [new TextRun({ text: item, bold: true })],
    })
  })
  list.push(...pqdParagraph)

  if(listQuestionTextExample.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    listQuestionTextExample.forEach((questionT: string, qIndex: number) => {
      const questionTextRun = new Paragraph({
        spacing:{before:150},
        indent: { left: '0.15 in' },
        children: RenderTextNewLineAndFontStyle(questionT.replaceAll(/\%s\%/g,space),space)
      })   
      list.push(questionTextRun)
      
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 100, type: WidthType.PERCENTAGE },
          rows: [
            ...listAnswerExample[qIndex]?.split('*').map(
              (answer: any, answerIndex: number) =>
                new TableRow({
                  children: [
                    new TableCell({
                      width: { size: columnWidth, type: WidthType.PERCENTAGE },
                      children: [
                        new Paragraph({
                          indent: { left: '0.15 in' },
                          children: [
                            new TextRun({
                              text: `${String.fromCharCode(65 + answerIndex)}. `,
                              ...(listCorrectExample[qIndex] === convertTextCell(answer)
                                ? {
                                    underline: {
                                      type: UnderlineType.SINGLE,
                                      color: trueColor,
                                    },
                                    color: trueColor,
                                  }
                                : {}),
                            }),
                            new TextRun({text: answer}),
                          ],
                        }),
                      ],
                    }),
                  ],
                }),
            ),
          ],
        }),
      )
    })
  }
  if(listQuestionTextQuestions.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: listQuestionTextQuestions.length > 1 ? 'Questions:' : 'Question:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    listQuestionTextQuestions.forEach((questionT: string, qIndex: number) => {
      const questionTextRun = new Paragraph({
        spacing:{before:150},
        indent: { left: '0.15 in' },
        children: RenderTextNewLineAndFontStyle(`${index + qIndex}. ${questionT.replaceAll(/\%s\%/g,space)}`,space)
      })   
      list.push(questionTextRun)
      
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 100, type: WidthType.PERCENTAGE },
          rows: [
            ...listAnswerQuestions[qIndex]?.split('*').map(
              (answer: any, answerIndex: number) =>
                new TableRow({
                  children: [
                    new TableCell({
                      width: { size: columnWidth, type: WidthType.PERCENTAGE },
                      children: [
                        new Paragraph({
                          indent: { left: '0.15 in' },
                          text: `${String.fromCharCode(
                            65 + answerIndex,
                          )}. ${answer}`,
                        }),
                      ],
                    }),
                  ],
                }),
            ),
          ],
        }),
      )
    })
  }
  

  return list
}
function MultiChoiceQuestion7(question: any, index: number, list: any[]) {
  type dataViewModel = {
    questions: string[]
    answers: string[]
    corrects: string[]
  }
  const listAnswer: string[] = question?.answers?.split('#') || []
  const listQuestionText: string[] = question?.question_text?.split('#') || []
  const listCorrectAnswer: string[] = question?.correct_answers?.split('#') || []
  
  const example: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
  }
  const mainQuestion: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
  }
  for(let i= 0; i < listQuestionText.length; i++ ){
    if(listQuestionText[i]?.startsWith('*')){
      example.questions.push(listQuestionText[i].substring(1))
      example.answers.push(listAnswer[i])
      example.corrects.push(listCorrectAnswer[i])
    }else{
      mainQuestion.questions.push(listQuestionText[i])
      mainQuestion.answers.push(listAnswer[i])
      mainQuestion.corrects.push(listCorrectAnswer[i])
    }
  }
  const pqdArr = question?.parent_question_description? `${question.parent_question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  const pqdParagraph = pqdArr.map((item: string, mIndex: number) => {
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === pqdArr.length - 1 ? 200 : 0,
      },
      children: [new TextRun({ text: item, bold: true })],
    })
  })
  list.push(...pqdParagraph)
  if(example.questions.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: example.questions.length > 1 ? 'Examples:' : 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.questions.forEach((questionText:any, indexItem:number)=>{
      const answerListImage = example.answers[indexItem]?.split('*') || []
      const listCorrectImage =example.corrects[indexItem]
      const questionTextRun = RenderTextNewLineAndFontStyle(questionText.replaceAll(/\%s\%/g,space),space);
      list.push(new Paragraph({
        spacing:{
          before: 150,
        },
        indent:{left: '0.13 in'},
        children: questionTextRun
      }))
      if(answerListImage.length === 4){
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              }},
              width: { size: 96, type: WidthType.PERCENTAGE },
              alignment: AlignmentType.END,
              rows:[
                new TableRow({
                  children:[
                    ...answerListImage.map((answer:string,answerIndex: number)=>{
                      if(answerIndex < 2){
                        const imageUrl = `${uploadDir}/${answer}`
                        const imageSize = sizeOfImage(imageUrl)
                        const maxHeight = 250
                        let imgWidth = 136
                        let imgHeight = (imageSize.height * 136) / imageSize.width
                        if (imgHeight > maxHeight) {
                          imgHeight = 250
                          imgWidth = (imageSize.width * 250) / imageSize.height
                        }
                          return new TableCell({
                            width:{size:48, type:WidthType.PERCENTAGE},
                            verticalAlign: VerticalAlign.BOTTOM,
                            children:[
                              new Paragraph({
                                spacing: { before: 200, after: 200 },
                                alignment: AlignmentType.CENTER,
                                children:[
                                  new ImageRun({
                                    data: fs.readFileSync(imageUrl),
                                    transformation:{
                                      width:imgWidth,
                                      height:imgHeight,
                                    }
                                  })
                                ]
                              })
                            ]
                        })
                      }
                      
                    })
                  ]
                }),
                new TableRow({
                  height:{value:500, rule: HeightRule.EXACT},
                  children:[
                    ...answerListImage.map((answer:string,answerIndex: number)=>{
                      if(answerIndex < 2){
                        return new TableCell({
                          width:{size:48, type:WidthType.PERCENTAGE},
                          verticalAlign: VerticalAlign.TOP,
                          children:[
                              new Paragraph({
                                alignment: AlignmentType.CENTER,
                                children:[
                                  new TextRun({
                                    text: answerIndex.toString() === listCorrectImage
                                    ? 
                                    `${String.fromCharCode(65 + answerIndex,)}. ☑` 
                                    : 
                                    `${String.fromCharCode(65 + answerIndex,)}. ☐`,
                                    color: '#3c3c3c',
                                    bold: true,
                                    })
                                ]
                              })
                            ]
                        })
                      }
                    })
                  ]
                }),
                new TableRow({
                  children:[
                    ...answerListImage.map((answer:string,answerIndex: number)=>{
                      if(answerIndex > 1){
                        const imageUrl = `${uploadDir}/${answer}`
                        const imageSize = sizeOfImage(imageUrl)
                        const maxHeight = 250
                        let imgWidth = 136
                        let imgHeight = (imageSize.height * 136) / imageSize.width
                        if (imgHeight > maxHeight) {
                          imgHeight = 250
                          imgWidth = (imageSize.width * 250) / imageSize.height
                        }
                        return  new TableCell({
                          width:{size:48, type:WidthType.PERCENTAGE},
                          verticalAlign: VerticalAlign.BOTTOM,
                          children:[
                            new Paragraph({
                              spacing: { before: 200, after: 200 },
                              alignment: AlignmentType.CENTER,
                              children:[
                                new ImageRun({
                                  data: fs.readFileSync(imageUrl),
                                  transformation:{
                                    width:imgWidth,
                                    height:imgHeight,
                                  }
                                })
                              ]
                            })
                          ]
                      })
                      }
                    })
                  ]
                }),
                new TableRow({
                  height:{value:500, rule: HeightRule.EXACT},
                  children:[
                    ...answerListImage.map((answer:string,answerIndex: number)=>{
                      if(answerIndex > 1){
                        return new TableCell({
                          width:{size:48, type:WidthType.PERCENTAGE},
                          verticalAlign: VerticalAlign.TOP,
                          children:[
                              new Paragraph({
                                alignment: AlignmentType.CENTER,
                                children:[
                                  new TextRun({
                                    text: answerIndex.toString() === listCorrectImage
                                    ? 
                                    `${String.fromCharCode(65 + answerIndex,)}. ☑` 
                                    : 
                                    `${String.fromCharCode(65 + answerIndex,)}. ☐`,
                                    color: '#3c3c3c',
                                    bold: true,
                                    })
                                ]
                              })
                            ]
                        })
                      }
                    })
                  ]
                }),
              ]
          })
        )
      }else{
        const columnWidth = Math.round(96.0 / Math.max(3))
        const maxRow = Math.ceil(answerListImage.length / 3)
        const tableRowArr = []
        for(let i =0; i< maxRow; i++){
          tableRowArr.push(i)
        }
        const tableRow:any[] = []
        tableRowArr.forEach((indexRow:number)=>{
          tableRow.push(
            new TableRow({
              children:[
                ...answerListImage.map((answer:string,answerIndex: number)=>{
                  if((answerIndex + 1 >= indexRow * 3 + 1) && (answerIndex + 1 <= indexRow * 3 + 3)){
                    const imageUrl = `${uploadDir}/${answer}`
                    const imageSize = sizeOfImage(imageUrl)
                    const maxHeight = 250
                    let imgWidth = 136
                    let imgHeight = (imageSize.height * 136) / imageSize.width
                    if (imgHeight > maxHeight) {
                      imgHeight = 250
                      imgWidth = (imageSize.width * 250) / imageSize.height
                    }
                    return new TableCell({
                      width:{size:columnWidth, type:WidthType.PERCENTAGE},
                      verticalAlign: VerticalAlign.BOTTOM,
                      children:[
                        new Paragraph({
                          spacing: { before: 200, after: 200 },
                          alignment: AlignmentType.CENTER,
                          children:[
                            new ImageRun({
                              data: fs.readFileSync(imageUrl),
                              transformation:{
                                width:imgWidth,
                                height:imgHeight,
                              }
                            })
                          ]
                        }),
                      ]
                    })
                  }
                })
              ]
            })
          )
          tableRow.push(
            new TableRow({
              height:{value:500, rule: HeightRule.EXACT},
              children: [
                ...answerListImage.map((answer:string,answerIndex: number)=>{
                  if((answerIndex + 1 >= indexRow * 3 + 1) && (answerIndex + 1 <= indexRow * 3 + 3)){
                    return new TableCell({
                      width:{size:columnWidth, type:WidthType.PERCENTAGE},
                      verticalAlign: VerticalAlign.TOP,
                      children:[
                        new Paragraph({
                          alignment: AlignmentType.CENTER,
                          children:[
                            new TextRun({
                              text: answerIndex.toString() === listCorrectImage
                              ? 
                              `${String.fromCharCode(65 + answerIndex,)}. ☑` 
                              : 
                              `${String.fromCharCode(65 + answerIndex,)}. ☐`,
                              color: '#3c3c3c',
                              bold: true,
                            }),
                          ]
                        }),
                        
                      ]
                    })
                  }
                })
              ]
            })
          )
        })
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              }},
              width: { size: 96, type: WidthType.PERCENTAGE },
              alignment: AlignmentType.END,
              rows: [...tableRow]
          })
        )
      }
    })
  }
  
  if(mainQuestion.questions.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: mainQuestion.questions.length > 1 ? 'Questions:' : 'Question:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    mainQuestion.questions.forEach((questionText:any, indexItem:number)=>{
      const answerListImage = mainQuestion.answers[indexItem]?.split('*') || []
      const listCorrectImage =mainQuestion.corrects[indexItem]
       const questionTextRun = RenderTextNewLineAndFontStyle(`${index + indexItem}. ${questionText.replaceAll(/\%s\%/g,space)}`,space);
      list.push(new Paragraph({
        spacing:{
          before: 150,
        },
        indent:{left: '0.13 in'},
        children: questionTextRun
      }))
      if(answerListImage.length === 4){
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              }},
              width: { size: 96, type: WidthType.PERCENTAGE },
              alignment: AlignmentType.END,
              rows:[
                new TableRow({
                  children:[
                    ...answerListImage.map((answer:string,answerIndex: number)=>{
                      if(answerIndex < 2){
                        const imageUrl = `${uploadDir}/${answer}`
                        const imageSize = sizeOfImage(imageUrl)
                        const maxHeight = 250
                        let imgWidth = 136
                        let imgHeight = (imageSize.height * 136) / imageSize.width
                        if (imgHeight > maxHeight) {
                          imgHeight = 250
                          imgWidth = (imageSize.width * 250) / imageSize.height
                        }
                          return new TableCell({
                            width:{size:48, type:WidthType.PERCENTAGE},
                            verticalAlign: VerticalAlign.BOTTOM,
                            children:[
                              new Paragraph({
                                spacing: { before: 200, after: 200 },
                                alignment: AlignmentType.CENTER,
                                children:[
                                  new ImageRun({
                                    data: fs.readFileSync(imageUrl),
                                    transformation:{
                                      width:imgWidth,
                                      height:imgHeight,
                                    }
                                  })
                                ]
                              })
                            ]
                        })
                      }
                      
                    })
                  ]
                }),
                new TableRow({
                  height:{value:500, rule: HeightRule.EXACT},
                  children:[
                    ...answerListImage.map((answer:string,answerIndex: number)=>{
                      if(answerIndex < 2){
                        return new TableCell({
                          width:{size:48, type:WidthType.PERCENTAGE},
                          verticalAlign: VerticalAlign.TOP,
                          children:[
                              new Paragraph({
                                alignment: AlignmentType.CENTER,
                                children:[
                                  new TextRun({
                                    text: `${String.fromCharCode(65 + answerIndex,)}. ☐`,
                                    color: '#3c3c3c',
                                    bold: true,
                                    })
                                ]
                              })
                            ]
                        })
                      }
                    })
                  ]
                }),
                new TableRow({
                  children:[
                    ...answerListImage.map((answer:string,answerIndex: number)=>{
                      if(answerIndex > 1){
                        const imageUrl = `${uploadDir}/${answer}`
                        const imageSize = sizeOfImage(imageUrl)
                        const maxHeight = 250
                        let imgWidth = 136
                        let imgHeight = (imageSize.height * 136) / imageSize.width
                        if (imgHeight > maxHeight) {
                          imgHeight = 250
                          imgWidth = (imageSize.width * 250) / imageSize.height
                        }
                        return  new TableCell({
                          width:{size:48, type:WidthType.PERCENTAGE},
                          verticalAlign: VerticalAlign.BOTTOM,
                          children:[
                            new Paragraph({
                              spacing: { before: 200, after: 200 },
                              alignment: AlignmentType.CENTER,
                              children:[
                                new ImageRun({
                                  data: fs.readFileSync(imageUrl),
                                  transformation:{
                                    width:imgWidth,
                                    height:imgHeight,
                                  }
                                })
                              ]
                            })
                          ]
                      })
                      }
                    })
                  ]
                }),
                new TableRow({
                  height:{value:500, rule: HeightRule.EXACT},
                  children:[
                    ...answerListImage.map((answer:string,answerIndex: number)=>{
                      if(answerIndex > 1){
                        return new TableCell({
                          width:{size:48, type:WidthType.PERCENTAGE},
                          verticalAlign: VerticalAlign.TOP,
                          children:[
                              new Paragraph({
                                alignment: AlignmentType.CENTER,
                                children:[
                                  new TextRun({
                                    text: `${String.fromCharCode(65 + answerIndex,)}. ☐`,
                                    color: '#3c3c3c',
                                    bold: true,
                                    })
                                ]
                              })
                            ]
                        })
                      }
                    })
                  ]
                }),
              ]
          })
        )
      }else{
        const columnWidth = Math.round(96.0 / Math.max(3))
        const maxRow = Math.ceil(answerListImage.length / 3)
        const tableRowArr = []
        for(let i =0; i< maxRow; i++){
          tableRowArr.push(i)
        }
        const tableRow:any[] = []
        tableRowArr.forEach((indexRow:number)=>{
          tableRow.push(
            new TableRow({
              children:[
                ...answerListImage.map((answer:string,answerIndex: number)=>{
                  if((answerIndex + 1 >= indexRow * 3 + 1) && (answerIndex + 1 <= indexRow * 3 + 3)){
                    const imageUrl = `${uploadDir}/${answer}`
                    const imageSize = sizeOfImage(imageUrl)
                    const maxHeight = 250
                    let imgWidth = 136
                    let imgHeight = (imageSize.height * 136) / imageSize.width
                    if (imgHeight > maxHeight) {
                      imgHeight = 250
                      imgWidth = (imageSize.width * 250) / imageSize.height
                    }
                    return new TableCell({
                      width:{size:columnWidth, type:WidthType.PERCENTAGE},
                      verticalAlign: VerticalAlign.BOTTOM,
                      children:[
                        new Paragraph({
                          spacing: { before: 200, after: 200 },
                          alignment: AlignmentType.CENTER,
                          children:[
                            new ImageRun({
                              data: fs.readFileSync(imageUrl),
                              transformation:{
                                width:imgWidth,
                                height:imgHeight,
                              }
                            })
                          ]
                        }),
                      ]
                    })
                  }
                })
              ]
            })
          )
          tableRow.push(
            new TableRow({
              height:{value:500, rule: HeightRule.EXACT},
              children: [
                ...answerListImage.map((answer:string,answerIndex: number)=>{
                  if((answerIndex + 1 >= indexRow * 3 + 1) && (answerIndex + 1 <= indexRow * 3 + 3)){
                    return new TableCell({
                      width:{size:columnWidth, type:WidthType.PERCENTAGE},
                      verticalAlign: VerticalAlign.TOP,
                      children:[
                        new Paragraph({
                          alignment: AlignmentType.CENTER,
                          children:[
                            new TextRun({
                              text: `${String.fromCharCode(65 + answerIndex,)}. ☐`,
                              color: '#3c3c3c',
                              bold: true,
                            }),
                          ]
                        }),
                        
                      ]
                    })
                  }
                })
              ]
            })
          )
        })
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              }},
              width: { size: 96, type: WidthType.PERCENTAGE },
              alignment: AlignmentType.END,
              rows: [...tableRow]
          })
        )
      }
    })
  }
  return list
}

function MultiChoiceQuestion9(question: any, index:number, list: any[]){
  type dataViewModel = {
    questions: string[]
    answers: string[]
    corrects: string[]
    images: any[],
  }

  const listAnswer: string[] = question?.answers?.split('#') || []
  const listQuestionText: string[] = question?.question_text?.split('#') || []
  const listCorrectAnswer: string[] = question?.correct_answers?.split('#') || []
  const listImages = question?.image?.split('#') || []

  const example: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
    images: [],
  }
  const mainQuestion: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
    images: [],
  }

  for (let i = 0; i < listQuestionText.length; i++) {
    if (listQuestionText[i]?.startsWith('*')) {
      example.questions.push(listQuestionText[i].substring(1))
      example.answers.push(listAnswer[i])
      example.corrects.push(listCorrectAnswer[i])
      example.images.push(listImages[i])
    } else {
      mainQuestion.questions.push(listQuestionText[i])
      mainQuestion.answers.push(listAnswer[i])
      mainQuestion.corrects.push(listCorrectAnswer[i])
      mainQuestion.images.push(listImages[i])
    }
  }
  const pqdArr = question?.parent_question_description? `${question.parent_question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  const pqdParagraphMC9 = pqdArr.map((item: string, mIndex: number) => {

    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === pqdArr.length - 1 ? 200 : 0,
      },
      children: [new TextRun({ text: item, bold: true })],
    })
  })
  list.push(...pqdParagraphMC9)
  const space = '______________'
  if(example.questions.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
          after:200
        },
        children: [
          new TextRun({
            text: 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.questions.forEach((questionText: string, indexItem: number) => {
      const answers = example.answers[indexItem]
          ?.split('*')
          .map((answer: string) => formatTextStyleObject(answer))
  
          const questionTextRun = new Paragraph({
            children: RenderTextNewLineAndFontStyle(questionText.replaceAll(/\%s\%/g,space),space)
          })   

      const imageUrl = `${uploadDir}/${example.images[indexItem]}`
      const imageSize = sizeOfImage(imageUrl)
  
      if (answers.length === 4 || answers.length <=2){
        list.push(
          new Table({
            margins: { top: 150},
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 4, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: ``,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 22, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.LEFT,
                        children: [
                          new ImageRun({
                            data: fs.readFileSync(imageUrl),
                            transformation: {
                              width: 136,
                              height:
                                (imageSize.height * 136) / imageSize.width,
                            },
                          }),
                          ,
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    margins: {left: 60},
                    width: { size: 74, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      questionTextRun,
                      new Table({
                        // margins: {left: 40, top: 40},
                        borders: {
                          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          insideHorizontal: {
                            style: BorderStyle.SINGLE,
                            size: 1,
                            color: '#ffffff',
                          },
                          insideVertical: {
                            style: BorderStyle.SINGLE,
                            size: 1,
                            color: '#ffffff',
                          },
                        },
                        width: { size: 100, type: WidthType.PERCENTAGE },
                        alignment: AlignmentType.START,
                        rows: [
                          new TableRow({
                            children: [
                              ...answers.map((answer: any, answerIndex: number) => {
                                if(answerIndex < 2){
                                  return new TableCell({
                                    width: { size: 50, type: WidthType.PERCENTAGE },
                                    verticalAlign: VerticalAlign.TOP,
                                    children: [
                                      new Paragraph({
                                        spacing: {
                                          before: 200,
                                          after: 200,
                                        },
                                        alignment: AlignmentType.START,
                                        children: [
                                          new TextRun({
                                            text: `${String.fromCharCode(65 + answerIndex)}. `,
                                            ...(example.corrects[indexItem] ===
                                            convertTextCell(answer)
                                              ? {
                                                  underline: {
                                                    type: UnderlineType.SINGLE,
                                                    color: trueColor,
                                                  },
                                                  color: trueColor,
                                                }
                                              : {}),
                                          }),
                                          ...getArrTextRunFromArrTextStyleObject(answer),
                                        ],
                                      }),
                                    ],
                                  })
                                }
                                
                              }),
                            ],
                          }),
                          ...(answers.length == 4?[
                            new TableRow({
                              children: [
                                ...answers.map((answer: any, answerIndex: number) => {
                                  if(answerIndex > 1){
                                    return new TableCell({
                                      width: { size: 50, type: WidthType.PERCENTAGE },
                                      verticalAlign: VerticalAlign.TOP,
                                      children: [
                                        new Paragraph({
                                          spacing: {
                                            before: 200,
                                            after: 200,
                                          },
                                          alignment: AlignmentType.START,
                                          children: [
                                            new TextRun({
                                              text: `${String.fromCharCode(65 + answerIndex)}. `,
                                              ...(example.corrects[indexItem] ===
                                              convertTextCell(answer)
                                                ? {
                                                    underline: {
                                                      type: UnderlineType.SINGLE,
                                                      color: trueColor,
                                                    },
                                                    color: trueColor,
                                                  }
                                                : {}),
                                            }),
                                            ...getArrTextRunFromArrTextStyleObject(answer),
                                          ],
                                        }),
                                      ],
                                    })
                                  }
                                  
                                }),
                              ],
                            })
                          ]:[])
                        ],
                      })
                    ],
                  }),
                ],
              }),
              new TableRow({
                height:{value:200, rule: HeightRule.EXACT},
                children: [
                  new TableCell({
                    children: [
                      new Paragraph('')
                    ]
                  })
                ]
              })
            ],
          }),
        )
      }
  
      else{
        const columnWidth = Math.round(100.0 / Math.max(3))
        const maxRow = Math.ceil(answers.length / 3)
        const tableRowArr = []
        for(let i =0; i < maxRow; i++){
          tableRowArr.push(i)
        }
        const tableRow:any[] = []
        tableRowArr.forEach((indexRow: number) => {
          tableRow.push(
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  if((answerIndex + 1 >= indexRow * 3 + 1) && (answerIndex + 1 <= indexRow * 3 + 3)){
                    return new TableCell({
                      width: { size: columnWidth, type: WidthType.PERCENTAGE },
                      verticalAlign: VerticalAlign.TOP,
                      children: [
                        new Paragraph({
                          spacing: {
                            before: 200,
                            after: 200,
                          },
                          alignment: AlignmentType.START,
                          children: [
                            new TextRun({
                              text: `${String.fromCharCode(65 + answerIndex)}. `,
                              ...(example.corrects[indexItem] ===
                              convertTextCell(answer)
                                ? {
                                    underline: {
                                      type: UnderlineType.SINGLE,
                                      color: trueColor,
                                    },
                                    color: trueColor,
                                  }
                                : {}),
                            }),
                            ...getArrTextRunFromArrTextStyleObject(answer),
                          ],
                        }),
                      ],
                    })
                  }
                  
                }),
              ],
            }),
          )
        })
  
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 4, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: ``,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 22, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.LEFT,
                        children: [
                          new ImageRun({
                            data: fs.readFileSync(imageUrl),
                            transformation: {
                              width: 136,
                              height:
                                (imageSize.height * 136) / imageSize.width,
                            },
                          }),
                          ,
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    margins: {left: 60},
                    width: { size: 74, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      questionTextRun,
                      new Table({
                        // margins: {left: 40, top: 40},
                        borders: {
                          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          insideHorizontal: {
                            style: BorderStyle.SINGLE,
                            size: 1,
                            color: '#ffffff',
                          },
                          insideVertical: {
                            style: BorderStyle.SINGLE,
                            size: 1,
                            color: '#ffffff',
                          },
                        },
                        width: { size: 100, type: WidthType.PERCENTAGE },
                        alignment: AlignmentType.START,
                        rows: [...tableRow]
                      })
                    ]
                  })
                ]
              }),
              new TableRow({
                height:{value:200, rule: HeightRule.EXACT},
                children: [
                  new TableCell({
                    children: [
                      new Paragraph('')
                    ]
                  })
                ]
              })
            ]
          })
        )
      }
      
    })
  }
  if (mainQuestion.questions.length > 0) {
      list.push(
        new Paragraph({
          spacing: {
            before: 200,
            after:200
          },
          children: [
            new TextRun({
              text: mainQuestion.questions.length > 1 ? 'Questions:' : 'Question:',
              bold: true,
              underline: {
                type: UnderlineType.SINGLE,
              },
            }),
          ],
        }),
      )
    
    mainQuestion.questions.forEach((questionText: string, indexItem: number) => {
      const answers = mainQuestion.answers[indexItem]
        ?.split('*')
        .map((answer: string) => formatTextStyleObject(answer))
  
        const questionTextRun = new Paragraph({
          children: RenderTextNewLineAndFontStyle(questionText.replaceAll(/\%s\%/g,space),space)
        }) 
      // list.push(...questionTextRun)
      const imageUrl = `${uploadDir}/${mainQuestion.images[indexItem]}`
      const imageSize = sizeOfImage(imageUrl)
  
      if (answers.length === 4 || answers.length <= 2){
        list.push(
          new Table({
            margins: { top: 150 },
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            width: { size: 100, type: WidthType.PERCENTAGE },
            alignment: AlignmentType.END,
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 4, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: `${index + indexItem}. `,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 22, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.LEFT,
                        children: [
                          new ImageRun({
                            data: fs.readFileSync(imageUrl),
                            transformation: {
                              width: 136,
                              height:
                                (imageSize.height * 136) / imageSize.width,
                            },
                          }),
                          ,
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 74, type: WidthType.PERCENTAGE },
                    margins: {left: 60},
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      questionTextRun,
                      new Table({
                        // margins: {left: 40, top: 40},
                        borders: {
                          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          insideHorizontal: {
                            style: BorderStyle.SINGLE,
                            size: 1,
                            color: '#ffffff',
                          },
                          insideVertical: {
                            style: BorderStyle.SINGLE,
                            size: 1,
                            color: '#ffffff',
                          },
                        },
                        width: { size: 100, type: WidthType.PERCENTAGE },
                        alignment: AlignmentType.START,
                        rows: [
                          new TableRow({
                            children: [
                              ...answers.map((answer: any, answerIndex: number) => {
                                if(answerIndex < 2){
                                  return new TableCell({
                                    width: { size: 50, type: WidthType.PERCENTAGE },
                                    verticalAlign: VerticalAlign.TOP,
                                    children: [
                                      new Paragraph({
                                        spacing: {
                                          before: 200,
                                          after: 200,
                                        },
                                        alignment: AlignmentType.START,
                                        children: [
                                          new TextRun(
                                            `${String.fromCharCode(65 + answerIndex)}. `,
                                          ),
                                          ...getArrTextRunFromArrTextStyleObject(answer),
                                        ],
                                      }),
                                    ],
                                  })
                                }
                                
                              }),
                            ],
                          }),
                          ...(answers.length == 4?[
                            new TableRow({
                              children: [
                                ...answers.map((answer: any, answerIndex: number) => {
                                  if(answerIndex > 1){
                                    return new TableCell({
                                      width: { size: 50, type: WidthType.PERCENTAGE },
                                      verticalAlign: VerticalAlign.TOP,
                                      children: [
                                        new Paragraph({
                                          spacing: {
                                            before: 200,
                                            after: 200,
                                          },
                                          alignment: AlignmentType.START,
                                          children: [
                                            new TextRun(
                                              `${String.fromCharCode(65 + answerIndex)}. `,
                                            ),
                                            ...getArrTextRunFromArrTextStyleObject(answer),
                                          ],
                                        }),
                                      ],
                                    })
                                  }
                                  
                                }),
                              ],
                            })
                          ]:[])
                        ]
                      })
                    ],
                  }),
                ]
              }),
              new TableRow({
                height:{value:200, rule: HeightRule.EXACT},
                children: [
                  new TableCell({
                    children: [
                      new Paragraph('')
                    ]
                  })
                ]
              })
            ],
          }),
        )
      }
      
      else{
        const columnWidth = Math.round(100.0 / Math.max(3))
        const maxRow = Math.ceil(answers.length / 3)
        const tableRowArr = []
        for(let i =0; i < maxRow; i++){
          tableRowArr.push(i)
        }
        const tableRow:any[] = []
        tableRowArr.forEach((indexRow: number) => {
          tableRow.push(
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  if((answerIndex + 1 >= indexRow * 3 + 1) && (answerIndex + 1 <= indexRow * 3 + 3)){
                    return new TableCell({
                      width: { size: columnWidth, type: WidthType.PERCENTAGE },
                      verticalAlign: VerticalAlign.TOP,
                      children: [
                        new Paragraph({
                          spacing: {
                            before: 200,
                            after: 200,
                          },
                          alignment: AlignmentType.START,
                          children: [
                            new TextRun(
                              `${String.fromCharCode(65 + answerIndex)}. `,
                            ),
                            ...getArrTextRunFromArrTextStyleObject(answer),
                          ],
                        }),
                      ],
                    })
                  }
                }),
              ],
            }),
          )
        })
  
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 4, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: `${index + indexItem}. `,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 22, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.LEFT,
                        children: [
                          new ImageRun({
                            data: fs.readFileSync(imageUrl),
                            transformation: {
                              width: 136,
                              height:
                                (imageSize.height * 136) / imageSize.width,
                            },
                          }),
                          ,
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    margins: {left: 60},
                    width: { size: 74, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      questionTextRun,
                      new Table({
                        // margins: {left: 40, top: 40},
                        borders: {
                          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
                          insideHorizontal: {
                            style: BorderStyle.SINGLE,
                            size: 1,
                            color: '#ffffff',
                          },
                          insideVertical: {
                            style: BorderStyle.SINGLE,
                            size: 1,
                            color: '#ffffff',
                          },
                        },
                        width: { size: 100, type: WidthType.PERCENTAGE },
                        alignment: AlignmentType.START,
                        rows: [...tableRow]
                      })
                    ]
                  })
                ]
              }),
              new TableRow({
                height:{value:200, rule: HeightRule.EXACT},
                children: [
                  new TableCell({
                    children: [
                      new Paragraph('')
                    ]
                  })
                ]
              })
            ]
          })
        )
      }
    },
    )
  }
  return list
}
//mutil choice type 5 update
function MultiChoiceQuestion5_v2(question: any, index: number, list: any[]) {
  const questionList:string[] = question.question_text?.split('#') ?? []
  const answerList:string[] = question.answers?.split('#') ?? []
  const listCorrectAnswer:string[] = question.correct_answers?.split('#') ?? []
  const TextRunQ = question?.parent_question_description? `${question.parent_question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  // const columnWidth =
  //   100.0 / Math.max(...answerList.map((m: string[]) => m.length))
  const listQuestionTextExample:string[] = []
  const listQuestionTextQuestions:string[] = []
  const listAnswerExample:string[] = []
  const listAnswerQuestions: string[] = []
  const listCorrectExample:string[] = []
  for(let i = 0; i< questionList.length; i++){
    const item:string = questionList[i]
    if(item.startsWith('*')){
      listQuestionTextExample.push(item.replace('*',''))
      listAnswerExample.push(answerList[i])
      listCorrectExample.push(listCorrectAnswer[i])
    }else{
      listQuestionTextQuestions.push(item)
      listAnswerQuestions.push(answerList[i])
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.parent_question_text ? 200 : 0,
      },
      children: TextRunQ.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  if (question.parent_question_text) {
    ReadingScript(question.parent_question_text, list)
  }
  if(listQuestionTextExample.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    listQuestionTextExample.map((questionText: string, indexItem: number) => {
      //get object style arr
      const answers = listAnswerExample[indexItem]
        ?.split('*')
        .map((answer: string) => formatTextStyleObject(answer))
  
      const columnWidth = 100.0 / answers.length
  
      // write index if multiple question
      const hasQuestionText = questionText && questionText != ''
      if (questionList.length && hasQuestionText) {
        const questionTextRun = new Paragraph({
          spacing:{before:150},
          indent: { left: '0.15 in' },
          children: RenderTextNewLineAndFontStyle(questionText.replaceAll(/\%s\%/g,space),space)
        })   
        list.push(questionTextRun)
      }
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 100, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  return new TableCell({
                    width: { size: columnWidth, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        indent: { left: '0.15 in' },
                        children: [
                          new TextRun({
                            text: `${String.fromCharCode(65 + answerIndex)}. `,
                            ...(listCorrectExample[indexItem] === convertTextCell(answer)
                              ? {
                                  underline: {
                                    type: UnderlineType.SINGLE,
                                    color: trueColor,
                                  },
                                  color: trueColor,
                                }
                              : {}),
                          }),
                          ...getArrTextRunFromArrTextStyleObject(answer),
                        ],
                      }),
                    ],
                  })
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  if(listQuestionTextQuestions.length > 0){
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: listQuestionTextQuestions.length > 1 ? 'Questions:' : 'Question:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    listQuestionTextQuestions.map((questionText: string, indexItem: number) => {
      //get object style arr
      const answers = listAnswerQuestions[indexItem]
        ?.split('*')
        .map((answer: string) => formatTextStyleObject(answer))
  
      const columnWidth = 100.0 / answers.length
  
      // write index if multiple question
      const hasQuestionText = questionText && questionText != ''
      if (questionList.length && hasQuestionText) {
        const questionTextRun = new Paragraph({
          spacing:{before:150},
          indent: { left: '0.15 in' },
          children: RenderTextNewLineAndFontStyle(`${index + indexItem}. ${questionText.replaceAll(/\%s\%/g,space)}`,space)
        })   
        list.push(questionTextRun)
      }
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 100, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  return new TableCell({
                    width: { size: columnWidth, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        indent: { left: '0.15 in' },
                        children: [
                          new TextRun(
                            `${String.fromCharCode(65 + answerIndex)}. `,
                          ),
                          ...getArrTextRunFromArrTextStyleObject(answer),
                        ],
                      }),
                    ],
                  })
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  

  return list
}
function MultiChoiceQuestion11(question: any, _index: number, _paragraph: any[]){
  type DataModel = {
    answerString: string[]
    correctString: string[]
  }

  const answerGroup = question?.answers.split('#') ?? []
  const correctGroup = question?.correct_answers.split('#') ?? []
  const exampleGroup: DataModel = {answerString: [], correctString: []}
  const questionGroup: DataModel = {answerString: [], correctString: []}

  answerGroup.forEach((item: string, index: number) => {
    if( item.startsWith('ex') ){
      exampleGroup.answerString.push(item.replace('ex|', ''))
      exampleGroup.correctString.push(correctGroup[index])
    }else{
      questionGroup.answerString.push(item)
      questionGroup.correctString.push(correctGroup[index])
    }
  })

  const _titleRender = ( text: string ) => {
    _paragraph.push( new Paragraph({
      spacing: { before: 200, after: 200 },
      children: [new TextRun({
        text: `${text}`, bold: true, underline: {type: UnderlineType.SINGLE}})]
    }))
  }
  const _tableRender = ( group: any, index: number, onCheck: boolean ) => {
    const answerArray = group.split('*')
    const correctArray = correctGroup[index]
    
    let rowRender: TableRow[] = []
    let cellRender: TableCell[] = []

    answerArray.map((item: string, i: number) => {
      let checked = correctArray.includes(i.toString())
      cellRender.push( _cellRender(onCheck, checked, item, i) )
      if( (i != 0 && (i + 1) % 3 == 0) || i == answerArray.length - 1 ){
        rowRender.push( _rowRender(cellRender) )
        cellRender = []
      }
    })
    const options = {
      margins: { top: 150, left: 300},
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff', },
        insideVertical: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff', },
      },
      alignment: AlignmentType.END,
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows : rowRender
    }
    return new Table(options)
  }
  const _rowRender = ( render: TableCell[] ) => {
    return new TableRow({
      children: render
    })
  }
  const _cellRender = ( onCheckBox: boolean = true, checkBox: boolean, imageString: string, index: number) => {
    let _checkbox = checkBox && onCheckBox ? '☑' : '☐'
    const imageUrl = `${uploadDir}/${imageString}`
    const imageSize = sizeOfImage(imageUrl) 
    const maxWidth = 100;
    const maxheight = 100;
    let height = maxheight;
    let width = (imageSize.width * height) / imageSize.height;
    if(width > maxWidth){
      width = maxWidth;
      height = (imageSize.height * width) / imageSize.width;
    }

    return new TableCell({
      width: { size: 33.3333, type: WidthType.PERCENTAGE },
      verticalAlign: VerticalAlign.BOTTOM,
      children: [
        new Paragraph({
          alignment: AlignmentType.CENTER,
          children: [
            new ImageRun({
              data: fs.readFileSync(imageUrl),
              transformation: {
                height: height,
                width: width,
              },
            }),
          ],
        }),
        new Paragraph({
          spacing: { before: 30, after: 30 },
          alignment: AlignmentType.CENTER,
          text: `${String.fromCharCode(65 + index)}. ${_checkbox}`,
        })
      ],
    })
  }

  const _mergeRender = ( groupArray: DataModel, hiddenTitle: boolean = false, onCheck: boolean) => {
    groupArray.answerString.map((item: string, index: number) => {
      if( hiddenTitle ){
        _paragraph.push( new Paragraph({ 
          tabStops: [
            { type: TabStopType.LEFT, position: 500, }],
          spacing: {before: 10, after: 10},
          text: `\t${_index + index}. `
        }))
      }
      _paragraph.push( _tableRender(item, index, onCheck) )
    })
  }

  _paragraph.push(new Paragraph({
    spacing: { 
      before: 200,
      after: question.question_text ? 200 : 0},
    children: [new TextRun({ 
      text: `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`, bold: true })]}))

  if( exampleGroup.answerString.length > 0 ){
    let tailExampleString = exampleGroup.answerString.length > 1 ? 's':''
    let exampleString = `Example${tailExampleString}:`
    _paragraph.push(_titleRender(exampleString))
    _mergeRender(exampleGroup, false, true)
  }

  let tailQuestionString = questionGroup.answerString.length > 1 ? 's':''
  let questionString = `Question${tailQuestionString}:`
  _titleRender(questionString)
  _mergeRender(questionGroup, true, false)

  return _paragraph
}

function DragAndDropQuestion(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correctAnswers = (question.correct_answers ?? '').split('#') ?? []
  const images = (question.image ?? '').split('#') || []
  const checkExam = (question.question_text ?? '').split('#') || []
  const checkOG = question?.question_text

  const example: any = {
    images: [],
    answers: [],
    corrects: [],
  }
  const mainQuestion: any = {
    images: [],
    answers: [],
    corrects: [],
  }

  for (let i = 0; i < answers.length; i++) {
    if (answers[i]) {
      if (checkOG === null || checkExam[i] !== '*') {
        if (!images[i]) {
          mainQuestion.images.push('')
          mainQuestion.answers.push(answers[i])
          mainQuestion.corrects.push(correctAnswers[i])
          continue
        }
        mainQuestion.images.push(images[i])
        mainQuestion.answers.push(answers[i])
        mainQuestion.corrects.push(correctAnswers[i])
        continue
      } else {
        if (!images[i]) {
          example.images.push('')
          example.answers.push(answers[i])
          example.corrects.push(correctAnswers[i])
          continue
        }
        example.images.push(images[i])
        example.answers.push(answers[i])
        example.corrects.push(correctAnswers[i])
        continue
      }
    }
  }
  const pQuestionDesArr = question.parent_question_description? `${question.parent_question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: pQuestionDesArr.map((item:string, mIndex:number)=>
      new TextRun({
        text:item,
        bold:true,
        break: mIndex > 0 ? 1 : 0
      })),
    }),
  )

  if (example.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: example.answers.length > 1 ? 'Examples:' : 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.answers.forEach((answer: string, exIndex: number) => {
      const correct = example.corrects[exIndex]
      const imageUrl = example.images[exIndex]
        ? `${uploadDir}/${example.images[exIndex]}`
        : ''

      if (imageUrl) {
        const imageSize = sizeOfImage(imageUrl)
        list.push(new Paragraph(''))
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 5, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: ``,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 20, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.CENTER,
                        children: [
                          new ImageRun({
                            data: fs.readFileSync(imageUrl),
                            transformation: {
                              width: 136,
                              height:
                                (imageSize.height * 136) / imageSize.width,
                            },
                          }),
                          ,
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 75, type: WidthType.PERCENTAGE },
                    // margins: { left: 150 },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        indent: {
                          left: '0.2 in',
                        },
                        text: `Keywords: ${answer?.replace(/\*/g, '/')}`,
                      }),
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        indent: {
                          left: '0.2 in',
                        },
                        text: `→ ${correct
                          ?.replace(/\*/g, ' ')
                          .replace(/\%\/\%/g, '/')}`,
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        )
      } else {
        list.push(new Paragraph(''))
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 5, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.TOP,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: ``,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 95, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.TOP,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        text: `Keywords: ${answer?.replace(/\*/g, '/')}`,
                      }),
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        text: `→ ${correct
                          ?.replace(/\*/g, ' ')
                          .replace(/\%\/\%/g, '/')}`,
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        )
      }
    })
  }

  if (mainQuestion.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: mainQuestion.answers.length > 1 ? 'Questions:' : 'Question:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )

    mainQuestion.answers.forEach((answer: string, aIndex: number) => {
      const imageUrl = mainQuestion.images[aIndex]
        ? `${uploadDir}/${mainQuestion.images[aIndex]}`
        : ''
      if (imageUrl !== '') {
        const imageSize = sizeOfImage(imageUrl)
        list.push(new Paragraph(''))
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 5, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: `${index + aIndex}. `,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 20, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.CENTER,
                        children: [
                          new ImageRun({
                            data: fs.readFileSync(imageUrl),
                            transformation: {
                              width: 136,
                              height:
                                (imageSize.height * 136) / imageSize.width,
                            },
                          }),
                          ,
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 75, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        indent: {
                          left: '0.2 in',
                        },
                        text: `Keywords: ${answer?.replace(/\*/g, '/')}`,
                      }),
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        indent: {
                          left: '0.2 in',
                        },
                        text: `→ .......................................`,
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        )
      } else if (!mainQuestion.images[aIndex]) {
        list.push(new Paragraph(''))
        list.push(
          new Table({
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 5, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.TOP,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: `${index + aIndex}. `,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 95, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.TOP,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        text: `Keywords: ${answer?.replace(/\*/g, '/')}`,
                      }),
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        children: [
                          new TextRun({
                            text: `→ .......................................`,
                          }),
                        ],
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        )
      }
    })
  }

  return list
}

function FillInBlankQuestion1(question: any, index: number, list: any[]) {
  const questionDesArr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: mIndex === questionDesArr.length - 1 ? 200 : 0,
        },
        indent:
          mIndex > 0
            ? {
                left: '0.2 in',
              }
            : {},
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)

  const questionText = question.question_text
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      text: `${index}. `,
      children: RenderTextNewLineAndFontStyle(`${questionText.replaceAll(/\%s\%/g, space)}`, space),
    }),
  )

  list.push(
    new Paragraph({
      spacing: {
        before: 150,
      },
      indent: {
        left: '0.2 in',
      },
      children: [
        new TextRun({
          text: `(${question.answers})`,
          bold: true,
          italics: true,
        }),
      ],
    }),
  )
  return list
}
function FillInBlankQuestion2_3_v2(
  question: any,
  index: number,
  list: any[],
) {
  const correctAnswers = question.correct_answers.split('#')
  const questionDesArr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after:
            question.parent_question_text &&
            mIndex === questionDesArr.length - 1
              ? 200
              : 0,
        },
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)
  if (question.image) {
    const imageSize = sizeOfImage(`${uploadDir}/${question.image}`)
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: (imageSize.width * 136) / imageSize.height,
              height: 136,
            },
          }),
        ],
      }),
    )
  }
  if (question.parent_question_text) {
    ReadingScript(question.parent_question_text, list)
  }
  const questionText = question.question_text
  const totalLength = correctAnswers.length
  const exampleIndex = correctAnswers.findIndex((item: any) => item.startsWith('*'))
  let exCorrect: any = ''
  for (let i = 0; i < totalLength; i++) {
    if(correctAnswers[i]?.startsWith('*')){
      exCorrect = correctAnswers[i].replace(/\*/g, '')
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: RenderTextNewLineAndFontStyleFB(questionText, index, exampleIndex, exCorrect)
    }),
  )
  return list
}

function FillInBlankQuestion6(question: any, index: number, list: any[]) {
  const correctAnswers = question.correct_answers.split('#')
  const questionDesArr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after:
            question.parent_question_text &&
            mIndex === questionDesArr.length - 1
              ? 200
              : 0,
        },
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)
  if (question.image) {
    const imageSize = sizeOfImage(`${uploadDir}/${question.image}`)
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: (imageSize.width * 136) / imageSize.height,
              height: 136,
            },
          }),
        ],
      }),
    )
  }
  if (question.parent_question_text) {
    ReadingScript(question.parent_question_text, list)
  }
  const questionText = question.question_text
  const totalLength = correctAnswers.length
  const exampleIndex = correctAnswers.findIndex((item: any) => item.startsWith('*'))
  let exCorrect: any = ''
  for (let i = 0; i < totalLength; i++) {
    if(correctAnswers[i]?.startsWith('*')){
      exCorrect = correctAnswers[i].replace(/\*/g, '')
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: RenderTextNewLineAndFontStyleFB(questionText, index, exampleIndex, exCorrect)
    }),
  )
  return list
}

function FillInBlankQuestion4_v2(
  question: any,
  index: number,
  list: any[],
) {
  const correctAnswers = question.correct_answers.split('#')
  const questionDesArr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: 100,
        },
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)
  const imageSize = sizeOfImage(`${uploadDir}/${question.image}`)
  const maxWidth = 600;
  const maxHeight = 136;  
  let height = maxHeight;
  let width = (imageSize.width * height) / imageSize.height
  if(width>maxWidth){
    width = maxWidth;
    height = (imageSize.height * width) / imageSize.width
  }
  list.push(
    new Paragraph({
      spacing: { before: 150 },
      alignment: AlignmentType.CENTER,
      children: [
        new ImageRun({
          data: fs.readFileSync(`${uploadDir}/${question.image}`),
          transformation: {
            width: width,
            height: height,
          },
        }),
      ],
    }),
  )
  if (question.parent_question_text) {
    ReadingScript(question.parent_question_text, list)
  }
  const questionText = question.question_text
  const totalLength = correctAnswers.length
  const exampleIndex = correctAnswers.findIndex((item: any) => item.startsWith('*'))
  let exCorrect: any = ''
  for (let i = 0; i < totalLength; i++) {
    if(correctAnswers[i]?.startsWith('*')){
      exCorrect = correctAnswers[i].replace(/\*/g, '')
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: RenderTextNewLineAndFontStyleFB(questionText, index, exampleIndex, exCorrect)
    }),
  )
  return list
}

function FillInBlankQuestion5(question: any, index: number, list: any[]) {
  const question_description_arr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: question_description_arr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  if (question.image) {
    const imageSize = sizeOfImage(`${uploadDir}/${question.image}`)
    const maxWidth = 600;
    const maxHeight = 136;  
    let height = maxHeight;
    let width = (imageSize.width * height) / imageSize.height
    if(width>maxWidth){
      width = maxWidth;
      height = (imageSize.height * width) / imageSize.width
    }
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: width,
              height: height,
            },
          }),
        ],
      }),
    )
  }
  list.push(
    new Paragraph({
      spacing: { before: 150, line: 300 },
      indent: {
        left: '0.2 in',
      },
      children: Array.from(
        { length: question.total_question },
        (_, _index) =>
          new TextRun({
            text: `(${index + _index})___________________`,
            break: _index > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  return list
}

function FillInBlankQuestion7_v2(question: any, index: number, list: any[]) {
  const correctAnswers = question.correct_answers.split('#')
  const questionDesArr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after:
            question.parent_question_text &&
            mIndex === questionDesArr.length - 1
              ? 200
              : 0,
        },
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)
  if (question.image) {
    const imageSize = sizeOfImage(`${uploadDir}/${question.image}`)
    const maxWidth = 600;
    const maxHeight = 136;  
    let height = maxHeight;
    let width = (imageSize.width * height) / imageSize.height
    if(width>maxWidth){
      width = maxWidth;
      height = (imageSize.height * width) / imageSize.width
    }
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: width,
              height: height,
            },
          }),
        ],
      }),
    )
  }
  if (question.parent_question_text) {
    ReadingScript(question.parent_question_text, list)
  }
  const questionText = question.question_text
  const totalLength = correctAnswers.length
  const exampleIndex = correctAnswers.findIndex((item: any) => item.startsWith('*'))
  let exCorrect: any = ''
  for (let i = 0; i < totalLength; i++) {
    if(correctAnswers[i]?.startsWith('*')){
      exCorrect = correctAnswers[i].replace(/\*/g, '')
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: RenderTextNewLineAndFontStyleFB(questionText, index, exampleIndex, exCorrect),
    }),
  )
  return list
}

function FillInBlankQuestion8(question: any, index: number, list: any[]) {
  const imageNote = question?.audio_script || ''
  const image = question?.image
  const listSuggestAnswer =  question?.answers?.split('#') || [] 
  const correctAnswers = question.correct_answers.split('#')
  const listCorrectAnswer:any[]=  correctAnswers.map((item:any) => item.split('%/%')).reduce((a:any, b:any) => [...a, ...b], []) ?? [];

  const mainImage = image.split('*')[0]
  const arrayImg = image.split('*')[1].split('#')
  const arrayImgRaw = arrayImg.map((item: any) => item.split('|')).reduce((a:any, b:any) => [...a, ...b], [])

  const arrayTextString = [...listCorrectAnswer,...listSuggestAnswer]
  const arrayImgEx=[]
  const arrayImgQuestion = []
  const arrayCorrectEx = []
  const arrayCorrectQuestion = []

  const indexExam = listCorrectAnswer.findIndex((m:string)=> m.startsWith("*"))
  if(indexExam != -1){
    arrayImgEx.push(arrayImgRaw[indexExam]);
    arrayCorrectEx.push(arrayTextString[indexExam]);
    arrayImgRaw.splice(indexExam, 1)
    arrayTextString.splice(indexExam, 1)
  }

  let randomIndexes = Array.from({ length: arrayTextString.length }, (v, i) => i)
  randomIndexes = shuffleArray(randomIndexes)
  for (let i = 0; i < randomIndexes.length; i++) {
   const indexRandom = randomIndexes[i];
   arrayImgQuestion.push(arrayImgRaw[indexRandom])
   arrayCorrectQuestion.push(arrayTextString[indexRandom])
  }

  const answerData = [...arrayCorrectEx,...arrayCorrectQuestion]

  const lenCA = correctAnswers.length

  const answerListImage = [...arrayImgEx,...arrayImgQuestion]
  const questionDesArr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: 100,
        },
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)
  const imageSize = sizeOfImage(`${uploadDir}/${mainImage}`)
  const maxWidth = 600;
  const maxHeight = 136;  
  let height = maxHeight;
  let width = (imageSize.width * height) / imageSize.height
  if(width>maxWidth){
    width = maxWidth;
    height = (imageSize.height * width) / imageSize.width
  }
  list.push(
    new Paragraph({
      spacing: { before: 150, after: 0},
      alignment: AlignmentType.CENTER,
      children: [
        new ImageRun({
          data: fs.readFileSync(`${uploadDir}/${mainImage}`),
          transformation: {
            width: width,
            height: height,
          },
        }),
      ],
    }),
  )
  if(imageNote){
    list.push(
      new Paragraph({
        spacing: { before: 50, after: 100},
        alignment: AlignmentType.CENTER,
        children: [
          new TextRun({
            text: imageNote,
            color: '#3c3c3c',
            bold:true
          }),
        ],
      }),
    )
  }
  
  const questionText = question.question_text
  const exampleIndex = correctAnswers.findIndex((item: any) => item.startsWith('*'))
  let exCorrect: any = ''
  for (let i = 0; i < lenCA; i++) {
    if(correctAnswers[i]?.startsWith('*')){
      exCorrect = correctAnswers[i].replace(/\*/g, '')
    }
  }

  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        after: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: RenderTextNewLineAndFontStyleFB(questionText, index, exampleIndex, exCorrect),
    }),
  )

  if (answerListImage.length === 4 || answerListImage.length <= 2){
    list.push(
      new Table({
        margins: { top: 100},
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        alignment: AlignmentType.END,
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children:[
              ...answerListImage.map((answer: string, answerIndex: number)=>{
                if(answerIndex < 2){
                  const imageUrl = `${uploadDir}/${answer}`
                  const imageSize = sizeOfImage(imageUrl)
                  const maxHeight = 136
                  let imgWidth = 136
                  let imgHeight = (imageSize.height * 136) / imageSize.width
                  if (imgHeight > maxHeight) {
                    imgHeight = 136
                    imgWidth = (imageSize.width * 136) / imageSize.height
                  }
                    return new TableCell({
                      width:{size:48, type:WidthType.PERCENTAGE},
                      verticalAlign: VerticalAlign.BOTTOM,
                      children:[
                        new Paragraph({
                          spacing: { before: 150, after: 150 },
                          alignment: AlignmentType.CENTER,
                          children:[
                            new ImageRun({
                              data: fs.readFileSync(imageUrl),
                              transformation:{
                                width:imgWidth,
                                height:imgHeight,
                              },
                            }),
                            new TextRun({break:1}),
                            new TextRun({
                              ...(answerData[answerIndex]?.startsWith('*') 
                              ? {
                                text: `${answerData[answerIndex].replace(/\*/g, '')}`,
                                strike: true,
                                color: trueColor,
                                bold: true,
                                break:1,
                              
                              } 
                              : {
                                text: `${answerData[answerIndex]}`,
                                color: '#3c3c3c',
                                bold: true,
                                break: 1
                              })
                            }),
                          ]
                        })
                      ]
                  })
                }
                
              })
            ]
          }),
          ...(answerListImage.length == 4?[ 
            new TableRow({
              children:[
                ...answerListImage.map((answer:string,answerIndex: number)=>{
                  if(answerIndex > 1){
                    const imageUrl = `${uploadDir}/${answer}`
                    const imageSize = sizeOfImage(imageUrl)
                    const maxHeight = 136
                    let imgWidth = 136
                    let imgHeight = (imageSize.height * 136) / imageSize.width
                    if (imgHeight > maxHeight) {
                      imgHeight = 136
                      imgWidth = (imageSize.width * 136) / imageSize.height
                    }
                    return  new TableCell({
                      width:{size:48, type:WidthType.PERCENTAGE},
                      verticalAlign: VerticalAlign.BOTTOM,
                      children:[
                        new Paragraph({
                          spacing: { before: 150, after: 150 },
                          alignment: AlignmentType.CENTER,
                          children:[
                            new ImageRun({
                              data: fs.readFileSync(imageUrl),
                              transformation:{
                                width:imgWidth,
                                height:imgHeight,
                              }
                            }),
                            new TextRun({break:1}),
                            new TextRun({
                              ...(answerData[answerIndex]?.startsWith('*') 
                              ? {
                                text: `${answerData[answerIndex].replace(/\*/g, '')}`,
                                strike: true,
                                color: trueColor,
                                bold: true,
                                break:1
                              } 
                              : {
                                text: `${answerData[answerIndex]}`,
                                color: '#3c3c3c',
                                bold: true,
                                break:1
                              })
                            }),
                          ]
                        })
                      ]
                  })
                  }
                })
              ]
            }),
          ]:[])
          
        ]
      })
    )
  }
  else{
    const columnWidth = Math.round(100.0 / Math.max(3))
    const maxRow = Math.ceil(answerListImage.length / 3)
    const tableRowArr = []
    for(let i =0; i< maxRow; i++){
      tableRowArr.push(i)
    }
    const tableRow:any[] = []
    tableRowArr.forEach((indexRow:number)=>{
      tableRow.push(
        new TableRow({
          children:[
            ...["1","2","3"].map((temp:string, answerIndex: number)=>{
              const indexImage = indexRow*3 + answerIndex;
              // if((answerIndex + 1 >= indexRow * 3 + 1) && (answerIndex + 1 <= indexRow * 3 + 3)){
              if(indexImage < answerListImage.length){
                const answer = answerListImage[indexImage];
                const imageUrl = `${uploadDir}/${answer}`
                const imageSize = sizeOfImage(imageUrl)
                const maxHeight = 136
                let imgWidth = 136
                let imgHeight = (imageSize.height * 136) / imageSize.width
                if (imgHeight > maxHeight) {
                  imgHeight = 136
                  imgWidth = (imageSize.width * 136) / imageSize.height
                }
                return new TableCell({
                  width:{size:columnWidth, type:WidthType.PERCENTAGE},
                  verticalAlign: VerticalAlign.BOTTOM,
                  children:[
                    new Paragraph({
                      spacing: { before: 150, after: 150 },
                      alignment: AlignmentType.CENTER,
                      children:[
                        new ImageRun({
                          data: fs.readFileSync(imageUrl),
                          transformation:{
                            width:imgWidth,
                            height:imgHeight,
                          }
                        }),
                        new TextRun({break:1}),
                        new TextRun({
                          ...(answerData[indexImage]?.startsWith('*') 
                          ? {
                            text: `${answerData[indexImage].replace(/\*/g, '')}`,
                            strike: true,
                            color: trueColor,
                            bold: true,
                            break:1
                          } 
                          : {
                            text: `${answerData[indexImage]}`,
                            color: '#3c3c3c',
                            bold: true,
                            break:1
                          })
                        }),
                      ]
                    })
                  ]
                })
              }else{
                return new TableCell({
                  width:{size:columnWidth, type:WidthType.PERCENTAGE},
                  verticalAlign: VerticalAlign.BOTTOM,
                  children:[
                    new Paragraph(""),
                  ]
                })
              }
            })
          ]
        })
      )
    })

    list.push(
      new Table({
        margins: { top: 100},
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#000000' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        alignment: AlignmentType.END,
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [...tableRow]
      })
    )
  }

  return list
}

function LikertScaleQuestion(question: any, index: number, list: any[]) {
  const listAnswer: string[] = question.answers.split('#')
  const question_description_arr = question.question_description ? `${question.question_description} ${question.total_question > 1 ? `(${question.total_question} questions)`:""}`.split('\n') : []
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: question_description_arr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  if (question.question_text) {
    ReadingScript(question.question_text, list)
  }
  list.push(new Paragraph({ text: '' }))
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          tableHeader: true,
          height: { value: 600, rule: HeightRule.ATLEAST },
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Answers', bold: true })],
                  indent: { start: 200 },
                }),
              ],
              width: { size: 70, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'True', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'False', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Not given', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
          ],
        }),
        ...listAnswer.map(
          (answer: string, aIndex: number) =>
            new TableRow({
              height: { value: 500, rule: HeightRule.ATLEAST },
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      text: `${index + aIndex}. `,
                      children: RenderTextNewLineAndFontStyle(answer),
                      indent: { start: 200 },
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
              ],
            }),
        ),
      ],
    }),
  )
  return list
}

function MatchingGameQuestion(question: any, index: number, list: any[]) {
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length)
 const question_description_arr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: question_description_arr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  if (question.question_text) {
    ReadingScript(question.question_text, list)
    // has table front and back
    list.push(new Paragraph({children:[]}))
  }
  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: Array.from(
        { length: maxLength },
        (_, _index) =>
          new TableRow({
            children: [
              new TableCell({
                width: { size: 50, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph(
                      {
                        children: listAnswer[0][_index] ? [
                          new TextRun(`${index + _index}. `),
                          ...getArrTextRunFromArrTextStyleObject(formatTextStyleObject(listAnswer[0][_index])),
                        ] : [
                            new TextRun('')
                        ],
                      }
                  ),
                ],
              }),
              new TableCell({
                width: { size: 50, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph(
                      {
                        children: listAnswer[1][_index] ? [
                          new TextRun(`${String.fromCharCode(97 + _index)}. `),
                          ...getArrTextRunFromArrTextStyleObject(formatTextStyleObject(listAnswer[1][_index])),
                        ] : [
                          new TextRun('')
                        ],
                      }
                  ),
                ],
              }),
            ],
          }),
      ),
    }),
  )

  return list
}

function ShortAnswerQuestion(question: any, index: number, list: any[]) {
  const questionDesArr = question.question_description?.split('\n')
  if (!questionDesArr) return list
  const questionDesParagraph = questionDesArr?.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: mIndex === questionDesArr.length - 1 ? 200 : 0,
        },
        indent: mIndex > 0 && {
          left: '0.15 in',
        },
        children: [
          new TextRun({
            text: item,
            bold: true,
          }),
        ],
      })
    },
  )
  if (questionDesParagraph) {
    list.push(...questionDesParagraph)
  }

  const questionText = question.question_text
  const questionTextRun = new Paragraph({
    spacing: {
      before: 150
    },
    indent: {
      left: '0.15 in'
    },
    children: RenderTextNewLineAndFontStyle(`${index}. ${questionText.replaceAll(/\%s\%/g, space)}`, space)
  })   
  list.push(questionTextRun)

  const answerStr = question.answers??""
  const questionAnswerArr = answerStr?.split('\n')
  const questionAnswerParagraph = questionAnswerArr?.map(
    (item: string, mIndex: number) => {
      if (mIndex === 0) {
        item = `→ ${item}`
      }
      if (mIndex === questionAnswerArr.length - 1) {
        if(!answerStr.includes("%s%")){
          item = `${item}___________`
        }
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 150 : 0,
          after: mIndex === questionAnswerArr.length - 1 ? 150 : 0,
        },
        indent: mIndex > 0 && {
          left: '0.15 in',
        },
        text: item.replaceAll(/\%s\%/g, '___________'),
      })
    },
  )
  if (questionAnswerParagraph) {
    list.push(...questionAnswerParagraph)
  }

  return list
}

function TrueFalseQuestion(question: any, index: number, list: any[]) {
  const listAnswer: string[] = question.answers.split('#')
  const question_description_arr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: question_description_arr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  if (question.question_text) {
    ReadingScript(question.question_text, list)
  }
  list.push(new Paragraph({ text: '' }))
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          tableHeader: true,
          height: { value: 600, rule: HeightRule.ATLEAST },
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Answers', bold: true })],
                  indent: { start: 200 },
                }),
              ],
              width: { size: 80, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'True', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'False', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
          ],
        }),
        ...listAnswer.map(
          (answer: string, aIndex: number) =>
            new TableRow({
              height: { value: 500, rule: HeightRule.ATLEAST },
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      text: `${index + aIndex}. `,
                      children: RenderTextNewLineAndFontStyle(answer),
                      indent: { start: 200 },
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
              ],
            }),
        ),
      ],
    }),
  )
  return list
}
function TrueFalseQuestion5( question: any, index: number, _export: any[] ){
  const answers = question.answers.split('#')
  const corrects = question.correct_answers.split('#')
  const images = question.image.split('#')
  const example: any = { answers: [], corrects: [], images: [] }
  const questions: any = { answers: [], corrects: [], images: [] }
  // Separate questions and example
  for( let _index = 0; _index < corrects.length; _index++ ){
    // Example of question
    if( answers[_index].startsWith('*') ){
      example.corrects.push(corrects[_index])
      example.answers.push(answers[_index].replace('*',''))
      example.images.push(images[_index])
    }else{
      // Question list
      questions.corrects.push(corrects[_index])
      questions.answers.push(answers[_index])
      questions.images.push(images[_index])
    }
  }
  // Push description and text question to layout 
  const title = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  
  _export.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: title.map((item:string, mIndex:number)=>
      new TextRun({
        text:item,
        bold:true,
        break: mIndex > 0 ? 1 : 0
      })),
    }),
  )
  // Style table
  const _borderStyle = { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' }
  const _tableBorders = { 
    top: _borderStyle, left: _borderStyle, right: _borderStyle, bottom: _borderStyle, 
    insideVertical: _borderStyle, insideHorizontal: _borderStyle }

  // Push example to layout if have
  if( example.answers.length ){
    // Heading Example
    _export.push(
      new Paragraph({
        spacing: { before: 200 },
        children: [
          new TextRun({ text: 'Example:', bold: true, underline: { type: UnderlineType.SINGLE } })
        ]
      }))
    example.answers.forEach((value: string, _index: number)=>{
      const correct = example.corrects[_index]
      const image_url = `${uploadDir}/${example.images[_index]}`
      const image_size = sizeOfImage(image_url)
      // Style text
      const _textLetterTrue = { text: correct == 'T' ? '☑' : '☐', color: '#9cabbd', bold: true }
      const _textTrue = { text: ' True        ', color: trueColor, bold: true }
      const _textLetterFalse = { text: correct == 'F' ? '☑' : '☐', color: '#9cabbd', bold: true}
      const _textFalse = {  text: ' False  ', color: falseColor, bold: true, }
  
      _export.push(
        new Table({
          margins: { top: 200 },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE},
          borders: _tableBorders,
          rows: [
            new TableRow({children: [
              new TableCell({
                width: { size: 3, type: WidthType.PERCENTAGE },
                verticalAlign: VerticalAlign.CENTER,
                children: [new Paragraph({
                  alignment: AlignmentType.CENTER,
                  children: [new TextRun({ text: '' })]
                })]
              }),
              new TableCell({
                width: { size: 12, type: WidthType.PERCENTAGE },
                children: [new Paragraph({
                  alignment: AlignmentType.LEFT,
                  children: [new ImageRun({
                    data: fs.readFileSync(image_url),
                    transformation: {
                      width: 136,
                      height: (image_size.height * 136) / image_size.width
                    }
                  })]
                })]
              }),
              new TableCell({
                width: { size: 38, type: WidthType.PERCENTAGE },
                margins: { left: 150 },
                verticalAlign: VerticalAlign.CENTER,
                children: [new Paragraph({
                  children: RenderTextNewLineAndFontStyle(value),
                })]
              }),
              new TableCell({
                width: { size: 20, type: WidthType.PERCENTAGE },
                verticalAlign: VerticalAlign.CENTER,
                children: [new Paragraph({
                  alignment: AlignmentType.END,
                  children: [
                    new TextRun(_textLetterTrue),
                    new TextRun(_textTrue),
                    new TextRun(_textLetterFalse),
                    new TextRun(_textFalse)]
                })]
              }),
            ]})]
      }))
    })
  }
  
  // Push question to layout if have
  if (questions.answers.length) {
    _export.push(
      new Paragraph({
        spacing: { before: 200 },
        children: [new TextRun({ text: questions.answers.length === 1 ? 'Question' : 'Questions:', bold: true, underline: { type: UnderlineType.SINGLE } })]
      }))
    questions.answers.forEach((value: string, _index: number)=>{
      const image_url = `${uploadDir}/${questions.images[_index]}`
      const image_size = sizeOfImage(image_url)
      // Style text
      const _textTrue = { text: ' True        ', color: trueColor, bold: true }
      const _textFalse = {  text: ' False  ', color: falseColor, bold: true, }
      const _textEmpty = { text: '☐', color: '#9cabbd', bold: true, }
  
      _export.push(
        new Table({
          margins: { top: 200 },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          borders: _tableBorders,
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 3, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [new Paragraph({
                    alignment: AlignmentType.CENTER,
                    children: [new TextRun({ text: `${index + _index}.` })]
                  })]
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE},
                  verticalAlign: VerticalAlign.CENTER,
                  children: [new Paragraph({
                    alignment: AlignmentType.LEFT,
                    children: [new ImageRun({
                      data: fs.readFileSync(image_url),
                      transformation: {
                        width: 136,
                        height: (image_size.height * 136) / image_size.width
                      }
                    })]
                  })]
                }),
                new TableCell({
                  width: { size: 38, type: WidthType.PERCENTAGE },
                  margins: { left: 150 },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [new Paragraph({
                    children: RenderTextNewLineAndFontStyle(value),
                  })]
                }),
                new TableCell({
                  width: { size: 20, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [new Paragraph({
                    alignment: AlignmentType.END,
                    children: [
                      new TextRun( _textEmpty ),
                      new TextRun( _textTrue ),
                      new TextRun( _textEmpty ),
                      new TextRun( _textFalse )]
                  })]
                })
              ]})]
        }))
    })
  }
  return _export
}
function TrueFalseQuestion4(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correcrAnswers = question.correct_answers.split('#')
  const images = question.image.split('#')

  const example: any = {
    answers: [],
    corrects: [],
    images: [],
  }
  const mainQuestion: any = {
    answers: [],
    corrects: [],
    images: [],
  }

  //cup example
  for (let i = 0; i < correcrAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        mainQuestion.corrects.push(correcrAnswers[i])
        mainQuestion.answers.push(answers[i])
        mainQuestion.images.push(images[i])
        continue
      } else {
        example.corrects.push(correcrAnswers[i])
        example.answers.push(answers[i].replace('*', ''))
        example.images.push(images[i])
        continue
      }
    }
  }
  const question_description_arr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: question_description_arr.map((item:string, mIndex:number)=>
      new TextRun({
        text:item,
        bold:true,
        break: mIndex > 0 ? 1 : 0
      })),
    }),
  )

  if (example.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: example.answers.length > 1 ? 'Examples:' : 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.answers.forEach((answer: string, aIndex: number) => {
      const correct = example.corrects[aIndex]
      const imageUrl = `${uploadDir}/${example.images[aIndex]}`

      const imageSize = sizeOfImage(imageUrl)
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 3, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.CENTER,
                      children: [
                        new TextRun({
                          text: ``,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.LEFT,
                      children: [
                        new ImageRun({
                          data: fs.readFileSync(imageUrl),
                          transformation: {
                            width: 136,
                            height: (imageSize.height * 136) / imageSize.width,
                          },
                        }),
                        ,
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 38, type: WidthType.PERCENTAGE },
                  margins: { left: 150 },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          children: RenderTextNewLineAndFontStyle(answer)
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 20, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.END,
                      children: [
                        new TextRun({
                          text: correct == 'T' ? '☑' : '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' True        ',
                          color: trueColor,
                          bold: true,
                        }),
                        new TextRun({
                          text: correct == 'F' ? '☑' : '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' False  ',
                          color: falseColor,
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  if (mainQuestion.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: mainQuestion.answers.length > 1 ? 'Questions:' : 'Question:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    mainQuestion.answers.forEach((answer: string, aIndex: number) => {
      const imageUrl = `${uploadDir}/${mainQuestion.images[aIndex]}`

      const imageSize = sizeOfImage(imageUrl)
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 3, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.CENTER,
                      children: [
                        new TextRun({
                          text: `${index + aIndex}. `,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.LEFT,
                      children: [
                        new ImageRun({
                          data: fs.readFileSync(imageUrl),
                          transformation: {
                            width: 136,
                            height: (imageSize.height * 136) / imageSize.width,
                          },
                        }),
                        ,
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 38, type: WidthType.PERCENTAGE },
                  margins: { left: 150 },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          children: RenderTextNewLineAndFontStyle(answer)
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 20, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.END,
                      children: [
                        new TextRun({
                          text: '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' True        ',
                          color: trueColor,
                          bold: true,
                        }),
                        new TextRun({
                          text: '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' False   ',
                          color: falseColor,
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
    })
  }

  return list
}
function TrueFalseQuestion3(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correcrAnswers = question.correct_answers.split('#')
  const image = question.image
  const imageUrl = `${uploadDir}/${image}`
  const example: any = {
    answers: [],
    corrects: [],
  }
  const mainQuestion: any = {
    answers: [],
    corrects: [],
  }

  //cup example
  for (let i = 0; i < correcrAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        mainQuestion.corrects.push(correcrAnswers[i])
        mainQuestion.answers.push(answers[i])
        continue
      } else {
        example.corrects.push(correcrAnswers[i])
        example.answers.push(answers[i].replace('*', ''))
        continue
      }
    }
  }
  const question_description_arr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: question_description_arr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  const imageSize = sizeOfImage(imageUrl)
  const maxWidth = 600;
  const maxHeight = 136;  
  let height = maxHeight;
  let width = (imageSize.width * height) / imageSize.height
  if(width>maxWidth){
    width = maxWidth;
    height = (imageSize.height * width) / imageSize.width
  }
  list.push(
    new Paragraph({
      spacing: { before: 200, after: 200 },
      alignment: AlignmentType.CENTER,
      children: [
        new ImageRun({
          data: fs.readFileSync(imageUrl),
          transformation: {
            width: width,
            height: height,
          },
        }),
      ],
    }),
  )
  if (example.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: example.answers.length > 1 ? 'Examples:' : 'Example:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.answers.forEach((answer: string, aIndex: number) => {
      const correct = example.corrects[aIndex]
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 50, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          children: RenderTextNewLineAndFontStyle(answer),
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.END,
                      children: [
                        new TextRun({
                          text: correct == 'T' ? '☑' : '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' True        ',
                          color: trueColor,
                          bold: true,
                        }),
                        new TextRun({
                          text: correct == 'F' ? '☑' : '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' False  ',
                          color: falseColor,
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  if (mainQuestion.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: mainQuestion.answers.length > 1 ? 'Questions:' : 'Question:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    mainQuestion.answers.forEach((answer: string, aIndex: number) => {
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 50, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun(`${index + aIndex}. `),
                        ...getArrTextRunFromArrTextStyleObject(formatTextStyleObject(answer))
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.END,
                      children: [
                        new TextRun({
                          text: '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' True        ',
                          color: trueColor,
                          bold: true,
                        }),
                        new TextRun({
                          text: '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' False   ',
                          color: falseColor,
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
    })
  }

  return list
}
function MultiResponseQuestion1(question: any, index: number, list: any[]) {
  const answers: string[] = question.answers.split('#')
  const listAnswers: string[][] = []
  while (answers.length > 0) {
    listAnswers.push(answers.splice(0, 4))
  }
  const questionDesArr = question.question_description?.split('\n') || []
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      if (mIndex === 0) {
        item = `${index}. ${item}`
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 150 : 0,
          after: mIndex === questionDesArr.length - 1 ? 150 : 0,
        },
        indent: mIndex > 0 && {
          left: '0.15 in',
        },
        children: [
          new TextRun({
            text: item,
            bold: true,
          }),
        ],
      })
    },
  )
  list.push(...questionDesParagraph)

  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: listAnswers.map(
        (items, itemsIndex) =>
          new TableRow({
            children: items.map(
              (item, itemIndex) =>
                new TableCell({
                  width: { size: 25, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      text: `${String.fromCharCode(
                        65 + itemsIndex * 4 + itemIndex,
                      )}. `,
                      children: RenderTextNewLineAndFontStyle(item),
                      indent: { left: '0.1 in' },
                    }),
                  ],
                }),
            ),
          }),
      ),
    }),
  )

  return list
}

function MultiResponseQuestion3(question: any, index: number, list: any[]) {
  const answers: string[] = question.answers.split('#')
  const questionDesArr = question.question_description?.split('\n') || []
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      if (mIndex === 0) {
        item = `${index}. ${item}`
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: mIndex === questionDesArr.length - 1 ? 150 : 0,
        },
        indent: mIndex > 0 && {
          left: '0.15 in',
        },
        children: [
          new TextRun({
            text: item,
            bold: true,
          }),
        ],
      })
    },
  )
  list.push(...questionDesParagraph)

  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: answers.map(
        (answer: string, answerIndex: number) =>
          new TableRow({
            children: [
              new TableCell({
                width: { size: 100, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph({
                    indent: { left: '0.2 in' },
                    text: `${String.fromCharCode(65 + answerIndex)}. `,
                    children: RenderTextNewLineAndFontStyle(answer),
                  }),
                ],
              }),
            ],
          }),
      ),
    }),
  )

  return list
}

function SelectFromListQuestion1(question: any, index: number, list: any[]) {
  const listAnswers: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))

  const listCorrect = question.correct_answers.split("#")||[]
  const lenCorrect = listCorrect.length;
  const columnWidth =
    90.0 / Math.max(...listAnswers.map((m: string[]) => m.length))
    const questionDesArr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.parent_question_text ? 200 : 0,
      },
      children: questionDesArr.map(
        (item: string, m: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: m > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  const questionText = question.question_text
  const exampleIndex = listCorrect.findIndex((item: any) => item.startsWith('*'))
  let exCorrect: any = ''
  for (let i = 0; i < listAnswers.length; i++) {
    if (listCorrect[i]?.startsWith('*')) {
      exCorrect = listCorrect[i].replace(/\*/g, '')
    }
  }

  if(exampleIndex != -1) {
    listAnswers.splice(exampleIndex, 1)
  }

  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: RenderTextNewLineAndFontStyleFB(questionText, index, exampleIndex, exCorrect),
    }),
  )
  
  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: listAnswers.map(
        (answers, answersIndex) =>
          new TableRow({
            children: [
              new TableCell({
                children: [
                  new Paragraph({
                    text: `(${index + answersIndex})`,
                    indent: { start: '0.2 in' },
                  }),
                ],
                width: { size: 10, type: WidthType.PERCENTAGE },
              }),
              ...answers.map(
                (answer, answerIndex) =>
                  new TableCell({
                    width: { size: columnWidth, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        text: `${String.fromCharCode(
                          65 + answerIndex,
                        )}. ${answer}`,
                      }),
                    ],
                  }),
              ),
            ],
          }),
      ),
    }),
  )

  return list
}

function SelectFromListQuestion2(question: any, index: number, list: any[]) {
  const listImage = question.image.split('#')
  const listQuestion: string[] = question.question_text.split('#')
  const listAnswers = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const columnWidth = 90.0 / Math.max(...listAnswers.map((m: any) => m.length))
  const questionDesArr = question.question_description? `${question.question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.parent_question_text ? 200 : 0,
      },
      children: questionDesArr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  const maxWidth = 600;
  const maxHeight = 136;
 
  listQuestion.forEach((ques, qIndex) => {
    const imageSize = sizeOfImage(`${uploadDir}/${listImage[qIndex]}`)
    let height = maxHeight;
    let width = (imageSize.width * height) / imageSize.height
    if(width>maxWidth){
      width = maxWidth;
      height = (imageSize.height * width) / imageSize.width
    }
    list.push(
      new Paragraph({
        spacing: { before: 200, after: 200 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${listImage[qIndex]}`),
            transformation: {
              width: width,
              height: height,
            },
          }),
        ],
      }),
    )
    const answers = listAnswers[qIndex]

    list.push(
      new Paragraph({
        spacing: {
          before: 150,
          line: 300,
        },
        indent: {
          left: '0.2 in',
        },
        children: RenderTextNewLineAndFontStyleFB(ques, index + qIndex , -1, ''),
      }),
    )
    list.push(
      new Table({
        alignment: AlignmentType.START,
        borders: {
          top: { style: BorderStyle.SINGLE, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, color: '#ffffff' },
          insideHorizontal: { style: BorderStyle.SINGLE, color: '#ffffff' },
          insideVertical: { style: BorderStyle.SINGLE, color: '#ffffff' },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: answers.map(
              (answer: string, aIndex: number) =>
                new TableCell({
                  width: { size: columnWidth, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      spacing: {
                        before: 150,
                        after: 200,
                      },
                      indent: { start: '0.2 in' },
                      text: `${String.fromCharCode(65 + aIndex)}. ${answer}`,
                    }),
                  ],
                }),
            ),
          }),
        ],
      }),
    )
  })

  return list
}

function MIXQuestion1(question: any, index: number, list: any[]) {
  const listAnswers: string[] = question.answers.split('[]')
  const tfAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction = question.question_description.split('[]')
  const mcQuestions = question.question_text.split('[]')[1].split('#')

  const columnWidth =
    100.0 / Math.max(...mcAnswers.map((m: string) => m.split('*').length))
    const pQuestionDesArr = question.parent_question_description? `${question.parent_question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 200,
      },
      children: pQuestionDesArr.map((item: string, mIndex: number) => {
          return new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          })
        }),
    }),
  )
  ReadingScript(question.parent_question_text, list)
  const task1Arr = listInstruction[0].split('\n')
  const task1Paragraph = task1Arr.map((item: string, mIndex: number) => {
    if (mIndex === 0) {
      item = `Task 1: ${item}`
    }
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === task1Arr.length - 1 ? 150 : 0,
      },
      indent: mIndex > 0 && { left: '0.45 in' },
      children: [
        new TextRun({
          text: item,
          bold: true,
        }),
      ],
    })
  })
  list.push(...task1Paragraph)
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          tableHeader: true,
          height: { value: 600, rule: HeightRule.ATLEAST },
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Answers', bold: true })],
                  indent: { start: 200 },
                }),
              ],
              width: { size: 80, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'True', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'False', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
          ],
        }),
        ...tfAnswers.map(
          (answer: string, aIndex: number) =>
            new TableRow({
              height: { value: 500, rule: HeightRule.ATLEAST },
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      text: `${index + aIndex}. `,
                      children: RenderTextNewLineAndFontStyle(answer),
                      indent: { start: 200 },
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
              ],
            }),
        ),
      ],
    }),
  )

  const task2Arr = listInstruction[1].split('\n')
  const task2Paragraph = task2Arr.map((item: string, mIndex: number) => {
    if (mIndex === 0) {
      item = `Task 2: ${item}`
    }
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === task2Arr.length - 1 ? 150 : 0,
      },
      indent: mIndex > 0 && { left: '0.45 in' },
      children: [
        new TextRun({
          text: item,
          bold: true,
        }),
      ],
    })
  })
  list.push(...task2Paragraph)

  mcQuestions.forEach((questionT: string, qIndex: number) => {
    const answers = mcAnswers[qIndex].split('*')
    .map((item:string)=> formatTextStyleObject(item))
    const questionTextRun = new Paragraph({
      spacing:{before:150},
      indent: { left: '0.2 in' },
      children: RenderTextNewLineAndFontStyle(`${index + tfAnswers.length + qIndex}. ${questionT.replaceAll(/\%s\%/g,space)}`,space)
    })   
    list.push(questionTextRun)
    list.push(
      new Table({
        margins: { top: 200 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: answers.map(
          (answer: any, answerIndex: number) =>
            new TableRow({
              children: [
                new TableCell({
                  width: { size: columnWidth, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      indent: { left: '0.2 in' },
                      children:[ 
                        new TextRun(
                          `${String.fromCharCode(65 + answerIndex)}. `,
                          ),
                        ...getArrTextRunFromArrTextStyleObject(answer),
                    ],
                    }),
                  ],
                }),
              ],
            }),
        ),
      }),
    )
  })

  return list
}

function MIXQuestion2(question: any, index: number, list: any[]) {
  const listAnswers: string[] = question.answers.split('[]')
  const lsAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction = question.question_description.split('[]')
  const mcQuestions = question.question_text.split('[]')[1].split('#')

  const columnWidth =
    100.0 / Math.max(...mcAnswers.map((m: string) => m.split('*').length))
    const pQuestionDesArr = question.parent_question_description? `${question.parent_question_description} ${question.total_question>1?`(${question.total_question} questions)`:""}`.split('\n') :[]
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 200,
      },
      children: pQuestionDesArr.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  ReadingScript(question.parent_question_text, list)
  const task1Arr = listInstruction[0].split('\n')
  const task1Paragraph = task1Arr.map((item: string, mIndex: number) => {
    if (mIndex === 0) {
      item = `Task 1: ${item}`
    }
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === task1Arr.length - 1 ? 150 : 0,
      },
      indent: mIndex > 0 && { left: '0.45 in' },
      children: [
        new TextRun({
          text: item,
          bold: true,
        }),
      ],
    })
  })
  list.push(...task1Paragraph)
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          tableHeader: true,
          height: { value: 600, rule: HeightRule.ATLEAST },
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Answers', bold: true })],
                  indent: { start: 200 },
                }),
              ],
              width: { size: 70, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'True', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'False', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Not given', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
          ],
        }),
        ...lsAnswers.map(
          (answer: string, aIndex: number) =>
            new TableRow({
              height: { value: 500, rule: HeightRule.ATLEAST },
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      text: `${index + aIndex}. `,
                      children: RenderTextNewLineAndFontStyle(answer),
                      indent: { start: 200 },
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
              ],
            }),
        ),
      ],
    }),
  )

  const task2Arr = listInstruction[1].split('\n')
  const task2Paragraph = task2Arr.map((item: string, mIndex: number) => {
    if (mIndex === 0) {
      item = `Task 2: ${item}`
    }
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === task2Arr.length - 1 ? 150 : 0,
      },
      indent: mIndex > 0 && { left: '0.45 in' },
      children: [
        new TextRun({
          text: item,
          bold: true,
        }),
      ],
    })
  })
  list.push(...task2Paragraph)

  mcQuestions.forEach((questionT: string, qIndex: number) => {
    const answers = mcAnswers[qIndex].split('*')
    .map((item:string)=> formatTextStyleObject(item))
    const questionTextRun = new Paragraph({
      spacing:{before:150},
      indent: { left: '0.2 in' },
      children: RenderTextNewLineAndFontStyle(`${index + lsAnswers.length + qIndex}. ${questionT.replaceAll(/\%s\%/g,space)}`,space)
    })   
    list.push(questionTextRun)
    list.push(
      new Table({
        margins: { top: 200 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: answers.map(
          (answer: any, answerIndex: number) =>
            new TableRow({
              children: [
                new TableCell({
                  width: { size: columnWidth, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      indent: { left: '0.2 in' },
                      children:[ 
                        new TextRun(
                          `${String.fromCharCode(65 + answerIndex)}. `,
                          ),
                        ...getArrTextRunFromArrTextStyleObject(answer),
                    ],
                    }),
                  ],
                }),
              ],
            }),
        ),
      }),
    )
  })

  return list
}
