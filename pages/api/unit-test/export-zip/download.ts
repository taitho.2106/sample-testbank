import fs from 'fs'

import JSZip from 'jszip'
import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { USER_ROLES } from '@/interfaces/constants'
import { query } from 'lib/db'
import { userInRight } from 'utils'
import { getFileExtension } from 'utils/string'
import { uploadDir } from '@/constants/index'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  if (!session) {
    return res.status(403).end('Forbidden')
  }
  
  const user: any = session?.user
  const isSystem = userInRight([USER_ROLES.Operator], session)
  switch (req.method) {
    case 'GET':
      return getFileById()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getFileById() {
    const { id} = req.query
    if (!id) return res.status(400).json({code:0, message: '`id` required' })
    try {
      try {
        let queryStr = `SELECT * FROM unit_test_export WHERE  id = ? and deleted = 0 `
        const queryParams = [id]
        if(isSystem){
          queryStr += ' and scope = 0 '
        }else{
          queryStr += ' and created_by = ?'
          queryParams.push(user.id)
        }
        const results: any = await query<any>(
            queryStr,
            queryParams,
        )
        if(!results || results.length==0){
          return res.status(400).json({code:0, message: "No data" })
        }
        const folderPath:string = results[0].link_file;
        if(folderPath.endsWith(".zip")){
          if (!fs.existsSync(folderPath)) {
            return res.status(400).json({code:0, message: "Không tìm thấy file" })
          }
          const fileName = folderPath.replace(/^.*[\\\/]/, '');
          const fileNameEncode = encodeURI(fileName).replace(/,/g, ' ')
            res.writeHead(200, {
              // 'Content-Type': 'application/octet-stream',
              'Content-Type': 'application/json',
              'Content-Disposition': `attachment; filename=${fileNameEncode}`,
            })
            const readStream = fs.createReadStream(folderPath);
            readStream.pipe(res);
          return;
        } else{
          const files = await listFiles(folderPath);
          const fileUnitTestDocx = files.find(m => !m.startsWith("The Answers") && !m.startsWith("Audio Script"))
          const zip = new JSZip()
          files.forEach(fileName => {
            const path = `${folderPath}/${fileName}`;
            const itemBff =  fs.readFileSync(path);
            zip.file(fileName, itemBff)
          })
          const audio_str_file:string = results[0].audio_file;
          if(audio_str_file){
            const listAudio = audio_str_file.split("*").map(m=>{
              const items = m.split(":")
              return {
                path:items[0],
                index:items[1]
              }
            })
            if(listAudio.length > 0){
              const audioFolderZip = zip.folder('Audio')
              listAudio.forEach(itemAudio=>{
                const fileExt = getFileExtension(itemAudio.path)
                const fileNameAudio = `Question Number ${itemAudio.index}${fileExt}`
                const audioBff = fs.readFileSync(
                  `${uploadDir}/${itemAudio.path}`,
                )
                audioFolderZip.file(fileNameAudio, audioBff)
              })
            }
          }
          const fileZip = fileUnitTestDocx.replace(".docx","").replace(/^.*[\\\/]/, '');
         const fileNameEncode = encodeURIComponent(fileZip).replace(/,/g, ' ') 
          res.writeHead(200, {
            'Content-Type': 'application/octet-stream',
            'Content-Disposition': "attachment; filename*=UTF-8''"+fileNameEncode+".zip"+"",
          })
          zip.generateNodeStream().pipe(res)
        }
      } catch (error: any) {
        console.log("download export- 111------",error)
        let message = error?.message;
        if(message && message.startsWith("ENOENT: no such file or directory")){
          message = "No such file or directory..."
        }
        return res.status(500).json({code:0, message: message })
      }
    } catch (e: any) {
      console.log("download export- getFileById------",e)
      return res.status(500).json({code:0, message: e.message })
    }
  }
}

const listFiles = async (directory: string) => {
  const dirents = await fs.readdirSync(directory, { withFileTypes: true })
  return dirents
    .filter((dirent) => dirent.isFile())
    .map((dirent) => dirent.name)
}
export default handler
