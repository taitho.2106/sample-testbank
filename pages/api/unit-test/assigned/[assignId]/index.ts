import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';
import dayjs from "dayjs";

import { classUnitTestSelect } from "dto/entitiesSelect";
import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import prisma from 'lib/prisma';
import { mapClassUnitTestToDTO } from "dto/unitTest";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            assignId: yup.number(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session) => {
            const teacher: any = session.user;
            const { assignId } = req.query;

            const result = await prisma.classUnitTest.findUnique({
                where: {
                    id: +assignId,
                },
                select: {
                    ...classUnitTestSelect,
                    class: {
                        select: {
                            id: true,
                            name: true,
                            owner_id: true,
                        },
                    },
                }
            });

            if (result && teacher.id !== result?.class?.owner_id) {
                return res.status(403).json(errorResponseDTO(403, 'Not owner assign!'));
            }

            return res.status(200).json(apiResponseDTO(true, mapClassUnitTestToDTO(result, dayjs())));
        }
    },
});

export default handler