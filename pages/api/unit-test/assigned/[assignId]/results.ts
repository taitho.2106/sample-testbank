import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import { commonStatus } from "@/constants/index";
import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            name: yup.string(),
            assignId: yup.number(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher: any = session.user;
            const { assignId, name }: any = req.query;

            const unitTestAssigned = await prisma.classUnitTest.findFirst({
                where: {
                    status: commonStatus.ACTIVE,
                    created_by: teacher.id,
                    id: +assignId,
                },
                select: {
                    class: {
                        select: {
                            id: true,
                            name: true,
                        }
                    },
                    group: {
                        select: {
                            id: true,
                            name: true,
                        }
                    },
                    student: {
                        select: {
                            id: true,
                            full_name: true,
                            phone: true,
                        }
                    }
                },
            })

            if (!unitTestAssigned) {
                return res.status(404).json(errorResponseDTO(404, 'Unit test assign not found!'));
            }

            let result: any = [];

            if (unitTestAssigned.student) {
                const tmpResult = await prisma.unit_test_result.findFirst({
                    where: {
                        class_unit_test_id: +assignId,
                        user_id: unitTestAssigned.student.id,
                    },
                    orderBy: {
                        created_date: 'desc'
                    }
                })

                result = [
                    {
                        student: {
                            ...unitTestAssigned.student,
                            unit_test_result: [tmpResult].filter(Boolean),
                        }
                    }
                ]
            } else {
                const whereCondition: any = {
                    class_id: unitTestAssigned.class.id,
                    student: {
                        full_name: {
                            contains: name,
                        }
                    },
                }

                const tmpResult = await prisma.classStudent.findMany({
                    where: whereCondition,
                    select: {
                        student: {
                            select: {
                                id: true,
                                full_name: true,
                                phone: true,
                                unit_test_result: {
                                    where: {
                                        class_unit_test_id: +assignId,
                                    },
                                    orderBy: {
                                        created_date: 'desc',
                                    },
                                    take: 1,
                                },
                            },
                        },
                        class_group_id: true,
                        status: true,
                    },
                    orderBy: {
                        student: {
                            full_name: 'asc',
                        }
                    }
                })

                result = tmpResult;
            }

            return res.status(200).json(apiResponseDTO(true, result
                .filter((el: any) => {
                    if (el.status === commonStatus.INACTIVE && !el.student.unit_test_result[0]) {
                        return false;
                    }

                    if (unitTestAssigned.group && unitTestAssigned.group.id !== el.class_group_id && !el.student.unit_test_result[0]) {
                        return false;
                    }

                    return true;
                })
                .map(({ student }: any) => ({
                    ...student,
                    unit_test_result: student.unit_test_result[0] ?? null,
                }))
            ));
        }
    },
});

export default handler