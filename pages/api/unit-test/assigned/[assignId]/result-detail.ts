import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            studentId: yup.string().required(),
            assignId: yup.number(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher: any = session.user;
            const { assignId, studentId } = req.query;

            const classUnitTest = await prisma.classUnitTest.findUnique({
                where: { id: +assignId },
                select: {
                    unitTest: {
                        select: {
                            name: true,
                        }
                    },
                }
            })
            if (!classUnitTest) {
                return res.status(400).json(errorResponseDTO(400, 'Bad request!'));
            }

            const studentInfo = await prisma.user.findUnique({
                where: { id: studentId as string },
                select: {
                    id: true,
                    full_name: true,
                    phone: true,
                }
            })
            let result: any = await prisma.unit_test_result.findMany({
                where: {
                    class_unit_test_id: +assignId,
                    user_id: studentId as string,
                    unitTestAssign: {
                        created_by: teacher.id,
                    }
                },
                orderBy: {
                    created_date: 'desc'
                }
            });

            return res.status(200).json(apiResponseDTO(true, {
                content: result,
                student: studentInfo,
                unitTest: classUnitTest.unitTest,
            }));
        }
    },
});

export default handler