import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import { commonDeleteStatus, commonStatus } from "@/constants/index";
import { classUnitTestSelect } from "dto/entitiesSelect";
import createHandler, { apiResponseDTO, paginationResponse } from "lib/apiHandle";
import prisma from "lib/prisma";
import { STATUS_CODE_EXAM } from "@/interfaces/constants";
import { getDateRangeConditionByStatus } from "@/utils/date";
import { mapListClassUnitTestToDTO } from "dto/unitTest";

const getParamsSchema = yup.object().shape({
    classId: yup.number(),
    examStatus: yup.number().oneOf(Object.values(STATUS_CODE_EXAM)),
});

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        pageable: true,
        querySchema: getParamsSchema,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any, paginationData) => {
            const teacher: any = session.user;
            const { name, classId, grade, examStatus }: any = req.query;

            const whereCondition = {
                unitTest: {
                    name: {
                        contains: name ? name + '' : undefined,
                    }
                },
                class_id: classId ? +classId : undefined,
                class: {
                    grade: grade ? grade + '' : undefined,
                    deleted: commonDeleteStatus.ACTIVE,
                },
                created_by: teacher.id,
                status: commonStatus.ACTIVE,
                ...(examStatus ? getDateRangeConditionByStatus(examStatus) : {}),
            }

            const [ result, total ] = await prisma.$transaction([
                prisma.classUnitTest.findMany({
                    where: whereCondition,
                    select: classUnitTestSelect,
                    orderBy: {
                        id: 'desc',
                    },
                    ...paginationData,
                }),
                prisma.classUnitTest.count({
                    where: whereCondition,
                    orderBy: {
                        id: 'desc',
                    }
                })
            ])

            return res.status(200).json(apiResponseDTO(true, paginationResponse(mapListClassUnitTestToDTO(result), total, paginationData)));
        }
    },
});

export default handler