import fs from 'fs'

import JSZip from 'jszip'
import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { uploadDir } from '@/constants/index'
import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    
    if (!session) {
        return res.status(403).end('Forbidden')
    }

    switch (req.method) {
        case 'GET':
            return getUnitTestById()
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }

    async function getUnitTestById() {
        const { id } = req.query
        if (!id) return res.status(400).json({ message: '`id` required' })

        try {
            const unitTests: any[] = await query<any[]>(
                'SELECT name FROM unit_test WHERE deleted = 0 AND id = ? LIMIT 1',
                [id],
            )
            const unitTest = unitTests[0]

            const sections = await query<any[]>(
                'SELECT id FROM unit_test_section WHERE deleted = 0 AND unit_test_id = ?',
                [id],
            )
            const sectionIds: number[] = sections.map((m) => m.id)
            const parts = await query<any[]>(
                'SELECT id FROM unit_test_section_part WHERE deleted = 0 AND unit_test_section_id IN (?)',
                [sectionIds],
            )
            const partIds: number[] = parts.map((m) => m.id)
            const questions: any[] = await query<any[]>(
                `SELECT utspq.unit_test_section_part_id, q.* FROM unit_test_section_part_question utspq
                LEFT JOIN question q ON utspq.question_id = q.id
                WHERE utspq.deleted = 0 AND utspq.unit_test_section_part_id IN (?)`,
                [partIds],
            )

            const questionsWithAudio = questions.filter(item => item.audio !== null && item.audio !== '')

            if (questionsWithAudio.length === 0) {
                return res.status(400).json({ message: "no-audio" })
            }

            const zip = new JSZip()
            let questionIndex = 1

            for (const item of questions) {
                if (item.audio) {
                    const segs = item.audio.split('/')
                    const partName = segs[segs.length - 1].split('.')
                    const data = fs.readFileSync(`${uploadDir}/${item.audio}`)
                    zip.file(`Question Number ${questionIndex}.${partName[partName.length - 1]}`, data)
                }
                if (excludeType.includes(item.question_type)) {
                    questionIndex += 1
                } else {
                    questionIndex += item.total_question
                }
            }
            const fileZip = unitTest.name.replace(/[.*\/\\^]/g, '');
            const fileNameEncode = encodeURIComponent(fileZip).replace(/,/g, ' ')
            res.writeHead(200, {
                'Content-Type': 'application/octet-stream',
                'Content-Disposition': "attachment; filename*=UTF-8''" + fileNameEncode + "-audio.zip" + "",
            })
            zip.generateNodeStream().pipe(res)
        } catch (error: any) {
            let message = error?.message;
            if (message && message.startsWith("ENOENT: no such file or directory")) {
                message = "export-audio-failed"
            }
            res.status(500).json({ message: message })
        }
    }
}

const excludeType = ['MR1', 'MR2', 'MR3']

export default handler