import { commonStatus, SALE_ITEM_TYPE } from "@/constants/index";
import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";
import { getStudentComboUnitTest } from "services/comboUnitTest";
import { addUnitTestAssetForUser } from "services/userAssets";

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;
            const { unitTestId } = req.body;

            const unitTest = await prisma.unit_test.findUnique({ where: { id: unitTestId } })
            if (!unitTest) {
                return res.status(400).json(errorResponseDTO(400, 'Bad request!'));
            }

            const saleInfo = await prisma.saleItem.findFirst({
                where: {
                    item_type: SALE_ITEM_TYPE.UNIT_TEST,
                    item_id: unitTestId,
                }
            })
            const studentCombo = await getStudentComboUnitTest(student.id);
            if (saleInfo && (
                !studentCombo ||
                studentCombo.isExpired ||
                !studentCombo.quantity ||
                !studentCombo.item?.unit_test_types?.includes(unitTest.unit_test_type)
            )) {
                return res.status(400).json(errorResponseDTO(400, 'Bad request!'));
            }

            const assetExisted = await prisma.user_asset.findFirst({
                where: {
                    user_id: student.id,
                    asset_type: SALE_ITEM_TYPE.UNIT_TEST,
                    asset_id: unitTestId + '',
                }
            })
            if (assetExisted) {
                return res.status(400).json(errorResponseDTO(400, 'Already added!'));
            }

            const saveAssetTransactions = [ addUnitTestAssetForUser(unitTestId, student.id) ];
            if (saleInfo) {
                saveAssetTransactions.push(prisma.user_asset.update({
                    where: { id: studentCombo.id },
                    data: {
                        quantity: Math.max(studentCombo.quantity - 1, 0),
                        updated_date: new Date(),
                    }
                }));
            }

            await prisma.$transaction(saveAssetTransactions);

            return res.status(200).json(apiResponseDTO(true, null));
        }
    },
});

export default handler