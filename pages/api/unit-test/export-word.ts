import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
import { query } from 'lib/db'
import fs from 'fs'
const sizeOf = require('image-size')

import {
  Document,
  Packer,
  Paragraph,
  TextRun,
  Table,
  TableCell,
  TableRow,
  BorderStyle,
  WidthType,
  AlignmentType,
  VerticalAlign,
  HeightRule,
  HeadingLevel,
  ImageRun,
  UnderlineType,
} from 'docx'
import { replaceSpecialCharacter } from 'api/utils'
import { formatTextStyleObject } from 'utils/string'
import convertTextCell from '@/questions_middleware/convertTextCell'
import { uploadDir } from '@/constants/index'

const trueColor = '#009d12'
const falseColor = '#d25045'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })

  if (!session) {
    return res.status(403).end('Forbidden')
  }

  switch (req.method) {
    case 'GET':
      return getUnitTestById()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getUnitTestById() {
    const { id } = req.query
    if (!id) return res.status(400).json({ message: '`id` required' })

    try {
      const unitTests: any[] = await query<any[]>(
        `SELECT g.name as grade_name ,u.*FROM unit_test as u
        join grade as g on u.template_level_id = g.id
        WHERE u.deleted = 0 AND u.id = ? LIMIT 1`,
        [id],
      )
      const unitTest = unitTests[0]
      if (unitTests.length === 0) {
        return res.status(400).json({ message: 'data not found' })
      }
      const sections: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section WHERE deleted = 0 AND unit_test_id = ?',
        [id],
      )
      if (sections.length > 0) {
        unitTest.sections = sections
      }
      const sectionIds = sections.map((m) => m.id)
      const parts: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section_part WHERE deleted = 0 AND unit_test_section_id IN (?)',
        [sectionIds],
      )
      if (parts.length > 0) {
        for (const item of sections) {
          item.parts = parts.filter((m) => m.unit_test_section_id === item.id)
        }
      }
      const partIds = parts.map((m) => m.id)
      const questions: any[] = await query<any[]>(
        `SELECT utspq.unit_test_section_part_id, q.* FROM unit_test_section_part_question utspq
         LEFT JOIN question q ON utspq.question_id = q.id
         WHERE utspq.deleted = 0 AND utspq.unit_test_section_part_id IN (?)`,
        [partIds],
      )
      if (questions.length > 0) {
        for (const item of parts) {
          item.questions = questions.filter(
            (m) => m.unit_test_section_part_id === item.id,
          )
        }
      }
      const doc = new Document({
        sections: [
          {
            properties: {
              page: {
                margin: {
                  top: '0.5 in',
                  bottom: '0.5 in',
                  left: '0.5 in',
                  right: '0.5 in',
                },
              },
            },
            children: [
              new Table({
                width: { size: 100, type: WidthType.PERCENTAGE },
                rows: [
                  new TableRow({
                    children: [
                      new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                          size: 50,
                          type: WidthType.PERCENTAGE,
                        },
                        children: [
                          new Paragraph({
                            spacing: {
                              line: 300,
                            },
                            alignment: AlignmentType.CENTER,
                            children: [
                              new TextRun('UBND QUẬN _________'),
                              new TextRun({
                                text: 'PHÒNG GIÁO DỤC VÀ ĐÀO TẠO',
                                break: 1,
                              }),
                              new TextRun({
                                text: 'ĐỀ CHÍNH THỨC',
                                break: 1,
                              }),
                            ],
                          }),
                        ],
                      }),
                      new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                          size: 50,
                          type: WidthType.PERCENTAGE,
                        },
                        columnSpan: 2,
                        children: [
                          new Paragraph({
                            spacing: {
                              line: 300,
                              before: 200,
                              after: 200,
                            },
                            alignment: AlignmentType.CENTER,
                            children: [
                              new TextRun('ĐỀ KIỂM TRA __________'),
                              new TextRun({
                                text: 'Năm học: 20__ - 20__',
                                break: 1,
                              }),
                              new TextRun({
                                text: `[MÔN HỌC] - ${unitTest.grade_name}`,
                                break: 1,
                              }),
                              new TextRun({
                                text: `Thời gian làm bài: ${unitTest.time} phút`,
                                break: 1,
                              }),
                            ],
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableRow({
                    children: [
                      new TableCell({
                        verticalAlign: VerticalAlign.CENTER,
                        width: {
                          size: 50,
                          type: WidthType.PERCENTAGE,
                        },
                        children: [
                          new Paragraph({
                            indent: {
                              left: '0.2 in',
                            },
                            spacing: {
                              line: 300,
                              before: 200,
                              after: 200,
                            },
                            alignment: AlignmentType.START,
                            children: [
                              new TextRun('Trường: ________________________'),
                              new TextRun({
                                text: 'Họ tên: _______________________',
                                break: 1,
                              }),
                              new TextRun({
                                text: 'Lớp: _____________',
                                break: 1,
                              }),
                            ],
                          }),
                        ],
                      }),
                      new TableCell({
                        verticalAlign: VerticalAlign.TOP,
                        width: {
                          size: 12,
                          type: WidthType.PERCENTAGE,
                        },
                        children: [
                          new Paragraph({
                            spacing: {
                              before: 200,
                            },
                            alignment: AlignmentType.CENTER,
                            children: [new TextRun('Điểm')],
                          }),
                        ],
                      }),
                      new TableCell({
                        verticalAlign: VerticalAlign.TOP,
                        width: {
                          size: 38,
                          type: WidthType.PERCENTAGE,
                        },
                        children: [
                          new Paragraph({
                            spacing: {
                              before: 200,
                            },
                            alignment: AlignmentType.CENTER,
                            children: [new TextRun('Nhận xét của giáo viên')],
                          }),
                        ],
                      }),
                    ],
                  }),
                ],
              }),
              new Paragraph({
                alignment: AlignmentType.CENTER,
                heading: HeadingLevel.HEADING_2,
                children: [
                  new TextRun({ text: '', break: 1 }),
                  new TextRun({
                    text: unitTest.name,
                    break: 1,
                    color: '#188fba',
                    bold: true,
                  }),
                  new TextRun({
                    text: `Time allotted: ${unitTest.time}`,
                    break: 1,
                    color: '#188fba',
                    bold: true,
                  }),
                ],
              }),
              ...renderSection(unitTest),
              new Paragraph({
                spacing: { before: 200 },
                alignment: AlignmentType.CENTER,
                children: [
                  new TextRun({
                    text: '---THE END---',
                    color: '#188fba',
                    bold: true,
                  }),
                ],
              }),
            ],
          },
        ],
      })

      res.writeHead(200, {
        'Content-Type':
          'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
        'Content-Disposition': `attachment; filename=${replaceSpecialCharacter(
          unitTest.name,
        )}.docx`,
      })

      Packer.toBuffer(doc).then((buffer) => {
        res.write(buffer, 'binary')
        res.end()
      })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

function renderSection(data: any) {
  const list: any[] = []

  let currentQuestion = 1

  data.sections.forEach((section: any, sIndex: number) => {
    list.push(new Paragraph(''))
    list.push(
      new Table({
        borders: {
          top: { style: BorderStyle.SINGLE, color: '#188fba' },
          left: { style: BorderStyle.SINGLE, color: '#188fba' },
          bottom: { style: BorderStyle.SINGLE, color: '#188fba' },
          right: { style: BorderStyle.SINGLE, color: '#188fba' },
          insideHorizontal: { style: BorderStyle.SINGLE, color: '#188fba' },
          insideVertical: { style: BorderStyle.SINGLE, color: '#188fba' },
        },
        margins: { top: 100, right: 100, left: 100, bottom: 100 },
        alignment: AlignmentType.LEFT,
        rows: [
          new TableRow({
            children: [
              new TableCell({
                children: [
                  new Paragraph({
                    children: [
                      new TextRun({
                        text: `PART ${sIndex + 1} `,
                        color: '#188fba',
                        bold: true,
                      }),
                    ],
                  }),
                ],
              }),
              new TableCell({
                borders: {
                  top: { style: BorderStyle.SINGLE, color: '#ffffff' },
                  right: { style: BorderStyle.SINGLE, color: '#ffffff' },
                  bottom: { style: BorderStyle.SINGLE, color: '#ffffff' },
                },
                children: [
                  new Paragraph({
                    children: [
                      new TextRun({
                        text: `${section.parts[0].name}`,
                        color: '#188fba',
                        bold: true,
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        ],
      }),
    )

    section.parts[0].questions.forEach((question: any, qIndex: number) => {
      switch (question.question_type) {
        case 'MC1':
          MultiChoiceQuestion1_v2(question, currentQuestion, list)
          break
        case 'MC2':
          MultiChoiceQuestion2_v2(question, currentQuestion, list)
          break
        case 'MC3':
          MultiChoiceQuestion1(question, currentQuestion, list)
          break
        case 'MC4':
          MultiChoiceQuestion4(question, currentQuestion, list)
          break
        case 'MC5':
          MultiChoiceQuestion5_v2(question, currentQuestion, list)
          break
        case 'MC6':
          MultiChoiceQuestion5(question, currentQuestion, list)
          break
        case 'DD1':
          DragAndDropQuestion(question, currentQuestion, list)
          break
        case 'FB1':
          FillInBlankQuestion1(question, currentQuestion, list)
          break
        case 'FB2':
        case 'FB3':
        case 'FB4':
          FillInBlankQuestion2_3_4_v2(question, currentQuestion, list)
          break
        case 'FB6':
        case 'FB7':
          FillInBlankQuestion2(question, currentQuestion, list)
          break
        case 'FB5':
          FillInBlankQuestion5(question, currentQuestion, list)
          break
        case 'LS1':
        case 'LS2':
          LikertScaleQuestion(question, currentQuestion, list)
          break
        case 'MG1':
        case 'MG2':
        case 'MG3':
          MatchingGameQuestion(question, currentQuestion, list)
          break
        case 'SA1':
          ShortAnswerQuestion(question, currentQuestion, list)
          break
        case 'TF1':
        case 'TF2':
          TrueFalseQuestion(question, currentQuestion, list)
          break
        case 'MR1':
        case 'MR2':
          MultiResponseQuestion1(question, currentQuestion, list)
          break
        case 'MR3':
          MultiResponseQuestion3(question, currentQuestion, list)
          break
        case 'SL1':
          SelectFromListQuestion1(question, currentQuestion, list)
          break
        case 'SL2':
          SelectFromListQuestion2(question, currentQuestion, list)
          break
        case 'MIX1':
          MIXQuestion1(question, currentQuestion, list)
          break
        case 'MIX2':
          MIXQuestion2(question, currentQuestion, list)
          break
        case 'TF4':
          TrueFalseQuestion4(question, currentQuestion, list)
          break
        case 'TF3':
          TrueFalseQuestion3(question, currentQuestion, list)
          break
      }

      if (excludeType.includes(question.question_type)) {
        currentQuestion += 1
      } else {
        currentQuestion += question.total_question
      }
    })
  })

  return list
}
const excludeType = ['MR1', 'MR2', 'MR3']

const getArrTextRunFromArrTextStyleObject = (data: any[]) => {
  const arrItem: any = []
  data.forEach((item: any) => {
    arrItem.push(
      new TextRun({
        text: item.text.replaceAll(/\%s\%/g, '______________'),
        bold: item.bold,
        italics: item.italic,
        ...(item.underline
          ? {
              underline: {
                type: UnderlineType.SINGLE,
              },
            }
          : {}),
      }),
    )
  })
  return arrItem
}

//mc1 multi question
function MultiChoiceQuestion1_v2(question: any, index: number, list: any[]) {
  const listAnswer = question.answers.split('#')

  const listQuestionText = question.question_text.split('#')
  const qdArr = question.question_description.split('\n')
  if (listQuestionText.length == 1) {
    const qdParagraph = qdArr.map((item: any, mIndex: number) => {
      if (mIndex === 0) {
        item = `${index}.  ${item}`
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: mIndex === qdArr.length - 1 ? 200 : 0,
        },
        indent: mIndex > 0 ? { left: '0.2 in' } : {},
        children: [new TextRun({ text: item, bold: true })],
      })
    })
    list.push(...qdParagraph)

    listQuestionText.map((questionText: string, indexItem: number) => {
      //get object style arr

      const answers = listAnswer[indexItem]
        ?.split('*')
        .map((answer: string) => formatTextStyleObject(answer))

      const questionTextArr = questionText.split('\n')
      const columnWidth = 100.0 / answers.length

      // write index if multiple question
      const hasQuestionText = questionText && questionText.trim() != ''
      const questionTextParagraph = questionTextArr.map(
        (item: string, mIndex: number) => {
          return new Paragraph({
            spacing: {
              before: 150,
            },
            indent: {
              left: '0.2 in',
            },
            children: formatTextStyleObject(
              item.replaceAll(/\%s\%/g, '______________'),
            ).map((item: any) => {
              return new TextRun({ ...item, italics: item.italic })
            }),
          })
        },
      )
      if (hasQuestionText) {
        list.push(...questionTextParagraph)
      }

      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 100, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  return new TableCell({
                    width: { size: columnWidth, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        //  ...(hasQuestionText)? {}:{indent: { left: '0.11 in' }},
                        indent: { left: '0.2 in' },
                        children: [
                          new TextRun(
                            `${String.fromCharCode(65 + answerIndex)}. `,
                          ),
                          ...getArrTextRunFromArrTextStyleObject(answer),
                        ],
                      }),
                    ],
                  })
                }),
              ],
            }),
          ],
        }),
      )
    })
    return list
  } else {
    const qdParagraphMC = qdArr.map((item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: 200,
        },
        children: [new TextRun({ text: item, bold: true })],
      })
    })
    list.push(...qdParagraphMC)

    listQuestionText.map((questionText: string, indexItem: number) => {
      //get object style arr
      console.log('questionText', questionText)
      const answers = listAnswer[indexItem]
        ?.split('*')
        .map((answer: string) => formatTextStyleObject(answer))

      const questionTextArr = questionText.split('\n')
      const columnWidth = 100.0 / answers.length
      const questionTextRun = questionTextArr.map(
        (item: string, mIndex: number) => {
          if (mIndex === 0) {
            item = `${indexItem + index}. ${item}`
          }
          return new Paragraph({
            spacing: {
              before: 150,
            },
            indent: mIndex > 0 ? { left: '0.13 in' } : {},
            children: formatTextStyleObject(
              item.replaceAll(/\%s\%/g, '______________'),
            ).map((item: any) => {
              return new TextRun({ ...item, italics: item.italic })
            }),
          })
        },
      )
      // write index if multiple question
      const hasQuestionText = questionText && questionText.trim() != ''
      if (hasQuestionText) {
        list.push(...questionTextRun)
      }

      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 100, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  return new TableCell({
                    width: { size: 1, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        children: [
                          answerIndex == 0
                            ? !hasQuestionText
                              ? new TextRun({
                                  text: `${indexItem + index}. `,
                                })
                              : new TextRun({ text: `    ` })
                            : {},
                          new TextRun(
                            `${String.fromCharCode(65 + answerIndex)}. `,
                          ),
                          ...getArrTextRunFromArrTextStyleObject(answer),
                        ],
                      }),
                    ],
                  })
                }),
              ],
            }),
          ],
        }),
      )
    })
    return list
  }
}
//mc2 multi question
function MultiChoiceQuestion2_v2(question: any, index: number, list: any[]) {
  type dataViewModel = {
    questions: string[]
    answers: string[]
    corrects: string[]
  }
  const listAnswer: string[] = question.answers.split('#')
  const listQuestionText: string[] = question.question_text.split('#')
  const listCorrectAnswer: string[] = question.correct_answers.split('#')
  const qdArr = question.parent_question_description?.split('\n') || []

  const image = question.image
  const imageUrl = `${uploadDir}/${image}`
  const imageSize = sizeOf(imageUrl)
  const example: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
  }
  const mainQuestion: dataViewModel = {
    questions: [],
    answers: [],
    corrects: [],
  }
  for (let i = 0; i < listQuestionText.length; i++) {
    if (listQuestionText[i]?.startsWith('*')) {
      example.questions.push(listQuestionText[i].substring(1))
      example.answers.push(listAnswer[i])
      example.corrects.push(listCorrectAnswer[i])
    } else {
      mainQuestion.questions.push(listQuestionText[i])
      mainQuestion.answers.push(listAnswer[i])
      mainQuestion.corrects.push(listCorrectAnswer[i])
    }
  }
  const qdParagraphMC = qdArr.map((item: string, mIndex: number) => {
    return new Paragraph({
      spacing: {
        before: 200,
      },
      children: [new TextRun({ text: item, bold: true })],
    })
  })
  list.push(...qdParagraphMC)
  const maxWidth = 450
  let imgHeight = 136
  let imgWidth = (imageSize.width * 136) / imageSize.height
  if (imgWidth > maxWidth) {
    imgWidth = 450
    imgHeight = (imageSize.height * 450) / imageSize.width
  }
  list.push(
    new Paragraph({
      spacing: { before: 200, after: 200 },
      alignment: AlignmentType.CENTER,
      children: [
        new ImageRun({
          data: fs.readFileSync(imageUrl),
          transformation: {
            width: imgWidth,
            height: imgHeight,
          },
        }),
      ],
    }),
  )
  if (example.questions.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Examples:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.questions.forEach((questionText: string, indexItem: number) => {
      const answers = example.answers[indexItem]
        ?.split('*')
        .map((answer: string) => formatTextStyleObject(answer))

      const questionTextArr = questionText.split('\n')
      const questionTextRun = questionTextArr.map(
        (item: string, mIndex: number) => {
          return new Paragraph({
            spacing: {
              before: 150,
            },
            indent: { left: '0.13 in' },
            children: formatTextStyleObject(
              item.replaceAll(/\%s\%/g, '______________'),
            ).map((item: any) => {
              return new TextRun({ ...item, italics: item.italic })
            }),
          })
        },
      )
      list.push(...questionTextRun)
      list.push(
        new Table({
          margins: { top: 150 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          width: { size: 96, type: WidthType.PERCENTAGE },
          alignment: AlignmentType.END,
          rows: [
            new TableRow({
              children: [
                ...answers.map((answer: any, answerIndex: number) => {
                  return new TableCell({
                    width: { size: 1, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        children: [
                          new TextRun({
                            text: `${String.fromCharCode(65 + answerIndex)}. `,
                            ...(example.corrects[indexItem] ===
                            convertTextCell(answer)
                              ? {
                                  underline: {
                                    type: UnderlineType.SINGLE,
                                    color: trueColor,
                                  },
                                  color: trueColor,
                                }
                              : {}),
                          }),
                          ...getArrTextRunFromArrTextStyleObject(answer),
                        ],
                      }),
                    ],
                  })
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  if (mainQuestion.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Questions:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    mainQuestion.questions.forEach(
      (questionText: string, indexItem: number) => {
        const answers = mainQuestion.answers[indexItem]
          ?.split('*')
          .map((answer: string) => formatTextStyleObject(answer))

        const questionTextArr = questionText.split('\n')
        const columnWidth = 100.0 / answers.length
        const questionTextRun = questionTextArr.map(
          (item: string, mIndex: number) => {
            if (mIndex == 0) {
              item = `${index + indexItem}. ${item}`
            }
            return new Paragraph({
              spacing: {
                before: 150,
              },
              indent: { left: '0.13 in' },
              children: formatTextStyleObject(
                item.replaceAll(/\%s\%/g, '______________'),
              ).map((item: any) => {
                return new TextRun({ ...item, italics: item.italic })
              }),
            })
          },
        )
        list.push(...questionTextRun)
        list.push(
          new Table({
            margins: { top: 150 },
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            width: { size: 96, type: WidthType.PERCENTAGE },
            alignment: AlignmentType.END,
            rows: [
              new TableRow({
                children: [
                  ...answers.map((answer: any, answerIndex: number) => {
                    return new TableCell({
                      width: { size: 1, type: WidthType.PERCENTAGE },
                      children: [
                        new Paragraph({
                          children: [
                            new TextRun(
                              `${String.fromCharCode(65 + answerIndex)}. `,
                            ),
                            ...getArrTextRunFromArrTextStyleObject(answer),
                          ],
                        }),
                      ],
                    })
                  }),
                ],
              }),
            ],
          }),
        )
      },
    )
  }

  return list
}
function MultiChoiceQuestion4(question: any, index: number, list: any[]) {
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: [
        new TextRun({
          text: `${index}.  ${question.question_description}`,
          bold: true,
        }),
      ],
    }),
  )
  const answers = question.answers
    ?.split('*')
    .map((answer: string) => formatTextStyleObject(answer))

  const columnWidth = 100.0 / answers.length

  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          children: [
            ...answers.map((answer: any, answerIndex: number) => {
              return new TableCell({
                width: { size: columnWidth, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph({
                    //  ...(hasQuestionText)? {}:{indent: { left: '0.11 in' }},
                    indent: { left: '0.2 in' },
                    children: [
                      new TextRun(`${String.fromCharCode(65 + answerIndex)}. `),
                      ...getArrTextRunFromArrTextStyleObject(answer),
                    ],
                  }),
                ],
              })
            }),
          ],
        }),
      ],
    }),
  )
  return list
}
function MultiChoiceQuestion1(question: any, index: number, list: any[]) {
  const answers = question.answers.split('*')
  const columnWidth = 100.0 / answers.length
  const isMC4 = question.question_type === 'MC4'

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: [
        new TextRun({
          text: `${index}.  ${question.question_description}`,
          bold: true,
        }),
      ],
    }),
  )
  if (question.question_text) {
    list.push(
      new Paragraph({
        spacing: {
          before: 150,
        },
        indent: {
          left: '0.2 in',
        },
        children: [
          new TextRun({
            text: `${question.question_text.replace(
              /\%s\%/g,
              '______________',
            )}`,
          }),
        ],
      }),
    )
  }
  if (question.image) {
    const imageSize = sizeOf(`${uploadDir}/${question.image}`)
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: (imageSize.width * 136) / imageSize.height,
              height: 136,
            },
          }),
        ],
      }),
    )
  }
  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          children: [
            ...answers.map(
              (answer: string, answerIndex: number) =>
                new TableCell({
                  width: { size: columnWidth, type: WidthType.PERCENTAGE },
                  children: [
                    !isMC4
                      ? new Paragraph({
                          indent: { left: '0.2 in' },
                          text: `${String.fromCharCode(
                            65 + answerIndex,
                          )}. ${answer}`,
                        })
                      : new Paragraph({
                          indent: { left: '0.2 in' },
                          children: [
                            new TextRun(
                              `${String.fromCharCode(65 + answerIndex)}. `,
                            ),
                            ...answer.split('%').map(
                              (part: string, partIndex: number) =>
                                new TextRun({
                                  text: part,
                                  bold: partIndex === 1,
                                  ...(partIndex === 1
                                    ? {
                                        underline: {
                                          type: UnderlineType.SINGLE,
                                        },
                                      }
                                    : {}),
                                }),
                            ),
                          ],
                        }),
                  ],
                }),
            ),
          ],
        }),
      ],
    }),
  )
  return list
}

function ReadingScript(script: string, list: any[]) {
  const textRunArr = script.split('\n')
  const textRunParagraph = textRunArr.map((item: string, mIndex: number) => {
    return new Paragraph({
      keepLines: false,
      spacing: {
        before: mIndex === 0 ? 60 : 0,
        after: mIndex === textRunArr.length - 1 ? 60 : 0,
        line: 300,
      },
      indent: {
        left: '0.1 in',
        right: '0.1 in',
      },
      children: formatTextStyleObject(item).map((item: any) => {
        return new TextRun({
          ...item,
          italics: item.italic,
        })
      }),
    })
  })
  list.push(
    new Table({
      width: { size: 100, type: WidthType.PERCENTAGE },
      borders: {
        top: { color: '#35b9e6', style: BorderStyle.DASHED },
        left: { color: '#35b9e6', style: BorderStyle.DASHED },
        right: { color: '#35b9e6', style: BorderStyle.DASHED },
        bottom: { color: '#35b9e6', style: BorderStyle.DASHED },
      },
      rows: [
        new TableRow({
          children: [
            new TableCell({
              shading: { fill: '#edf9fd' },
              children: [...textRunParagraph],
            }),
          ],
        }),
      ],
    }),
  )
}

function MultiChoiceQuestion5(question: any, index: number, list: any[]) {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers
    ?.split('#')
    .map((m: string) => m.split('*'))
  const columnWidth =
    100.0 / Math.max(...answerList.map((m: string[]) => m.length))
  const pqdArr = question.parent_question_description.split('\n')
  const pqdParagraph = pqdArr.map((item: string, mIndex: number) => {
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === pqdArr.length - 1 ? 200 : 0,
      },
      children: [new TextRun({ text: item, bold: true })],
    })
  })
  list.push(...pqdParagraph)
  questionList.forEach((questionT: string, qIndex: number) => {
    const questionTArr = questionT.split('\n')
    const questionTParagraph = questionTArr.map(
      (item: string, mIndex: number) => {
        if (mIndex === 0) {
          item = `${index + qIndex}. ${item}`
        }
        return new Paragraph({
          spacing: {
            before: mIndex === 0 ? 200 : 0,
          },
          indent: mIndex > 0 ? { left: '0.15 in' } : {},
          children: [
            new TextRun({ text: item.replaceAll(/\%s\%/g, '______________') }),
          ],
        })
      },
    )
    list.push(...questionTParagraph)
    list.push(
      new Table({
        margins: { top: 200 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          ...answerList[qIndex].map(
            (answer: string, answerIndex: number) =>
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: columnWidth, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        indent: { left: '0.15 in' },
                        text: `${String.fromCharCode(
                          65 + answerIndex,
                        )}. ${answer}`,
                      }),
                    ],
                  }),
                ],
              }),
          ),
        ],
      }),
    )
  })

  return list
}

//mutil choice type 5 update
function MultiChoiceQuestion5_v2(question: any, index: number, list: any[]) {
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const TextRunQ = question.parent_question_description.split('\n')

  // const columnWidth =
  //   100.0 / Math.max(...answerList.map((m: string[]) => m.length))
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.parent_question_text ? 200 : 0,
      },
      children: TextRunQ.map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  if (question.parent_question_text) {
    ReadingScript(question.parent_question_text, list)
  }
  questionList.map((questionText: string, indexItem: number) => {
    //get object style arr
    const answers = answerList[indexItem]
      ?.split('*')
      .map((answer: string) => formatTextStyleObject(answer))

    const columnWidth = 100.0 / answers.length

    // write index if multiple question
    const hasQuestionText = questionText && questionText.trim() != ''
    if (questionList.length && hasQuestionText) {
      const questionTextArr = questionText.split('\n')
      const paragraphs = questionTextArr.map((item: any, mIndex: number) => {
        if (mIndex === 0) {
          item = `${indexItem + index}. ${item}`
        }
        return new Paragraph({
          spacing: {
            before: mIndex === 0 ? 150 : 0,
          },
          indent: mIndex > 0 ? { left: '0.15 in' } : {},
          children: formatTextStyleObject(
            item.replaceAll(/\%s\%/g, '______________'),
          ).map((i: any) => {
            return new TextRun(i)
          }),
        })
      })
      list.push(...paragraphs)
    }
    list.push(
      new Table({
        margins: { top: 200 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: [
              ...answers.map((answer: any, answerIndex: number) => {
                return new TableCell({
                  width: { size: columnWidth, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      indent: { left: '0.15 in' },
                      children: [
                        new TextRun(
                          `${String.fromCharCode(65 + answerIndex)}. `,
                        ),
                        ...getArrTextRunFromArrTextStyleObject(answer),
                      ],
                    }),
                  ],
                })
              }),
            ],
          }),
        ],
      }),
    )
  })

  return list
}
function DragAndDropQuestion(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correctAnswers = question.correct_answers.split('#')
  const images = question.image?.split('#') || ''
  const checkExam = question.question_text?.split('#') || ''
  const checkOG = question?.question_text

  const example: any = {
    images: [],
    answers: [],
    corrects: [],
  }
  const mainQuestion: any = {
    images: [],
    answers: [],
    corrects: [],
  }

  for (let i = 0; i < answers.length; i++) {
    if (answers[i]) {
      if ( checkOG === null || checkExam[i] !=='*') {
        if(!images[i]){
          mainQuestion.images.push('')
          mainQuestion.answers.push(answers[i])
          mainQuestion.corrects.push(correctAnswers[i])
          continue
        }
        mainQuestion.images.push(images[i])
        mainQuestion.answers.push(answers[i])
        mainQuestion.corrects.push(correctAnswers[i])
        continue
      } else {
        if (!images[i]) {
          example.images.push('')
          example.answers.push(answers[i])
          example.corrects.push(correctAnswers[i])
          continue
        }
        example.images.push(images[i])
        example.answers.push(answers[i])
        example.corrects.push(correctAnswers[i])
        continue
      }
    }
  }

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: [
        new TextRun({
          text: question.parent_question_description,
          bold: true,
        }),
      ],
    }),
  )

  if (example.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Examples:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.answers.forEach((answer: string, exIndex: number) => {
      const correct = example.corrects[exIndex]
      const imageUrl = example.images[exIndex]? `${uploadDir}/${example.images[exIndex]}`: ""
      
      if (imageUrl){
        const imageSize = sizeOf(imageUrl)
        list.push(
          new Table({
            margins: { top: 200 },
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 5, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: ``,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 20, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.CENTER,
                        children: [
                          new ImageRun({
                            data: fs.readFileSync(imageUrl),
                            transformation: {
                              width: 136,
                              height: (imageSize.height * 136) / imageSize.width,
                            },
                          }),
                          ,
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 75, type: WidthType.PERCENTAGE },
                    // margins: { left: 150 },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        spacing:{
                          before: 150,
                        },
                        indent: {
                          left: '0.2 in',
                        },
                        text: `Keywords: ${answer?.replace(/\*/g, '/')}`,
                      }),
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        indent: {
                          left: '0.2 in',
                        },
                        text: `→ ${correct?.replace(/\*/g, ' ')}`,
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        )
      }
      else{
        list.push(
          new Table({
            margins: { top: 200 },
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 5, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.TOP,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: ``,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 95, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.TOP,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        text: `Keywords: ${answer?.replace(/\*/g, '/')}`,
                      }),
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        text: `→ ${correct?.replace(/\*/g, ' ')}`,
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        )
      }
    })
  }

  if (mainQuestion.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Questions:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )

    mainQuestion.answers.forEach((answer: string, aIndex: number) => {
      const imageUrl = mainQuestion.images[aIndex] ? `${uploadDir}/${mainQuestion.images[aIndex]}`: ""
      if(imageUrl !== "") {
      const imageSize = sizeOf(imageUrl)
        list.push(
          new Table({
            margins: { top: 200 },
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 5, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: `${index + aIndex}. `,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 20, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.CENTER,
                        children: [
                          new ImageRun({
                            data: fs.readFileSync(imageUrl),
                            transformation: {
                              width: 136,
                              height: (imageSize.height * 136) / imageSize.width,
                            },
                          }),
                          ,
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 75, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.CENTER,
                    children: [
                      new Paragraph({
                        spacing:{
                          before: 150,
                        },
                        indent: {
                          left: '0.2 in',
                        },
                        text: `Keywords: ${answer?.replace(/\*/g, '/')}`,
                      }),
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        indent: {
                          left: '0.2 in',
                        },
                        text:`→ .......................................`,
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        )
      }
      else if (!mainQuestion.images[aIndex]){
        list.push(
          new Table({
            margins: { top: 200 },
            borders: {
              top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
              insideHorizontal: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
              insideVertical: {
                style: BorderStyle.SINGLE,
                size: 1,
                color: '#ffffff',
              },
            },
            alignment: AlignmentType.END,
            width: { size: 100, type: WidthType.PERCENTAGE },
            rows: [
              new TableRow({
                children: [
                  new TableCell({
                    width: { size: 5, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.TOP,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        children: [
                          new TextRun({
                            text: `${index + aIndex}. `,
                          }),
                        ],
                      }),
                    ],
                  }),
                  new TableCell({
                    width: { size: 95, type: WidthType.PERCENTAGE },
                    verticalAlign: VerticalAlign.TOP,
                    children: [
                      new Paragraph({
                        alignment: AlignmentType.START,
                        text: `Keywords: ${answer?.replace(/\*/g, '/')}`,
                      }),
                      new Paragraph({
                        spacing: {
                          before: 150,
                        },
                        children: [
                          new TextRun({
                            text:`→ .......................................`,
                          }),
                        ]
                      }),
                    ],
                  }),
                ],
              }),
            ],
          }),
        )
      }
    })
  }

  return list
}

function FillInBlankQuestion1(question: any, index: number, list: any[]) {
  const questionDesArr = question.question_description.split('\n')
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      if (mIndex === 0) {
        item = `${index}.  ${item}`
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: mIndex === questionDesArr.length - 1 ? 200 : 0,
        },
        indent:
          mIndex > 0
            ? {
                left: '0.2 in',
              }
            : {},
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)
  const questionTextArr = question.question_text.split('\n')
  const questionTextParagraph = questionTextArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 150 : 0,
          after: mIndex === questionTextArr.length - 1 ? 150 : 0,
          line: 300,
        },
        indent: {
          left: '0.2 in',
        },
        text: item.replaceAll(/\%s\%/g, '___________'),
      })
    },
  )
  list.push(...questionTextParagraph)
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
      },
      indent: {
        left: '0.2 in',
      },
      children: [
        new TextRun({
          text: `(${question.answers})`,
          bold: true,
          italics: true,
        }),
      ],
    }),
  )
  return list
}
function FillInBlankQuestion2_3_4_v2(
  question: any,
  index: number,
  list: any[],
) {
  const questionDesArr = question.question_description.split('\n')
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after:
            question.parent_question_text &&
            mIndex === questionDesArr.length - 1
              ? 200
              : 0,
        },
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)
  if (question.image) {
    const imageSize = sizeOf(`${uploadDir}/${question.image}`)
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: (imageSize.width * 136) / imageSize.height,
              height: 136,
            },
          }),
        ],
      }),
    )
  }
  if (question.parent_question_text) {
    ReadingScript(question.parent_question_text, list)
  }
  let questionText = question.question_text
  for (let i = 0; i < question.total_question; i++) {
    questionText = questionText.replace('%s%', `(${index + i})___________`)
  }
  if(question.question_type == "FB2"){
    console.log("questionText----",questionText)
  }
  const questionTextRunArr: TextRun[] = []
  questionText.split('\n').forEach((part: string, partIndex: number) => {
    let breakLine = partIndex>0
    formatTextStyleObject(part).forEach((item: any) => {
      questionTextRunArr.push(
        new TextRun({
          ...item,
          italics: item.italic,
          break: breakLine ? 1 : 0,
        }),
      )
      breakLine = false;
    })
  })
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: questionTextRunArr,
      // children: questionText.split('\n').map(
      //   (part: string, partIndex: number) =>
      //     new TextRun({
      //       text: part,
      //       break: partIndex > 0 ? 1 : 0,
      //     }),
      // ),
    }),
  )
  return list
}
function FillInBlankQuestion2(question: any, index: number, list: any[]) {
  const questionDesArr = question.question_description.split('\n')
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after:
            question.parent_question_text &&
            mIndex === questionDesArr.length - 1
              ? 200
              : 0,
        },
        children: [new TextRun({ text: item, bold: true })],
      })
    },
  )
  list.push(...questionDesParagraph)
  if (question.image) {
    const imageSize = sizeOf(`${uploadDir}/${question.image}`)
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: (imageSize.width * 136) / imageSize.height,
              height: 136,
            },
          }),
        ],
      }),
    )
  }
  if (question.parent_question_text) {
    ReadingScript(question.parent_question_text, list)
  }
  let questionText = question.question_text
  for (let i = 0; i < question.total_question; i++) {
    questionText = questionText.replace('%s%', `(${index + i})___________`)
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: questionText.split('\n').map(
        (part: string, partIndex: number) =>
          new TextRun({
            text: part,
            break: partIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  return list
}

function FillInBlankQuestion5(question: any, index: number, list: any[]) {
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
      },
      children: question.question_description.split('\n').map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  if (question.image) {
    const imageSize = sizeOf(`${uploadDir}/${question.image}`)
    list.push(
      new Paragraph({
        spacing: { before: 150 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${question.image}`),
            transformation: {
              width: (imageSize.width * 136) / imageSize.height,
              height: 136,
            },
          }),
        ],
      }),
    )
  }
  list.push(
    new Paragraph({
      spacing: { before: 150, line: 300 },
      indent: {
        left: '0.2 in',
      },
      children: Array.from(
        { length: question.total_question },
        (_, _index) =>
          new TextRun({
            text: `(${index + _index})___________________`,
            break: _index > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  return list
}

function LikertScaleQuestion(question: any, index: number, list: any[]) {
  const listAnswer: string[] = question.answers.split('#')

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: [
        new TextRun({
          text: question.question_description,
          bold: true,
        }),
      ],
    }),
  )

  if (question.question_text) {
    ReadingScript(question.question_text, list)
  }
  list.push(new Paragraph({ text: '' }))
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          tableHeader: true,
          height: { value: 600, rule: HeightRule.ATLEAST },
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Answers', bold: true })],
                  indent: { start: 200 },
                }),
              ],
              width: { size: 70, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'True', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'False', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Not given', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
          ],
        }),
        ...listAnswer.map(
          (answer: string, aIndex: number) =>
            new TableRow({
              height: { value: 500, rule: HeightRule.ATLEAST },
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      text: `${index + aIndex}. ${answer}`,
                      indent: { start: 200 },
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
              ],
            }),
        ),
      ],
    }),
  )
  return list
}

function MatchingGameQuestion(question: any, index: number, list: any[]) {
  const listAnswer: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
  const maxLength = Math.max(listAnswer[0].length, listAnswer[1].length)

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: question.question_description.split('\n').map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  if (question.question_text) {
    ReadingScript(question.question_text, list)
  }

  list.push(new Paragraph({ text: '' }))
  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: Array.from(
        { length: maxLength },
        (_, _index) =>
          new TableRow({
            children: [
              new TableCell({
                width: { size: 50, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph(
                    listAnswer[0][_index]
                      ? `${index + _index}. ${listAnswer[0][_index]}`
                      : '',
                  ),
                ],
              }),
              new TableCell({
                width: { size: 50, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph(
                    listAnswer[1][_index]
                      ? `${String.fromCharCode(97 + _index)}. ${
                          listAnswer[1][_index]
                        }`
                      : '',
                  ),
                ],
              }),
            ],
          }),
      ),
    }),
  )

  return list
}

function ShortAnswerQuestion(question: any, index: number, list: any[]) {
  const questionDesArr = question.question_description?.split('\n')
  if (!questionDesArr) return list
  const questionDesParagraph = questionDesArr?.map(
    (item: string, mIndex: number) => {
      if (mIndex === 0) {
        item = `${index}. ${item}`
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: mIndex === questionDesArr.length - 1 ? 200 : 0,
        },
        indent: mIndex > 0 && {
          left: '0.15 in',
        },
        children: [
          new TextRun({
            text: item,
            bold: true,
          }),
        ],
      })
    },
  )
  if (questionDesParagraph) {
    list.push(...questionDesParagraph)
  }
  const questionTextArr = question.question_text?.split('\n')
  const questionTextParagraph = questionTextArr?.map(
    (item: string, mIndex: number) => {
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 150 : 0,
          after: mIndex === questionTextArr.length - 1 ? 150 : 0,
        },
        indent: {
          left: '0.15 in',
        },
        children: item?.split(/\%u\%/g).map(
          (item: any, m: number) =>
            new TextRun({
              text: item,
              ...(m % 2 === 1
                ? { underline: { type: UnderlineType.SINGLE } }
                : {}),
            }),
        ),
      })
    },
  )
  if (questionTextParagraph) {
    list.push(...questionTextParagraph)
  }
  const questionAnswerArr = question.answers?.split('\n')
  const questionAnswerParagraph = questionAnswerArr?.map(
    (item: string, mIndex: number) => {
      if (mIndex === 0) {
        item = `→ ${item}`
      }
      if (mIndex === questionAnswerArr.length - 1) {
        item = `${item}.............................`
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 150 : 0,
          after: mIndex === questionAnswerArr.length - 1 ? 150 : 0,
        },
        indent: mIndex > 0 && {
          left: '0.15 in',
        },
        text: item.replaceAll(/\%s\%/g, '___________'),
      })
    },
  )
  if (questionAnswerParagraph) {
    list.push(...questionAnswerParagraph)
  }

  return list
}

function TrueFalseQuestion(question: any, index: number, list: any[]) {
  const listAnswer: string[] = question.answers.split('#')

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: question.question_description.split('\n').map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  if (question.question_text) {
    ReadingScript(question.question_text, list)
  }
  list.push(new Paragraph({ text: '' }))
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          tableHeader: true,
          height: { value: 600, rule: HeightRule.ATLEAST },
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Answers', bold: true })],
                  indent: { start: 200 },
                }),
              ],
              width: { size: 80, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'True', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'False', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
          ],
        }),
        ...listAnswer.map(
          (answer: string, aIndex: number) =>
            new TableRow({
              height: { value: 500, rule: HeightRule.ATLEAST },
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      text: `${index + aIndex}. ${answer}`,
                      indent: { start: 200 },
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
              ],
            }),
        ),
      ],
    }),
  )
  return list
}
function TrueFalseQuestion4(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correcrAnswers = question.correct_answers.split('#')
  const images = question.image.split('#')

  const example: any = {
    answers: [],
    corrects: [],
    images: [],
  }
  const mainQuestion: any = {
    answers: [],
    corrects: [],
    images: [],
  }

  //cup example
  for (let i = 0; i < correcrAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        mainQuestion.corrects.push(correcrAnswers[i])
        mainQuestion.answers.push(answers[i])
        mainQuestion.images.push(images[i])
        continue
      } else {
        example.corrects.push(correcrAnswers[i])
        example.answers.push(answers[i].replace('*', ''))
        example.images.push(images[i])
        continue
      }
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: [
        new TextRun({
          text: question.question_description,
          bold: true,
        }),
      ],
    }),
  )

  if (example.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Examples:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.answers.forEach((answer: string, aIndex: number) => {
      const correct = example.corrects[aIndex]
      const imageUrl = `${uploadDir}/${example.images[aIndex]}`

      const imageSize = sizeOf(imageUrl)
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 3, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.CENTER,
                      children: [
                        new TextRun({
                          text: ``,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.LEFT,
                      children: [
                        new ImageRun({
                          data: fs.readFileSync(imageUrl),
                          transformation: {
                            width: 136,
                            height: (imageSize.height * 136) / imageSize.width,
                          },
                        }),
                        ,
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 38, type: WidthType.PERCENTAGE },
                  margins: { left: 150 },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: answer,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.END,
                      children: [
                        new TextRun({
                          text: correct == 'T' ? '☑' : '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' True        ',
                          color: trueColor,
                          bold: true,
                        }),
                        new TextRun({
                          text: correct == 'F' ? '☑' : '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' False  ',
                          color: falseColor,
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  if (mainQuestion.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Questions:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    mainQuestion.answers.forEach((answer: string, aIndex: number) => {
      const imageUrl = `${uploadDir}/${mainQuestion.images[aIndex]}`

      const imageSize = sizeOf(imageUrl)
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 3, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.CENTER,
                      children: [
                        new TextRun({
                          text: `${index + aIndex}. `,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.LEFT,
                      children: [
                        new ImageRun({
                          data: fs.readFileSync(imageUrl),
                          transformation: {
                            width: 136,
                            height: (imageSize.height * 136) / imageSize.width,
                          },
                        }),
                        ,
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 38, type: WidthType.PERCENTAGE },
                  margins: { left: 150 },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: `${answer}`,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.END,
                      children: [
                        new TextRun({
                          text: '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' True        ',
                          color: trueColor,
                          bold: true,
                        }),
                        new TextRun({
                          text: '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' False   ',
                          color: falseColor,
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
    })
  }

  return list
}
function TrueFalseQuestion3(question: any, index: number, list: any[]) {
  const answers = question.answers.split('#')
  const correcrAnswers = question.correct_answers.split('#')
  const image = question.image
  const imageUrl = `${uploadDir}/${image}`
  const example: any = {
    answers: [],
    corrects: [],
  }
  const mainQuestion: any = {
    answers: [],
    corrects: [],
  }

  //cup example
  for (let i = 0; i < correcrAnswers.length; i++) {
    if (answers[i]) {
      if (!answers[i].startsWith('*')) {
        mainQuestion.corrects.push(correcrAnswers[i])
        mainQuestion.answers.push(answers[i])
        continue
      } else {
        example.corrects.push(correcrAnswers[i])
        example.answers.push(answers[i].replace('*', ''))
        continue
      }
    }
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.question_text ? 200 : 0,
      },
      children: question.question_description.split('\n').map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  const imageSize = sizeOf(imageUrl)

  list.push(
    new Paragraph({
      spacing: { before: 200, after: 200 },
      alignment: AlignmentType.CENTER,
      children: [
        new ImageRun({
          data: fs.readFileSync(imageUrl),
          transformation: {
            width: (imageSize.width * 136) / imageSize.height,
            height: 136,
          },
        }),
      ],
    }),
  )
  if (example.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Examples:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    example.answers.forEach((answer: string, aIndex: number) => {
      const correct = example.corrects[aIndex]
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 50, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: answer,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.END,
                      children: [
                        new TextRun({
                          text: correct == 'T' ? '☑' : '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' True        ',
                          color: trueColor,
                          bold: true,
                        }),
                        new TextRun({
                          text: correct == 'F' ? '☑' : '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' False  ',
                          color: falseColor,
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
    })
  }
  if (mainQuestion.answers.length > 0) {
    list.push(
      new Paragraph({
        spacing: {
          before: 200,
        },
        children: [
          new TextRun({
            text: 'Questions:',
            bold: true,
            underline: {
              type: UnderlineType.SINGLE,
            },
          }),
        ],
      }),
    )
    mainQuestion.answers.forEach((answer: string, aIndex: number) => {
      list.push(
        new Table({
          margins: { top: 200 },
          borders: {
            top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
            insideHorizontal: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
            insideVertical: {
              style: BorderStyle.SINGLE,
              size: 1,
              color: '#ffffff',
            },
          },
          alignment: AlignmentType.END,
          width: { size: 98, type: WidthType.PERCENTAGE },
          rows: [
            new TableRow({
              children: [
                new TableCell({
                  width: { size: 50, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: `${index + aIndex}. ${answer}`,
                        }),
                      ],
                    }),
                  ],
                }),
                new TableCell({
                  width: { size: 12, type: WidthType.PERCENTAGE },
                  verticalAlign: VerticalAlign.CENTER,
                  children: [
                    new Paragraph({
                      alignment: AlignmentType.END,
                      children: [
                        new TextRun({
                          text: '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' True        ',
                          color: trueColor,
                          bold: true,
                        }),
                        new TextRun({
                          text: '☐',
                          color: '#9cabbd',
                          bold: true,
                        }),
                        new TextRun({
                          text: ' False   ',
                          color: falseColor,
                          bold: true,
                        }),
                      ],
                    }),
                  ],
                }),
              ],
            }),
          ],
        }),
      )
    })
  }

  return list
}
function MultiResponseQuestion1(question: any, index: number, list: any[]) {
  const answers: string[] = question.answers.split('#')
  const listAnswers: string[][] = []
  while (answers.length > 0) {
    listAnswers.push(answers.splice(0, 4))
  }
  const questionDesArr = question.question_description.split('\n')
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      if (mIndex === 0) {
        item = `${index}. ${item}`
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 150 : 0,
          after: mIndex === questionDesArr.length - 1 ? 150 : 0,
        },
        indent: mIndex > 0 && {
          left: '0.15 in',
        },
        children: [
          new TextRun({
            text: item,
            bold: true,
          }),
        ],
      })
    },
  )
  list.push(...questionDesParagraph)

  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: listAnswers.map(
        (items, itemsIndex) =>
          new TableRow({
            children: items.map(
              (item, itemIndex) =>
                new TableCell({
                  width: { size: 25, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      text: `${String.fromCharCode(
                        65 + itemsIndex * 4 + itemIndex,
                      )}. ${item}`,
                      indent: { left: '0.1 in' },
                    }),
                  ],
                }),
            ),
          }),
      ),
    }),
  )

  return list
}

function MultiResponseQuestion3(question: any, index: number, list: any[]) {
  const answers: string[] = question.answers.split('#')

  const questionDesArr = question.question_description.split('\n')
  const questionDesParagraph = questionDesArr.map(
    (item: string, mIndex: number) => {
      if (mIndex === 0) {
        item = `${index}. ${item}`
      }
      return new Paragraph({
        spacing: {
          before: mIndex === 0 ? 200 : 0,
          after: mIndex === questionDesArr.length - 1 ? 150 : 0,
        },
        indent: mIndex > 0 && {
          left: '0.15 in',
        },
        children: [
          new TextRun({
            text: item,
            bold: true,
          }),
        ],
      })
    },
  )
  list.push(...questionDesParagraph)

  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: answers.map(
        (answer: string, answerIndex: number) =>
          new TableRow({
            children: [
              new TableCell({
                width: { size: 100, type: WidthType.PERCENTAGE },
                children: [
                  new Paragraph({
                    indent: { left: '0.2 in' },
                    text: `${String.fromCharCode(65 + answerIndex)}. ${answer}`,
                  }),
                ],
              }),
            ],
          }),
      ),
    }),
  )

  return list
}

function SelectFromListQuestion1(question: any, index: number, list: any[]) {
  const listAnswers: string[][] = question.answers
    .split('#')
    .map((m: string) => m.split('*'))

  const columnWidth =
    90.0 / Math.max(...listAnswers.map((m: string[]) => m.length))

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.parent_question_text ? 200 : 0,
      },
      children: question.question_description.split('\n').map(
        (item: string, m: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: m > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  let questionText = question.question_text
  for (let i = 0; i < question.total_question; i++) {
    questionText = questionText.replace('%s%', `(${index + i})___________`)
  }
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: questionText.split('\n').map(
        (part: string, partIndex: number) =>
          new TextRun({
            text: part,
            break: partIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  list.push(
    new Table({
      margins: { top: 200 },
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#ffffff',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: listAnswers.map(
        (answers, answersIndex) =>
          new TableRow({
            children: [
              new TableCell({
                children: [
                  new Paragraph({
                    text: `(${index + answersIndex})`,
                    indent: { start: '0.2 in' },
                  }),
                ],
                width: { size: 10, type: WidthType.PERCENTAGE },
              }),
              ...answers.map(
                (answer, answerIndex) =>
                  new TableCell({
                    width: { size: columnWidth, type: WidthType.PERCENTAGE },
                    children: [
                      new Paragraph({
                        text: `${String.fromCharCode(
                          65 + answerIndex,
                        )}. ${answer}`,
                      }),
                    ],
                  }),
              ),
            ],
          }),
      ),
    }),
  )

  return list
}

function SelectFromListQuestion2(question: any, index: number, list: any[]) {
  const listImage = question.image.split('#')
  const listQuestion: string[] = question.question_text.split('#')
  const listAnswers = question.answers
    .split('#')
    .map((m: string) => m.split('*'))
    const columnWidth =
    90.0 / Math.max(...listAnswers.map((m: any) => m.length))
  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: question.parent_question_text ? 200 : 0,
      },
      children: question.question_description.split('\n').map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )

  listQuestion.forEach((ques, qIndex) => {
    const imageSize = sizeOf(`${uploadDir}/${listImage[qIndex]}`)
    list.push(
      new Paragraph({
        spacing: { before: 200, after: 200 },
        alignment: AlignmentType.CENTER,
        children: [
          new ImageRun({
            data: fs.readFileSync(`${uploadDir}/${listImage[qIndex]}`),
            transformation: {
              width: (imageSize.width * 136) / imageSize.height,
              height: 136,
            },
          }),
        ],
      }),
    )
  const answers = listAnswers[qIndex]
  const questionText = ques.replace(/\%s\%/g, '___________')
  list.push(
    new Paragraph({
      spacing: {
        before: 150,
        line: 300,
      },
      indent: {
        left: '0.2 in',
      },
      children: questionText.split('\n').map(
        (part: string, partIndex: number) =>
          new TextRun({
            text: `${index + qIndex + partIndex}. ${part}`,
            break: partIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
    list.push(
      new Table({
        alignment: AlignmentType.START,
        borders: {
          top: { style: BorderStyle.SINGLE, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, color: '#ffffff' },
          insideHorizontal: { style: BorderStyle.SINGLE, color: '#ffffff' },
          insideVertical: { style: BorderStyle.SINGLE, color: '#ffffff' },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: [
          new TableRow({
            children: answers.map((answer:string, aIndex:number)=>
            new TableCell({
              width: { size: columnWidth, type: WidthType.PERCENTAGE },
              children:[
                new Paragraph({
                  spacing:{
                    before: 150,
                    after:200,
                  },
                  indent: { start: '0.2 in' },
                  text: `${String.fromCharCode(
                    65 + aIndex,
                  )}. ${answer}`,
                }),
              ]
            })
            )
          })
        ],
      }),
    )
  })
 

  return list
}

function MIXQuestion1(question: any, index: number, list: any[]) {
  const listAnswers: string[] = question.answers.split('[]')
  const tfAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction = question.question_description.split('[]')
  const mcQuestions = question.question_text.split('[]')[1].split('#')

  const columnWidth =
    100.0 / Math.max(...mcAnswers.map((m: string) => m.split('*').length))

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 200,
      },
      children: question.parent_question_description
        .split('\n')
        .map((item: string, mIndex: number) => {
          return new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          })
        }),
    }),
  )
  ReadingScript(question.parent_question_text, list)
  const task1Arr = listInstruction[0].split('\n')
  const task1Paragraph = task1Arr.map((item: string, mIndex: number) => {
    if (mIndex === 0) {
      item = `Task 1: ${item}`
    }
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === task1Arr.length - 1 ? 150 : 0,
      },
      indent: mIndex > 0 && { left: '0.45 in' },
      children: [
        new TextRun({
          text: item,
          bold: true,
        }),
      ],
    })
  })
  list.push(...task1Paragraph)
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          tableHeader: true,
          height: { value: 600, rule: HeightRule.ATLEAST },
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Answers', bold: true })],
                  indent: { start: 200 },
                }),
              ],
              width: { size: 80, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'True', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'False', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
          ],
        }),
        ...tfAnswers.map(
          (answer: string, aIndex: number) =>
            new TableRow({
              height: { value: 500, rule: HeightRule.ATLEAST },
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      text: `${index + aIndex}. ${answer}`,
                      indent: { start: 200 },
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
              ],
            }),
        ),
      ],
    }),
  )

  const task2Arr = listInstruction[0].split('\n')
  const task2Paragraph = task2Arr.map((item: string, mIndex: number) => {
    if (mIndex === 0) {
      item = `Task 1: ${item}`
    }
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === task2Arr.length - 1 ? 150 : 0,
      },
      indent: mIndex > 0 && { left: '0.45 in' },
      children: [
        new TextRun({
          text: item,
          bold: true,
        }),
      ],
    })
  })
  list.push(...task2Paragraph)

  mcQuestions.forEach((questionT: string, qIndex: number) => {
    const answers = mcAnswers[qIndex].split('*')
    const questionTextArr = questionT.split('\n')
    const questionTParagraph = questionTextArr.map(
      (item: string, mIndex: number) => {
        if (mIndex === 0) {
          item = `${index + tfAnswers.length + qIndex}. ${item}`
        }
        return new Paragraph({
          spacing: {
            before: mIndex === 0 ? 200 : 0,
            after: mIndex === questionTextArr.length - 1 ? 150 : 0,
          },
          indent: mIndex > 0 && { left: '0.2 in' },
          children: [
            new TextRun({
              text: item.replaceAll(/\%s\%/g, '______________'),
              bold: true,
            }),
          ],
        })
      },
    )
    list.push(...questionTParagraph)
    list.push(
      new Table({
        margins: { top: 200 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: answers.map(
          (answer: string, answerIndex: number) =>
            new TableRow({
              children: [
                new TableCell({
                  width: { size: columnWidth, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      indent: { left: '0.2 in' },
                      text: `${String.fromCharCode(
                        65 + answerIndex,
                      )}. ${answer}`,
                    }),
                  ],
                }),
              ],
            }),
        ),
      }),
    )
  })

  return list
}

function MIXQuestion2(question: any, index: number, list: any[]) {
  const listAnswers: string[] = question.answers.split('[]')
  const lsAnswers: string[] = listAnswers[0].split('#')
  const mcAnswers: string[] = listAnswers[1].split('#')
  const listInstruction = question.question_description.split('[]')
  const mcQuestions = question.question_text.split('[]')[1].split('#')

  const columnWidth =
    100.0 / Math.max(...mcAnswers.map((m: string) => m.split('*').length))

  list.push(
    new Paragraph({
      spacing: {
        before: 200,
        after: 200,
      },
      children: question.parent_question_description.split('\n').map(
        (item: string, mIndex: number) =>
          new TextRun({
            text: item,
            bold: true,
            break: mIndex > 0 ? 1 : 0,
          }),
      ),
    }),
  )
  ReadingScript(question.parent_question_text, list)
  const task1Arr = listInstruction[0].split('\n')
  const task1Paragraph = task1Arr.map((item: string, mIndex: number) => {
    if (mIndex === 0) {
      item = `Task 1: ${item}`
    }
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === task1Arr.length - 1 ? 150 : 0,
      },
      indent: mIndex > 0 && { left: '0.45 in' },
      children: [
        new TextRun({
          text: item,
          bold: true,
        }),
      ],
    })
  })
  list.push(...task1Paragraph)
  list.push(
    new Table({
      borders: {
        top: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        left: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        bottom: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        right: { style: BorderStyle.SINGLE, size: 1, color: '#d5dee8' },
        insideHorizontal: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
        insideVertical: {
          style: BorderStyle.SINGLE,
          size: 1,
          color: '#d5dee8',
        },
      },
      width: { size: 100, type: WidthType.PERCENTAGE },
      rows: [
        new TableRow({
          tableHeader: true,
          height: { value: 600, rule: HeightRule.ATLEAST },
          children: [
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Answers', bold: true })],
                  indent: { start: 200 },
                }),
              ],
              width: { size: 70, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'True', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'False', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
            new TableCell({
              children: [
                new Paragraph({
                  children: [new TextRun({ text: 'Not given', bold: true })],
                  alignment: AlignmentType.CENTER,
                }),
              ],
              width: { size: 10, type: WidthType.PERCENTAGE },
              shading: { fill: '#c9ecf8' },
              verticalAlign: VerticalAlign.CENTER,
            }),
          ],
        }),
        ...lsAnswers.map(
          (answer: string, aIndex: number) =>
            new TableRow({
              height: { value: 500, rule: HeightRule.ATLEAST },
              children: [
                new TableCell({
                  children: [
                    new Paragraph({
                      text: `${index + aIndex}. ${answer}`,
                      indent: { start: 200 },
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
                new TableCell({
                  children: [
                    new Paragraph({
                      children: [
                        new TextRun({
                          text: '◯',
                          color: '#9cabbd',
                          bold: true,
                        }),
                      ],
                      alignment: AlignmentType.CENTER,
                    }),
                  ],
                  verticalAlign: VerticalAlign.CENTER,
                }),
              ],
            }),
        ),
      ],
    }),
  )

  const task2Arr = listInstruction[1].split('\n')
  const task2Paragraph = task2Arr.map((item: string, mIndex: number) => {
    if (mIndex === 0) {
      item = `Task 2: ${item}`
    }
    return new Paragraph({
      spacing: {
        before: mIndex === 0 ? 200 : 0,
        after: mIndex === task2Arr.length - 1 ? 150 : 0,
      },
      indent: mIndex > 0 && { left: '0.45 in' },
      children: [
        new TextRun({
          text: item,
          bold: true,
        }),
      ],
    })
  })
  list.push(...task2Paragraph)

  mcQuestions.forEach((questionT: string, qIndex: number) => {
    const answers = mcAnswers[qIndex].split('*')
    const questionTextArr = questionT.split('\n')
    const questionTParagraph = questionTextArr.map(
      (item: string, mIndex: number) => {
        if (mIndex === 0) {
          item = `${index + lsAnswers.length + qIndex}. ${item}`
        }
        return new Paragraph({
          spacing: {
            before: mIndex === 0 ? 200 : 0,
            after: mIndex === questionTextArr.length - 1 ? 150 : 0,
          },
          indent: mIndex > 0 && { left: '0.2 in' },
          children: [
            new TextRun({
              text: item.replaceAll(/\%s\%/g, '______________'),
              bold: true,
            }),
          ],
        })
      },
    )
    list.push(...questionTParagraph)
    list.push(
      new Table({
        margins: { top: 200 },
        borders: {
          top: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          left: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          bottom: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          right: { style: BorderStyle.SINGLE, size: 1, color: '#ffffff' },
          insideHorizontal: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
          insideVertical: {
            style: BorderStyle.SINGLE,
            size: 1,
            color: '#ffffff',
          },
        },
        width: { size: 100, type: WidthType.PERCENTAGE },
        rows: answers.map(
          (answer: string, answerIndex: number) =>
            new TableRow({
              children: [
                new TableCell({
                  width: { size: columnWidth, type: WidthType.PERCENTAGE },
                  children: [
                    new Paragraph({
                      indent: { left: '0.2 in' },
                      text: `${String.fromCharCode(
                        65 + answerIndex,
                      )}. ${answer}`,
                    }),
                  ],
                }),
              ],
            }),
        ),
      }),
    )
  })

  return list
}

export default handler
