import createHandler, { apiResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";

export const VOUCHER_HISTORY_STATUS = {
    WAIT: {
        code: 1,
    },
    PAID: {
        code: 2,
    },
    CANCEL: {
        code: 3,
    }
}

export const voucherValidate: any = async (voucher_code: string, user: any, order_price: number) => {
    const checkExistVoucher: any = await prisma.voucher.findFirst({
        where: {
            code: voucher_code,
            start_date: {
                lt: new Date()
            },
            end_date: {
                gt: new Date()
            },
            OR: [
                {
                    user_role_id: user.user_role_id
                },
                {
                    user_role_id: null
                }
            ]
        }
    })

    if (!checkExistVoucher?.id) {
        return {
            message: "voucher not found or invalid",
            error_code: 2,
        }
    }

    const checkIsUserUsedVoucher = await prisma.voucher_history.count({
        where: {
            user_id: user.id,
            voucher_id: checkExistVoucher.id,
            status: {
                not: VOUCHER_HISTORY_STATUS.CANCEL.code
            }
        }
    })

    if (checkIsUserUsedVoucher >= checkExistVoucher.per_of_user) {
        return {
            message: "The number of uses has expired for user",
            error_code: 3,
        }
    }

    const checkIsLimitedVoucher = await prisma.voucher_history.count({
        where: {
            voucher_id: checkExistVoucher.id,
            status: {
                not: VOUCHER_HISTORY_STATUS.CANCEL.code
            }
        }
    })

    if (checkIsLimitedVoucher >= checkExistVoucher.limited) {
        return {
            message: "The number of uses has expired for voucher",
            error_code: 4,
        }
    }

    switch (checkExistVoucher.discount_type) {
        case 0: { // Giảm giá theo %
            const discount_calculate = (checkExistVoucher.discount_value * order_price) / 100;
            checkExistVoucher.discount_price = Math.min(discount_calculate, checkExistVoucher.discount_max)
            break
        }

        case 1: { // Giảm giá theo vnd
            const discount_calculate = checkExistVoucher.discount_value;
            checkExistVoucher.discount_price = Math.min(discount_calculate, checkExistVoucher.discount_max)
            break
        }

        default:
            return {
                message: "voucher not found or invalid",
                error_code: 2,
            }
    }

    return checkExistVoucher
}

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const user = session.user;

            const body = req?.body;
            const voucher_code = body?.voucher_code;
            const order_price = body?.order_price;
            let checkExistVoucher: any = {};

            if (!voucher_code || typeof (voucher_code) !== "string") {
                return res.status(200).json({
                    message: "`voucher_code` is required field, type string",
                    error_code: 1,
                })
            }

            try {
                checkExistVoucher = await voucherValidate(voucher_code, user, order_price);

                if (checkExistVoucher.error_code) {
                    return res.status(200).json(checkExistVoucher)
                }

            } catch (error) {
                return res.status(200).json({ message: error, error_code: 5 })
            }

            return res.status(200).json(apiResponseDTO(true, checkExistVoucher));
        }
    },
});

export default handler