import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { USER_ROLES } from 'interfaces/constants'
import { query, queryWithTransaction } from 'lib/db'
import { userInRight } from 'utils'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  let user: any = {}
  if (!session) {
    return res.status(403).end('Forbidden')
  }
  if (session) {
    user = session?.user
    
  }
  const isSystem:any = userInRight([USER_ROLES.Operator],session)
  switch (req.method) {
    case 'GET':
      return getUnitTestIntegrateById()
    case 'DELETE':
      if(isSystem){
        return deleteUnitTestIntegrateById()
      }else{
        return res.status(400).end('Not Permission')
      }
      
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getUnitTestIntegrateById() {
    const { id } = req.query
    if (!id) return res.status(400).json({code:0, message: 'id required' })
    try {
    const result = await query<any>(`
    SELECT u.id,
    u.third_party_code,
    u.unit_test_id,
    u.created_date,
    u.updated_date,
    us_create.user_name as created_by,
    us_update.user_name as updated_by,
    ut.name, 
    ut.template_level_id, 
    ut.series_id, 
    ut.unit_test_origin_id 
    FROM unit_test_third_party as u 
    JOIN unit_test as ut ON u.unit_test_id = ut.id 
    left join user as us_create on u.created_by = us_create.id 
    left join user as us_update on u.updated_by = us_update.id 
    WHERE u.deleted = 0 AND u.id = ? AND ut.deleted = 0 LIMIT 1
    `,[id])
    return res.status(200).json({
        code:1,
        data: result[0],
      })
    } catch (e: any) {
      res.status(500).json({code:0, message: e.message })
    }
  }
  async function deleteUnitTestIntegrateById() {
    const { id } = req.query
    try {
      const findId = await query<any>(`SELECT unit_test_id FROM unit_test_third_party where deleted = 0 AND id = ? `,[id])
      if(findId.length < 0){
        return res.status(400).json({code:0,message:'Not found id'})
      }
      const listQ: any[] = [
        () => [`UPDATE unit_test_third_party SET deleted = 1 WHERE id = ? `,[id]]
      ]
      listQ.push(() =>{
        return [`UPDATE unit_test SET deleted = 1 WHERE id = ?`,[findId[0].unit_test_id]]
      })
      const result:any[] = await queryWithTransaction<any>(listQ)
      return res.status(200).json({
        data: result[0],
        message: `Delete ${id} successful`,
      })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
