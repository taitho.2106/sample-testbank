import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { PACKAGES, USER_ROLES } from '@/interfaces/constants'
import { query, queryWithTransaction } from 'lib/db'
import { userInRight } from 'utils'
import { unitTestSources } from '@/constants/index'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  let user: any = {}
  if (!session) {
    return res.status(403).end('Forbidden')
  }
  if (session) {
    user = session?.user
  }
  const isSystem = userInRight([USER_ROLES.Operator], session)
  if (isSystem) {
    switch (req.method) {
      case 'POST':
        return await createUnitTestIntegrate()
      case 'PUT':
        return await updateUnitTestIntegrate()
      default:
        return res.status(405).json({ code: 0, message: 'Method Not Allowed' })
    }
  } else {
    return res.status(400).json({ code: 0, message: 'Not permission' })
  }

  async function createUnitTestIntegrate() {
    const data = req.body
    const dateNow = new Date()
    const startDate = data?.start_date ? new Date(data.start_date) : null
    const endDate = data?.end_date ? new Date(data.end_date) : null
    try {
      // const confirmField = await query<any>(
      //   `SELECT g.deleted as gradeDeleted, s.deleted as seriesDeleted, u.*FROM unit_test as u
      //   LEFT JOIN series as s ON u.series_id = s.id
      //   LEFT JOIN grade as g ON u.template_level_id = g.id
      //   where u.deleted = 0 AND u.id = ?`,
      //   [data.id]
      // )
      // // console.log("confirmField---create", confirmField)
      // if((confirmField[0].gradeDeleted === 1 || confirmField[0].gradeDeleted === null) && confirmField[0].seriesDeleted === 0){
      //     return res.status(200).json({ code: 3, message: 'Không tạo mới được vì Grade không tồn tại.' })
      // }
      // else if((confirmField[0].gradeDeleted === 1 || confirmField[0].gradeDeleted === null) && (confirmField[0].seriesDeleted === 1 || confirmField[0].seriesDeleted === null)){
      //   return res.status(200).json({ code: 5, message: 'Không tạo mới được vì Grade và Series không tồn tại.' })
      // }
      // else if(confirmField[0].gradeDeleted === 0 && (confirmField[0].seriesDeleted === 1 || confirmField[0].seriesDeleted === null)){
      //   return res.status(200).json({ code: 4, message: 'Không tạo mới được vì Series không tồn tại.' })
      // }
      if(!data.series_id || data.series_id ==1){
        return res.status(200).json({ code: 4,  message: 'Vui lòng cập nhật chương trình cho đề thi trước',})
      }
      const listSeries: any[] = await query<any[]>(
        `SELECT * FROM series
          WHERE deleted = 0 AND id = ?`,
        [data.series_id],
      )
      if (listSeries.length == 0) {
        return res.status(200).json({
          code: 4,
          message: 'Vui lòng cập nhật chương trình cho đề thi trước',
        })
      }
      const listGrade: any[] = await query<any[]>(
        `SELECT * FROM grade
          WHERE deleted = 0 AND id = ?`,
        [data.template_level_id],
      )
      if (listGrade.length == 0) {
        return res.status(200).json({
          code: 3,
          message: 'Vui lòng cập nhật khối lớp cho đề thi trước',
        })
      }
      //check unit_test and unit_test_third_party
      const response: any[] = await query<any>(
        `select utt.id from unit_test ut
        join unit_test_third_party utt on ut.id = utt.unit_test_id
        where ut.deleted = 0 and utt.deleted = 0 and
         ut.unit_test_origin_id = ? and utt.third_party_code = ? `,
        [data.unit_test_origin_id, data.third_party_code],
      )
      if (response.length > 0) {
        return res
          .status(200)
          .json({
            code: 2,
            message: 'Đã có đề thi tích hợp tương tự.',
            data: JSON.parse(JSON.stringify(response[0].id)),
          })
      }
      const listIndex: any = { index: 0, sections: [] }
      let index = 0
      const listQ: any[] = [
        () => [
          `INSERT INTO unit_test(
            template_id, 
            name, 
            template_level_id, 
            total_question, 
            time, 
            is_publish, 
            created_by, 
            created_date, 
            total_point, 
            start_date, 
            end_date, 
            scope, 
            unit_type,
            series_id,
            unit_test_origin_id,
            unit_test_type,
            package_level,
            is_international_exam,
            source
          ) 
          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [
            data.template_id,
            data.name,
            data.template_level_id,
            data.total_question ?? 0,
            data.time,
            data.is_publish ?? 0,
            user.id,
            dateNow,
            data.total_point,
            startDate,
            endDate,
            data?.scope || 0,
            ['0', '1'].includes(data?.unit_type) ? parseInt(data.unit_type) : 0,
            data.series_id || null,
            data.unit_test_origin_id || null,
            data.unit_test_type ?? null,
            data.package_level,
            data.is_international_exam || 0,
            unitTestSources.I_TEST,
          ],
        ],
      ]
      data.sections.forEach((section: any, sectionIndex: number) => {
        listQ.push((r1: any, r2: any) => {
          return [
            `INSERT INTO unit_test_section(unit_test_id, section, modified_date) VALUES(?,?,?)`,
            [
              r2[listIndex.index].insertId,
              section?.section ? section.section : '',
              dateNow,
            ],
          ]
        })
        index++
        listIndex.sections.push({ index, parts: [] })
        section.parts.forEach((part: any, partIndex: number) => {
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section_part(unit_test_section_id, name, question_types, total_question, points, modified_date) 
               VALUES(?,?,?,?,?,?)`,
              [
                r2[listIndex.sections[sectionIndex].index].insertId,
                part.name,
                part.question_types,
                part.total_question,
                part.points,
                dateNow,
              ],
            ]
          })
          index++
          listIndex.sections[sectionIndex].parts.push({ index })
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO unit_test_section_part_question(unit_test_section_part_id, question_id, modified_date) VALUES ?`,
              [
                part.questions.map((question: any) => {
                  return [
                    r2[listIndex.sections[sectionIndex].parts[partIndex].index]
                      .insertId,
                    question?.id,
                    dateNow,
                  ]
                }),
              ],
            ]
          })
          index++
        })
      })
      listQ.push((r1: any, r2: any) => [
        `INSERT INTO unit_test_third_party(third_party_code, unit_test_id, deleted, created_by, created_date) VALUES(?,?,?,?,?)`,
        [
          data?.third_party_code,
          r2[listIndex.index].insertId,
          0,
          user.id,
          dateNow,
        ],
      ])
      const result: any[] = await queryWithTransaction<any>(listQ)
      if (result[0].affectedRows === 0) {
        return res
          .status(200)
          .json({ code: 0, message: `Create unsuccessfull` })
      }
      return res
        .status(200)
        .json({ code: 1, id: result[0].insertId, userId: user.id })
    
    } catch (e: any) {
      console.log('message===', e.message)
      res.status(500).json({ code: 0, message: e.message })
    }
  }

  async function updateUnitTestIntegrate() {
    const data: any = req.body
    const dateNow = new Date()
    try {
      const unitTest = await query<any>(
        `SELECT 1 FROM unit_test_third_party WHERE deleted = 0 AND id = ? LIMIT 1`,
        [data.id],
      )
      if (unitTest.length === 0) {
        return res.status(200).json({ code: 0, message: 'data not found' })
      }
      if(!data.series_id || data.series_id ==1){
        return res.status(200).json({ code: 4,  message: 'Vui lòng cập nhật chương trình cho đề thi trước',})
      }
      const listSeries: any[] = await query<any[]>(
        `SELECT * FROM series
          WHERE deleted = 0 AND id = ?`,
        [data.series_id],
      )
      if (listSeries.length == 0) {
        return res.status(200).json({
          code: 4,
          message: 'Vui lòng cập nhật chương trình cho đề thi trước',
        })
      }
      const listGrade: any[] = await query<any[]>(
        `SELECT * FROM grade
          WHERE deleted = 0 AND id = ?`,
        [data.template_level_id],
      )
      if (listGrade.length == 0) {
        return res.status(200).json({
          code: 3,
          message: 'Vui lòng cập nhật khối lớp cho đề thi trước',
        })
      }
      // const confirmField = await query<any>(
      //   `SELECT g.deleted as gradeDeleted, s.deleted as seriesDeleted, u.*FROM unit_test as u
      //   LEFT JOIN series as s ON u.series_id = s.id
      //   LEFT JOIN grade as g ON u.template_level_id = g.id
      //   where u.deleted = 0 AND u.id = ?`,
      //   [data.unit_test_id]
      // )
      // // console.log("confirmField---update", confirmField)
      // if((confirmField[0].gradeDeleted === 1 || confirmField[0].gradeDeleted === null) && confirmField[0].seriesDeleted === 0){
      //     return res.status(200).json({ code: 3, message: 'Grade không tồn tại.' })
      // }
      // else if((confirmField[0].gradeDeleted === 1 || confirmField[0].gradeDeleted === null) && (confirmField[0].seriesDeleted === 1 || confirmField[0].seriesDeleted === null)){
      //   return res.status(200).json({ code: 5, message: 'Grade và Series không tồn tại.' })
      // }
      // else if(confirmField[0].gradeDeleted === 0 && (confirmField[0].seriesDeleted === 1 || confirmField[0].seriesDeleted === null)){
      //   return res.status(200).json({ code: 4, message: 'Series không tồn tại.' })
      // }

      //check data in unit_test_third_party
      //check unit_test and unit_test_third_party
     const check: any[] = await query<any>(
      `select utt.id from unit_test ut
      join unit_test_third_party utt on ut.id = utt.unit_test_id
      where ut.deleted = 0 and utt.deleted = 0 and
       ut.unit_test_origin_id = ? and utt.third_party_code = ? and utt.id not in (?) `,
      [data.unit_test_origin_id, data.third_party_code,data.id],
    )
    if (check.length > 0) {
      return res
        .status(200)
        .json({
          code: 2,
          message: 'Đã có đề thi tích hợp tương tự.',
          data: JSON.parse(JSON.stringify(check[0].id)),
        })
    }
      const response: any = await query<any>(
        'UPDATE unit_test_third_party SET third_party_code = ? , updated_by = ?, updated_date = ? where id = ? ',
        [data.third_party_code, user?.id, dateNow, data.id],
      )
      if (response.affectedRows === 0) {
        return res
          .status(200)
          .json({ code: 0, message: `Update unsuccessful` })
      }
      return res
        .status(200)
        .json({ code: 1, id: data.unit_test_id, message: 'Update successful' })
      
    } catch (e: any) {
      res.status(500).json({ code: 0, message: e.message })
    }
  }
}
export default handler
