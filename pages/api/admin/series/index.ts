import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from 'lib/db' 

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    const queryRequest:any = req.query;
    const page:number = queryRequest.page ?? 1;
    const limit:number = queryRequest.limit ?? 10;
    const dataBody = req.body;
    const { title, typeArr, grade } = dataBody
    const user: any = session?.user
    if (user?.is_admin !== 1) {
      return res.status(403).json({message:'Not permission'})
    }
    switch (req.method) {
        case 'GET':
            return getAllGradeId()
        case 'POST':
            return getSeries()
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
    async function getAllGradeId() {
        try {
            const results: any = await query<any>(
                `SELECT * FROM series WHERE deleted = 0 ORDER BY grade_eduhome_id`,
                [],
            )
            return res.status(200).json({
                data: results,
            })
        } catch (error: any) {
            return res.status(500).json({ message: error.message })
        }
    }

    async function getSeries () {
        let queryStr =  `SELECT * FROM series as s WHERE s.deleted = 0`
        let queryCount = `SELECT COUNT(*) as total FROM series s WHERE s.deleted = 0 `;
        
        const params = []
        if(typeArr && typeArr.length > 0){
            if(typeArr.length == 1 && typeArr[0] == 2){
                queryStr+= ` AND s.series_type != ? `;
                queryCount+= ` AND s.series_type != ? `;
                params.push([1]);
            }else{
                queryStr+= ` AND s.series_type IN(?) `;
                queryCount+= ` AND s.series_type IN(?) `;
                params.push(typeArr);
            }
            
            
        }
        if(title && title.length > 0){
            queryStr+= ` AND s.name LIKE ? `;
            queryCount+= ` AND s.name LIKE ? `
            params.push(`%${title}%`);
        }
        if(grade && grade.length>0){
            queryStr+= ` AND s.grade_id IN (?) `;
            queryCount+= ` AND s.grade_id IN (?) `;
            params.push(grade);
        }

        if (page !== undefined && page > 0) {
            queryStr += ` LIMIT ${limit} OFFSET ${(page-1)*limit} `
        }
        const results: any[] = await query(queryStr, params)
        const total = await query<any[]>(queryCount, params)
        
        return res.json({
            data: results,
            totalPage: Math.ceil(total[0].total / limit),
            totalRecords: total[0].total,
        })
    }
}

export default handler