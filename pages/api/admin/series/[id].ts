import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from '../../../../lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    const user: any = session?.user
    if (user?.is_admin !== 1) {
        return res.status(403).json({message:'Not permission'})
    }
    switch (req.method) {
        case 'GET':
            return getSeriesById()
        case 'PUT':
            return updateSeries()
        case 'POST':
            return createSeries()
        case 'DELETE':
            return deleteSeries()
        default:
            return res.status(405).json({message:`Method ${req.method} Not Allowed`})
    }
    async function getSeriesById() {
        const id = req.query.id
        try {
            const results: any = await query<any>(
                `SELECT * FROM series where deleted = 0 AND id = ?`,
                [id],
            )
            return res.status(200).json({
                data: results,
                code: 1,
                message: 'Get series success'
            })
        } catch (error: any) {
            return res.status(500).json({ code: 0, message: error.message })
        }
    }

    async function updateSeries() {
        const idOrigin = req.query.id
        const {id, name, priority} = req.body
        const dateNow = new Date()
        try {
            const seriesId:any = await query<any>(
                `SELECT * FROM series WHERE deleted = 0 AND id = ?`,[idOrigin]
            )
            if(seriesId){
                if(!req.body || !Boolean(name) || !Boolean(priority)){
                    return res.status(400).json({code: 0, message:"Data body fail"})
                }
                const results:any = await query<any>(
                    `UPDATE series SET name = ? , priority = ?, updated_by = ?, updated_date = ? WHERE deleted = 0 AND id = ?`,
                    [name, priority, user.id, dateNow, idOrigin]
                )
                if(results.affectedRows === 0){
                    return res.status(400).json({code: 0, message:'update series unsuccessful'})
                }  
                return res.status(200).json({
                    data: results, 
                    code: 1, 
                    message:'update series successful'
                })
            } else{
                return res.status(400).json({
                    code: 0, 
                    message:`Do not have seriesId = ${idOrigin} `
                })
            }
        } catch (error:any) {
            return res.status(500).json({code: 0, message: error.message})
        }
    }
    
    async function createSeries() {
        const { id, name, priority } = req.body
        const dateNow = new Date()
        try {
            if (!req.body || !Boolean(name) || !Boolean(priority)) {
                return res.status(400).json({ message: 'Data body fail' })
            } else {
                const checkId:any = await query<any>(
                    `SELECT * FROM series WHERE id = ?`,[id]
                )
                if(checkId.length === 0){
                    const results: any = await query<any>(
                    `INSERT INTO series(
                        id,
                        name, 
                        priority, 
                        created_by,
                        created_date,
                        deleted
                        ) 
                        VALUES(?,?,?,?,?,?)
                        `,
                    [id, name, priority, user.id, dateNow, 0],
                    )
                    if(results.affectedRows === 1){
                        return res
                        .status(200)
                        .json({ 
                            data: results, 
                            code: 1, 
                            message: 'Thêm chương trình mới thành công' 
                        })
                    }
                    return res.json({code: 0, message: 'Tạo mới chương trình không thành công'})
                }
                if(checkId.length === 1 && checkId[0].deleted === 1){
                    const results:any = await query<any>(
                        `UPDATE series SET name = ? , priority = ?, deleted = ?, updated_by = ?, updated_date = ? WHERE id = ?`,
                        [name, priority, 0, user.id, dateNow, id]
                    )
                    if(results.affectedRows === 1){
                        return res
                        .status(200)
                        .json({ 
                            data: results, 
                            code: 1, 
                            message: 'Thêm chương trình mới thành công'
                        })
                    }
                    return res.json({code: 0, message: 'Tạo mới chương trình không thành công'})
                }
                return res.json({code: 2, message: 'Mã chương trình này đã có trong hệ thống'})
            }
        } catch (error: any) {
          return res.status(500).json({ message: error.message })
        }
    }

    async function deleteSeries() {
        const { id } = req.query
        try {
            const results: any = await query<any>(
                `UPDATE series SET deleted = 1 where id= ?`,
                [id],
            )
            if(results.affectedRows === 1){
                return res.status(200).json({
                    data: results,
                    message: `Delete series ${id} successful`,
                    code: 1,
                })
            }
            return res.json({code:0, message: 'delete failed'})
        }
        catch (error: any) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export default handler;