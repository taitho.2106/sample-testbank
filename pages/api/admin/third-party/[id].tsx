import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
import { query } from 'lib/db' 

enum THIRDPARTY_RESPONSE_MESSAGE {
    'Tạo mới đối tác không thành công',
    'Thêm đối tác mới thành công',
    'Mã đối tác này đã có trong hệ thống',
    'Lấy dữ liệu đối tác thành công',
    'Không tìm thấy dữ liệu để cập nhật',
    'Cập nhật đối tác thành công',
    'Xoá đối tác thành công',
    'Xoá đối tác thất bại'
}

enum THIRDPARTY_RESPONSE_STATUS{
    'Permission denied',
    'Method Not Allowed'
}

const ThirdPartyAPI = async (request: NextApiRequest, response: NextApiResponse) => {
    const session = await getSession({ req: request })
    const user: any = session?.user

    if (user?.is_admin !== 1) return response.status(403).json({message: THIRDPARTY_RESPONSE_STATUS[0]})

    // Join third_party and third_party_config THEN select it
    const _SParamsMainAndConfig = [
        'main.id as code_id', 'main.display','main.priority',
        'config.id as config_id', 'config.grade_id', 'config.series_id',
        'config.child_app_id','config.secret_key'
    ]
    const querySelectMainAndConfig = [
        `SELECT ${_SParamsMainAndConfig.join(', ')}`, 
        'FROM third_party as main',
        'LEFT JOIN third_party_config as config',
        'ON main.id = config.third_party_code',
        'WHERE main.deleted = 0 AND (config.deleted is null or config.deleted = 0)',
        'AND main.id = ?'
    ]

    // Declare Insert
    const _IParamsMain = ['id', 'display', 'priority', 'deleted', 'created_date']
    const queryInsertMain = [
        `INSERT INTO third_party(${_IParamsMain.join(', ')})`,
        'VALUES( ?, ?, ?, ?, ?)'
    ]
    const _IParamsConfig = [ 
        'third_party_code', 'grade_id', 'series_id', 'child_app_id', 'secret_key', 'deleted' ]
    const queryInsertConfig = [
        `INSERT INTO third_party_config(${_IParamsConfig.join(', ')})`,
        'VALUES( ?, ?, ?, ?, ?, ?)'
    ]

    // Declare Update
    const _UParmasMain = ['display = ?', 'priority = ?', 'deleted = ?', 'updated_date = ?' ]
    const queryUpdateMain = [
        'UPDATE third_party', `SET ${_UParmasMain.join(', ')}`, 'WHERE id = ?'
    ]
    const _UParmasConfig = [ 
        'grade_id = ?', 'series_id = ?', 'child_app_id = ?', 'secret_key = ?', 'deleted = ?' ]
    const queryUpdateConfig = [
        `UPDATE third_party_config`, `SET ${_UParmasConfig.join(', ')}`, 'WHERE third_party_code = ?'
    ]

    //Declare Select
    const querySelectMain = [
        'SELECT *', 'FROM third_party', 'WHERE id = ?']
    
    const querySelectConfig = [
        'SELECT *', 'FROM third_party_party', 'WHERE third_party_code = ?']

    switch (request.method) {
        case 'GET':
            return getThirdPartyById()
        case 'PUT':
            return updateThirdParty()
        case 'POST':
            return createThirdParty()
        case 'DELETE':
            return deleteThirdParty()
        default:
            return response.status(405).json({message: THIRDPARTY_RESPONSE_STATUS[1]})
    }

    async function getThirdPartyById() {
        const id = request.query.id
        try {
            const results: any = await query<any>( `${querySelectMainAndConfig.join(' ')}`, [id] )

            return response.status(200).json({
                data: results, code: 1, message: THIRDPARTY_RESPONSE_MESSAGE[3]
            })

        } catch (error: any) {
            return response.status(500).json({ code: 0, message: error.message })
        }
    }

    async function updateThirdParty() {
        const code_id = request.query.id
        const {display, priority, child_app_id, 
            series_id, grade_id, secret_key} = request.body
        try {

            const record:any = await query<any>( 
                `SELECT * FROM third_party WHERE deleted = 0 AND id = ?`, [code_id] )

            if( record.affectedRows == 0 ){
                return response.status(400).json({code: 0, message: THIRDPARTY_RESPONSE_MESSAGE[4]})
            }
            
            const stringMain = ['display = ?', 'priority = ?', 'deleted = 0', 'updated_date = ?']
            const stringUpdateMain = [
                'UPDATE third_party',
                `SET ${stringMain.join(', ')}`,
                'WHERE id = ?'
            ]
            //update main
            const recordMain = await query<any>(
                `${stringUpdateMain.join(' ')}`,
                [display, priority,new Date(), code_id]
            )

            if( recordMain.affectedRows == 0 ) 
                return response.json({code: 2, message: THIRDPARTY_RESPONSE_MESSAGE[0]})
            
             //check config exist   
            const recordExist:any = await query<any>( 
                `SELECT * FROM third_party_config WHERE deleted = 0 AND third_party_code = ?`, [code_id] )
            
            if(recordExist.length==0){ //insert config
                const resultConfig = await query<any>(
                    `${queryInsertConfig.join(' ')}`, 
                    [code_id, grade_id, series_id, child_app_id, secret_key, 0]
                )
                if( resultConfig.affectedRows == 0 ) {
                    return response.json({code: 2, message: THIRDPARTY_RESPONSE_MESSAGE[0]})
                }
            }else{ //update config
                const stringConfig = [ 
                    'grade_id = ?', 'series_id = ?', 'child_app_id = ?', 'secret_key = ?' ]
                const stringUpdateConfig = [
                    `UPDATE third_party_config`,
                    `SET ${stringConfig.join(', ')}`,
                    'WHERE third_party_code = ? and deleted = 0'
                ]
    
                const recordConfig = await query<any>(
                    `${stringUpdateConfig.join(' ')}`,
                    [grade_id, series_id, child_app_id, secret_key, code_id]
                )
    
                if( recordConfig.affectedRows == 0 ) 
                    return response.json({code: 2, message: THIRDPARTY_RESPONSE_MESSAGE[0]})
            }
            
            const recordUpdate: any = await query<any>( 
                `${querySelectMainAndConfig.join(' ')}`, [code_id] )

            return response.status(200).json({
                data: recordUpdate, 
                code: 1, 
                message: THIRDPARTY_RESPONSE_MESSAGE[5]
            })
            
        } catch (error:any) {
            return response.status(500).json({code: 0, message: error.message})
        }
    }
    
    async function createThirdParty() {
        const { code_id, display, priority, grade_id, 
            series_id, child_app_id, secret_key } = request.body

        try {
            if ( !request.body ) {
                return response.status(400).json({ message: THIRDPARTY_RESPONSE_MESSAGE[4] })
            } else {

                const recordExist = await query<any>( 
                    'SELECT * FROM third_party WHERE id = ?', [code_id])

                if(recordExist.length > 0){
                    if(recordExist[0].deleted == 0){
                        return response.json({code: 2, message: THIRDPARTY_RESPONSE_MESSAGE[2]})
                    }
                    // Update row had been deleted
                    const recordMain = await query<any>( 
                        `${queryUpdateMain.join(' ')}`, 
                        [display, priority, 0, new Date(), code_id]
                    )
                    if( recordMain.affectedRows == 0 ) {
                        return response.json({code: 2, message: THIRDPARTY_RESPONSE_MESSAGE[0]})
                    }
                    
                    const recordConfig = await query<any>( 
                        `${queryInsertConfig.join(' ')}`, 
                        [code_id, grade_id, series_id, child_app_id, secret_key, 0]
                    )
                    if( recordConfig.affectedRows == 0 ) {
                        return response.json({code: 2, message: THIRDPARTY_RESPONSE_MESSAGE[0]})
                    }
                }else{
                    // Create if not have
                    const recordMain = await query<any>( 
                        `${queryInsertMain.join(' ')}`, 
                        [code_id, display, priority, 0, new Date()]
                    )
                    if( recordMain.affectedRows == 0 ) {
                        return response.json({code: 2, message: THIRDPARTY_RESPONSE_MESSAGE[0]})
                    }
                    
                    const recordConfig = await query<any>(
                        `${queryInsertConfig.join(' ')}`, 
                        [code_id, grade_id, series_id, child_app_id, secret_key, 0]
                    )
                    if( recordConfig.affectedRows == 0 ) {
                        return response.json({code: 2, message: THIRDPARTY_RESPONSE_MESSAGE[0]})
                    }
                }
                
                const recordNew: any = await query<any>( 
                    `${querySelectMainAndConfig.join(' ')}`, [code_id] )

                return response.status(200).json({
                    data: recordNew,
                    code: 1,
                    message: THIRDPARTY_RESPONSE_MESSAGE[1],
                })
            }
        } catch (error: any) {
          return response.status(500).json({ message: error.message })
        }
    }

    async function deleteThirdParty() {
        const code_id = request.query.id
        try {
            const recordMain: any = await query<any>(
                `UPDATE third_party SET deleted = 1 where id = ?`,
                [code_id],
            )
            if( recordMain.affectedRows == 0 ){
                return response.status(400).json({code: 0, message: THIRDPARTY_RESPONSE_MESSAGE[6]})
            }

            const recordConfig: any = await query<any>(
                `UPDATE third_party_config SET deleted = 1 where third_party_code = ? and deleted = 0`,
                [code_id],
            )
            if( recordConfig.affectedRows == 0 ){
                return response.status(400).json({code: 0, message: THIRDPARTY_RESPONSE_MESSAGE[6]})
            }

            return response.status(200).json({
                data: [],
                message: THIRDPARTY_RESPONSE_MESSAGE[5],
                code: 1,
            })
        }
        catch (error: any) {
            return response.status(500).json({ message: error.message })
        }
    }
}

export default ThirdPartyAPI