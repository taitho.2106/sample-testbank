import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
import { query } from 'lib/db' 

const ThirdPartyAPI = async (request: NextApiRequest, response: NextApiResponse) => {
    const session = await getSession({req:request})
    const user: any = session?.user

    if( user?.is_admin !== 1 ) return response.status(403).json({message:'Permission denied'})

    switch (request.method) {
        case 'GET':
            return queryAndGetData()
        default:
            return response.status(405).end(`Method ${request.method} Not Allowed`)
    }

    async function queryAndGetData () {
        const selectString = [
            'main.id as code_id', 
            'main.display',
            'main.priority',
            'main.created_date',
            'main.updated_date',
            'config.id as config_id',
            'config.grade_id',
            'config.series_id',
            'config.child_app_id',
            'config.secret_key'
        ]
        const sqlStringArray = [
            `SELECT ${selectString.join(', ')}`, 
            'FROM third_party as main',
            'LEFT JOIN third_party_config as config',
            'ON main.id = config.third_party_code',
            'WHERE main.deleted = 0 and config.deleted = 0',
            'ORDER BY main.priority ASC, main.id ASC'            
        ]
        const resultJoin: any[] = await query( `${sqlStringArray.join(' ')}`, [])
        const mainResult: any[] = await query( 'select id as code_id, display, priority from third_party where deleted =0', [])
        const result = mainResult.map((item:any)=>{
            const itemJoin = resultJoin.find(m=> m.code_id == item.code_id);
            if(itemJoin){
                return {...itemJoin, date:itemJoin.updated_date||itemJoin.created_date};
            }else{
                return {...item,date:null};
            }
        }).sort((a:any,b:any)=>{
            const dateA = new Date(a.date).getTime();
            const dateB = new Date(b.date).getTime();
            return dateA <= dateB ? 1 : -1;  
        })
        return response.json({ data: result })
    }
}

export default ThirdPartyAPI