import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import api from 'lib/api'
import { query } from 'lib/db'
import { paths } from 'api/paths'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    let user: any = {}
    if (session) {
      user = session?.user
    }
    if (user?.is_admin !== 1) {
        return res.status(403).json({message:'Not permission'})
    }
    switch (req.method) {
        case 'POST':
            return await getAllSeries()
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }

    async function getAllSeries() {
        const eduHomeSeries: any = await api.get(
            `${process.env.NEXT_PUBLIC_EDUHOME_BASE_API}${paths.api_eduhome_get_series}`,
            { headers: { Authorization: `${process.env.EDUHOME_AUTH}` } },
        )
        const dataEDH = eduHomeSeries.data.data
        const listGrade = await query<any[]>(
            'SELECT id, grade_eduhome_id FROM grade where deleted =0 and grade_eduhome_id is not null',
            [],
        )
        const dateNow = new Date()
        const objGrade:any = {}
        if(listGrade.length >0){
            const lenGrade = listGrade.length;
            for(let i=0; i< lenGrade; i++){
                objGrade[`${listGrade[i].grade_eduhome_id}`] = listGrade[i].id
            }
        }
        try {
            if(dataEDH.length > 0){                
                for (let i = 0; i < dataEDH.length; i++){
                    const checkId:any = await query<any>(
                        `SELECT id FROM series WHERE series_eduhome_id = ? and deleted = 0`,[dataEDH[i].Id]
                    )
                    if(checkId.length >0){
                        const seriesItem:any = await query<any>(
                            `UPDATE series SET 
                                name = ? , 
                                priority = ?, 
                                grade_eduhome_id = ?, 
                                series_eduhome_id = ?, 
                                image_url = ?, 
                                series_type = ?, 
                                deleted = ?,
                                grade_id = ?,
                                updated_by = ?,
                                updated_date = ? 
                                WHERE id = ?`,
                            [   
                                dataEDH[i].Name, 
                                dataEDH[i].Priority, 
                                dataEDH[i].IdGrade, 
                                dataEDH[i].Id, 
                                dataEDH[i].Image, 
                                dataEDH[i].Type, 
                                0,
                                objGrade[`${dataEDH[i].IdGrade}`] || null,
                                user.id,
                                dateNow,
                                checkId[0].id, 
                            ]
                        )   
                    }
                    else if(checkId.length === 0){
                        const seriesItem:any = await query<any>(
                            `INSERT INTO series (
                                name, 
                                priority, 
                                grade_eduhome_id, 
                                series_eduhome_id, 
                                image_url, 
                                series_type, 
                                deleted,
                                grade_id,
                                created_by,
                                created_date
                                ) 
                            VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`,
                            [
                                dataEDH[i].Name, 
                                dataEDH[i].Priority, 
                                dataEDH[i].IdGrade, 
                                dataEDH[i].Id,  
                                dataEDH[i].Image, 
                                dataEDH[i].Type, 
                                0,
                                objGrade[`${dataEDH[i].IdGrade}`] || null,
                                user.id,
                                dateNow
                            ]
                        )
                    }
                }
                return res.status(200).json({
                    code: 1, 
                    message:'Đã update thêm series từ EduHome thành công'
                })                
            }
        } catch (error:any) {
            console.log("update series from eduhome----", error)
            return res.status(500).json({code: 0, message: error.message})
        }    
    }
}
export default handler