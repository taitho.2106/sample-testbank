import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from 'lib/db' 

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    const user: any = session?.user
    if (user?.is_admin !== 1) {
      return res.status(403).json({message:'Permission denied'})
    }
    switch (req.method) {
        case 'GET':
            return getSkills()
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
    async function getSkills () {
        const results: any[] = await query(
            `SELECT * FROM skills WHERE deleted = 0  AND priority >= 1
            ORDER BY priority ASC, id ASC`, [],
        )
        return res.json({
            data: results,
        })
    }
}

export default handler