import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from '../../../../lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    const user: any = session?.user
    if (user?.is_admin !== 1) {
        return res.status(403).json({message:'Permission denied'})
    }
    switch (req.method) {
        case 'GET':
            return getSkillsById()
        case 'PUT':
            return updateSkills()
        case 'POST':
            return createSkills()
        case 'DELETE':
            return deleteSkills()
        default:
            return res.status(405).json({message:`Method ${req.method} Not Allowed`})
    }
    async function getSkillsById() {
        const id = req.query.id
        try {
            const results: any = await query<any>(
                `SELECT * FROM skills where deleted = 0 AND id = ?`,
                [id],
            )
            return res.status(200).json({
                data: results,
                code: 1,
                message: 'Get the skill sucessful'
            })
        } catch (error: any) {
            return res.status(500).json({ code: 0, message: error.message })
        }
    }

    async function updateSkills() {
        const idOrigin = req.query.id
        const {id, name, priority} = req.body
        try {
            const idSkill:any = await query<any>(
                `SELECT * FROM skills WHERE deleted = 0 AND id = ?`,[idOrigin]
            )
            if(idSkill){
                if(!req.body || !Boolean(name) || !Boolean(priority)){
                    return res.status(400).json({code: 0, message:"Data body fail"})
                }
                const results:any = await query<any>(
                    `UPDATE skills SET name = ? , priority = ?, updated_date = ?, updated_by = ? WHERE deleted = 0 AND id = ?`,
                    [name, priority, new Date(), user.id, idOrigin]
                )
                if(results.affectedRows === 0){
                    return res.status(400).json({code: 0, message:'Failed to update'})
                }  
                return res.status(200).json({
                    data: results, 
                    code: 1, 
                    message:'Update successful'
                })
            } else{
                return res.status(400).json({
                    code: 0, 
                    message:`Do not have id = ${idOrigin} `
                })
            }
        } catch (error:any) {
            return res.status(500).json({code: 0, message: error.message})
        }
    }
    
    async function createSkills() {
        const { id, name, priority } = req.body
        try {
            if (!req.body || !Boolean(name) || !Boolean(priority)) {
                return res.status(400).json({ message: 'Data body fail' })
            } else {
                const checkId:any = await query<any>(
                    `SELECT * FROM skills WHERE id = ?`,[id]
                )
                if(checkId.length === 0){
                    const results: any = await query<any>(
                    `INSERT INTO skills(
                        id,
                        name, 
                        priority, 
                        deleted,
                        created_date,
                        created_by
                        ) 
                        VALUES(?,?,?,?,?,?)
                        `,
                    [id, name, priority, 0, new Date(), user.id],
                    )
                    if(results.affectedRows === 1){
                        return res
                        .status(200)
                        .json({ 
                            data: results, 
                            code: 1, 
                            message: 'Thêm kỹ năng mới thành công' 
                        })
                    }
                    return res.json({code: 0, message: 'Tạo mới kỹ năng không thành công'})
                }
                if(checkId.length === 1 && checkId[0].deleted === 1){
                    const results:any = await query<any>(
                        `UPDATE skills SET name = ? , priority = ?, deleted = ? WHERE id = ?`,
                        [name, priority, 0, id]
                    )
                    if(results.affectedRows === 1){
                        return res
                        .status(200)
                        .json({ 
                            data: results, 
                            code: 1, 
                            message: 'Thêm kỹ năng mới thành công'
                        })
                    }
                    return res.json({code: 0, message: 'Tạo mới kỹ năng không thành công'})
                }
                return res.json({code: 2, message: 'Mã kỹ năng này đã có trong hệ thống'})
            }
        } catch (error: any) {
          return res.status(500).json({ message: error.message })
        }
    }

    async function deleteSkills() {
        const { id } = req.query
        try {
            const results: any = await query<any>(
                `UPDATE skills SET deleted = 1 where id= ?`,
                [id],
            )
            if(results.affectedRows === 1){
                return res.status(200).json({
                    data: results,
                    message: `Delete skills ${id} successful`,
                    code: 1,
                })
            }
            return res.json({code:0, message: 'delete failed'})
        }
        catch (error: any) {
            return res.status(500).json({ message: error.message })
        }
    }
}

export default handler;