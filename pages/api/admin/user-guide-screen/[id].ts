import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from '../../../../lib/db'

const handler: any = async (req:NextApiRequest,res:NextApiResponse) =>{
    const session = await getSession({req})
    const user:any = session.user
    switch (req.method) {
        case "GET":
            return getUserGuideScreenById();
        case "PUT":
            return updateUserGuideScreen();
        case "DELETE":
            return deleteUserGuideScreen();
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
    async function getUserGuideScreenById() {
        const id = req.query.id
        try {
            const results:any= await query<any>(
                `SELECT * FROM user_guide_screen WHERE deleted = 0 AND id = ?`,[id])
             return res.status(200).json({
                data:results,
             }) 
        } catch (error: any) {
           return res.status(500).json({message:error.message}) 
        }
    }

    async function updateUserGuideScreen() {
        const id = req.query.id
        const {name,discription} = req.body
        const dateNow = new Date()
        try {
            const screenId:any = await query<any>(`SELECT * FROM user_guide_screen WHERE deleted = 0 AND id = ?`,[id])
            if(screenId && screenId.length > 0){
                if(!req.body || !Boolean(name) || !Boolean(discription)){
                    return res.status(400).json({message:"Data body fail"})
                }
                else{
                    const results:any = await query<any>(`UPDATE user_guide_screen SET name = ? , discription = ? , update_by = ? , update_date = ? WHERE deleted = 0 AND id = ?`,[name,discription,user.id,dateNow,id]) 
                    if(results.affectedRows === 0){
                        return res.status(400).json({message:'update insuccessful'})
                    }  
                    return res.status(200).json({data:results,message:'update successfull'})
                }
                
            }else{
                return res.status(400).json({message:`Do not have screenId = ${id} `})
            }
        } catch (error:any) {
            return res.status(500).json({message:error.message})
        }
    }

    async function deleteUserGuideScreen() {
        const {id} = req.body
        try {
            if(!req.body || !Boolean(id)){
                return res.status(400).json({message:'Data body fail'})
            }else{
                const screenId:any = await query<any>(`SELECT * FROM user_guide_screen WHERE deleted = 0 AND id = ?`,[id])
                if(screenId && screenId.length > 0){
                    const results:any = await query<any>(`UPDATE user_guide_screen SET deleted = 1`)
                    if(results.affectedRows === 0){
                        return res.status(400).json({message:`Delete screenId ${id} insuccessful`})
                    }
                    return res.status(200).json({data:results,message:`Delete screenId ${id} successful`})
                }else{
                    return res.status(400).json({message:`Do not find screenId:${id}`})
                }
            }
            
        } catch (error:any) {
            return res.status(500).json({message:error.message})
        }
    }

}
export default handler