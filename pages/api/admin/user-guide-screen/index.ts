import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from '../../../../lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session.user
  switch (req.method) {
    case 'GET':
      return getUserGuideScreen()
    case 'POST':
      return createUserGuideScreen()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }
  async function getUserGuideScreen() {
    // const {page, limit} = req.query
    try {
      const results: any = await query<any>(
        `SELECT * FROM user_guide_screen WHERE deleted = 0`,
        [],
      )
      // const totals = await query<any>(
      //     `SELECT COUNT(*) as total FROM user_guide_screen WHERE deleted = 0`,[]
      // )
      return res.status(200).json({
        data: results,
        // page:page,
        // limit:limit,
        // totalItems:totals[0].total,
        // totalPages: Math.ceil(totals[0].total/parseInt(limit.toString()))
      })
    } catch (error: any) {
      return res.status(500).json({ message: error.message })
    }
  }

  async function createUserGuideScreen() {
    const { name, discription } = req.body
    console.log('body', { name, discription })
    const dateNow = new Date()
    try {
      if (!req.body || !Boolean(name) || !Boolean(discription)) {
        return res.status(400).json({ message: 'Data body fail' })
      } else {
        const results: any = await query<any>(
          `INSERT INTO user_guide_screen(
                    name, 
                    discription, 
                    create_by,
                    create_date,
                    update_by,
                    update_date
                  ) 
                  VALUES(?,?,?,?,?,?)`,
          [name, discription, user.id, dateNow, user.id, dateNow],
        )
        return res
          .status(200)
          .json({ data: results, message: 'create successed' })
      }
    } catch (error: any) {
      return res.status(500).json({ message: error.message })
    }
  }
}
export default handler
