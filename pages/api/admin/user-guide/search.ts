import { NextApiRequest, NextApiResponse } from 'next'

import { query } from 'lib/db' 



const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const dataBody = req.body;
  const { title, typeArr, idScreenArr } = dataBody
  const queryRequest:any = req.query;
  const page:number = queryRequest.page??1;
  const limit:number = queryRequest.limit??10;

  switch (req.method) {
    case 'POST':
      return getUserGuide()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }
  async function getUserGuide() {
    let queryStr = `SELECT u.id, 
    u.title,
    u.screen_id as screenId, 
    u.title_en as titleEn, 
    u.link_file_en as linkfileEn, 
    u.link_file as linkfile, 
    u.created_date,
    u.type_data, 
    u1.name as screen 
    FROM user_guide as u JOIN user_guide_screen as u1 ON u.screen_id = u1.id
    WHERE u.deleted = 0 `;
    let queryCount = `SELECT COUNT(*) as total FROM user_guide u
    WHERE u.deleted = 0 `;
    const params = []
    if(typeArr && typeArr.length>0){
      queryStr+= ` and u.type_data in(?) `;
      queryCount+= ` and u.type_data in(?) `;
      params.push(typeArr);
    }
    if(idScreenArr && idScreenArr.length>0){
      queryStr+= ` and u.screen_id in (?) `;
      queryCount+= ` and u.screen_id in (?) `;
      params.push(idScreenArr);
    }
    if(title && title.length>0){
      queryStr+= ` and u.title LIKE ? `;
      queryCount+= ` and u.title LIKE ? `
      params.push(`%${title}%`);
    }
    queryStr += ' ORDER BY u.created_date desc '
    if (page !== undefined && page>0) {
      queryStr += ` LIMIT ${limit} OFFSET ${(page-1)*limit} `
    }
const results: any[] = await query<any[]>(queryStr, params)
const total = await query<any[]>(queryCount, params)

return res.json({
data: results,
totalPage: Math.ceil(total[0].total / limit),
totalRecords: total[0].total,
})
  }
}

export default handler
