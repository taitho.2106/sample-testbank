import { IncomingForm } from 'formidable'
import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { UploadFileSftp } from 'lib/sftpFile'

import { query } from '../../../../lib/db'
import fileUtils from '../../../../utils/file'
import { userGuideDir } from '@/constants/index'
export const config = {
  api: {
    bodyParser: false,
  },
}
const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session.user
 if (user?.is_admin !== 1) {
    return res.status(403).json({message:'Not permission'})
  }
  switch (req.method) {
    case 'GET':
      return getUserGuideById()
    case 'DELETE':
      return deleteUserGuide()
    case 'POST':
      const response: any = await handleRequest(req, res, user)
      switch (response.code) {
        case 1:
          return res.status(200).json({
            message: 'Success',
          })
        case -1:
          return res.status(500).json({
            message: 'Have an error',
          })
        default:
          return res.status(400).json({
            message: response.message,
          })
      }
    default:
      return res.status(405).json({message:`Method ${req.method} Not Allowed`})
  }
  async function getUserGuideById() {
    const id = req.query.id
    try {
      const results: any = await query<any>(
        `SELECT * FROM user_guide WHERE deleted = 0 AND id = ?`,
        [id],
      )
      return res.status(200).json({
        data: results,
      })
    } catch (error: any) {
      return res.status(500).json({ message: error.message })
    }
  }

  async function deleteUserGuide() {
    const { id } = req.query
    try {
      if (parseInt(id.toString()) <= 0) {
        return res
          .status(400)
          .json({ message: `Do not find userGuideId ${id}` })
      } else {
        const results: any = await query<any>(
          `UPDATE user_guide SET deleted = 1 where id= ?`,
          [id],
        )
        if (results.affectedRows === 0) {
          return res
            .status(400)
            .json({ message: `Delete userGuideId ${id} insuccessful` })
        }
        return res.status(200).json({
          data: results,
          message: `Delete userGuideId ${id} successful`,
        })
      }
    } catch (error: any) {
      return res.status(500).json({ message: error.message })
    }
  }
}

const handleRequest = (
  req: NextApiRequest,
  res: NextApiResponse,
  user: any,
) => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  return new Promise((resolve, reject) => {
    const form = new IncomingForm()
    const id: any = req.query.id
    const userGuideId: number = parseInt(id)
    form.parse(req, async (err, fields: any, files: any) => {
      try {
        if (err) throw err
        const {origin, title, titleEn, idScreen, type, file, fileEn } = fields
        // console.log('file', files)
        // console.log('fields', fields)
        // console.log('type', typeof fields.type)
        const type_data = parseInt(fields.type)
        // console.log('type', typeof type_data)
        let linkFile = file
        let linkFileEn = fileEn
        console.log("origin====",origin)
        //verify data
        if (
          title.lenght <= 0 ||
          title.length > 100 ||
          titleEn.length <= 0 ||
          titleEn.length > 100 ||
          idScreen <= 0 ||
          type_data <= 0 ||
          type_data > 2
        ) {
          return resolve({ code: 0, message: 'Data body fail' })
        }

        if (linkFile && !validURL(encodeURI(linkFile))) {
          return resolve({ code: 0, message: 'File url is incorrect' })
        }
        if (linkFileEn && !validURL(encodeURI(linkFileEn))) {
          return resolve({ code: 0, message: 'FileEn url is incorrect' })
        }

        if (!linkFile && files.file) {
          if (type_data === 2) {
            const resVn = await saveFile(files.file)
            if (resVn.status === true) {
              linkFile = `${origin}${resVn.url}`
            } else {
              return resolve({ code: 0, message: 'Upload file fail' })
            }
          } else {
            const resVn: any = await UploadFileSftp(files.file)
            await fileUtils.remove(files.file.filepath)
            if (resVn.status === true) {
              linkFile = resVn.response
            } else {
              return resolve({ code: 0, message: 'Upload file fail' })
            }
          }
        }
        if (!linkFileEn && files.fileEn) {
          if (type_data === 2) {
            const resEn = await saveFile(files.fileEn)
            if (resEn.status === true) {
              linkFileEn = `${origin}${resEn.url}`
            } else {
              return resolve({ code: 0, message: 'Upload file fail' })
            }
          } else {
            const resEn: any = await UploadFileSftp(files.fileEn)
            await fileUtils.remove(files.fileEn.filepath)
            if (resEn.status === true) {
              linkFileEn = resEn.response
            } else {
              return resolve({ code: 0, message: 'Upload file fail' })
            }
          }
        }
        if (!linkFile || !validURL(encodeURI(linkFile))) {
          return resolve({ code: 0, message: 'File is incorrect' })
        }
        if (!linkFileEn || !validURL(encodeURI(linkFileEn))) {
          return resolve({ code: 0, message: 'FileEn is incorrect' })
        }
        console.log('file 2')
        const dataUpdate = {
          title,
          titleEn,
          idScreen,
          type,
          linkFile,
          linkFileEn,
        }
        console.log('dataUpdate', dataUpdate)
        if (userGuideId > 0) {
          const result = await updateUserGuide(dataUpdate, userGuideId, user)
          return resolve(result)
        } else {
          const result = await createUserGuide(dataUpdate, user)
          return resolve(result)
        }
      } catch (ex: any) {
        console.log('error', ex.message)
        return resolve({ code: -1, message: ex })
      }
    })
  })
}
async function updateUserGuide(data: any, id: number, user: any) {
  const { title, titleEn, type, idScreen, linkFile, linkFileEn } = data
  const dateNow = new Date()

  try {
    if (id > 0) {
      const results: any = await query<any>(
        `UPDATE user_guide SET screen_id=?, title = ? , title_en = ? , type_data = ? , link_file = ? , link_file_en = ? ,updated_by = ? , updated_date = ? WHERE deleted = 0 AND id = ?`,
        [
          idScreen,
          title,
          titleEn,
          type,
          linkFile,
          linkFileEn,
          user.id,
          dateNow,
          id,
        ],
      )
      if (results.affectedRows === 0) {
        return { code: 0, message: 'Update fail' }
      }
      return { code: 1, message: 'Update success' }
    } else {
      return { code: 0, message: 'userGuideId not found' }
    }
  } catch (error: any) {
    return { code: -1, message: error.message }
  }
}
async function createUserGuide(data: any, user: any) {
  const { title, titleEn, type, idScreen, linkFile, linkFileEn } = data
  const dateNow = new Date()
  try {
    const results: any = await query<any>(
      `INSERT INTO user_guide(
        title, 
        title_en,
        screen_id, 
        type_data,
        link_file,
        link_file_en,
        created_by,
        created_date,
        deleted
      ) 
      VALUES(?,?,?,?,?,?,?,?,?)`,
      [
        title,
        titleEn,
        idScreen,
        type,
        linkFile,
        linkFileEn,
        user.id,
        dateNow,
        0,
      ],
    )
    if (results.affectedRows === 0) {
      return { code: 0, message: 'Insert fail' }
    } else {
      return { code: 1, message: 'Insert success' }
    }
  } catch (error: any) {
    return { code: -1, message: error.message }
  }
}
const saveFile = async (file: any) => {
  const folder = `/userGuideFiles`
  const pathDir = userGuideDir
  const filePath = file.filepath
  const fileName = file.originalFilename
  let files = null
  try {
    files = await fileUtils.exist(pathDir)
  } catch (error) {
    console.log('error', error)
  }
  if (!files) {
    fileUtils.createDir(pathDir, { recursive: true })
  }
  const fileData = await fileUtils.readFile(filePath)
  const response = await fileUtils.writeFile(`${pathDir}/${fileName}`, fileData)
  await fileUtils.remove(filePath)
  if (Boolean(response)) {
    return {
      status: true,
      url: `${folder}/${fileName}`,
    }
  }
}

export default handler

function validURL(str: string) {
  const pattern = new RegExp(
    '^(https?:\\/\\/)?' + // protocol
      '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.)+[a-z]{2,}|' + // domain name
      '((\\d{1,3}\\.){3}\\d{1,3}))' + // OR ip (v4) address
      '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*' + // port and path
      '(\\?[;&a-z\\d%_.~+=-]*)?' + // query string
      '(\\#[-a-z\\d_]*)?$',
    'i',
  ) // fragment locator
  return !!pattern.test(str)
}
