import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from 'lib/db' 

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    const user: any = session?.user
    if (user?.is_admin !== 1) {
      return res.status(403).json({message:'Not permission'})
    }
    switch (req.method) {
        case 'GET':
            return getGrade()
        default:
            return res.status(405).end(`Method ${req.method} Not Allowed`)
    }
    async function getGrade() {
        const results: any[] = await query(
            `SELECT * FROM grade WHERE deleted = 0
            ORDER BY priority 
            `,
            [],
        )
        return res.json({
            data: results,
        })
    }
}

export default handler