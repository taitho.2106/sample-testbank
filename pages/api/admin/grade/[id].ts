import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from '../../../../lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    const user: any = session?.user
    if (user?.is_admin !== 1) {
        return res.status(403).json({message:'Not permission'})
    }
    switch (req.method) {
        case 'GET':
            return getGradeById()
        case 'PUT':
            return updateGrade()
        case 'POST':
            return createGrade()
        case 'DELETE':
            return deleteGrade()
        default:
            return res.status(405).json({message:`Method ${req.method} Not Allowed`})
    }
    async function getGradeById() {
        const id = req.query.id
        try {
            const results: any = await query<any>(
                `SELECT * FROM grade where deleted = 0 AND id = ?`,
                [id],
            )
            return res.status(200).json({
                data: results,
                code: 1,
                message: 'Get grade success'
            })
        } catch (error: any) {
            return res.status(500).json({code:0, message: error.message })
        }
    }

    async function updateGrade() {
        const idOrigin = req.query.id
        const {id, name, priority, grade_eduhome_id} = req.body
        const dateNow = new Date()
        try {
            const gradeId:any = await query<any>(
                `SELECT * FROM grade WHERE deleted = 0 AND id = ?`,[idOrigin]
            )
            if(gradeId.length == 0){
                return res.status(200).json({
                    code: 0, 
                    message:'Khối lớp này không tồn tại trong hệ thống'
                })    
            }
            //has grade id
            if(grade_eduhome_id){
                //check isExist with grade id
                const existGradeEduhomeId: any = await query<any>(
                    `SELECT * FROM grade WHERE grade_eduhome_id = ? AND deleted = 0`,[grade_eduhome_id]
                )
                if(existGradeEduhomeId.length >0 && existGradeEduhomeId[0].id != id){
                    return res
                    .status(200)
                    .json({  
                        code: 0, 
                        message: 'Grade ID đang được sử dụng'
                    })
                }
            }
            if(!req.body || !Boolean(name) || !Boolean(priority)){
                return res.status(400).json({code: 0, message:"Data body fail"})
            }
            const results:any = await query<any>(
                `UPDATE grade SET name = ?, priority = ?, updated_by = ?, updated_date = ?, grade_eduhome_id = ? WHERE deleted = 0 AND id = ?`,
                [name, priority, user.id, dateNow, grade_eduhome_id||null, idOrigin]
            )
            if(results.affectedRows === 0){
                return res.status(200).json({code: 0, message:'Cập nhật khối lớp không thành công'})
            }  
            return res.status(200).json({
                code: 1, 
                message:'Cập nhật khối lớp thành công'
            })    
        } catch (error:any) {
            return res.status(500).json({code: 0, message: error.message})
        }
    }
    
    async function createGrade() {
        const { id, name, priority, grade_eduhome_id } = req.body
        const dateNow = new Date()
        try {
            if (!req.body || !Boolean(name) || !Boolean(priority)) {
                return res.status(400).json({ code:0, message: 'Data body fail' })
            } else {
                const checkId:any = await query<any>(
                    `SELECT * FROM grade WHERE id = ?`,[id]
                )
                //has grade id
                if(grade_eduhome_id){
                    //check isExist with grade id
                    const existGradeEduhomeId: any = await query<any>(
                        `SELECT * FROM grade WHERE grade_eduhome_id = ? AND deleted = 0`,[grade_eduhome_id]
                    )
                    if(existGradeEduhomeId.length >0 && existGradeEduhomeId[0].id != id){
                        return res
                        .status(200)
                        .json({  
                            code: 0, 
                            message: 'Grade ID đang được sử dụng'
                        })
                    }
                }
                if(checkId.length === 0){
                    const results: any = await query<any>(
                    `INSERT INTO grade(
                        id,
                        name, 
                        priority, 
                        created_by,
                        created_date,
                        deleted, 
                        grade_eduhome_id
                        ) 
                        VALUES(?,?,?,?,?,?,?)
                        `,
                    [id, name, priority, user.id, dateNow, 0, grade_eduhome_id||null],
                    )
                    if(results.affectedRows === 1){
                        return res
                        .status(200)
                        .json({ 
                            data: results, 
                            code: 1, 
                            message: 'Thêm khối lớp mới thành công'
                        })
                    }
                    return res.json({code: 0, message: 'Tạo mới khối lớp không thành công'})
                }else{
                    if(checkId[0].deleted === 1){
                        const results:any = await query<any>(
                            `UPDATE grade SET name = ? , priority = ?, deleted = ?, updated_by = ?, updated_date = ?, grade_eduhome_id = ? WHERE id = ?`,
                            [name, priority, 0, user.id, dateNow, grade_eduhome_id||null, id]
                        )
                        if(results.affectedRows === 1){
                            return res
                            .status(200)
                            .json({ 
                                data: results, 
                                code: 1, 
                                message: 'Thêm khối lớp mới thành công'
                            })
                        }
                        return res.json({code: 0, message: 'Tạo mới khối lớp không thành công'})
                    }else{
                        return res
                        .status(200)
                        .json({  
                            code: 0, 
                            message: 'Khối lớp đã tồn tại trong hệ thống'
                        })
                    }
                }
                
            }
        } catch (error: any) {
            console.log("error---",error)
          return res.status(500).json({ message: error.message })
        }
    }

    async function deleteGrade() {
        const { id } = req.query
        try {
            const results: any = await query<any>(
                `UPDATE grade SET deleted = 1 where id= ?`,
                [id],
            )
            if(results.affectedRows === 1){
                return res.status(200).json({
                    data: results,
                    message: `Delete grade ${id} successful`,
                    code: 1,
                })
            }
            return res.json({code:0, message: 'delete this grade failed'})
        }
        catch (error: any) {
            return res.status(500).json({ code:0, message: error.message })
        }
    }
}

export default handler;