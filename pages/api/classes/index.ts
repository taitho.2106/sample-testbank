import createHandler, { apiResponseDTO, errorResponseDTO, paginationResponse, roleConfig } from "lib/apiHandle"
import { NextApiRequest, NextApiResponse } from "next"
import prisma from 'lib/prisma';
import * as yup from "yup";
import { commonDeleteStatus, commonStatus, LIMIT_CLASS_PACKAGE_BASIC } from "@/constants/index";
import { transformObject } from "@/utils/index";
import { getLastPackage } from "services/package";
import { PACKAGES } from 'interfaces/constants'

const createClassSchema = yup.object({
    name: yup.string().trim().required(),
    grade: yup.string().trim().required(),
});

const handleCreateClass = async (req: NextApiRequest, res: NextApiResponse, session: any) => {
    const data = req.body;
    const curDate = new Date();
    const teacher: any = session.user;
    const servicePackage: any = session.servicePackage;

    // Giới hạn gói "phổ thông" của giáo viên chỉ được tạo 1 lớp
    if (servicePackage.code === PACKAGES.BASIC.CODE) {
        const currentTeacherClassCount = await prisma.classes.count({
            where: {
                owner_id: teacher.id,
                deleted: commonDeleteStatus.ACTIVE,
                status: commonStatus.ACTIVE,
            }
        });

        if (currentTeacherClassCount >= LIMIT_CLASS_PACKAGE_BASIC) {
            return res.status(400).json(errorResponseDTO(400, 'Reach limit class!'));
        }
    }

    const result = await prisma.classes.create({
        data: {
            ...data,
            owner_id: teacher.id,
            created_by: teacher.id,
            updated_by: teacher.id,
            created_date: curDate,
            updated_date: curDate,
        },
    })

    return res.status(200).json(apiResponseDTO(true, result));
}

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        pageable: true,
        querySchema: yup.object({
            status: yup.number(),
        }),
        accessRoles: [ roleConfig.teacher ],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any, paginationData) => {
            const { name, grade, status } = transformObject(req.query, {
                name: transformObject.types.String,
                grade: transformObject.types.Array,
                status: transformObject.types.Number,
            });
            const teacher: any = session.user;

            const whereCondition = {
                grade: {
                    in: grade,
                },
                status,
                name: {
                    contains: name,
                },
                owner_id: teacher.id,
                deleted: commonDeleteStatus.ACTIVE,
            };

            const [classes, totalClass] = await prisma.$transaction([
                prisma.classes.findMany({
                    ...paginationData,
                    where: whereCondition,
                    orderBy: [
                        { created_date: 'desc' }
                    ]
                }),
                prisma.classes.count({
                    where: whereCondition
                }),
            ]);

            const classCounts = await prisma.classStudent.groupBy({
                by: [ 'class_id' ],
                where: {
                    status: commonStatus.ACTIVE,
                    class_id: {
                        in: classes.map(el => el.id)
                    }
                },
                _count: true,
            })

            return res.status(200)
                .json(apiResponseDTO(
                    true,
                    paginationResponse((classes || []).map((classInfo) => ({
                        ...classInfo,
                        totalStudent: classCounts.find(cl => cl.class_id === classInfo.id)?._count || 0,
                    })), totalClass, paginationData)
                ));
        }
    },
    'POST': {
        authRequired: true,
        bodySchema: createClassSchema,
        accessRoles: [ roleConfig.teacher ],
        requireServicePackage: [],
        handler: handleCreateClass,
    }
});

export default handler