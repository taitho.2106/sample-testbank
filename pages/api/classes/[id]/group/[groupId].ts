import { NextApiRequest, NextApiResponse } from "next"
import * as yup from 'yup';

import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle"
import prisma from 'lib/prisma';
import { commonDeleteStatus, commonStatus } from "@/constants/index";

const updateGroupSchema = yup.object({
    name: yup.string().max(255).trim().required(),
});

const handleUpdateClass = async (req: NextApiRequest, res: NextApiResponse, session: any) => {
    const { id: classId, groupId } = req.query;
    const { name, addStudents, removeStudents } = req.body;
    const teacher: any = session.user;

    const classGroup = await prisma.classGroup.findFirst({
        where: {
            id: +groupId,
            class_id: +classId,
            status: commonStatus.ACTIVE,
            class: {
                id: +classId,
                owner_id: teacher.id,
                status: commonStatus.ACTIVE,
                deleted: commonDeleteStatus.ACTIVE,
            }
        },
        select: {
            class: {
                select: {
                    owner_id: true
                }
            }
        }
    });

    if (!classGroup || classGroup.class.owner_id !== teacher.id) {
        return res.status(400).json(errorResponseDTO(400, 'Group not found!'));
    }

    const result = await prisma.$transaction(async (tx) => {
        const result = await tx.classGroup.update({
            where: { id: +groupId },
            data: {
                name,
                ...getAuditData(session),
            }
        });

        if (addStudents?.length) {
            await tx.classStudent.updateMany({
                where: {
                    student_id: { in: addStudents },
                    class_id: +classId,
                },
                data: { class_group_id: +groupId }
            })
        }

        if (removeStudents?.length) {
            await tx.classStudent.updateMany({
                where: {
                    student_id: { in: removeStudents },
                    class_id: +classId,
                },
                data: { class_group_id: null }
            })
        }

        return result;
    });

    return res.status(200).json(apiResponseDTO(true, result));
}

const handler: any = createHandler({
    'PUT': {
        authRequired: true,
        bodySchema: updateGroupSchema,
        handler: handleUpdateClass,
    },
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const { id: classId, groupId } = req.query;
            const teacher: any = session.user;

            const classGroup = await prisma.classGroup.findFirst({
                where: {
                    id: +groupId,
                    class_id: +classId,
                    status: commonStatus.ACTIVE,
                    class: {
                        id: +classId,
                        owner_id: teacher.id,
                        deleted: commonDeleteStatus.ACTIVE,
                    }
                },
                select: {
                    id: true,
                    name: true,
                    classStudent: {
                        select: {
                            student: {
                                select: {
                                    id: true,
                                    full_name: true,
                                    phone: true,
                                }
                            }
                        }
                    }
                }
            });

            if (!classGroup) {
                return res.status(400).json(errorResponseDTO(400, 'Group not found!'));
            }

            const result = {
                id: classGroup.id,
                name: classGroup.name,
                students: (classGroup.classStudent || []).map(el => el.student)
            }

            return res.status(200).json(apiResponseDTO(true, result));
        }
    },
    'DELETE': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const { id: classId, groupId } = req.query;
            const teacher: any = session.user;

            const classGroup = await prisma.classGroup.findFirst({
                where: {
                    id: +groupId,
                    status: commonStatus.ACTIVE,
                    class: {
                        id: +classId,
                        owner_id: teacher.id,
                        status: commonStatus.ACTIVE,
                        deleted: commonDeleteStatus.ACTIVE,
                    }
                },
                select: {
                    id: true,
                }
            });

            if (!classGroup) {
                return res.status(400).json(errorResponseDTO(400, 'Group not found!'));
            }

            const [ result ] = await prisma.$transaction([
                prisma.classGroup.update({
                    where: {
                        id: classGroup.id,
                    },
                    data: {
                        ...getAuditData(session),
                        status: commonStatus.INACTIVE,
                    }
                }),
                prisma.classStudent.updateMany({
                    where: {
                        class_group_id: classGroup.id,
                    },
                    data: {
                        ...getAuditData(session),
                        class_group_id: null,
                    }
                }),
            ])

            return res.status(200).json(apiResponseDTO(true, result));
        }
    }
});

export default handler