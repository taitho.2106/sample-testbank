import * as yup from "yup";
import { NextApiRequest, NextApiResponse } from "next"

import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle"
import prisma from 'lib/prisma';
import { getActiveClass } from "services/classes";

const createGroupSchema = yup.object({
    name: yup.string().max(255).trim().required(),
});

const handleCreate = async (req: NextApiRequest, res: NextApiResponse, session: any) => {
    const { id: classId } = req.query;
    const { name, addStudents } = req.body;
    const teacher: any = session.user;

    const classInfo = await getActiveClass(+classId, teacher.id);
    if (!classInfo) {
        return res.status(400).json(errorResponseDTO(400, 'Class not found'));
    }

    const data = {
        name,
        class_id: +classId,
        ...getAuditData(session, true),
    };

    const result = await prisma.$transaction(async (tx) => {
        const result = await tx.classGroup.create({ data });
        if (addStudents) {
            await tx.classStudent.updateMany({
                where: {
                    student_id: {
                        in: addStudents,
                    },
                    class_id: +classId,
                },
                data: {
                    class_group_id: result.id,
                }
            })
        }

        return result;
    });

    return res.status(200).json(apiResponseDTO(true, result));
}

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        bodySchema: createGroupSchema,
        handler: handleCreate,
    }
});

export default handler