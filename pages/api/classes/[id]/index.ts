import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle"
import { NextApiRequest, NextApiResponse } from "next"
import * as yup from 'yup';

import prisma from 'lib/prisma';
import { commonDeleteStatus } from "@/constants/index";

const updateClassSchema = yup.object({
    name: yup.string().trim().required(),
    grade: yup.string().trim().required(),
});

const handleUpdateClass = async (req: NextApiRequest, res: NextApiResponse, session: any) => {
    const { name, start_year, end_year, grade } = req.body;
    const teacher: any = session.user;
    const { id: classId } = req.query;

    const teacherClass = await prisma.classes.findFirst({
        where: {
            id: +classId,
            owner_id: teacher.id,
            deleted: commonDeleteStatus.ACTIVE,
        }
    })

    if (!teacherClass) {
        return res.status(400).json(errorResponseDTO(400, 'Class not found'));
    }

    try {
        const result = await prisma.classes.update({
            where: {
                id: +classId,
            },
            data: {
                name, start_year, end_year, grade,
                ...getAuditData(session),
            },
        });

        return res.status(200).json(apiResponseDTO(true, result));
    } catch (error: any) {
        return res.status(200).json(apiResponseDTO(false, null, error?.meta?.cause || '', 400));
    }
}

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session) => {
            const { id } = req.query;
            const teacher: any = session.user;
            const result = await prisma.classes.findFirst({
                where: {
                    id: +id,
                    owner_id: teacher.id,
                    deleted: commonDeleteStatus.ACTIVE,
                },
            });

            return res.status(200).json(apiResponseDTO(true, result));
        }
    },
    'DELETE': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session) => {
            const { id } = req.query;
            const teacher: any = session.user;

            const teacherClass = await prisma.classes.findFirst({
                where: {
                    id: +id,
                    owner_id: teacher.id,
                    deleted: commonDeleteStatus.ACTIVE,
                }
            })

            if (!teacherClass) {
                return res.status(400).json(errorResponseDTO(400, 'Class not found'));
            }

            try {
                const result = await prisma.classes.update({
                    where: {
                        id: +id,
                    },
                    data: {
                        deleted: commonDeleteStatus.DELETED,
                        ...getAuditData(session),
                    }
                });
                return res.status(200).json(apiResponseDTO(true, result));
            } catch (error: any) {
                return res.status(400).json(errorResponseDTO(400, error?.meta?.cause || ''));
            }
        }
    },
    'PUT': {
        authRequired: true,
        bodySchema: updateClassSchema,
        handler: handleUpdateClass,
    },
});

export default handler