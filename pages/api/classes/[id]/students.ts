import createHandler, { apiResponseDTO } from "lib/apiHandle"
import { NextApiRequest, NextApiResponse } from "next"
import prisma from 'lib/prisma';
import { commonDeleteStatus, commonStatus } from "@/constants/index";
import studentDTO from "dto/studentDTO";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session) => {
            const { id, search, includeDeleted } = req.query;
            const teacher: any = session.user;
            const result = await prisma.classes.findFirst({
                include: {
                    students: {
                        select: {
                            student: {
                                select: studentDTO,
                            },
                            student_code: true,
                            class_group_id: true,
                        },
                        where: {
                            status: includeDeleted === '1' ? undefined : commonStatus.ACTIVE,
                            OR: search ? [
                                { student_code: { contains: search as string } },
                                { student: {
                                    OR: [
                                        { full_name: { contains: search as string } },
                                        { phone: { contains: search as string } },
                                    ]
                                } },
                            ] : undefined
                        },
                        orderBy: [
                            { student: { full_name: 'asc' } }
                        ]
                    },
                },
                where: {
                    deleted: commonDeleteStatus.ACTIVE,
                    id: +id,
                    owner_id: teacher.id,
                }
            });

            return res.status(200).json(apiResponseDTO(true, (result?.students || []).map(({ student, ...rest }) => ({ ... student, ...rest }))));
        }
    },
});

export default handler