import { NextApiRequest, NextApiResponse } from "next"

import prisma from 'lib/prisma';
import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle"
import { commonStatus } from "@/constants/index";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const { id: classId }: any = req.query;
            const teacher: any = session.user;

            const teacherClass = await prisma.classes.findFirst({
                where: {
                    id: +classId,
                    owner_id: teacher.id,
                }
            })

            if (!teacherClass) {
                return res.status(400).json(errorResponseDTO(400, 'Class not found!'))
            }

            const classes = await prisma.classGroup.findMany({
                where: {
                    class_id: +classId,
                    status: commonStatus.ACTIVE,
                },
                include: {
                    _count: {
                        select: {
                            classStudent: true
                        }
                    }
                },
                orderBy: [
                    { created_date: 'desc' }
                ]
            })

            return res.status(200)
                .json(apiResponseDTO(
                    true,
                    (classes || []).map(({ _count, ...rest }) => ({ ...rest, totalStudent: _count.classStudent }))
                ));
        }
    },
});

export default handler