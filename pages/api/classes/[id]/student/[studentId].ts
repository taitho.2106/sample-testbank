import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";

import { commonDeleteStatus, commonStatus } from "@/constants/index";
import prisma from 'lib/prisma';
import { validStudentCode } from "validations";
import { getActiveClass } from "services/classes";
import { ClassError } from '@/constants/errorCodes';

const handler: any = createHandler({
    'DELETE': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session) => {
            const teacher: any = session.user;
            const { id, studentId }: any = req.query;

            const classStudent = await prisma.classStudent.findFirst({
                where: {
                    student_id: studentId,
                    classes: {
                        id: +id,
                        owner_id: teacher.id,
                        status: commonStatus.ACTIVE,
                        deleted: commonDeleteStatus.ACTIVE,
                    }
                }
            })

            if (!classStudent) {
                return res.status(400).json(errorResponseDTO(400, 'Params invalid!'))
            }

            try {
                const result = await prisma.classStudent.update({
                    where: {
                        class_id_student_id: {
                            class_id: +id,
                            student_id: studentId,
                        },
                    },
                    data: {
                        ...getAuditData(session),
                        status: commonStatus.INACTIVE,
                        class_group_id: null,
                        student_code: null,
                    },
                });
                return res.status(200).json(apiResponseDTO(true, result));
            } catch (error: any) {
                console.log({ error });
                return res.status(200).json(apiResponseDTO(false, null, error?.meta?.cause || '', 400));
            }
        }
    },
    'PUT': {
        authRequired: true,
        bodySchema: validStudentCode,
        handler: async (req: NextApiRequest, res: NextApiResponse, session) => {
            const { studentCode } = req.body;
            const teacher: any = session.user;
            const { id, studentId }: any = req.query;

            const classes = await getActiveClass(+id, teacher.id);
            if (!classes) {
                return res.status(400).json(errorResponseDTO(400, 'Class invalid!'));
            }

            if (studentCode) {
                const classStudent = await prisma.classStudent.findFirst({
                    where: {
                        student_code: studentCode,
                        class_id: +id,
                        student_id: {
                            not: studentId,
                        }
                    }
                });
                if (classStudent) {
                    return res.status(400).json(errorResponseDTO(ClassError.STUDENT_EXISTED_CODE, 'Student code existed!'));
                }
            }

            await prisma.classStudent.update({
                where: {
                    class_id_student_id: {
                        student_id: studentId,
                        class_id: +id,
                    }
                },
                data: {
                    ...getAuditData(session),
                    student_code: studentCode,
                }
            })

            return res.status(200).json(apiResponseDTO(true, null));
        }
    }
});

export default handler