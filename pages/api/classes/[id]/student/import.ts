import { NextApiRequest, NextApiResponse } from "next";

import { ClassError } from "@/constants/errorCodes";
import { DATE_DISPLAY_FORMAT, LIMIT_CLASS_STUDENT_PACKAGE_BASIC } from "@/constants/index";
import { PACKAGES } from "@/interfaces/constants";
import { getFormDataFromRequest } from "@/utils/api";
import { convertDateStringToDate } from "@/utils/date";
import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import { countClassActiveStudent, getActiveClass } from "services/classes";
import {
    addStudentsToClassIfNotExist,
    addStudentsToTeacherIfNotExist,
    createUsersAndSendMail,
    verifyStudentData
} from 'services/student';
import fileUtils from 'utils/file';

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        requireServicePackage: [],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any, p:any, { t }:any) => {
            const { files }: any = await getFormDataFromRequest(req);
            const { user: teacher, servicePackage }: any = session;
            const classId = +req.query.id;

            if (!files?.file) {
                return res.status(400).json(errorResponseDTO(400, 'File invalid'));
            }

            if (classId) {
                const classInfo = await getActiveClass(classId, teacher.id);
                if (!classInfo) {
                    return res.status(400).json(errorResponseDTO(400, 'Class not found'));
                }
            }

            try {
                const studentData = (await fileUtils.readExcelFile(files.file, [
                    'studentCode',
                    'fullName',
                    'phone',
                    'email',
                    'birthday',
                    'gender',
                ]));

                if (servicePackage.code === PACKAGES.BASIC.CODE) {
                    const studentCount = await countClassActiveStudent(classId);
                    if (studentCount + (studentData.length || 0) > LIMIT_CLASS_STUDENT_PACKAGE_BASIC) {
                        return res.status(400).json(errorResponseDTO(ClassError.IMPORT_STUDENT_REACH_LIMIT, 'Reach limit class student!'));
                    }
                }

                const studentDataVerified = await verifyStudentData(studentData, t);

                const studentWillCreate = studentDataVerified
                    .filter((stu: any) => !stu.status && !stu.id)
                    .map(({ birthday, gender, ...rest}: any) => ({
                        ...rest,
                        birthday: birthday ? convertDateStringToDate(birthday, DATE_DISPLAY_FORMAT, true).local().toDate() : undefined,
                        gender: gender ? +gender : gender,

                    }));
                const studentCreateResult: any = await createUsersAndSendMail(studentWillCreate);
                for (const newStu of studentCreateResult) {
                    const stu = studentDataVerified.find((stu: any) => !stu.status && stu.phone === newStu.phone)
                    if (stu) {
                        stu.id = newStu.id;
                    }
                }

                const studentsAdd = studentDataVerified.filter((stu: any) => !!stu.id);

                await addStudentsToTeacherIfNotExist(studentsAdd, teacher.id, session);
                const { errors } = await addStudentsToClassIfNotExist(studentsAdd, classId, session);

                for (const stuError of errors) {
                    const stu = studentDataVerified.find((stu: any) => !stu.status && stu.phone === stuError.phone)
                    if (stu) {
                        stu.status = stuError.status;
                    }
                }

                return res.status(200).json(apiResponseDTO(true, studentDataVerified));
            } catch (error) {
                console.log({ error });
                return res.status(400).json(errorResponseDTO(400, 'Error Undefined'));
            }
        }
    },
});

export const config = {
    api: {
        bodyParser: false,
    },
};

export default handler