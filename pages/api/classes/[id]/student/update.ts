import { NextApiRequest, NextApiResponse } from "next"
import * as yup from 'yup';

import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle"
import prisma from 'lib/prisma';
import { commonDeleteStatus, commonStatus, LIMIT_CLASS_STUDENT_PACKAGE_BASIC } from "@/constants/index";
import { PACKAGES } from "@/interfaces/constants";
import { countClassActiveStudent } from "services/classes";
import { ClassError } from "@/constants/errorCodes";

const updateClassStudentSchema = yup.object({
    id: yup.number(),
    addStudents: yup.array().of(yup.string()),
    removeStudents: yup.array().of(yup.string()),
});

const handleUpdateClass = async (req: NextApiRequest, res: NextApiResponse, session: any) => {
    const { id: classId } = req.query;
    const { addStudents, removeStudents } = req.body;
    const { user: teacher, servicePackage }: any = session;

    const classes = await prisma.classes.findFirst({
        where: {
            id: +classId,
            owner_id: teacher.id,
            status: commonStatus.ACTIVE,
            deleted: commonDeleteStatus.ACTIVE,
        }
    });

    if (!classes) {
        return res.status(400).json(errorResponseDTO(400, 'Class not found!'));
    }

    if (servicePackage.code === PACKAGES.BASIC.CODE) {
        const studentCount = await countClassActiveStudent(+classId);

        if (studentCount + (addStudents?.length || 0) - (removeStudents?.length || 0) > LIMIT_CLASS_STUDENT_PACKAGE_BASIC) {
            return res.status(400).json(errorResponseDTO(ClassError.IMPORT_STUDENT_REACH_LIMIT, 'Reach limit class student!'));
        }
    }

    const result = await prisma.$transaction(async (tx) => {
        if (addStudents?.length) {
            const studentAlreadyAdd = await tx.classStudent.findMany({
                where: {
                    student_id: { in: addStudents },
                    class_id: +classId,
                },
                select: {
                    student_id: true,
                    status: true,
                }
            })

            const studentInactive = studentAlreadyAdd.filter(stu => stu.status === commonStatus.INACTIVE)
            if (studentInactive.length) {
                await tx.classStudent.updateMany({
                    where: {
                        student_id: { in: studentInactive.map(el => el.student_id) },
                        class_id: +classId,
                        status: commonStatus.INACTIVE,
                    },
                    data: {
                        ...getAuditData(session),
                        status: commonStatus.ACTIVE,
                    }
                })
            }

            const studentsAddNew = addStudents.filter((studentId: string) => !studentAlreadyAdd.find(stu => stu.student_id === studentId));
            if (studentsAddNew.length) {
                await tx.classStudent.createMany({
                    data: studentsAddNew.map((stu: any) => ({
                        ...getAuditData(session, true),
                        class_id: +classId,
                        student_id: stu,
                        status: commonStatus.ACTIVE,
                    }))
                })
            }
        }

        if (removeStudents?.length) {
            await tx.classStudent.updateMany({
                where: {
                    student_id: { in: removeStudents },
                    class_id: +classId,
                },
                data: {
                    ...getAuditData(session),
                    status: commonStatus.INACTIVE,
                }
            })
        }

        return null;
    });

    return res.status(200).json(apiResponseDTO(true, result));
}

const handler: any = createHandler({
    'PUT': {
        authRequired: true,
        bodySchema: updateClassStudentSchema,
        requireServicePackage: [],
        handler: handleUpdateClass,
    },
});

export default handler