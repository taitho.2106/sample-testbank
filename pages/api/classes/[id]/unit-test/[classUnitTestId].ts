import { NextApiRequest, NextApiResponse } from "next"
import * as yup from 'yup';

import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle"
import prisma from 'lib/prisma';
import { getDateStatusFromStartEnd } from "@/utils/date";
import { STATUS_CODE_EXAM } from "@/interfaces/constants";

const handler: any = createHandler({
    'DELETE': {
        authRequired: true,
        querySchema: yup.object().shape({
            classUnitTestId: yup.string().required(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const { classUnitTestId } = req.query;
            const teacher: any = session.user;

            const classUnitTest = await prisma.classUnitTest.findFirst({
                where: {
                    id: +classUnitTestId,
                },
                select: {
                    class: {
                        select: {
                            owner_id: true,
                        }
                    },
                    start_date: true,
                    end_date: true,
                }
            });

            if (classUnitTest?.class?.owner_id !== teacher.id) {
                return res.status(400).json(errorResponseDTO(400, 'Unit test assign not found!'));
            }

            const examStatus = getDateStatusFromStartEnd(classUnitTest.start_date, classUnitTest.end_date);
            if (examStatus === STATUS_CODE_EXAM.UPCOMING) {
                await prisma.classUnitTest.delete({
                    where: {
                        id: +classUnitTestId,
                    }
                })

                return res.status(200).json(apiResponseDTO(true, null));
            }

            return res.status(400).json(errorResponseDTO(400, 'Unassign unit test failed!', {
                examStatus,
            }))
        }
    }
});

export default handler