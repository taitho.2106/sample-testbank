import { NextApiRequest, NextApiResponse } from "next";
import * as yup from "yup";

import { getContentEmailNewUnitTestAssign } from "@/constants/email-template";
import { commonStatus } from "@/constants/index";
import dayjs from "dayjs";
import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle";
import prisma from 'lib/prisma';
import { getActiveClass } from "services/classes";
import { sendEmail } from "services/email";

const createGroupSchema = yup.object({
    groupId: yup.number(),
    unitTests: yup.array().of(
        yup.object({
            start_date: yup.date(),
            end_date: yup.date(),
            unit_type: yup.number().required(),
            unit_test_id: yup.number().required(),
        })
    )
});

const sendMailNotify = async (classId: number, groups: number[], students: string[], classUnitTest: any[], teacher: any) => {
    let classStudents = await prisma.classStudent.findMany({
        where: {
            class_id: classId,
            student_id: students?.length ? { in: students } : undefined,
            class_group_id: groups?.length ? { in: groups } : undefined,
            status: commonStatus.ACTIVE,
        },
        select: {
            class_group_id: true,
            student: {
                select: {
                    email: true,
                    id: true,
                }
            }
        }
    });

    if (classStudents?.length && classUnitTest?.length) {
        const subject = `${dayjs().format('Ngày DD/MM/YYYY HH:mm')} - Bạn vừa được giáo viên${teacher.full_name ? ' ' + teacher.full_name : ''} giao đề thi`;

        if (students?.length) {
            for (let stu of students) {
                const studentEmail = classStudents.find(el => el.student.id === stu)?.student?.email;
                const studentUnitTests = classUnitTest.filter(el => el.student_id === stu);

                if (studentEmail && studentUnitTests.length) {
                    sendEmail({
                        to: studentEmail,
                        subject,
                        content: getContentEmailNewUnitTestAssign(studentUnitTests, teacher),
                    });
                }
            }
            return;
        }

        if (groups?.length) {
            for (let groupId of groups) {
                const studentEmails = classStudents.filter(el => el.class_group_id === groupId).map(el => el.student.email).filter(Boolean);
                const studentUnitTests = classUnitTest.filter(el => el.class_group_id === groupId);

                if (studentEmails?.length && studentUnitTests.length) {
                    sendEmail({
                        to: studentEmails,
                        subject,
                        content: getContentEmailNewUnitTestAssign(studentUnitTests, teacher),
                    });
                }
            }
            return;
        }

        sendEmail({
            to: classStudents.map(el => el.student.email).filter(Boolean),
            subject,
            content: getContentEmailNewUnitTestAssign(classUnitTest, teacher),
        });
    }
}

const handleCreate = async (req: NextApiRequest, res: NextApiResponse, session: any) => {
    const { id: classId } = req.query;
    const { groups, students, unitTests } = req.body;
    const teacher: any = session.user;

    const classInfo = await getActiveClass(+classId, teacher.id);
    if (!classInfo) {
        return res.status(400).json(errorResponseDTO(400, 'Class not found!'));
    }

    let result;
    if (unitTests?.length) {
        const prepareUnitTestCreate = unitTests.map((unitTest: any) => {
            const { start_date, end_date, ...rest } = unitTest;
            return {
                class_id: +classId,
                ...rest,
                start_date: start_date ? dayjs(start_date).toDate() : start_date,
                end_date: end_date ? dayjs(end_date).toDate() : end_date,
                ...getAuditData(session, true),
            }
        })

        let unitTestCreate = prepareUnitTestCreate;
        if (students?.length) {
            const existedStudents = await prisma.user.findMany({
                where: {
                    id: {
                        in: students,
                    }
                },
                select: {
                    id: true,
                }
            })

            if (existedStudents.length) {
                unitTestCreate = existedStudents.reduce((rs: any, student: any) => (
                    [ ...rs, ...prepareUnitTestCreate.map((el: any) => ({ ...el, student_id: student.id })) ]
                ), [])
            }
        }

        if (groups?.length) {
            const existedGroups = await prisma.classGroup.findMany({
                where: {
                    id: { in: groups }
                },
                select: {
                    id: true,
                }
            })

            if (existedGroups.length) {
                unitTestCreate = existedGroups.reduce((rs: any, group: any) => (
                    [ ...rs, ...prepareUnitTestCreate.map((el: any) => ({ ...el, class_group_id: group.id })) ]
                ), [])
            }
        }

        result = await prisma.$transaction(unitTestCreate.map((data: any) => prisma.classUnitTest.create({
            data,
            select: {
                id: true,
                start_date: true,
                end_date: true,
                unit_type: true,
                class_group_id: true,
                student_id: true,
                unitTest: {
                    select: {
                        name: true,
                        id: true,
                        time: true,
                    }
                }
            }
        })));

        sendMailNotify(+classId, groups, students, result, teacher);
    }


    return res.status(200).json(apiResponseDTO(true, result));
}

const handler: any = createHandler({
    'POST': {
        authRequired: true,
        bodySchema: createGroupSchema,
        handler: handleCreate,
    }
});

export default handler