import createHandler, { apiResponseDTO, errorResponseDTO, getAuditData } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import { commonDeleteStatus, commonStatus } from "@/constants/index";
import prisma from 'lib/prisma';

const handler: any = createHandler({
    'PUT': {
        authRequired: true,
        bodySchema: yup.object({
            status: yup.number().required().oneOf(Object.values(commonStatus)),
        }),
        querySchema: yup.object({
            id: yup.number().required(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const teacher: any = session.user;
            const { status } = req.body;
            const { id: classId } = req.query;

            const teacherClass = await prisma.classes.findFirst({
                where: {
                    id: +classId,
                    owner_id: teacher.id,
                    deleted: commonDeleteStatus.ACTIVE,
                }
            })

            if (!teacherClass) {
                return res.status(400).json(errorResponseDTO(400, 'Class not found'));
            }

            try {
                const result = await prisma.classes.update({
                    where: {
                        id: +classId,
                    },
                    data: {
                        status,
                        ...getAuditData(session),
                    },
                });

                return res.status(200).json(apiResponseDTO(true, result));
            } catch (error: any) {
                return res.status(200).json(apiResponseDTO(false, null, error?.meta?.cause || '', 400));
            }
        },
    },
});

export default handler