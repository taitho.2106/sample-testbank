import createHandler, { apiResponseDTO, paginationResponse } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";

import { commonDeleteStatus, commonStatus } from "@/constants/index";
import prisma from 'lib/prisma';

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session) => {
            const { id: classId } = req.query;
            const teacher: any = session.user;

            const studentInClass = await prisma.classStudent.findMany({
                where: {
                    class_id: +classId,
                    status: commonStatus.ACTIVE,
                    classes: {
                        status: commonStatus.ACTIVE,
                        deleted: commonDeleteStatus.ACTIVE,
                    }
                },
                select: {
                    student_id: true,
                }
            })

            const students = await prisma.teacherStudent.findMany({
                where: {
                    status: commonStatus.ACTIVE,
                    teacher_id: teacher.id,
                    student_id: {
                        notIn: studentInClass.map(el => el.student_id)
                    }
                },
                select: {
                    student: {
                        select: {
                            id: true,
                            full_name: true,
                            phone: true,
                        },
                    },
                },
                orderBy: {
                    student: {
                        full_name: 'asc',
                    }
                }
            })

            return res.status(200).json(apiResponseDTO(
                true,
                students.map((el: any) => el.student),
            ));
        }
    },
});

export default handler