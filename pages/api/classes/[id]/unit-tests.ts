import { NextApiRequest, NextApiResponse } from "next"
import * as yup from 'yup';

import createHandler, { apiResponseDTO, errorResponseDTO, paginationResponse, transformToArray } from "lib/apiHandle"
import prisma from 'lib/prisma';
import { classUnitTestSelect } from "dto/entitiesSelect";
import { dayjs, getDateRangeConditionByStatus } from "@/utils/date";
import { mapListClassUnitTestToDTO } from "dto/unitTest";
import { STATUS_CODE_EXAM } from "@/interfaces/constants";
import { DATE_VALUE_FORMAT, OBJECT_ASSIGN } from "@/constants/index";

const dateInputValidate = yup.string().test('test-date-string', 'Date string invalid', (value) => {
    return !value || dayjs(value, DATE_VALUE_FORMAT, true).isValid();
});

const getParamsSchema = yup.object().shape({
    classId: yup.number(),
    groupId: yup.number(),
    examStatus: yup.array().of(yup.number().oneOf(Object.values(STATUS_CODE_EXAM))).transform(value => {
        if (!Array.isArray(value)) {
            return [value];
        }

        return value;
    }),
    startDate: dateInputValidate,
    endDate: dateInputValidate,
    assignFor: yup.number().oneOf([OBJECT_ASSIGN.CLASS, OBJECT_ASSIGN.GROUP, OBJECT_ASSIGN.STUDENT]),
});

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: getParamsSchema,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any, paginationData) => {
            let {
                id: classId,
                groupId,
                students,
                examStatus,
                startDate,
                endDate,
                name,
                assignFor,
            }: any = req.query;
            const teacher: any = session.user;
            const examStatusListFilter = transformToArray(examStatus, (el: any) => +el);
            assignFor = +assignFor;

            if (startDate) {
                startDate = dayjs(startDate).startOf('d').toDate();
            }

            if (endDate) {
                endDate = dayjs(endDate).endOf('d').toDate();
            }

            const teacherClass = await prisma.classes.findFirst({
                where: {
                    id: +classId,
                    owner_id: teacher.id,
                }
            })

            if (!teacherClass) {
                return res.status(400).json(errorResponseDTO(400, 'Class not found!'))
            }

            const dateCondition = examStatusListFilter.map((status: any) => getDateRangeConditionByStatus(status));
            const whereCondition: any = {
                class_id: +classId,
                start_date: { gte: startDate },
                end_date: { lte: endDate },
                unitTest: {
                    name: {
                        contains: name,
                    }
                },
            }
            if (assignFor === OBJECT_ASSIGN.CLASS) {
                whereCondition.class_group_id = null;
                whereCondition.student_id = null;
            }
            if (assignFor === OBJECT_ASSIGN.GROUP) {
                whereCondition.student_id = null;

                if (groupId) {
                    whereCondition.class_group_id = +groupId;
                } else {
                    whereCondition.class_group_id = {
                        not: null,
                    }
                }
            }
            if (assignFor === OBJECT_ASSIGN.STUDENT) {
                whereCondition.class_group_id = null;

                if (students?.length) {
                    whereCondition.student_id = {
                        in: students,
                    };
                } else {
                    whereCondition.student_id = {
                        not: null,
                    }
                }
            }

            if (dateCondition.length) {
                whereCondition.OR = dateCondition;
            }

            const [ unitTests, totalUnitTest ]: any = await prisma.$transaction([
                prisma.classUnitTest.findMany({
                    ...paginationData,
                    where: whereCondition,
                    select: classUnitTestSelect,
                    orderBy: [
                        { created_date: 'desc' }
                    ]
                }),
                prisma.classUnitTest.count({
                    where: whereCondition
                }),
            ]);

            return res.status(200)
                .json(apiResponseDTO(
                    true,
                    paginationResponse(mapListClassUnitTestToDTO(unitTests), totalUnitTest, paginationData)
                ));
        }
    },
});

export default handler