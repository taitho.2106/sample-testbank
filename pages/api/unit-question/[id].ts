import fs from 'fs'

import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { COLOR_SHAPE, SHAPE_CODE, USER_ROLES } from 'interfaces/constants'
import {
  PRACTICE_TEST_ACTIVITY,
  SKILLS_SELECTIONS,
} from 'interfaces/struct'
import { StructType } from 'interfaces/types'
import { query } from 'lib/db'
import { userInRight } from 'utils'
import { getTextWithoutFontStyle } from 'utils/string'

import shuffleArray from '@/questions_middleware/shuffleArray'
import Guid from 'utils/guid'
import dayjs from 'dayjs'
import { mapClassUnitTestToDTO } from "dto/unitTest";

let baseHostUrl = ''

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  // const session = await getSession({ req })
  // if (!userInRight([USER_ROLES.Operator], session)) {
  //   return res.status(403).end('Forbidden')
  // }
  baseHostUrl = req.headers.referer.split('/').splice(0, 3).join('/')

  switch (req.method) {
    case 'GET':
      return getDataById()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getDataById() {
    const { id } = req.query
    try {
      const unitTests: any[] = await query<any[]>(
        'SELECT * FROM unit_test WHERE id = ? LIMIT 1',
        [id],
      )
      if (unitTests.length === 0) {
        return res.status(400).json({ message: 'data not found' })
      }
      const unitTest = unitTests[0]
      const sections: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section WHERE deleted = 0 AND unit_test_id = ?',
        [id],
      )
      if (sections.length > 0) {
        unitTest.sections = sections
      }
      const sectionIds = sections.map((m) => m.id)
      const parts: any[] = await query<any[]>(
        'SELECT * FROM unit_test_section_part WHERE deleted = 0 AND unit_test_section_id IN (?)',
        [sectionIds],
      )
      if (parts.length > 0) {
        for (const item of sections) {
          item.parts = parts.filter((m) => m.unit_test_section_id === item.id)
        }
      }
      const partIds = parts.map((m) => m.id)
      const questions: any[] = await query<any[]>(
        `SELECT utspq.unit_test_section_part_id, q.* FROM unit_test_section_part_question utspq
         LEFT JOIN question q ON utspq.question_id = q.id
         WHERE utspq.deleted = 0 AND utspq.unit_test_section_part_id IN (?)`,
        [partIds],
      )
      if (questions.length > 0) {
        for (const item of parts) {
          item.questions = questions.filter(
            (m) => m.unit_test_section_part_id === item.id,
          )
        }
      }
      const listGrade = await query<any[]>(
        'SELECT id as code, name as display FROM grade where deleted = 0 order by priority',
        [],
      )
      if (listGrade.length > 0) {
        unitTest.listGrade = listGrade
      }
      const unitTestResponse = convertDataToPracticeTest(unitTest);

      return res.json(mapClassUnitTestToDTO(unitTestResponse, dayjs()))
    } catch (e: any) {
      console.log(e)
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler

const convertDataToPracticeTest = (unitTest: any) => {
  const sectionNames: any = {}
  SKILLS_SELECTIONS.map((value: any) => {
    sectionNames[value.code] = value.display
  })

  const data: any = {
    id:unitTest.id,
    name: unitTest.name,
    time: unitTest.time,
    start_date: unitTest.start_date,
    end_date: unitTest.end_date,
    unit_type: unitTest.unit_type,
    sections: [],
    template_level_id:
      unitTest.listGrade.find(
        (find: StructType) => find?.code === unitTest?.template_level_id,
      )?.display || '---',
  }
  for (const item of unitTest.sections) {
    data.sections.push({
      id: item.id,
      name: item.parts[0].name,
      totalGroup: 1,
      totalPoint: item.parts[0].points,
      list: item.parts[0].questions.map(convertQuestion),
      code: item.section,
    })
  }
  return data
}

const convertQuestion = (question: any) => {
  const questionType = PRACTICE_TEST_ACTIVITY[question.question_type]
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const correctAnswerList = question.correct_answers?.split('#')
  const result = {
    id: question.id,
    activityType: questionType,
    audioInstruction: getFullUrl(question.audio),
    audioScript: question.audio_script,
    imageInstruction: getFullUrl(question.image),
    instruction: question.question_description,
    questionInstruction: '',
    totalQuestion: question.total_question,
    listContent: [{}],
  }

  if (questionType === 1) {
    result.listContent = answerList.map(
      (answers: any, answerIndex: number) => ({
        id: question.id,
        activityType: questionType,
        question: questionList ? questionList[answerIndex] : '',
        answers: answers.split('*').map((answer: any) => ({
          text: answer,
          isCorrect: answer === correctAnswerList[answerIndex],
        })),
      }),
    )
  } else if (questionType === 11) {
    result.instruction = question.parent_question_description
    result.questionInstruction = question.parent_question_text
    result.listContent = [
      {
        questionsGroup: questionList.map((qt: string, qtIndex: number) => {
          const answers = answerList[qtIndex].split('*')
          return {
            id: question.id,
            question: qt.replace('*',''),
            isExample: qt.startsWith('*') ? true : false,
            activityType: questionType,
            answers: answers.map((answer: any) => ({
              text: answer,
              isCorrect: getTextWithoutFontStyle(answer) === correctAnswerList[qtIndex],
            })),
          }
        }),
      },
    ]
  } else if (questionType === 2) {
    result.listContent = questionList.map((ql: any, qlIndex: number) => {
      const answers = answerList[qlIndex].split('*')
      return {
        id: question.id,
        activityType: questionType,
        question: ql,
        answers: answers.map((answer: any) => ({
          word: answer.toUpperCase(),
          answerList: question.correct_answers ?? '',
        })),
      }
    })
  } else if (questionType === 3) {
    result.questionInstruction = question.parent_question_text ?? ''
    result.listContent = questionList.map((ql: any, qlIndex: number) => {
      return {
        id: question.id,
        activityType: questionType,
        question: ql,
        answers: correctAnswerList.map((ca: any, caIndex: number) => ({
          answer: ca,
          position: caIndex,
        })),
      }
    })
  } else if (questionType === 4) {
    result.listContent = answerList.map((al: any, alIndex: number) => {
      const answers = al.split('*')
      return {
        id: question.id,
        activityType: questionType,
        answers: answers.map((answer: any) => ({
          text: answer,
          isCorrect: getTextWithoutFontStyle(answer) === correctAnswerList[alIndex],
        })),
      }
    })
  } else if (questionType === 5) {
    result.listContent = questionList.map((ql: any, qlIndex: number) => {
      return {
        id: question.id,
        activityType: questionType,
        question: ql,
        answers: answerList.map((answer: any, answerIndex: number) => ({
          answer: correctAnswerList[answerIndex],
          answerList: answer.replaceAll('*', '/'),
          position: answerIndex,
        })),
      }
    })
  } else if (questionType === 6) {
    result.instruction = question.parent_question_description || question.question_description
    const listImage = question.image?.split('#')
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: answerList.map((al: any, alIndex: number) => {
          const correctAnswers = correctAnswerList[alIndex].split('%/%')
          const correctOrders = correctAnswers.map((answer: any) => {
            return answer.trim().split('*')
          })
          return {
            imageInstruction: listImage ? getFullUrl(listImage[alIndex]) : "",
            question: al.replaceAll('*', '/'),
            answer: correctAnswerList[alIndex]
              .trim()
              .replaceAll('*', ' ')
              .replaceAll(' .', '.')
              .replaceAll(' ,', ',')
              .replaceAll(' !', '!')
              .replaceAll(' ?', '?')
              .replaceAll('%/%', '/'),
            answerOrder: correctOrders,
            isExam: (!questionList || questionList[alIndex] !== '*') ? 'question' : 'exam',
          }
        }
        ),
      },
    ]
  } else if (questionType === 7) {
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        question: questionList[0],
        answers: [
          {
            start: answerList ? answerList[0] : '',
            answer: question.correct_answers ?? '',
          },
        ],
      },
    ]
  } else if (questionType === 8 || questionType === 19) {
    result.questionInstruction = question.question_text
    result.imageInstruction = question.question_type === 'TF4' ? question?.image?.split('#').map((item:string)=> getFullUrl(item)).join('#') : getFullUrl(question.image)
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: answerList.map((answer: any, answerIndex: number) => ({
          text: answer,
          isCorrect: correctAnswerList[answerIndex] === 'T',
        })),
      },
    ]
  } else if (questionType === 9) {
    result.questionInstruction = question.question_text
    const partLeft = answerList[0].split('*')
    const partRight = answerList[1].split('*')
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: partRight.map((pr: any, prIndex: number) => ({
          left: partLeft[prIndex],
          right: pr,
          rightAnswerPosition: partRight.findIndex(
            (m: any) => m === correctAnswerList[prIndex],
          ),
        })),
      },
    ]
  } else if (questionType === 12 || questionType === 13) {
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: answerList.map((as: any) => ({
          text: as,
          isCorrect: correctAnswerList.includes(as),
        })),
      },
    ]
  } else if (questionType === 14) {
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: [
          {
            answers: correctAnswerList,
          },
        ],
      },
    ]
  } else if (questionType === 15) {
    const answerValues: any = { T: 'TRUE', F: 'FALSE', NI: 'NI' }
    result.questionInstruction = question.question_text ?? ''
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: answerList.map((as: any, asIndex: number) => ({
          text: as,
          answer: answerValues[correctAnswerList[asIndex]],
          answerList: Object.values(answerValues).join('/'),
        })),
      },
    ]
  } else if (questionType === 16) {
    result.imageInstruction = ''
    const listImage = question.image.split('#')
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: questionList.map((ql: any, qlIndex: number) => {
          return {
            imageInstruction: getFullUrl(listImage[qlIndex]),
            question: ql,
            answer: correctAnswerList[qlIndex],
            answerList: answerList[qlIndex].replaceAll('*', '/'),
            position: qlIndex,
          }
        }),
      },
    ]
  } else if (questionType === 17) {
    result.instruction = question.parent_question_description
    result.questionInstruction = question.parent_question_text

    const descriptionList = question.question_description.split('[]')
    const questionList = question.question_text.split('[]')
    const questionListMC = questionList[1].split('#')
    const answerList = question.answers.split('[]')
    const answerListMC = answerList[1].split('#')
    const correctList = question.correct_answers.split('[]')
    const correctListTF = correctList[0].split('#')
    const correctListMC = correctList[1].split('#')
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        questionList: [
          {
            activityType: 8,
            instruction: descriptionList[0],
            answers: answerList[0]
              .split('#')
              .map((answer: any, answerIndex: number) => ({
                text: answer,
                isCorrect: correctListTF[answerIndex] === 'T',
              })),
          },
          {
            activityType: 11,
            instruction: descriptionList[1],
            questionsGroup: questionListMC.map((qt: any, qtIndex: number) => {
              const answers = answerListMC[qtIndex].split('*')
              return {
                question: qt,
                answers: answers.map((answer: any) => ({
                  text: answer,
                  isCorrect: getTextWithoutFontStyle(answer) === correctListMC[qtIndex],
                })),
              }
            }),
          },
        ],
      },
    ]
  } else if (questionType === 18) {
    const answerValues: any = { T: 'TRUE', F: 'FALSE', NI: 'NI' }
    result.instruction = question.parent_question_description
    result.questionInstruction = question.parent_question_text

    const descriptionList = question.question_description.split('[]')
    const questionList = question.question_text.split('[]')
    const questionListMC = questionList[1].split('#')
    const answerList = question.answers.split('[]')
    const answerListMC = answerList[1].split('#')
    const correctList = question.correct_answers.split('[]')
    const correctListLS = correctList[0].split('#')
    const correctListMC = correctList[1].split('#')
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        questionList: [
          {
            activityType: 8,
            instruction: descriptionList[0],
            answers: answerList[0]
              .split('#')
              .map((answer: any, answerIndex: number) => ({
                text: answer,
                answer: answerValues[correctListLS[answerIndex]],
                answerList: Object.values(answerValues).join('/'),
              })),
          },
          {
            activityType: 15,
            instruction: descriptionList[1],
            questionsGroup: questionListMC.map((qt: any, qtIndex: number) => {
              const answers = answerListMC[qtIndex].split('*')
              return {
                question: qt,
                answers: answers.map((answer: any) => ({
                  text: answer,
                  isCorrect: getTextWithoutFontStyle(answer) === correctListMC[qtIndex],
                })),
              }
            }),
          },
        ],
      },
    ]
  } else if (questionType === 20) {
    result.instruction = question.question_description
    result.questionInstruction = question.parent_question_text
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        questionsGroup: questionList.map((qt: string, qtIndex: number) => {
          const answers = answerList[qtIndex].split('*')
          return {
            id: question.id,
            question: qt.replaceAll('*',''),
            activityType: questionType,
            isExample: qt.startsWith('*') ? true : false,
            answers: answers.map((answer: any) => ({
              text: answer,
              isCorrect: getTextWithoutFontStyle(answer) === correctAnswerList[qtIndex],
            })),
          }
        }),
      },
    ]
  }else if (questionType === 21) {
    result.instruction = question.parent_question_description||question.question_description||""
    result.questionInstruction = question.parent_question_text
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        questionsGroup: questionList.map((qt: any, qtIndex: number) => {
          const answers = answerList[qtIndex].split('*')
          return {
            id: question.id,
            question: qt,
            activityType: questionType,
            answers: answers.map((answer: any) => ({
              text: answer,
              isCorrect: getTextWithoutFontStyle(answer) === correctAnswerList[qtIndex],
            })),
          }
        }),
      },
    ]
  }else if (questionType === 22 || questionType === 30) { // MC7, MC8 new
    result.instruction = question.parent_question_description 
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        questionsGroup: questionList.map((qt: any, qtIndex: number) => {
          const answers = answerList[qtIndex].split('*')
          return {
            id: question.id,
            activityType: questionType,
            question: qt,
            answers: answers.map((answer: any, aIndex: number) => ({
              imageAns: getFullUrl(answer),
              isCorrect: aIndex.toString() === correctAnswerList[qtIndex],
            })),
          }
        }),
      },
    ]
  }else if (questionType === 23) {
    result.questionInstruction = question.question_text
    const partLeft:any[] = answerList[0].split('*')||[]
    const partRight:any[] = answerList[1].split('*')||[]
    const checkExample = answerList[2]?.split('*')||[]
    const answersExample = []
    if(checkExample && checkExample[0] && checkExample[0]=="ex"){
      answersExample.push({
        left: partLeft[0],
        right: correctAnswerList[0],
        rightAnswerPosition: 0
      })
      const indexRight = partRight.indexOf(correctAnswerList[0])
      if(indexRight!=-1){
        partRight.splice(indexRight,1);
      }
      partLeft.shift();
      correctAnswerList.shift();
    }

    const answerDataRes =  partRight.map((pr: any, prIndex: number) => {
      const indexCorrect = partRight.findIndex(
        (m: any) => m === correctAnswerList[prIndex],
      );
      return {
        left: partLeft[prIndex],
        right: pr,
        rightAnswerPosition: indexCorrect
      }
    });
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: answerDataRes,
        corect_answers : correctAnswerList,
        answersExample: answersExample
      },
    ]
  }else if(questionType === 24 || questionType === 32){ // MC9, MC10
    result.instruction = question.parent_question_description||question.question_description||""
    result.questionInstruction = question.question_text
    result.imageInstruction = question?.image?.split('#').map((item:string)=> getFullUrl(item)).join('#')
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        questionsGroup:questionList.map((qt:any, qtIndex:number)=>{
          const answers = answerList[qtIndex].split('*')
          return {
            id: question.id,
            question: qt,
            activityType: questionType,
            answers: answers.map((answer: any) => ({
              text: answer,
              isCorrect: getTextWithoutFontStyle(answer) === correctAnswerList[qtIndex],
            })),
          }
        })
      },
    ]
  }else if (questionType === 25 || questionType === 26) {
    result.questionInstruction = question.parent_question_text ?? ''
    result.listContent = questionList.map((ql: any, qlIndex: number) => {
      return {
        id: question.id,
        activityType: questionType,
        question: ql,
        answers: correctAnswerList.map((ca: string, caIndex: number) => ({
          answer: ca,
          position: caIndex,
        })),
      }
    })
  }
  else if (questionType === 27) {
    const image = question?.image
    const mainImage = image.split('*')[0]
    const listSuggestAnswer =  question?.answers?.split('#') || [] 
    const correctAnswers = question.correct_answers.split('#')
    const arrayImg = image.split('*')[1].split('#')
  
    const answerData = [...correctAnswers,...listSuggestAnswer]
    const answerListImage = arrayImg
    result.imageInstruction = mainImage
    result.audioScript = question.audio_script ?? ''
    result.questionInstruction = question.parent_question_text ?? ''
    result.listContent = questionList.map((question: any, qIndex: number) => {
      return {
        id: question.id,
        activityType: questionType,
        question: question,
        answers: answerData.map((ca: string, caIndex: number) => ({
          answer: ca,
          position: caIndex,
          image: answerListImage[caIndex]
        })),
      }
    })
  }else if (questionType === 28 || questionType === 40) {
    const image = question?.image?.split('*') || []
    const mainImage = image[0]
    const correctAnswers = question.correct_answers.split('#')

    result.imageInstruction = getFullUrl(mainImage)
    result.audioScript = question.audio_script ?? ''
    result.questionInstruction = question.parent_question_text ?? ''
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: (correctAnswers ?? []).map((answer: string, answerIndex: number) => ({
          answer:answer.split('*')[2],
          position:answer.split('*')[1],
          image: answer.split('*')[0].startsWith('ex|') ? getFullUrl(answer.split('*')[0].substring(3)) : getFullUrl(answer.split('*')[0]),
          isExam: answer.split('*')[0].startsWith('ex|'),
          key: Guid.newGuid()
        })),
      },
    ]  
  }else if (questionType === 29) {
    result.audioScript = question.audio_script ?? ''
    result.questionInstruction = question.question_text
    result.imageInstruction = question?.image?.split('#').map((item:string)=> getFullUrl(item)).join('#')
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: answerList.map((answer: any, answerIndex: number) => ({
          text: answer,
          isCorrect: correctAnswerList[answerIndex] === 'T',
        })),
      },
    ]
  }else if (questionType === 31) {
    const image = question?.image?.split('*') || []
    const mainImage = image[0]
    const correctAnswers = question.correct_answers.split('#')

    result.imageInstruction = getFullUrl(mainImage)
    result.audioScript = question.audio_script ?? ''
    result.questionInstruction = question.parent_question_text ?? ''
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: (correctAnswers ?? []).map((answer: string, answerIndex: number) => ({
          isSelected:answer.split('*')[2] === 'True' ? true : false,
          position:answer.split('*')[1],
          image: answer.split('*')[0].startsWith('ex|') ? getFullUrl(answer.split('*')[0].substring(3)) : getFullUrl(answer.split('*')[0]),
          isExam: answer.split('*')[0].startsWith('ex|'),
          key: Guid.newGuid()
        })),
      }
    ]
  }else if (questionType === 34) {
    const image = question?.image?.split('*') || []
    const mainImage = image[0]
    const correctAnswers = question.correct_answers.split('#')

    result.imageInstruction = getFullUrl(mainImage)
    result.audioScript = question.audio_script ?? ''
    result.questionInstruction = question.parent_question_text ?? ''
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: (correctAnswers ?? []).map((answer: string, answerIndex: number) => ({
          answer:answer.split('*')[2],
          position:answer.split('*')[1],
          image: answer.split('*')[0].startsWith('ex|') ? getFullUrl(answer.split('*')[0].substring(3)) : getFullUrl(answer.split('*')[0]),
          isExam: answer.split('*')[0].startsWith('ex|'),
          key: Guid.newGuid()
        })),
      }
    ]
  }else if (questionType === 36){
    result.audioScript = question.audio_script ?? ''
    const listImage = question?.image?.split('#') || []
    const correctAnswersList = question.correct_answers.split('#')
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: correctAnswersList.map((answer: any, aIndex: number) => ({
          answer:answer,
          image: getFullUrl(listImage[aIndex]),
          isExam: answer.startsWith('*'),
          key: Guid.newGuid(),
        })
        )
      }
    ]
  }else if (questionType === 37) {
    result.questionInstruction = question.question_text
    const imageList:string[] = question?.image?.split('#') ?? []
    const partLeft:string[] = answerList[0].split('*') ?? []
    const partRight:string[] = answerList[1].split('*') ?? []
    const checkExample = answerList[2]?.split('*') ?? []
    const answersExample = []
    if(checkExample && checkExample[0] && checkExample[0] === "ex"){
      answersExample.push({
        left: partLeft[0],
        right: correctAnswerList[0],
        rightAnswerPosition: 0,
        image: getFullUrl(imageList[0])
      })
      const indexRight = partRight.indexOf(correctAnswerList[0])
      if(indexRight!=-1){
        partRight.splice(indexRight,1);
      }
      partLeft.shift();
      correctAnswerList.shift();
      imageList.shift()
    }

    const answerDataRes =  partRight.map((pr: any, prIndex: number) => {
      const indexCorrect = partRight.findIndex(
        (m: any) => m === correctAnswerList[prIndex],
      );
      return {
        left: partLeft[prIndex],
        right: pr,
        rightAnswerPosition: indexCorrect,
        image:getFullUrl(imageList[prIndex])
      }
    });
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: answerDataRes,
        corect_answers : correctAnswerList,
        answersExample: answersExample
      },
    ]
  }else if (questionType === 38) {
    result.audioScript = question.audio_script ?? ''
    result.questionInstruction = question.parent_question_text ?? ''
    const listData = answerList.map((answer: string, answerIndex: number) => 
    {
      const items = answer.split("*");
      return {
        color:items[2],
        shape:items[1],
        isExam: items[0].startsWith('ex|'),
        key: Guid.newGuid()
      }
    })
    listData.sort((m:any,n:any)=> {
      if(m.isExam) return -1
      if(n.isExam) return 1;
    })
    const listShape:any = {}
    for(let i=0; i<listData.length; i++){
      if(listShape[listData[i].shape]){
        listShape[listData[i].shape].push(listData[i].color)
      }else{
        listShape[listData[i].shape] = [listData[i].color]
      }
    }
    // list shape constants
    for(let i=0; i< SHAPE_CODE.length; i++){
      if(!listShape[SHAPE_CODE[i]]){
        listShape[SHAPE_CODE[i]] = []
      }
      while(listShape[SHAPE_CODE[i]].length<3){
        const arrColorNotHas = COLOR_SHAPE.filter(m=>listShape[SHAPE_CODE[i]].findIndex((x:string)=>x==m.color) == -1 )
        const color = shuffleArray(arrColorNotHas)[0].color;
        listShape[SHAPE_CODE[i]].push(color)
      }
      if(listData[0].isExam && SHAPE_CODE[i] == listData[0].shape){
        // not mix
      }else{
         listShape[SHAPE_CODE[i]] = shuffleArray(listShape[SHAPE_CODE[i]])
      }
    }
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: listData,
        shapes:listShape
      }
    ]
  }else if (questionType === 33) {
    result.questionInstruction = question.question_text
    const partLeft:string[] = answerList[0].split('*') ?? []
    const partRight:string[] = answerList[1].split('*') ?? []
    const checkExample = answerList[2]?.split('*') ?? []
    const answersExample = []
    if(checkExample && checkExample[0] && checkExample[0] === "ex"){
      answersExample.push({
        left: getFullUrl(partLeft[0]),
        right: getFullUrl(correctAnswerList[0]),
        rightAnswerPosition: 0,
      })
      const indexRight = partRight.indexOf(correctAnswerList[0])
      if(indexRight!=-1){
        partRight.splice(indexRight,1);
      }
      partLeft.shift();
      correctAnswerList.shift();
    }

    const answerDataRes =  partRight.map((pr: any, prIndex: number) => {
      const indexCorrect = partRight.findIndex(
        (m: any) => m === correctAnswerList[prIndex],
      );
      return {
        left: getFullUrl(partLeft[prIndex]),
        right: getFullUrl(pr),
        rightAnswerPosition: indexCorrect,
      }
    });
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: answerDataRes,
        corect_answers : correctAnswerList,
        answersExample: answersExample
      },
    ]
  }else if (questionType === 39 || questionType === 42) {
    const image = question?.image?.split('*') || []
    const mainImage = image[0]
    const correctAnswers = question.correct_answers.split('#')

    result.imageInstruction = getFullUrl(mainImage)
    result.audioScript = question.audio_script ?? ''
    result.questionInstruction = question.parent_question_text ?? ''
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        answers: (correctAnswers ?? []).map((answer: string, answerIndex: number) => ({
          color:answer.split('*')[2],
          position:answer.split('*')[1],
          image: answer.split('*')[0].startsWith('ex|') ? getFullUrl(answer.split('*')[0].substring(3)) : getFullUrl(answer.split('*')[0]),
          isExam: answer.split('*')[0].startsWith('ex|'),
          key: Guid.newGuid()
        })),
      }
    ]
  }else if (questionType === 41) { // MC11
    result.instruction = question.question_description 
    result.listContent = [
      {
        id: question.id,
        activityType: questionType,
        questionsGroup: answerList.map((answerQuestion: any, qtIndex: number) => {
          const answers = answerQuestion.split('*')
          const correctAnswers :string[] = correctAnswerList[qtIndex].split('*') ?? []
          return {
            id: question.id,
            activityType: questionType,
            isExample: answerQuestion.startsWith('ex|'),
            answers: answers.map((answer: any, aIndex: number) => ({
              imageAns: getFullUrl(answer.replace('ex|','')),
              isCorrect: correctAnswers.includes(aIndex.toString()),
            })),
          }
        }),
      },
    ]
  }

  return result
}

const getFullUrl = (url: string) => {
  const result = ''
  if (url != null && url != '') {
    return `${baseHostUrl}/upload/${url}`
  }
  return result
}
