import { NextApiRequest, NextApiResponse } from 'next'

import { EMAIL_CONFIG } from '@/interfaces/constants'
import dayjs from 'dayjs'
import { Message, SMTPClient } from 'emailjs'
import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case 'POST':
      return sendMail()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function sendMail() {

    const client = new SMTPClient(EMAIL_CONFIG)

    const data = req.body
    const user = await query<any[]>('SELECT * FROM user WHERE id = ? LIMIT 1', [
      data?.teacherId,
    ])
    try {
      const dateNow = dayjs()
      if (user.length > 0) {
        const EMAIL_TEMPLATE = `
        <!DOCTYPE html>
        <html>

        <head></head>

        <body>
          <div style="width: 100%;padding: 24px 0;background-color: #eeeeee;margin: 0px auto;">
              <table style="border-spacing: 0;max-width: 640px;margin: 0 auto;background-color: white;padding: 24px;font-family: 'Open Sans', sans-serif;font-weight: 400;font-size: 16px;line-height: 22px;box-shadow: 0px 10px 36px rgba(0, 0, 0, 0.08);border-top: 9px solid #ff6337;">
                <tbody>
                  <tr>
                    <td style="width: 100%;min-width: 100%;padding: 0;" width="100%">
                      <div style="font-weight: 500;font-size: 22px;line-height: 28px;margin: 8px 0 32px 0;text-align: center;">
                        Kính gửi quý thầy/ cô kết quả bài thi ${data?.testName} của học viên ${data?.name}. Thầy/ cô vui lòng kiểm tra thông tin chi tiết bên dưới.
                      </div>
                      <div style="border: 1px solid #D1E9FE;padding: 16px 32px;border-radius: 16px;overflow: auto;margin-bottom: 16px;">
                        <p>- Bài thi:<span> ${data?.testName}</span></p>
                        <p>- Khối lớp:<span> ${data?.gradeName.replace('g', '')}</span></p>
                        ${data?.class && `<p>- Lớp:<span> ${data?.class}</span></p>`}
                        <p>- Tên học sinh:<span> ${data?.name}</span></p>
                        <p>- Email:<span> ${data?.email}</span></p>
                        <p>- Xem lại bài thi:<span> <a href="${data?.historyURL}">${data?.historyURL}</a></span></p>
                        <p>- Điểm:<span> ${data?.point}</span></p>
                      </div>
                      <div style="font-weight: 500;font-size: 15px;line-height: 28px;margin: 16px 0;text-align: center; color: #ff0000;">
                          Đây là email tự động, vui lòng không trả lời email này!
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
        </body>

        </html>
          `
        const message = new Message({
          text: ``,
          from: EMAIL_CONFIG.user,
          // from: 'phamhaibkit1@gmail.com',
          to: user[0].email,
          cc: '',
          subject: `${dateNow.format('Ngày DD/MM/YYYY HH:mm')} - ${data?.testName} Kết quả bài thi của học viên ${data?.name}`,
          attachment: [{ data: EMAIL_TEMPLATE, alternative: true }],
        })

        client.send(message, (err, message) => {
          console.log('Mail message', err || message)
        })

        if (data?.email) {
          const EMAIL_STUDENT_TEMPLATE = `
          <!DOCTYPE html>
          <html>
  
          <head></head>
  
          <body>
            <div style="width: 100%;padding: 24px 0;background-color: #eeeeee;margin: 0px auto;">
              <table style="border-spacing: 0;max-width: 640px;margin: 0 auto;background-color: white;padding: 24px;font-family: 'Open Sans', sans-serif;font-weight: 400;font-size: 16px;line-height: 22px;box-shadow: 0px 10px 36px rgba(0, 0, 0, 0.08);border-top: 9px solid #ff6337;">
                <tbody>
                  <tr>
                    <td style="width: 100%;min-width: 100%;padding: 0;" width="100%">
                      <div style="font-weight: 500;font-size: 22px;line-height: 28px;margin: 8px 0 32px 0;text-align: center;">
                          Kính gửi học viên ${data?.name} kết quả của bài thi ${data?.testName}. Bạn vui lòng kiểm tra thông tin chi tiết bên dưới.
                      </div>
                      <div style="border: 1px solid #D1E9FE;padding: 16px 32px;border-radius: 16px;overflow: auto;margin-bottom: 16px;">
                        <p>- Bài thi:<span> ${data?.testName}</span></p>
                        <p>- Khối lớp:<span> ${data?.gradeName.replace('g', '')}</span></p>
                        <p>- Lớp:<span> ${data?.class}</span></p>
                        <p>- Tên học sinh:<span> ${data?.name}</span></p>
                        <p>- Email:<span> ${data?.email}</span></p>
                        <p>- Xem lại bài thi:<span> <a href="${data?.historyURL}">${data?.historyURL}</a></span></p>
                        <p>- Điểm:<span> ${data?.point}</span></p>
                      </div>
                      <div style="font-weight: 500;font-size: 15px;line-height: 28px;margin: 16px 0;text-align: center; color: #ff0000;">
                          Đây là email tự động, vui lòng không trả lời email này!
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
          </body>
  
          </html>
          `

          const messageStudent = new Message({
            text: ``,
            from: EMAIL_CONFIG.user,
            // from: 'phamhaibkit1@gmail.com',
            to: data?.email,
            cc: '',
            subject: `${dateNow.format('Ngày DD/MM/YYYY HH:mm')} - ${data?.testName} Kết quả bài thi của học viên ${data?.name}`,
            attachment: [{ data: EMAIL_STUDENT_TEMPLATE, alternative: true }],
          })
  
          client.send(messageStudent, (err, message) => {
            // console.log('Mail messageStudent', err || message)
          })
        }
        return res.json({
          isSuccess: true,
        })
      } else {
        return res.json({
          error: true,
          message: 'Không tìm thấy địa chỉ mail giáo viên',
        })
      }
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
