import { NextApiRequest, NextApiResponse } from 'next'

import { query } from 'lib/db'
import { cryptoDecrypt_key } from 'utils/crypto'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case 'POST':
      await handlePostRequest(req, res)
      break
    default:
      return res.status(405).json({ code: 0, message: 'Method Not Allowed' })
  }
}
const handlePostRequest = async (req: NextApiRequest, res: NextApiResponse) => {
  const dataResult = await getDataThirdParty(req.body)

  if (dataResult.code == 1) {
    return res.status(200).json(dataResult)
  } else {
    return res.status(400).json(dataResult)
  }
}
export const getDataThirdParty = async (body: any) => {
  const { third_party_code, unit_test_id, value } = body

  const arrData: any = await query<any>(
    `select secret_key from unit_test u
        join unit_test_third_party ut on u.id = ut.unit_test_id
        join third_party_config tp on u.template_level_id = tp.grade_id and u.series_id = tp.series_id and ut.third_party_code = tp.third_party_code
        where u.id = ? and ut.third_party_code = ? and u.deleted=0 and ut.deleted=0`,
    [unit_test_id, third_party_code],
  )

  if (!arrData || arrData.length == 0) {
    return { code: 0, message: 'Không tìm thấy bài thi tích hợp' }
  } else {
    const objData = JSON.parse(JSON.stringify(arrData[0]))
    const parseData = cryptoDecrypt_key(objData.secret_key, value)
    return { code: 1, data: parseData }
  }
}
export default handler
//secret key = "Hpv+cT+YZiHsvOFeiWhMc99en7jZmnRpb3nDkb5ZE/6GA6rdKeRI8Zw/uzdfPZdCxCLijd2hIu4++WBtlMVUsg=="
/// value= "pW2zqqhjgfmRWluu8dryynjE0sL1JoFxeL5v0WJI8BkKzcMnkjoHVgRYhJgys5PW66c/zdkJoHFqbTDGzNqx8MOkp6ENUIlCYti5quq1B6mivRMYsPyJYUG9RmTUwRJNk21gQnEIpKunWHE9KUHdxvelVjGDBdkOkyW72KmmIRHKDprHS0HwgNrH1Y0zbdV59wZ96hNurnzTmgv13BTf+px76hmUUrGlF7YENs97gcbWEOpjth03/ak26XLNEx7mW0L7yk+EVogTHAI37fxTiTuw4q4Cul85oLQR9EpPNAIJA5UVFuBoQpx4XDfx2mnV/M2HJmWl5Oh20QK80SdQVw=="
