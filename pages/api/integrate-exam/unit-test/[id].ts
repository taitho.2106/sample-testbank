import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { USER_ROLES } from 'interfaces/constants'
import { query } from 'lib/db'
import { userInRight } from 'utils'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  if (!userInRight([USER_ROLES.Operator], session)) {
    return res.status(403).end('Forbidden')
  }
  switch (req.method) {
    case 'GET':
      return getUnitTestById(req, res)
    case 'DELETE':
      break
    default:
      return res.status(405).json({ code: 0, message: 'Method Not Allowed' })
  }
}

async function getUnitTestById(req: NextApiRequest, res: NextApiResponse) {
  const { id } = req.query
  if (!id) return res.status(400).json({ code: 0, message: 'id required' })
  try {
    const unitTests: any[] = await query<any[]>(
      'SELECT * FROM unit_test WHERE deleted = 0 AND id = ? ',
      [id],
    )
    if (unitTests.length === 0) {
      return res.status(400).json({ code: 0, message: 'data not found' })
    }
    return res.status(200).json({ code: 1, data: unitTests, message: '' })
  } catch (error: any) {
    return res.status(500).json({ code: 0, message: error.message })
  }
}

export default handler
