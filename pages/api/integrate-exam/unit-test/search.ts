import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from '../../../../lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  if (!session){
    return res.status(403).json('Forbidden')
  }
  switch (req.method) {
    case 'POST':
      return getUnitTestIntegrate()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getUnitTestIntegrate() {
  
  try {
    const page: any  = req?.body?.page || 0
    const limit = 10
    const name = req?.body?.name || ''
    const series_id  = req?.body?.series_id || []
    const third_party_code = req?.body?.third_party_code || []
    const template_level_id = req?.body?.template_level_id || [] 
    
    // filter conditions
    let condition = ''
    if(name){
      condition += ` AND (ut.name LIKE '%${name}%')`
    }
    if(series_id.length > 0){
        let series_id_string = ''
        series_id.forEach((item: string, i: number) => (
            series_id_string += `${i !== 0 ? ',' : ''}"${item}"`
        ))
        condition += ` AND ut.series_id IN (${series_id_string})`
    }

    if(third_party_code.length > 0){
        let third_party_code_string = ''
        third_party_code.forEach((item: string, i: number) => (
          third_party_code_string += `${i !== 0 ? ',' : ''}"${item}"`
        ))
        condition += ` AND u.third_party_code IN (${third_party_code_string})`
    }

    if(template_level_id.length > 0){
        let template_level_id_string = ''
        template_level_id.forEach((item: string, i: number) => (
            template_level_id_string += `${i !== 0 ? ',' : ''}"${item}"`
        ))
        condition += ` AND ut.template_level_id IN (${template_level_id_string})`
    }
    // console.log("condition==", condition)

    const results: any[] = await query(
      `SELECT u.*, ut.name, ut.template_level_id, ut.series_id FROM unit_test as ut 
      JOIN unit_test_third_party as u
      ON (u.unit_test_id = ut.id) 
      WHERE u.deleted = 0  AND ut.deleted = 0 ${condition} 
      ORDER BY u.created_date DESC 
      LIMIT ${limit} OFFSET ${parseInt(page)*limit}`,
      [],
    )

    const totals = await query<any>(
      `SELECT DISTINCT u.id FROM unit_test_third_party as u
      JOIN unit_test as ut ON ut.id = u.unit_test_id
      WHERE ut.deleted = 0 AND u.deleted = 0 ${condition}`,
      []
    )
    // console.log("total====", totals)
    // console.log("results====", results)
    return res.status(200).json({
      data: results,
      currentPage:parseInt(page),
      limit:limit,
      totalPages: Math.ceil(totals.length/limit)
    })
  } 

  catch (error: any) {
      console.log("error.message===", error.message)
      return res.status(500).json({ message: error.message })
    }
  }

}
export default handler
