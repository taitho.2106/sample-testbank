import { NextApiRequest, NextApiResponse } from 'next'

import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case 'POST':
      await handlePostRequest(req, res)
      break
    default:
      return res.status(405).json({ code: 0, message: 'Method Not Allowed' })
  }
}
const handlePostRequest = async (req: NextApiRequest, res: NextApiResponse) => {
  const dataResult = await getThirdPartyChildAppId(req.body)
  if (dataResult.code == 1) {
    return res.status(200).json(dataResult)
  } else {
    return res.status(400).json(dataResult)
  }
}
export const getThirdPartyChildAppId = async (body: any) => {
  const { third_party_code, unit_test_id } = body
  if(!third_party_code || !unit_test_id){
    return { code: 0, message: 'body fail' }
  }
  const arrData: any = await query<any>(
    `select child_app_id from unit_test u
        join unit_test_third_party ut on u.id = ut.unit_test_id
        join third_party_config tp on u.template_level_id = tp.grade_id and u.series_id = tp.series_id and ut.third_party_code = tp.third_party_code
        where u.id = ? and ut.third_party_code = ? and u.deleted=0 and ut.deleted=0 and tp.deleted = 0`,
    [unit_test_id, third_party_code],
  )
  if (!arrData || arrData.length == 0) {
    return { code: 0, message: 'Không tìm thấy bài thi tích hợp' }
  } else {
    const objData = JSON.parse(JSON.stringify(arrData[0]))
    return { code: 1, data: objData.child_app_id }
  }
}
export default handler
