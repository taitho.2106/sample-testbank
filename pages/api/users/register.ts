import bcrypt from 'bcrypt'
import { NextApiRequest, NextApiResponse } from 'next'

import { commonDeleteStatus } from '@/constants/index'
import { initResource } from '@/hooks/useTranslation'
import { PACKAGES, USER_ASSET_TYPE, USER_ROLES } from '@/interfaces/constants'
import { generateUsername } from '@/utils/user'
import dayjs from 'dayjs'
import { UserDataType } from 'interfaces/types'
import { query } from 'lib/db'
import prisma from 'lib/prisma'
import { getPackageByCode } from 'services/package'
import { saveUserAssets } from 'services/userAssets'

const trialMonth = 1;

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const locale = req.cookies.locale
  const { t } = initResource(locale);
  switch (req.method) {
    case 'POST':
      return createUser()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function createUser() {
    const item: UserDataType = req.body
    try {
      let salt = null
      let password = null
      if (item.password) {
        salt = await bcrypt.genSalt(10)
        password = await bcrypt.hash(item.password, salt)
      }

      const newId = (await query<any[]>('SELECT uuid() as newId'))[0].newId

      const user = await prisma.user.findFirst({
        where: {
            OR: [
                { email: item.email },
                { phone: item.phone },
            ],
            deleted: commonDeleteStatus.ACTIVE,
        }
      });

      if (user) {
        return res.json({
          error: true,
          message: t('mess-handler')['phone-email-usaged'],
        })
      }

      const username = generateUsername(item.phone);

      const acceptRegisterRoles = [
        USER_ROLES.Student,
        USER_ROLES.Teacher,
      ]
      if (!item.user_role_id || !acceptRegisterRoles.includes(item.user_role_id)) {
        return res.json({
            error: true,
            message: 'User role invalid!',
        })
      }

      const result = await query<any>(
        `INSERT INTO user (id, user_name, email, password, salt, user_role_id, created_date, phone, full_name, province_id)
                   VALUES (?,?,?,?,?,?,?,?,?,?)`,
        [
          newId,
          username,
          item.email ? item.email.toLowerCase() : item.email,
          password,
          salt,
          item.user_role_id,
          new Date(),
          item.phone,
          item.full_name,
          item.province_id
        ],
      )

      if (item.user_role_id === USER_ROLES.Teacher) {
          // save user asset package trial
          const startDate = new Date();
          const trialPackage = await getPackageByCode(PACKAGES.BASIC.CODE);
          const endDate = dayjs(startDate).add(trialMonth, 'M').toDate();
          const saveAssetResult = saveUserAssets({
            userId: newId,
            assetId: trialPackage.id,
            assetType: USER_ASSET_TYPE.SERVICE_PACKAGE,
            quantity: 1,
            createdDate: startDate,
            endDate,
        });

        if (!saveAssetResult) {
            throw Error('insert trial package failed')
        }
      }

      if (result.affectedRows !== 1) {
        throw Error('insert failed')
      }
    return res.json({ id: newId })      
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
