import { uploadDir } from '@/constants/index'
import { initResource } from '@/hooks/useTranslation'
import { IncomingForm } from 'formidable'
import { query } from 'lib/db'
import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
import fileUtils from '../../../utils/file'

export const config = {
  api: {
    bodyParser: false,
  },
}

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session?.user
  const locale = req.cookies.locale
  const { t } = initResource(locale);
  if (!session) {
    return res.status(403).end('Forbidden')
  }

  const uploadPath = `${uploadDir}/${user.id}/avatar`

  switch (req.method) {
    case 'POST':
      return saveAvatar()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function saveAvatar() {
    try {
      const item: any = await handleRequest(req)
      const temp = new Date().getTime()
      const partName = item.file_avatar.originalFilename.split('.')
      const fileName = `icon-${temp}.${partName[partName.length - 1]}`
      await saveFile(item.file_avatar.filepath, uploadPath, fileName)
      const path = `${user.id}/avatar/${fileName}`

      const result = await query<any>(
        'UPDATE user SET avatar = ? WHERE id = ?',
        [path, user.id],
      )
      if (result.affectedRows === 0)
        return res.json({
          isSuccess: false,
          message: t('mess-handler')['Password change failed'],
        })
      return res.json({ isSuccess: true, avatar: path })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

const handleRequest = (req: NextApiRequest) => {
  return new Promise((resolve, reject) => {
    const form = new IncomingForm()
    form.parse(req, async (err, fields, files) => {
      if (err) reject(err)
      resolve({ ...fields, ...files })
    })
  })
}

const saveFile = async (
  filePath: string,
  pathDes: string,
  fileName: string,
) => {
  let file = null
  try {
    file = await fileUtils.exist(pathDes)
  } catch {}
  if (!file) {
    fileUtils.createDir(pathDes, { recursive: true })
  }
  const fileData = await fileUtils.readFile(filePath)
  return await fileUtils.writeFile(`${pathDes}/${fileName}`, fileData)
}

export default handler
