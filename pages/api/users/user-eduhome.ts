import bcrypt from 'bcrypt'
import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
import { signIn } from 'next-auth/client'
import { UserEduhomeDataType } from 'interfaces/types'
import { query } from 'lib/db'
import { userInRight } from 'utils'
import { USER_ROLES } from 'interfaces/constants'
const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  switch (req.method) {
    case 'POST':
      return checkAddUser()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function checkAddUser() {
    const item: UserEduhomeDataType = req.body
    try {
      console.log("server_date_now",new Date())
        console.log("body", item);
      let salt = null
      let password = null
      salt = await bcrypt.genSalt(10)
      password = await bcrypt.hash("Testbank@123", salt)
      const user = await query<any[]>(
        'SELECT * FROM user WHERE user_name = ? and deleted = 0',
        [item.phone],
      )
      if (user.length > 0) { //đã có tài khoản

        console.log("user",user[0].user_id, item)
       await  updateUserDateLimit(user[0].id, item);
       return  res.status(200).json({ message: "success" })
      }
      const newId = (await query<any[]>('SELECT uuid() as newId'))[0].newId
      const regNumber = /^\d+$/;
      const user_name = item.phone;
      const phone = item.phone.length<=10?(item.phone.match(regNumber)?item.phone:""):"";

      const result = await query<any>(
        `INSERT INTO user (id, user_name, phone, password, salt, user_role_id, created_date)
                   VALUES (?,?,?,?,?,?,?)`,
        [
          newId,
          user_name,
          phone,
          password,
          salt,
          USER_ROLES.Teacher,
          new Date(),
        ],
      )
      if(item.mode.toLowerCase() == 'trial'){
        const result_user_time = await query<any>(
            `INSERT INTO user_date_limit ( user_id, start_date, end_date, is_trial_mode, deleted, created_date)
                       VALUES (?,FROM_UNIXTIME(?),FROM_UNIXTIME(?),?,?,?)`,
            [
              newId,
              item.start_date/1000,
              item.end_date/1000,
              true,
              0,
              new Date(),
            ],
          )
      }
      console.log(result)
      if (result.affectedRows !== 1) {
          console.log("result==================================",result)
        throw Error('insert failed')
      }
      return res.json({ id: newId })
    } catch (e: any) {
     return res.status(500).json({ message: e.message })
    }
  }
  async function updateUserDateLimit( user_id:string, model:UserEduhomeDataType) {

    const itemChecks = await query<any[]>(
      'SELECT * FROM user_date_limit WHERE deleted=0 and user_id = ?',
      [user_id],
    )
    const isTrialMode = model.mode == 'trial'?true:false;

    //update to active
    if(itemChecks.length>0 && isTrialMode == false){
      const item = itemChecks[0];
      console.log("item_checkuserlimit2", item);
      if(item.is_trial_mode==1){ //only update when before status == trial
        const start_date =  model.start_date ? new Date(model.start_date):new Date();
        const end_date =model.end_date ? new Date(model.end_date):new Date();
        const queryString = "Update user_date_limit set start_date=FROM_UNIXTIME(?), end_date= FROM_UNIXTIME(?), is_trial_mode= ?, updated_date=?, updated_by = ? where id=?";
          const result = await query<any>(
              queryString,[model.start_date/1000, model.end_date/1000, isTrialMode, new Date(), user_id, item.id]
            );
      }
    }
  }
}

export default handler
