import { NextApiRequest, NextApiResponse } from 'next'

import { commonDeleteStatus } from '@/constants/index'
import { initResource } from '@/hooks/useTranslation'
import { UserDataType } from 'interfaces/types'
import prisma from 'lib/prisma'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const locale = req.cookies.locale
  const { t } = initResource(locale);
  switch (req.method) {
    case 'POST':
      return createUser()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function createUser() {
    const item: UserDataType = req.body
    const { phone, email } = item;
    try {
      const user = await prisma.user.findFirst({
        where: {
            OR: [
                { email },
                { phone }
            ],
            deleted: commonDeleteStatus.ACTIVE,
        }
      });

      let message = undefined, isExist = false, field = undefined;
      if (user) {
        isExist = true;
        const isPhoneExisted = phone === user.phone;
        field = isPhoneExisted ? 'phone' : 'email';
        message = isPhoneExisted ? t(t('mess-handler')['phone-registerd'],[t('phone')]) : t(t('mess-handler')['phone-registerd'], ['Email']);
      }

      return res.json({ isExist, message, field })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
