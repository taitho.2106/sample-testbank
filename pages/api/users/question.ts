import { NextApiRequest, NextApiResponse } from "next";

import { getContentEmailNotiNewUserQuestion } from "@/constants/email-template";
import { RECEIVE_FEEDBACK_EMAIL } from "@/constants/index";
import dayjs from "dayjs";
import prisma from "lib/prisma";
import { sendEmail } from "services/email";

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    switch (req.method) {
        case 'POST': {
            try {
                const {
                    name,
                    email,
                    phone,
                    feedback,
                } = req.body;

                if ([name, email, phone].some(el => el !== 0 && !el)) {
                    return res.status(400).json({ isSuccess: false, message: `Missing required field` });
                }

                await prisma.user_question.create({
                    data: {
                        name,
                        email,
                        phone,
                        feedback,
                        created_date: new Date(),
                    }
                });

                sendEmail({
                    to: RECEIVE_FEEDBACK_EMAIL,
                    subject: `${dayjs().format('Ngày DD/MM/YYYY HH:mm')} - i-Test vừa tiếp nhận một thông tin liên hệ`,
                    content: getContentEmailNotiNewUserQuestion({
                        customerName: name,
                        customerPhone: phone,
                        customerEmail: email,
                        customerFeedback: feedback,
                    }),
                });

                return res.json({ isSuccess: true, message: 'Insert success' });
            } catch (error) {
                return res.json({ isSuccess: false, error });
            }
        }
        default: {
            return res.status(405).end(`Method ${req.method} Not Allowed`);
        }
    }
}

export default handler;