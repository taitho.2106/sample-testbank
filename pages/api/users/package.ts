import dayjs from "dayjs";
import { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/client";

import { getLastPackage } from "services/package";

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    if (!session) {
        return res.status(401).end('Unauthorized')
    }

    switch (req.method) {
        case 'GET': {
            const user: any = session?.user;
            const data = await getLastPackage(user.id);

            return res.json({
                data: data ?? null,
                result: !!data,
                serverTime: new Date()
            });
        }
        default: {
            return res.status(405).end(`Method ${req.method} Not Allowed`)
        }
    }
}

export default handler;