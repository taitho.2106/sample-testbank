import bcrypt from 'bcrypt'
import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { initResource } from '@/hooks/useTranslation'
import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session?.user
  const locale = req.cookies.locale
  const { t } = initResource(locale);
  if (!session) {
    return res.status(403).end('Forbidden')
  }

  switch (req.method) {
    case 'POST':
      return changePassword()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function changePassword() {
    const { password, new_password } = req.body
    try {
      const userInfos = await query<any[]>(
        `SELECT * FROM user WHERE id = ? LIMIT 1`,
        [user.id],
      )

      if (userInfos.length > 0) {
        const userInfo = userInfos[0]
        const result = await bcrypt.compare(password, userInfo.password)
        if (result) {
          const salt = await bcrypt.genSalt(10)
          const newPassword = await bcrypt.hash(new_password, salt)
          const result = await query<any>(
            'UPDATE user SET salt = ?, password = ? WHERE id = ?',
            [salt, newPassword, user.id],
          )
          if (result.affectedRows === 0)
            return res.json({
              isSuccess: false,
              message: t('mess-handler')['change-password-fail'],
            })
          return res.json({ isSuccess: true })
        } else {
          return res.json({
            isSuccess: false,
            message: t('mess-handler')['old-password-incorrect'],
          })
        }
      } else {
        return res.status(404).end('User not found')
      }
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
