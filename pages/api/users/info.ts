import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import prisma from 'lib/prisma'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session?.user
  if (!session) {
    return res.status(403).end('Forbidden')
  }

  switch (req.method) {
    case 'GET':
      return getInfo()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getInfo() {
    try {
      const userInfo = await prisma.user.findUnique({
        where: { id: user.id },
        select: {
            user_name: true,
            email: true,
            full_name: true,
            avatar: true,
            phone: true,
            address: true,
            user_role_id: true,
            is_admin: true,
            is_received_gift: true,
            province_id: true
        }
      })
      return res.json(userInfo)
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
