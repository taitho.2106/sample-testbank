import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { commonDeleteStatus } from '@/constants/index'
import { initResource } from '@/hooks/useTranslation'
import prisma from 'lib/prisma'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session?.user
  if (!session) {
    return res.status(403).end('Forbidden')
  }
  const locale = req.cookies.locale
  const { t } = initResource(locale);
  switch (req.method) {
    case 'POST':
      return saveInfo()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function saveInfo() {
    const { full_name, phone, address, email, province_id } = req.body
    try {
      const existedUser = await prisma.user.findFirst({
        where: {
            OR: [
                { phone: phone },
                { email: email },
            ],
            deleted: commonDeleteStatus.ACTIVE,
            id: { not: user.id }
        }
      });
      if (existedUser) {
        const getErrorData = () => {
            if (existedUser.phone === phone) {
                return { field: 'phone', message: t(t('mess-handler')['phone-usaged'],[t('phone')]) };
            }
            return { field: 'email', message: t(t('mess-handler')['phone-usaged'],['Email']) };
        }

        return res.json({
            error: true,
            ...getErrorData(),
        })
      }

      const saveResult = await prisma.user.update({
        data: {
            full_name,
            email: user.email ? undefined : email,
            phone: user.phone ? undefined : phone,
            address,
            province_id
        },
        where: {
            id: user.id,
        }
      });

      if (saveResult) {
        return res.json({ isSuccess: true })
      }

      return res.json({
        isSuccess: false,
        message: t('mess-handler')['save-fail'],
      })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
