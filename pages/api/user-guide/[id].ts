import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {

    switch (req.method) {
        case 'GET':
          return getUserGuideById()
       
        default:
          return res.status(405).end(`Method ${req.method} Not Allowed`)
    }

    async function getUserGuideById() {
        const { id } = req.query
        try {
            if (!id) {
                return res.status(400).json({ message: '`id` required' })
            }
            const results = await query<any[]>(
                'SELECT * FROM user_guide WHERE deleted = 0 AND id = ? LIMIT 1',
                [id],
            )
            if (results.length === 0) {
                return res.status(400).json({ message: 'data not found' })
            }
            return res.status(200).json(results[0])
        } catch (e: any) {
            res.status(500).json({ message: e.message })
        }
      }

}

export default handler