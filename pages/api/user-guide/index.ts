import { NextApiRequest, NextApiResponse } from 'next'
// import { getSession } from 'next-auth/client'

import { UserGuideDataType } from 'interfaces/types'
import { query } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    switch (req.method) {
        case 'GET':
        return getUserGuide()
        default:
        return res.status(405).end(`Method ${req.method} Not Allowed`)
    }

    async function getUserGuide() {
       const { screenId }:any = req.query
       if(!screenId){
       return res.status(400).json({ message: "ScreenId is incorrect" })
       }
        try {
            const user_guide = await query<any[]>(
                'SELECT * FROM user_guide WHERE deleted = 0 and screen_id = ?',
                [screenId],
            )
            return res.status(200).json(user_guide)
        }
        catch (e: any) {
            console.log("e",e);
            res.status(500).json({ message: e.message })
        }
    }
}

export default handler