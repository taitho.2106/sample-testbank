import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'

import { paths } from 'api/paths'
import api from 'lib/api'
import { query, queryWithTransaction } from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  let user: any = {}
  if (session) {
    user = session?.user
  }
  switch (req.method) {
    case 'PUT':
      //send data
      await createUnitTestResult()
      break
    case 'GET':
      return await getUnitTestResult()
    default:
      return res.status(405).json({ code: 0, message: 'Method Not Allowed' })
  }

  async function getUnitTestResult() {
    try {
      const unitTestResultId = req.query.id
      const unitTestId = req.query.unit_test_id
      if (!unitTestResultId)
        return res.status(400).json({ code: 0, message: 'data not found' })

      const infoResult = await query<any>(
        `select id as 
        unit_test_id,
         third_party_user_id, 
        third_party_code, 
        point, max_point, 
        created_date, 
        user_name,
        user_class,
        user_email
        from unit_test_result where id = ? and unit_test_id = ?`,
        [unitTestResultId, unitTestId],
      )
      if (!infoResult || infoResult.length == 0) {
        return res.status(400).json({ code: 0, message: 'data not found' })
      }
      const dataQuestions = await query<any>(
        `select usr.unit_test_section_id, 
         qr.total_question as group_question, 
         qr.question_id, 
         qr.point as question_point,
         qr.user_answer 
        from unit_test_result as ur
        join unit_test_section_result as usr on ur.id = usr.unit_test_result_id and ur.deleted = usr.deleted
        join question_result as qr on usr.id =  qr.unit_test_section_result_id and usr.deleted = qr.deleted
        where ur.deleted = 0 and ur.id = ?
        `,
        [unitTestResultId],
      )
      let bookName = ''
      const thirdPartyChildAppIds = await query<any>(
        `select child_app_id from third_party_config as t
      join unit_test u on t.grade_id = u.template_level_id and t.series_id = u.series_id
      where third_party_code = ? and u.id = ?  
      LIMIT 1`,
        [infoResult[0].third_party_code,unitTestId]
      )
      console.log("thirdPartyChildAppIds---",thirdPartyChildAppIds)
      // BOOK DETAIL
      if (thirdPartyChildAppIds && thirdPartyChildAppIds[0]) {
        const thirdPartyChildAppId = parseInt(thirdPartyChildAppIds[0].child_app_id)
        const themeResult: any = await api.get(
          `${process.env.NEXT_PUBLIC_EDUHOME_BASE_API}${paths.api_eduhome_get_theme_info}?id=${thirdPartyChildAppId}`,
          { headers: { Authorization: `${process.env.EDUHOME_AUTH}` } },
        )
        console.log("themeResult,----",themeResult.data)
        if (themeResult && themeResult.status == 200) {
          bookName = themeResult.data.NameVN
        }
      }else{
        const unitTestNames: any[] = await query(
          `SELECT name FROM unit_test WHERE id = ?`,
          [unitTestId])
          if(unitTestNames.length >0){
            bookName = unitTestNames[0].name
          }
      }
      // ACCOUNT INFO
      const accountInfo = {
        user_name:!infoResult[0].third_party_code?`${infoResult[0].user_name||""} - ${infoResult[0].user_class||""}`:"",
        user_phone:"",
        user_email:infoResult[0].user_email
      }
      if(infoResult[0].third_party_user_id){
        const accountRes: any = await api.get(
          `${process.env.NEXT_PUBLIC_EDUHOME_ACC_BASE_API}${paths.api_eduhome_get_account_info}/${infoResult[0].third_party_user_id}`,
          { headers: { Authorization: `${process.env.EDUHOME_AUTH}` } },
        )
        if (accountRes && accountRes.status == 200) {
          accountInfo.user_email = ""
          accountInfo.user_name = accountRes.data.Fullname,
          accountInfo.user_phone = accountRes.data.Phone?accountRes.data.Phone:accountRes.data.Login
        }
      }
      const dataResult = {
        ...infoResult[0],
        book_name:bookName,
        ...accountInfo,
        section_data: dataQuestions,
      }
      return res.status(200).json({ code: 1, data: dataResult })
    } catch (e) {
      console.log('e------', e)
      return res.status(500).json({ code: 0, message: 'Have an error' })
    }
  }

  async function createUnitTestResult() {
    const data = req.body
    const dateNow = new Date()
    try {
      if (!data) {
        return res.status(400).json({ code: 0, message: 'data not found' })
      }

      //check unit_test
      const unitTests = await query<any>(
        `SELECT id,name FROM unit_test WHERE deleted = 0 AND id = ? LIMIT 1`,
        [data.unit_test_id],
      )
      if (unitTests.length === 0) {
        return res.status(400).json({ code: 0, message: 'unit_test not found' })
      }
      if (data?.third_party_code) {
        //check third_party
        const thirdParties = await query<any>(
          `SELECT * FROM third_party WHERE id = ? LIMIT 1`,
          [data.third_party_code],
        )
        if (thirdParties.length === 0) {
          return res
            .status(400)
            .json({ code: 0, message: 'third_party_code is incorrect' })
        }
      }
      const unitTest = unitTests[0]
      const listIndex: any = { index: 0, sections: [] }
      let index = 0
      const listQ: any[] = [
        () => [
          `INSERT INTO unit_test_result(
            unit_test_id, 
            unit_test_name, 
            point, 
            max_point, 
            test_time, 
            third_party_code, 
            third_party_user_id, 
            third_party_device_id,
            user_name, 
            user_class, 
            user_email, 
            deleted, 
            created_by, 
            created_date
          ) 
          VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
          [
            data.unit_test_id,
            unitTest.name,
            data.point,
            data.max_point,
            data.time,
            data.third_party_code,
            data.third_party_user_id,
            data.third_party_device_id,
            data.user_name,
            data.user_class,
            data.user_email,
            0,
            user?.id,
            dateNow,
          ],
        ],
      ]
      data.sections.forEach((section: any, sectionIndex: number) => {
        listQ.push((r1: any, r2: any) => {
          return [
            `INSERT INTO unit_test_section_result(unit_test_section_id, unit_test_result_id, name, point, max_point, deleted, created_by, created_date) 
            VALUES(?,?,?,?,?,?,?,?)`,
            [
              section.unit_test_section_id,
              r2[listIndex.index].insertId,
              section.section_name,
              section.point,
              section.max_point,
              0,
              user?.id,
              dateNow,
            ],
          ]
        })
        index++
        listIndex.sections.push({ index })
        section.questions.forEach((question: any, questionIndex: number) => {
          listQ.push((r1: any, r2: any) => {
            return [
              `INSERT INTO question_result(question_id, unit_test_section_result_id, total_question, point, max_point, user_answer, deleted, created_by, created_date) 
               VALUES(?,?,?,?,?,?,?,?,?)`,
              [
                question.question_id,
                r2[listIndex.sections[sectionIndex].index].insertId,
                question.total_question,
                question.point,
                question.max_point,
                question.user_answer,
                0,
                user?.id,
                dateNow,
              ],
            ]
          })
          index++
        })
      })

      const result = await queryWithTransaction<any>(listQ)
      console.log('createUnitTestResult====success', result[0].insertId)
      return res
        .status(200)
        .json({ code: 1, data: { id: result[0].insertId, userId: user.id } })
    } catch (e: any) {
      return res.status(500).json({ code: 0, message: e.message })
    }
  }
}

const ex = `{
  "unit_test_id":3,
  "point":10,
  "max_point":100,
  "time":10000,
  "third_party_code":"eduhome",
  "user_name":"nguyen van a",
  "user_class": "class 1",
  "user_email":"ng@d.com",
  "sections":[
      {
          "unit_test_section_id":5,
          "section_name": "section 1",
          "point":5,
          "max_point":10,
          "questions":[
              {
                  "question_id":123,
                  "total_question":3,
                  "point":5,
                  "max_point":5,
                  "user_answer":"{\"status\":true,\"value\":\"dap an 1\"}"
              },
              {
                  "question_id":124,
                  "total_question":2,
                  "point":0,
                  "max_point":2,
                  "user_answer":"{\"status\":true,\"value\":\"dap an 1\"}"
              },
              {
                  "question_id":125,
                  "total_question":2,
                  "point":0,
                  "max_point":3,
                  "user_answer":"{\"status\":true,\"value\":\"dap an 1\"}"
              }
          ]
      },
              {
          "unit_test_section_id":6,
          "section_name": "section 2",
          "point":5,
          "max_point":95,
          "questions":[{
                  "question_id":128,
                  "total_question":3,
                  "point":5,
                  "max_point":5,
                  "user_answer":"{\"status\":true,\"value\":\"dap an 1\"}"
              },
              {
                  "question_id":129,
                  "total_question":2,
                  "point":0,
                  "max_point":2,
                  "user_answer":"{\"status\":true,\"value\":\"dap an 1\"}"
              },
              {
                  "question_id":122,
                  "total_question":2,
                  "point":0,
                  "max_point":3,
                  "user_answer":"{\"status\":true,\"value\":\"dap an 1\"}"
              }]
      }
  ]
}`
export default handler
