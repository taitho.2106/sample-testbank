import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
import { query} from 'lib/db'

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const user: any = session?.user
  const isAdmin = user?.is_admin == 1;
  switch (req.method) {
    case 'POST':
      return updatePrivacy()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function updatePrivacy() {
    if(!isAdmin){
        return res.status(403).end('Forbidden') 
    }
    const data: any = req.body
    const id = data?.id
    console.log("data----",data)
    if (!id) return res.status(400).json({ message: 'data not found' })
    const privacy = data?.privacy || 0
    try {
      await query<any>(
        `
        UPDATE question 
        SET privacy = ? 
        WHERE id = ?
      `,
        [privacy,id],
      )
      return res.status(200).json({ status: 200, data: { ...data } })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler
