import { uploadDir } from '@/constants/index'
import fs from 'fs'
import fileUtils from 'utils/file'
import Guid from 'utils/guid'
import { getFileExtension } from 'utils/string'
const handleDL1Data = async (data: any, uploadPath: string, userId: string) => {
  type answerType = {
    data: string
    key: string
    minX: number
    minY: number
    maxX: number
    maxY: number
    name?: string
    isExample?: boolean
  }
  try {
    if (!data) return data
    uploadPath = `${uploadPath}/dl1`
    if (data.image_question_base64 && data.image_question_export_base64) {
      //image question
      const fileExQ = data.image_question_base64.split(';')[0].split('/')[1]
      const baseDataQ = data.image_question_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameQ = `${Guid.newGuid()}.${fileExQ}`
      await saveFile(baseDataQ, uploadPath, newFileNameQ)
      const imageQuestion = `${userId}/dl1/${newFileNameQ}`

      //image export word
      const fileExW = data.image_question_export_base64.split(';')[0].split('/')[1]
      const baseDataW = data.image_question_export_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameW = `${Guid.newGuid()}.${fileExW}`
      await saveFile(baseDataW, uploadPath, newFileNameW)
      const imageExport = `${userId}/dl1/${newFileNameW}`
      data.image = `${imageQuestion}*${imageExport}`
      delete data.image_question_base64
      delete data.image_question_export_base64
    }else{
      throw Error("Có lỗi xảy ra, không tìm thấy hình ảnh được tải lên, vui lòng thử lại")
    }
    delete data.passData

    if (data.list_answer_item && data.list_image_item_position) {
      const list_answer_item = JSON.parse(data.list_answer_item)
      const list_image_item_position = JSON.parse(data.list_image_item_position)
      const arrCorrectAnswer: answerType[] = []
      for (let i = 0; i < list_answer_item.length; i++) {
        const item = list_answer_item[i]
        const fileEx = item.data.split(';')[0].split('/')[1]
        const baseData = item.data.replace(/^data:image\/[a-z]+;base64,/, '')
        const newFileName = `${Guid.newGuid()}.${fileEx}`
        await saveFile(baseData, uploadPath, newFileName)
        const imageUrl = `${userId}/dl1/${newFileName}`
        arrCorrectAnswer.push({
          ...item,
          data: imageUrl,
        })
        list_image_item_position[i].name = newFileName
        list_image_item_position[i].isExample = item.isExample
      }
      //sort
      arrCorrectAnswer.sort((m, n) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      list_image_item_position.sort((m: any, n: any) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      //sort
      const strCorrectAnswer = arrCorrectAnswer
        .map((m) => `${m.isExample ? 'ex|' : ''}${m.data}*${m.minX}:${m.minY}:${m.maxX}:${m.maxY}*${m.name}`)
        .join('#')
      data.correct_answers = strCorrectAnswer
      delete data.list_answer_item
      // data postion;
      const filePath = `${uploadPath}/${Guid.newGuid()}`
      try {
        await fs.writeFileSync(filePath, JSON.stringify(list_image_item_position))
        data.answers = filePath.replace(uploadDir, 'upload/')
      } catch (ex) {
        console.log('ẽ-----', ex)
      }
      delete data.list_image_item_position
    }
    return data
  } catch (ex1) {
    console.log('ex---handleDL1Data', ex1)
    throw ex1;
  }
}
const handleDL2Data = async (data: any, uploadPath: string, userId: string) => {
  type answerType = {
    data: string
    key: string
    minX: number
    minY: number
    maxX: number
    maxY: number
    name?: string
    isExample?: boolean
    correctUrl?: string
  }
  try {
    if (!data) return data
    uploadPath = `${uploadPath}/dl2`
    if (data.image_question_base64 && data.image_question_export_base64) {
      //image question
      const fileExQ = data.image_question_base64.split(';')[0].split('/')[1]
      const baseDataQ = data.image_question_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameQ = `${Guid.newGuid()}.${fileExQ}`
      await saveFile(baseDataQ, uploadPath, newFileNameQ)
      const imageQuestion = `${userId}/dl2/${newFileNameQ}`

      //image export word
      const fileExW = data.image_question_export_base64.split(';')[0].split('/')[1]
      const baseDataW = data.image_question_export_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameW = `${Guid.newGuid()}.${fileExW}`
      await saveFile(baseDataW, uploadPath, newFileNameW)
      const imageExport = `${userId}/dl2/${newFileNameW}`
      data.image = `${imageQuestion}*${imageExport}`
      delete data.image_question_base64
      delete data.image_question_export_base64
    }else{
      throw Error("Có lỗi xảy ra, không tìm thấy hình ảnh được tải lên, vui lòng thử lại")
    }
    delete data.passData

    if (data.list_answer_item && data.list_image_item_position) {
      const list_answer_item = JSON.parse(data.list_answer_item)
      const list_image_item_position = JSON.parse(data.list_image_item_position)
      const arrCorrectAnswer: answerType[] = []
      for (let i = 0; i < list_answer_item.length; i++) {
        const item = list_answer_item[i]
        const fileEx = item.data.split(';')[0].split('/')[1]
        const baseData = item.data.replace(/^data:image\/[a-z]+;base64,/, '')
        const newFileName = `${Guid.newGuid()}.${fileEx}`
        await saveFile(baseData, uploadPath, newFileName)
        const imageUrl = `${userId}/dl2/${newFileName}`
        let imageCorrectUrl = item.correctUrl
        const fileCorrect = data.image_files?.find((m: any) => m.originalFilename.startsWith(item.key))
        if (fileCorrect) {
          if (fileCorrect.size > 0) {
            const newFileName = Guid.newGuid() + getFileExtension(fileCorrect.originalFilename)
            await saveFileBuffer(fileCorrect.filepath, uploadPath, newFileName)
            imageCorrectUrl = `${userId}/dl2/${newFileName}`
          }
        }

        arrCorrectAnswer.push({
          ...item,
          data: imageUrl,
          correctUrl: imageCorrectUrl,
        })
        list_image_item_position[i].name = newFileName
        list_image_item_position[i].isExample = item.isExample
      }
      //sort
      arrCorrectAnswer.sort((m, n) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      list_image_item_position.sort((m: any, n: any) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      //sort
      const strCorrectAnswer = arrCorrectAnswer
        .map((m) => `${m.isExample ? 'ex|' : ''}${m.data}*${m.minX}:${m.minY}:${m.maxX}:${m.maxY}*${m.correctUrl}`)
        .join('#')
      data.correct_answers = strCorrectAnswer
      // console.log("strCorrectAnswer-----",strCorrectAnswer)
      delete data.list_answer_item
      data.image_files = []
      // data postion;
      const filePath = `${uploadPath}/${Guid.newGuid()}`
      try {
        await fs.writeFileSync(filePath, JSON.stringify(list_image_item_position))
        data.answers = filePath.replace(uploadDir, 'upload/')
      } catch (ex) {
        console.log('ẽ-----', ex)
      }
      delete data.list_image_item_position
    }
    return data
  } catch (ex1) {
    console.log('ex---handleDL2Data', ex1)
    throw ex1;
  }
}
const handleSO1Data = async (data: any, uploadPath: string, userId: string) => {
  type answerTypeSO = {
    data: string
    key: string
    minX: number
    minY: number
    maxX: number
    maxY: number
    name?: string
    isExample?: boolean
    isSelected?: boolean
  }
  try {
    if (!data) return data
    uploadPath = `${uploadPath}/so1`
    if (data.image_question_base64 && data.image_question_export_base64) {
      //image question
      const fileExQ = data.image_question_base64.split(';')[0].split('/')[1]
      const baseDataQ = data.image_question_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameQ = `${Guid.newGuid()}.${fileExQ}`
      await saveFile(baseDataQ, uploadPath, newFileNameQ)
      const imageQuestion = `${userId}/so1/${newFileNameQ}`

      //image export word
      const fileExW = data.image_question_export_base64.split(';')[0].split('/')[1]
      const baseDataW = data.image_question_export_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameW = `${Guid.newGuid()}.${fileExW}`
      await saveFile(baseDataW, uploadPath, newFileNameW)
      const imageExport = `${userId}/so1/${newFileNameW}`
      data.image = `${imageQuestion}*${imageExport}`
      delete data.image_question_base64
      delete data.image_question_export_base64
    }else{
      throw Error("Có lỗi xảy ra, không tìm thấy hình ảnh được tải lên, vui lòng thử lại")
    }
    delete data.passData

    if (data.list_answer_item && data.list_image_item_position) {
      const list_answer_item = JSON.parse(data.list_answer_item)
      const list_image_item_position = JSON.parse(data.list_image_item_position)
      const arrCorrectAnswer: answerTypeSO[] = []
      for (let i = 0; i < list_answer_item.length; i++) {
        const item = list_answer_item[i]
        const fileEx = item.data.split(';')[0].split('/')[1]
        const baseData = item.data.replace(/^data:image\/[a-z]+;base64,/, '')
        const newFileName = `${Guid.newGuid()}.${fileEx}`
        await saveFile(baseData, uploadPath, newFileName)
        const imageUrl = `${userId}/so1/${newFileName}`
        arrCorrectAnswer.push({
          ...item,
          data: imageUrl,
        })
        list_image_item_position[i].name = newFileName
        list_image_item_position[i].isExample = item.isExample
      }
      //sort
      arrCorrectAnswer.sort((m, n) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      list_image_item_position.sort((m: any, n: any) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      //sort
      const strCorrectAnswer = arrCorrectAnswer
        .map(
          (m) =>
            `${m.isExample ? 'ex|' : ''}${m.data}*${m.minX}:${m.minY}:${m.maxX}:${m.maxY}*${
              m.isSelected ? 'True' : 'False'
            }`,
        )
        .join('#')
      data.correct_answers = strCorrectAnswer
      delete data.list_answer_item
      // data postion;
      const filePath = `${uploadPath}/${Guid.newGuid()}`
      try {
        await fs.writeFileSync(filePath, JSON.stringify(list_image_item_position))
        data.answers = filePath.replace(uploadDir, 'upload/')
      } catch (ex) {
        console.log('ẽ-----', ex)
      }
      delete data.list_image_item_position
    }
    return data
  } catch (ex1) {
    console.log('ex---handleSO1Data', ex1)
    throw ex1;
  }
}
const handleFC1Data = async (data: any, uploadPath: string, userId: string) => {
  type answerType = {
    data: string
    key: string
    minX: number
    minY: number
    maxX: number
    maxY: number
    color?: string
    isExample?: boolean
  }
  try {
    if (!data) return data
    uploadPath = `${uploadPath}/fc1`
    if (data.image_question_base64 && data.image_question_export_base64) {
      //image question
      const fileExQ = data.image_question_base64.split(';')[0].split('/')[1]
      const baseDataQ = data.image_question_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameQ = `${Guid.newGuid()}.${fileExQ}`
      await saveFile(baseDataQ, uploadPath, newFileNameQ)
      const imageQuestion = `${userId}/fc1/${newFileNameQ}`

      //image export word
      const fileExW = data.image_question_export_base64.split(';')[0].split('/')[1]
      const baseDataW = data.image_question_export_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameW = `${Guid.newGuid()}.${fileExW}`
      await saveFile(baseDataW, uploadPath, newFileNameW)
      const imageExport = `${userId}/fc1/${newFileNameW}`
      data.image = `${imageQuestion}*${imageExport}`
      delete data.image_question_base64
      delete data.image_question_export_base64
    }else{
      throw Error("Có lỗi xảy ra, không tìm thấy hình ảnh được tải lên, vui lòng thử lại")
    }
    delete data.passData
    let temp1 = ''
    let temp2 = ''
    if (data.list_answer_item && data.list_image_item_position) {
      const list_answer_item = JSON.parse(data.list_answer_item)
      const list_image_item_position = JSON.parse(data.list_image_item_position)
      const arrCorrectAnswer: answerType[] = []
      for (let i = 0; i < list_answer_item.length; i++) {
        const item = list_answer_item[i]
        const fileEx = item.data.split(';')[0].split('/')[1]
        const baseData = item.data.replace(/^data:image\/[a-z]+;base64,/, '')
        const newFileName = `${Guid.newGuid()}.${fileEx}`
        await saveFile(baseData, uploadPath, newFileName)
        const imageUrl = `${userId}/fc1/${newFileName}`
        arrCorrectAnswer.push({
          ...item,
          data: imageUrl,
        })
        list_image_item_position[i].isExample = item.isExample
        list_image_item_position[i].color = item.color
      }
      //sort
      arrCorrectAnswer.sort((m, n) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      list_image_item_position.sort((m: any, n: any) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      //sort
      const strCorrectAnswer = arrCorrectAnswer
        .map((m) => `${m.isExample ? 'ex|' : ''}${m.data}*${m.minX}:${m.minY}:${m.maxX}:${m.maxY}*${m.color}`)
        .join('#')
      data.correct_answers = strCorrectAnswer
      delete data.list_answer_item
      // data postion;
      const filePath = `${uploadPath}/${Guid.newGuid()}`
      try {
        await fs.writeFileSync(filePath, JSON.stringify(list_image_item_position))
        temp1 = filePath.replace(uploadDir, 'upload/')
      } catch (ex) {
        console.log('ẽ-----', ex)
      }
      delete data.list_image_item_position
    }

    if (data.list_image_item) {
      const tempArr: any[] = []
      const list_image_item = JSON.parse(data.list_image_item)
      for (let i = 0; i < list_image_item.length; i++) {
        const item = list_image_item[i]
        const file = item.split(';')[0].split('/')[1]
        const baseData = item.replace(/^data:image\/[a-z]+;base64,/, '')
        const newFileName = `${Guid.newGuid()}.${file}`
        await saveFile(baseData, uploadPath, newFileName)
        const imageUrl = `${userId}/fc1/${newFileName}`
        tempArr.push(imageUrl)
      }
      const filePath = `${uploadPath}/${Guid.newGuid()}_image_item`
      try {
        await fs.writeFileSync(filePath, JSON.stringify(tempArr))
        temp2 = filePath.replace(uploadDir, 'upload/')
      } catch (ex) {
        console.log('ex-----', ex)
      }
      delete data.list_image_item
    }
    data.answers = temp1 + '*' + temp2
    return data
  } catch (ex1) {
    console.log('ex---handleFC1Data', ex1)
    throw ex1;
  }
}

const handleFC2Data = async (data: any, uploadPath: string, userId: string) => {
  type answerType = {
    data: string
    key: string
    minX: number
    minY: number
    maxX: number
    maxY: number
    color?: string
    isExample?: boolean
  }
  try {
    if (!data) return data
    uploadPath = `${uploadPath}/fc2`
    if (data.image_question_base64 && data.image_question_export_base64) {
      //image question
      const fileExQ = data.image_question_base64.split(';')[0].split('/')[1]
      const baseDataQ = data.image_question_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameQ = `${Guid.newGuid()}.${fileExQ}`
      await saveFile(baseDataQ, uploadPath, newFileNameQ)
      const imageQuestion = `${userId}/fc2/${newFileNameQ}`

      //image export word
      const fileExW = data.image_question_export_base64.split(';')[0].split('/')[1]
      const baseDataW = data.image_question_export_base64.replace(/^data:image\/[a-z]+;base64,/, '')
      const newFileNameW = `${Guid.newGuid()}.${fileExW}`
      await saveFile(baseDataW, uploadPath, newFileNameW)
      const imageExport = `${userId}/fc2/${newFileNameW}`
      data.image = `${imageQuestion}*${imageExport}`
      delete data.image_question_base64
      delete data.image_question_export_base64
    }else{
      throw Error("Có lỗi xảy ra, không tìm thấy hình ảnh được tải lên, vui lòng thử lại")
    }
    delete data.passData
    let temp1 = ''
    let temp2 = ''
    if (data.list_answer_item && data.list_image_item_position) {
      const list_answer_item = JSON.parse(data.list_answer_item)
      const list_image_item_position = JSON.parse(data.list_image_item_position)
      const arrCorrectAnswer: answerType[] = []
      for (let i = 0; i < list_answer_item.length; i++) {
        const item = list_answer_item[i]
        const fileEx = item.data.split(';')[0].split('/')[1]
        const baseData = item.data.replace(/^data:image\/[a-z]+;base64,/, '')
        const newFileName = `${Guid.newGuid()}.${fileEx}`
        await saveFile(baseData, uploadPath, newFileName)
        const imageUrl = `${userId}/fc2/${newFileName}`
        arrCorrectAnswer.push({
          ...item,
          data: imageUrl,
        })
        list_image_item_position[i].isExample = item.isExample
        list_image_item_position[i].color = item.color
      }
      //sort
      arrCorrectAnswer.sort((m, n) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      list_image_item_position.sort((m: any, n: any) => {
        if (m.isExample) return -1
        if (n.isExample) return 1
      })
      //sort
      const strCorrectAnswer = arrCorrectAnswer
        .map((m) => `${m.isExample ? 'ex|' : ''}${m.data}*${m.minX}:${m.minY}:${m.maxX}:${m.maxY}*${m.color}`)
        .join('#')
      data.correct_answers = strCorrectAnswer
      delete data.list_answer_item
      // data postion;
      const filePath = `${uploadPath}/${Guid.newGuid()}`
      try {
        await fs.writeFileSync(filePath, JSON.stringify(list_image_item_position))
        temp1 = filePath.replace(uploadDir, 'upload/')
      } catch (ex) {
        console.log('ẽ-----', ex)
      }
      delete data.list_image_item_position
    }

    if (data.list_image_item) {
      const tempArr: any[] = []
      const list_image_item = JSON.parse(data.list_image_item)
      for (let i = 0; i < list_image_item.length; i++) {
        const item = list_image_item[i]
        const file = item.split(';')[0].split('/')[1]
        const baseData = item.replace(/^data:image\/[a-z]+;base64,/, '')
        const newFileName = `${Guid.newGuid()}.${file}`
        await saveFile(baseData, uploadPath, newFileName)
        const imageUrl = `${userId}/fc2/${newFileName}`
        tempArr.push(imageUrl)
      }
      const filePath = `${uploadPath}/${Guid.newGuid()}_image_item`
      try {
        await fs.writeFileSync(filePath, JSON.stringify(tempArr))
        temp2 = filePath.replace(uploadDir, 'upload/')
      } catch (ex) {
        console.log('ex-----', ex)
        throw ex
      }
      delete data.list_image_item
    }
    data.answers = temp1 + '*' + temp2
    return data
  }catch (ex1) {
    console.log('ex---handleFC2Data', ex1)
    throw ex1
  }
}
const saveFile = async (fileData: string, pathDes: string, fileName: string) => {
  let file = null
  try {
    file = await fileUtils.exist(pathDes)
  } catch {}
  if (!file) {
    fileUtils.createDir(pathDes, { recursive: true })
  }
  return await fileUtils.writeFileBase64(`${pathDes}/${fileName}`, fileData)
}
const saveFileBuffer = async (filePath: string, pathDes: string, fileName: string) => {
  let file = null
  try {
    file = await fileUtils.exist(pathDes)
  } catch {}
  if (!file) {
    fileUtils.createDir(pathDes, { recursive: true })
  }
  const fileData = await fileUtils.readFile(filePath)
  return await fileUtils.writeFile(`${pathDes}/${fileName}`, fileData)
}
const handleQuestionBackend = { handleDL1Data, handleDL2Data, handleSO1Data, handleFC1Data, handleFC2Data }
export default handleQuestionBackend
