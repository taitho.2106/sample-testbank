import { NextApiRequest, NextApiResponse } from 'next';
import { getSession } from 'next-auth/client';

import { uploadDir } from '@/constants/index';
import { IncomingForm } from 'formidable';
import { USER_ROLES } from "interfaces/constants";
import { PRACTICE_TEST_ACTIVITY } from 'interfaces/struct';
import { QuestionDataType } from 'interfaces/types';
import { escapeSearchString, query } from 'lib/db';
import { userInRight } from 'utils';
import Guid from 'utils/guid';
import { getFileExtension } from 'utils/string';
import fileUtils from '../../../utils/file';
import handleQuestionBackend from './handleQuestionBackend';

export const config = {
  api: {
    bodyParser: false,
  },
}

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
  const session = await getSession({ req })
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session)
  if (!userInRight([USER_ROLES.Operator, USER_ROLES.Teacher], session)) {
    return res.status(403).end('Forbidden')
  }
  const user: any = session.user
  const isAdmin = user?.is_admin == 1
  const uploadPath = `${uploadDir}/${user.id}`

  switch (req.method) {
    case 'GET':
      return getQuestions()
    case 'POST':
      return createQuestion()
    case 'PUT':
      return updateQuestion()
    default:
      return res.status(405).end(`Method ${req.method} Not Allowed`)
  }

  async function getQuestions() {
    const { page, limit, p, s, g, sk, l, qt, q, m, cd } = req.query
    try {
      let orderByStr = 'ORDER BY q.created_date desc'
      if (isTeacher && m === "0") {
        orderByStr = ' ORDER BY g.priority, q.created_date desc'
      }
      const params: any[] = []
      let queryStr = `SELECT q.*, u.user_name, g.priority FROM question q
                      LEFT JOIN user u ON q.created_by = u.id
                      LEFT JOIN grade g ON g.id = q.grade
                      WHERE q.deleted = 0 ${isAdmin?"":"AND (privacy is null OR privacy !=1)"} `
      let queryCount = `SELECT COUNT(*) as total FROM question q
      LEFT JOIN user u ON q.created_by = u.id
      WHERE q.deleted = 0 ${isAdmin?"":"AND (privacy is null OR privacy !=1)"} `
      if (p) {
        queryStr += ' AND q.publisher IN (?)'
        queryCount += ' AND q.publisher IN (?)'
        params.push((p as string).split(','))
      }
      if (s) {
        queryStr += ' AND q.series IN (?)'
        queryCount += ' AND q.series IN (?)'
        params.push((s as string).split(','))
      }
      if (g) {
        queryStr += ' AND q.grade IN (?)'
        queryCount += ' AND q.grade IN (?)'
        params.push((g as string).split(','))
      }
      if (sk) {
        queryStr += ' AND q.skills IN (?)'
        queryCount += ' AND q.skills IN (?)'
        params.push((sk as string).split(','))
      }
      if (l) {
        queryStr += ' AND q.level IN (?)'
        queryCount += ' AND q.level IN (?)'
        params.push((l as string).split(','))
      }
      if (qt) {
        queryStr += ' AND q.question_type IN (?)'
        queryCount += ' AND q.question_type IN (?)'
        params.push((qt as string).split(','))
      }
      if (q) {
        const qFormat = `%${escapeSearchString(q.toString())}%`
        const querySearch = ` AND (q.question_text LIKE ? OR q.parent_question_description LIKE ? OR q.question_description LIKE ? OR q.id = ? OR q.name LIKE ?)`;
        queryStr += querySearch;
        queryCount += querySearch;
      
          
          if(q.includes('\\')){
            const qArray = q.toString().split('')
            for (let i = 0; i < qArray.length; i++) {
              if(qArray[i] === '\\'){
                qArray[i] = `\\${qArray[i]}`
              }
            }
            const newFormat = `%${qArray.join('')}%`
            params.push(newFormat)
            params.push(newFormat)
            params.push(newFormat)
            params.push(escapeSearchString(qArray.join('')))
            params.push(newFormat)
          }else{
            params.push(qFormat)
            params.push(qFormat)
            params.push(qFormat)
            params.push(escapeSearchString(q.toString()))
            params.push(qFormat)
          }
        }
      if (cd) {
        const cdDate = new Date(Number(cd))
        const cdFormat = `${cdDate.getFullYear()}-${
          cdDate.getMonth() + 1
        }-${cdDate.getDate()}`
        queryStr += ` AND DATE(q.created_date) = ?`
        queryCount += ` AND DATE(q.created_date) = ?`
        params.push(cdFormat)
      }
      const scopeId = m === '1' ? 1 : 0
      queryStr += ` AND q.scope = ${scopeId}`
      queryCount += ` AND q.scope = ${scopeId}`
      if (scopeId === 1) {
        if (!user.is_admin) {
          queryStr += ' AND q.created_by = ?'
          queryCount += ' AND q.created_by = ?'
          params.push(user.id)
        }
      }
      const results: any[] = await query<any[]>(
        `${queryStr} ${orderByStr}
         LIMIT ${limit} OFFSET ${
          parseInt(page.toString()) * parseInt(limit.toString())
        }`,
        params,
      )
      const totals = await query<any[]>(queryCount, params)
      return res.json({
        data: results,
        totalRecords: totals[0].total,
      })
    } catch (e: any) {
      res.status(500).json({ message: e.message })
    }
  }

  async function createQuestion() {
    try {
      let item: any = await handleRequest(req)
     
      if (!item.question_type) {
        return res.status(400).json({ message: '`question type` are required' })
      }
      if (item.isExam) {
        const answerArr = item.answers.split('#')
        const examArr = item.isExam.split('#')
        answerArr.map((item: string, index: number) => {
          if (examArr[index] === 'false') {
            answerArr[index] = `*${item}`
          }
        })
        item.answers = answerArr.join('#')
      }
      if(item?.question_type === 'DL1' || item?.question_type === 'DL3'){
        item = await handleQuestionBackend.handleDL1Data(item,uploadPath,user.id)
      }
      if(item?.question_type === 'DL2'){
        item = await handleQuestionBackend.handleDL2Data(item,uploadPath,user.id)
      }
      if (item.image_file) {
        const newFileName =
          Guid.newGuid() + getFileExtension(item.image_file.originalFilename)
        await saveFile(item.image_file.filepath, uploadPath, newFileName)
        item.image = `${user.id}/${newFileName}`
      }
      if (item.audio_file) {
        const newFileName =
          Guid.newGuid() + getFileExtension(item.audio_file.originalFilename)
        await saveFile(item.audio_file.filepath, uploadPath, newFileName)
        item.audio = `${user.id}/${newFileName}`
      }
      if (item.image_files) {
          let i = 0
          const listImage: string[] = item.image?.split('#') ?? []
          for (const file of item.image_files) {
            if (file.size > 0) {
              const newFileName =
                Guid.newGuid() + getFileExtension(file.originalFilename)
              await saveFile(file.filepath, uploadPath, newFileName)
              listImage[i] = `${user.id}/${newFileName}`
            }
            i++
          }
          item.image = listImage.join('#')
      }
      if( item?.question_type === 'MG6' ){
        const answerGroup: string[] = item.answers?.split('#') ?? []
        const correctArray: string[] = item.correct_answers?.split('#') ?? []
        const pathLeftList: string[] = answerGroup[0].split('*') ?? []
        const pathRightList: string[] = answerGroup[1].split('*') ?? []
        const rightStringURL: string[] = answerGroup[1].split('*') ?? []
        const exampleList: string[] = answerGroup[2]?.split('*') ?? []
        const totalLength = rightStringURL.length

        for( let i = 0; i < totalLength; i++ ){
          const fileLeft = item[`mg6_images_left_${i}`]
          if( fileLeft?.size > 0 ){
            const nameLeft = Guid.newGuid() + getFileExtension(fileLeft.originalFilename)
            await saveFile(fileLeft.filepath, uploadPath, nameLeft)
            pathLeftList[i] = `${user.id}/${nameLeft}`
          }
          const fileRight = item[`mg6_images_right_${i}`]
          if(fileRight?.size > 0){
            const nameRight = Guid.newGuid() + getFileExtension(fileRight.originalFilename)
            await saveFile(fileRight.filepath, uploadPath, nameRight)
            pathRightList[i] = `${user.id}/${nameRight}`

            const correctIndex = correctArray.indexOf(rightStringURL[i])
            if( correctIndex > -1 ){
              correctArray[correctIndex] = pathRightList[i]
            }
          }
        }
        
        item.answers =`${pathLeftList.join("*")}#${pathRightList.join('*')}${exampleList.length>0?`#${exampleList.join('*')}`:""}`

        item.correct_answers = correctArray.join('#')

      }
      if(item?.question_type === 'MC7'){
        const listAnswers = item?.answers.split('#') ?? []
          for(let i = 0; i<listAnswers.length; i++){
            const answerItem =listAnswers[i].split('*')
            for(let j = 0; j< answerItem.length;j++){
              const file = item[`mc7_image_files_${i+1}_${j+1}`]
              if(file?.size > 0){
                const newFileName =
                  Guid.newGuid() + getFileExtension(file.originalFilename)
                  await saveFile(file.filepath, uploadPath, newFileName)
                  answerItem[j] = `${user.id}/${newFileName}`
              }
            }
            listAnswers[i] =answerItem.join('*')
          }
          item.answers = listAnswers.join('#')
      }

      if(item?.question_type === 'MC8'){
        const listAnswers = item?.answers.split('#') ?? []
        for(let i = 0; i<listAnswers.length; i++){
          const answerItem =listAnswers[i].split('*')
          for(let j = 0; j< answerItem.length;j++){
            const file = item[`mc8_image_files_${i+1}_${j+1}`]
            if(file?.size > 0){
              const newFileName =
                Guid.newGuid() + getFileExtension(file.originalFilename)
                await saveFile(file.filepath, uploadPath, newFileName)
                answerItem[j] = `${user.id}/${newFileName}`
            }
          }
          listAnswers[i] =answerItem.join('*')
        }
        item.answers = listAnswers.join('#')
      }

      if(item?.question_type === 'MC11'){
        const listAnswers = item?.answers.split('#') ?? []
        for(let i = 0; i<listAnswers.length; i++){
          const answerItem = listAnswers[i].split('*')
          for(let j = 0; j< answerItem.length;j++){
            const file = item[`mc11_image_files_${i+1}_${j+1}`]
            if(file?.size > 0){
              const isExample = answerItem[j].startsWith('ex') ? 'ex|' : ''
              const newFileName =
                Guid.newGuid() + getFileExtension(file.originalFilename)
                await saveFile(file.filepath, uploadPath, newFileName)
                answerItem[j] = `${isExample}${user.id}/${newFileName}`
            }
          }
          listAnswers[i] = answerItem.join('*')
        }
        item.answers = listAnswers.join('#')
      }

      if (item?.image_file_group0) {
        const newFileName =
          Guid.newGuid() + getFileExtension(item.image_file_group0.originalFilename)
        await saveFile(item.image_file_group0.filepath, uploadPath, newFileName)
        item.image_group0 = `${user.id}/${newFileName}`
      }
  
      if (item?.image_group1) {
          const listImage: string[] = item.image_group1?.split('#')
          for (let i = 0; i< listImage.length ; i++) {
            const itemImage:string[] = listImage[i]?.split('|')
            for(let j = 0; j < itemImage.length ; j++){
              const file = item[`image_file_group1_${i+1}_${j+1}`]
              if (file && file.size > 0) {
                const newFileName =
                  Guid.newGuid() + getFileExtension(file.originalFilename)
                await saveFile(file.filepath, uploadPath, newFileName)
                itemImage[j] = `${user.id}/${newFileName}`
              }
            }
            listImage[i] = itemImage.join('|')
          }
          item.image_group1 = listImage.join('#')
      }
      if (item?.image_file_group2) {
        let i = 0
        const listImage: string[] =item?.image_group2?.length > 0 ?  item.image_group2?.split('#') : []
        for (const file of item.image_file_group2) {
          if (file && file.size > 0) {
            const newFileName =
              Guid.newGuid() + getFileExtension(file.originalFilename)

            await saveFile(file.filepath, uploadPath, newFileName)
            listImage[i] = `${user.id}/${newFileName}`
          }
          i++
        }
        item.image_group2 = listImage.join('#')
      }

      if(item?.question_type === 'FB8'){
        let imageFB8 = ''
        imageFB8 = item?.image_group2?.length > 0 ? `${item?.image_group0}*${item?.image_group1}#${item?.image_group2}` : `${item?.image_group0}*${item?.image_group1}` 
        item.image = imageFB8
      }
      if(item?.question_type === 'SO1'){
        item = await handleQuestionBackend.handleSO1Data(item,uploadPath,user.id)
      }
      if(item?.question_type === 'FC1'){
        item = await handleQuestionBackend.handleFC1Data(item,uploadPath,user.id)
      }
      if(item?.question_type === 'FC2'){
        item = await handleQuestionBackend.handleFC2Data(item,uploadPath,user.id)
      }
      const isSystem = userInRight([USER_ROLES.Operator], session)
      const result = await query<any>(
        `INSERT INTO question (publisher, test_type, series, grade, cerf, format, types, skills, question_type, 
          level, \`group\`, parent_question_description, parent_question_text, question_description, question_text, 
          image, video, audio, answers, correct_answers, points, created_date, audio_script, created_by, scope, total_question, privacy, name)
                      VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
        [
          item.publisher ?? 'NA',
          item.test_type,
          item.series ?? 1,
          item.grade,
          item.cerf ?? 'NA',
          item.format ?? 'NA',
          item.types ?? 'NA',
          item.skills,
          item.question_type,
          item.level ?? 'NA',
          item.group,
          item.parent_question_description,
          item.parent_question_text,
          item.question_description,
          item.question_text,
          item.image,
          item.video,
          item.audio,
          item.answers,
          item.correct_answers,
          item.points ?? 0,
          new Date(),
          item.audio_script,
          user.id,
          isSystem ? 0 : 1,
          calcuQuestion(item),
          item.privacy||0,
          item.name,
        ],
      )
      if (result.affectedRows !== 1) {
        throw Error('insert failed')
      }
      return res.json({ id: result.insertId })
    } catch (e: any) {
      console.log("create question0----", e)
      res.status(500).json({ message: e.message })
    }
  }

  async function updateQuestion() {
    try {
      let item: any = await handleRequest(req)
      if (!item.question_type) {
        return res.status(400).json({ message: '`question type` are required' })
      }
      if (item.image_file) {
        const newFileName =
          Guid.newGuid() + getFileExtension(item.image_file.originalFilename)
        await saveFile(item.image_file.filepath, uploadPath, newFileName)
        item.image = `${user.id}/${newFileName}`
      }
      if(item?.question_type === 'DL1' || item?.question_type === 'DL3'){
        item = await handleQuestionBackend.handleDL1Data(item,uploadPath,user.id)
      }
      if(item?.question_type === 'DL2'){
        item = await handleQuestionBackend.handleDL2Data(item,uploadPath,user.id)
      }
      if(item?.question_type === 'FC1'){
        item = await handleQuestionBackend.handleFC1Data(item,uploadPath,user.id)
      }
      if(item?.question_type === 'FC2'){
        item = await handleQuestionBackend.handleFC2Data(item,uploadPath,user.id)
      }
      if (item.isExam) {
        const answerArr = item.answers.split('#')
        const examArr = item.isExam.split('#')
        answerArr.map((item: string, index: number) => {
          if (examArr[index] === 'false') {
            answerArr[index] = `*${item}`
          }
        })
        item.answers = answerArr.join('#')
      }
      if (item.audio_file) {
        const newFileName =
          Guid.newGuid() + getFileExtension(item.audio_file.originalFilename)
        await saveFile(item.audio_file.filepath, uploadPath, newFileName)
        item.audio = `${user.id}/${newFileName}`
      }
      if (item.image_files) {
        let i = 0
        const listImage: string[] = item.image.split('#') ?? []
        for (const file of item.image_files) {
          if (file.size > 0) {
            const newFileName =
              Guid.newGuid() + getFileExtension(file.originalFilename)
            await saveFile(file.filepath, uploadPath, newFileName)
            listImage[i] = `${user.id}/${newFileName}`
          }
          i++
        }
        item.image = listImage.join('#')
      }
      if( item?.question_type === 'MG6' ){
        const answerGroup: string[] = item.answers?.split('#') ?? []
        const correctArray: string[] = item.correct_answers?.split('#') ?? []
        const rightStringURL: string[] = answerGroup[1]?.split('*') ?? []
        const pathLeftList: string[] = answerGroup[0]?.split('*') ?? []
        const pathRightList: string[] = answerGroup[1]?.split('*') ?? []
        const exampleList: string[] = answerGroup[2]?.split('*') ?? []
        const totalLength = rightStringURL.length

        for( let i = 0; i < totalLength; i++ ){
          const fileLeft = item[`mg6_images_left_${i}`]
          if( fileLeft?.size > 0 ){
            const nameLeft = Guid.newGuid() + getFileExtension(fileLeft.originalFilename)
            await saveFile(fileLeft.filepath, uploadPath, nameLeft)
            pathLeftList[i] = `${user.id}/${nameLeft}`
          }

          const fileRight = item[`mg6_images_right_${i}`]
          if(fileRight?.size > 0){
            const nameRight = Guid.newGuid() + getFileExtension(fileRight.originalFilename)
            await saveFile(fileRight.filepath, uploadPath, nameRight)
            pathRightList[i] = `${user.id}/${nameRight}`

            const indexCorrect = correctArray.indexOf(rightStringURL[i])
            if( indexCorrect > -1 ){
              correctArray[indexCorrect] = pathRightList[i]
            }
          }
        }

        item.answers =`${pathLeftList.join("*")}#${pathRightList.join('*')}${exampleList.length>0?`#${exampleList.join('*')}`:""}`

        item.correct_answers = correctArray.join('#')

      }
      if(item?.question_type === 'MC7'){
        const listAnswers = item?.answers.split('#') ?? []
          for(let i = 0; i<listAnswers.length; i++){
            const answerItem =listAnswers[i].split('*')
            for(let j = 0; j< answerItem.length;j++){
              const file = item[`mc7_image_files_${i+1}_${j+1}`]
              if(file?.size > 0){
                const newFileName =
                  Guid.newGuid() + getFileExtension(file.originalFilename)
                  await saveFile(file.filepath, uploadPath, newFileName)
                  answerItem[j] = `${user.id}/${newFileName}`
              }
            }
            listAnswers[i] =answerItem.join('*')
          }
          item.answers = listAnswers.join('#')
      }

      if(item?.question_type === 'MC8'){
        const listAnswers = item?.answers.split('#') ?? []
        for(let i = 0; i<listAnswers.length; i++){
          const answerItem =listAnswers[i].split('*')
          for(let j = 0; j< answerItem.length;j++){
            const file = item[`mc8_image_files_${i+1}_${j+1}`]
            if(file?.size > 0){
              const newFileName =
                Guid.newGuid() + getFileExtension(file.originalFilename)
                await saveFile(file.filepath, uploadPath, newFileName)
                answerItem[j] = `${user.id}/${newFileName}`
            }
          }
          listAnswers[i] =answerItem.join('*')
        }
        item.answers = listAnswers.join('#')
      }

      if(item?.question_type === 'MC11'){
        const listAnswers = item?.answers.split('#') ?? []
        for(let i = 0; i<listAnswers.length; i++){
          const answerItem = listAnswers[i].split('*')
          for(let j = 0; j< answerItem.length;j++){
            const file = item[`mc11_image_files_${i+1}_${j+1}`]
            if(file?.size > 0){
              const isExample = answerItem[j].startsWith('ex') ? 'ex|' : ''
              const newFileName =
                Guid.newGuid() + getFileExtension(file.originalFilename)
                await saveFile(file.filepath, uploadPath, newFileName)
                answerItem[j] = `${isExample}${user.id}/${newFileName}`
            }
          }
          listAnswers[i] = answerItem.join('*')
        }
        item.answers = listAnswers.join('#')
      }
      
      if (item?.image_file_group0) {
        const newFileName =
          Guid.newGuid() + getFileExtension(item.image_file_group0.originalFilename)
        await saveFile(item.image_file_group0.filepath, uploadPath, newFileName)
        item.image_group0 = `${user.id}/${newFileName}`
      }
  
      if (item?.image_group1) {
          const listImage: string[] = item.image_group1?.split('#')
          for (let i = 0; i< listImage.length ; i++) {
            const itemImage:string[] = listImage[i]?.split('|')
            for(let j = 0; j < itemImage.length ; j++){
              const file = item[`image_file_group1_${i+1}_${j+1}`]
              if (file && file.size > 0) {
                const newFileName =
                  Guid.newGuid() + getFileExtension(file.originalFilename)
                await saveFile(file.filepath, uploadPath, newFileName)
                itemImage[j] = `${user.id}/${newFileName}`
              }
            }
            listImage[i] = itemImage.join('|')
          }
          item.image_group1 = listImage.join('#')
      }
      if (item?.image_file_group2) {
        let i = 0
        const listImage: string[] =item?.image_group2?.length > 0 ?  item.image_group2?.split('#') : []
        for (const file of item.image_file_group2) {
          if (file && file.size > 0) {
            const newFileName =
              Guid.newGuid() + getFileExtension(file.originalFilename)

            await saveFile(file.filepath, uploadPath, newFileName)
            listImage[i] = `${user.id}/${newFileName}`
          }
          i++
        }
        item.image_group2 = listImage.join('#')
      }
    
      if(item?.question_type === 'FB8'){
        let imageFB8 = ''
        imageFB8 = item?.image_group2?.length > 0 ? `${item?.image_group0}*${item?.image_group1}#${item?.image_group2}` : `${item?.image_group0}*${item?.image_group1}` 
        item.image = imageFB8
      }
      if(item?.question_type === 'SO1'){
        item = await handleQuestionBackend.handleSO1Data(item,uploadPath,user.id)
      }
      const result = await query<any>(
        `UPDATE question SET publisher = ?, test_type = ?, series = ?, grade = ?, cerf = ?,
         format = ?, types = ?, skills = ?, question_type = ?, level = ?, \`group\` = ?, 
         question_description = ?, question_text = ?, image = ?, video = ?, audio = ?, 
         answers = ?, correct_answers = ?, points = ?, parent_question_description = ?, 
         parent_question_text = ?, audio_script = ?, total_question = ?, privacy = ?, name = ? WHERE id = ? ${
           user.is_admin ? '' : ` AND created_by = '${user.id}'`
         }`,
        [
          item.publisher ?? 'NA',
          item.test_type,
          item.series ?? 1,
          item.grade,
          item.cerf ?? 'NA',
          item.format ?? 'NA',
          item.types ?? 'NA',
          item.skills,
          item.question_type,
          item.level ?? 'NA',
          item.group,
          item.question_description,
          item.question_text,
          item.image,
          item.video,
          item.audio,
          item.answers,
          item.correct_answers,
          item.points ?? 0,
          item.parent_question_description,
          item.parent_question_text,
          item.audio_script,
          calcuQuestion(item),
          item.privacy ??0,
          item.name,
          item.id,
        ],
      )
      if (result.affectedRows !== 1) {
        throw Error('update failed')
      }
      return res.json({ ...item, created_by: user.is_admin ? null : user.id })
    } catch (e: any) {
      console.error("update---question", e)
      res.status(500).json({ message: e.message })
    }
  }
}

export default handler

export const calcuQuestion = (question: QuestionDataType) => {
  let total = 1
  const questionType = PRACTICE_TEST_ACTIVITY[question.question_type]
  const questionList = question.question_text?.split('#')
  const answerList = question.answers?.split('#')
  const answerListTF = question.answers
    ?.split('#')
    .filter((item: string) => !item.startsWith('*'))
  const correctAnswerList = question.correct_answers?.split(/\#|\[\]/g)
  
  try {
    if (questionType === 1) {
      total = 1
    } else if (questionType === 11) {
      total = questionList.filter(m=> !m?.startsWith("*")).length;
    } else if (questionType === 2) {
      total = questionList.length
    } else if (questionType === 3) { //FB2, FB3, FB6
      total = correctAnswerList.filter(m => !m?.startsWith("*")).length
    } else if (questionType === 4) {
      total = answerList.length
    } else if (questionType === 5) { //SL01 
      total = correctAnswerList.filter(m => !m?.startsWith("*")).length
    } else if (questionType === 6) {
      const answerListDD1 = question.question_text?.split('#')
        .filter((item: string) => !item.startsWith('*'))
      total = answerListDD1.length
    } else if (questionType === 7) {
      total = 1
    } else if (questionType === 8 || questionType === 19) {
      total = answerListTF.length
    } else if (questionType === 9) {
      const partLeft = answerList[0].split('*')
      total = partLeft.length
    } else if (questionType === 12 || questionType === 13) {
      total = 1
    } else if (questionType === 14) {
      total = correctAnswerList.length
    } else if (questionType === 15) {
      total = answerList.length
    } else if (questionType === 16) {
      total = correctAnswerList.length
    } else if (questionType === 17) {
      total = correctAnswerList.length
    } else if (questionType === 18) {
      total = correctAnswerList.length
    } else if (questionType === 20) {
      total = questionList.filter(m=> !m?.startsWith("*")).length;
    } else if (questionType === 21) { //mc2, check example
      // start * is example
      total = questionList.filter(m=> !m?.startsWith("*")).length;
    } else if (questionType === 23 || questionType === 37 || questionType === 33) { //MG4, MG5, MG6 check example
      const totalCorrect = correctAnswerList.length;
      const listCheckExample = answerList[2]?.split("*")||[]
      const totalExample = listCheckExample.filter(m=>m=="ex").length;
      total = totalCorrect - totalExample;
    } else if (questionType === 22 || questionType === 24 || questionType === 30 || questionType === 32) { //MC7, MC9, MC8, MC10 check example
      total = questionList.filter(m=> !m?.startsWith("*")).length;
    } else if (questionType === 25 || questionType === 26) { //FB4, FB7
      total = correctAnswerList.filter(m=> !m?.startsWith("*")).length;
    } else if (questionType === 27) { //FB8
      total = correctAnswerList.filter(m=> !m?.startsWith("*")).length;
    } else if (questionType === 28 || questionType === 34 || questionType === 40 ) { //DL1 DL2 DL3
      const correctAnswerListDL = correctAnswerList.filter((item) => !item.startsWith('ex|')).length
      total = correctAnswerListDL
    } else if (questionType === 29) { //TF5
      total = answerListTF.length
    } else if (questionType === 31) { //SO1
      const correctAnswerListSO = correctAnswerList.filter((item) => !item.startsWith('ex|'))
      const totalArr = []
      for(let i = 0; i< correctAnswerListSO.length; i++){
        const item = correctAnswerListSO[i]
        if(item.split('*')[2] === 'True'){
          totalArr.push(item)
        }
      }
      total = totalArr.length
    } else if (questionType === 36) { //DD2
      const answerListDD2 = question.correct_answers
        ?.split('#')
        .filter((item: string) => !item.startsWith('*'))
      total = answerListDD2.length
    }
    else if (questionType === 38) { //DS1
      total = answerList.filter(m=> !m.startsWith("ex|")).length
    }else if(questionType === 39 || questionType === 42) { //FC1, FC2
      total = correctAnswerList.filter((item) => !item.startsWith('ex|')).length
    }else if (questionType === 41) { //MC11
      total = answerList.filter(m=> !m.startsWith("ex|")).length
    }
    
  } catch (ex) {}
  return total
}

const handleRequest = (req: NextApiRequest) => {
  return new Promise((resolve, reject) => {
    const form = new IncomingForm()
    form.parse(req, async (err, fields, files) => {
      if (err) reject(err)
      const obFiles: any = {}
      const image_files_key = []
      const image_files_group2_key = []
      for (const key of Object.keys(files)) {
        if (key.startsWith('image_files')) {
          image_files_key.push(key)
        } else if(key.startsWith('image_file_group2')){
          image_files_group2_key.push(key)
        }else {
          obFiles[key] = files[key]
        }
      }
      //sort key image_files
      image_files_key.sort()
      image_files_group2_key.sort()

      for (let i = 0; i < image_files_key.length; i++) {
        if (obFiles['image_files'] === undefined) obFiles['image_files'] = []
        obFiles['image_files'].push(files[image_files_key[i]])
      }
      for (let i = 0; i < image_files_group2_key.length; i++) {
        if (obFiles['image_file_group2'] === undefined) obFiles['image_file_group2'] = []
        obFiles['image_file_group2'].push(files[image_files_group2_key[i]])
      }
      resolve({ ...fields, ...obFiles })
    })
  })
}

const saveFile = async (
  filePath: string,
  pathDes: string,
  fileName: string,
) => {
  let file = null
  try {
    file = await fileUtils.exist(pathDes)
  } catch {}
  if (!file) {
    fileUtils.createDir(pathDes, { recursive: true })
  }
  const fileData = await fileUtils.readFile(filePath)
  return await fileUtils.writeFile(`${pathDes}/${fileName}`, fileData)
}
