/* eslint-disable @typescript-eslint/no-unused-vars */
import fs from 'fs'

import Excel from 'exceljs'
import { IncomingForm } from 'formidable'
import { NextApiRequest, NextApiResponse } from 'next'
import { getSession } from 'next-auth/client'
// @ts-ignore
import { unrar } from 'unrar-promise'

import {
  FORMAT_SELECTIONS,
  PUBLISHER_SELECTIONS,
  TEST_TYPE_SELECTIONS,
  TYPES_SELECTIONS,
} from '@/interfaces/struct'
import convertRowObject from '@/questions_middleware/convertRowObject'
import getDisplay from '@/questions_middleware/getDisplay'
import { ApiDataResponse } from 'interfaces/types'
import { query } from 'lib/db'

import fileUtils from '../../../../utils/file'
import templateQuestions from './templates/index'
export const config = {
  api: {
    bodyParser: false,
  },
}

//type
const TypeProcess = {
  extracting: 1,
  processing: 2,
}
type CallbackProgress = (process: number, total: number) => void

const tmpDir = `./temps/questions`
const tmpDirProcess = './temps/questions/process'
const fileExtensions = ['zip', 'rar']
const handler: any = async (
  req: NextApiRequest,
  res: NextApiResponse<ApiDataResponse>,
) => {
  try {
    const session = await getSession({ req })
    const user: any = session?.user
    switch (req.method) {
      case 'POST':
        // if (!session) {
        //   return res.status(403).json({ code: 0, message: 'Forbidden' })
        // }
        await handleRequestPost(req, res, user)
        break
      case 'GET':
        await getProcessImport(req, res)
        break
      default:
        res.status(405).json({ code: 0, message: 'Method Not Allowed' })
    }
  } catch (err) {
    console.log('err--import-question', err)
    res.status(500).json({ code: 0, message: 'have an error' })
  }
}
const createProcess = (
  filePath: string,
  status: number,
  title: string,
  success: number,
  total: number,
) => {
  try {
    fs.writeFileSync(
      filePath,
      JSON.stringify({
        status: status,
        title: title,
        success: success,
        total: total,
      }),
    )
    return
  } catch {
    return
  }
}

const getProcessImport = async (req: NextApiRequest, res: NextApiResponse) => {
  const processName = req.query.process_name
  if (fs.existsSync(`${tmpDirProcess}/${processName}`)) {
    const data = await fs.readFileSync(
      `${tmpDirProcess}/${processName}`,
      'utf8',
    )
    res.status(200).json(JSON.parse(data))
  } else {
    res.status(200).json(null)
  }
}
//
const getFileExtension = (fileName: string) => {
  if (fileName) {
    return (
      fileName.substring(fileName.lastIndexOf('.') + 1, fileName.length) ||
      fileName
    )
  } else {
    return ''
  }
}

//import - question
const handleRequestPost = (
  req: NextApiRequest,
  res: NextApiResponse<ApiDataResponse>,
  user: any,
) => {
  return new Promise((resolve, reject) => {
    const options = {
      maxFileSize: 100 * 1024 * 1024,
    }
    const form = new IncomingForm(options)

    form.parse(req, async (err: any, fields: any, files: any) => {
      const processName = fields.process_name ?? 'temp'
      const processPath = `${tmpDirProcess}/${processName}`
      const newDate = new Date()
      const destinationFolder = `question_imports/${user?.id}/${(
        newDate.getFullYear() + ''
      ).slice(
        -2,
      )}${newDate.getMonth()}${newDate.getDate()}${newDate.getHours()}${newDate.getMinutes()}${newDate.getSeconds()}`

      const listGrade = await query<any[]>(
        'SELECT id as code, name as display FROM grade where deleted =0 order by priority',
        [],
      )
      const listSeries = await query<any[]>(
        'SELECT id as code, name as display FROM series where deleted =0 order by priority',
        [],
      )
      try {
        if (err) throw err
        if (!files.file) {
          return resolve(
            res
              .status(400)
              .json({
                code: 0,
                message:
                  'Đã xảy ra một số vấn đề trong quá trình tải file. Vui lòng tải lại file của bạn',
              }),
          )
        }

        const { format, grade, publisher, series, test_type, types } = fields
        const fileName = files.file.originalFilename
        const ext = getFileExtension(fileName)?.toLowerCase() || ''
        if (fileExtensions.indexOf(ext) < 0) {
          return resolve(
            res
              .status(400)
              .json({
                code: 0,
                message: 'Chỉ hổ trợ các file có định dạng: zip, rar',
              }),
          )
        }

        //create folder
        if (!fs.existsSync(tmpDir)) {
          fs.mkdirSync(tmpDir, { recursive: true })
        }
        if (!fs.existsSync(tmpDirProcess)) {
          fs.mkdirSync(tmpDirProcess, { recursive: true })
        }
        //create temp folder
        fs.mkdtemp(`${tmpDir}/f-`, async (err, folder) => {
          if (err)
            return resolve(
              res.status(400).json({
                code: 0,
                message: 'Error creating temporary directory',
              }),
            )
          else {
            createProcess(
              processPath,
              TypeProcess.extracting,
              'Extracting data...',
              0,
              0,
            )
            //extract file
            try {
              if (ext == 'zip') {
                await saveFileZip(folder, files.file)
              } else if (ext == 'rar') {
                await saveFileRar(folder, files.file)
              }
            } catch (e) {
              console.log('extract file', e)
            }
            
            const checkGradeCode = listGrade.findIndex((m:any)=> m.code == fields.grade ) !=- 1
            const checkSeriesCode = listSeries.findIndex((m:any)=> m.code == fields.series ) !=-1
            // check field grade active in database
            if(!checkGradeCode && !checkSeriesCode){
              deleteDir(folder)
              await fileUtils.remove(processPath)
              return resolve(
                res.status(400).json({
                  code: 2,
                  message:
                    'Khối lớp và chương trình vừa chọn không còn trong hệ thống',
                }),
              )
            }

            // check field series active in database
            if(!checkSeriesCode){
              deleteDir(folder)
              await fileUtils.remove(processPath)
              return resolve(
                res.status(400).json({
                  code: 2,
                  message:
                    'Chương trình vừa chọn không còn trong hệ thống',
                }),
              )
            }
            if(!checkGradeCode){
              deleteDir(folder)
              await fileUtils.remove(processPath)
              return resolve(
                res.status(400).json({
                  code: 2,
                  message:
                    'Khối lớp vừa chọn không còn trong hệ thống',
                }),
              )
            }
            // list file in folder extract
            const fileData = await listFiles(folder)
            // console.log('fileData', fileData)
            if (!fileData || fileData.length == 0) {
              deleteDir(folder)
              await fileUtils.remove(processPath)
              return resolve(
                res.status(400).json({
                  code: 2,
                  message:
                    'Không tìm thấy file câu hỏi (.xlsx) trong file bạn tải lên',
                }),
              )
            }

            //count xlsx file
            const countXlsx = fileData.filter(
              (file: any) =>
                getFileExtension(file).toLowerCase().indexOf('xlsx') >= 0,
            )
            if (countXlsx.length > 1) {
              deleteDir(folder)
              await fileUtils.remove(processPath)
              return resolve(
                res.status(400).json({
                  code: 2,
                  message:
                    'Vui lòng chỉ tạo 1 file câu hỏi (.xlsx) trong file bạn tải lên',
                }),
              )
            }
            let hasExcelFile = false
            for (const file of fileData) {
              if (getFileExtension(file).toLowerCase().indexOf('xlsx') >= 0) {
                hasExcelFile = true
                const filePath = `${folder}/${file}`

                const dataQuestions: any = await handleExcelFile(filePath)
                // handle question.
                // resolve(res.status(200).json(dataQuestions));
                const dataResults = await handleQuestions(
                  dataQuestions,
                  user,
                  format,
                  grade,
                  publisher,
                  series,
                  test_type,
                  types,
                  (num, total) => {
                    createProcess(
                      processPath,
                      TypeProcess.processing,
                      'Processing data...',
                      num,
                      total,
                    )
                  },
                  { temp: folder, destination: destinationFolder },
                )
                //delete folder temp
                deleteDir(folder)
                await fileUtils.remove(processPath)
                return resolve(
                  res.status(200).json({
                    code: 1,
                    message: 'Import questions successfully',
                    data: {
                      grade: getDisplay.handleGetDisplay(listGrade, fields.grade),
                      test_type: getDisplay.handleGetDisplay(
                        TEST_TYPE_SELECTIONS,
                        fields.test_type,
                      ),
                      publisher: getDisplay.handleGetDisplay(
                        PUBLISHER_SELECTIONS,
                        fields.publisher,
                      ),
                      format: getDisplay.handleGetDisplay(
                        FORMAT_SELECTIONS,
                        fields.format,
                      ),
                      types: getDisplay.handleGetDisplay(TYPES_SELECTIONS, fields.types),
                      series: getDisplay.handleGetDisplayCodeNumber(
                        listSeries,
                        fields.series,
                      ),
                      result: dataResults,
                    },
                  }),
                )
              }
              // if (file.indexOf('.csv') >= 0) {
              //   const filePath = `${folder}/${file}`
              //   const dataQuestion = await handleCsvFile(filePath)
              //   // handle question.

              //   //delete folder temp
              //   deleteDir(folder)
              //   return resolve(
              //     res.status(200).json({
              //       code: 1,
              //       message: 'Import questions successfull',
              //       data: dataQuestion,
              //     }),
              //   )
              // }
            }
            deleteDir(folder)
            await fileUtils.remove(processPath)

            //hasnot
            if (hasExcelFile === false) {
              return resolve(
                res.status(400).json({
                  code: 2,
                  message:
                    'Không tìm thấy file câu hỏi (.xlsx) trong file bạn tải lên',
                }),
              )
            }
            deleteDir(folder)
            await fileUtils.remove(processPath)
            return resolve(
              res.status(400).json({
                code: 0,
                message: 'Import questions unsuccessfull',
              }),
            )
          }
          //delete foder temp-upload
          deleteDir(folder)
          await fileUtils.remove(processPath)
        })
      } catch (ex: any) {
        console.log('ex==========', ex)
        if (ex?.httpCode == 413) {
          //payload to large
          return resolve(
            res.status(400).json({
              code: 2,
              message: 'Dung lượng file tải lên tối đa là 100MB',
            }),
          )
        } else
          return resolve(
            res.status(500).json({
              code: 0,
              message: ex.message,
            }),
          )
      }
    })
  })
}
//handle Question:
const handleQuestions = async (
  listQuestions: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
  callback: CallbackProgress,
  folder: any,
) => {
  try {
    let i = 1
    let countQuestion = 0
    let success = 0
    const listResult: any = []
    // let question_type_before = ""
    const len = listQuestions.length
    while (i < len) {
      const item = listQuestions[i]
      const keyQuestionType = `question_type`
      if (!item[keyQuestionType] || item[keyQuestionType] == '') {
        countQuestion += 1
        const itemResult0 = {
          ...item,
          index: countQuestion,
          success: false,
          arr_message: ['Vui lòng nhập thông tin cột Question Type'],
        }
        listResult.push(itemResult0)
      } else
        switch (
          //27 template
          item[keyQuestionType].toUpperCase() //question_type
        ) {
          case 'DD1':
            const listDataDD1 = []
            listDataDD1.push(item)
            while (i < len - 1) {
              const itemCheck = listQuestions[i + 1]
              if (checkSubQuestion(itemCheck)) {
                listDataDD1.push(itemCheck)
                i++
              } else {
                break
              }
            }
            const resultDD1: any = await templateQuestions.handleDataDD1(
              listDataDD1,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultDD1['success'] == true) {
              success += 1
            }
            resultDD1.listResult.map(
              (item: any) => (item['index'] = countQuestion),
            )
            listResult.push(...resultDD1.listResult)
            break
          case 'MC1':
            const listDataMC1 = []
            listDataMC1.push(item)
            while (i < len - 1) {
              const itemCheck = listQuestions[i + 1]
              if (checkSubQuestion(itemCheck)) {
                listDataMC1.push(itemCheck)
                i++
              } else {
                break
              }
            }
            const resultMC1: any = await templateQuestions.handleDataMC1(
              listDataMC1,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultMC1.success == true) {
              success += 1
            }
            resultMC1.listResult.map(
              (item: any) => (item['index'] = countQuestion),
            )
            listResult.push(...resultMC1.listResult)
            break
          case 'MC2':
            const listDataMC2 = []
            listDataMC2.push(item)
            while (i < len - 1) {
              const itemCheck = listQuestions[i + 1]
              if (checkSubQuestion(itemCheck)) {
                listDataMC2.push(itemCheck)
                i++
              } else {
                break
              }
            }
            const resultMC2: any = await templateQuestions.handleDataMC2(
              listDataMC2,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultMC2.success == true) {
              success += 1
            }
            resultMC2.listResult.map(
              (item: any) => (item['index'] = countQuestion),
            )
            listResult.push(...resultMC2.listResult)
            break
          case 'MC3':
            const resultMC3: any = await templateQuestions.handleDataMC3(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultMC3['success'] == true) {
              success += 1
            }
            resultMC3['index'] = countQuestion
            listResult.push(resultMC3)
            break
          case 'MC4':
            const resultMC4: any = await templateQuestions.handleDataMC4(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultMC4['success'] == true) {
              success += 1
            }
            resultMC4['index'] = countQuestion
            listResult.push(resultMC4)
            break
          case 'MC5':
            const listDataMC5 = []
            listDataMC5.push(item)
            while (i < len - 1) {
              const itemCheck = listQuestions[i + 1]
              if (checkSubQuestion(itemCheck)) {
                listDataMC5.push(itemCheck)
                i++
              } else {
                break
              }
            }
            const resultMC5: any = await templateQuestions.handleDataMC5(
              listDataMC5,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultMC5.success == true) {
              success += 1
            }
            resultMC5.listResult.map(
              (item: any) => (item['index'] = countQuestion),
            )
            listResult.push(...resultMC5.listResult)

            break
          case 'MC6':
            const listDataMC6 = []
            listDataMC6.push(item)
            while (i < len - 1) {
              const itemCheck = listQuestions[i + 1]
              if (checkSubQuestion(itemCheck)) {
                listDataMC6.push(itemCheck)
                i++
              } else {
                break
              }
            }
            const resultMC6: any = await templateQuestions.handleDataMC6(
              listDataMC6,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultMC6.success == true) {
              success += 1
            }
            resultMC6.listResult.map(
              (item: any) => (item['index'] = countQuestion),
            )
            listResult.push(...resultMC6.listResult)

            break
          case 'FB1':
            const resultFB1: any = await templateQuestions.handleDataFB1(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultFB1.success == true) {
              success += 1
            }
            resultFB1['index'] = countQuestion
            listResult.push(resultFB1)
            break
          case 'FB2':
            const resultFB2: any = await templateQuestions.handleDataFB2(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultFB2.success == true) {
              success += 1
            }
            resultFB2['index'] = countQuestion
            listResult.push(resultFB2)
            break
          case 'FB3':
            const resultFB3: any = await templateQuestions.handleDataFB3(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultFB3.success == true) {
              success += 1
            }
            resultFB3['index'] = countQuestion
            listResult.push(resultFB3)
            break
          case 'FB4':
            const resultFB4: any = await templateQuestions.handleDataFB4(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultFB4.success == true) {
              success += 1
            }
            resultFB4['index'] = countQuestion
            listResult.push(resultFB4)
            break
          case 'FB5':
            const resultFB5: any = await templateQuestions.handleDataFB5(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultFB5.success == true) {
              success += 1
            }
            resultFB5['index'] = countQuestion
            listResult.push(resultFB5)
            break
          case 'FB6':
            const resultFB6: any = await templateQuestions.handleDataFB6(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultFB6.success == true) {
              success += 1
            }
            resultFB6['index'] = countQuestion
            listResult.push(resultFB6)
            break
          case 'FB7':
            const resultFB7: any = await templateQuestions.handleDataFB7(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultFB7.success == true) {
              success += 1
            }
            resultFB7['index'] = countQuestion
            listResult.push(resultFB7)
            break
          case 'SA1':
            const resulSA1: any = await templateQuestions.handleDataSA1(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resulSA1.success == true) {
              success += 1
            }
            resulSA1['index'] = countQuestion
            listResult.push(resulSA1)
            break
          case 'TF1':
            const resulTF1: any = await templateQuestions.handleDataTF1(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resulTF1.success == true) {
              success += 1
            }
            resulTF1['index'] = countQuestion
            listResult.push(resulTF1)
            break
          case 'TF2':
            const resulTF2: any = await templateQuestions.handleDataTF2(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resulTF2.success == true) {
              success += 1
            }
            resulTF2['index'] = countQuestion
            listResult.push(resulTF2)
            break
          case 'LS1':
            const resultLS1: any = await templateQuestions.handleDataLS1(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultLS1.success == true) {
              success += 1
            }
            resultLS1['index'] = countQuestion
            listResult.push(resultLS1)
            break
          case 'LS2':
            const resultLS2: any = await templateQuestions.handleDataLS2(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultLS2.success == true) {
              success += 1
            }
            resultLS2['index'] = countQuestion
            listResult.push(resultLS2)
            break
          case 'SL1':
            const resultSL1: any = await templateQuestions.handleDataSL1(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultSL1.success == true) {
              success += 1
            }
            resultSL1['index'] = countQuestion
            listResult.push(resultSL1)
            break
          case 'SL2':
            const listDataSL2 = []
            listDataSL2.push(item)
            while (i < len - 1) {
              const itemCheck = listQuestions[i + 1]
              if (checkSubQuestion(itemCheck)) {
                listDataSL2.push(itemCheck)
                i++
              } else {
                break
              }
            }
            const resultSL2: any = await templateQuestions.handleDataSL2(
              listDataSL2,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultSL2.success == true) {
              success += 1
            }
            resultSL2.listResult.map(
              (item: any) => (item['index'] = countQuestion),
            )
            listResult.push(...resultSL2.listResult)

            break
          case 'MR1':
            const resultMR1: any = await templateQuestions.handleDataMR1(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultMR1['success'] == true) {
              success += 1
            }
            resultMR1['index'] = countQuestion
            listResult.push(resultMR1)
            break
          case 'MR2':
            const resultMR2: any = await templateQuestions.handleDataMR2(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultMR2['success'] == true) {
              success += 1
            }
            resultMR2['index'] = countQuestion
            listResult.push(resultMR2)
            break
          case 'MR3':
            const resultMR3: any = await templateQuestions.handleDataMR3(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultMR3['success'] == true) {
              success += 1
            }
            resultMR3['index'] = countQuestion
            listResult.push(resultMR3)
            break
          case 'MG1':
            const resultMG1: any = await templateQuestions.handleDataMG1(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultMG1['success'] == true) {
              success += 1
            }
            resultMG1['index'] = countQuestion
            listResult.push(resultMG1)
            break
          case 'MG2':
            const resultMG2: any = await templateQuestions.handleDataMG2(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
            )
            countQuestion += 1
            if (resultMG2['success'] == true) {
              success += 1
            }
            resultMG2['index'] = countQuestion
            listResult.push(resultMG2)
            break
          case 'MG3':
            const resultMG3: any = await templateQuestions.handleDataMG3(
              item,
              user,
              format,
              grade,
              publisher,
              series,
              test_type,
              types,
              folder,
            )
            countQuestion += 1
            if (resultMG3['success'] == true) {
              success += 1
            }
            resultMG3['index'] = countQuestion
            listResult.push(resultMG3)
            break
            case 'TF3':
              const resultTF3: any = await templateQuestions.handleDataTF3(
                item,
                user,
                format,
                grade,
                publisher,
                series,
                test_type,
                types,
                folder,
              )
              countQuestion += 1
              if (resultTF3['success'] == true) {
                success += 1
              }
              resultTF3['index'] = countQuestion
              listResult.push(resultTF3)
              break
              case 'TF4':
                const resultTF4: any = await templateQuestions.handleDataTF4(
                  item,
                  user,
                  format,
                  grade,
                  publisher,
                  series,
                  test_type,
                  types,
                  folder,
                )
                countQuestion += 1
                if (resultTF4['success'] == true) {
                  success += 1
                }
                resultTF4['index'] = countQuestion
                listResult.push(resultTF4)
                break
          default:
            countQuestion += 1
            item['success'] = false
            item['arr_message'] = ['question type không đúng']
            item['index'] = countQuestion
            listResult.push(item)
            break
        }

      i++
      //callbackProcess
      callback(i, len - 1)
    }
    return {
      listResult: listResult,
      countQuestion: countQuestion,
      success: success,
    }
  } catch (er) {
    console.log('er=========', er)
    return {}
  }
}
const checkSubQuestion = (itemRow: any) => {
  if (!itemRow) return false
  if (
    (!itemRow['question_type'] || itemRow['question_type'] == '') &&
    (!itemRow['level'] || itemRow['level'] == '') &&
    (!itemRow['skill'] || itemRow['skill'] == '')
  ) {
    return true
  } else {
    return false
  }
}

// handle excel
const handleExcelFile = async (filePath: string) => {
  try {
    const workbook = new Excel.Workbook()

    await workbook.xlsx.readFile(filePath)
    // console.log('workbook==========loaded')
    if (workbook.worksheets.length <= 0) {
      // console.log('handleExcelFile---workbook.worksheets.length=0')
      return []
    }
    const worksheet = workbook.worksheets[0]
    if (!worksheet) {
      // console.log('handleExcelFile---worksheet fail', worksheet)
      return []
    }
    const data: any = []
    let index = 1
    worksheet.eachRow(function (row: any) {
      const rowData: any = {}
      row.eachCell(function (item: any) {

        let value = item?._value?.model?.value;
        if (value) {
          //cell font style
          const itemFont = item?._value.model?.style?.font || {}
          //cell value is object
          if (typeof value == 'object') {
            const textArr: any = []
            // get cellvalue detail
            if (value.richText && Array.isArray(value.richText)) {
              value?.richText.map((text: any, indexText: number) => {
                const textObj: any = {}
                textObj.text = text?.text

                if (!text.font) {
                  // if text not has font, set with font cell.
                  const { bold, italic, underline } = itemFont
                  textObj.underline = underline
                  textObj.italic = italic
                  textObj.bold = bold
                } else {
                  //text underline
                  if (text?.font?.underline == true) {
                    textObj.underline = true
                  }
                  if (text?.font?.bold == true) {
                    textObj.bold = true
                  }
                  if (text?.font?.italic == true) {
                    textObj.italic = true
                  }
                }

                textArr.push(textObj)
              })
              rowData[item?._address] = textArr
            }
          } else {
            if(typeof value == "number" || typeof value == "boolean"){
              value = value.toString();
            }
            //cell is text
            //if cell has font => set font with font cell
            const { bold, italic, underline } = itemFont
            if (bold || italic || underline) {
              rowData[item?._address] = [
                {
                  text: value,
                  bold,
                  italic,
                  underline,
                },
              ]
            } else {
              rowData[item?._address] = value
            }
          }
        }else if(typeof value == "number" || typeof value == "boolean"){
            //cell font style
            const itemFont = item?._value.model?.style?.font || {}
          const { bold, italic, underline } = itemFont
            if (bold || italic || underline) {
              rowData[item?._address] = [
                {
                  text: value.toString(),
                  bold,
                  italic,
                  underline,
                },
              ]
            } else {
              rowData[item?._address] = value.toString()
            }
        }
      })
      const dataConvert = convertRowObject(rowData, index)
      data.push(dataConvert)
      index++
    })
    return data
  } catch (e) {
    console.log('err==handleExcelFile', e)
    return []
  }
}
// handle csv
const handleCsvFile = async (filePath: string) => {
  const workbook = new Excel.Workbook()
  await workbook.csv.readFile(filePath)
  const worksheet = workbook.worksheets[0]
  // console.log(worksheet)
  const data: any = []

  worksheet.eachRow(function (row: any) {
    const rowData: any = {}
    row.eachCell(function (item: any) {
      const value = item._value.model.value
      if (value) {
        //cell value is object
        if (typeof value == 'object') {
          const textArr: any = []

          // get cellvalue detail
          value.richText.map((text: any) => {
            const textObj: any = {}
            textObj.text = text.text

            //text underline
            if (text?.font?.underline == true) {
              textObj.underline = true
            } else if (text?.font?.bold == true) {
              textObj.bold = true
            }
            textArr.push(textObj)
          })
          rowData[item._address] = textArr
        } else {
          //cell is text
          rowData[item._address] = value
        }
      }
    })
    data.push(rowData)
  })
  // console.log('data-csv', JSON.stringify(data))
  return data
}
//extract zip
const saveFileZip = async (dir: string, file: any) => {
  try {
    await fileUtils.unzip(file.filepath, dir)
    await fileUtils.remove(file.filepath)
  } catch (e) {
    console.log('err.saveFileZip======', e)
  }
}
//extract rar
const saveFileRar = async (dir: string, file: any) => {
  try {
    await unrar(file.filepath, dir)
    await fileUtils.remove(file.filepath)
  } catch (e) {
    console.log('err.saveFileRar======', e)
  }
}
//get list file in forder.
const listFiles = async (directory: string) => {
  const dirents = await fs.readdirSync(directory, { withFileTypes: true })
  return dirents
    .filter((dirent) => dirent.isFile())
    .map((dirent) => dirent.name)
}

//delete directory
const deleteDir = async (folder: string) => {
  try {
    if (folder) {
      fs.rmSync(folder, { recursive: true })
    }
  } catch (e) {
    console.error(
      `An error has occurred while removing the temp folder at ${folder}. Please remove it manually. Error: ${e}`,
    )
  }
}

//create question when upload file
export const createQuestionUpload = async (params: any, user: any) => {
  try {
    const {
      publisher,
      test_type,
      series,
      grade,
      cerf,
      format,
      types,
      skills,
      question_type,
      level,
      parent_question_description,
      parent_question_text,
      question_description,
      question_text,
      image,
      video,
      audio,
      answers,
      correct_answers,
      audio_script,
      points,
      total_question,
    } = params

    const arr_message = []
    for (const key in params) {
      if (params.hasOwnProperty(key)) {
        const check = verifyLengthData(key, params[key])
        if (check.code == 0) {
          arr_message.push(check.message)
        }
      }
    }

    //có lỗi khi kiểm tra chiều dài
    if (arr_message.length > 0) {
      return { code: 0, arr_message: arr_message }
    }
    const isSystem = user?.is_admin === 1 || user?.user_role_id === 1
    const result = await query<any>(
      `INSERT INTO question (publisher, test_type, series, grade, cerf, format, types, skills, question_type, 
      level, parent_question_description, parent_question_text, question_description, question_text, 
      image, video, audio, answers, correct_answers,audio_script, points, created_date, created_by, scope, total_question)
                  VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`,
      [
        publisher ?? 'NA',
        test_type,
        series ?? 1,
        grade,
        cerf ?? 'NA',
        format ?? 'NA',
        types ?? 'NA',
        skills,
        question_type,
        level ?? 'NA',
        parent_question_description,
        parent_question_text,
        question_description,
        question_text,
        image,
        video,
        audio,
        answers,
        correct_answers,
        audio_script,
        points ?? 0,
        new Date(),
        user?.id,
        isSystem ? 0 : 1,
        total_question,
      ],
    )
    if (result.affectedRows !== 1) {
      return { code: 0, arr_message: ['Không thể lưu trữ thành công'] }
    }
    return { code: 1, arr_message: ['Tạo câu hỏi thành công'] }
  } catch (error: any) {
    // console.log('error,createQuestionUpload', error)
    return { code: 0, arr_message: ['Có lỗi xảy ra khi lưu trữ dữ liệu'] }
  }
}

const verifyLengthData = (column: string, data: string) => {
  const objLength: any = {
    parent_question_description: 2000,
    parent_question_text: 10000,
    question_description: 10000,
    question_text: 10000,
    image: 2000,
    video: 2000,
    audio: 2000,
    answers: 10000,
    correct_answers: 10000,
    audio_script: 10000,
  }
  if (data && objLength[column] && data.length > objLength[column]) {
    return {
      code: 0,
      message: `${column} không được lớn hơn ${objLength[column]} ký tự`,
    }
  }
  return { code: 1, message: '' }
}
export default handler
