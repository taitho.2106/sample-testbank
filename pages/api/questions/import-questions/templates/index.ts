/* eslint-disable import/no-anonymous-default-export */

import handleDataDD1 from './DD/DD1'
import handleDataFB1 from './FB/FB1'
import handleDataFB2 from './FB/FB2'
import handleDataFB3 from './FB/FB3'
import handleDataFB4 from './FB/FB4'
import handleDataFB5 from './FB/FB5'
import handleDataFB6 from './FB/FB6'
import handleDataFB7 from './FB/FB7'
import handleDataLS1 from './LS/LS1'
import handleDataLS2 from './LS/LS2'
import handleDataMC1 from './MC/MC1_v2'
import handleDataMC2 from './MC/MC2_v2'
import handleDataMC3 from './MC/MC3'
import handleDataMC4 from './MC/MC4'
import handleDataMC5 from './MC/MC5'
import handleDataMC6 from './MC/MC6'
import handleDataMG1 from './MG/MG1'
import handleDataMG2 from './MG/MG2'
import handleDataMG3 from './MG/MG3'
import handleDataMR1 from './MR/MR1'
import handleDataMR2 from './MR/MR2'
import handleDataMR3 from './MR/MR3'
import handleDataSA1 from './SA/SA1'
import handleDataSL1 from './SL/SL1'
import handleDataSL2 from './SL/SL2'
import handleDataTF1 from './TF/TF1'
import handleDataTF2 from './TF/TF2'
import handleDataTF3 from './TF/TF3'
import handleDataTF4 from './TF/TF4'


//mc7, mc8, fb4, fb7, fb8, MG4 -- miss
export default {
  handleDataMC1,
  handleDataMC2,
  handleDataMC3,
  handleDataMC4,
  handleDataMC5,
  handleDataMC6,
  handleDataFB1,
  handleDataFB2,
  handleDataFB3,
  handleDataFB4,
  handleDataFB5,
  handleDataFB6,
  handleDataFB7,
  handleDataSL1,
  handleDataSL2,
  handleDataSA1,
  handleDataDD1,
  handleDataMR1,
  handleDataMR2,
  handleDataMR3,
  handleDataMG1,
  handleDataMG2,
  handleDataMG3,
  handleDataLS1,
  handleDataLS2,
  handleDataTF1,
  handleDataTF2,
  handleDataTF3,
  handleDataTF4
}
