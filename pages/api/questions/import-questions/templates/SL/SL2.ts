import { USER_ROLES } from '@/interfaces/constants'
import handlerSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import copyFileQuestion from '@/questions_middleware/copyFileQuestion'
import handlerAnswerSL2 from '@/questions_middleware/handleAnswer/SL2'
import { checkExistFile } from '@/questions_middleware/verifyRow/checkCellFormatFile'
import {
  requireCellQuestions,
  verifyValueLCS,
} from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../..'

const handleDataSL2 = async (
  arrData: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
  folder: any,
) => {
  try {
    let hasError = false
    const { temp, destination } = folder
    const listResult: any = []
    const questionType = arrData[0]['question_type']
    let textAudio = ''
    // let textImage = ''
    const arrImage = []
    const arrAnswer = []
    const arrCorrectAnswer = []
    const arrQuestionText = []
    for (let index = 0; index < arrData.length; index++) {
      const data = arrData[index]

      const dataResutl = { ...data }
      const arrCellHasData = []

      const arrMessage = []

      const arrCellRequire = [
        // 'parent_question_description',
        'question_text',
      ]
      let arrVerify: any = ['skill', 'level']
      if (index == 0) {
        arrCellRequire.push('question_description')
        arrCellRequire.push('skill')
        arrCellRequire.push('level')
        arrCellRequire.push('audio')

    // issystem
    const isSystem = user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
    if (isSystem) {
          arrCellRequire.push('cefr') //cefr
          arrVerify.push('cefr')
        } else if (data['cefr']) {
          arrVerify.push('cefr')
          arrCellHasData.push('cefr')
        }
      } else {
        arrVerify = []
      }
      arrCellRequire.push('image')
      //require
      const checkCellRequire = requireCellQuestions(data, arrCellRequire)
      if (checkCellRequire.code == 0) {
        arrMessage.push(checkCellRequire.message)
        hasError = true
      }
      arrCellHasData.push(...checkCellRequire.arrCellHasData)
      //check audio
      if (index == 0 && arrCellHasData.indexOf('audio') !== -1) {
        const audio = convertTextCell(data['audio'])
        const listAudio = audio.split('|')
        const checkFile = checkExistFile(temp, listAudio, 'audio', ['mp3'])
        if (checkFile.code == 0) {
          arrMessage.push(checkFile.message)
          hasError = true
        } else {
          copyFileQuestion(temp, destination, listAudio)
          for (let i = 0; i < listAudio.length; i++) {
            listAudio[i] = `${destination}/${listAudio[0]}`
          }
          textAudio = listAudio.join(`#`)
        }
      }
      //check image -- hasdata
      if (arrCellHasData.includes('image')) {
        const image = convertTextCell(data['image'])

        const checkFile = checkExistFile(temp, [image], 'image', [
          'png',
          'jpg',
          'jpeg',
        ])
        if (checkFile.code == 0) {
          arrMessage.push(checkFile.message)
          hasError = true
        } else {
          copyFileQuestion(temp, destination, [image])
          arrImage.push(`${destination}/${image}`)
        }
      }
      //verify value
      for (let i = 0; i < arrVerify.length; i++) {
        const key = arrVerify[i]
        if (arrCellHasData.indexOf(key) !== -1) {
          const resultV = await verifyValueLCS(key, data[key])
          if (resultV.code == 0) {
            arrMessage.push(resultV.message)
            hasError = true
          }
        }
      }
      const question_text = handlerSpaceWord(
        convertTextCell(data[`question_text`]),
      )
      //validate question text
      if (question_text && question_text.length > 0) {
        const countSpace = (question_text.match(/%s%/g) || []).length
        if (countSpace != 1) {
          hasError = true
          arrMessage.push(
            'Vui lòng nhập 1 chỗ trống ___ ở Question Text',
          )
        }
        arrQuestionText.push(question_text)
      }
      //validate answer
      const handlerAnswer = handlerAnswerSL2(data)
      if (handlerAnswer.code === 1) {
        arrCorrectAnswer.push(handlerAnswer.correct_answers)
        arrAnswer.push(handlerAnswer.arr_answer)
      } else {
        hasError = true
        arrMessage.push(handlerAnswer.message)
      }
      dataResutl['arr_message'] = [...arrMessage]
      listResult.push({ ...dataResutl })
    }
    const data0 = arrData[0]
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data0['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data0['skill'].toUpperCase(),
        question_type: questionType.toUpperCase(),
        level: data0['level'].toUpperCase(),
        question_description: convertTextCell(data0['question_description']),
        question_text: arrQuestionText.join('#'),
        answers: arrAnswer.join('#'),
        correct_answers: arrCorrectAnswer.join('#'),
        total_question: arrData.length,
        audio: textAudio,
        image: arrImage.join('#'),
        audio_script: convertTextCell(data0['audio_script']),
      }
      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        hasError = false
      } else {
        hasError = true
        listResult[0]['arr_message'].push(...resSaveDb.arr_message)
      }
    }
    if (hasError == true) {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = false
      }
    } else {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = true
      }
    }
    return { success: !hasError, listResult: listResult }
  } catch (error: any) {
    return { success: false, listResult: arrData }
  }
}

export default handleDataSL2
