import { USER_ROLES } from '@/interfaces/constants'
import handlerSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import handlerAnswerSL1 from '@/questions_middleware/handleAnswer/SL1'
import {
  requireCellQuestions,
  verifyValueLCS,
} from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../..'

const handleDataSL1 = async (
  data: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
) => {
  try {
    const dataResutl = { ...data }
    let hasError = false
    const arrMessage = []
    const arrCellRequire = [
      'question_description',
      'question_text',
      'skill',
      'level',
    ]
    const arrCellHasData = []
    const arrVerify = ['skill', 'level']

    // issystem
    const isSystem = user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
    if (isSystem) {
      arrCellRequire.push('cefr') //cefr
      arrVerify.push('cefr')
    } else if (data['cefr']) {
      arrVerify.push('cefr')
      arrCellHasData.push('cefr')
    }
    const checkCellRequire = requireCellQuestions(data, arrCellRequire)
    if (checkCellRequire.code == 0) {
      arrMessage.push(checkCellRequire.message)
      hasError = true
    }
    arrCellHasData.push(...checkCellRequire.arrCellHasData)
    //verify value
    for (let i = 0; i < arrVerify.length; i++) {
      const key = arrVerify[i]
      if (arrCellHasData.indexOf(key) !== -1) {
        const resultV =await verifyValueLCS(key, data[key])
        if (resultV.code == 0) {
          arrMessage.push(resultV.message)
          hasError = true
        }
      }
    }
    const question_text = handlerSpaceWord(
      convertTextCell(data[`question_text`]),
    )
    //validate question text
    if (Boolean(question_text)) {
      if ((question_text.match(/%s%/g) || []).length < 1) {
        hasError = true
        arrMessage.push(
          'Vui lòng nhập ít nhất 1 chỗ trống ___ ở câu hỏi – Question Text',
        )
      }
    }
    //validate answer
    const numSpace = Boolean(question_text) && (question_text.match(/%s%/g) || []).length
    const handlerAnswer = handlerAnswerSL1(data,numSpace)
    if(handlerAnswer.code === 0){
      hasError = true 
      arrMessage.push(handlerAnswer.message)
    }
    
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data['skill'].toUpperCase(),
        question_type: data['question_type'].toUpperCase(),
        level: data['level'].toUpperCase(),
        question_description: convertTextCell(data['question_description']),
        question_text: convertTextCell(question_text),
        answers: handlerAnswer.arr_answer,
        correct_answers: handlerAnswer.correct_answers,
        total_question: handlerAnswer.lengthCorrectAnswer,
      }
      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        dataResutl['success'] = true
      } else {
        dataResutl['success'] = false
        arrMessage.push(...resSaveDb.arr_message)
        dataResutl['arr_message'] = arrMessage
      }
    } else {
      dataResutl['success'] = false
      dataResutl['arr_message'] = arrMessage
    }
    return dataResutl
  } catch (error: any) {
    data['success'] = false
    data['arr_message'] = ['Có lỗi xảy ra']
    return data
  }
}

export default handleDataSL1
