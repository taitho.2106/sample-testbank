import { USER_ROLES } from '@/interfaces/constants'
import convertTextCell from '@/questions_middleware/convertTextCell'
import copyFileQuestion from '@/questions_middleware/copyFileQuestion'
import handlerAnswerFB17 from '@/questions_middleware/handleAnswer/FB1_7'
import { checkExistFile } from '@/questions_middleware/verifyRow/checkCellFormatFile'
import {
  requireCellQuestions,
  verifyValueLCS,
} from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../..'
const handeDataFB5 = async (
  data: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
  folder: any,
) => {
  try {
    const dataResutl = { ...data }
    let hasError = false
    const arrMessage = []
    const { temp, destination } = folder
    const arrCellRequire = [
      'question_description',
      'skill',
      'level',
      'image',
      'audio',
    ]
    let textImage = ''
    let textAudio = ''
    const arrCellHasData = []
    const arrVerify = ['skill', 'level']

    // issystem
    const isSystem = user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
    if (isSystem) {
      arrCellRequire.push('cefr') //cefr
      arrVerify.push('cefr')
    } else if (data['cefr']) {
      arrVerify.push('cefr')
      arrCellHasData.push('cefr')
    }
    //check required
    const checkCellRequire = requireCellQuestions(data, arrCellRequire)
    if (checkCellRequire.code == 0) {
      arrMessage.push(checkCellRequire.message)
      hasError = true
    }
    arrCellHasData.push(...checkCellRequire.arrCellHasData)
    //check image
    if (arrCellHasData.includes('image')) {
      const images = convertTextCell(data['image'])
      const listImage = images.split('|')
      const checkFileImage = checkExistFile(temp, listImage, 'image', [
        'png',
        'jpg',
        'jpeg',
      ])
      if (checkFileImage.code == 0) {
        arrMessage.push(checkFileImage.message)
        hasError = true
      } else {
        copyFileQuestion(temp, destination, listImage)
        for (let i = 0; i < listImage.length; i++) {
          listImage[i] = `${destination}/${listImage[0]}`
        }
        textImage = listImage.join(`#`)
      }
    }

    //check audio
    if (arrCellHasData.includes('audio')) {
      const audio = convertTextCell(data['audio'])
      const listAudio = audio.split('|')
      const checkFileAudio = checkExistFile(temp, listAudio, 'audio', ['mp3'])
      if (checkFileAudio.code == 0) {
        arrMessage.push(checkFileAudio.message)
        hasError = true
      } else {
        copyFileQuestion(temp, destination, listAudio)
        for (let i = 0; i < listAudio.length; i++) {
          listAudio[i] = `${destination}/${listAudio[0]}`
        }
        textAudio = listAudio.join(`#`)
      }
    }

    //verify value
    for (let i = 0; i < arrVerify.length; i++) {
      const key = arrVerify[i]
      if (arrCellHasData.indexOf(key) !== -1) {
        const resultV = await verifyValueLCS(key, data[key])
        if (resultV.code == 0) {
          arrMessage.push(resultV.message)
          hasError = true
        }
      }
    }

    //validate data Answers
    const handlerAnswer = handlerAnswerFB17(data)
    if(handlerAnswer.code === 0) {
      hasError = true 
      arrMessage.push(handlerAnswer.message)
    }
    // if (handlerAnswer.data.lengthAnswer < 1) {
    //   hasError = true
    //   arrMessage.push(
    //     'Vui lòng có ít nhất 1 đáp án, nhập đáp án vào ô Answer 1',
    //   )
    // }
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data['skill'].toUpperCase(),
        question_type: data['question_type'].toUpperCase(),
        level: data['level'].toUpperCase(),
        question_description: convertTextCell(data['question_description']),
        correct_answers: handlerAnswer.data.stringAnswer,
        total_question: handlerAnswer.data.lengthAnswer,
        image: textImage,
        audio: textAudio,
      }
      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        dataResutl['success'] = true
      } else {
        dataResutl['success'] = false
        arrMessage.push(...resSaveDb.arr_message)
        dataResutl['arr_message'] = arrMessage
      }
    } else {
      dataResutl['success'] = false
      dataResutl['arr_message'] = arrMessage
    }
    return dataResutl
  } catch (error: any) {
    data['success'] = false
    data['arr_message'] = ['Có lỗi xảy ra']
    return data
  }
}

export default handeDataFB5
