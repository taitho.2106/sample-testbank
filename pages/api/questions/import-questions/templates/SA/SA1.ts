import { USER_ROLES } from '@/interfaces/constants'
import handlerBoldUnderline from '@/questions_middleware/convertBoldUnderline'
import handlerSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import handlerAnswerSA1 from '@/questions_middleware/handleAnswer/SA1'
import {
  requireCellQuestions,
  verifyValueLCS,
} from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../..'
const handleDataSA1 = async (
  data: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
) => {
  try {
    const dataResutl = { ...data }
    let hasError = false
    const arrMessage = []
    const arrCellRequire = [
      'question_description',
      'question_text',
      'audio_script',
      'skill',
      'level',
    ]
    const arrCellHasData = []
    const arrVerify = ['skill', 'level']

    // issystem
    const isSystem = user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
    if (isSystem) {
      arrCellRequire.push('cefr') //cefr
      arrVerify.push('cefr')
    } else if (data['cefr']) {
      arrVerify.push('cefr')
      arrCellHasData.push('cefr')
    }
    const checkCellRequire = requireCellQuestions(data, arrCellRequire)
    if (checkCellRequire.code == 0) {
      arrMessage.push(checkCellRequire.message)
      hasError = true
    }
    arrCellHasData.push(...checkCellRequire.arrCellHasData)
    //verify value
    for (let i = 0; i < arrVerify.length; i++) {
      const key = arrVerify[i]
      if (arrCellHasData.indexOf(key) !== -1) {
        const resultV =await verifyValueLCS(key, data[key])
        if (resultV.code == 0) {
          arrMessage.push(resultV.message)
          hasError = true
        }
      }
    }

    //validate data Question Text
    // let question_text = ''
    // const arr = data['question_text']
    // if (data[`question_text`] && data[`question_text`].length > 0) {
    //   for (const i in arr) {
    //     if (arr[i].underline === true) {
    //       arr[i].text = arr[i].text.replace(arr[i].text, '...')
    //     }
    //     question_text += arr[i].text
    //   }
    // }
    const question_text = handlerSpaceWord(
      convertTextCell(data[`question_text`]),
    )

    if (question_text && question_text.length > 0) {
      const countSpace = (question_text.match(/%s%/g) || []).length
      if (countSpace > 1) {
        hasError = true
        arrMessage.push(
          'Vui lòng chỉ nhập 1 chỗ trống ___ trong Question Text',
        )
      }
    }

    //validate audio_script
    let dataAudioScript = ''
    if (Array.isArray(data['audio_script'])) {
      dataAudioScript = handlerBoldUnderline(data['audio_script'])
    } else {
      dataAudioScript = data['audio_script']
    }
    //validate data Answer
    const handlerAnswer = handlerAnswerSA1(data)
    if (handlerAnswer.code === 0) {
      hasError = true
      arrMessage.push(handlerAnswer.message)
    }
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data['skill'].toUpperCase(),
        question_type: data['question_type'].toUpperCase(),
        level: data['level'].toUpperCase(),
        question_description: convertTextCell(data['question_description']),
        question_text: dataAudioScript,
        correct_answers: handlerAnswer.correct_answer,
        answers: question_text,
        total_question: 1,
      }
      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        dataResutl['success'] = true
      } else {
        dataResutl['success'] = false
        arrMessage.push(...resSaveDb.arr_message)
        dataResutl['arr_message'] = arrMessage
      }
    } else {
      dataResutl['success'] = false
      dataResutl['arr_message'] = arrMessage
    }
    return dataResutl
  } catch (error: any) {
    data['success'] = false
    data['arr_message'] = ['Có lỗi xảy ra']
    return data
  }
}

export default handleDataSA1
