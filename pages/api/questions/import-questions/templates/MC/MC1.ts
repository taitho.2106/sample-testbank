import { USER_ROLES } from '@/interfaces/constants'
import handlerSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import handleAnswerMC1_5 from '@/questions_middleware/handleAnswer/MC1_2_5'
import { requireCellQuestions } from '@/questions_middleware/verifyRow/checkCellRequired'
import { verifyValueLCS } from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../../../import-questions/index'
export default async function handleDataMC1(
  data: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
) {
  try {
    const dataResutl = { ...data }

    let hasError = false
    const arrMessage = []
    const arrCellRequire = [
      'question_description',
      'question_text',
      'skill',
      'level',
    ]

    const arrCellHasData = []
    let textAnswer = ''
    let textCorrectAnswer = ''

    const arrVerify = ['skill', 'level']

    // issystem
    const isSystem = user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
    if (isSystem) {
      arrCellRequire.push('cefr') //cefr
      arrVerify.push('cefr')
    } else if (data['cefr']) {
      arrVerify.push('cefr')
      arrCellHasData.push('cefr')
    }
    //require
    const checkCellRequire = requireCellQuestions(data, arrCellRequire)
    if (checkCellRequire.code == 0) {
      arrMessage.push(checkCellRequire.message)
      hasError = true
    }
    arrCellHasData.push(...checkCellRequire.arrCellHasData)
    //question text
    const questionTextSpace = handlerSpaceWord(
      convertTextCell(data['question_text']),
    )
    //verify value
    for (let i = 0; i < arrVerify.length; i++) {
      const key = arrVerify[i]
      if (arrCellHasData.indexOf(key) !== -1) {
        const resultV = await verifyValueLCS(key, data[key])
        if (resultV.code == 0) {
          arrMessage.push(resultV.message)
          hasError = true
        }
      }
    }

    //answer
    const checkAnswer = handleAnswerMC1_5(data)
    if (checkAnswer.code == 0) {
      arrMessage.push(checkAnswer.message)
      hasError = true
    } else {
      textAnswer = checkAnswer.data.answers
      textCorrectAnswer = checkAnswer.data.correctAnswers
    }
    //
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data['skill'].toUpperCase(),
        question_type: data['question_type'].toUpperCase(),
        level: data['level'].toUpperCase(),
        question_description: convertTextCell(data['question_description']),
        question_text: questionTextSpace,
        answers: textAnswer,
        correct_answers: textCorrectAnswer,
        total_question: 1,
      }
      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        dataResutl['success'] = true
      } else {
        dataResutl['success'] = false
        arrMessage.push(...resSaveDb.arr_message)
        dataResutl['arr_message'] = arrMessage
      }
    } else {
      dataResutl['success'] = false
      dataResutl['arr_message'] = arrMessage
    }
    return dataResutl
  } catch (err) {
    data['success'] = false
    data['arr_message'] = ['Có lỗi xảy ra']
    return data
  }
}
