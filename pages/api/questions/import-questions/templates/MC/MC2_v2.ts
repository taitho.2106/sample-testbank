import { USER_ROLES } from '@/interfaces/constants'
import convertFontStyle from '@/questions_middleware/convertFontStyle'
import convertSpaceWord from '@/questions_middleware/convertSpaceWord'
import handlerSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import copyFileQuestion from '@/questions_middleware/copyFileQuestion'
import handleAnswerMC1_2_5 from '@/questions_middleware/handleAnswer/MC1_2_5'
import { checkExistFile } from '@/questions_middleware/verifyRow/checkCellFormatFile'
import { requireCellQuestions } from '@/questions_middleware/verifyRow/checkCellRequired'
import { verifyValueLCS } from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../../index'

export default async function handleDataMC2(
  arrData: any[],
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
  folder: any,
) {
  try {
    // đếm khoảng trống trong câu hỏi
    // chỉ 1 hình ảnh
    //có câu ví dụ
    // câu ví dụ có * trước câu hỏi
    let hasError = false
    const { temp, destination } = folder
    const listResult: any = []
    const questionType = arrData[0]['question_type']
    let textImage = ''
    const arrAnswer = []
    const arrCorrectAnswer = []
    const arrQuestionText = []
    for (let index = 0; index < arrData.length; index++) {
      const data = arrData[index]

      const dataResutl = { ...data }
      const arrCellHasData = []

      const arrMessage = []
      let textAnswer = ''
      let textCorrectAnswer = ''

      const arrCellRequire = [
        // 'parent_question_description',
        'question_text',
      ]
      let arrVerify: any = ['skill', 'level']
      if (index == 0) {
        arrCellRequire.push('parent_question_description')
        arrCellRequire.push('skill')
        arrCellRequire.push('level')
        arrCellRequire.push('image')
        // issystem
        const isSystem =
          user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
        if (isSystem) {
          arrCellRequire.push('cefr') //cefr
          arrVerify.push('cefr')
        } else if (data['cefr']) {
          arrVerify.push('cefr')
          arrCellHasData.push('cefr')
        }
      } else {
        arrVerify = []
      }
      //require
      const checkCellRequire = requireCellQuestions(data, arrCellRequire)
      if (checkCellRequire.code == 0) {
        arrMessage.push(checkCellRequire.message)
        hasError = true
      }
      arrCellHasData.push(...checkCellRequire.arrCellHasData)

      //verify value
      for (let i = 0; i < arrVerify.length; i++) {
        const key = arrVerify[i]
        if (arrCellHasData.indexOf(key) !== -1) {
          const resultV = await verifyValueLCS(key, data[key])
          if (resultV.code == 0) {
            arrMessage.push(resultV.message)
            hasError = true
          }
        }
      }
      //question_text
      if (data['question_text']) {
        let questionTextSpace = convertSpaceWord(
          convertFontStyle(data['question_text']),
        )
        if (questionTextSpace) {
          if (questionTextSpace.toLowerCase().endsWith('|ex')) {
            questionTextSpace = questionTextSpace.slice(0, -3)
            if (questionTextSpace.length == 0) {
              arrMessage.push('Vui lòng nhập nội dung câu hỏi ví dụ')
              hasError = true
            } else {
              questionTextSpace = '*' + questionTextSpace
            }
          }
          arrQuestionText.push(questionTextSpace)
        }
      }
      //answer
      const checkAnswer = handleAnswerMC1_2_5(data)
      if (checkAnswer.code == 0) {
        arrMessage.push(checkAnswer.message)
        hasError = true
      } else {
        textAnswer = checkAnswer.data.answers
        textCorrectAnswer = checkAnswer.data.correctAnswers
      }
      //check image -- hasdata
      if (arrCellHasData.includes('image')) {
        const images = convertTextCell(data['image'])
        const listImage = images.split('|')
        const checkFile = checkExistFile(temp, listImage, 'image', [
          'png',
          'jpg',
          'jpeg',
        ])
        if (checkFile.code == 0) {
          arrMessage.push(checkFile.message)
          hasError = true
        }
        if (hasError == false) {
          copyFileQuestion(temp, destination, listImage)
          for (let i = 0; i < listImage.length; i++) {
            listImage[i] = `${destination}/${listImage[0]}`
          }
          textImage = listImage.join(`#`)
        }
      }
      arrCorrectAnswer.push(textCorrectAnswer)
      arrAnswer.push(textAnswer)
      dataResutl['arr_message'] = [...arrMessage]
      listResult.push({ ...dataResutl })
    }
    const data0 = arrData[0]
    if (hasError == false) {
      //sort example, check count example and question.
      const dataExampleSort = sortExample(
        arrQuestionText,
        arrAnswer,
        arrCorrectAnswer,
      )
      if (dataExampleSort.count_example > 1) {
        hasError = true
        listResult[0]['arr_message'].push('Chỉ cho phép tối đa 1 ví dụ')
      }
      if (dataExampleSort.count_question == 0) {
        hasError = true
        listResult[0]['arr_message'].push('Vui lòng nhập ít nhất 1 câu hỏi')
      }
      if (hasError == false) {
        const param = {
          publisher: publisher,
          test_type: test_type,
          series: series,
          grade: grade,
          cerf: (data0['cefr'] || 'NA').toUpperCase(),
          format: format,
          types: types,
          skills: data0['skill'].toUpperCase(),
          question_type: questionType.toUpperCase(),
          level: data0['level'].toUpperCase(),
          parent_question_description: convertTextCell(
            data0['parent_question_description'],
          ),
          question_text: dataExampleSort.question_text.join('#'),
          answers: dataExampleSort.answers.join('#'),
          correct_answers: dataExampleSort.correct_answers.join('#'),
          total_question: dataExampleSort.count_question,
          image: textImage,
        }
        const resSaveDb = await createQuestionUpload(param, user)
        if (resSaveDb.code == 1) {
          hasError = false
        } else {
          hasError = true
          listResult[0]['arr_message'].push(...resSaveDb.arr_message)
        }
      }
    }
    if (hasError == true) {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = false
      }
    } else {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = true
      }
    }
    return { success: !hasError, listResult: listResult }
  } catch (err) {
    return { success: false, listResult: arrData }
  }
}

type dataSort = {
  answers: string[]
  correct_answers: string[]
  question_text: string[]
  count_question: number
  count_example: number
}
const sortExample = (
  listQuestionText: any[],
  listAnswer: any[],
  listCorrect: any[],
) => {
  const data: dataSort = {
    answers: [],
    correct_answers: [],
    question_text: [],
    count_question: 0,
    count_example: 0,
  }
  const questionsExample = []
  const answersExample: string[] = []
  const answerCorrectsExample: string[] = []
  const questions = []
  const answers: string[] = []
  const answerCorrects: string[] = []
  for (let i = 0; i < listQuestionText.length; i++) {
    if (listQuestionText[i]?.startsWith('*')) {
      questionsExample.push(listQuestionText[i])
      answersExample.push(listAnswer[i])
      answerCorrectsExample.push(listCorrect[i])
      data.count_example += 1
    } else {
      questions.push(listQuestionText[i])
      answers.push(listAnswer[i])
      answerCorrects.push(listCorrect[i])
      data.count_question += 1
    }
  }
  data.answers = [...answersExample, ...answers]
  data.correct_answers = [...answerCorrectsExample, ...answerCorrects]
  data.question_text = [...questionsExample, ...questions]
  return data
}
