import { USER_ROLES } from '@/interfaces/constants'
import convertSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import copyFileQuestion from '@/questions_middleware/copyFileQuestion'
import handleAnswerMC3_6 from '@/questions_middleware/handleAnswer/MC3_6'
import { checkExistFile } from '@/questions_middleware/verifyRow/checkCellFormatFile'
import { requireCellQuestions } from '@/questions_middleware/verifyRow/checkCellRequired'
import { verifyValueLCS } from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../../index'

export default async function handleDataMC6(
  arrData: any[],
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
  folder: any,
) {
  try {
    let hasError = false
    const { temp, destination } = folder
    const listResult: any = []
    const questionType = arrData[0]['question_type']
    let textAudio = ''
    const arrAnswer = []
    const arrCorrectAnswer = []
    const arrQuestionText = []
    for (let index = 0; index < arrData.length; index++) {
      const data = arrData[index]

      const dataResutl = { ...data }
      const arrCellHasData = []

      const arrMessage = []
      let textAnswer = ''
      let textCorrectAnswer = ''

      const arrCellRequire = [
        // 'parent_question_description',
        'question_text',
      ]
      let arrVerify: any = ['skill', 'level']
      if (index == 0) {
        arrCellRequire.push('parent_question_description')
        arrCellRequire.push('skill')
        arrCellRequire.push('level')
        arrCellRequire.push('audio')
        // issystem
        const isSystem =
          user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
        if (isSystem) {
          arrCellRequire.push('cefr') //cefr
          arrVerify.push('cefr')
        } else if (data['cefr']) {
          arrVerify.push('cefr')
          arrCellHasData.push('cefr')
        }
      } else {
        arrVerify = []
      }
      //require
      const checkCellRequire = requireCellQuestions(data, arrCellRequire)
      if (checkCellRequire.code == 0) {
        arrMessage.push(checkCellRequire.message)
        hasError = true
      }
      arrCellHasData.push(...checkCellRequire.arrCellHasData)
      //check audio
      if (index == 0 && arrCellHasData.indexOf('audio') !== -1) {
        const audio = convertTextCell(data['audio'])
        const listAudio = audio.split('|')
        const checkFile = checkExistFile(temp, listAudio, 'audio', ['mp3'])
        if (checkFile.code == 0) {
          arrMessage.push(checkFile.message)
          hasError = true
        } else {
          copyFileQuestion(temp, destination, listAudio)
          for (let i = 0; i < listAudio.length; i++) {
            listAudio[i] = `${destination}/${listAudio[0]}`
          }
          textAudio = listAudio.join(`#`)
        }
      }
      //verify value
      for (let i = 0; i < arrVerify.length; i++) {
        const key = arrVerify[i]
        if (arrCellHasData.indexOf(key) !== -1) {
          const resultV = await verifyValueLCS(key, data[key])
          if (resultV.code == 0) {
            arrMessage.push(resultV.message)
            hasError = true
          }
        }
      }
      //question_text
      const questionTextSpace = convertSpaceWord(
        convertTextCell(data['question_text']),
      )
      arrQuestionText.push(questionTextSpace)
      //answer
      const checkAnswer = handleAnswerMC3_6(data)
      if (checkAnswer.code == 0) {
        arrMessage.push(checkAnswer.message)
        hasError = true
      } else {
        textAnswer = checkAnswer.data.answers
        textCorrectAnswer = checkAnswer.data.correctAnswers
      }
      arrCorrectAnswer.push(textCorrectAnswer)
      arrAnswer.push(textAnswer)
      dataResutl['arr_message'] = [...arrMessage]
      listResult.push({ ...dataResutl })
    }
    const data0 = arrData[0]
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data0['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data0['skill'].toUpperCase(),
        question_type: questionType.toUpperCase(),
        level: data0['level'].toUpperCase(),
        parent_question_description: convertTextCell(
          data0['parent_question_description'],
        ),
        question_text: arrQuestionText.join('#'),
        answers: arrAnswer.join('#'),
        correct_answers: arrCorrectAnswer.join('#'),
        total_question: arrData.length,
        audio: textAudio,
        audio_script: convertTextCell(data0['audio_script']),
      }
      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        hasError = false
      } else {
        hasError = true
        listResult[0]['arr_message'].push(...resSaveDb.arr_message)
      }
    }
    if (hasError == true) {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = false
      }
    } else {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = true
      }
    }
    return { success: !hasError, listResult: listResult }
  } catch (err) {
    return { success: false, listResult: arrData }
  }
}
