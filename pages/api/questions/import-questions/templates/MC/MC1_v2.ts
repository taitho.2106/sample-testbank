import { USER_ROLES } from '@/interfaces/constants'
import convertFontStyle from '@/questions_middleware/convertFontStyle'
import convertSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import handleAnswerMC1_2_5 from '@/questions_middleware/handleAnswer/MC1_2_5'
import { requireCellQuestions } from '@/questions_middleware/verifyRow/checkCellRequired'
import { verifyValueLCS } from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../../index'

export default async function handleDataMC1(
  arrData: any[],
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
) {
  try {
    let hasError = false
    const listResult: any = []
    const questionType = arrData[0]['question_type']
    const arrAnswer = []
    const arrCorrectAnswer = []
    const arrQuestionText = []
    for (let index = 0; index < arrData.length; index++) {
      const data = arrData[index]

      const dataResutl = { ...data }
      const arrCellHasData = []

      const arrMessage = []
      let textAnswer = ''
      let textCorrectAnswer = ''
      const arrCellRequire = [
        // 'parent_question_description',
        // 'question_text',
      ]
      let arrVerify = ['skill', 'level']

      if (index == 0) {
        arrCellRequire.push('question_description')
        arrCellRequire.push('skill')
        arrCellRequire.push('level')
        // issystem
        const isSystem =
          user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
        if (isSystem) {
          arrCellRequire.push('cefr') //cefr
          arrVerify.push('cefr')
        } else if (data['cefr']) {
          arrVerify.push('cefr')
          arrCellHasData.push('cefr')
        }
      } else {
        arrVerify = []
      }
      //require
      const checkCellRequire = requireCellQuestions(data, arrCellRequire)
      if (checkCellRequire.code == 0) {
        arrMessage.push(checkCellRequire.message)
        hasError = true
      }
      arrCellHasData.push(...checkCellRequire.arrCellHasData)

      //verify value
      for (let i = 0; i < arrVerify.length; i++) {
        const key = arrVerify[i]
        if (arrCellHasData.indexOf(key) !== -1) {
          const resultV = await verifyValueLCS(key, data[key])
          if (resultV.code == 0) {
            arrMessage.push(resultV.message)
            hasError = true
          }
        }
      }

      //question_text
      if(data['question_text']){
        const questionTextSpace = convertSpaceWord(
          convertFontStyle(data['question_text']),
        )
        arrQuestionText.push(questionTextSpace)
      }else{
        arrQuestionText.push(" ")
      }
     
      //answer
      const checkAnswer = handleAnswerMC1_2_5(data)
      if (checkAnswer.code == 0) {
        arrMessage.push(checkAnswer.message)
        hasError = true
      } else {
        textAnswer = checkAnswer.data.answers
        textCorrectAnswer = checkAnswer.data.correctAnswers
      }
      //
      arrCorrectAnswer.push(textCorrectAnswer)
      arrAnswer.push(textAnswer)
      dataResutl['arr_message'] = [...arrMessage]
      listResult.push({ ...dataResutl })
    }
    const data0 = arrData[0]
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data0['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data0['skill'].toUpperCase(),
        question_type: questionType.toUpperCase(),
        level: data0['level'].toUpperCase(),
        question_description: convertTextCell(
          data0['question_description'],
        ),
        parent_question_text: convertSpaceWord(
            convertFontStyle(data0['parent_question_text']),
        ),
        question_text: arrQuestionText.join('#'),
        answers: arrAnswer.join('#'),
        correct_answers: arrCorrectAnswer.join('#'),
        total_question: arrData.length,
      }

      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        hasError = false
      } else {
        hasError = true
        listResult[0]['arr_message'].push(...resSaveDb.arr_message)
      }
    }

    if (hasError == true) {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = false
      }
    } else {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = true
      }
    }
    return { success: !hasError, listResult: listResult }
  } catch (err) {
    return { success: false, listResult: arrData }
  }
}
