import { USER_ROLES } from '@/interfaces/constants'
import handlerSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import handlerAnswerTF12 from '@/questions_middleware/handleAnswer/TF1_2'
import {
  requireCellQuestions,
  verifyValueLCS,
} from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../..'

const handleDataTF2 = async (
  data: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
) => {
  try {
    const dataResutl = { ...data }
    let hasError = false
    const arrMessage = []
    const arrCellRequire = [
      'question_description',
      'question_text',
      'skill',
      'level',
    ]
    const arrCellHasData = []
    const arrVerify = ['skill', 'level']

    // issystem
    const isSystem = user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
    if (isSystem) {
      arrCellRequire.push('cefr') //cefr
      arrVerify.push('cefr')
    } else if (data['cefr']) {
      arrVerify.push('cefr')
      arrCellHasData.push('cefr')
    }

    const checkCellRequire = requireCellQuestions(data, arrCellRequire)
    if (checkCellRequire.code == 0) {
      arrMessage.push(checkCellRequire.message)
      hasError = true
    }
    arrCellHasData.push(...checkCellRequire.arrCellHasData)
    //verify value
    for (let i = 0; i < arrVerify.length; i++) {
      const key = arrVerify[i]
      if (arrCellHasData.indexOf(key) !== -1) {
        const resultV =await await verifyValueLCS(key, data[key])
        if (resultV.code == 0) {
          arrMessage.push(resultV.message)
          hasError = true
        }
      }
    }
    //validate question text
    const question_text = handlerSpaceWord(
      convertTextCell(data[`question_text`]),
    )
    //validate answer
    const handlerAnswer = handlerAnswerTF12(data)
    if (handlerAnswer.code === 0) {
      hasError = true
      arrMessage.push(handlerAnswer.message)
    }
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data['skill'].toUpperCase(),
        question_type: data['question_type'].toUpperCase(),
        level: data['level'].toUpperCase(),
        question_text: question_text,
        question_description: convertTextCell(data['question_description']),
        answers: handlerAnswer.answer,
        correct_answers: handlerAnswer.correct_answer,
        total_question: handlerAnswer.length_corret_answer,
        audio_script: convertTextCell(data['audio_script']),
      }
      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        dataResutl['success'] = true
      } else {
        dataResutl['success'] = false
        arrMessage.push(...resSaveDb.arr_message)
        dataResutl['arr_message'] = arrMessage
      }
    } else {
      dataResutl['success'] = false
      dataResutl['arr_message'] = arrMessage
    }
    return dataResutl
  } catch (error: any) {
    data['success'] = false
    data['arr_message'] = ['Có lỗi xảy ra']
    return data
  }
}

export default handleDataTF2
