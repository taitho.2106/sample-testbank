import { USER_ROLES } from '@/interfaces/constants'
import convertTextCell from '@/questions_middleware/convertTextCell'
import copyFileQuestion from '@/questions_middleware/copyFileQuestion'
import handleAnswerMR2 from '@/questions_middleware/handleAnswer/MR1_2_3'
import { checkExistFile } from '@/questions_middleware/verifyRow/checkCellFormatFile'
import { requireCellQuestions, verifyValueLCS } from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../../../import-questions/index'
export default async function handleDataMR2(
  data: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
  folder: any,
) {
  try {
    const dataResult = { ...data }
    const { temp, destination } = folder
    let hasError = false
    let textAudio = ''
    const totalQuestion = 1
    const arrMessage = []
    const arrCellRequire = ['question_description', 'skill', 'level', 'audio']

    const arrCellHasData = []
    const arrVerify = ['skill', 'level']
    let textAnswer = ''
    let textCorrectAnswer = ''

    // issystem
    const isSystem = user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
    if (isSystem) {
      arrCellRequire.push('cefr') //cefr
      arrVerify.push('cefr')
    } else if (data['cefr']) {
      arrVerify.push('cefr')
      arrCellHasData.push('cefr')
    }
    //require
    const checkCellRequire = requireCellQuestions(data, arrCellRequire)
    if (checkCellRequire.code == 0) {
      arrMessage.push(checkCellRequire.message)
      hasError = true
    }
    arrCellHasData.push(...checkCellRequire.arrCellHasData)

    //verify value
    for (let i = 0; i < arrVerify.length; i++) {
      const key = arrVerify[i]
      if (arrCellHasData.indexOf(key) !== -1) {
        const resultV =await verifyValueLCS(key, data[key])
        if (resultV.code == 0) {
          arrMessage.push(resultV.message)
          hasError = true
        }
      }
    }
    if (arrCellHasData.indexOf('audio') !== -1) {
      const audio = convertTextCell(data['audio'])
      const checkFile = checkExistFile(temp, [audio], 'audio', ['mp3'])
      if (checkFile.code == 0) {
        arrMessage.push(checkFile.message)
        hasError = true
      } else {
        copyFileQuestion(temp, destination, [audio])
        textAudio = `${destination}/${audio}`
      }
    }
    //answer
    const checkAnswer = handleAnswerMR2(data)
    if (checkAnswer.code == 0) {
      arrMessage.push(checkAnswer.message)
      hasError = true
    } else {
      textAnswer = checkAnswer.data.answers
      textCorrectAnswer = checkAnswer.data.correctAnswers
      // totalQuestion = checkAnswer.data.countCorrectAnswer
    }
    //
    if (hasError == false) {
      const param = {
        publisher: publisher,
        test_type: test_type,
        series: series,
        grade: grade,
        cerf: (data['cefr'] || 'NA').toUpperCase(),
        format: format,
        types: types,
        skills: data['skill'].toUpperCase(),
        question_type: data['question_type'].toUpperCase(),
        level: data['level'].toUpperCase(),
        question_description: convertTextCell(data['question_description']),
        answers: textAnswer,
        correct_answers: textCorrectAnswer,
        total_question: totalQuestion,
        audio: textAudio,
        audio_script: convertTextCell(data['audio_script'])
      }
      const resSaveDb = await createQuestionUpload(param, user)
      if (resSaveDb.code == 1) {
        dataResult['success'] = true
      } else {
        dataResult['success'] = false
        arrMessage.push(...resSaveDb.arr_message)
        dataResult['arr_message'] = arrMessage
      }
    } else {
      dataResult['success'] = false
      dataResult['arr_message'] = arrMessage
    }
    return dataResult
  } catch {
    data['success'] = false
    data['arr_message'] = ['Có lỗi xảy ra']
    return data
  }
}
