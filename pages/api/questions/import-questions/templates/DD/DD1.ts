import { USER_ROLES } from '@/interfaces/constants'
import convertFontStyle from '@/questions_middleware/convertFontStyle'
import convertSpaceWord from '@/questions_middleware/convertSpaceWord'
import convertTextCell from '@/questions_middleware/convertTextCell'
import copyFileQuestion from '@/questions_middleware/copyFileQuestion'
import handleAnswerDD1 from '@/questions_middleware/handleAnswer/DD1'
import { checkExistFile } from '@/questions_middleware/verifyRow/checkCellFormatFile'
import { requireCellQuestions } from '@/questions_middleware/verifyRow/checkCellRequired'
import { verifyValueLCS } from '@/questions_middleware/verifyRow/checkCellRequired'

import { createQuestionUpload } from '../../../import-questions/index'

export default async function handleDataDD1(
  arrData: any,
  user: any,
  format: string,
  grade: string,
  publisher: string,
  series: string,
  test_type: string,
  types: string,
  folder: any,
) {
  //
  try {
    let hasError = false
    const listResult: any = []
    const questionType = arrData[0]['question_type']
    const { temp, destination } = folder

    const arrAnswer = []
    const arrCorrectAnswer = []
    const arrExamIndex = [] // save index of example
    const arrQuesIndex = [] // save index of normal question
    const arrQuestionText = []
    const arrImage = []
    let textImage = ''
    let textExam = '' // save example/ answer order
    let listAns = ''
    let listCorrect = ''

    const arrAnsEx = []
    const arrCorrectAnsEx = []

    const arrAns = []
    const arrCorrectAns = []

    for (let index = 0; index < arrData.length; index++) {
      const data = arrData[index]
      const dataResutl = { ...data }
      const arrCellHasData = []
      const arrMessage = []
      let textAnswer = ''
      let textCorrectAnswer = ''
      const arrCellRequire = []
      let arrVerify = ['skill', 'level']

      if (index == 0) {
        arrCellRequire.push('parent_question_description')
        arrCellRequire.push('skill')
        arrCellRequire.push('level')
        // is system
        const isSystem =
          user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator
        if (isSystem) {
          arrCellRequire.push('cefr') //cefr
          arrVerify.push('cefr')
        } else if (data['cefr']) {
          arrVerify.push('cefr')
          arrCellHasData.push('cefr')
        }
      } else {
        arrVerify = []
      }

      //require
      const checkCellRequire = requireCellQuestions(data, arrCellRequire)
      if (checkCellRequire.code == 0) {
        arrMessage.push(checkCellRequire.message)
        hasError = true
      }
      arrCellHasData.push(...checkCellRequire.arrCellHasData)

      // verify value
      for (let i = 0; i < arrVerify.length; i++) {
        const key = arrVerify[i]
        if (arrCellHasData.indexOf(key) !== -1) {
          const resultV = await verifyValueLCS(key, data[key])
          if (resultV.code == 0) {
            arrMessage.push(resultV.message)
            hasError = true
          }
        }
      }

      //check exam status via question_text
      const question_text = convertTextCell(data['question_text'])
      // if(question_text){
      //   arrQuestionText.push('*')
      // }
      // else if(!question_text){
      //   arrQuestionText.push("")
      // }
      if (question_text && question_text.toLowerCase() === 'ex') {
        arrExamIndex.push(index)
      } else {
        arrQuesIndex.push(index)
      }

      // validate answer
      const checkAnswer = handleAnswerDD1(data)
      if (checkAnswer.code == 0) {
        arrMessage.push(checkAnswer.message)
        hasError = true
      } else {
        textAnswer = checkAnswer.data.answers
        textCorrectAnswer = checkAnswer.data.correctAnswers
      }

      arrCorrectAnswer.push(textCorrectAnswer)
      arrAnswer.push(textAnswer)
      dataResutl['arr_message'] = [...arrMessage]
      listResult.push({ ...dataResutl })

      //check image
      const image = convertTextCell(data['image'])
      if (!image) {
        arrImage.push('')
      } else if (image) {
        if (checkAnswer.code == 1) {
          //check image vs answer
          const checkFile = checkExistFile(temp, [image], 'image', [
            'png',
            'jpg',
            'jpeg',
          ])
          if (checkFile.code == 0) {
            arrMessage.push(checkFile.message)
            hasError = true
          } else {
            copyFileQuestion(temp, destination, [image])
            arrImage.push(`${destination}/${image}`)
          }
        }
      }
    }

    const data0 = arrData[0]

    if (hasError == false) {
      const arr_image = []
      const arr_image_ex = []
      const qtExam = []
      const qt = []

      for (let imgIndex = 0; imgIndex < arrImage.length; imgIndex++) {
        if (arrExamIndex.includes(imgIndex)) {
          arr_image_ex.push(arrImage[imgIndex])
        } else arr_image.push(arrImage[imgIndex])
      }

      // separate example and answer-question
      for (let ansIndex = 0; ansIndex < arrAnswer.length; ansIndex++) {
        if (arrExamIndex.includes(ansIndex)) {
          arrAnsEx.push(arrAnswer[ansIndex])
          arrCorrectAnsEx.push(arrCorrectAnswer[ansIndex])
          qtExam.push('*')
        } else {
          arrAns.push(arrAnswer[ansIndex])
          arrCorrectAns.push(arrCorrectAnswer[ansIndex])
          qt.push('')
        }
      }

      //check pass data
      if (arrAns.length == 0) {
        hasError = true
        listResult[0]['arr_message'].push('Vui lòng nhập ít nhất 1 câu hỏi')
      }
      if (arrAnsEx.length > 3) {
        hasError = true
        listResult[0]['arr_message'].push('Chỉ cho phép tối đa 3 ví dụ')
      }
      if (hasError == false) {
        listAns = [...arrAnsEx, ...arrAns].join('#')
        listCorrect = [...arrCorrectAnsEx, ...arrCorrectAns].join('#')
        textImage = [...arr_image_ex, ...arr_image].join('#')
        textExam = [...qtExam, ...qt].join('#')

        const param = {
          publisher: publisher,
          test_type: test_type,
          series: series,
          grade: grade,
          cerf: (data0['cefr'] || 'NA').toUpperCase(),
          format: format,
          types: types,
          skills: data0['skill'].toUpperCase(),
          question_type: questionType.toUpperCase(),
          level: data0['level'].toUpperCase(),
          parent_question_description: convertTextCell(
            data0['parent_question_description'],
          ),
          question_text: textExam,
          answers: listAns,
          correct_answers: listCorrect,
          total_question: arrAns.length,
          image: textImage,
        }
        const resSaveDb = await createQuestionUpload(param, user)
        if (resSaveDb.code == 1) {
          hasError = false
        } else {
          hasError = true
          listResult[0]['arr_message'].push(...resSaveDb.arr_message)
        }
      }
    }
    if (hasError == true) {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = false
      }
    } else {
      for (let i = 0; i < listResult.length; i++) {
        listResult[i]['success'] = true
      }
    }

    return {
      success: !hasError,
      listResult: listResult,
    }
  } catch (error: any) {
    return {
      success: false,
      listResult: arrData,
    }
  }
}
