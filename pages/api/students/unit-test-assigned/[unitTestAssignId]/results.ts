import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";
import { dayjs } from "@/utils/date";
import { mapClassUnitTestToDTO } from "dto/unitTest";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            unitTestAssignId: yup.number(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;
            const { unitTestAssignId } = req.query;

            const classUnitTest = await prisma.classUnitTest.findUnique({
                where: {
                    id: +unitTestAssignId,
                },
                select: {
                    class_id: true,
                    class_group_id: true,
                    student_id: true,
                    start_date: true,
                    end_date: true,
                    unit_type: true,
                    unitTest: {
                        select: {
                            id: true,
                            name: true,
                            unit_type: true,
                            total_question: true,
                            time: true,
                            deleted: true,
                        }
                    },
                }
            });

            if (!classUnitTest) {
                return res.status(400).json(errorResponseDTO(400, 'Unit test assign not found!'));
            }

            const classStudent = await prisma.classStudent.findFirst({
                where: {
                    student_id: student.id,
                    class_id: classUnitTest.class_id,
                },
                select: {
                    status: true,
                    class_group_id: true,
                    created_by: true,
                }
            })

            const result: any = await prisma.unit_test_result.findMany({
                where: {
                    class_unit_test_id: +unitTestAssignId,
                    user_id: student.id,
                },
                orderBy: {
                    created_date: 'desc'
                }
            });

            return res.status(200).json(apiResponseDTO(true, {
                content: result,
                classUnitTest: mapClassUnitTestToDTO(classUnitTest, dayjs()),
                classStudent,
            }));
        }
    },
});

export default handler