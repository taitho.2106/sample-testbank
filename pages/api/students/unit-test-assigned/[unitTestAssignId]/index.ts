import createHandler, { apiResponseDTO, errorResponseDTO } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import { USER_ROLES } from "@/interfaces/constants";
import prisma from 'lib/prisma';
import { commonDeleteStatus, commonStatus } from "@/constants/index";
import { mapClassUnitTestToDTO } from "dto/unitTest";
import { dayjs } from "@/utils/date";
import { checkStudentAccessUnitTestAssigned } from "services/unitTest";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            unitTestAssignId: yup.number().required(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;
            const { unitTestAssignId } = req.query;

            if (student.user_role_id !== USER_ROLES.Student) {
                return res.status(400).json(errorResponseDTO(400, 'User not student!'));
            }

            const unitTestAssign: any = await prisma.classUnitTest.findUnique({
                where: {
                    id: +unitTestAssignId,
                },
                select: {
                    id: true,
                    unitTest: true,
                    class_id: true,
                    class_group_id: true,
                    student_id: true,
                    start_date: true,
                    end_date: true,
                    unit_type: true,
                    class: true,
                }
            })

            if (!unitTestAssign) {
                return res.status(404).json(errorResponseDTO(404, 'Unit test not found'))
            }

            if (!(await checkStudentAccessUnitTestAssigned(student.id, unitTestAssign))) {
                return res.status(400).json(errorResponseDTO(400, 'Unit test not assign for student'));
            }

            unitTestAssign.lastSubmitted = await prisma.unit_test_result.findFirst({
                where: {
                    user_id: student.id,
                    class_unit_test_id: unitTestAssign.id,
                },
                select: {
                    id: true,
                    unit_test_id: true,
                    created_date: true,
                },
                orderBy: {
                    created_date: 'desc',
                }
            })

            return res.json(apiResponseDTO(true, mapClassUnitTestToDTO(unitTestAssign, dayjs())));
        }
    },
});

export default handler