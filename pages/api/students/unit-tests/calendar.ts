import * as yup from 'yup';
import createHandler, { apiResponseDTO } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";
import prisma from 'lib/prisma';
import { dayjs } from '@/utils/date';
import { classUnitTestSelect } from 'dto/entitiesSelect';
import { commonDeleteStatus, commonStatus } from '@/constants/index';
import { mapListClassUnitTestToDTO } from 'dto/unitTest';

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            year: yup.number().required(),
            month: yup.number().min(1).max(12).required(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;
            const { year, month } = req.query;
            const filterMonth = dayjs(`${year}-${month}-01`);

            const classStudents = await prisma.classStudent.findMany({
                where: {
                    student_id: student.id,
                    status: commonStatus.ACTIVE,
                    classes: {
                        deleted: commonDeleteStatus.ACTIVE,
                    }
                }
            });
            const classIds = classStudents.map(el => el.class_id);
            const groupIds = classStudents.map(el => el.class_group_id).filter(Boolean);
            const start_date = {
                gte: filterMonth.startOf('M').toDate(),
                lte: filterMonth.endOf('M').toDate(),
            };

            const classUnitTestIds = (await prisma.unit_test_result.findMany({
                where: {
                    unitTestAssign: { start_date },
                    user_id: student.id,
                },
                select: {
                    class_unit_test_id: true,
                }
            })).map(el => el.class_unit_test_id)

            const result = await prisma.classUnitTest.findMany({
                where: {
                    start_date,
                    class_id: { in: classIds },
                    OR: [
                        { class_group_id: { in: groupIds } },
                        { student_id: student.id },
                        {
                            student_id: null,
                            class_group_id: null,
                        },
                        { id: { in: classUnitTestIds } }
                    ]
                },
                select: {
                    ...classUnitTestSelect,
                    class: {
                        select: {
                            id: true,
                            name: true,
                            owner: {
                                select: {
                                    full_name: true,
                                }
                            }
                        }
                    }
                },
                orderBy: { start_date: 'asc' }
            });

            return res.status(200).json(apiResponseDTO(true, mapListClassUnitTestToDTO(result)));
        }
    },
});

export default handler