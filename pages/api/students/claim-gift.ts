import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import { SALE_ITEM_TYPE, unitTestGiftByGrade } from "@/constants/index";
import createHandler, { apiResponseDTO, errorResponseDTO, roleConfig } from "lib/apiHandle";
import prisma from "lib/prisma";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [ roleConfig.student ],
        querySchema: yup.object().shape({
            grade: yup.string().required(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;
            const { grade } = req.query;
            const userInfo = await prisma.user.findUnique({ where: { id: student.id } });

            if (userInfo.is_received_gift === 1) {
                return res.status(400).json(errorResponseDTO(400, 'Already received gift!'));
            }

            const unitTest: any = unitTestGiftByGrade[grade as keyof Object];
            if (!unitTest?.length) {
                return res.status(400).json(errorResponseDTO(400, 'Grade invalid!'));
            }

            try {
                const currentDate = new Date();
                await prisma.$transaction([
                    prisma.user.update({
                        where: { id: student.id },
                        data: { is_received_gift: 1 }
                    }),
                    prisma.user_asset.createMany({
                        data: unitTest.map((unitTestId: number) => ({
                            user_id: student.id,
                            asset_id: unitTestId + '',
                            asset_type: SALE_ITEM_TYPE.UNIT_TEST,
                            created_date: currentDate,
                            updated_date: currentDate,
                            start_date: currentDate,
                            quantity: 1,
                        })),
                    })
                ]);

                return res.status(200).json(apiResponseDTO(true, null));
            } catch (error) {
                return res.status(500).json(errorResponseDTO(500, 'Server error!'));
            }
        }
    },
});

export default handler