import { NextApiRequest, NextApiResponse } from "next";

import createHandler, { apiResponseDTO, roleConfig } from "lib/apiHandle";
import { getStudentComboUnitTest } from "services/comboUnitTest";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [ roleConfig.student ],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;

            const currentCombo = await getStudentComboUnitTest(student.id);

            return res.status(200).json(apiResponseDTO(true, currentCombo))
        }
    },
});

export default handler