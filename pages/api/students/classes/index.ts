import { commonDeleteStatus, commonStatus } from "@/constants/index";
import createHandler, { apiResponseDTO, roleConfig } from "lib/apiHandle";
import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [ roleConfig.student ],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;

            const classes = await prisma.classStudent.findMany({
                where: {
                    student_id: student.id,
                    classes: {
                        deleted: commonDeleteStatus.ACTIVE,
                    }
                },
                select: {
                    classes: {
                        select: {
                            id: true,
                            name: true,
                            owner: {
                                select: {
                                    full_name: true,
                                    phone: true,
                                }
                            }
                        }
                    }
                },
                orderBy: {
                    created_date: 'desc',
                }
            })

            return res.json(apiResponseDTO(true, classes.map(({ classes }) => ({
                id: classes.id,
                name: classes.name,
                teacher: classes.owner,
            }))));
        }
    },
});

export default handler