import { mapListClassUnitTestToDTO } from "dto/unitTest";
import createHandler, { apiResponseDTO, errorResponseDTO, roleConfig } from "lib/apiHandle";
import { NextApiRequest, NextApiResponse } from "next";
import { getStudentUnitTestAvailable } from "services/unitTest";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [roleConfig.student],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;
            const { classId } = req.query;

            const unitTest: any = await getStudentUnitTestAvailable(+classId, student.id);

            if (!unitTest) {
                return res.status(403).json(errorResponseDTO(403, 'Forbidden'));
            }

            return res.json(apiResponseDTO(true, mapListClassUnitTestToDTO(unitTest.map((el: any) => ({
                ...el,
                unit_test_result: el.unit_test_result[0] ?? null,
            })))));
        }
    },
});

export default handler