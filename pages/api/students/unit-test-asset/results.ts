import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import createHandler, { apiResponseDTO } from "lib/apiHandle";
import prisma from "lib/prisma";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            unitTestId: yup.number(),
        }),
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            const student: any = session.user;
            const { unitTestId } = req.query;

            const result: any = await prisma.unit_test_result.findMany({
                where: {
                    unit_test_id: +unitTestId,
                    user_id: student.id,
                },
                orderBy: {
                    created_date: 'desc'
                }
            });
            const unitTest = await prisma.unit_test.findUnique({ where: { id: +unitTestId } })

            return res.status(200).json(apiResponseDTO(true, {
                content: result,
                unitTest,
            }));
        }
    },
});

export default handler