const startNo: string[] = [];
for (let i = 1; i < 1000; i++) {
  startNo.push(i+".");
}
const handlePdfMC1 = (dataPages: any[], body:any) => {
    const listItem: any = [];
    for (let i = 0; i < dataPages.length; i++) {
      const pageRaw = dataPages[i];
      const pageWord = [];
      const constContentPage = dataPages[i].content;
      constContentPage.sort(function (a: any, b: any) {
        return a.x - b.x;
      });
      constContentPage.sort(function (a: any, b: any) {
        if ((a.y - b.y) * (a.y - b.y) < 2) {
          return 0;
        } else {
          return a.y - b.y;
        }
      });
      let lastY;
      for (let j = 0; j < constContentPage.length; j++) {
        const item = constContentPage[j];
        if ((lastY >= item.y - 2 && lastY <= item.y + 2) || !lastY) {
          pageWord.push(constContentPage[j].str);
          listItem.push({ ...item});
        } else {
          listItem.push({ ...item});
          pageWord.push("" + constContentPage[j].str);
        }
        lastY = item.y;
      }
    }
    // return result
    return splitQuestion(listItem,body);
  };
  

  const splitQuestion = (listItem: any[],body:any) => {
    let isStartQuestion = false;
    const listQuestion = [];
    const{question_type, level, cefr, skill} = body
    let items: any = [];
    for (let i = 0; i < listItem.length; i++) {
      const checkKeyNo = startNo.find((no) =>
        listItem[i].str.startsWith(no)
      );
      if (checkKeyNo) {
  
        let indexCheckno = listItem[i].str.indexOf(checkKeyNo);
        indexCheckno += checkKeyNo.length-1
        while(indexCheckno< listItem[i].str.length){
          if(listItem[i].str[indexCheckno] == ""){
            indexCheckno ++;
          }else{
            break;
          }
        }
  
       listItem[i].str ="@@@"+ listItem[i].str.substring(indexCheckno, listItem[i].str.length-1)
        // if(isStartQuestion){
        //   questions.push(question)
        // }
        listQuestion.push(items);
  
        isStartQuestion = true;
  
        items = [];
        items.push(listItem[i]);
        if (i == listItem.length - 1) {
          listQuestion.push(items);
        }
      } else if (i == listItem.length - 1) {
        listQuestion.push(items);
      } else {
        items.push(listItem[i]);
      }
    }
    
   
    let question_description = "";
    const listQuestionResult:any = []
    let indexQuestion = 0;
    listQuestion.map(el=> handlerQuestion(el)).forEach(m=>{
      const question:any = {}
      if(typeof m =="string"){
        const indexQuestionDes = m.toLowerCase().indexOf(body.key || "choose the")
        if(indexQuestionDes!==-1){
          question_description = m.substring(indexQuestionDes, m.length)
        }
      }else{
        if(indexQuestion==0){
          question.question_type = question_type;
        question.cefr = cefr;
        question.skill = skill;
        question.level = level;
        question.question_description = question_description;
        }
        question.question_text = m.textQuestion;
        for(let i =0; i<m.arrAnswer.length; i++){
          question[`answer_${i+1}`] = m.arrAnswer[i];
        }
        listQuestionResult.push(question)
        indexQuestion +=1;
      }
    })
    return listQuestionResult;
  };
  const handlerQuestion = (arr: any[]) => {
    //  console.log("arr---", arr)
    const check = arr[0].str.startsWith("@@@")
    if (!check) {
      return arr.map(el=> el.str).join(" ");
    }
    const arrAnswer = [];
    let startText = false;
    let textQuestion = "";
    const arrStartAnswer = ["A.", "B.", "C.", "D.", "E.", "F."];
    let endTextQuestion = false;
    let textAnswer = "";
    for (let i = 1; i < arr.length; i++) {
      let text = arr[i].str;
      if (startText && i>1 && !endTextQuestion) {
        if (text.length > 0 && text.trim().length == 0 && arr[i].width>=3 ) { //space width >=3
          text = " __ ";
        }
      }
      if(!startText){
        startText = true;
        continue;
      }
      startText = true;
      
      const checkKeyAnswer = arrStartAnswer.find((key) =>
      text.startsWith(key)
      );
      
      if (checkKeyAnswer) {
        text=text.substring(2,text.length).trim()
        endTextQuestion = true;
        if (textAnswer != "") {
          arrAnswer.push(textAnswer);
         textAnswer = "";
        }
        textAnswer += text;
  
        if(i == arr.length-1){
          arrAnswer.push(textAnswer);
        }
      } else {
        if(endTextQuestion && textAnswer.length>0){
          textAnswer += text;
          if(i ==arr.length-1){
            arrAnswer.push(textAnswer);
          }
        }
      }
      if(!endTextQuestion){
        textQuestion += text;
      }
    }
    return {
      textQuestion,
      arrAnswer
    }
  };
  export default handlePdfMC1