import { IncomingForm } from "formidable";
import { NextApiRequest, NextApiResponse } from "next";
import { PDFExtract, PDFExtractOptions } from "pdf.js-extract";

import handlePdfMC1 from './MC/MC1'
import handlePdfTF2 from "./TF/TF2"

export const config = {
  api: {
    bodyParser: false,
  },
}
const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    try {
      const result = await handleRequest1(req, res);
      res.json(result);
    } catch (err) {
      res.status(500).json(err);
    }
    
  };

  const handleRequest1 = (req: NextApiRequest, res: NextApiResponse) => {
    return new Promise((resolve, reject) => {
      const form = new IncomingForm();
      form.parse(req, async (err: any, fields: any, files: any) => {
        try {
          const file = files.file;
          const body = fields
          //    console.log(file)
          // const data = fs.createReadStream(file.filepath)
          const pdfExtract = new PDFExtract();
          const options: PDFExtractOptions = {};
          pdfExtract.extract(file.filepath, options)
            .then((data:any) => {
              // resolve(data.pages)
              switch(body.question_type){
                case "MC1":
                    const pageData = handlePdfMC1(data.pages,body);
                    resolve({code:1, data:pageData});
                    break;
                default:
                    resolve({code:0, message:"Loại câu hỏi này sẽ được cập nhật trong thời gian tới"});
              }
              
            })
            .catch((err) => console.log("err-------", err));
        } catch (ex: any) {
          console.log("ex=====", ex);
          reject(ex);
        }
      });
    });
  };
  export default handler;