const startNo: string[] = [];
for (let i = 1; i < 1000; i++) {
  startNo.push(i+".");
}
const arrStartAnswer = ["A.", "B.", "C.", "D.", "E.", "F."];
export default function handlePdfTF2 (dataPages: any[], body:any) {
    const listItem: any = [];
    for (let i = 2; i < dataPages.length; i++) {
      const constContentPage = dataPages[i].content;
      constContentPage.sort(function (a: any, b: any) {
        return a.x - b.x;
      });
      constContentPage.sort(function (a: any, b: any) {
        if ((a.y - b.y) * (a.y - b.y) < 2) {
          return 0;
        } else {
          return a.y - b.y;
        }
      });
      let lastY;
      for (let j = 0; j < constContentPage.length; j++) {
        const item = constContentPage[j];
        if ((lastY >= item.y - 2 && lastY <= item.y + 2) || !lastY) { // cungf dong
          listItem.push({ ...item});
        } else {
            listItem.push({ ...item});
        }
        lastY = item.y;
      }
    }
    return splitQuestion(listItem,body);
  };
  

  const splitQuestion = (listItem: any[],body:any) => {
    let isStartQuestion = false;
    const listQuestion = [];
    const{question_type, level, cefr, skill, key} = body
    let items: any = [];
    for (let i = 0; i < listItem.length; i++) {
      const checkKeyNo = startNo.find((no) =>
        listItem[i].str.startsWith(no)
      );
      if (checkKeyNo) {
  
        let indexCheckno = listItem[i].str.indexOf(checkKeyNo);
        indexCheckno += checkKeyNo.length-1
        while(indexCheckno< listItem[i].str.length){
          if(listItem[i].str[indexCheckno] == ""){
            indexCheckno ++;
          }else{
            break;
          }
        }
  
       listItem[i].str ="@@@"+ listItem[i].str.substring(indexCheckno, listItem[i].str.length-1)
        // if(isStartQuestion){
        //   questions.push(question)
        // }
        listQuestion.push(items);
  
        isStartQuestion = true;
  
        items = [];
        items.push(listItem[i]);
        if (i == listItem.length - 1) {
          listQuestion.push(items);
        }
      } else if (i == listItem.length - 1) {
        listQuestion.push(items);
      } else {
        items.push(listItem[i]);
      }
    }
    
   
    let question_description = "";
    let question_text = "";
    let listQuestionResult:any = []
    let indexQuestion = 0;
    
    return listQuestion;
  };
