import dayjs from "dayjs";
import { NextApiRequest, NextApiResponse } from "next";

import { commonDeleteStatus, DATE_DISPLAY_FORMAT, SALE_ITEM_TYPE } from "@/constants/index";
import { fillEmptyData } from "@/utils/api";
import { compareDateString } from "@/utils/date";
import createHandler, { apiResponseDTO, roleConfig } from "lib/apiHandle";
import prisma, { prismaJoin } from "lib/prisma";

import { Prisma } from "@prisma/client";
import { PACKAGES_FOR_STUDENT, USER_ROLES } from "interfaces/constants";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [roleConfig.admin],
        handler: async (req: NextApiRequest, res: NextApiResponse) => {
            let { startDate, endDate, userRoles }: any = req.query;

            endDate = dayjs(endDate || undefined).endOf('d').toDate();

            if (!startDate) {
                startDate = dayjs().startOf('d').subtract(29, 'day').toDate();
            } else {
                startDate = dayjs(startDate).toDate();
            }

            if (!userRoles) {
                userRoles = [ USER_ROLES.Teacher, USER_ROLES.Student ];
            } else {
                if (typeof userRoles === 'string') {
                    userRoles = [+userRoles];
                } else {
                    userRoles = userRoles.map((el: string) => +el).filter(Boolean)
                }
            }

            const newUserGroupByDate: any = await prisma.$queryRaw`SELECT DATE(CONVERT_TZ(u.created_date, '+00:00', '+07:00')) groupBy, 
                COUNT(DISTINCT u.id) as totalUser 
                FROM \`user\` as u
                WHERE u.created_date >= ${startDate} 
                    AND u.created_date <= ${endDate} 
                    AND u.user_role_id IN (${prismaJoin(userRoles)}) AND u.deleted = ${commonDeleteStatus.ACTIVE}
                GROUP BY groupBy`;

            const totalUser = await prisma.user.count({
                where: {
                    deleted: commonDeleteStatus.ACTIVE,
                    user_role_id: {
                        in: userRoles
                    }
                }
            });

            let userByPackage: any[] = [];
            const currentDate = dayjs()
            let queryString;
            if (userRoles.length === 1) {
                let asset_type;
                const role = Number(userRoles[0])
                if (role === USER_ROLES.Teacher) {
                    asset_type = SALE_ITEM_TYPE.SERVICE_PACKAGE
                    const listPackage = await prisma.service_package.findMany({
                        where: {
                            NOT: {
                                code: 'COMBO'
                            }
                        },
                        select: {
                            id: true,
                            name: true,
                            code: true,
                        }
                    })
                    queryString = Prisma.sql`SELECT p.code as code, count(*) as 'total' FROM user as u LEFT JOIN user_asset as ua ON u.id = ua.user_id and ua.asset_type = ${asset_type} LEFT JOIN service_package as p on ua.asset_id = p.id WHERE u.created_date >= ${startDate} and u.created_date <= ${endDate} and u.user_role_id = ${role} and u.deleted = ${commonDeleteStatus.ACTIVE} group by p.code`
                    const dataPackageForNewTeacher: any[] = await prisma.$queryRaw(queryString)
                    listPackage.forEach((pack: any) => {
                        pack.total = Number(dataPackageForNewTeacher.find(rs => rs.code === pack.code)?.total) || 0;
                        pack.asset_id =  pack.id
                    })

                    userByPackage = listPackage
                }
                if (role === USER_ROLES.Student) {
                    asset_type = SALE_ITEM_TYPE.COMBO_UNIT_TEST
                    const listComboUnitTest = await prisma.comboUnitTest.findMany({
                        select: {
                            id: true,
                            name: true,
                            code: true,
                        }
                    })
                    queryString = Prisma.sql`SELECT u.id, ua.*, p.code, p.name FROM user as u LEFT JOIN user_asset as ua ON u.id = ua.user_id and ua.asset_type = ${asset_type} LEFT JOIN combo_unit_test as p on ua.asset_id = p.id WHERE u.created_date >= ${startDate} and u.created_date <= ${endDate} and u.user_role_id = ${role} and u.deleted = ${commonDeleteStatus.ACTIVE} order by u.created_date desc`
                    const newPackageByRole: any[] = await prisma.$queryRaw(queryString);
                    const dataPackageForNewStudent: any[] = newPackageByRole?.map((item: any) => {
                        if (dayjs(item.end_date).diff(currentDate) < 0 || !item.asset_type) {
                            return {
                                name: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.NAME,
                                asset_id: "0",
                                code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
                            }
                        }
                        return item
                    })
                    const filterGetPackageFree: any = countBy(dataPackageForNewStudent).filter((pack: any) => pack.code === PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE)
                    const packageFree = {
                        id: 0,
                        name: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.NAME,
                        code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE,
                        total: filterGetPackageFree[0]?.total || 0,
                        asset_id: 0
                    }
                    listComboUnitTest.forEach((pack: any) => {
                        const groupData: any[] = countBy(dataPackageForNewStudent)
                        const currentPack: any = groupData.find((cb: any) => cb.code === pack.code)
                        pack.total = currentPack ? Number(currentPack?.total) : 0;
                        pack.asset_id =  pack.id
                    })
                    userByPackage = [packageFree,...listComboUnitTest]
                }
            }

            const userByRole = await prisma.user.groupBy({
                by: ['user_role_id'],
                where: {
                    created_date: {
                        gte: startDate,
                        lte: endDate,
                    },
                    user_role_id: {
                        in: userRoles
                    },
                    deleted: commonDeleteStatus.ACTIVE
                },
                _count: true,
            });

            const dataMapping = !!userByRole.length ?
                userByRole.map(el => ({ total: el._count, user_role_id: el.user_role_id })) :
                userRoles.map((item: any) => ({ total: 0, user_role_id: item }))

            const formattedUser = newUserGroupByDate.map((el: any) => ({
                total: Number(el.totalUser),
                date: dayjs(el.groupBy).format(DATE_DISPLAY_FORMAT),
            }))

            return res.status(200).json(apiResponseDTO(true, {
                users: fillEmptyData(formattedUser, startDate, endDate, {
                        total: 0
                    })
                    .sort((a: any, b: any) => compareDateString(a.date, b.date)),
                totalUser,
                userByRole: dataMapping,
                userByPackage: userByPackage.sort((curr:any, next:any) => curr.asset_id - next.asset_id)
            }));
        }
    },
});

export default handler

const countBy = (arr: any[]) => {
    const grouped = arr.reduce((map, item) => {
        const { name, code, asset_id } = item;
        if (!map.has(name)) {
            map.set(name, { asset_id, name, code, total: 0 });
        }
        map.get(name).total++;
        return map;
    }, new Map());
    return Array.from(grouped.values());
};