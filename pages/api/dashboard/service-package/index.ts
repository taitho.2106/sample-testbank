import dayjs from "dayjs";
import { NextApiRequest, NextApiResponse } from "next";

import createHandler, { apiResponseDTO, roleConfig, transformToArray } from "lib/apiHandle";
import prisma from "lib/prisma";
import { commonDeleteStatus, SALE_ITEM_TYPE } from "@/constants/index";
import { USER_ROLES } from "@/interfaces/constants";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [roleConfig.admin],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            let { startDate, endDate }: any = req.query;
            const { packages, userRoles = '2' } = req.query

            endDate = dayjs(endDate || undefined).endOf('d').toDate();

            if (!startDate) {
                startDate = dayjs().startOf('d').subtract(30, 'day').toDate();
            } else {
                startDate = dayjs(startDate).startOf('d').toDate();
            }
            let PackageLists = [SALE_ITEM_TYPE.SERVICE_PACKAGE, SALE_ITEM_TYPE.COMBO_SERVICE_PACKAGE]
            if (+userRoles === USER_ROLES.Teacher) {
                PackageLists = [SALE_ITEM_TYPE.SERVICE_PACKAGE, SALE_ITEM_TYPE.COMBO_SERVICE_PACKAGE]
                const servicePackagesStorage = await prisma.service_package.findMany();
                const saleItems = await prisma.saleItem.findMany({
                    where: {
                        item_type: {
                            in: PackageLists
                        }
                    },
                    select: {
                        id: true,
                        item_id: true,
                    }
                })
                let saleItemFilter = [];
                if (!packages) {
                    saleItemFilter = saleItems.map(el => el.id)
                } else {
                    const servicePackageIds = transformToArray(packages, (el: any) => +el);
                    saleItemFilter = servicePackageIds.map(id => saleItems.find(si => si.item_id === id)?.id).filter(Boolean);
                }

                const orderSummary = await prisma.order.groupBy({
                    by: ['sale_item_id'],
                    where: {
                        created_date: {
                            gte: startDate,
                            lte: endDate,
                        },
                        vnp_response_code: '00',
                        vnp_transaction_status: '00',
                        sale_item_id: {
                            in: saleItemFilter,
                        },
                    },
                    _count: true,
                    _sum: {
                        actual_price: true,
                    }
                })
                if (!packages) {
                    return res.status(200).json(apiResponseDTO(true, {
                        orders: servicePackagesStorage.map(el => {
                            const packageId = saleItems.find(si => si.item_id === el.id)?.id;
                            const orderItem = orderSummary.find(or => or.sale_item_id === packageId);
                            return {
                                totalAmount: Number(Boolean(orderItem) ? orderItem._sum.actual_price : 0),
                                orderCount: Number(Boolean(orderItem) ? orderItem._count : 0),
                                servicePackage: el
                            };
                        })
                    }));
                }
                // const packageId = saleItems.find(si => si.id === +packages)?.item_id;
                const orders = !!orderSummary.length ?
                    orderSummary.map(el => {
                        const packageId = saleItems.find(si => si.id === el.sale_item_id)?.item_id;
                        return {
                            totalAmount: Number(el._sum.actual_price),
                            orderCount: Number(el._count),
                            servicePackage: servicePackagesStorage.find(sp => sp.id === packageId)
                        };
                    }) :
                    [{
                        totalAmount: 0,
                        orderCount: 0,
                        servicePackage: servicePackagesStorage.find(sp => sp.id === +packages)
                    }];

                return res.status(200).json(apiResponseDTO(true, { orders }));

            }
            if (+userRoles === USER_ROLES.Student) {
                PackageLists = [SALE_ITEM_TYPE.COMBO_UNIT_TEST, SALE_ITEM_TYPE.UNIT_TEST]
                const comboUnitTestStorage = await prisma.comboUnitTest.findMany();
                const saleItemIdStudent = await prisma.saleItem.findMany({
                    where: {
                        item_type: SALE_ITEM_TYPE.COMBO_UNIT_TEST
                    },
                    select: {
                        id: true,
                        name: true,
                        item_id: true,
                    }
                })
                if (!packages) {
                    const saleItemFilter = saleItemIdStudent.map((el: any) => el.id)

                    const orderSummary = await prisma.order.groupBy({
                        by: ['sale_item_id'],
                        where: {
                            created_date: {
                                gte: startDate,
                                lte: endDate,
                            },
                            vnp_response_code: '00',
                            vnp_transaction_status: '00',
                            sale_item_id: {
                                in: saleItemFilter,
                            },
                        },
                        _count: true,
                        _sum: {
                            actual_price: true,
                        }
                    })
                    const orderSummaryUnitTest = await prisma.order.aggregate({
                        where: {
                            created_date: {
                                gte: startDate,
                                lte: endDate,
                            },
                            vnp_response_code: '00',
                            vnp_transaction_status: '00',
                            saleItem: {
                                item_type: SALE_ITEM_TYPE.UNIT_TEST
                            }
                        },
                        _count: true,
                        _sum: {
                            actual_price: true,
                        }
                    })
                    const orderPackage = comboUnitTestStorage.map(el => {
                        const packageId = saleItemIdStudent.find((si: any) => si.item_id === el.id)?.id;
                        const orderItem = orderSummary.find(or => or.sale_item_id === packageId)
                        return {
                            totalAmount: Number(orderItem ? orderItem._sum.actual_price : 0),
                            orderCount: Number(orderItem ? orderItem._count : 0),
                            servicePackage: el,
                        }
                    })
                    const orderUnitTest = {
                        totalAmount: Number(orderSummaryUnitTest._sum.actual_price),
                        orderCount: Number(orderSummaryUnitTest._count),
                        servicePackage: {
                            name: 'Mua đề lẻ',
                            code: SALE_ITEM_TYPE.UNIT_TEST
                        }
                    }

                    return res.status(200).json(apiResponseDTO(true, {
                        orders: [...orderPackage, orderUnitTest]
                    }));
                } else {
                    if (!Boolean(isNaN(Number(packages)))) {
                        const packageId = saleItemIdStudent.find((si: any) => si.id === +packages)?.item_id;
                        const saleItemFilter: number[] = transformToArray(packages, (el: any) => +el) as number[];
                        const orderSummary = await prisma.order.groupBy({
                            by: ['sale_item_id'],
                            where: {
                                created_date: {
                                    gte: startDate,
                                    lte: endDate,
                                },
                                vnp_response_code: '00',
                                vnp_transaction_status: '00',
                                sale_item_id: {
                                    in: saleItemFilter,
                                },
                            },
                            _count: true,
                            _sum: {
                                actual_price: true,
                            }
                        })
                        const orders = !!orderSummary.length ?
                            orderSummary.map(el => {
                                const packageId = saleItemIdStudent.find(si => si.id === el.sale_item_id)?.item_id;
                                return {
                                    totalAmount: Number(el._sum.actual_price),
                                    orderCount: Number(el._count),
                                    servicePackage: comboUnitTestStorage.find(sp => sp.id === packageId)
                                };
                            }) :
                            [{
                                totalAmount: 0,
                                orderCount: 0,
                                servicePackage: comboUnitTestStorage.find(sp => sp.id === packageId)
                            }];
                        return res.status(200).json(apiResponseDTO(true, { orders }));
                    }

                    const orderSummaryUnitTest = await prisma.order.aggregate({
                        where: {
                            created_date: {
                                gte: startDate,
                                lte: endDate,
                            },
                            vnp_response_code: '00',
                            vnp_transaction_status: '00',
                            saleItem: {
                                item_type: packages.toString()
                            }
                        },
                        _count: true,
                        _sum: {
                            actual_price: true,
                        }
                    })

                    const orderUnitTest = {
                        totalAmount: Number(orderSummaryUnitTest._sum.actual_price),
                        orderCount: Number(orderSummaryUnitTest._count),
                        servicePackage: {
                            name: 'Mua đề lẻ',
                            code: SALE_ITEM_TYPE.UNIT_TEST
                        }
                    }

                    return res.status(200).json(apiResponseDTO(true, {
                        orders: [orderUnitTest]
                    }));
                }
            }
        }
    },
});

export default handler