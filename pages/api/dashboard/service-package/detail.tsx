import dayjs from "dayjs";
import { NextApiRequest, NextApiResponse } from "next";

import { DATE_DISPLAY_FORMAT, SALE_ITEM_TYPE } from "@/constants/index";
import { fillEmptyData } from "@/utils/api";
import { compareDateString } from "@/utils/date";
import createHandler, { apiResponseDTO, roleConfig, transformToArray } from "lib/apiHandle";
import prisma, { prismaJoin } from "lib/prisma";
import { USER_ROLES } from "@/interfaces/constants";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [roleConfig.admin],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            let { startDate, endDate }: any = req.query;
            const { packages, userRoles } = req.query;

            endDate = dayjs(endDate || undefined).endOf('d').toDate();

            if (!startDate) {
                startDate = dayjs().startOf('d').subtract(29, 'day').toDate();
            } else {
                startDate = dayjs(startDate).startOf('d').toDate();
            }
            const filterByRole = +userRoles === USER_ROLES.Teacher ?
                [SALE_ITEM_TYPE.SERVICE_PACKAGE, SALE_ITEM_TYPE.COMBO_SERVICE_PACKAGE] :
                [SALE_ITEM_TYPE.COMBO_UNIT_TEST, SALE_ITEM_TYPE.UNIT_TEST]

            const saleItems = await prisma.saleItem.findMany({
                where: {
                    item_type: {
                        in: filterByRole
                    }
                }
            })
            if (+userRoles === USER_ROLES.Teacher) {
                let saleItemFilter = [];
                if (!packages) {
                    saleItemFilter = saleItems.map(el => el.id)
                } else {
                    const servicePackageIds = transformToArray(packages, (el: any) => +el);
                    saleItemFilter = servicePackageIds.map(id => saleItems.find(si => si.item_id === id)?.id).filter(Boolean);
                }

                const orders: any = await prisma.$queryRaw`SELECT DATE(CONVERT_TZ(created_date, '+00:00', '+07:00')) groupBy, 
                COUNT(DISTINCT id) as orderCount, 
                SUM(total) as totalAmount 
                FROM \`order\` 
                WHERE created_date >= ${startDate} 
                    AND created_date <= ${endDate} 
                    AND vnp_response_code = '00' 
                    AND vnp_transaction_status = '00' 
                    AND sale_item_id IN (${prismaJoin(saleItemFilter)})
                GROUP BY groupBy`

                const correctData = orders.map((el: any) => ({
                    orderCount: Number(el.orderCount),
                    totalAmount: Number(el.totalAmount),
                    date: dayjs(el.groupBy).format(DATE_DISPLAY_FORMAT)
                }))

                return res.status(200).json(apiResponseDTO(true, {
                    orders: fillEmptyData(correctData, startDate, endDate, {
                        orderCount: 0,
                        totalAmount: 0,
                    })
                        .sort((a: any, b: any) => compareDateString(a.date, b.date))
                }));
            }

            if (+userRoles === USER_ROLES.Student) {
                if (!packages) {
                    const saleItemFilter = saleItems.map(el => el.id)
                    const orders: any = await prisma.$queryRaw`SELECT DATE(CONVERT_TZ(created_date, '+00:00', '+07:00')) groupBy, 
                        COUNT(DISTINCT id) as orderCount, 
                        SUM(total) as totalAmount 
                        FROM \`order\` 
                        WHERE created_date >= ${startDate} 
                            AND created_date <= ${endDate} 
                            AND vnp_response_code = '00' 
                            AND vnp_transaction_status = '00' 
                            AND sale_item_id IN (${prismaJoin(saleItemFilter)})
                        GROUP BY groupBy`

                    const correctData = orders.map((el: any) => ({
                        orderCount: Number(el.orderCount),
                        totalAmount: Number(el.totalAmount),
                        date: dayjs(el.groupBy).format(DATE_DISPLAY_FORMAT)
                    }))

                    return res.status(200).json(apiResponseDTO(true, {
                        orders: fillEmptyData(correctData, startDate, endDate, {
                            orderCount: 0,
                            totalAmount: 0,
                        })
                            .sort((a: any, b: any) => compareDateString(a.date, b.date))
                    }));
                } else {
                    if(!Boolean(isNaN(Number(packages)))) {
                        const saleItemFilter = transformToArray(packages, (el: any) => +el);
                        const orders: any = await prisma.$queryRaw`SELECT DATE(CONVERT_TZ(created_date, '+00:00', '+07:00')) groupBy, 
                            COUNT(DISTINCT id) as orderCount, 
                            SUM(total) as totalAmount 
                            FROM \`order\` 
                            WHERE created_date >= ${startDate} 
                                AND created_date <= ${endDate} 
                                AND vnp_response_code = '00' 
                                AND vnp_transaction_status = '00' 
                                AND sale_item_id IN (${prismaJoin(saleItemFilter)})
                            GROUP BY groupBy`

                        const correctData = orders.map((el: any) => ({
                            orderCount: Number(el.orderCount),
                            totalAmount: Number(el.totalAmount),
                            date: dayjs(el.groupBy).format(DATE_DISPLAY_FORMAT)
                        }))

                        return res.status(200).json(apiResponseDTO(true, {
                            orders: fillEmptyData(correctData, startDate, endDate, {
                                orderCount: 0,
                                totalAmount: 0,
                            })
                                .sort((a: any, b: any) => compareDateString(a.date, b.date))
                        }));
                    }

                    const listSaleItemId = await prisma.saleItem.findMany({
                        where: {
                            item_type: packages.toString()
                        },
                        select: {
                            id: true,
                        }
                    })
                    const listIdMapping = listSaleItemId.map(item => item.id)
                    const orderSummaryUnitTest: any[] = await prisma.$queryRaw`SELECT DATE(CONVERT_TZ(created_date, '+00:00', '+07:00')) groupBy, 
                        COUNT(DISTINCT id) as orderCount, 
                        SUM(total) as totalAmount 
                        FROM \`order\` 
                        WHERE created_date >= ${startDate} 
                            AND created_date <= ${endDate} 
                            AND vnp_response_code = '00' 
                            AND vnp_transaction_status = '00' 
                            AND sale_item_id IN (${prismaJoin(listIdMapping)})
                        GROUP BY groupBy`

                    const correctData = orderSummaryUnitTest.map((el: any) => ({
                        orderCount: Number(el.orderCount),
                        totalAmount: Number(el.totalAmount),
                        date: dayjs(el.groupBy).format(DATE_DISPLAY_FORMAT)
                    }))

                    return res.status(200).json(apiResponseDTO(true, {
                        orders: fillEmptyData(correctData, startDate, endDate, {
                            orderCount: 0,
                            totalAmount: 0,
                        })
                            .sort((a: any, b: any) => compareDateString(a.date, b.date))
                    }));
                }
            }
        }
    },
});

export default handler