import dayjs from "dayjs";
import { NextApiRequest, NextApiResponse } from "next";

import { DATE_DISPLAY_FORMAT } from "@/constants/index";
import { fillEmptyData } from "@/utils/api";
import { compareDateString } from "@/utils/date";
import createHandler, { apiResponseDTO, roleConfig } from "lib/apiHandle";
import prisma, { prismaJoin } from "lib/prisma";
import { Prisma } from "@prisma/client";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        accessRoles: [roleConfig.admin],
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any) => {
            let { startDate, endDate }: any = req.query;
            const { userRoles } = req.query;

            endDate = dayjs(endDate || undefined).endOf('d').toDate();

            if (!startDate) {
                startDate = dayjs().startOf('d').subtract(29, 'day').toDate();
            } else {
                startDate = dayjs(startDate).startOf('d').toDate();
            }
            let roleJoinCondition = null
            if (userRoles) {
                const arrayUserRole = (userRoles as string).split(',').filter(Boolean);
                roleJoinCondition = Prisma.sql`LEFT JOIN \`user\` as u 
                ON o.user_id = u.id WHERE o.created_date >= ${startDate} AND o.created_date <= ${endDate} AND u.user_role_id IN (${prismaJoin(arrayUserRole)})`
            } else {
                roleJoinCondition = Prisma.sql`WHERE o.created_date >= ${startDate} AND o.created_date <= ${endDate}`
            }
            const orders: any = await prisma.$queryRaw`SELECT DATE(CONVERT_TZ(o.created_date, '+00:00', '+07:00')) groupBy, 
                (CASE WHEN o.vnp_response_code = '00' AND o.vnp_transaction_status = '00' THEN 1 ELSE 0 END) as succeed, 
                COUNT(DISTINCT o.id) as totalOrder, 
                SUM(o.actual_price) as total 
                FROM \`order\` as o 
                ${roleJoinCondition} 
                GROUP BY groupBy, succeed`

            const orderSucceed: any = {
                total: 0,
                orderCount: 0,
                data: [],
            };
            const orderFailed: any = {
                total: 0,
                orderCount: 0,
                data: [],
            };

            const updateOrder = (orderDetails: any, result: any) => {
                const orderCount = Number(orderDetails.totalOrder);
                const orderAmount = Number(orderDetails.total);
                const dateString = dayjs(orderDetails.groupBy).format(DATE_DISPLAY_FORMAT);

                result.orderCount += orderCount;
                result.total += orderAmount;

                result.data.push({
                    orderCount,
                    total: orderAmount,
                    date: dateString,
                });
            }

            for (const orderDetails of orders) {
                if (Number(orderDetails.succeed) === 1) {
                    updateOrder(orderDetails, orderSucceed);
                } else {
                    updateOrder(orderDetails, orderFailed);
                }
            }

            const mapResponse = ({ total, orderCount, data }: any) => {
                return {
                    total,
                    orderCount,
                    data: fillEmptyData(data, startDate, endDate, {
                            orderCount: 0,
                            total: 0,
                        })
                        .sort((a: any, b: any) => compareDateString(a.date, b.date))
                }
            }

            return res.status(200).json(apiResponseDTO(true, {
                succeed: mapResponse(orderSucceed),
                failed: mapResponse(orderFailed),
            }));
        }
    },
});

export default handler