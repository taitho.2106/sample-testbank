import prisma from "lib/prisma";
import { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/client";

import { getSaleItemInfo } from "services/saleItem";

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    if (!session) {
        return res.status(401).end('Unauthorized')
    }

    switch (req.method) {
        case 'GET': {
            const { order_id } = req.query;
            const orderInfo: any = await prisma.order.findUnique({ where: { id: +order_id } });
            if (orderInfo) {
                orderInfo.saleItem = await getSaleItemInfo(orderInfo.sale_item_id);
                orderInfo.total = Number(orderInfo.total);
                orderInfo.actual_price = Number(orderInfo.actual_price);
            }
            return res.json({
                data: orderInfo ?? null,
                result: !!orderInfo,
            });
        }
        default: {
            return res.status(405).end(`Method ${req.method} Not Allowed`)
        }
    }
}

export default handler;