import { commonStatus, SALE_ITEM_TYPE } from "@/constants/index";
import { ORDER_STATUS, PACKAGES_FOR_STUDENT } from "@/interfaces/constants";
import { dayjs } from "@/utils/date";
import prisma from "lib/prisma";
import { getStudentComboUnitTest } from "services/comboUnitTest";
import { addUnitTestAssetForUser } from "services/userAssets";
import { eventConfig } from "@/utils/index";

export const handleOrderUnitTest = async (order: any, saleItem: any) => {
    const assetExisted = await prisma.user_asset.findFirst({
        where: {
            user_id: order.user_id,
            asset_type: SALE_ITEM_TYPE.UNIT_TEST,
            asset_id: saleItem.item_id + '',
        }
    })

    if (!assetExisted) {
        await addUnitTestAssetForUser(saleItem.item_id, order.user_id);
    }

    if (!dayjs().isBetween(eventConfig.startDate, eventConfig.endDate)) {
        return undefined;
    }

    const DATETIME_VALUE_FORMAT = 'YYYY-MM-DD HH:mm:ss';
    const orderSuccessCountResult: any = await prisma.$queryRaw`
        SELECT COUNT(*) total FROM \`order\` ord
            JOIN sale_item si ON ord.sale_item_id = si.id
        WHERE si.item_type = ${SALE_ITEM_TYPE.UNIT_TEST}
            AND ord.vnp_response_code = '00'
            AND ord.vnp_transaction_status = '00'
            AND ord.user_id = ${order.user_id}
            AND ord.status = ${ORDER_STATUS.DONE}
            AND ord.created_date >= ${dayjs(eventConfig.startDate).utc().format(DATETIME_VALUE_FORMAT)}
            AND ord.created_date <= ${dayjs(eventConfig.endDate).utc().format(DATETIME_VALUE_FORMAT)}
    `
    const orderSuccessCount: any = Number(orderSuccessCountResult[0].total);

    if (eventConfig.gifts.hasOwnProperty(orderSuccessCount)) {
        const config: any = { ...eventConfig.gifts[orderSuccessCount as keyof Object] };
        const { addQuantity, addDayExpire, comboCode }: any = config;
        config.saleItemType = saleItem.item_type;
        config.oldCombo = PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE;
        config.newCombo = comboCode;

        const oldCombo = await getStudentComboUnitTest(order.user_id);

        const currDate = dayjs();
        const comboUnitTest = await prisma.comboUnitTest.findFirst({
            where: { code: comboCode }
        })

        if (oldCombo) {
            let startDate = currDate.toDate();
            let endDate = currDate.add(addDayExpire, 'd').toDate();
            let quantity = addQuantity;
            let asset_id = comboUnitTest.id + '';
            const oldComboCode = oldCombo.item.code;

            if (dayjs(oldCombo.end_date).isAfter(dayjs())) {
                config.oldCombo = oldComboCode;
                startDate = oldCombo.start_date;
                endDate = dayjs(oldCombo.end_date).add(addDayExpire, 'd').toDate();
                quantity += oldCombo.quantity;

                if (oldComboCode === PACKAGES_FOR_STUDENT.SUPER_INTELLIGENCE_CHALLENGE.CODE) {
                    asset_id = oldCombo.item.id + '';
                    config.newCombo = oldComboCode;
                }
            }

            await prisma.user_asset.update({
                where: { id: oldCombo.id },
                data: {
                    quantity,
                    updated_date: currDate.toDate(),
                    asset_id,
                    start_date: startDate,
                    end_date: endDate,
                    status: commonStatus.ACTIVE,
                }
            })
        } else {
            await prisma.user_asset.create({
                data: {
                    user_id: order.user_id,
                    asset_id: comboUnitTest.id + '',
                    asset_type: SALE_ITEM_TYPE.COMBO_UNIT_TEST,
                    quantity: addQuantity,
                    created_date: currDate.toDate(),
                    updated_date: currDate.toDate(),
                    start_date: currDate.toDate(),
                    end_date: currDate.add(addDayExpire, 'd').toDate(),
                }
            })
        }

        return JSON.stringify(config);
    }

    return undefined;
}