import dayjs from "dayjs";
import { NextApiRequest, NextApiResponse } from "next";

import { getContentEmailBuyComboServicePackage } from "@/constants/email-template";
import {
    commonDeleteStatus,
    commonStatus,
    RECEIVE_FEEDBACK_EMAIL,
    SALE_ITEM_TYPE
} from "@/constants/index";
import { ORDER_STATUS, USER_ASSET_TYPE, VNPAY_STATUS_CODE } from "@/interfaces/constants";
import { log4js } from "@/utils/logger";
import { createSign, sortObject } from "@/utils/nvpay";
import prisma from "lib/prisma";
import { getLastPackage } from "services/package";
import { getSaleItemInfo } from "services/saleItem";
import { saveUserAssets, updateUserAssets } from "services/userAssets";

import { sendEmail } from "../../../services/email";
import { VOUCHER_HISTORY_STATUS } from "../voucher/validate";
import { handleOrderUnitTest } from "./unit-test";

const querystring = require('qs');


const logger = log4js.getLogger('vnpay_ipn');

const saveUserPackage = async (order: any, item: any) => {
    const currDate = new Date();
    let endDate = dayjs(currDate).add(order.quantity, 'M').toDate();

    const userPackage: any = await getLastPackage(order.user_id);
    if (userPackage) {
        let startDate = new Date();
        if (userPackage.id == item.item_id && dayjs(userPackage.end_date).isAfter(dayjs())) {
            startDate = userPackage.start_date;
            endDate = dayjs(userPackage.end_date).add(order.quantity, 'M').toDate();
        }

        updateUserAssets({
            id: userPackage.asset_id,
            assetId: item.item_id,
            startDate,
            endDate,
            quantity: 1,
        });
    } else {
        saveUserAssets({
            userId: order.user_id,
            assetId: item.item_id,
            assetType: USER_ASSET_TYPE.SERVICE_PACKAGE,
            quantity: 1,
            createdDate: currDate,
            endDate,
        });
    }
}

const handleOrderComboUnitTestSuccess = async (order: any, saleItem: any) => {
    const currDate = dayjs();
    const oldCombo = await prisma.user_asset.findFirst({
        where: {
            user_id: order.user_id,
            asset_type: SALE_ITEM_TYPE.COMBO_UNIT_TEST,
        }
    })

    if (oldCombo) {
        let startDate = currDate.toDate();
        let endDate = currDate.add(saleItem.day_expire, 'd').toDate();
        let quantity = saleItem.item.limit;

        if (oldCombo.asset_id == saleItem.item_id && dayjs(oldCombo.end_date).isAfter(dayjs())) {
            startDate = oldCombo.start_date;
            endDate = dayjs(oldCombo.end_date).add(saleItem.day_expire, 'd').toDate();
            quantity += oldCombo.quantity;
        }

        await prisma.user_asset.update({
            where: {
                id: oldCombo.id,
            },
            data: {
                quantity,
                updated_date: currDate.toDate(),
                asset_id: saleItem.item_id + '',
                start_date: startDate,
                end_date: endDate,
                status: commonStatus.ACTIVE,
            }
        })
    } else {
        await prisma.user_asset.create({
            data: {
                user_id: order.user_id,
                asset_id: saleItem.item_id + '',
                asset_type: SALE_ITEM_TYPE.COMBO_UNIT_TEST,
                quantity: saleItem.item.limit,
                created_date: currDate.toDate(),
                updated_date: currDate.toDate(),
                start_date: currDate.toDate(),
                end_date: currDate.add(saleItem.day_expire, 'd').toDate(),
            }
        })
    }
}

const handlerOrderComboServicePackage = async  (res: NextApiResponse, order: any, saleItem: any) => {
    if (order.extra_data) {
        const accounts = JSON.parse(order.extra_data)
        const user = await prisma.user.findFirst({
            where: {
                id: order.user_id,
                deleted: commonDeleteStatus.ACTIVE,
            }
        })

        if (user) {
            try {
                sendEmail({
                    to: user.email,
                    subject: `${dayjs().format('Ngày DD/MM/YYYY HH:mm')} - Bạn đã thanh toán thành công Gói combo tài khoản dành cho Giáo viên`,
                    content: getContentEmailBuyComboServicePackage({
                        emailTo: user.email,
                        user,
                        accountsBuyComboServicePackage: accounts
                    })
                }).then();
                sendEmail({
                    to: RECEIVE_FEEDBACK_EMAIL,
                    subject: `${dayjs().format('Ngày DD/MM/YYYY HH:mm')} - i-Test ghi nhận thanh toán thành công Gói combo tài khoản dành cho Giáo viên`,
                    content: getContentEmailBuyComboServicePackage({
                        emailTo: RECEIVE_FEEDBACK_EMAIL,
                        user,
                        accountsBuyComboServicePackage: accounts
                    })
                }).then();

            } catch (err) {
                console.log({err});
            }
        }
    }

}

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    switch (req.method) {
        case 'GET': {
            let vnpLog: any = {};
            try {
                const vnpParamsOrigin: any = req.query;
                delete vnpParamsOrigin['step']; // own param

                const ipAddr = req.headers['x-forwarded-for'] ||
                    req.connection?.remoteAddress ||
                    req.socket?.remoteAddress;

                vnpLog = {
                    params: vnpParamsOrigin,
                    ipAddr,
                    date: new Date(),
                };

                let vnp_Params = vnpParamsOrigin;
                const secureHash = vnp_Params['vnp_SecureHash'];
                const orderId = vnp_Params['vnp_TxnRef'];
                const rspCode = vnp_Params['vnp_ResponseCode'];
                const trxStatus = vnp_Params['vnp_TransactionStatus'];
                const trxNo = vnp_Params['vnp_TransactionNo'];
                const amount = vnp_Params['vnp_Amount'];

                // params not checksum
                delete vnp_Params['vnp_SecureHash'];
                delete vnp_Params['vnp_SecureHashType'];
                const order: any = await prisma.order.findUnique({ where: { id: +orderId } })

                const checkOrderResult = async () => {
                    vnp_Params = sortObject(vnp_Params);
                    const signData = querystring.stringify(vnp_Params, { encode: false });
                    const signed = createSign(signData);
                    if (secureHash !== signed) {
                        return { messageToVNP: 'Checksum failed', rspCodeToVNP: '97' };
                    }

                    if (!order) {
                        return { messageToVNP: 'Order Not Found', rspCodeToVNP: '01' };
                    }

                    const checkAmount = Number(order.actual_price) === (amount / 100);
                    if (!checkAmount) {
                        return { messageToVNP: 'Amount invalid', rspCodeToVNP: '04' };
                    }

                    const orderStatus = order?.status;
                    if (orderStatus !== ORDER_STATUS.NEW) {
                        return { messageToVNP: 'Order already confirmed', rspCodeToVNP: '02' };
                    }

                    if (rspCode === trxStatus && trxStatus === '00') {
                        return {
                            isPaymentSuccess: true,
                            messageToVNP: 'Success',
                            rspCodeToVNP: '00',
                            shouldUpdateOrder: true,
                        };
                    }

                    return {
                        messageToVNP: 'Success',
                        rspCodeToVNP: '00',
                        shouldUpdateOrder: true,
                        explainOrderStatus: VNPAY_STATUS_CODE[rspCode as keyof Object] || 'Lỗi quá trình xử lý',
                    };
                }

                const { isPaymentSuccess, messageToVNP, rspCodeToVNP, shouldUpdateOrder, explainOrderStatus }: any = await checkOrderResult();
                vnpLog.response = { isPaymentSuccess, messageToVNP, rspCodeToVNP, explainOrderStatus };

                if (shouldUpdateOrder) {
                    // update voucher_history change status
                    try {
                        await prisma.voucher_history.update({
                            where: {
                                order_id: +orderId
                            },
                            data: {
                                status: isPaymentSuccess ? VOUCHER_HISTORY_STATUS.PAID.code : VOUCHER_HISTORY_STATUS.CANCEL.code
                            }
                        })
                    } catch(err: any) {
                        if (err.code !== "P2025") {
                            console.error("Update status for voucher_history fail: ", err)
                        }
                    }

                    let extra_data = undefined;
                    if (isPaymentSuccess) {
                        const saleItem = await getSaleItemInfo(order.sale_item_id);

                        switch (saleItem.item_type) {
                            case SALE_ITEM_TYPE.SERVICE_PACKAGE: {
                                await saveUserPackage(order, saleItem);
                                break;
                            }
                            case SALE_ITEM_TYPE.UNIT_TEST: {
                                extra_data = await handleOrderUnitTest(order, saleItem);
                                break;
                            }
                            case SALE_ITEM_TYPE.COMBO_UNIT_TEST: {
                                await handleOrderComboUnitTestSuccess(order, saleItem);
                                break;
                            }
                            case SALE_ITEM_TYPE.COMBO_SERVICE_PACKAGE: {
                                await handlerOrderComboServicePackage(res, order, saleItem);
                                break;
                            }
                            default:
                                break;
                        }
                    }

                    await prisma.order.update({
                        where: { id: +orderId },
                        data: {
                            status: ORDER_STATUS.DONE,
                            vnp_transaction_no: +trxNo,
                            vnp_transaction_status: trxStatus,
                            vnp_response_code: rspCode,
                            note: explainOrderStatus ?? messageToVNP,
                            vnpay_params: JSON.stringify(vnpLog),
                            extra_data,
                        },
                    })
                }

                return res.status(200).json({
                    Message: messageToVNP,
                    RspCode: rspCodeToVNP,
                });
            } catch (error) {
                logger.error(JSON.stringify(error));
                return res.status(200).json({
                    Message: '99',
                    RspCode: '99',
                });
            } finally {
                logger.info(JSON.stringify(vnpLog));
            }
        }
        default: {
            return res.status(405).end(`Method ${req.method} Not Allowed`)
        }
    }
}

export default handler;