import dayjs from 'dayjs';
import { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/client";

let querystring = require('qs');

import { BANK_CODES, ORDER_STATUS, paths, STEPS } from '@/interfaces/constants';
import { createSign, sortObject } from '@/utils/nvpay';
import prisma from "lib/prisma";
import { getSaleItemInfo } from "services/saleItem";
import { VOUCHER_HISTORY_STATUS, voucherValidate } from '../voucher/validate';
import { formatNumber } from '@/utils/index';
import { SALE_ITEM_TYPE } from '@/constants/index';

const makeOrderInfo = (saleItem: any, orderInfo: any) => {
    const paymentInfoMap = {
        [SALE_ITEM_TYPE.UNIT_TEST]: 'Thanh toan mua de le',
        [SALE_ITEM_TYPE.SERVICE_PACKAGE]: 'Thanh toan mua goi dich vu',
        [SALE_ITEM_TYPE.COMBO_UNIT_TEST]: 'Thanh toan mua combo de',
        [SALE_ITEM_TYPE.COMBO_SERVICE_PACKAGE]: 'Thanh toan mua combo goi dich vu',
    }
    return `${paymentInfoMap[saleItem.item_type as keyof Object]}. MGD: ${orderInfo.id}. So tien ${formatNumber(orderInfo.total, 0)} VND.`
}

const handler: any = async (req: NextApiRequest, res: NextApiResponse) => {
    const session = await getSession({ req })
    if (!session) {
        return res.status(401).end('Unauthorized')
    }

    switch (req.method) {
        case 'POST': {
            const {
                itemId,
                language = 'vn',
                quantity = 1,
                bankCode = BANK_CODES.VNPAYQR,
                returnPath = paths.paymentResults,
                voucher_code,
                accounts = []
            } = req.body;
            if (!Object.values(BANK_CODES).includes(bankCode)) {
                return res.status(400).json({
                    message: 'bankCode invalid!',
                    result: false,
                });
            }

            const saleItemInfo = await getSaleItemInfo(itemId);
            if (!saleItemInfo) {
                return res.status(400).json({
                    message: 'Sale item not found!',
                    result: false,
                });
            }

            const user: any = session?.user;
            const curDate = new Date();
            const total = quantity * saleItemInfo.exchange_price;
            let discount_price = 0;
            let voucher_id;
            let extra_data = '';

            // re validate voucher
            if (voucher_code) {
                const checkValidateVoucher = await voucherValidate(voucher_code, user, total);

                if (checkValidateVoucher.error_code) {
                    return res.status(200).json({
                        message: 'voucher invalid',
                        error_code: checkValidateVoucher.error_code,
                        result: false,
                    });
                }

                discount_price = checkValidateVoucher.discount_price;
                voucher_id = checkValidateVoucher.id;
            }
            //list account buy combo service package
            if (!!accounts.length) {
                extra_data = JSON.stringify(accounts)
            }

            const actual_price = total - discount_price > 5000 ? total - discount_price : 5000;

            const saveResult = await prisma.order.create({
                data: {
                    user_id: user.id,
                    sale_item_id: +itemId,
                    created_date: curDate,
                    updated_date: curDate,
                    status: ORDER_STATUS.NEW,
                    quantity,
                    total,
                    actual_price,
                    extra_data,
                }
            })

            if (!saveResult) {
                return res.status(400).json({
                    message: 'Create order failed!',
                    result: false,
                });
            }

            // create voucher history
            if (voucher_id) {
                const newVoucherHistory = await prisma.voucher_history.create({
                    data: {
                        voucher_id: voucher_id,
                        order_id: saveResult.id,
                        user_id: user.id,
                        discount_price: discount_price,
                        created_date: curDate,
                        updated_date: curDate,
                        status: VOUCHER_HISTORY_STATUS.WAIT.code
                    }
                })
    
                if (!newVoucherHistory) {
                    return res.status(400).json({
                        message: 'Create voucher failed!',
                        result: false,
                    });
                }
            }

            process.env.TZ = 'Asia/Ho_Chi_Minh';
            let ipAddr = req.headers['x-forwarded-for'] ||
                req.connection.remoteAddress ||
                req.socket.remoteAddress;

            const returnUrl = process.env.NEXT_PUBLIC_WEB_URL + returnPath + `?step=${STEPS.DONE}`;
            let vnpUrl = process.env.NEXT_PUBLIC_VNP_URL;

            let createDate = dayjs(curDate).format('YYYYMMDDHHmmss');
            let orderId = saveResult.id;
            let locale = language;
            let currCode = 'VND';
            let vnp_Params: any = {};
            vnp_Params['vnp_Version'] = '2.1.0';
            vnp_Params['vnp_Command'] = 'pay';
            vnp_Params['vnp_TmnCode'] = process.env.NEXT_PUBLIC_VNP_TMN_CODE;
            vnp_Params['vnp_Locale'] = locale;
            vnp_Params['vnp_CurrCode'] = currCode;
            vnp_Params['vnp_TxnRef'] = orderId;
            vnp_Params['vnp_OrderInfo'] = makeOrderInfo(saleItemInfo, saveResult);
            vnp_Params['vnp_OrderType'] = 'other';
            vnp_Params['vnp_Amount'] = actual_price * 100;
            vnp_Params['vnp_ReturnUrl'] = returnUrl;
            vnp_Params['vnp_IpAddr'] = ipAddr;
            vnp_Params['vnp_CreateDate'] = createDate;
            vnp_Params['vnp_BankCode'] = bankCode;

            vnp_Params = sortObject(vnp_Params);
            let signData = querystring.stringify(vnp_Params, { encode: false });
            vnp_Params['vnp_SecureHash'] = createSign(signData);
            vnpUrl += '?' + querystring.stringify(vnp_Params, { encode: false });

            console.log("vnpUrl",vnpUrl)

            return res.json({ vnpUrl, result: true });
        }
        default: {
            return res.status(405).end(`Method ${req.method} Not Allowed`)
        }
    }
}

export default handler;