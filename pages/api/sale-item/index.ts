import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import { SALE_ITEM_TYPE } from "@/constants/index";
import createHandler, { apiResponseDTO } from "lib/apiHandle";
import { getListSaleItemByType } from "services/saleItem";

const handler: any = createHandler({
    'GET': {
        querySchema: yup.object().shape({
            type: yup.string().required().oneOf(Object.values(SALE_ITEM_TYPE)),
        }),
        pageable: true,
        handler: async (req: NextApiRequest, res: NextApiResponse, session: any, paginationData) => {
            const { type } = req.query;
            const result = await getListSaleItemByType(type as string, paginationData.skip, paginationData.take);
            return res.status(200).json(apiResponseDTO(true, result));
        }
    },
});

export default handler