import { NextApiRequest, NextApiResponse } from "next";
import * as yup from 'yup';

import createHandler, { apiResponseDTO } from "lib/apiHandle";
import { getSaleItemInfo } from "services/saleItem";

const handler: any = createHandler({
    'GET': {
        authRequired: true,
        querySchema: yup.object().shape({
            id: yup.number().required(),
        }),
        pageable: true,
        handler: async (req: NextApiRequest, res: NextApiResponse) => {
            const { id } = req.query;
            const result = await getSaleItemInfo(+id);
            return res.status(200).json(apiResponseDTO(true, result));
        }
    },
});

export default handler