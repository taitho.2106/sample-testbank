import BreadCrumbAdmin from "components/pages/admin/components/BreadCrumbAdmin";
import SeriesPage from "components/pages/admin/SeriesPage";
import WrapperAdmin from "components/pages/admin/WrapperAdmin";

export default function Series() {
    const pageTitle = "Quản lý chương trình"
    const breadcrumbData:any = [
        {name:'Tổng Quan', url:'/'},
        {name: pageTitle, url:null}
    ]
    return ( 
        <WrapperAdmin pageTitle={pageTitle}>
            <div>
                <BreadCrumbAdmin data={breadcrumbData}/>
            </div>
            <div style={{marginTop:'2rem'}}>
                <SeriesPage />
            </div>
        </WrapperAdmin> 
    );
}