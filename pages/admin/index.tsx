import BreadCrumbAdmin from 'components/pages/admin/components/BreadCrumbAdmin';
import WrapperAdmin from 'components/pages/admin/WrapperAdmin';

function AdminPage({session,result}:any) {
  // useEffect(()=>{
  //   async function fetchData() {
  //     const response = await fetch('http://localhost:3006/api/admin/user-guide-screen?page=1&limit=10')
  //     const result = await response.json()
  //     console.log("result",result); 
  //   }
  //   fetchData()
  // },[])
  const pageTitle = "Tổng quan"
  const breadcrumbData:any = [
    {name:pageTitle,url:'/admin'}
  ]
  return (
    <WrapperAdmin userInfo={session?.user} pageTitle={pageTitle}>
       <div>
            <BreadCrumbAdmin data={breadcrumbData}/>
        </div>
        <div style={{marginTop:'2rem'}}>Tổng Quan</div>
    </WrapperAdmin>
  )

 
}

export default AdminPage
