import BreadCrumbAdmin from "components/pages/admin/components/BreadCrumbAdmin";
import ThirdPartyPage from "components/pages/admin/ThirdPartyPage";
import WrapperAdmin from "components/pages/admin/WrapperAdmin";

export default function ThirdParty() {
    const pageTitle = "Quản lý tích hợp"
    const breadcrumbData:any = [
        {name:'Tổng Quan', url: '/'},
        {name: pageTitle, url: null}
    ]
    return ( 
        <WrapperAdmin pageTitle={pageTitle}>
            <div>
                <BreadCrumbAdmin data={breadcrumbData}/>
            </div>
            <div style={{marginTop:'2rem'}}>
                <ThirdPartyPage />
            </div>
        </WrapperAdmin> 
    );
}