import BreadCrumbAdmin from "components/pages/admin/components/BreadCrumbAdmin";
import UserGuidePage from "components/pages/admin/UserGuidePage";
import WrapperAdmin from "components/pages/admin/WrapperAdmin";

function UserGuide() {
    const pageTitle = "Quản lý hướng dẫn sử dụng"
    const breadcrumbData:any = [
        {name:'Tổng Quan', url:'/'},
        {name: pageTitle, url:null}
    ]
    return ( 
        <WrapperAdmin pageTitle={pageTitle}>
        <div>
            <BreadCrumbAdmin data={breadcrumbData}/>
        </div>
        <div style={{marginTop:'2rem'}}>
          <UserGuidePage />
        </div>
    </WrapperAdmin> 
     );
}

export default UserGuide;