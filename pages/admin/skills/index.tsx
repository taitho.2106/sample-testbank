import BreadCrumbAdmin from "components/pages/admin/components/BreadCrumbAdmin";
import SkillsPage from "components/pages/admin/SkillsPage";
import WrapperAdmin from "components/pages/admin/WrapperAdmin";

export default function Skills() {
    const pageTitle = "Quản lý kỹ năng"
    const breadcrumbData:any = [
        {name:'Tổng Quan', url:'/'},
        {name: pageTitle, url:null}
    ]
    return ( 
        <WrapperAdmin pageTitle={pageTitle}>
            <div>
                <BreadCrumbAdmin data={breadcrumbData}/>
            </div>
            <div style={{marginTop:'2rem'}}>
                <SkillsPage />
            </div>
        </WrapperAdmin> 
    );
}