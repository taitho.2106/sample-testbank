import BreadCrumbAdmin from "components/pages/admin/components/BreadCrumbAdmin";
import WrapperAdmin from "components/pages/admin/WrapperAdmin";

function UserGuideScreen() {
    const pageTitle = "Quản lý màn hình"
    const breadcrumbData:any = [
        {name:'Tổng Quan', url:'/'},
        {name: pageTitle, url:null}
    ]
    return ( 
    <WrapperAdmin pageTitle={pageTitle}>
        <div>
            <BreadCrumbAdmin data={breadcrumbData}/>
        </div>
        <div style={{marginTop:'2rem'}}>User Guide Screen</div>
    </WrapperAdmin> 
    );
}

export default UserGuideScreen;
