import BreadCrumbAdmin from "components/pages/admin/components/BreadCrumbAdmin";
import GradePage from "components/pages/admin/GradePage";
import WrapperAdmin from "components/pages/admin/WrapperAdmin";

export default function Grade() {
    const pageTitle = "Quản lý khối lớp"
    const breadcrumbData:any = [
        {name:'Tổng Quan', url:'/'},
        {name: pageTitle, url:null}
    ]
    return ( 
        <WrapperAdmin pageTitle={pageTitle}>
            <div>
                <BreadCrumbAdmin data={breadcrumbData}/>
            </div>
            <div style={{marginTop:'2rem'}}>
                <GradePage />
            </div>
        </WrapperAdmin> 
    );
}