module.exports = {
   apps: [
      {
         name: 'i-test',
         script: 'npm start',
         env: {
            NODE_ENV: 'production'
         }
      }
   ]
};