import { PACKAGES } from "@/interfaces/constants";

const googleTagManagerId = process.env.NEXT_PUBLIC_GTM_ID;

const responseCodeVNPay = {
    '00': 'Giao dịch thành công',
    '01': 'Không tìm thấy giao dịch',
    '02': 'Giao dịch đã được cập nhật trước đó',
    '03': 'Không tìm thấy gói dịch vụ',
    '04': 'Tổng tiền không hợp lệ',
    '97': 'Kiểm tra toàn vẹn dữ liệu thất bại', // Checksum failed
    '07': 'Trừ tiền thành công. Giao dịch bị nghi ngờ (liên quan tới lừa đảo, giao dịch bất thường).',
    '09': 'Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng chưa đăng ký dịch vụ InternetBanking tại ngân hàng.',
    '10': 'Giao dịch không thành công do: Khách hàng xác thực thông tin thẻ/tài khoản không đúng quá 3 lần',
    '11': 'Giao dịch không thành công do: Đã hết hạn chờ thanh toán. Xin quý khách vui lòng thực hiện lại giao dịch.',
    '12': 'Giao dịch không thành công do: Thẻ/Tài khoản của khách hàng bị khóa.',
    '13': 'Giao dịch không thành công do Quý khách nhập sai mật khẩu xác thực giao dịch (OTP). Xin quý khách vui lòng thực hiện lại giao dịch.',
    '24': 'Giao dịch không thành công do: Khách hàng hủy giao dịch',
    '51': 'Giao dịch không thành công do: Tài khoản của quý khách không đủ số dư để thực hiện giao dịch.',
    '65': 'Giao dịch không thành công do: Tài khoản của Quý khách đã vượt quá hạn mức giao dịch trong ngày.',
    '75': 'Ngân hàng thanh toán đang bảo trì.',
    '79': 'Giao dịch không thành công do: KH nhập sai mật khẩu thanh toán quá số lần quy định. Xin quý khách vui lòng thực hiện lại giao dịch',
    '99': 'Các lỗi khác (lỗi còn lại, không có trong danh sách mã lỗi đã liệt kê)',
}

const UNIT_TEST_GROUP = {
    TEXTBOOK: 1,
    TEXTBOOK_SUPPLEMENT: 2,
    INTERNATIONAL_EXAM: 3,
}

const UNIT_TEST_TYPE = {
    PRACTICE_UNIT_TEST: 1,
    PRACTICE_LANGUAGE: 2,
    PRACTICE_SKILL: 3,

    EXAM_15: 4,
    // EXAM_45: 5,
    EXAM_MID_TERM: 6,
    EXAM_END_OF_TERM: 7,

    FOCUS_GOOD_STUDENT: 8,
    FOCUS_SUPPLEMENT: 9,
    FOCUS_REINFORCE: 10,

    INTERNATIONAL: {
        CAMBRIDGE: {
            YOUNG: {
                PRE_A1: 11,
                A1_MOVERS: 12,
                A2_FLYERS: 13,
            },
            A2_KEY: {
                FOR_SCHOOL: 14,
                GENERAL: 15,
            },
            B1: {
                FOR_SCHOOL: 16,
                GENERAL: 17,
            },
        },
        IELTS: {
            GENERAL: 18,
            ACADEMIC: 19,
        },
        APTIS: {
            GENERAL: 20,
            ADVANCE: 21,
        },
        TOEFL: {
            PRIMARY: 22,
            JUNIOR: 23,
            IBT: 24,
            ITP: 25,
        },
        TOEIC: {
            LISTENING_READING: 26,
            SPEAKING_WRITING: 27,
        }
    },
}

const unitTestTypeOptions = [
    {
        groupName: 'practice',
        groupOrder: 1,
        options: [
            { label: 'practice-unit-test', value: UNIT_TEST_TYPE.PRACTICE_UNIT_TEST },
            { label: 'practice-language', value: UNIT_TEST_TYPE.PRACTICE_LANGUAGE },
            { label: 'practice-skill', value: UNIT_TEST_TYPE.PRACTICE_SKILL },
        ]
    },
    {
        groupName: 'exam',
        groupOrder: 2,
        options: [
            { label: 'exam-15', value: UNIT_TEST_TYPE.EXAM_15 },
            // { label: '45-minute tests', value: UNIT_TEST_TYPE.EXAM_45 },
            { label: 'exam-mid-term', value: UNIT_TEST_TYPE.EXAM_MID_TERM },
            { label: 'exam-end-of-term', value: UNIT_TEST_TYPE.EXAM_END_OF_TERM },
        ]
    },
    {
        groupName: 'focus',
        groupOrder: 3,
        accessPackages: [ PACKAGES.STANDARD.CODE, PACKAGES.ADVANCE.CODE ],
        options: [
            { label: 'focus-good-student', value: UNIT_TEST_TYPE.FOCUS_GOOD_STUDENT },
            { label: 'focus-reinforce', value: UNIT_TEST_TYPE.FOCUS_REINFORCE },
            { label: 'focus-supplement', value: UNIT_TEST_TYPE.FOCUS_SUPPLEMENT },
        ]
    },
]

const unitTestTypeInternationalOptions = [
    {
        groupName: 'CAMBRIDGE',
        code:1,
        childrens: [
            {
                groupName: 'Young Learners',
                groupOrder: 1,
                options: [
                    {
                        label: "Cambridge English - Starters",
                        value: UNIT_TEST_TYPE.INTERNATIONAL.CAMBRIDGE.YOUNG.PRE_A1,
                        image: '/images-v2/books/cam-01.png',
                    },
                    {
                        label: 'Cambridge English - Movers',
                        value: UNIT_TEST_TYPE.INTERNATIONAL.CAMBRIDGE.YOUNG.A1_MOVERS,
                        image: '/images-v2/books/cam-02.png',
                    },
                    {
                        label: 'Cambridge English - Flyers',
                        value: UNIT_TEST_TYPE.INTERNATIONAL.CAMBRIDGE.YOUNG.A2_FLYERS,
                        image: '/images-v2/books/cam-03.png',
                    },
                ],
            },
            {
                groupName: 'A2 Key',
                groupOrder: 2,
                options: [
                    {
                        label: 'A2 Key - For Schools',
                        value: UNIT_TEST_TYPE.INTERNATIONAL.CAMBRIDGE.A2_KEY.FOR_SCHOOL,
                        image: '/images-v2/books/A2-01.png',
                    },
                    {
                        label: 'A2 Key - General',
                        value: UNIT_TEST_TYPE.INTERNATIONAL.CAMBRIDGE.A2_KEY.GENERAL,
                        image: '/images-v2/books/a2-02.png',
                    },
                ],
            },
            {
                groupName: 'B1 Preliminary',
                groupOrder: 3,
                options: [
                    {
                        label: 'B1 Preliminary - For Schools',
                        value: UNIT_TEST_TYPE.INTERNATIONAL.CAMBRIDGE.B1.FOR_SCHOOL,
                        image: '/images-v2/books/b1-01.png',
                    },
                    {
                        label: 'B1 Preliminary - General',
                        value: UNIT_TEST_TYPE.INTERNATIONAL.CAMBRIDGE.B1.GENERAL,
                        image: '/images-v2/books/b1-02.png',
                    },
                ],
            },
        ]
    },
    {
        groupName: 'IELTS',
        code:2,
        options: [
            {
                label: 'IELTS - General',
                value: UNIT_TEST_TYPE.INTERNATIONAL.IELTS.GENERAL,
                image: '/images-v2/books/ielts-g.png',
            },
            {
                label: 'IELTS - Academic',
                value: UNIT_TEST_TYPE.INTERNATIONAL.IELTS.ACADEMIC,
                image: '/images-v2/books/ielts-a.png',
            },
        ]
    },
    {
        groupName: 'APTIS',
        code:3,
        options: [
            {
                label: 'APTIS - General',
                value: UNIT_TEST_TYPE.INTERNATIONAL.APTIS.GENERAL,
                image: '/images-v2/books/aptis-g.png',
            },
            {
                label: 'APTIS - Advanced',
                value: UNIT_TEST_TYPE.INTERNATIONAL.APTIS.ADVANCE,
                image: '/images-v2/books/aptis-a.png',
            },
        ]
    },
    {
        groupName: 'TOEFL',
        code:4,
        options: [
            {
                label: 'TOEFL - Primary',
                value: UNIT_TEST_TYPE.INTERNATIONAL.TOEFL.PRIMARY,
                image: '/images-v2/books/toefl-p.png',
            },
            {
                label: 'TOEFL - Junior',
                value: UNIT_TEST_TYPE.INTERNATIONAL.TOEFL.JUNIOR,
                image: '/images-v2/books/toefl-j.png',
            },
            {
                label: 'TOEFL - iBT',
                value: UNIT_TEST_TYPE.INTERNATIONAL.TOEFL.IBT,
                image: '/images-v2/books/toefl-iBT.png',
            },
            {
                label: 'TOEFL - iTP',
                value: UNIT_TEST_TYPE.INTERNATIONAL.TOEFL.ITP,
                image: '/images-v2/books/toefl-iTP.png',
            },
        ]
    },
    {
        groupName: 'TOEIC',
        code:5,
        options: [
            {
                label: 'TOEIC - Listening & Reading',
                value: UNIT_TEST_TYPE.INTERNATIONAL.TOEIC.LISTENING_READING,
                image: '/images-v2/books/toeic-lr.png',
            },
            {
                label: 'TOEIC - Speaking & Writing',
                value: UNIT_TEST_TYPE.INTERNATIONAL.TOEIC.SPEAKING_WRITING,
                image: '/images-v2/books/toeic-sw.png',
            },
        ]
    },
];

const uploadDir = process.env.NEXT_PUBLIC_UPLOAD_DIR;
const userGuideDir = process.env.NEXT_PUBLIC_USER_GUIDE_DIR;
const unitTestExportDir = process.env.NEXT_PUBLIC_UNIT_TEST_EXPORT_DIR;
const logDir = process.env.NEXT_PUBLIC_LOG_DIR;
const IS_INTERNATIONAL_EXAM = 1

const unitTestSources = Object.freeze({
    TEST_BANK: 'TESTBANK',
    I_TEST: 'ITEST',
});

export const commonStatus = Object.freeze({
    INACTIVE: 0,
    ACTIVE: 1,
});

export const commonDeleteStatus = Object.freeze({
    DELETED: 1,
    ACTIVE: 0,
});

const genderEnum = {
    MALE: 1,
    FEMALE: 2,
}

const DATE_DISPLAY_FORMAT = 'DD/MM/YYYY';
const DATE_VALUE_FORMAT = 'YYYY-MM-DD';

const APP_MODE = process.env.NEXT_PUBLIC_APP_MODE

const SALE_ITEM_TYPE = Object.freeze({
    SERVICE_PACKAGE: 'SERVICE_PACKAGE',
    UNIT_TEST: 'UNIT_TEST',
    COMBO_UNIT_TEST: 'COMBO_UNIT_TEST',
    COMBO_SERVICE_PACKAGE: 'COMBO_SERVICE_PACKAGE',
})
const EMAIL_COMPANY = 'info@i-test.vn'
const WEB_URL = process.env.NEXT_PUBLIC_WEB_URL;

const RECEIVE_FEEDBACK_EMAIL = process.env.NEXT_PUBLIC_RECEIVE_FEEDBACK_EMAIL
const LIMIT_CLASS_STUDENT_PACKAGE_BASIC = 60;
const LIMIT_CLASS_PACKAGE_BASIC = 2;

const OBJECT_ASSIGN = Object.freeze({
    CLASS: 1,
    GROUP: 2,
    STUDENT: 3,
});

const unitTestGiftByGrade = {
    'G1': [ 3458 ,3459 ,3460 ,3461 ,3468 ,5722 ,5724 ,5726 ,5727 ,6275 ],
    'G2': [ 3499, 3500, 3501, 3502, 3505, 5733, 5734, 5735, 5738, 6281 ],
    'G3': [ 22254, 22255, 22256, 22257, 3497, 22266, 22267, 22268, 22269, 3517 ],
    'G4': [ 21670, 21673, 21674, 21675, 21787, 21691, 21694, 21695, 21696, 21791 ],
    'G5': [ 5165, 5166, 5167, 5168, 5169, 5713, 5716, 5717, 5718, 5721 ],
    'G6': [ 22208, 22209, 22210, 22211, 22212, 11740, 22240, 22243, 22244, 11479 ],
    'G7': [ 21895, 21896, 21899, 21900, 21904, 11872, 21929, 21930, 21931, 11787 ],
    'G8': [ 21794, 21795, 21797, 21798, 21800, 21808, 21769, 21771, 21772, 21775 ],
    'G9': [ 12114, 12120, 12185, 12187, 12190, 9200, 3247, 8452, 13666, 21910 ],
    'G10': [ 11585, 1482, 11711, 12065, 13666, 17599, 17600, 17628, 17629, 17630 ],
    'G11': [ 22534, 22537, 22559, 22551, 13666, 17599, 17600, 17628, 17629, 17630 ],
    'G12': [ 22534, 22537, 22559, 22551, 13666, 17599, 17600, 17628, 17629, 17630 ],
}

export {
    UNIT_TEST_GROUP,
    UNIT_TEST_TYPE,
    unitTestTypeOptions,
    uploadDir,
    logDir,
    unitTestTypeInternationalOptions,
    IS_INTERNATIONAL_EXAM,
    unitTestSources,
    userGuideDir,
    unitTestExportDir,
    genderEnum,
    googleTagManagerId,
    DATE_DISPLAY_FORMAT,
    DATE_VALUE_FORMAT,
    APP_MODE,
    SALE_ITEM_TYPE,
    EMAIL_COMPANY,
    WEB_URL,
    LIMIT_CLASS_STUDENT_PACKAGE_BASIC,
    LIMIT_CLASS_PACKAGE_BASIC,
    OBJECT_ASSIGN,
    unitTestGiftByGrade,
    RECEIVE_FEEDBACK_EMAIL,
};

export default responseCodeVNPay;