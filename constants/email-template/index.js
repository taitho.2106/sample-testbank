import { dayjs } from "@/utils/date";
import { RECEIVE_FEEDBACK_EMAIL, WEB_URL } from "..";

export const getContentEmailUnitTestInfo = ({ unitTestURL }) => {
    return `<!DOCTYPE html>
    <html>
       <head></head>
       <body>
            <div style="width: 100%;padding: 24px 0;background-color: #eeeeee;margin: 0px auto;">
                <table style="border-spacing: 0;max-width: 640px;margin: 0 auto;background-color: white;padding: 24px;font-family: 'Open Sans', sans-serif;font-weight: 400;font-size: 16px;line-height: 22px;box-shadow: 0px 10px 36px rgba(0, 0, 0, 0.08);border-top: 9px solid #ff6337;">
                    <tbody>
                        <tr>
                            <td style="width: 100%;min-width: 100%;padding: 0;" width="100%">
                                <div style="font-weight: 500;font-size: 22px;line-height: 28px;margin: 8px 0 32px 0;text-align: center;">
                                    Bạn nhận được một yêu cầu làm bài thi. Hãy kiểm tra thông tin chi tiết bên dưới.
                                </div>
                                <div style="border: 1px solid #D1E9FE;padding: 16px 32px;border-radius: 16px;overflow: auto;margin-bottom: 16px;">
                                    <p>Đường dẫn bài làm:<span> <a href="${unitTestURL}">${unitTestURL}</a></span></p>
                                </div>
                                <div style="font-weight: 500;font-size: 15px;line-height: 28px;margin: 16px 0;text-align: center; color: #ff0000;">
                                    Đây là email tự động, vui lòng không trả lời email này!
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
       </body>
    </html>`;
}

export const getContentEmailAccountCreated = ({ username, password }) => {
    return `<!DOCTYPE html>
    <html>
       <head></head>
       <body>
          <h4>Chào bạn, Bạn đã được giáo viên tạo tài khoản học viên trên i-Test. Vui lòng sử dụng thông tin sau để đăng nhập và bắt đầu sử dụng.</h4>
          <div>Link đăng nhập: <a href="https://i-test.vn/login">https://i-test.vn/login</a></div>
          <div>Tên đăng nhập: ${username}</div>
          <div>Mật khẩu: ${password}</div>
          <div style="font-weight: 500;font-size: 15px;line-height: 28px;margin: 16px 0; color: #ff0000;">
            Đây là email tự động, vui lòng không trả lời email này!
          </div>
       </body>
    </html>`;
}


export const getContentEmailNotiNewUserQuestion = ({ customerName, customerPhone, customerEmail, customerFeedback }) => {
    return `<!DOCTYPE html>
    <html>
        <body>
            <p><b>Tên khách hàng:</b> ${customerName}</p>
            <p><b>Số điện thoại:</b> ${customerPhone}</p>
            <p><b>Email:</b> ${customerEmail}</p>
            <p style="white-space: pre-wrap;"><b>Nội dung cần hỗ trợ:</b> ${customerFeedback}</p>
        </body>
    </html>`;
}

export const getContentEmailNewUnitTestAssign = (unitTests, assignBy) => {
    const fullTimeFormat = 'HH:mm DD/MM/YYYY';
    return `<!DOCTYPE html>
    <html>
       <head></head>
       <body>
            <div style="width: 100%;padding: 24px 0;background-color: #eeeeee;margin: 0px auto;">
                <table style="border-spacing: 0;max-width: 640px;margin: 0 auto;background-color: white;padding: 24px;font-family: 'Open Sans', sans-serif;font-weight: 400;font-size: 16px;line-height: 22px;box-shadow: 0px 10px 36px rgba(0, 0, 0, 0.08);border-top: 9px solid #ff6337;">
                    <tbody>
                        <tr>
                            <td style="width: 100%;min-width: 100%;padding: 0;" width="100%">
                                <div style="font-weight: 500;font-size: 22px;line-height: 28px;margin: 8px 0 32px 0;text-align: center;">
                                    Bạn vừa được giáo viên giao đề thi. Hãy kiểm tra thông tin chi tiết bên dưới.
                                </div>
                                ${unitTests.map(unitTestAssign => {
                                    const url = `${WEB_URL}/practice-test/${assignBy?.id}===${unitTestAssign.unitTest?.id}?idAssign=${unitTestAssign.id}`;
                                    const time = dayjs(unitTestAssign.start_date).format(fullTimeFormat) + ' - ' + dayjs(unitTestAssign.end_date).format(fullTimeFormat);
                                    return `
                                        <div style="border: 1px solid #D1E9FE;padding: 16px 32px;border-radius: 16px;overflow: auto;margin-bottom: 16px;">
                                            <p style="margin: 8px 0;"><b>Tên đề thi:</b> ${unitTestAssign.unitTest?.name || '-'}</p>
                                            <p style="margin: 8px 0;"><b>Thời gian làm bài:</b> ${time}</p>
                                            <p style="margin: 8px 0;"><b>Thời lượng bài thi:</b> ${unitTestAssign.unitTest?.time || '-'}</p>
                                            <p style="margin: 8px 0;"><b>Loại đề thi:</b> ${unitTestAssign.unit_type ? 'Kiểm tra' : 'Luyện tập'}</p>
                                            <p style="margin: 8px 0;"><b>Link làm bài:</b> <a href="${url}">${url}</a></p>
                                        </div>
                                    `
                                }).join('')}
                                <div style="font-weight: 500;font-size: 15px;line-height: 28px;margin: 16px 0;text-align: center; color: #ff0000;">
                                    Đây là email tự động, vui lòng không trả lời email này!
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
       </body>
    </html>`;
}

export const titleAccountCreated = `${dayjs().format('Ngày DD/MM/YYYY HH:mm')} - Bạn vừa được khởi tạo tài khoản học viên trên i-Test`;

export  const getContentEmailBuyComboServicePackage = ({ emailTo, user, accountsBuyComboServicePackage }) => {
    const isSendToAdminDTP = emailTo === RECEIVE_FEEDBACK_EMAIL
    return `<!DOCTYPE html>
    <html>
       <head></head>
       <body>
            <div style="width: 100%;padding: 24px 0;background-color: #eeeeee;margin: 0px auto;">
                <table style="border-spacing: 0;max-width: 640px;margin: 0 auto;background-color: white;padding: 24px;font-family: 'Open Sans', sans-serif;font-weight: 400;font-size: 16px;line-height: 22px;box-shadow: 0px 10px 36px rgba(0, 0, 0, 0.08);border-top: 9px solid #ff6337;">
                    <tbody>
                        <tr>
                            <td style="width: 100%;min-width: 100%;padding: 0;">
                                <div style="font-weight: 500;font-size: 22px;line-height: 28px;margin: 8px 0 32px 0;text-align: center;">
                                    ${isSendToAdminDTP ?
                                        `Giáo viên <b>${user.full_name}</b> - ${user.phone} vừa thực hiện thanh toán thành công <b>Gói combo tài khoản</b>.`
                                        :
                                        "Bạn vừa thực hiện thanh toán thành công <b>Gói combo tài khoản</b>."
                                    }
                                </div>
                                <p><b>Danh sách tài khoản gồm:</b></p>
                                <table style="width: 100%; border-collapse: collapse; margin-top: 16px;">
                                      <tr style="background-color: #F9E5D7;">
                                            <th style="text-align: center; height: 40px; border: 1px solid #dddddd; color: #8B5252;">STT</th>
                                            <th style="text-align: left; height: 40px; padding-left: 5px; border: 1px solid #dddddd; color: #8B5252;">Họ và tên</th>
                                            <th style="text-align: left; height: 40px; padding-left: 5px; border: 1px solid #dddddd; color: #8B5252;">Số điện thoại</th>
                                            <th style="text-align: left; height: 40px; padding-left: 5px; border: 1px solid #dddddd; color: #8B5252;">Email</th>
                                      </tr>
                                      ${accountsBuyComboServicePackage.map((account, index) => {
                                          return `
                                              <tr>
                                                   <td style="text-align: center; height: 40px; border: 1px solid #dddddd;">${index + 1}</td>
                                                   <td style="height: 40px; padding-left: 5px; border: 1px solid #dddddd;">${account.full_name}</td>
                                                   <td style="height: 40px; padding-left: 5px; border: 1px solid #dddddd;">${account.phone}</td>
                                                   <td style="height: 40px; padding-left: 5px; border: 1px solid #dddddd;">
                                                        <span><a href="mailto:${account.email}">${account.email}</a></span>
                                                    </td>
                                              </tr>
                                          `
                                      }).join("")}
                                </table>
                                ${isSendToAdminDTP ?
                                    "" :
                                    "<p>Cảm ơn bạn đã tin tưởng và sử dụng sản phẩm tại i-Test.</p>" +
                                    "<p>Trân trọng.</p>"
                                }
                                <div style="font-weight: 500;font-size: 15px;line-height: 28px;margin: 16px 0;text-align: center; color: #ff0000;">
                                    Đây là email tự động, vui lòng không trả lời email này!
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
       </body>
    </html>`;
}