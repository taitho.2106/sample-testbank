import { SALE_ITEM_TYPE } from "./index";

const baseHeader = {
    'Content-Type': 'application/json',
};

// const multipartFormHeader = {
//     'Content-Type': 'multipart/form-data',
// };

const apiConfig = {
    dashboard: {
        turnover: {
            baseURL: `/api/dashboard/turnover`,
            method: 'GET',
            headers: baseHeader,
        },
        user: {
            baseURL: `/api/dashboard/user`,
            method: 'GET',
            headers: baseHeader,
        },
        servicePackage: {
            baseURL: `/api/dashboard/service-package`,
            method: 'GET',
            headers: baseHeader,
        },
        servicePackageDetail: {
            baseURL: `/api/dashboard/service-package/detail`,
            method: 'GET',
            headers: baseHeader,
        },
    },
    servicePackage: {
        list: {
            baseURL: `/api/service-package`,
            method: 'GET',
            headers: baseHeader,
        },
    },
    comboUnitTest: {
        list: {
            baseURL: `/api/sale-item?type=${SALE_ITEM_TYPE.COMBO_UNIT_TEST}`,
            method: 'GET',
            headers: baseHeader,
        },
    },
    student: {
        claimGift: {
            baseURL: '/api/students/claim-gift',
            method: 'GET',
            headers: baseHeader,
        }
    }
};

export default apiConfig;