const path = require('path');

const nextReactSvgConfig = {
  include: path.resolve(__dirname, 'assets/icons'),
};

const withReactSvg = require('next-react-svg')(nextReactSvgConfig);

const nextConfig =
  {
    env: {
      // NEXT_PUBLIC_BASE_API_URL: 'https://testbank.com.vn',
    //   NEXT_PUBLIC_BASE_API_URL: process.env.NEXT_PUBLIC_BASE_API_URL,

      //release
      // NEXT_PUBLIC_BASE_API_URL_SUB_DOMAIN: 'https://testbank.eduhome.com.vn',

      // NEXT_PUBLIC_BASE_API_URL_SUB_DOMAIN: 'http://testbank.dtpds.com:3000',
    //   NEXT_PUBLIC_BASE_API_URL_SUB_DOMAIN: process.env.NEXT_PUBLIC_BASE_API_URL_SUB_DOMAIN,

      NEXT_PUBLIC_PRACTICE_TEST_BASE_API_URL: 'https://api.dtpmira.com',
      NEXT_PUBLIC_PRACTICE_TEST_GRADE_6_APP_KEY:
        'zk13UTwYGeJT6Nkd2+JeSZ23HehpR/ZJ17taRRJe34A/9MWpXqZ38lRTOQaUZ9dgT629KmVym0gMHPf/E+8pbA==',
      NEXT_PUBLIC_PRACTICE_TEST_GRADE_7_APP_KEY:
        'DVKpgvb2e49eW3w/IlJTklce6idB0WSf4nE3j+uP9/+j4rqwY1hUDpRU6egJkSUvFw1LnKIiy8jQKE/MYcsE1w==',
      NEXT_PUBLIC_PRACTICE_TEST_GRADE_8_APP_KEY:
        'JcNMe7mAkeXya8Warh75jIyKHDRFEg4WjlZ3vbHSdo21U1oj5mjSFN8uGk3JLYd1EOpoFmq/K01Eana6P0/xeg==',
      NEXT_PUBLIC_PRACTICE_TEST_GRADE_9_APP_KEY:
        '4JdDNOcJ9MMC0sWo8lnEZOfMWjttVOZud5M4WS0ZgJyO0ac4oiW30nONAPx0i2L3V2+WZVQ8JHhaoLPhXTudag==',
      NEXT_PUBLIC_PRACTICE_TEST_MODE: 'ACTIVE',
      NEXT_PUBLIC_PRACTICE_TEST_PLATFORM: 'WEB',
      NEXT_PUBLIC_PRACTICE_TEST_PREFIX_AUTH_TOKEN:
        'eyJzdWIiOiI2NjAiLCJ0eXBlIjoyLCJpYXQiOjE2MjQ4NTQ1MzF9###',
      NEXT_PUBLIC_PRACTICE_TEST_USER_ID: 'hungtest1',
      NEXT_PUBLIC_PRACTICE_TEST_VERSION: '26-10-2021',

      //db release
      // MYSQL_HOST: '192.168.9.9',
      // MYSQL_PORT: '3306',
      // MYSQL_DATABASE: 'testbank',
      // MYSQL_USER: 'testbankdb',
      // MYSQL_PASSWORD: 'Testbank@#$2022',

      // MYSQL_HOST: '192.168.9.20',
      // MYSQL_PORT: '3306',
      // MYSQL_DATABASE: 'testbank_dev',
      // MYSQL_USER: 'testbank_db',
      // MYSQL_PASSWORD: 'Dtps@2021',

      //app EDUHOME test
      // NEXT_PUBLIC_APP_ID:2214,
      // NEXT_PUBLIC_APP_SECRET_KEY:'xyJMp8JghYVh/uuqXF8sR83w6eCAU4EpuIlcU9NZNZm8pAeMbz2AT8VPKEtcYGsmEtIHWo7ZwbHFTLIrKNJACg==',
      // EDUHOME_BASE_API:'http://eduhome.dtpds.com',
      // EDUHOME_ACC_BASE_API:"http://appmgr.dtpds.com/accmgr",

      // //app EDUHOME Release
    //   NEXT_PUBLIC_APP_ID: process.env.NEXT_PUBLIC_APP_ID,
    //   NEXT_PUBLIC_APP_SECRET_KEY: process.env.NEXT_PUBLIC_APP_SECRET_KEY,
    //   EDUHOME_BASE_API: process.env.EDUHOME_BASE_API,
    //   EDUHOME_ACC_BASE_API: process.env.EDUHOME_ACC_BASE_API,

      EDUHOME_AUTH: "Basic YWRtaW46UFRVREBEVFBPQDEyMzQ1",
    },
    eslint: {
      ignoreDuringBuilds: true,
    },
    //todo remove this feature in future
    typescript: {
      ignoreBuildErrors: true,
    },
    i18n: {
      locales: ['en', 'vi'],
      defaultLocale: 'vi',
      localeDetection: false
    },
    reactStrictMode: true,
    async headers(){
      return [
        {
          source: '/payment',
          headers: [
            {
              key: 'Cache-Control',
              value: 'no-cache, no-store, must-revalidate',
            },
          ],
        },
      ]
    },
    webpack: (config) => {
      const { rules } = config.module;
      // Find the array of "style rules" in the webpack config.
      // This is the array of webpack rules that:
      // - is inside a 'oneOf' block
      // - contains a rule that matches 'file.css'
      const styleRules = (rules.find((m) => m.oneOf && m.oneOf.find(({ test: reg }) => reg.test('file.css'))) || {}).oneOf;
      if (!styleRules) return config;
      // Find all the webpack rules that handle CSS modules
      // Look for rules that match '.module.css' and '.module.scss' but aren't being used to generate
      // error messages.
      const cssModuleRules = [
        styleRules.find(({ test: reg, use }) => reg.test('file.module.css') && use.loader !== 'error-loader'),
        styleRules.find(({ test: reg, use }) => reg.test('file.module.scss') && use.loader !== 'error-loader'),
      ].filter((n) => n); // remove 'undefined' values
      // Add the 'localsConvention' config option to the CSS loader config in each of these rules.
      cssModuleRules.forEach((cmr) => {
        // Find the item inside the 'use' list that defines css-loader
        const cssLoaderConfig = cmr.use.find(({ loader }) => loader.includes('css-loader'));
        if (cssLoaderConfig && cssLoaderConfig.options && cssLoaderConfig.options.modules) {
          // Patch it with the new config
          cssLoaderConfig.options.modules.exportLocalsConvention = 'camelCase';
        }
      });
      return config;
    },
  }

  module.exports = withReactSvg(nextConfig);
