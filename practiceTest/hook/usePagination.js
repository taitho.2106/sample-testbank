import { useContext } from 'react'

import { questionListTransform } from '../api/dataTransform'
import { PageTestContext } from '../interfaces/contexts'

const usePagination = () => {
  const { currentPart, currentGroupBtn,currentGroupBtn6, currentQuestion, partData, progress } =
    useContext(PageTestContext)

  const questionList = questionListTransform(partData, currentPart.index)

  const getDirectionBtnState = (type, length) => {
    if (type === 'prev' && currentQuestion.index.includes(0)) {
      if (currentPart.index <= 0) {
        return '--disabled'
      }
      return '--prev-part'
    } else if (type === 'next') {
      if (
        !(
          currentPart.index < partData.length - 1 ||
          !currentQuestion.index.includes(questionList.length - 1)
        )
      )
        return '--disabled'
      if (currentQuestion.index.includes(length - 1)) return '--next-part'
    }
    return ''
  }
  const checkDoneQuestionType6 = (i, j,selectorList, selectorGroup)=>{
    if (selectorGroup[0].group === 1) {
      if(
        selectorGroup[0].value !== null &&
        Array.isArray(selectorGroup[0].value) &&
        Array.isArray(selectorGroup[0].value[0]) &&
        selectorGroup[0].value[0].length > 0
      ){
        return true;
      }
      
    } else {
      const firstIndex = selectorList.findIndex(
        (item) => item.id === selectorGroup[0].id,
      )
      if(
        selectorList[firstIndex].value !== null &&
        Array.isArray(selectorList[firstIndex].value) &&
        selectorList[firstIndex].value[j - firstIndex] !== null &&
        Array.isArray(selectorList[firstIndex].value[j - firstIndex]) && 
        selectorList[firstIndex].value[j - firstIndex].length > 0
      ){
        return true;
      }
    }
    return false;
  }
  const checkDoneQuestionTypeDefault = (i, j,selectorList, selectorGroup,)=>{
    if (selectorGroup[0].group === 1) {
      if (selectorGroup[0].value !== null && selectorGroup[0].value !== ''){
        return true;
      }
    } else {
      const firstIndex = selectorList.findIndex(
        (item) => item.id === selectorGroup[0].id,
      )
      if (selectorList[firstIndex].value !== null &&
        selectorList[firstIndex].value !== '' &&
        Array.isArray(selectorList[firstIndex].value) &&
        selectorList[firstIndex].value[j - firstIndex] !== null &&
        selectorList[firstIndex].value[j - firstIndex] !== ''
      ){
        return true;
      }
    }
    return false;
  }
  const checkDoneQuestionTypeDLFC = (i, j,selectorList, selectorGroup)=>{
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
      )
      const userAnswerQuestion = selectorList[firstIndex];
    if(
      userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value) &&
      (userAnswerQuestion.value[j - firstIndex]?.isMatched 
        || userAnswerQuestion.value[j - firstIndex]?.isSelected
        || userAnswerQuestion.value[j - firstIndex]?.color
        )
    ){
      return true;
    }
    return false;
  }
  const checkDoneQuestionTypeSO = (i, j,selectorList, selectorGroup)=>{
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
      )
      const userAnswerQuestion = selectorList[firstIndex];
      let userValue= userAnswerQuestion?.value || null;
      if(
        userValue &&
        Array.isArray(userValue)
      ){
       const userSelected = userValue.map(m=>m.isSelected);
       userSelected.sort((m,n)=>{
        if(m){
          return -1;
        }
        return 1;
       })
        if(userSelected[j - firstIndex]){
          return true;
        }
      }
    return false;
  }
  const checkDoneQuestionTypeMG = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion &&  userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value) &&
      (userAnswerQuestion.value[indexSubQuestion]?.isMatched)
    ){
      return true;
    }
    return false;
  }
  const checkDoneQuestionTypeMR = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
      const valueItem = (userAnswerQuestion.value[indexSubQuestion]);
      if( valueItem || Number.isInteger(valueItem)){
        return true;
      }
    }
    return false;
  }
  const checkDoneQuestionTypeMIX1 = (i, j,selectorList, selectorGroup)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
        const itemArr = userAnswerQuestion.value;
        // part1 and 2: LS1, MC5
        const arr = [...itemArr[0]||[], ...itemArr[1]||[]];
        if(arr[indexSubQuestion] === false || arr[indexSubQuestion] === true){ //boolean
          return true;
        }
        if(Number.isInteger(arr[indexSubQuestion])){ //number.
          return true;
        }
    }
    return false;
  }
  const checkDoneQuestionTypeMIX2 = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
        const itemArr = userAnswerQuestion.value;
        // part1 and 2: TF2, MC5
        const arr = [...itemArr[0]||[], ...itemArr[1]||[]];
        if(arr[indexSubQuestion] || arr[indexSubQuestion] == false){ //boolean
          return true;
        }
    }
    return false;
  }
  const checkDoneQuestionTypeMC11 = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
        const itemArr = userAnswerQuestion.value;
        const subQuestion = itemArr[indexSubQuestion];
        if(subQuestion && Array.isArray(subQuestion) && subQuestion.some(m=>m?.value)){
          return true;
        }
    }
    return false;
  }
  const checkDoneQuestionTypeDD2 = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
        const itemArr = userAnswerQuestion.value;
        const subQuestion = itemArr[indexSubQuestion];
        if(subQuestion && subQuestion.isMatched){
          return true;
        }
    }
    return false;
  }
  const getNumberBtnState = (key, numberPerPage) => {
    
    let className = ''
    const selectorList = progress.state[currentPart.index]
    const selectorGroup = selectorList.filter(
      (item) => item.id === selectorList[key].id,
    )
    const question = questionList.find(m=>m.id == selectorList[key].id);
    const currentQuestionType = question?.activityType||null
    if (selectorGroup.length > 0) {
      switch(currentQuestionType){
        case 6:
          if(checkDoneQuestionType6(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 28:
        case 34:
        case 39:
        case 40:
        case 42:
          if(checkDoneQuestionTypeDLFC(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 31:
          if(checkDoneQuestionTypeSO(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 9: 
        case 23: 
        case 33: 
        case 37: 
          if(checkDoneQuestionTypeMG(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
          case 12: 
          if(checkDoneQuestionTypeMR(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 17: 
          if(checkDoneQuestionTypeMIX1(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 18: 
          if(checkDoneQuestionTypeMIX2(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 41: 
          if(checkDoneQuestionTypeMC11(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 36: 
          if(checkDoneQuestionTypeDD2(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        default:
          if(checkDoneQuestionTypeDefault(currentPart.index,key,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
      }
    }
    if (currentQuestion.index.includes(key)) className += '--active '
    if(numberPerPage === 12){
      if (
        key < currentGroupBtn.index * numberPerPage ||
        key >= (currentGroupBtn.index + 1) * numberPerPage
      )
        className += '--hidden'
    }else{
      if (
        key < currentGroupBtn6.index * numberPerPage ||
        key >= (currentGroupBtn6.index + 1) * numberPerPage
      )
        className += '--hidden'
    }
    
    return className
  }

  const paginate = (list, target, partIndex = null, numberPerPage) => {
    if (partIndex !== null) currentPart.setIndex(partIndex)
    // find first duplicate index
    const targetItem = list.findIndex((item) => item.id === list[target].id)
    if (targetItem === -1) return
    // add all duplicate index
    let pushArr = []
    for (let count = 0; count < list[targetItem].group; count++)
      pushArr.push(targetItem + count)
    currentQuestion.setIndex(pushArr)

    // move to current group btn
    const groupIndex = Math.ceil((parseInt(target) + 1) / 12) - 1 ?? 0
    const groupIndex6 = Math.ceil((parseInt(target) + 1) / 6) - 1 ?? 0
    currentGroupBtn.setIndex(() => groupIndex)
    currentGroupBtn6.setIndex(() => groupIndex6)
  }

  return { getDirectionBtnState, getNumberBtnState, paginate }
}

export default usePagination
