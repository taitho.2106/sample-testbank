import { Fragment, useContext, useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { CustomHeading } from '../../../../atoms/heading'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradientWithHeader } from '../../../blockBottomGradient/BlockBottomGradientWithHeader'
import PageEnd from '@rsuite/icons/PageEnd'
import { checkIsScrollable } from '../../../../../interfaces/functionary'
import { convertStrToHtml } from '../../../../../../utils/string'

export const Game5 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const question = data?.question || ''

  const trueAnswers = answers.filter(m=>!m.answer.startsWith("*")).map((answer) =>
    answer.answerList
      .trim()
      .split('/')
      .findIndex((item) => item.trim() === answer.answer.trim()),
  )
  const finalSentences = question.split('%s%')

  const pickers = answers.filter(group=> !group.answer.startsWith("*")).map((group) => group.answerList.split('/'))

  const { mode, currentQuestion } = useContext(PageTestContext)

  const [isScrollable, setIsScrollable] = useState(false);
  const [scrollToBottom, setScrollToBottom] = useState(false)

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()

  const [currentGroupAnswer, setCurrentGroupAnswer] = useState(null)
  const [isOpenCollapse, setIsOpenCollapse] = useState(false)
  

  const defaultArray = getProgressActivityValue(
    Array.from(Array(pickers.length), () => null),
  )
  const [chosenAnswers, setChosenAnswers] = useState(defaultArray)

  const drawer = useRef(null)

  const checkDanger = (i) => {
    if (mode.state === TEST_MODE.review && userAnswer.status[i] === false)
      return '--danger'
    return ''
  }

  const checkInline = (i) => {
    if (
      ([TEST_MODE.play, TEST_MODE.review].includes(mode.state) &&
        chosenAnswers[i] !== null &&
        pickers[i][chosenAnswers[i]]) ||
      mode.state === TEST_MODE.check
    )
      return '--inline'
    return ''
  }

  const checkSuccess = (i) => {
    if (
      mode.state === TEST_MODE.check ||
      (mode.state === TEST_MODE.review && userAnswer.status[i] === true)
    )
      return '--success'
    return ''
  }

  const closeCollapse = (e) => {
    if (
      e.target.classList.contains('__space') ||
      e.target.classList.contains('__drawer') ||
      e.target.closest('.__drawer')
    )
      return
    setIsOpenCollapse(false)
  }

  const generateClassName = (i) => {
    let className = ''
    className += checkInline(i)
    className += ' '
    className += checkDanger(i)
    className += ' '
    className += checkSuccess(i)
    return className
  }

  const handleSpaceClick = (e) => {
    let time = 0
    if (isOpenCollapse) {
      time = 500
      setIsOpenCollapse(false)
    }
    const index = e.target.getAttribute('data-index')
    setCurrentGroupAnswer(parseInt(index))
    setTimeout(() => setIsOpenCollapse(true), time)
  }

  const handlePickerClick = (groupIndex, index) => {
    // update value
    let currentChosenAnswers = chosenAnswers
    currentChosenAnswers[groupIndex] = index
    // check isCorrect
    let statusArr = []
    trueAnswers.forEach((item, i) =>
      statusArr.push(item === currentChosenAnswers[i] ? true : false),
    )
    // save progress
    setProgressActivityValue(currentChosenAnswers, statusArr)
    // display
    setChosenAnswers([...currentChosenAnswers])
    setIsOpenCollapse(false)
  }
  //change question
  useEffect(() => {
    const defaultArray = getProgressActivityValue(
      Array.from(Array(pickers.length), () => null),
    )
    setChosenAnswers(defaultArray)
  }, [currentQuestion])

  useEffect(() => {
    if (userAnswer.group !== 1 && userAnswer.value === null)
      setProgressActivityValue(
        Array.from(Array(pickers.length), () => null),
        Array.from(Array(pickers.length), () => false),
      )

    window.addEventListener('click', closeCollapse)
    return () => window.removeEventListener('click', closeCollapse)
  }, [])

  useEffect(() => {
    if (drawer?.current && mode.state === TEST_MODE.play)
      drawer.current.scrollTo({ top: 0, behaviour: 'smooth' })
  }, [isOpenCollapse, drawer.current])

  const seeMore = () => {
    const element = document.getElementsByClassName("pt-o-game-5")[0]
    scrollTo(element, element.scrollHeight, 600);
  }

  function scrollTo(element, to, duration) {
    if (duration <= 0) return;
    const difference = Math.abs(to - element.scrollTop);
    const perTick = difference / duration * 10;

    setTimeout(function() {
        element.scrollTop = scrollToBottom ? (element.scrollTop - perTick) : (element.scrollTop + perTick);
        if (element.scrollTop === to) return;
        scrollTo(element, to, duration - 10);
    }, 10);
    setScrollToBottom(!scrollToBottom)
  }

  useEffect(() => {
    const element = document.getElementsByClassName("pt-o-game-5")[0];
    setIsScrollable(checkIsScrollable(element))
    element.addEventListener('scroll', () => {
      if (element.scrollHeight - Math.ceil(element.scrollTop) <= element.clientHeight)
      {
        setScrollToBottom(true)
      }
      if (element.scrollTop === 0) {
        setScrollToBottom(false)
      }
    })
    return () => {
      element.removeEventListener('scroll', () =>{
        //
      })
    }
  }, [])

  let spaceIndex = -1
  return (
    <GameWrapper className="pt-o-game-5">
      <div className="pt-o-game-5__container" data-animate="fade-in">
        <BlockBottomGradientWithHeader
          className="--sm-header vertical-issue"
          headerChildren={
            <CustomHeading tag="h6" className="__heading">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          }
          customChildren={
            <div
              ref={drawer}
              className={`__drawer ${isOpenCollapse ? '--show' : ''}`}
            >
              {pickers.map((picker, i) => (
                <Fragment key={i}>
                  {picker.map(
                    (item, j) =>
                      i === currentGroupAnswer && (
                        <div
                          key={j}
                          className={`__drawer-item ${
                            j === chosenAnswers[i] ? '--active' : ''
                          }`}
                          style={{
                            pointerEvents:
                              mode.state === TEST_MODE.play ? 'all' : 'none',
                          }}
                          onClick={() =>
                            mode.state === TEST_MODE.play &&
                            handlePickerClick(i, j)
                          }
                        >
                          {item}
                        </div>
                      ),
                  )}
                </Fragment>
              ))}
            </div>
          }
        >
          <Scrollbars universal={true}>
            <div className="__content">
              {finalSentences.map((item, j) => {
                const isExample  = answers[j-1]?.answer.startsWith("*");
                if (j !== 0 && !isExample) spaceIndex++
                return (
                  <Fragment key={j}>
                    {j !== 0 && (
                      <span
                        className={`__space ${isExample?" --example":generateClassName(spaceIndex)} `}
                        data-index={spaceIndex}
                        style={{
                          pointerEvents:
                            mode.state === TEST_MODE.play && !isExample ? 'all' : 'none',
                        }}
                        onClick={(e) =>
                          mode.state === TEST_MODE.play && !isExample && handleSpaceClick(e)
                        }
                      >
                        {/* check mode */}
                        {!isExample && mode.state === TEST_MODE.check &&
                          pickers[spaceIndex][trueAnswers[spaceIndex]]}
                        {/* play mode & review mode */}
                        {!isExample && [TEST_MODE.play, TEST_MODE.review].includes(
                          mode.state,
                        ) &&
                          chosenAnswers[spaceIndex] !== null &&
                          pickers[spaceIndex][chosenAnswers[spaceIndex]]}
                          {isExample && answers[j-1].answer.substring(1)}
                      </span>
                    )}
                    <FormatText tag="span">{convertStrToHtml(item)}</FormatText>
                  </Fragment>
                )
              })}
            </div>
          </Scrollbars>
        </BlockBottomGradientWithHeader>
      </div>
      {
        isScrollable &&
          <div className="pt-o-game-5__see-more"
          onClick={seeMore}
          >
            <PageEnd rotate={scrollToBottom ? -90 : 90} color="white" style={{fontSize: 28}}/>
          </div>
      }
    </GameWrapper>
  )
}
