import { Fragment, useContext, useEffect, useRef, useState } from 'react'

import PageEnd from '@rsuite/icons/PageEnd'
import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { checkIsScrollable } from '../../../../../interfaces/functionary'
import { CustomHeading } from '../../../../atoms/heading'
import { FormatText } from '../../../../molecules/formatText'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradientWithHeader } from '../../../blockBottomGradient/BlockBottomGradientWithHeader'

export const GameFB8 = ({ data }) => {
    let answerGroup = data.answers || []
    let answerData = []
    for (const element of answerGroup){
        const item = element;
        if(element.answer.includes('%/%')){
            const answers = element.answer.split("%/%")
            const images = element.image.split("|")
            for(let j=0; j< answers.length; j++){
                answerData.push(
                    {
                    ...item,answer:answers[j], image:images[j]
                    }
                )
            }
        }
        else{
          answerData.push(item)
        }
    }

    const exampleKeyWord = answerData.filter((ans) => ans.answer.startsWith('*'))
    const answerKeyWord = answerData.filter((ans) => !ans.answer.startsWith('*'))

    const imageNote = data.audioScript 
    const answers = data?.answers || []
    const imageInstruction = data?.imageInstruction || ''
    const instruction = data?.instruction || ''
    const question = data?.question || ''
    const questionInput = answers.filter((item) => !item.answer.startsWith('*'))
    const indexEx = answers.findIndex((item)=> item.answer.startsWith('*'))
    const trueAnswer = answers.filter((m)=> !m.answer.startsWith('*')).map((item) => item.answer)
    // const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
    const questionArr = convertStrToHtml(question.replace(/\%s\%/g,"###")).split('###')
    const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
        useProgress()

    const { mode, currentPart } = useContext(PageTestContext)

    const [isScrollable, setIsScrollable] = useState(false)
    const [scrollToBottom, setScrollToBottom] = useState(false)

    const defaultTextArr = getProgressActivityValue(
        Array.from(Array(questionInput.length), () => ''),
    )
    const [fillTextArr, setFillTextArr] = useState([...defaultTextArr])

    const contentRef = useRef(null)

    const checkDanger = (i) => {
        if (
            mode.state === TEST_MODE.review &&
            userAnswer.value &&
            userAnswer.value[i] !== '' &&
            userAnswer.status[i] === false
        ){
            return '--danger'
        }
        return ''
    }

    const checkEmpty = (i) => {
        if (fillTextArr[i] === '' && mode.state !== TEST_MODE.check){
            return '--empty'
        }
        return ''
    }

    const checkSuccess = (i) => {
        if (
        (mode.state === TEST_MODE.review &&
            userAnswer.value &&
            userAnswer.value[i] !== '' &&
            userAnswer.status[i] === true) ||
        mode.state === TEST_MODE.check
        ){
            return '--success'
        }
        return ''
    }

    const generateClassName = (i) => {
        let className = ''
        className += checkEmpty(i)
        className += ' '
        className += checkDanger(i)
        className += ' '
        className += checkSuccess(i)
        return className
    }

    const onPaste = (event) => {
        const index = parseInt(event.target.dataset['index'])
        event.preventDefault()
        const data = event.clipboardData.getData('text').replace(/\r\n/g, ' ').trim()
        event.target.innerHTML = data
        fillTextArr[index] = data
        setFillTextArr([...fillTextArr])
    }

    const handleInputBlur = () => {
        const answerValue = [...fillTextArr]
        let statusArr = []
        trueAnswer.forEach((item, i) => {
        const splitCheck = item.toLowerCase().trim().split('%/%').map(m=>m?.trim()).filter(m=>m)
        const textI = answerValue[i]||""
        statusArr.push(
            splitCheck.includes(
                textI
                ?.replace(/&nbsp;/g, ' ')
                .trim()
                .toLowerCase(),
            ) ||
            splitCheck.includes(
                textI
                ?.replace(/&nbsp;/g, ' ')
                .trim()
                .toLowerCase() + '.',
            )
            ? true
            : false,
        )
        })

        setProgressActivityValue(answerValue, statusArr)
    }

    const handleInputChange = (index, text) => {
        const textArr = [...fillTextArr]
        if (typeof textArr[index] !== 'string') return
        textArr[index] = text
        setFillTextArr(textArr)
    }

    useEffect(()=>{
        if(indexEx !== -1){
            const example = contentRef.current.querySelectorAll('.__editableEX')            
            example.forEach((item, i)=>{
                item.innerHTML = answers[indexEx].answer.substring(1)
            })
        }
    },[])

    useEffect(() => {
        if (!contentRef?.current) return
        const spaceList = contentRef.current.querySelectorAll('.__editable')

        spaceList.forEach((item, i) => {
            if ([TEST_MODE.review, TEST_MODE.play].includes(mode.state)) {
                if (userAnswer?.value) {
                    // console.log(`userAnswer=${i}=`,userAnswer?.value)
                        item.innerHTML = userAnswer.value[i] || ''
                        if (!userAnswer.value[i]) item.classList.add('--empty')
                } else {
                    item.innerHTML = ''
                    item.classList.add('--empty')
                }
                if (i !== 0 && mode.state === TEST_MODE.play) return
                // item.focus()
            } else if (mode.state === TEST_MODE.check) {
                item.innerHTML = trueAnswer[i].split('%/%').map(m=>m?.trim()).filter(m=>m).join(" / ")
            }
        })
    }, [contentRef, mode.state, data.id, currentPart])

    const seeMore = () => {
        const element = document.getElementsByClassName(
        'pt-o-game-fb8-image__container',
        )[0]
        scrollTo(element, element.scrollHeight, 600)
    }

    function scrollTo(element, to, duration) {
        if (duration <= 0) return
        let difference = Math.abs(to - element.scrollTop)
        let perTick = (difference / duration) * 10

        setTimeout(function () {
            element.scrollTop = scrollToBottom
                ? element.scrollTop - perTick
                : element.scrollTop + perTick
            if (element.scrollTop === to) return
            scrollTo(element, to, duration - 10)
        }, 10)
        setScrollToBottom(!scrollToBottom)
    }
    
    useEffect(() => {
        const element = document.getElementsByClassName('pt-o-game-fb8-image__container',)[0]
        setIsScrollable(checkIsScrollable(element))
        element.addEventListener('scroll', () => {
            if (
                element.scrollHeight - Math.ceil(element.scrollTop) <=
                element.clientHeight
            ) {
                setScrollToBottom(true)
            }
            if (element.scrollTop === 0) {
                setScrollToBottom(false)
            }
        })
        return () => {
            element.removeEventListener('scroll', () => {
            //
            })
        }
    }, [])

    const [allKeyWord, setAllKeyWord] = useState([])
    useEffect(() => {
        let currentIndex = answerKeyWord.length
        let randomIndex
        if ([TEST_MODE.play].includes(mode.state)) {
            while(currentIndex !== 0){
                randomIndex = Math.floor(Math.random() * currentIndex)
                currentIndex--
        
                [answerKeyWord[currentIndex], answerKeyWord[randomIndex]] = [answerKeyWord[randomIndex], answerKeyWord[currentIndex]];
            }
            setAllKeyWord([...exampleKeyWord, ...answerKeyWord])
        } else if ([TEST_MODE.check,TEST_MODE.review ].includes(mode.state)) {
            setAllKeyWord([...exampleKeyWord, ...answerKeyWord])
        }
    }, [data.id, mode.state])

    const renderClassName = (value) => {
        let answerClassName = ''
        if (value?.length === 4) {
            answerClassName = '--images-flex-wrap'
        }
        else{
            answerClassName = '--images-multi-line'
        }
        return answerClassName
    }

    const [isZoomMain, setIsZoomMain] = useState(false)
    const [isZoomKey, setIsZoomKey] = useState(false)
    const [zoomStrMain, setZoomStrMain] = useState('')
    const [zoomStrKey, setZoomStrKey] = useState('')

    let x = -1;

    return (
        <GameWrapper className="pt-o-game-fb8-image">
        <div className="pt-o-game-fb8-image__container">
            <div className='pt-o-game-fb8-image__left'>
                <div className="__instruction">
                    <span>{instruction}</span>
                </div>
                <div className='box-all-images'>
                    <div className='__image-instruction'>
                        <div className='image-main'>
                            <img 
                                src={getFullUrl(imageInstruction)}
                                alt='instruction_image'
                                onClick={() => {
                                    // setIsZoom(true)
                                    setIsZoomMain(true)
                                    setZoomStrMain(getFullUrl(imageInstruction))
                                }}
                            />
                            {zoomStrMain.length > 0 && (
                            <ImageZooming
                                data={{
                                    alt: `instruction_image`,
                                    src: `${zoomStrMain}`,
                                }}
                                status={{
                                    state: isZoomMain,
                                    setState: setIsZoomMain,
                                }}
                            />  
                            )}
                        </div>
                        <div className='text-main'>
                            <span>{imageNote}</span>
                        </div>
                    </div>
                    <div className={`list-image-answers`}>
                    {zoomStrKey.length > 0 && (
                        <ImageZooming
                            data={{
                                alt: 'image_answer',
                                src: `${zoomStrKey}`,
                            }}
                            status={{
                                state: isZoomKey,
                                setState: setIsZoomKey,
                            }}
                        />
                    )}
                    {allKeyWord.map((answerItem, index) => {
                        return (
                            <div 
                                key={`image-ans-${index}`}
                                className={`list-image-answers-cell ${renderClassName(answerData)}`}
                            >   
                                
                                <div className='list-image-answers-cell__item' key={index}>
                                    <img 
                                        src={getFullUrl(answerItem.image)} 
                                        alt='image_answer'
                                        onClick={() => {
                                            setIsZoomKey(true)
                                            setZoomStrKey(getFullUrl(answerItem.image))
                                        }}
                                    />
                                </div>
                                <div className={`list-image-answers-cell__text ${answerItem.answer.startsWith('*') ? '--example' : '--keyword'}`}>
                                    <span>{answerItem.answer.replace(/\*/g, '')}</span>
                                </div>
                                
                            </div>
                        )
                    })}
                    </div>

                </div>
            </div>
                <div className='pt-o-game-fb8-image__right'>
                <BlockBottomGradientWithHeader
                    headerChildren={
                    <CustomHeading tag="h6" className="__heading">
                        <FormatText tag="span">{instruction}</FormatText>
                    </CustomHeading>
                    }
                >
                    <Scrollbars universal={true}>
                    <div ref={contentRef} className="__content">
                        {questionArr.map((item, i) => {
                        if(i === 0  || i === indexEx + 1){

                        }else{
                            x = x + 1
                        }
                        return(
                        <Fragment key={i}>
                            {i !== 0 && i !== indexEx + 1 && (
                            <span 
                                data-index={x}
                                className={`__editable ${generateClassName(x)}`}
                                contentEditable={
                                    mode.state === TEST_MODE.play ? true : false
                                }
                                style={{
                                    pointerEvents:
                                        mode.state === TEST_MODE.play ? 'all' : 'none',
                                }}
                                onBlur={() =>
                                    mode.state === TEST_MODE.play && handleInputBlur()
                                }
                                onInput={(e) =>{
                                    mode.state === TEST_MODE.play &&
                                    handleInputChange(e.target.dataset.index, e.target.innerHTML)}
                                }
                                onPaste={(e) => {
                                    mode.state === TEST_MODE.play && onPaste(e)
                                }}
                            ></span>
                            )}
                            {i !== 0 && i === indexEx + 1 && (
                            <span
                                className={`__editableEx`}
                                contentEditable={false}
                            >
                                {answers[indexEx].answer.substring(1)}
                            </span>
                            )}
                            <span
                            dangerouslySetInnerHTML={{
                                __html: (item),
                            }}
                            ></span>
                        </Fragment>
                        )})}
                    </div>
                    </Scrollbars>
                </BlockBottomGradientWithHeader>
                </div>
            
            {isScrollable && (
            <div className="pt-o-game-fb8__see-more" onClick={seeMore}>
                <PageEnd
                rotate={scrollToBottom ? -90 : 90}
                color="white"
                style={{ fontSize: 28 }}
                />
            </div>
            )}
        </div>
        </GameWrapper>
    )
}
const getFullUrl = (url) => {
    const result = ''
    if (url != null && url != '') {
      return `${location.origin}/upload/${url}`
    }
    return result
  }