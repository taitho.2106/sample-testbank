import { useRef, useState, useContext, useEffect } from 'react'

import { useWindowSize } from 'utils/hook'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'
export default function GameSelectObject1 ({data}){
  const [width, height] = useWindowSize()
  const renderUIGameSelectObject1 = (data) => {
    if (width <= 1024 && window.matchMedia('(orientation: portrait)').matches) {
      return <GameSelectObject1Container key={`mobile_${data.id}_portrait`} data={data} />
    }
    return <GameSelectObject1Container key={`webpc_${data.id}`} data={data} />
  }
  return <>{renderUIGameSelectObject1(data)}</>
}
function GameSelectObject1Container({ data }) {
  const answer = data?.answers.filter(item => !item.isExam) ?? []
  const trueAnswer = answer
  const { getProgressActivityValue, setProgressActivityValue } = useProgress()
  const isPortrait = useRef( window.matchMedia("(orientation: portrait)").matches)
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const { mode } = useContext(PageTestContext)

  const defaultAnswer = getProgressActivityValue(
    answer.map((item,mIndex)=>({
      isSuccess: item.isSelected,
      isSelected: false
    }))
  )
  const answerGroup = useRef(defaultAnswer)
  const intervalRef = useRef(null)
  const audio = useRef(null)
  const [scaleNumber, setScaleNumber] = useState(0)
  const ratioRef = useRef(0)
  const [offsetImage, setOffsetImage] = useState({ top: 0, left: 0 })
  useEffect(() => {
    return () => {
        setScaleNumber(0)
    }
  }, [])
  useEffect(()=>{
    intervalRef.current = setInterval(() => {
      const container = document.getElementById('pt-o-game-select-object-1-content-id')
      const imageElement = container.querySelector('.image-instruction')
      const wraperImage = container.querySelector('.wrap-image')
      const isPortraitTemp = window.matchMedia('(orientation: portrait)').matches
      if(!imageElement || !imageElement.complete || imageElement.naturalWidth == 0) return
      const ratio = imageElement.width / imageElement.naturalWidth
      if(imageElement.height == 0 || imageElement.width == 0 || wraperImage.offsetHeight == 0 || wraperImage.offsetWidth == 0) return

      if(imageElement.height > wraperImage.offsetHeight){
        imageElement.style.height = Math.floor(wraperImage.offsetHeight) + 'px'
        imageElement.style.width = ''
      }
      if(ratio!=ratioRef.current){
        const listImage = wraperImage.querySelectorAll('.item-image-content')
        listImage.forEach((m)=>{
          onLoadImageItem(m)
        })
        setScaleNumber(ratio)
        ratioRef.current = ratio
      }else
      if(isPortraitTemp != isPortrait.current){
        isPortrait.current = isPortraitTemp
        const listImage = wraperImage.querySelectorAll('.item-image-content')
        listImage.forEach((m) => {
          onLoadImageItem(m)
        })
      }
    },200)
    return () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current)
      }
    }
  },[])
  const [width, height] = useWindowSize()
  const getPointDraw = (element, ratio) => {
    const canvas = document.createElement('canvas')
    canvas.style.visibility = 'hidden'
    const ctx = canvas.getContext('2d')
    const imgHeight = element.naturalHeight * ratio
    const imgWidth = element.naturalWidth * ratio
    ctx.drawImage(element, 0, 0, imgWidth, imgHeight)
    
    for(let i = 0; i < imgHeight; i++){
      const pixelData = ctx.getImageData(imgWidth / 2, i, 1, 1).data
      if (
        pixelData[0] !== 0 ||
        pixelData[1] !== 0 ||
        pixelData[2] !== 0 ||
        pixelData[3] !== 0
      ) {
        return { x: imgWidth / 2, y: i - 8}
      }
    }
  }
  useEffect(()=>{
    const elements = document.querySelectorAll("#pt-o-game-select-object-1-content-id .item-image.--answer-div")
    elements.forEach((imageElement, index)=>{
      setTimeout(() => {
        const classList =  [...imageElement.classList];
        imageElement.classList=[]
        imageElement.classList.add("item-image")
        imageElement.classList.add("--answer-div")
        setTimeout(() => {
          classList.forEach(m=> {
            imageElement.classList.add(m)
          })
        }, 0);
      }, 10);
    })
  },[mode.state])
  useEffect(() => {
    if(width ==0 || height ==0) return
    isPortrait.current = window.matchMedia("(orientation: portrait)").matches;
    const imageItem = document.querySelectorAll(
      '#pt-o-game-select-object-1-content-id .item-image',
    )
    const isPortraitTemp = window.matchMedia('(orientation: portrait)').matches
    if(isPortraitTemp != isPortrait.current){
      imageItem.forEach(m=> m.style.display = "none")
      const imageInstruction = document.getElementsByClassName('image-instruction')[0]
      //set size default
      // imageInstruction.style.visibility = 'hidden'
      imageInstruction.style.height = ""
      imageInstruction.style.width= "";
      imageInstruction.parentElement.style.height = ""
      imageInstruction.parentElement.style.width= "";
      imageInstruction.parentElement.parentElement.style.height = ""
      imageInstruction.parentElement.parentElement.style.width= "";
    }
  }, [width, height])
  
  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])
  // const [selected, setSelected] = useState(defaultSelected)
  const onClickDeleted = (imageId,e) => {
    e.preventDefault()
    e.stopPropagation()
    try {
      const imageElement = document.getElementById(imageId)
      if(imageElement.classList.contains('image-selected')){
        setTimeout(() => {
          const classList =  [...imageElement.classList];
          imageElement.classList=[]
          imageElement.classList.add("item-image")
          imageElement.classList.add("--answer-div")
          setTimeout(() => {
            classList.forEach(m=> {
              imageElement.classList.add(m)
            })
          }, 0);
        }, 10);
        const iconDeleted = imageElement.querySelector('.icon-deleted-image')
        iconDeleted.style.visibility = 'hidden'

        const indexAnswer = answer.findIndex(item => item.key === imageId)
        if(mode.state === TEST_MODE.play && answerGroup.current[indexAnswer]){
            //handler data
            answerGroup.current[indexAnswer].isSelected = false
            setProgressActivityValue(
              answerGroup.current, 
              answerGroup.current.map((item)=> item.isSuccess && item.isSelected)
            )
        }
      }
    } catch (error) {
      console.log("error",error)
    }
    
  }
  const onClickImage = (index, e) => {
    e.preventDefault()
    e.stopPropagation()
    try {
      const imageElement = e.currentTarget;
      const iconDeleted = imageElement.querySelector('.icon-deleted-image')
      // const lenghtTrue = selected.filter(m => m === true).length
      const countAnswerSelected = answer.filter(m=>m.isSelected).length;
      const userSelected = answerGroup.current.filter(m=>m.isSelected).length;
      if(userSelected >= countAnswerSelected) return
      if(iconDeleted){
        iconDeleted.style.visibility = 'visible'
        setTimeout(() => {
          const classList =  [...imageElement.classList];
          imageElement.classList=[]
          imageElement.classList.add("item-image")
          imageElement.classList.add("--answer-div")
          setTimeout(() => {
            classList.forEach(m=> {
              imageElement.classList.add(m)
            })
          }, 0);
        }, 10);

        const indexAnswer = answer.findIndex(item => item.key === e.currentTarget.id)

        if(mode.state === TEST_MODE.play && answerGroup.current[indexAnswer]) {
            //handler data
             answerGroup.current[indexAnswer].isSelected = true
            setProgressActivityValue(
              answerGroup.current, 
              answerGroup.current.map((item)=> {return item.isSuccess && item.isSelected})
            )
        }
      }
    } catch (error) {
      console.log("error",error);
    }
  }
  const setIsShowTapescript = (isShow) => {
    const elementScript = document.getElementById(
      'pt-o-game-select-object-1-script',
    )
    const elementRight = document.querySelector(
      '.pt-o-game-select-object-audio__right',
    )
    const elementLeft = document.querySelector(
      '.pt-o-game-select-object-audio__left',
    )
    if (isShow) {
      elementScript.style.display = 'block'
      elementLeft.style.display = 'none'
      elementRight.style.display = 'none'
    } else {
      elementScript.style.display = 'none'
      elementLeft.style.display = 'flex'
      elementRight.style.display = 'flex'
    }
  }

const checkClassName = (i) =>{
  let className = ''
  if(answerGroup.current[i]){
    if(answerGroup.current[i].isSelected){
      className = 'image-selected'
    }else{
      className = ''
    }
  }
 return className
}
const checkDanger = (i) => {
    if (
      (mode.state === TEST_MODE.review && answerGroup.current[i].isSelected && !trueAnswer[i].isSelected ) ||
        (mode.state === TEST_MODE.check && (!trueAnswer[i].isSelected))
    )
        return '--danger'
    return ''
}

const checkSuccess = (i) => {
    if (
        (mode.state === TEST_MODE.review && answerGroup.current[i].isSelected && trueAnswer[i].isSelected) ||
        (mode.state === TEST_MODE.check && (trueAnswer[i].isSelected))
    )
        return '--success'
    return ''
}

const generateClassName = (i) => {
  if(i >= 0){
    let className = ''
    className = checkSuccess(i)
    if(!className){
      className = checkDanger(i)
    }
    if(!className){
      className = checkClassName(i)
    }
    if(!className){
      className = 'image-default'
    }
    return className
  }
} 
const onLoadImageInstruction=(e)=>{
    const element = e.currentTarget
    if(element.height > element.parentElement.offsetHeight && element.parentElement.offsetHeight !=0){
      element.style.height = element.parentElement.offsetHeight + 'px'
    }
    const ratio = element.width / element.naturalWidth
    setScaleNumber(ratio)
    element.style.visibility = 'visible'
    ratioRef.current = ratio
    setOffsetImage({ top: element.offsetTop, left: element.offsetLeft })
}
const onLoadImageItem = (element) => {
  const posLeftID = element.parentElement.querySelector('.icon-deleted-image')
  const posLeftIT = element.parentElement.querySelector('.icon-success-image')
  const posLeftIF = element.parentElement.querySelector('.icon-danger-image')

  const offsetAnswer = getPointDraw(element, scaleNumber)
  if(offsetAnswer){
    posLeftID.style.left = `${offsetAnswer.x}px`
    posLeftIT.style.left = `${offsetAnswer.x}px`
    posLeftIF.style.left = `${offsetAnswer.x}px`
  
    posLeftID.style.top = `${offsetAnswer.y}px`
    posLeftIT.style.top = `${offsetAnswer.y}px`
    posLeftIF.style.top = `${offsetAnswer.y}px`
  }  
}
let iAnswer = -1
  return (
    <GameWrapper className="pt-o-game-select-object-audio pt-o-game-select-object-1">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div
        data-animate="fade-in"
        className="pt-o-game-select-object-audio__container"
      >
        <>
          <div
            className="__paper"
            id="pt-o-game-select-object-1-script"
            style={{ display: 'none' }}
          >
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
          <BlockWave
            className={`pt-o-game-select-object-audio__left 
            ${
              mode.state !== TEST_MODE.play ? 'pt-o-game-circle__result' : ''
            }`}
            // data-animate="fade-in"
          >
            <AudioPlayer
              className="__player"
              isPlaying={isPlaying}
              setIsPlaying={() => setIsPLaying(!isPlaying)}
            />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <div
            className="pt-o-game-select-object-audio__right"
            // data-animate="fade-in"
          >
            {mode.state !== TEST_MODE.play && (
              <CustomButton
                className="__tapescript"
                onClick={() => setIsShowTapescript(true)}
              >
                Tapescript
              </CustomButton>
            )}
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-select-object-1-content-id">
            <div className="wrap-image" 
            style={{
                  objectFit: 'contain',
                  maxHeight: '100%',
                  position: 'relative',
                  maxWidth: '100%',
                }}>
                  <img
                    className="image-instruction"
                    alt="image-instruction"
                    data-animate="fade-in"
                    src={data?.imageInstruction}
                    style={{ visibility: 'hidden', maxHeight: '100%'}}
                      onLoad={(e)=>{
                        onLoadImageInstruction(e)
                      }}
                  />
                  {data?.answers?.map((item, mIndex) => {
                    if(!item.isExam){
                      iAnswer++
                    }
                    const p = item.position.split(':')
                    const width = (p[2] - p[0]) * scaleNumber
                    const height = (p[3] - p[1]) * scaleNumber
                    const left = p[0] * scaleNumber
                    const top = p[1] * scaleNumber

                    const style = mode.state != TEST_MODE.play || item.isExam ? { pointerEvents: 'none' } : {}
                    style.position = 'absolute'
                    style.left = `${offsetImage.left + left}px`
                    style.top = `${offsetImage.top + top}px`
                    style.width = `${Math.floor(width)}px`
                    style.height = `${Math.floor(height)}px`
                    return(
                    <div
                      key={item.key}
                      id={item.key}
                      data-index={`${item.isExam ? -1 : iAnswer}`}
                      onClick={(e)=>{onClickImage(mIndex,e)}}
                      className={`item-image ${item.isExam ? '--example-div' : `--answer-div ${generateClassName(iAnswer)}`}`}
                      style={style}
                    >
                      <img 
                        src={item.image}
                        data-animate="fade-in"
                        alt=''
                        className='item-image-content'
                        onLoad={(e)=>{
                          onLoadImageItem(e.currentTarget)
                        }}
                      />
                      <img 
                        src='/images/icons/ic-deleted-so.png'
                        alt=''
                        className='icon-deleted-image'
                        onClick={(e)=>onClickDeleted(item.key,e)}
                      />
                      <img 
                        src='/images/icons/ic-true-so.png'
                        alt=''
                        className='icon-success-image'
                      />
                      <img 
                        src='/images/icons/ic-false-so.png'
                        alt=''
                        className='icon-danger-image'
                      />
                    </div>
                  )})}
                </div>
            </div>
          </div>
        </>
      </div>
    </GameWrapper>
  )
}
