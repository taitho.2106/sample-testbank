import { useContext, useState, Fragment, useEffect } from 'react'

import { convertStrToHtml } from 'utils/string';

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { checkIsScrollable } from '../../../../../interfaces/functionary'
import { CustomHeading } from '../../../../atoms/heading'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradient } from '../../../blockBottomGradient'
import { BlockPaper } from '../../../blockPaper'

export const GameMix2 = ({ data }) => {
  const questionList = data?.questionList || []
  const instruction = data?.instruction || ''
  const questionInstruction = data?.questionInstruction || ''
  
  const dataSL1 = questionList.filter(item => item.activityType === 8)[0]
  const dataMC5 = questionList.filter(item => item.activityType === 15)[0]

  const answers = dataSL1?.answers || []
  const choices = answers[0].answerList.split('/')

  const trueAnswers = answers.map((item) => item.answer)

  const questionsGroup = dataMC5?.questionsGroup || []

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } = useProgress()
  const { mode,currentQuestion } = useContext(PageTestContext)

  const [isScrollable, setIsScrollable] = useState(false);
  const [scrollToBottom, setScrollToBottom] = useState(false)

  const defaultData = getProgressActivityValue(
    [
      Array.from(Array(answers.length), () => null),
      Array.from(Array(questionsGroup.length), (e, i) => null),
    ]
  );

  const [action, setAction] = useState(defaultData[0])
  // const [statusSL1, setStatusSL1] = useState(defaultData[0])
  const [statusSL1, setStatusSL1] = useState(userAnswer.status[0])

  const [radio, setRadio] = useState(defaultData[1])
  // const [statusMC5, setStatusMC5] = useState(defaultData[1])
  const [statusMC5, setStatusMC5] = useState(userAnswer.status[1])

  const trueAnswer2 = questionsGroup.map((list) =>
    list.answers.findIndex((item) => item.isCorrect === true),
  )

  const generateClassName2 = (i, j) => {
    let className = ''
    className += checkPrimary(i, j)
    className += ' '
    className += checkDanger(i, j)
    className += ' '
    className += checkSuccess(i, j)
    return className
  }

  const checkPrimary = (i, j) => {
    if (
      mode.state === TEST_MODE.play &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--checked'
    return ''
  }

  const checkDanger = (i, j) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.status[1] &&
      userAnswer.status[1][i] === false &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--danger'
    return ''
  }

  const checkSuccess = (i, j) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.status[1] &&
        userAnswer.status[1][i] === true &&
        radio[i] !== undefined &&
        radio[i] === j) ||
      (mode.state === TEST_MODE.check && trueAnswer2[i] === j)
    )
      return '--success'
    return ''
  }

  const checkRadio = (index, bool) => {
    // update value
    let radioValueArr = action
    radioValueArr[index] = bool
    // check isCorrect
    let statusArr = []
    trueAnswers.forEach((item, i) => {
      statusArr.push(radioValueArr[i] === item ? true : false)
    })

    // set status game SL1
    setStatusSL1(statusArr)
  
    // save progress
    setProgressActivityValue([radioValueArr, radio], [statusArr, statusMC5])

    // display
    setAction([...radioValueArr])
  }

  const handleRadioClick2 = (i, j) => {
    let beforeArr = radio
    if (beforeArr[i] === undefined) return
    beforeArr[i] = j
    // check isCorrect
    let statusArr = []
    trueAnswer2.forEach((item, i) =>
      statusArr.push(item === beforeArr[i] ? true : false),
    )

    // set status game MC5
    setStatusMC5(statusArr)

    // save progress
    setProgressActivityValue([ action, beforeArr], [statusSL1, statusArr])
    // display
    setRadio([...beforeArr])
  }

  const seeMore = () => {
    const element = document.getElementsByClassName("__content")[1]
    scrollTo(element, element.scrollHeight, 600);
  }

  function scrollTo(element, to, duration) {
    if (duration <= 0) return;
    const difference = Math.abs(to - element.scrollTop);
    const perTick = difference / duration * 10;

    setTimeout(function() {
        element.scrollTop = scrollToBottom ? (element.scrollTop - perTick) : (element.scrollTop + perTick);
        if (element.scrollTop === to) return;
        scrollTo(element, to, duration - 10);
    }, 10);
    setScrollToBottom(!scrollToBottom)
  }
  //change question
  useEffect(() => {
    const defaultData = getProgressActivityValue(
      [
        Array.from(Array(answers.length), () => null),
        Array.from(Array(questionsGroup.length), (e, i) => null),
      ]
    );
    setAction(defaultData[0])
    setStatusSL1(userAnswer.status[0])
    setRadio(defaultData[1])
    setStatusMC5(userAnswer.status[1])
  }, [currentQuestion])
  useEffect(() => {
    const element = document.getElementsByClassName("__content")[1];
    setIsScrollable(checkIsScrollable(element))
    element.addEventListener('scroll', () => {
      if (element.scrollHeight - Math.ceil(element.scrollTop) <= element.clientHeight)
      {
        setScrollToBottom(true)
      }
      if (element.scrollTop === 0) {
        setScrollToBottom(false)
      }
    })
    return () => {
      element.removeEventListener('scroll', () =>{})
    }
  }, [])

  return (
    <GameWrapper className="pt-o-game-mix2">
      <div className="pt-o-game-mix2__left">
        <BlockPaper>
          <div className="__instrustion">{instruction}</div>
          <div className="__content">
            <span
              dangerouslySetInnerHTML={{
                __html: convertStrToHtml(questionInstruction),
              }}
            ></span>
          </div>
        </BlockPaper>
      </div>
      <div className="pt-o-game-mix2__right">
        <BlockBottomGradient className="__container">
          <div className="__content">
            <div className="__list">
              {/* <Scrollbars universal={true}> */}
                <div className="__header">
                  <CustomHeading tag="h6" className="__description">
                    <FormatText tag="span">{dataSL1?.instruction}</FormatText>
                  </CustomHeading>
                </div>
                <div className="__title">
                  {choices.map((item, i) => (
                    <CustomHeading key={i} className="__heading">
                      <span>{item}</span>
                    </CustomHeading>
                  ))}
                </div>
                {answers.map((item, i) => (
                  <RadioItem
                    key={i}
                    choices={choices}
                    data={item.text}
                    index={i}
                    trueValue={trueAnswers[i]}
                    state={action}
                    setState={checkRadio}
                  />
                ))}
                <div className="des2">
                  <FormatText tag="span">{dataMC5?.instruction}</FormatText>
                </div>
                {questionsGroup.map((item, i) => (
                  <div key={i} className="__radio-group">
                    <CustomHeading tag="h6" className="__radio-heading">
                      {convertStrToHtml(item.question).split('%s%').map((splitItem, j) => (
                        <Fragment key={j}>
                          {j % 2 === 1 && <div className="__space"></div>}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: (splitItem),
                            }}
                          ></span>
                        </Fragment>
                      ))}
                    </CustomHeading>
                    <div className="__radio-list">
                      {item.answers.map((childItem, j) => (
                        <div
                          key={j}
                          className={`__radio-item ${generateClassName2(i, j)}`}
                          style={{
                            pointerEvents:
                              mode.state === TEST_MODE.play ? 'all' : 'none',
                          }}
                          onClick={() =>
                            mode.state === TEST_MODE.play && handleRadioClick2(i, j)
                          }
                        >
                          <span
                            dangerouslySetInnerHTML={{
                              __html: convertStrToHtml(childItem.text),
                            }}
                          ></span>
                        </div>
                      ))}
                    </div>
                  </div>
                ))}
              {/* </Scrollbars> */}
            </div>
          </div>
        </BlockBottomGradient>
        {/* {isScrollable && 
          <div className="pt-o-game-mix2__see-more"
          onClick={seeMore}
          >
            <PageEnd rotate={scrollToBottom ? -90 : 90} color="white" style={{fontSize: 28}}/>
          </div>
        } */}
      </div>
      
    </GameWrapper>
  )
}

const RadioItem = ({ choices, data, index, trueValue, state, setState }) => {
  const { mode } = useContext(PageTestContext)

  const handleRadioClick = (i, val) => setState(i, val)

  const checkDanger = (radio) => {
    if (
      mode.state === TEST_MODE.review &&
      state[index] === radio &&
      radio !== trueValue
    )
      return '--danger'
    return ''
  }

  const checkSuccess = (radio) => {
    if (
      (mode.state === TEST_MODE.review &&
        state[index] === radio &&
        radio === trueValue) ||
      (mode.state === TEST_MODE.check && radio === trueValue)
    )
      return '--success'
    return ''
  }

  const ganerateClassName = (radio) => {
    let className = ''
    className += checkDanger(radio)
    className += ' '
    className += checkSuccess(radio)
    className += ' '
    return className
  }

  return (
    <div className="__item">
      <div className="__text">
        <FormatText tag="p">{convertStrToHtml(data)}</FormatText>
      </div>
      <div
        className={`__radio ${state[index] === choices[0] ? '--true' : ''
          } ${ganerateClassName(choices[0])}`}
        style={{ pointerEvents: mode.state == TEST_MODE.play ? 'all' : 'none' }}
        onClick={() =>
          mode.state === TEST_MODE.play &&
          state[index] !== choices[0] &&
          handleRadioClick(index, choices[0])
        }
      ></div>
      <div
        className={`__radio ${state[index] === choices[1] ? '--false' : ''
          } ${ganerateClassName(choices[1])}`}
        style={{ pointerEvents: mode.state == TEST_MODE.play ? 'all' : 'none' }}
        onClick={() =>
          mode.state === TEST_MODE.play &&
          state[index] !== choices[1] &&
          handleRadioClick(index, choices[1])
        }
      ></div>
      <div
        className={`__radio ${state[index] === choices[2] ? '--not-given' : ''
          } ${ganerateClassName(choices[2])}`}
        style={{ pointerEvents: mode.state == TEST_MODE.play ? 'all' : 'none' }}
        onClick={() =>
          mode.state === TEST_MODE.play &&
          state[index] !== choices[2] &&
          handleRadioClick(index, choices[2])
        }
      ></div>
    </div>
  )
}
