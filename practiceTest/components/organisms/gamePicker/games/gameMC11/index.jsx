import { Fragment, useContext, useEffect, useRef, useState } from 'react'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'

export const GameMC11 = ({ data }) => {
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const instruction = data?.instruction || ''
    const questionsGroup = data?.questionsGroup || []
    
    const questionsGroupExample = questionsGroup.filter((m) =>m.isExample)
    const questionsGroupGeneral = questionsGroup.filter((m) => !m.isExample)
    const trueAnswer = questionsGroupGeneral.map((list) =>
        list.answers.map((item) => item.isCorrect)
    )

    const audio = useRef(null)
    const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
        useProgress()
    const { mode, currentQuestion } = useContext(PageTestContext)

    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)
    const [isShowTapescript, setIsShowTapescript] = useState(false)
    
    const defaultRadio = getProgressActivityValue(
        questionsGroupGeneral.map((item)=> item.answers.map((m => ({
            value: false,
            isCorrect: m.isCorrect,
        }))))
    )

    const [radio, setRadio] = useState(defaultRadio)

    const handleRadioClick = (i, j) => {
        if (radio[i][j] === undefined) return
        const lengthAnswer = questionsGroupGeneral[i].answers.filter((item) => item.isCorrect)
        const lengthRadio = radio[i].filter(item => item.value)
        if(lengthRadio.length < lengthAnswer.length && !radio[i][j].value){
            radio[i][j].value = true
            setRadio([...radio])
            // save progress
            let statusArr = []
            const correctArr = radio.map((item) => item.map((m)=> m.isCorrect === m.value))
            statusArr = correctArr.map(item => item.filter(m => !m))
            setProgressActivityValue(radio, statusArr.map(item => item.length === 0))
                
        }else{
            radio[i][j].value = false
            setRadio([...radio])
            // save progress
            let statusArr = []
            const correctArr = radio.map((item) => item.map((m)=> m.isCorrect === m.value))
            statusArr = correctArr.map(item => item.filter(m => !m))
            setProgressActivityValue(radio, statusArr.map(item => item.length === 0))
        }
    }
        
    //change question
    useEffect(() => {
        const defaultRadio = getProgressActivityValue(
            questionsGroupGeneral.map((item)=> item.answers.map((m => ({
            value: false,
            isCorrect: m.isCorrect,
        }))))
        )
        setRadio(defaultRadio)
    }, [currentQuestion])
    
    const checkPrimary = (i, j) => {
        if (
            mode.state === TEST_MODE.play &&
            radio[i][j] !== undefined
            && radio[i][j].value === true
        )
            return '--checked'
        return ''
    }

    const checkDanger = (i, j) => {
        if (
            mode.state === TEST_MODE.review && 
            radio[i][j] !== undefined && 
            radio[i][j].value === true && 
            radio[i][j].value !== radio[i][j].isCorrect
        )
            return '--danger'
        return ''
    }

    const checkSuccess = (i, j) => {
        if (
            (mode.state === TEST_MODE.review && 
                radio[i][j] !== undefined && 
                radio[i][j].value === true && 
                radio[i][j].value === radio[i][j].isCorrect) ||
            (mode.state === TEST_MODE.check && trueAnswer[i][j])
        )
            return '--success'
        return ''
    }

    const generateClassName = (i, j) => {
        let className = ''
        className += checkPrimary(i, j)
        className += ' '
        className += checkDanger(i, j)
        className += ' '
        className += checkSuccess(i, j)
        return className
    }

    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        else {
            const countDownValue = audio.current.duration - audio.current.currentTime
            if (countDownValue > 0)
                setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
            else {
                setIsPLaying(false)
                audio.current.currentTime = 0
            }
        }
    }
    const renderItemEx = (i) => {
        let className = ''
        if(questionsGroupExample[i] && 
            (questionsGroupExample[i].answers.length === 4 || 
                questionsGroupExample[i].answers.length === 2)){
            className = '--column-ex-2'
        }
        return className
    }
    const renderItem = (i) => {
        let className = ''
        if(questionsGroupGeneral[i] && 
            (questionsGroupGeneral[i].answers.length === 4 || 
                questionsGroupGeneral[i].answers.length === 2)){
                className = '--column-2'
        }
        return className
    }
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])

    const canTouch = () => {
        const isTouch = 'ontouchstart' in window;
        return isTouch ? '--touch' : '--no-touch'
    }
    return (
        <GameWrapper className="pt-o-game-mc11-audio">
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>
            <div data-animate="fade-in" className='pt-o-game-mc11-audio__container'>
            {isShowTapescript ? (
                <div className="__paper">
                    <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
                        <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                    </div>
                    <BlockPaper>
                        <div className="__content">
                            <FormatText>{audioScript}</FormatText>
                        </div>
                    </BlockPaper>
                </div>
            ) : (
        <>
            <BlockWave className={`pt-o-game-mc11-audio__left ${mode.state !== TEST_MODE.play ? 'pt-o-game-mc7__result' : ''}`}>
                <AudioPlayer
                    className="__player"
                    isPlaying={isPlaying}
                    setIsPlaying={() => setIsPLaying(!isPlaying)}
                />
                <span className="__duration">{countDown}</span>
            </BlockWave>
            <div className="pt-o-game-mc11-audio__right" data-animate="fade-in">
                {mode.state !== TEST_MODE.play && (
                    <CustomButton
                    className="__tapescript"
                    onClick={() => setIsShowTapescript(true)}
                    >
                    Tapescript
                    </CustomButton>
                )}
                <div className="__header">
                    <FormatText tag="p">{instruction}</FormatText>
                </div>
                <div className="__content">
                {questionsGroupExample && questionsGroupExample.length > 0 && (
                    <div className="__content-example">
                        <div className="__title">
                            <span>Example:</span>
                        </div>
                        <hr />
                        <div className={`__content`}>
                        {questionsGroupExample.map((itemQuestion, index) => {
                            return (
                            <div
                                key={`example-question-${index}`}
                                className={`mc11-example-group`}
                            >
                                <div className={`mc11-example-group__radio-answer ${renderItemEx(index)}`}>
                                    {itemQuestion.answers.map((itemChild, i) => (
                                        <div 
                                            key={`example-question-ans-${i}`}
                                            className="mc11-example-group__radio-answer__ex-item"
                                            style={{
                                                boxShadow: itemChild.isCorrect ? '0px 0px 0px 1px #2dd238' : '0px 0px 0px 1px #d4e9ff'
                                            }}
                                        >
                                            <img src={itemChild.imageAns} alt=''/>
                                            <div className={`icon-true ${itemChild.isCorrect}`}>
                                                <img 
                                                    src={'/images/icons/ic-checked-true.png'}
                                                    alt=''
                                                />
                                            </div>
                                        </div>
                                    ))}
                                </div>
                            </div>
                            )
                        })}
                        </div>
                    </div>
                )}
                {questionsGroupGeneral && questionsGroupGeneral.length > 0 && (
                <div className="__content-question">
                    <div className="__title">
                        <span>{questionsGroupGeneral.length === 1 ? 'Question:' : 'Questions:'}</span>
                    </div>
                    <hr />
                    <div className="__content">
                    {questionsGroupGeneral.map((item, i) => {
                        return (
                        <Fragment key={`question-item-${i}`}>   
                            {i > 0 && (
                                <div 
                                    className='space-answers'
                                >
                                    <hr />
                                </div>
                            )}
                            <div  className={`__radio-group ${canTouch()}`}>
                                <div className={`__radio-list ${renderItem(i)}`}>
                                {item.answers.map((childItem, j) => (
                                    <div
                                        key={j}
                                        className={`__radio-item ${generateClassName(i,j,)}`}
                                        style={{
                                            pointerEvents:
                                            mode.state === TEST_MODE.play ? 'all' : 'none',
                                        }}
                                        onClick={() =>
                                            mode.state === TEST_MODE.play &&
                                            handleRadioClick(i, j)
                                        }
                                    >
                                        <img src={childItem.imageAns}alt=''/>
                                        <div className={`icon-true ${generateClassName(i,j,)}`}>
                                            <img 
                                                src={
                                                    generateClassName(i,j,).trim() === '--checked' ||  generateClassName(i,j,).trim() === '--success' ? 
                                                        '/images/icons/ic-checked-true.png' : 
                                                        '/images/icons/ic-checked-false.png' 
                                                    }
                                                alt=''
                                            />
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                        </Fragment>
                        )
                    })}
                    </div>
                </div>
                )}
                </div>
            </div>
        </>
        )}
            </div>
        </GameWrapper>
    )
}