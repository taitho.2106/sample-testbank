import { useContext, useState, useEffect } from 'react'

import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { CustomHeading } from '../../../../atoms/heading'
import { CustomImage } from '../../../../atoms/image'
import { FormatText } from '../../../../molecules/formatText'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'

export const GameMC2 = ({ data }) => {
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const questionsGroup = data?.questionsGroup || []
  const questionsGroupExample = questionsGroup.filter((m) =>
    m.question.startsWith('*'),
  )
  const questionsGroupGeneral = questionsGroup.filter(
    (m) => !m.question.startsWith('*'),
  )
  const trueAnswer = questionsGroupGeneral.map((list) =>
    list.answers.findIndex((item) => item.isCorrect === true),
  )

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()
  const { mode,currentQuestion } = useContext(PageTestContext)
  const [isZoom, setIsZoom] = useState(false)

  const [radio, setRadio] = useState([])

  const checkPrimary = (i, j) => {
    if (
      mode.state === TEST_MODE.play &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--checked'
    return ''
  }

  const checkDanger = (i, j) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.status[i] === false &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--danger'
    return ''
  }

  const checkSuccess = (i, j) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.status[i] === true &&
        radio[i] !== undefined &&
        radio[i] === j) ||
      (mode.state === TEST_MODE.check && trueAnswer[i] === j)
    )
      return '--success'
    return ''
  }

  const generateClassName = (i, j) => {
    let className = ''
    className += checkPrimary(i, j)
    className += ' '
    className += checkDanger(i, j)
    className += ' '
    className += checkSuccess(i, j)
    return className
  }

  const handleRadioClick = (i, j) => {
    
    let beforeArr = radio
    if (beforeArr[i] === undefined) return
    beforeArr[i] = j
    // check isCorrect
    let statusArr = []
    trueAnswer.forEach((item, i) =>
      statusArr.push(item === beforeArr[i] ? true : false),
    )
    // save progress
    setProgressActivityValue(beforeArr, statusArr)
    // display
    setRadio([...beforeArr])
  }

  //change question
  useEffect(() => {
    const defaultRadio = getProgressActivityValue(
      Array.from(Array(questionsGroupGeneral.length), (e, i) => null),
    )
    setRadio(defaultRadio)
  }, [currentQuestion])

  return (
    <GameWrapper className="pt-o-game-mc2-image">
      <div className="pt-o-game-mc2-image__left" >
        <div className="__image">
          <CustomImage
            className="__image-container"
            alt={'img'}
            src={`${imageInstruction}`}
            onClick={() => setIsZoom(true)}
          />
          <ImageZooming
            data={{ alt: 'img', src: `${imageInstruction}` }}
            status={{
              state: isZoom,
              setState: setIsZoom,
            }}
          />
        </div>
      </div>
      <div className="pt-o-game-mc2-image__right">
        <div className="__header">
          <FormatText tag="p">{instruction}</FormatText>
        </div>
        <div className="__content">
            {questionsGroupExample && questionsGroupExample.length > 0 && (
              <div className="__content-example">
                <div className="__title">
                  <span>
                    {`Example${questionsGroupExample.length > 1 ? 's' : ''}:`}
                  </span>
                </div>
                <div className="__content">
                  {questionsGroupExample.map((itemQuestion, index) => {
                    const html = convertStrToHtml(
                      itemQuestion.question.substring(1),
                    ).replaceAll('%s%', `<div class="__space"></div>`)
                    return (
                      <div
                        key={`question-${index}`}
                        className={`mc2-a-example`}
                      >
                        <div className={`mc2-a-example__question`}>
                          <div className="mc2-a-example__question__title">
                            <span
                              dangerouslySetInnerHTML={{
                                __html: html,
                              }}
                            ></span>
                          </div>
                        </div>
                        <div className="mc2-a-example__radio-answer">
                          {itemQuestion.answers.map((itemChild, i) => (
                            <div
                              key={`question-ans-${i}`}
                              className={`mc2-a-multichoiceanswers answer`}
                            >
                              <div className="mc2-a-multichoiceanswers__answer">
                                <ul className="mc2-a-multichoiceanswers__input ">
                                  <li>
                                    <input
                                      type="checkbox"
                                      name={`mc2-id${i}`}
                                      className="option-input checkbox"
                                      defaultChecked={itemChild.isCorrect}
                                      style={{ pointerEvents: 'none' }}
                                    />
                                    <label
                                      dangerouslySetInnerHTML={{
                                        __html: convertStrToHtml(itemChild.text),
                                      }}
                                    ></label>
                                  </li>
                                </ul>
                              </div>
                            </div>
                          ))}
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}
            {questionsGroupGeneral && questionsGroupGeneral.length > 0 && (
              <div className="__content-question">
                <div className="__title">
                  <span>
                    {`Question${questionsGroupGeneral.length > 1 ? 's' : ''}:`}
                  </span>
                </div>
                <div className="__content">
                  {questionsGroupGeneral.map((item, i) => {
                    const html = convertStrToHtml(item.question).replaceAll(
                      '%s%',
                      `<div class="__space"></div>`,
                    )
                    return (
                      <div key={i} className="__radio-group">
                        <CustomHeading tag="h6" className="__radio-heading">
                          <span
                            dangerouslySetInnerHTML={{
                              __html: html,
                            }}
                          ></span>
                        </CustomHeading>
                        <div className="__radio-list">
                          {item.answers.map((childItem, j) => (
                            <div
                              key={j}
                              className={`__radio-item ${generateClassName(
                                i,
                                j,
                              )}`}
                              style={{
                                pointerEvents:
                                  mode.state === TEST_MODE.play
                                    ? 'all'
                                    : 'none',
                              }}
                              onClick={() =>
                                mode.state === TEST_MODE.play &&
                                handleRadioClick(i, j)
                              }
                            >
                              <p
                                className="pt-a-text"
                                dangerouslySetInnerHTML={{
                                  __html: convertStrToHtml(childItem.text),
                                }}
                              ></p>
                              {/* <CustomText tag="p">{childItem.text}</CustomText> */}
                            </div>
                          ))}
                        </div>
                      </div>
                    )
                  })}
                </div>
              </div>
            )}
        </div>
      </div>
      
    </GameWrapper>
  )
}
