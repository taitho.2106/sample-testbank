import { useContext, useEffect, useState, useRef, Fragment } from 'react'

import PageEnd from '@rsuite/icons/PageEnd'
import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { checkIsScrollable } from '../../../../../interfaces/functionary'
import { CustomHeading } from '../../../../atoms/heading'
import { FormatText } from '../../../../molecules/formatText'
import { BlockPaper } from '../../../../organisms/blockPaper'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradient } from '../../../blockBottomGradient'

export const GameFB6 = ({ data }) => {
  const answers = data?.answers || []
  const questionInstruction = data?.questionInstruction || ''
  const instruction = data?.instruction || ''
  const question = data?.question || ''
  const questionInput = answers.filter((item) => !item.answer.startsWith('*'))

  // const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
  const questionArr = convertStrToHtml(question).split('%s%')

  const indexEx = answers.findIndex((item) => item.answer.startsWith('*'))
  const trueAnswer = answers
    .filter((m) => !m.answer.startsWith('*'))
    .map((item) => item.answer)

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } = useProgress()
  const { mode, currentPart } = useContext(PageTestContext)

  const [isScrollable, setIsScrollable] = useState(false)
  const [scrollToBottom, setScrollToBottom] = useState(false)

  const defaultTextArr = getProgressActivityValue(
    Array.from(Array(questionInput.length), () => ''),
  )
  const [fillTextArr, setFillTextArr] = useState([...defaultTextArr])

  const contentRef = useRef(null)

  const checkDanger = (i) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.value &&
      userAnswer.value[i] !== '' &&
      userAnswer.status[i] === false
    )
      return '--danger'
    return ''
  }

  const checkEmpty = (i) => {
    if (fillTextArr[i] === '' && mode.state !== TEST_MODE.check)
      return '--empty'
    return ''
  }

  const checkSuccess = (i) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.value &&
        userAnswer.value[i] !== '' &&
        userAnswer.status[i] === true) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = (i) => {
    let className = ''
    className += checkEmpty(i)
    className += ' '
    className += checkDanger(i)
    className += ' '
    className += checkSuccess(i)
    return className
  }

  const onPaste = (event) => {
    const index = parseInt(event.target.dataset['index'])
    event.preventDefault()
    const data = event.clipboardData
      .getData('text')
      .replace(/\r\n/g, ' ')
      .trim()
    event.target.innerHTML = data
    fillTextArr[index] = data
    setFillTextArr([...fillTextArr])
  }

  const handleInputBlur = () => {
    const answerValue = [...fillTextArr]
    let statusArr = []
    trueAnswer.forEach((item, i) => {
      const splitCheck = item.toLowerCase().trim().split('%/%').map(m=>m?.trim()).filter(m=>m)
      const textI = answerValue[i]
      statusArr.push(
        splitCheck.includes(
          textI
            .replace(/&nbsp;/g, ' ')
            .trim()
            .toLowerCase(),
        ) ||
          splitCheck.includes(
            textI
              .replace(/&nbsp;/g, ' ')
              .trim()
              .toLowerCase() + '.',
          )
          ? true
          : false,
      )
    })
    setProgressActivityValue(answerValue, statusArr)
  }

  const handleInputChange = (index, text) => {
    const textArr = [...fillTextArr]
    if (typeof textArr[index] !== 'string') return
    textArr[index] = text
    setFillTextArr(textArr)
  }

  useEffect(() => {
    if (!contentRef?.current) return
    const spaceList = contentRef.current.querySelectorAll('.__editable')
    
    spaceList.forEach((item, i) => {
      if ([TEST_MODE.review, TEST_MODE.play].includes(mode.state)) {
        if (userAnswer?.value) {
          item.innerHTML = userAnswer.value[i] || ''
          if (!userAnswer.value[i]) item.classList.add('--empty')
        } else {
          item.innerHTML = ''
          item.classList.add('--empty')
        }

        if (i !== 0 && mode.state === TEST_MODE.play) return
        // item.focus()
      } else if (mode.state === TEST_MODE.check) {
        item.innerHTML = trueAnswer[i].split('%/%').map(m=>m?.trim()).filter(m=>m).join(" / ")
      }
    })
  }, [contentRef, mode.state, data.id])

  const seeMore = () => {
    const element = document.getElementsByClassName("pt-o-game-fb6")[0]
    scrollTo(element, element.scrollHeight, 600);
  }

  function scrollTo(element, to, duration) {
    if (duration <= 0) return
    let difference = Math.abs(to - element.scrollTop)
    let perTick = (difference / duration) * 10
    setTimeout(function () {
      element.scrollTop = scrollToBottom
        ? element.scrollTop - perTick
        : element.scrollTop + perTick
      if (element.scrollTop === to) return
      scrollTo(element, to, duration - 10)
    }, 10)
    setScrollToBottom(!scrollToBottom)
  }

  useEffect(() => {
    const element = document.getElementsByClassName('pt-o-game-fb6')[0]
    setIsScrollable(checkIsScrollable(element))
    element.addEventListener('scroll', () => {
      if (
        element.scrollHeight - Math.ceil(element.scrollTop) <=
        element.clientHeight
      ) {
        setScrollToBottom(true)
      }
      if (element.scrollTop === 0) {
        setScrollToBottom(false)
      }
    })
    return () => {
      element.removeEventListener('scroll', () => {
        //
      })
    }
  }, [])
  let x = -1
  return (
    <GameWrapper className="pt-o-game-fb6" data-animate="fade-in">
      <div className="pt-o-game-fb6__left" >
        <BlockPaper>
          <div className="__content">
            <FormatText tag="p">
              {convertStrToHtml(questionInstruction)}
            </FormatText>
          </div>
        </BlockPaper>
      </div>
      <div className="pt-o-game-fb6__right">
        <BlockBottomGradient className="__container">
          <div className="__header">
            <CustomHeading tag="h6" className="__description">
              <FormatText tag="span">{instruction}</FormatText>
            </CustomHeading>
          </div>
          <div className="__content">
            <div className="__list">
              <Scrollbars universal={true}>
                <div ref={contentRef} className="__content">
                  {questionArr.map((item, i) => {
                    if (i === 0 || i === indexEx + 1) {
                    } else {
                      x = x + 1
                    }
                    return (
                      <Fragment key={i}>
                        {i !== 0 && i !== indexEx + 1 && (
                          <span
                            data-index={x}
                            className={`__editable ${generateClassName(x)}`}
                            contentEditable={
                              mode.state === TEST_MODE.play ? true : false
                            }
                            style={{
                              pointerEvents:
                                mode.state === TEST_MODE.play ? 'all' : 'none',
                            }}
                            onBlur={() =>
                              mode.state === TEST_MODE.play && handleInputBlur()
                            }
                            onInput={(e) => {
                              mode.state === TEST_MODE.play &&
                                handleInputChange(
                                  e.target.dataset.index,
                                  e.target.innerHTML,
                                )
                            }}
                            onPaste={(e) => {
                              mode.state === TEST_MODE.play && onPaste(e)
                            }}
                          ></span>
                        )}
                        {i !== 0 && i === indexEx + 1 && (
                          <span
                            className={`__editableEx`}
                            contentEditable={false}
                          >
                            {answers[indexEx].answer.substring(1)}
                          </span>
                        )}
                        {/* <FormatText tag="span">{item}</FormatText> */}
                        <span
                          dangerouslySetInnerHTML={{
                            __html: item,
                          }}
                        ></span>
                      </Fragment>
                    )
                  })}
                </div>
              </Scrollbars>
            </div>
          </div>
        </BlockBottomGradient>
      </div>
      {isScrollable && (
        <div className="pt-o-game-fb6__see-more" onClick={seeMore}>
          <PageEnd
            rotate={scrollToBottom ? -90 : 90}
            color="white"
            style={{ fontSize: 28 }}
          />
        </div>
      )}
    </GameWrapper>
  )
}
