import { useContext, useEffect, useRef, useState } from 'react'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'

export const Game6 = ({ data }) => {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const examList = answers.filter((item) => item.isExam === 'exam')
  const questionList = answers.filter((item) => item.isExam === 'question')
  const exampleImageList = examList.map((item) => item.imageInstruction)
  const questionImageList = questionList.map((item) => item.imageInstruction)
  const quesForExamList = examList.map((item) => item.question)
  const quesForQuestionList = questionList.map((item) => item.question)
  const answerExample = examList.map((item) => item.answer)
  // const answerQuestion = questionList.map((item) => item.answer)
  // const answerOrderExample = examList.map((item) => item.answerOrder)
  const answerOrderQuestion = questionList.map((item) => item.answerOrder)

  const dragListGroup = []
  const trueArrGroup = []
  const trueArrGroup1 = []

  const trueAnswerGroup = []
  const trueAnswerGroup1 = []

  const groupKey = useRef(Math.random())
  for (let qIndex = 0; qIndex < questionList.length; qIndex++) {
    const questionArr = quesForQuestionList[qIndex].split('/') //question
    const dragList = questionArr.map((item, i) => ({ index: i, value: item, key: `${groupKey.current}_${qIndex}` }))

    let trueAnswer = ''
    let trueAnswer1 = ''

    answerOrderQuestion[qIndex][0].forEach((element) => {
      trueAnswer += ` ${element}`
    })

    answerOrderQuestion[qIndex][1]?.forEach((element) => {
      trueAnswer1 += ` ${element}`
    })

    let trueArr = []
    let tmpTrueArr = []

    questionArr
      .sort((a, b) => b.length - a.length)
      .forEach((item, i) => {
        let indexOf = ` ${trueAnswer.toLowerCase()} `.indexOf(
          ` ${item.toLowerCase().trim()} `,
        )
        let filterList = tmpTrueArr.filter((filter) => filter.pos === indexOf)

        if (indexOf != -1) {
          while (filterList.length > 0) {
            indexOf = trueAnswer
              .toLowerCase()
              .indexOf(item.toLowerCase().trim(), filterList[0].pos + 1)
            filterList = tmpTrueArr.filter((filter) => filter.pos === indexOf)
          }
          tmpTrueArr.push({ index: i, pos: indexOf, value: item })
        }
      })
    trueArr = tmpTrueArr.sort((a, b) => a.pos - b.pos)
    trueArrGroup.push(trueArr)
    trueAnswerGroup.push(trueAnswer)

    let trueArr1 = []
    let tmpTrueArr1 = []

    if (answerOrderQuestion[qIndex][1]) {
      questionArr
        .sort((a, b) => b.length - a.length)
        .forEach((item, i) => {
          let indexOf = ` ${trueAnswer1.toLowerCase()} `.indexOf(
            ` ${item.toLowerCase().trim()} `,
          )
          let filterList = tmpTrueArr1.filter(
            (filter) => filter.pos === indexOf,
          )

          if (indexOf != -1) {
            while (filterList.length > 0) {
              indexOf = trueAnswer1
                .toLowerCase()
                .indexOf(item.toLowerCase().trim(), filterList[0].pos + 1)
              filterList = tmpTrueArr1.filter(
                (filter) => filter.pos === indexOf,
              )
            }
            tmpTrueArr1.push({ index: i, pos: indexOf, value: item })
          }
        })
      trueArr1 = tmpTrueArr1.sort((a, b) => a.pos - b.pos)
      trueArrGroup1.push(trueArr1)
      trueAnswerGroup1.push(trueAnswer1)
    }
    else{
      trueArrGroup1.push(trueArr1)
      trueAnswerGroup1.push(trueAnswer1)
    }
    
    dragListGroup.push(dragList)
  }
  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()

  const { mode, currentQuestion } = useContext(PageTestContext)

  const [tempData, setTempData] = useState({
    index: null,
    value: null,
  })

  const [coordinate, setCoordinate] = useState([0, 0])
  const [currentPosition, setCurrentPosition] = useState([], [])
  const [currentSplitItem, setCurrentSplitItem] = useState(null)
  const [dragingText, setDraggingText] = useState('')
  const [isDraggingBottom, setIsDraggingBottom] = useState(null)
  const [isDraggingTop, setIsDraggingTop] = useState(null)
  const [correctIndex, setIsCorrectIndex] = useState(-1)
  const [touchStartTimeStamp, setTouchStartTimeStamp] = useState(0)
  const [touchEndTimeStamp, setTouchEndTimeStamp] = useState(0)
  const defaultRadio = getProgressActivityValue(
    Array.from(Array(questionList.length), () => []),
  )
  const [chosenList, setChosenList] = useState(defaultRadio)

  const currentPositionGroup = []
  const cloneDragger = useRef(null)
  const dropBox = useRef(null)
  const allowDrop = (e, currentIndex) => e.preventDefault(currentIndex)
  
  const checkDanger = (i, currentIndex) => {
    if(!userAnswer) return ''
    const userAnswerValue = userAnswer.value[currentIndex]
    const correctAnswersOrder = questionList[currentIndex].answerOrder
    const currentValue = []
    if (mode.state === TEST_MODE.review && userAnswer.value[currentIndex].length > 0){
      for (let index = 0; index < correctAnswersOrder.length; index++) {
        const value = correctAnswersOrder[index]
        for (let j = 0; j < value.length; j++) {
          if(j === i){
            currentValue.push(value[j])
          }
        }
      }
      if(!currentValue.includes(userAnswerValue[i].value)){
        return '--danger'
      }
    }
    return ''
  }

  const checkInvisible = (i, currentIndex) => {
    if (i === isDraggingTop && correctIndex === currentIndex) return '--invisible'
    return ''
  }

  const checkSuccess = (i, currentIndex) => {
    if(!userAnswer) return ''
    const userAnswerValue = userAnswer.value[currentIndex]
    const correctAnswersOrder = questionList[currentIndex].answerOrder
    const currentValue = []
    if (mode.state === TEST_MODE.review && userAnswer.value[currentIndex].length > 0){
      for (let index = 0; index < correctAnswersOrder.length; index++) {
        const value = correctAnswersOrder[index]
        for (let j = 0; j < value.length; j++) {
          if(j === i){
            currentValue.push(value[j])
          }
        }
      }
      if(currentValue.includes(userAnswerValue[i].value)){
        return '--success'
      }
    }
    return ''
  }

  const dragging = (e, currentIndex) => {
    setCoordinate([e.clientX, e.clientY])
    // const dropItems = dropBox.current.querySelectorAll('.__drop-item')
    const dropItemsTest = document.querySelectorAll(`#question_${data.id}_${currentIndex} .__drop-item`)
    const cloneItemDragger = document.querySelectorAll(`#question_clone_${data.id}_${currentIndex}`)

    let positionArr = []
    dropItemsTest.forEach((item) => {
      const rect = item.getBoundingClientRect()
      positionArr.push([rect.left, rect.top])
    })
    setCurrentPosition([...positionArr])
    
    if (positionArr.length <= 0) return  
    const findIndex = positionArr.findIndex((item, i) =>
      i === positionArr.length - 1
        ? e.clientX >= item[0] 
        : e.clientX <
            item[0] +
            dropItemsTest[i].offsetWidth 
            + cloneItemDragger[0].offsetWidth 
            &&
          e.clientX >= item[0] &&
          e.clientY < item[1] + dropItemsTest[i].offsetHeight &&
          e.clientY >= item[1],
    )
    setCurrentSplitItem(findIndex)
  }

  const dragEnd = (e, position, currentIndex) => {
    setCurrentSplitItem(-2)
    setCoordinate([e.clientX, e.clientY])
    if (position === 'bottom' && correctIndex === currentIndex) setIsDraggingBottom(null)
    else if (position === 'top' && correctIndex === currentIndex) setIsDraggingTop(null)
    const theDrop = document.querySelectorAll(".__drop-box")
    for (let i = 0; i < theDrop.length; i++){
      theDrop[i].style.pointerEvents = 'all'
    }
  }

  const dragStart = (e, index, item, position, currentIndex) => {
    const img = new Image()
    img.src =
      'data:image/gif;base64,R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs='
    e.dataTransfer?.setDragImage(img, 0, 0)
    e.dataTransfer?.setData('index', item.index)
    e.dataTransfer?.setData('value', item.value)
    e.dataTransfer?.setData('key', item.key)
    setCoordinate([e.clientX, e.clientY])
    const theDrop = document.querySelectorAll(".__drop-box")
    for (let i = 0; i < theDrop.length; i++){
      if(item.key == theDrop[i].getAttribute('id')){
        theDrop[i].style.pointerEvents = 'all'
      }
      else{
        theDrop[i].style.pointerEvents = 'none'
      }
    }
    if (position === 'bottom' && correctIndex === currentIndex) setIsDraggingBottom(index)
    else if (position === 'top' && correctIndex === currentIndex) setIsDraggingTop(index)
    setDraggingText(item.value)
  }

  const dragStartMobile = (e, index, item, position, currentIndex) => {
    setTempData({
      ...tempData,
      index: item.index,
      value: item.value,
    })
    setCoordinate([e.clientX, e.clientY])
    if (position === 'bottom' && correctIndex === currentIndex) setIsDraggingBottom(index)
    else if (position === 'top' && correctIndex === currentIndex) setIsDraggingTop(index)
    setDraggingText(item.value)
  }

  const drop = (e, keyD, action, currentIndex) => {
    e.preventDefault()
    setIsDraggingBottom(null)
    setIsDraggingTop(null)
    const index = e.dataTransfer.getData('index')
    const value = e.dataTransfer.getData('value')
    const key = e.dataTransfer.getData('key')
    const item = { index: parseInt(index), value, key }
    if (keyD === item.key){
      if (action) {
        insertItem(item, currentIndex)
        return
      }
      removeItem(item, currentIndex)
    }
    return false
  }

  const dropMobile = (e, action, currentIndex) => {
    e.preventDefault()
    setIsDraggingBottom(null)
    setIsDraggingTop(null)
    const index = tempData.index
    const value = tempData.value
    const item = { index: parseInt(index), value }
    if (action) {
      insertItem(item, currentIndex)
      return
    }
    removeItem(item, currentIndex)
  }

  const generateClassName = (i, currentIndex) => {
    let className = ''
    className += checkInvisible(i, currentIndex)
    className += ' '
    className += checkDanger(i, currentIndex)
    className += ' '
    className += checkSuccess(i, currentIndex)
    return className
  }

  const insertItem = (selector, currentIndex) => {
    const chosenListCurrent = chosenList
    let userAnswerCurrent = userAnswer.status
    if (!Array.isArray(userAnswerCurrent)) {
      userAnswerCurrent = Array.from(Array(questionList.length), () => false)
    }
    let newArr = chosenListCurrent[currentIndex]

    // check duplicate item
    if (newArr.filter((item) => item.index === selector.index).length > 0)
      newArr = newArr.filter((item) => item.index !== selector.index)
    // update value
    newArr.splice(currentSplitItem + 1, 0, selector)
    // newArr.push(selector)
    // check isCorrect
    let checkStr = ''
    newArr.forEach((item) => (checkStr += `${item.value} `))
    chosenListCurrent[currentIndex] = newArr

    let checkCorrect =
      checkStr.toLowerCase().trim() ===
        trueAnswerGroup[currentIndex].toLowerCase().trim() ||
      (trueAnswerGroup1[currentIndex] &&
        checkStr.toLowerCase().trim() ===
          trueAnswerGroup1[currentIndex].toLowerCase().trim())
        ? true
        : false
    userAnswerCurrent[currentIndex] = checkCorrect
    // save progress
    setProgressActivityValue(chosenListCurrent, userAnswerCurrent)
    // display
    setChosenList(chosenListCurrent)
    setCurrentSplitItem(null)
    setTempData({
      index: null,
      value: null,
    })
  }

  const quickInsertItem = (selector, currentIndex) => {
    if (!selector.value) {
      return
    }
    // update value
    const chosenListCurrent = chosenList
    let userAnswerCurrent = userAnswer.status
    if (!Array.isArray(userAnswerCurrent)) {
      userAnswerCurrent = Array.from(Array(questionList.length), () => false)
    }
    let newArr = chosenListCurrent[currentIndex]
    newArr.push(selector)
    // check isCorrect
    let checkStr = ''
    newArr.forEach((item) => (checkStr += `${item.value} `))
    let checkCorrect =
      checkStr.toLowerCase().trim() ===
        trueAnswerGroup[currentIndex].toLowerCase().trim() ||
      (trueAnswerGroup1[currentIndex] &&
        checkStr.toLowerCase().trim() ===
          trueAnswerGroup1[currentIndex].toLowerCase().trim())
        ? true
        : false
    userAnswerCurrent[currentIndex] = checkCorrect
    // save progress
    setProgressActivityValue(chosenListCurrent, userAnswerCurrent)
    // display
    setChosenList(chosenListCurrent)
  }

  const quickRemoveItem = (selector, currentIndex) => {
    let userAnswerCurrent = userAnswer.status
    if (!Array.isArray(userAnswerCurrent)) {
      userAnswerCurrent = Array.from(Array(questionList.length), () => false)
    }
    if (!selector.value) {
      return
    }
    // update value
    const chosenListCurrent = chosenList
    const beforeArr = chosenListCurrent[currentIndex]
    const afterArr = beforeArr.filter((item) => item.index !== selector.index)
    // check isCorrect
    let checkStr = ''
    afterArr.forEach((item) => (checkStr += `${item.value} `))
    chosenListCurrent[currentIndex] = afterArr

    let checkCorrect =
      checkStr.toLowerCase().trim() ===
        trueAnswerGroup[currentIndex].toLowerCase().trim() ||
      (trueAnswerGroup1[currentIndex] &&
        checkStr.toLowerCase().trim() ===
          trueAnswerGroup1[currentIndex].toLowerCase().trim())
        ? true
        : false
    userAnswerCurrent[currentIndex] = checkCorrect
    // save progress
    setProgressActivityValue(chosenListCurrent, userAnswerCurrent)
    // display
    setChosenList(chosenListCurrent)
  }

  const removeItem = (selector, currentIndex) => {
    let userAnswerCurrent = userAnswer.status
    if (!Array.isArray(userAnswerCurrent)) {
      userAnswerCurrent = Array.from(Array(questionList.length), () => false)
    }
    // update value
    const chosenListCurrent = chosenList
    const beforeArr = chosenListCurrent[currentIndex]
    const afterArr = beforeArr.filter((item) => item.index !== selector.index)
    // check isCorrect
    let checkStr = ''
    afterArr.forEach((item) => (checkStr += `${item.value} `))
    chosenListCurrent[currentIndex] = afterArr

    let checkCorrect =
      checkStr.toLowerCase().trim() ===
        trueAnswerGroup[currentIndex].toLowerCase().trim() ||
      (trueAnswerGroup1[currentIndex] &&
        checkStr.toLowerCase().trim() ===
          trueAnswerGroup1[currentIndex].toLowerCase().trim())
        ? true
        : false
    userAnswerCurrent[currentIndex] = checkCorrect
    // save progress
    setProgressActivityValue(chosenListCurrent, userAnswerCurrent)
    // display
    setChosenList(chosenListCurrent)
  }

  const updatePosition = () => {
    const elementQuestion = document.querySelectorAll(`#game_question_container_${data.id} .box-answer`);    
    for(let i=0; i < elementQuestion.length; i++){
      const listDropItems = document.querySelectorAll(`#question_${data.id}_${i} .__drop-item`)
      if (chosenList[i]?.length !== listDropItems.length) return
      let positionArr = []
      listDropItems.forEach((item) => {
        const rect = item.getBoundingClientRect()
        positionArr.push([rect.left, rect.top])
      })
      setCurrentPosition([...positionArr])
      currentPositionGroup.push(currentPosition)
    }
  }

  //change question
  useEffect(() => {
    const defaultRadio = getProgressActivityValue(
      Array.from(Array(questionList.length), () => []),
    )
    setChosenList(defaultRadio)
    updatePosition()
  }, [currentQuestion])

  // Used to detect whether the users browser is an mobile browser
  function isMobile() {
    ///<summary>Detecting whether the browser is a mobile browser or desktop browser</summary>
    ///<returns>A boolean value indicating whether the browser is a mobile browser or not</returns>

    if (sessionStorage.desktop)
      // desktop storage
      return false
    else if (localStorage.mobile)
      // mobile storage
      return true

    // alternative
    const mobile = [
      'iphone',
      'ipad',
      'android',
      'blackberry',
      'nokia',
      'opera mini',
      'windows mobile',
      'windows phone',
      'iemobile',
      'tablet',
      'mobi',
    ]
    let ua = navigator.userAgent.toLowerCase()
    for (let i in mobile) if (ua.indexOf(mobile[i]) > -1) return true

    // nothing found.. assume desktop
    return false
  }

  const [isZoomA, setIsZoomA] = useState(false)
  const [isZoomE, setIsZoomE] = useState(false)
  const [zoomStrA, setZoomStrA] = useState('')
  const [zoomStrE, setZoomStrE] = useState('')

  let answerClassName = ''
  let countImg = 0
  for (let i=0; i< examList.length; i++ ) {
    const keywordExamLength = quesForExamList[i].split('/').length
    if(exampleImageList[i] === '') countImg += 1
    if (keywordExamLength > 3 && exampleImageList[i] !== '' || keywordExamLength > 6 || countImg === exampleImageList.length) {
      answerClassName = '--flex-content'
      break
    }
  }

  return (
    <GameWrapper className="pt-o-game-6" >
      <div className="pt-o-game-6__instruction">
        <span>{instruction}</span>
      </div>
      <div className="pt-o-game-6__container" id={`game_question_container_${data.id}`}>
        <div className="game-dd1-section">
          {examList.length > 0 && (
            <div className="game-dd1-section__exam-answers">
              <div className="header_exam">
                <span>{`Example${examList.length > 1 ? 's' : ''}:`}</span>
              </div>
              <hr />
              {zoomStrE.length > 0 && (
                <ImageZooming
                  data={{
                    alt: `instruction_example`,
                    src: `${zoomStrE}`,
                  }}
                  status={{
                    state: isZoomE,
                    setState: setIsZoomE,
                  }}
                />
              )}
              <div className={`item_exam ${answerClassName}`}>
                {examList.map((item, eIndex) => (
                  <div
                    key={eIndex + 1}
                    className={`item_exam__list ${
                      item.imageInstruction ? '' : 'no-example-image'
                    }`}
                  >
                    <div className="__image">
                      <img
                        src={item.imageInstruction}
                        alt=""
                        height={100}
                        width={100}
                        onClick={() => {
                          setIsZoomE(true)
                          setZoomStrE(exampleImageList[eIndex])
                        }}
                      />
                    </div>
                    <div className="dd1-example-keyword">
                      <div className="dd1-example-keyword__item">
                        {quesForExamList[eIndex].split('/').map((item, i) => (
                          <div key={i} className="dd1-example-keyword__cell">
                            <span className="dd1-example-keyword__name">
                              {item}
                            </span>
                          </div>
                        ))}
                      </div>
                      <div className="dd1-example-answers">
                        <div className="dd1-example-answers__text">
                          <img
                            alt=""
                            src="/images/icons/ic-arrow-right-sub.png"
                          />
                          <span>{answerExample[eIndex]}</span>
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
              </div>
            </div>
          )}
          <div className="game-dd1-section__content">
            <div className="header_questions">
              <span>{`Question${questionList.length > 1 ? 's' : ''}:`}</span>
            </div>
            <hr />
            {zoomStrA.length > 0 && (
              <ImageZooming
                data={{
                  alt: `instruction_answer`,
                  src: `${zoomStrA}`,
                }}
                status={{
                  state: isZoomA,
                  setState: setIsZoomA,
                }}
              />
            )}
            {questionList.map((item, aIndex) => (
              <div
                key={aIndex + 1}
                className={`box-answer ${aIndex} ${
                  item.imageInstruction ? '' : 'no-image'
                }`}
              >
                <div
                  className="item-image"
                  onClick={() => {
                    setIsZoomA(true)
                    setZoomStrA(questionImageList[aIndex])
                  }}
                >
                  <img src={item.imageInstruction} alt="" />
                </div>
                <div
                  className="item-answers"
                  data-animate="fade-in"
                  onDrop={(e) =>
                    mode.state === TEST_MODE.play && drop(e, dragListGroup[aIndex][0].key, false, aIndex)
                  }
                  onDragOver={(e) =>
                    mode.state === TEST_MODE.play && allowDrop(e, aIndex)
                  }
                >
                  <div
                    id={`${groupKey.current}_${aIndex}`}
                    ref={dropBox}
                    className={`__drop-box ${aIndex} ${mode.state === TEST_MODE.check ? '--check' : ''}`}
                    onDrop={(e) => {
                      if (mode.state === TEST_MODE.play) {
                        e.stopPropagation()
                        drop(e, dragListGroup[aIndex][aIndex].key, true, aIndex)
                      } 
                    }}
                    onDragOver={(e) => {
                      mode.state === TEST_MODE.play && allowDrop(e)
                      setIsCorrectIndex(aIndex)
                    }}
                    onTouchEnd={(e) => {
                      setIsCorrectIndex(aIndex)
                      setTouchEndTimeStamp(e.timeStamp)
                      const detectTime =
                        Math.floor(e.timeStamp) -
                        Math.floor(touchStartTimeStamp)
                      if (detectTime > 500 && tempData.value) {
                        const touch = e.changedTouches[0]
                        e.clientX = touch?.pageX
                        e.clientY = touch?.pageY
                        if (mode.state === TEST_MODE.play) {
                          e.stopPropagation()
                          const dropBoxDimension =
                            // dropBox.current.getBoundingClientRect()
                            document.querySelectorAll(`#game_question_container_${data.id} .__drop-box`)[aIndex].getBoundingClientRect()
                          const { top, left, width, height } = dropBoxDimension
                          let inTopBox =
                            e.clientX >= left &&
                            e.clientX <= left + width &&
                            e.clientY >= top &&
                            e.clientY <= top + height
                          dropMobile(e, inTopBox, aIndex)
                        }
                      } else {
                        quickRemoveItem(tempData, aIndex)
                        setIsDraggingTop(null)
                        setTempData({
                          value: null,
                          index: null,
                        })
                        setCurrentSplitItem(null)
                      }
                    }}
                  >
                    {[TEST_MODE.review, TEST_MODE.play].includes(mode.state) &&
                      chosenList[aIndex]?.map((item, i) => (
                        <div
                          key={i}
                          className="__drop-item"
                          style={{
                            transform:
                              mode.state === TEST_MODE.review ||
                              currentSplitItem === -2 ||
                              currentSplitItem === null ||
                              correctIndex !== aIndex
                                ? 'translateX(0)'
                                : i <= currentSplitItem
                                ? 'translateX(-3rem)'
                                : 'translateX(3rem)',
                          }}
                        >
                          <div
                            className={`__drop-content ${generateClassName(i,aIndex)}`}
                            draggable={true}
                            style={{
                              pointerEvents:
                                mode.state === TEST_MODE.play ? 'all' : 'none',
                            }}
                            onClick={() => {
                              if (!isMobile()) {
                                mode.state === TEST_MODE.play &&
                                  quickRemoveItem(item, aIndex)
                              }
                            }}
                            onDrag={(e) =>
                              mode.state === TEST_MODE.play && dragging(e, aIndex)
                            }
                            onDragStart={(e) =>
                              mode.state === TEST_MODE.play &&
                              dragStart(e, i, item, 'top', aIndex)
                            }
                            onDragEnd={(e) =>
                              mode.state === TEST_MODE.play && dragEnd(e, 'top', aIndex)
                            }
                            onTouchMove={(el) => {
                              const touch = el.changedTouches[0]
                              const e = {
                                clientX: touch?.pageX,
                                clientY: touch?.pageY,
                              }
                              mode.state === TEST_MODE.play && dragging(e, aIndex)
                            }}
                            onTouchStart={(el) => {
                              setTouchStartTimeStamp(el.timeStamp)
                              const touch = el.changedTouches[0]
                              const e = {
                                clientX: touch?.pageX,
                                clientY: touch?.pageY,
                              }
                              mode.state === TEST_MODE.play &&
                                dragStartMobile(e, i, item, 'top')
                            }}
                          >
                            {item.value}
                          </div>
                        </div>
                      ))}
                    {mode.state === TEST_MODE.check &&
                      (
                        questionList[aIndex].answerOrder.map((item, i) => (
                          <div key={i} className='__drop-true-answers'>
                            {
                              item.map((value, index) => (
                                <div key={index} className={`__drop-item ${aIndex}`}>
                            <div className="__drop-content --success">
                              {value}
                            </div>
                          </div>
                              ))
                            }
                          </div>
                        ))
                      )
                      }
                  </div>
                  <div
                    className={`__drag-box ${aIndex}`}
                    onTouchEndCapture={(e) => {
                      const touch = e.changedTouches[0]
                      e.clientX = touch?.pageX
                      e.clientY = touch?.pageY
                      if (mode.state === TEST_MODE.play) {
                        e.stopPropagation()
                        setIsCorrectIndex(aIndex)
                        const dropBoxDimension =
                          // dropBox.current.getBoundingClientRect()
                          document.querySelectorAll(`#game_question_container_${data.id} .__drop-box`)[aIndex].getBoundingClientRect()
                        const { top, left, width, height } = dropBoxDimension
                        let inTopBox =
                          e.clientX >= left &&
                          e.clientX <= left + width &&
                          e.clientY >= top &&
                          e.clientY <= top + height
                        if (inTopBox && tempData.value) {
                          dropMobile(e, true, aIndex)
                        } else {
                          quickInsertItem(tempData, aIndex)
                          setIsDraggingBottom(null)
                          setTempData({
                            value: null,
                            index: null,
                          })
                          setCurrentSplitItem(null)
                        }
                      }
                    }}
                  >
                    <div className={`__drag-length ${aIndex}`}>
                      {mode.state !== TEST_MODE.check &&
                        dragListGroup[aIndex].map((item, i) => (
                          <div
                            key={i}
                            className={`__drag-item ${aIndex} ${
                              chosenList[aIndex]?.findIndex(
                                (find) => find.index === i,
                              ) !== -1
                                ? '--hidden'
                                : ''
                            }`}
                          >
                            <div
                              className={`__drag-content ${aIndex} ${
                                isDraggingBottom === i && correctIndex === aIndex ? '--invisible' : ''
                              }`}
                              draggable={true}
                              style={{
                                pointerEvents:
                                  mode.state === TEST_MODE.play
                                    ? 'all'
                                    : 'none',
                              }}
                              onClick={() => {
                                if (!isMobile()) {
                                  mode.state === TEST_MODE.play &&
                                    quickInsertItem(item, aIndex)
                                }
                              }}
                              onDrag={(e) => {
                                mode.state === TEST_MODE.play && dragging(e, aIndex)
                              }}
                              onDragStart={(e) => {
                                mode.state === TEST_MODE.play &&
                                  dragStart(e, i, item, 'bottom', aIndex)
                              }}
                              onDragEnd={(e) => {
                                mode.state === TEST_MODE.play &&
                                  dragEnd(e, 'bottom', aIndex)
                              }}
                              onTouchMove={(el) => {
                                const touch = el.changedTouches[0]
                                const e = {
                                  clientX: touch?.pageX,
                                  clientY: touch?.pageY,
                                }
                                mode.state === TEST_MODE.play && dragging(e, aIndex)
                              }}
                              onTouchStart={(el) => {
                                const touch = el.changedTouches[0]
                                const e = {
                                  clientX: touch?.pageX,
                                  clientY: touch?.pageY,
                                }
                                mode.state === TEST_MODE.play &&
                                  dragStartMobile(e, i, item, 'bottom')
                              }}
                              onTouchEnd={(el) => {
                                const touch = el.changedTouches[0]
                                const e = {
                                  clientX: touch?.pageX,
                                  clientY: touch?.pageY,
                                }
                                mode.state === TEST_MODE.play &&
                                  dragEnd(e, 'bottom', aIndex)
                              }}
                            >
                              <span>{item.value}</span>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>

                  <div
                    ref={cloneDragger}
                    id={`question_clone_${data.id}_${aIndex}`}
                    className={`pt-o-game-6__dragging-clone ${aIndex} ${
                      correctIndex == aIndex && isDraggingBottom !== null ? '--bottom' : ''
                    } ${correctIndex == aIndex && isDraggingTop !== null ? '--top' : ''}`}
                    style={{  top: correctIndex === aIndex ? coordinate[1]: '{}', left: correctIndex === aIndex ? coordinate[0] :'{}' }}
                  >
                    {dragingText}
                  </div>
                </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </GameWrapper>
  )
}
