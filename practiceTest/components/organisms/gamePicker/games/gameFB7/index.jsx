import { useContext, useEffect, useState, useRef, Fragment } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { CustomImage } from '../../../../atoms/image'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradient } from '../../../blockBottomGradient'


export const GameFB7 = ({ data }) => {
  const answers = data?.answers || []
  const imageInstruction = data?.imageInstruction
  const instruction = data?.instruction || ''
  const question = data?.question||"" ;
  const questionInput = answers.filter((item) => !item.answer.startsWith('*'))

  // const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
  const questionArr = convertStrToHtml(question.replace(/\%s\%/g,"###")).split('###')
  const indexEx = answers.findIndex((item)=> item.answer.startsWith('*'))
  const trueAnswer = answers.filter((m)=> !m.answer.startsWith('*')).map((item) => item.answer)

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } = useProgress()
  const { mode, currentPart } = useContext(PageTestContext)

  const defaultTextArr = getProgressActivityValue(
    Array.from(Array(questionInput.length), () => ''),
  )
  const [fillTextArr, setFillTextArr] = useState([...defaultTextArr])

  const [isZoom, setIsZoom] = useState(false)

  const contentRef = useRef(null)

  const checkDanger = (i) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.value &&
      userAnswer.value[i] !== '' &&
      userAnswer.status[i] === false
    )
      return '--danger'
    return ''
  }

  const checkEmpty = (i) => {
    if (fillTextArr[i] === '' && mode.state !== TEST_MODE.check)
      return '--empty'
    return ''
  }

  const checkSuccess = (i) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.value &&
        userAnswer.value[i] !== '' &&
        userAnswer.status[i] === true) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = (i) => {
    let className = ''
    className += checkEmpty(i)
    className += ' '
    className += checkDanger(i)
    className += ' '
    className += checkSuccess(i)
    return className
  }

  const onPaste = (event) => {
    const index = parseInt(event.target.dataset['index'])
    event.preventDefault()
    const data = event.clipboardData
      .getData('text')
      .replace(/\r\n/g, ' ')
      .trim()
    event.target.innerHTML = data
    fillTextArr[index] = data
    setFillTextArr([...fillTextArr])
  }
  
  const handleInputBlur = () => {
    const answerValue = [...fillTextArr]
    let statusArr = []

    trueAnswer.forEach((item, i) => {
      const splitCheck = item.toLowerCase().trim().split('%/%').map(m=>m?.trim()).filter(m=>m)
      statusArr.push(
        splitCheck.includes(
          answerValue[i]
            .replace(/&nbsp;/g, ' ')
            .trim()
            .toLowerCase(),
        )
          ? true
          : false,
      )
    })
    setProgressActivityValue(answerValue, statusArr)
  }

  const handleInputChange = (index, text) => {
    const textArr = [...fillTextArr]
    if (typeof textArr[index] !== 'string') return
    textArr[index] = text
    setFillTextArr(textArr)
  }
  useEffect(()=>{
    if(indexEx !== -1){
        const example = contentRef.current.querySelectorAll('.__editableEX')
        
        example.forEach((item,i)=>{
            item.innerHTML = answers[indexEx].answer.substring(1)
        })
    }
  },[])
  useEffect(() => {
    if (!contentRef?.current) return
    const spaceList = contentRef.current.querySelectorAll('.__editable')
    
    spaceList.forEach((item, i) => {
      if ([TEST_MODE.review, TEST_MODE.play].includes(mode.state)) {
        if (userAnswer?.value) {
          item.innerHTML = userAnswer.value[i] || ''
          if (!userAnswer.value[i]) item.classList.add('--empty')
        } else {
          item.innerHTML = ''
          item.classList.add('--empty')
        }

        if (i !== 0 && mode.state === TEST_MODE.play) return
        // item.focus()
      } else if (mode.state === TEST_MODE.check){
        item.innerHTML = trueAnswer[i].split('%/%').map(m=>m?.trim()).filter(m=>m).join(" / ")
      }
    })
  }, [contentRef, mode.state, data.id])

  let x = -1;
  return (
    <GameWrapper className="pt-o-game-fb7">
      <div className="pt-o-game-fb7__left">
        <div className="top">
          <div className="image-left">
            <CustomImage
              className="__image-container"
              alt="Image Question"
              src={imageInstruction}
              yRate={0}
              onClick={() => setIsZoom(true)}

            />
            <ImageZooming
            data={{ alt: question, src: imageInstruction }}
            status={{
              state: isZoom,
              setState: setIsZoom,
            }}
          />
          </div>
        </div>
        <div className="bottom">
          <span>{instruction}</span>
        </div>
      </div>
      <div className="pt-o-game-fb7__right">
        <BlockBottomGradient className="__container">
          <div className="__content">
            <div className="__list">
              <Scrollbars universal={true}>
                  <div ref={contentRef} className="__content">
                    {questionArr.map((item, i) => {
                      if(i === 0  || i === indexEx + 1){

                      }else{
                          x = x + 1
                      }
                      return (
                      <Fragment key={i}>
                        {i !== 0 && i !== indexEx + 1 && (
                          <span
                            data-index={x}
                            className={`__editable ${generateClassName(x)}`}
                            contentEditable={
                              mode.state === TEST_MODE.play ? true : false
                            }
                            style={{
                              pointerEvents:
                                mode.state === TEST_MODE.play ? 'all' : 'none',
                            }}
                            onBlur={() =>
                              mode.state === TEST_MODE.play && handleInputBlur()
                            }
                            onInput={(e) =>
                              mode.state === TEST_MODE.play &&
                              handleInputChange(e.target.dataset.index, e.target.innerHTML)
                            }
                            onPaste={(e) => {
                              mode.state === TEST_MODE.play && onPaste(e)
                            }}
                          ></span>
                        )}
                        {i !== 0 && i === indexEx + 1 && (
                          <span
                            className={`__editableEx `}
                            contentEditable={false}
                          >{answers[indexEx].answer.substring(1)}</span>
                        )}
                        {/* <FormatText tag="span">{" " + item + " "}</FormatText> */}
                        <span
                          dangerouslySetInnerHTML={{
                            __html: (item),
                          }}
                        ></span>
                      </Fragment>
                    )})}
                  </div>
                </Scrollbars>
            </div>
          </div>
        </BlockBottomGradient>
      </div>
    </GameWrapper>
  )
}

