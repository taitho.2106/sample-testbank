import { useContext, useState, useEffect } from 'react'

import { Toggle } from 'rsuite'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { CustomHeading } from '../../../../atoms/heading'
import { FormatText } from '../../../../molecules/formatText'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { convertStrToHtml } from "../../../../../../utils/string";

export const GameTF3 = ({ data }) => {
  const answers = data?.answers || []
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''

  const answerList = answers.filter((item) => !item.text.startsWith('*'))
  const trueAnswers = answerList.map((item) => item.isCorrect)
  const examList = answers
    .filter((item) => item.text.startsWith('*'))
    .map((item) => {
      return {
        text: item.text.replace('*', ''),
        isCorrect: item.isCorrect,
      }
    })

  const [isZoom, setIsZoom] = useState(false)
  const { getProgressActivityValue, setProgressActivityValue } = useProgress()
  const defaultData = getProgressActivityValue(
    Array.from(Array(answerList.length), () => false),
  )
  const [action, setAction] = useState(defaultData)

  const { mode,currentQuestion } = useContext(PageTestContext)
  const checkRadio = (index, bool) => {
    // update value
    let radioValueArr = action
    radioValueArr[index] = bool
    // check isCorrect
    let statusArr = []
    trueAnswers.forEach((item, i) =>
      statusArr.push(item === radioValueArr[i] ? true : false),
    )

    setProgressActivityValue(radioValueArr, statusArr)
    setAction([...radioValueArr])
  }
  //change question
  useEffect(() => {
    const defaultData = getProgressActivityValue(
      Array.from(Array(answerList.length), () => false),
    )
    setAction(defaultData)
  }, [currentQuestion])
  useEffect(
    () =>
    mode.state == TEST_MODE.play && setProgressActivityValue(
        action,
        trueAnswers.map((item, i) => (item === action[i] ? true : false)),
      ),
    [],
  )

  const { userAnswer } = useProgress()

  const checkDanger = (index) => {
    if (mode.state === TEST_MODE.review && userAnswer.status[index] === false)
      return '--danger'
    return ''
  }

  const checkSuccess = (index) => {
    if (
      (mode.state === TEST_MODE.review && userAnswer.status[index] === true) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = (index) => {
    let className = ''
    className += checkDanger(index)
    className += ' '
    className += checkSuccess(index)
    return className
  }

  const handleRadioClick = (i, value) => checkRadio(i, value)

  return (
    <GameWrapper className="pt-o-game-tf3">
      <div className="pt-o-game-tf3__container">
        <div className="pt-o-game-tf3__left" data-animate="fade-right">
          <div className="__image">
            <img
              alt="instruction-image"
              src={`${imageInstruction}`}
              onClick={() => setIsZoom(true)}
            />
          </div>
          <ImageZooming
            data={{ alt: 'instruction', src: `${imageInstruction}` }}
            status={{
              state: isZoom,
              setState: setIsZoom,
            }}
          />
        </div>
        <div className="pt-o-game-tf3__right" data-animate="fade-left">
          <div className="__container">
            <div className="__header">
              <CustomHeading tag="h6" className="__description">
                <FormatText tag="span">{instruction}</FormatText>
              </CustomHeading>
            </div>
            <div className="__content" id="content-scrollbar">
              <div
                className="__example"
                style={{ display: examList.length > 0 ? '{}' : 'none' }}
              >
                <div className="header-example">
                  <span>{examList.length > 1 ? 'Examples:' : 'Example:'}</span>
                </div>
                {examList.map((item, i) => (
                  <div key={i + 1} className="list-example">
                    <div className="__example-text">
                      <span dangerouslySetInnerHTML={{__html:convertStrToHtml(item.text)}}></span>
                    </div>
                    <div
                      className={`__result ${
                        item.isCorrect ? 'True' : 'False'
                      }`}
                    >
                      <span>{item.isCorrect === true ? 'True' : 'False'}</span>
                    </div>
                  </div>
                ))}
              </div>
              <div className="__play-answers">
                <div className="header-answer">
                  <span>
                    {answerList.length > 1 ? 'Questions:' : 'Question:'}
                  </span>
                </div>
                <div className="list_answers">
                  {answerList.map((item, index) => (
                    <div key={index + 1} className="list-answers__item">
                      <div
                        className={`__answer-text ${generateClassName(index)}`}
                      >
                        <span dangerouslySetInnerHTML={{__html:convertStrToHtml(item.text)}}></span>
                      </div>
                      <div className="__result">
                        <Toggle
                          size="md"
                          checked={
                            mode.state === TEST_MODE.check
                              ? trueAnswers[index]
                              : action[index]
                          }
                          checkedChildren="True"
                          unCheckedChildren="False"
                          onChange={() => {
                            mode.state === TEST_MODE.play &&
                              handleRadioClick(index, !action[index])
                          }}
                        />
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </GameWrapper>
  )
}
