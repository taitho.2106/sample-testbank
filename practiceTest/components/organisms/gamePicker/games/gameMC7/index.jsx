import { useContext, useEffect, useRef, useState } from 'react'

import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomHeading } from '../../../../atoms/heading'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'

export const GameMC7 = ({ data }) => {
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const instruction = data?.instruction || ''
    const questionsGroup = data?.questionsGroup || []
    
    const questionsGroupExample = questionsGroup.filter((m) =>
        m.question.startsWith('*'),
    )
    const questionsGroupGeneral = questionsGroup.filter(
        (m) => !m.question.startsWith('*'),
    )
    const trueAnswer = questionsGroupGeneral.map((list) =>
        list.answers.findIndex((item) => item.isCorrect === true),
    )

    const audio = useRef(null)
    const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
        useProgress()
    const { mode, currentQuestion } = useContext(PageTestContext)

    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)
    const [isShowTapescript, setIsShowTapescript] = useState(false)

    const [radio, setRadio] = useState([])

    const handleRadioClick = (i, j) => {
        let beforeArr = radio
        if (beforeArr[i] === undefined) return
        beforeArr[i] = j
        // check isCorrect
        let statusArr = []
        trueAnswer.forEach((item, m) =>
          statusArr.push(item === beforeArr[m] ? true : false),
        )
        // save progress
        setProgressActivityValue(beforeArr, statusArr)
        // display
        setRadio([...beforeArr])
    }
        
    //change question
    useEffect(() => {
        const defaultRadio = getProgressActivityValue(
            Array.from(Array(questionsGroupGeneral.length), (e, i) => null),
        )
        setRadio(defaultRadio)
    }, [currentQuestion])

    const checkPrimary = (i, j) => {
        if (
            mode.state === TEST_MODE.play &&
            radio[i] !== undefined &&
            radio[i] === j
        )
            return '--checked'
        return ''
    }

    const checkDanger = (i, j) => {
        if (
            mode.state === TEST_MODE.review &&
            userAnswer.status[i] === false &&
            radio[i] !== undefined &&
            radio[i] === j
        )
            return '--danger'
        return ''
    }

    const checkSuccess = (i, j) => {
        if (
            (mode.state === TEST_MODE.review &&
                userAnswer.status[i] === true &&
                radio[i] !== undefined &&
                radio[i] === j) ||
            (mode.state === TEST_MODE.check && trueAnswer[i] === j)
        )
            return '--success'
        return ''
    }

    const generateClassName = (i, j) => {
        let className = ''
        className += checkPrimary(i, j)
        className += ' '
        className += checkDanger(i, j)
        className += ' '
        className += checkSuccess(i, j)
        return className
    }

    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        else {
            const countDownValue = audio.current.duration - audio.current.currentTime
            if (countDownValue > 0)
                setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
            else {
                setIsPLaying(false)
                audio.current.currentTime = 0
            }
        }
    }
    
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])

    const renderClassName = (value) => {
        let answerClassName = ''
        if (value?.answers?.length === 4) {
            answerClassName = '--images-flex-wrap'
        }
        else{
            answerClassName = '--images-multi-line'
        }
    return answerClassName

    }

    const renderClassNameEx = (value) => {
        let answerClassName = ''
        if (value?.answers?.length === 4) {
            answerClassName = '--images-flex-wrap'
        }
        else{
            answerClassName = '--images-multi-line'
        }
        return answerClassName
    }
    
    const canTouch = () => {
        const isTouch = 'ontouchstart' in window;
        return isTouch ? '--touch' : '--no-touch'
    }
    return (
        <GameWrapper className="pt-o-game-mc7-audio">
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>
            <div data-animate="fade-in" className='pt-o-game-mc7-audio__container'>
            {isShowTapescript ? (
                <div className="__paper">
                    <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
                        <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                    </div>
                    <BlockPaper>
                        <div className="__content">
                            <FormatText>{audioScript}</FormatText>
                        </div>
                    </BlockPaper>
                </div>
            ) : (
        <>
            <BlockWave className={`pt-o-game-mc7-audio__left ${mode.state !== TEST_MODE.play ? 'pt-o-game-mc7__result' : ''}`}>
                <AudioPlayer
                    className="__player"
                    isPlaying={isPlaying}
                    setIsPlaying={() => setIsPLaying(!isPlaying)}
                />
                <span className="__duration">{countDown}</span>
            </BlockWave>
            <div className="pt-o-game-mc7-audio__right" data-animate="fade-in">
                {mode.state !== TEST_MODE.play && (
                    <CustomButton
                    className="__tapescript"
                    onClick={() => setIsShowTapescript(true)}
                    >
                    Tapescript
                    </CustomButton>
                )}
                <div className="__header">
                    <FormatText tag="p">{instruction}</FormatText>
                </div>
                <div className="__content">
                {questionsGroupExample && questionsGroupExample.length > 0 && (
                    <div className="__content-example">
                        <div className="__title">
                            <span>
                                {`Example${questionsGroupExample.length > 1 ? 's' : ''}:`}
                            </span>
                        </div>
                        <div className={`__content`}>
                        {questionsGroupExample.map((itemQuestion, index) => {
                            const html = convertStrToHtml(itemQuestion.question.substring(1),).replaceAll('%s%', `<div class="__space"></div>`)
                            return (
                            <div
                                key={`question-${index}`}
                                className={`mc7-example-group`}
                            >
                                <div className={`mc7-example-group__question`}>
                                    <div className="mc7-example-group__question__title">
                                        <span
                                        dangerouslySetInnerHTML={{
                                            __html: html,
                                        }}
                                        ></span>
                                    </div>
                                </div>
                                <div className={`mc7-example-group__radio-answer ${renderClassNameEx(itemQuestion)}`}>
                                {itemQuestion.answers.map((itemChild, i) => (
                                    <div 
                                        key={`question-ans-${i}`}
                                        className="mc7-example-group__radio-answer__ex-item"
                                        style={{
                                            boxShadow: itemChild.isCorrect ? '0px 0px 0px 5px #2dd238' : '0px 0px 0px 2px #d4e9ff'
                                        }}
                                    >
                                        <img src={itemChild.imageAns} alt=''/>
                                        <div className={`icon-true ${itemChild.isCorrect}`}>
                                            <img 
                                                src={'/images/icons/ic-checked-true.png'}
                                                alt=''
                                            />
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                            )
                        })}
                        </div>
                    </div>
                    )}
                    {questionsGroupGeneral && questionsGroupGeneral.length > 0 && (
                    <div className="__content-question">
                        <div className="__title">
                            <span>
                                {`Question${questionsGroupGeneral.length > 1 ? 's' : ''}:`}
                            </span>
                        </div>
                        <div className="__content">
                        {questionsGroupGeneral.map((item, i) => {
                            const html = convertStrToHtml(item.question).replaceAll(
                            '%s%',
                            `<div class="__space"></div>`,
                            )
                            return (
                            <div key={i} className={`__radio-group ${canTouch()}`}>
                                {i > 0 && (<div style={{ width: '70%', transform: 'translateX(25%)'}}><hr style={{border:'1px solid #2692ff'}}/></div>)
                                }
                                <CustomHeading tag="h6" className="__radio-heading">
                                    <span
                                        dangerouslySetInnerHTML={{
                                        __html: html,
                                        }}
                                    ></span>
                                </CustomHeading>
                                <div className={`__radio-list ${renderClassName(item)}`}>
                                {item.answers.map((childItem, j) => (
                                    <div
                                        key={j}
                                        className={`__radio-item ${generateClassName(i,j,)}`}
                                        style={{
                                            pointerEvents:
                                            mode.state === TEST_MODE.play ? 'all' : 'none',
                                        }}
                                        onClick={() =>
                                            mode.state === TEST_MODE.play &&
                                            handleRadioClick(i, j)
                                        }
                                    >
                                        <img src={childItem.imageAns}alt=''/>
                                        <div className={`icon-true ${generateClassName(i,j,)}`}>
                                            <img 
                                                src={
                                                    generateClassName(i,j,).trim() === '--checked' ||  generateClassName(i,j,).trim() === '--success' ? 
                                                        '/images/icons/ic-checked-true.png' : 
                                                        '/images/icons/ic-checked-false.png' 
                                                    }
                                                alt=''
                                            />
                                        </div>
                                    </div>
                                ))}
                                </div>
                            </div>
                            )
                        })}
                        </div>
                    </div>
                    )}
                </div>
            </div>
        </>
        )}
            </div>
            
        </GameWrapper>
    )
}
