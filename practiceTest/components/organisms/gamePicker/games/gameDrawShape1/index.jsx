import { useRef, useState, useContext, useEffect } from 'react'

import ShapeComponent from 'components/atoms/shape'
import Guid from 'utils/guid'
import { useWindowSize } from 'utils/hook'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'
import {isIOS } from 'utils/log'
const COLOR = {
  exam: '67ab8d',
  correct: '2dd238',
  danger: 'ff7f74',
  stroke_bg: '97bee5',
}
export default function GameDrawShape1({ data }) {
  const heightItemRef = useRef({ left: 80, right: 36 })
  const elementDragRef = useRef(null)
  const elementTouchRef = useRef(null)
  const leftSelectedIndex = useRef(null)
  let answer = data?.answers ?? []
  const shapes = data.shapes
  const indexExample = answer.findIndex((m) => m.isExam)
  if (indexExample != -1) {
    const itemExample = answer.find((m) => m.isExam)
    answer.splice(indexExample, 1)
    answer.splice(0, 0, itemExample)
  }
  let indexAnswer = -1
  answer = answer.map((m) => {
    if (!m.isExam) {
      indexAnswer += 1
      return { ...m, indexAnswer: indexAnswer }
    } else {
      return m
    }
  })
  const { getProgressActivityValue, setProgressActivityValue } = useProgress()
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const audio = useRef(null)
  const { mode } = useContext(PageTestContext)
  const defaultAnswer = getProgressActivityValue(answer.filter((item) => !item.isExam).map(() => null))
  const [answerGroup, setAnswerGroup] = useState(defaultAnswer)
  function checkMobile() {
    if (sessionStorage.desktop)
      // desktop storage
      return false
    else if (localStorage.mobile)
      // mobile storage
      return true

    // alternative
    const mobile = [
      'iphone',
      'ipad',
      'android',
      'blackberry',
      'nokia',
      'opera mini',
      'windows mobile',
      'windows phone',
      'iemobile',
      'tablet',
      'mobi',
    ]
    let ua = navigator.userAgent.toLowerCase()
    for (let i in mobile) if (ua.indexOf(mobile[i]) > -1) return true
    const ios = isIOS()
    
    return ios
  }
  const checkM = checkMobile();
  const isMobile = useRef(checkM)

  useEffect(() => {
    const checkCorrect = answerGroup.map((item, index) => {
      if (!item) return false
      const itemAnswer = answer.find((m) => m.color == item.c && m.shape == item.s)
      if (itemAnswer && itemAnswer.indexAnswer == index) return true
      return false
    })
    setProgressActivityValue(answerGroup, checkCorrect)
  }, [answerGroup])
  const [width, height] = useWindowSize()
  useEffect(() => {
    if(width == 0 || height == 0) return;
    const isPortrait = window.matchMedia('(orientation: portrait)').matches
    if (!isPortrait) {
      if (width > 1300) {
        heightItemRef.current = { left: 80, right: 36 }
      } else if (width > 1024) {
        heightItemRef.current = { left: 60, right: 36 }
      } else if (width > 960) {
        heightItemRef.current = { left: 60, right: 35 }
      } else if (width > 768) {
        heightItemRef.current = { left: 48, right: 30 }
      } else if (width > 640) {
        heightItemRef.current = { left: 42, right: 30 }
      } else {
        heightItemRef.current = { left: 40, right: 26 }
      }
    } else {
      if (width > 480) {
        if (height > 1300) {
          heightItemRef.current = { left: 80, right: 44 }
        } else if (height > 1024) {
          heightItemRef.current = { left: 70, right: 42 }
        } else if (height > 960) {
          heightItemRef.current = { left: 68, right: 38 }
        } else if (height > 768) {
          heightItemRef.current = { left: 62, right: 36 }
        } else {
          heightItemRef.current = { left: 50, right: 36 }
        }
      } else {
        if (width > 360) {
          heightItemRef.current = {  left: 48, right: 32 }
        } else if (width > 320) {
          heightItemRef.current = { left: 46, right: 28 }
        } else {
          heightItemRef.current = { left: 40, right: 25 }
        }
      }
    }
    setAnswerGroup([...answerGroup])
  }, [width, height])
  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0) setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }
  const enableScrollView = (mode)=>{
    const contentLeft = document.querySelector(".pt-o-game-draw-shape-1__right .__content-draw")
    const contentRight = document.querySelector(".pt-o-game-draw-shape-1__right .__content-shape div")
    if(mode){
        contentLeft && contentLeft.classList.remove("disable-scroll-ds1")
        contentRight && contentRight.classList.remove("disable-scroll-ds1")
        document.body.classList.remove("disable-scroll-ds1")
    }else{
      contentLeft && contentLeft.classList.add("disable-scroll-ds1")
      contentRight && contentRight.classList.add("disable-scroll-ds1")
      document.body.classList.add("disable-scroll-ds1")
    }
  }
  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])
  useEffect(() => {

    return ()=> {
      enableScrollView(true)
    }
  }, [])
  const setIsShowTapescript = (isShow) => {
    const elementScript = document.getElementById('pt-o-game-draw-shape-1-script')
    const elementRight = document.querySelector('.pt-o-game-draw-shape-1__right')
    const elementLeft = document.querySelector('.pt-o-game-draw-shape-1__left')
    if (isShow) {
      elementScript.style.display = 'block'
      elementLeft.style.display = 'none'
      elementRight.style.display = 'none'
    } else {
      elementScript.style.display = 'none'
      elementLeft.style.display = 'flex'
      elementRight.style.display = 'flex'
    }
  }
  const renderShape = (shape, color, height, strokeStyle) => {
    // strokeDasharray, strokeColor="", strokeWidth,
    switch (shape) {
      case 'circle':
        return (
          <ShapeComponent.CircleShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'triangle':
        return (
          <ShapeComponent.TriangleShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'square':
        return (
          <ShapeComponent.SquareShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'star':
        return (
          <ShapeComponent.StarShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'rectangle':
        return (
          <ShapeComponent.RectangleShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
      case 'oval':
        return (
          <ShapeComponent.OvalShape
            fill={color}
            height={height}
            stroke={strokeStyle?.strokeColor}
            strokeWidth={strokeStyle?.strokeWidth}
            strokeDasharray={strokeStyle?.strokeDasharray}
          />
        )
    }
  }

  const dragStart = (e, shape, color, className, indexAnswer) => {
    e.dataTransfer?.setData('item_class', className)
    e.dataTransfer?.setData('shape', shape)
    e.dataTransfer?.setData('color', color)
    if (indexAnswer) {
      e.dataTransfer?.setData('index_data_transfer', indexAnswer)
    }
  }
  const drop = (e, index) => {
    e.preventDefault()
    if (e.dataTransfer?.getData('item_class') == 'item-shape') {
      const shape = e.dataTransfer.getData('shape')
      const color = e.dataTransfer.getData('color')
      answerGroup[index] = { s: shape, c: color }
      leftSelectedIndex.current = index+""
      setAnswerGroup([...answerGroup])
    } else if (e.dataTransfer?.getData('item_class') == 'item-draw-shape__image') {
      const shape = e.dataTransfer.getData('shape')
      const color = e.dataTransfer.getData('color')
      const indexBefore = e.dataTransfer.getData('index_data_transfer')
      const itemCurrent = answerGroup[index]
      answerGroup[index] = { s: shape, c: color }
      if (itemCurrent) {
        answerGroup[parseInt(indexBefore)] = {
          s: itemCurrent.s,
          c: itemCurrent.c,
        }
      } else {
        answerGroup[parseInt(indexBefore)] = null
      }
      leftSelectedIndex.current = index+""
      setAnswerGroup([...answerGroup])
    }
  }
  const dropContentShape = (e) => {
    e.preventDefault()
    if (e.dataTransfer?.getData('item_class') == 'item-draw-shape__image') {
      const indexBefore = e.dataTransfer.getData('index_data_transfer')
      answerGroup[parseInt(indexBefore)] = null
      setAnswerGroup([...answerGroup])
    }
  }
  const onClickItemLeft = (e, item) => {
    if (mode.state === TEST_MODE.play && !item.isExam) {
      if (e.currentTarget.classList.contains('--selected')) {
        const listItemLeft = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-draw-shape')
        listItemLeft.forEach((m) => m.classList.remove('--selected'))
        e.currentTarget.classList.remove('--selected')
        leftSelectedIndex.current = null
      } else {
        const listItemLeft = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-draw-shape')
        listItemLeft.forEach((m) => m.classList.remove('--selected'))
        e.currentTarget.classList.add('--selected')
        leftSelectedIndex.current = item.indexAnswer + ''
      }
      allowDrop(e)
    }
  }
  const touchStartImageLeft = (e, shape, color, indexAnswer) => {
    // console.log("e.changedTouches[0]",e.changedTouches[0].clientY +"///"+ e.changedTouches[0].pageY)
    // alert(e.changedTouches[0].clientY +"///"+ e.changedTouches[0].pageY)
    const listItemLeft = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-draw-shape')
    listItemLeft.forEach((m) => {
      if (!m.classList.contains('--example')) {
        m.classList.add('--selected')
      }
    })
    const contentShape = document.querySelector('.pt-o-game-draw-shape-1__right .__content-shape')
    contentShape.classList.add('--selected')
    // dragStart(e, key, color, 'item-shape')

    //create element for show drag
    const target = e.currentTarget;
    const eventInfo = {
      currentTarget: target,
      clientX: e.changedTouches[0].clientX,
      clientY: e.changedTouches[0].clientY,
    }
    elementTouchRef.current = cloneElementDraw(shape, color, 0.9, target, eventInfo, indexAnswer)
    elementTouchRef.current.style.visibility = 'hidden'
    setTimeout(() => {
      if(elementTouchRef.current){
        elementTouchRef.current.style.visibility = 'visible'
      }
      const canVibrate = window.navigator.vibrate
      if (canVibrate) window.navigator.vibrate(100)
    }, 220);
  }
  const touchEndImageLeft = (e) => {
    const boxSvgDragRef = elementTouchRef.current.childNodes[0].getBoundingClientRect()
    const topDrag = e.changedTouches[0].clientY - boxSvgDragRef.height / 2
    const leftDrag = e.changedTouches[0].clientX - boxSvgDragRef.width / 2

    removeElementSelectAndDragging()
    const contentShape = document.querySelector('.pt-o-game-draw-shape-1__right .__content-shape')
    const listItemLeft = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-draw-shape')
    let indexSelectedSuccess = elementTouchRef.current.dataset['indexanswer'];

    const boxContentShape = contentShape.getBoundingClientRect()
    const inBoxContentShape =
      leftDrag >= boxContentShape.left &&
      leftDrag <= boxContentShape.left + boxContentShape.width - boxSvgDragRef.width &&
      topDrag >= boxContentShape.top &&
      topDrag <= boxContentShape.top + boxContentShape.height - boxSvgDragRef.height
    //check remove shape
    if (inBoxContentShape) {
      answerGroup[parseInt(elementTouchRef.current.dataset['indexanswer'])] = null
      setAnswerGroup([...answerGroup])
    } else {
      //check drop if in shape box
      for (let i = 0; i < listItemLeft.length; i++) {
        const m = listItemLeft[i]
        if(m.classList.contains("--example")) continue
        const dropBoxDimension = m.getBoundingClientRect()
        const { top, left, width, height } = dropBoxDimension
        let inTopBox =
          leftDrag >= left &&
          leftDrag <= left + width - boxSvgDragRef.width &&
          topDrag >= top &&
          topDrag <= top + height - boxSvgDragRef.height
        if (inTopBox) {
          const shape = elementTouchRef.current.dataset['shape']
          const color = elementTouchRef.current.dataset['color']
          const indexItem = parseInt(m.dataset['indexanswer'])
          indexSelectedSuccess = indexItem +""
          const itemBefore = answerGroup[indexItem]
          answerGroup[indexItem] = {
            s: shape,
            c: color,
          }
          if (itemBefore) {
            answerGroup[parseInt(elementTouchRef.current.dataset['indexanswer'])] = { s: itemBefore.s, c: itemBefore.c }
          } else {
            answerGroup[parseInt(elementTouchRef.current.dataset['indexanswer'])] = null
          }
          setAnswerGroup([...answerGroup])
          break
        }
      }
    }
    leftSelectedIndex.current = indexSelectedSuccess;
    elementTouchRef.current && elementTouchRef.current.remove()
    elementTouchRef.current = null
    
  }
  const touchStartItemRight = (e, shapeItem, color) => {
    const listItemLeft = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-draw-shape')
    listItemLeft.forEach((m) => {
      if (!m.classList.contains('--example')) {
        m.classList.add('--selected')
      }
    })
    const svgElement = e.currentTarget.parentElement.querySelector('.item-shape-svg')
    const ratio =
      heightItemRef.current.left - 4 > svgElement.offsetHeight
        ? (heightItemRef.current.left - 4) / svgElement.offsetHeight
        : 1
    // alert(ratio)
    const eventInfo = {
      currentTarget: e.currentTarget,
      clientY: e.changedTouches[0].clientY,
      clientX: e.changedTouches[0].clientX,
    }
    const target = e.currentTarget
    elementTouchRef.current = cloneElementDraw(shapeItem, color, ratio, svgElement, eventInfo)
    elementTouchRef.current.style.visibility = 'hidden'
    target.parentElement.classList.add('--dragging')
    setTimeout(() => {
      if(elementTouchRef.current){
        elementTouchRef.current.style.visibility = 'visible'
      }
      const canVibrate = window.navigator.vibrate
      if (canVibrate) window.navigator.vibrate(100)
    }, 220);

  }
  const touchEndItemRight = (e) => {
    const boxSvgDragRef = elementTouchRef.current.childNodes[0].getBoundingClientRect()
    const topDrag = e.changedTouches[0].clientY - boxSvgDragRef.height / 2
    const leftDrag = e.changedTouches[0].clientX - boxSvgDragRef.width / 2

    const listItemLeft = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-draw-shape')
    listItemLeft.forEach((m) => m.classList.remove('--selected'))

    const listItemRightDragging = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-shape.--dragging')
    listItemRightDragging.forEach((m) => m.classList.remove('--dragging'))

    for (let i = 0; i < listItemLeft.length; i++) {
      const m = listItemLeft[i]
      const dropBoxDimension = m.getBoundingClientRect()
      const { top, left, width, height } = dropBoxDimension
      let inTopBox =
        leftDrag >= left &&
        leftDrag <= left + width - boxSvgDragRef.width &&
        topDrag >= top &&
        topDrag <= top + height - boxSvgDragRef.height
      if (inTopBox) {
        const shape = elementTouchRef.current.dataset['shape']
        const color = elementTouchRef.current.dataset['color']
        answerGroup[parseInt(m.dataset['indexanswer'])] = { s: shape, c: color }
        setAnswerGroup([...answerGroup])
        break
      }
    }
    elementTouchRef.current && elementTouchRef.current.remove()
    elementTouchRef.current = null
  }

  const onClickItemRight = (shape, color) => {
    if (leftSelectedIndex.current != null) {
      answerGroup[parseInt(leftSelectedIndex.current)] = { s: shape, c: color }
      const listItemLeft = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-draw-shape')
      listItemLeft.forEach((m) => m.classList.remove('--selected'))
      leftSelectedIndex.current = null
      setAnswerGroup([...answerGroup])
    }
  }
  const cloneElementDraw = (shape, color, ratio, elementClone, eventInfo, indexAnswer) => {
    const elementDrag = document.createElement('div')
    elementDrag.classList.add('temp-drag-drop-ds1')
    elementDrag.style.position = 'fixed'
    elementDrag.dataset['shape'] = shape
    elementDrag.dataset['color'] = color
    if (typeof indexAnswer == 'number') {
      elementDrag.dataset['indexanswer'] = indexAnswer
    }
    elementDrag.style.touchAction = 'none'

    const elementW = elementClone.offsetWidth
    const elementH = elementClone.offsetHeight
    elementDrag.style.height = elementH + 'px'
    elementDrag.style.width = elementW + 'px'
    elementDrag.style.transform = `scale(${ratio},${ratio})`
    elementDrag.innerHTML = elementClone.innerHTML
    // alert(e.currentTarget.classList)

    const boxSvgDrag = eventInfo.currentTarget.getBoundingClientRect()
    const topDrag = eventInfo.clientY - boxSvgDrag.height / 2
    const leftDrag = eventInfo.clientX - boxSvgDrag.width / 2
    elementDrag.style.top = topDrag + 'px'
    elementDrag.style.left = leftDrag + 'px'
    elementDrag.style.pointerEvents = 'none'
    document.body.append(elementDrag)
    return elementDrag
  }

  //remove selecting and dragging status
  const removeElementSelectAndDragging = () => {
    const listItemLeft = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-draw-shape')
    listItemLeft.forEach((m) => {
      if (!m.classList.contains('--example') && m.dataset["indexanswer"] != leftSelectedIndex.current) {
        m.classList.remove('--selected')
      }
    })
    const contentShape = document.querySelector('.pt-o-game-draw-shape-1__right .__content-shape')
    contentShape.classList.remove('--selected')
    const listItemRightDragging = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-shape.--dragging')
    listItemRightDragging.forEach((m) => m.classList.remove('--dragging'))
  }

  const allowDrop = (e) => e.preventDefault()
  return (
    <GameWrapper className="pt-o-game-draw-shape-1">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() => setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))}
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-draw-shape-1__container">
        <>
          <div className="__paper" id="pt-o-game-draw-shape-1-script" style={{ display: 'none' }}>
            <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
          <BlockWave className={`pt-o-game-draw-shape-1__left`}>
            <AudioPlayer className="__player" isPlaying={isPlaying} setIsPlaying={() => setIsPLaying(!isPlaying)} />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <div className="pt-o-game-draw-shape-1__right">
            {mode.state !== TEST_MODE.play && (
              <CustomButton className="__tapescript" onClick={() => setIsShowTapescript(true)}>
                Tapescript
              </CustomButton>
            )}
            <div className="__header">
            {instruction}
            </div>
            <div className="__content">
              <div className="__content-draw">
                {answer.map((item, index) => {
                  let color = item.color
                  let shape = item.shape
                  let isCorrect = false
                  let className = ''
                  const itemAnswerGroup = answerGroup[item.indexAnswer]
                  if (!item.isExam) {
                    if (mode.state === TEST_MODE.play || mode.state === TEST_MODE.review) {
                      color = itemAnswerGroup?.c
                      shape = itemAnswerGroup?.s
                    }
                  }
                  if (mode.state == TEST_MODE.play) {
                    if (itemAnswerGroup) {
                      className = '--matched'
                    }
                  }
                  if (mode.state == TEST_MODE.check) {
                    isCorrect = true
                    className = '--success'
                  }
                  if (mode.state == TEST_MODE.review) {
                    if (itemAnswerGroup) {
                      className = '--danger'
                      if (item.color == color && item.shape == shape) {
                        isCorrect = true
                        className = '--success'
                      }
                    }
                  }
                  if (item.isExam) {
                    className = '--example'
                  }
                  const disable = mode.state !== TEST_MODE.play || item.isExam
                  return (
                    <div className={`item-draw-card`} key={item.key}>
                      <div
                        className={`item-draw-shape ${className} ${leftSelectedIndex.current == item.indexAnswer && !item.isExam ?"--selected":""}` }
                        data-indexanswer={item.indexAnswer}
                        onClick={(e) => {
                          onClickItemLeft(e, item)
                          e.preventDefault()
                          e.stopPropagation()
                        }}
                        onDrop={(e) => {
                          elementTouchRef.current && elementTouchRef.current.remove()
                          elementDragRef.current = null
                          if (mode.state === TEST_MODE.play && !item.isExam) {
                            e.stopPropagation()
                            removeElementSelectAndDragging()
                            drop(e, item.indexAnswer)
                          }
                        }}
                        onDragOver={(e) => {
                          if (mode.state === TEST_MODE.play && !item.isExam) {
                            // e.currentTarget.classList.add('--selected')
                            allowDrop(e)
                          }
                        }}
                        onDragLeaveCapture={(e) => {
                          if (mode.state === TEST_MODE.play && !item.isExam) {
                            // const listItemLeft = document.querySelectorAll(".pt-o-game-draw-shape-1__right .item-draw-shape")
                            // listItemLeft.forEach(m=> m.classList.remove("--selected"))
                          }
                        }}
                        onDragEnd={(e) => {
                          elementTouchRef.current && elementTouchRef.current.remove()
                          elementDragRef.current = null
                          if (mode.state === TEST_MODE.play && !item.isExam) {
                            removeElementSelectAndDragging()
                          }
                        }}
                      >
                        <div data-animate="fade-in"
                          className="item-draw-shape__image"
                          style={disable ? { pointerEvents: 'none' } : {}}
                          draggable={!(disable || isMobile.current)}
                          onDragStart={(e) => {
                            if (isMobile.current) {
                              e.preventDefault()
                              return
                            }
                            elementDragRef.current = 1
                            if (elementTouchRef.current) {
                              elementTouchRef.current.remove()
                            }
                            // alert("onDragStart")
                            if (mode.state === TEST_MODE.play && !item.isExam && itemAnswerGroup) {
                              const listItemLeft = document.querySelectorAll(
                                '.pt-o-game-draw-shape-1__right .item-draw-shape',
                              )
                              listItemLeft.forEach((m) => {
                                if (!m.classList.contains('--example')) {
                                  m.classList.add('--selected')
                                }
                              })
                              const contentShape = document.querySelector('.pt-o-game-draw-shape-1__right .__content-shape')
                              contentShape.classList.add('--selected')
                              const listItemRightDragging = document.querySelectorAll('.pt-o-game-draw-shape-1__right .item-shape.--dragging')
                              listItemRightDragging.forEach((m) => m.classList.remove('--dragging'))
                              dragStart(e, shape, color, 'item-draw-shape__image', item.indexAnswer + '')
                            }else{
                              e.preventDefault()
                            }
                            
                          }}
                          onTouchStart={(e) => {
                            e.stopPropagation()
                            e.currentTarget.style.touchAction="none"
                            if (!itemAnswerGroup || !isMobile.current) return
                            elementTouchRef.current && elementTouchRef.current.remove()
                            touchStartImageLeft(e, shape, color, item.indexAnswer)
                            enableScrollView(false)
                          }}
                          onTouchMove={(e) => {
                            if (elementDragRef.current) {
                              return
                            }
                            if (elementTouchRef.current == null) return
                            enableScrollView(false)
                            e.currentTarget.style.touchAction="none"
                            const boxSvgDragRef = elementTouchRef.current.childNodes[0].getBoundingClientRect()
                            const topDrag = e.changedTouches[0].clientY - boxSvgDragRef.height / 2
                            const leftDrag = e.changedTouches[0].clientX - boxSvgDragRef.width / 2
                            elementTouchRef.current.style.top = topDrag + 'px'
                            elementTouchRef.current.style.left = leftDrag + 'px'
                            elementTouchRef.current.style.visibility = 'visible'
                            elementTouchRef.current.focus()
                          }}
                          onTouchCancel={(e) => {
                            elementTouchRef.current && elementTouchRef.current.remove()
                            removeElementSelectAndDragging()
                            enableScrollView(true)
                          }}
                          onTouchEnd={(e) => {
                            e.preventDefault()
                            removeElementSelectAndDragging()
                            if (elementTouchRef.current != null){
                              touchEndImageLeft(e)
                            }
                            enableScrollView(true)
                          }}
                        >
                          {shape && color && renderShape(shape, `#${color}`, heightItemRef.current.left, null)}
                          {!item.isExam &&
                          shape &&
                          color &&
                          (mode.state == TEST_MODE.check || mode.state == TEST_MODE.review) ? (
                            <img className="icon-result" alt="" src={`/images/icons/ic-${isCorrect}-so.png`}></img>
                          ) : (
                            ''
                          )}
                        </div>
                      </div>
                      <span className="item-draw-text">{`${item.isExam ? 'Example' : item.indexAnswer + 1}`}</span>
                    </div>
                  )
                })}
              </div>
              <div
                className="__content-shape"
                draggable={!isMobile.current}
                onDrop={(e) => {
                  dropContentShape(e)
                }}
                onDragOver={(e) => {
                  if (e.dataTransfer?.types.indexOf('index_data_transfer') != -1) {
                    allowDrop(e)
                  }
                }}
              >
                <div>
                  {Object.keys(shapes).map((key, index) => {
                    const shapeItem = key
                    return (
                      <div className="list-shape" key={`${data.id}_shape_${key}`}  data-animate="fade-in">
                        {shapes[key].map((color) => {
                          let className = ''
                          let strokeDasharray = ''
                          let strokeColor = ''
                          let strokeWidth = 0
                          const isExam =
                            answer.findIndex((m) => m.color == color && m.shape == shapeItem && m.isExam) != -1
                          let itemAnswerGroupIndex = answerGroup.findIndex(
                            (item) => item && item.s == shapeItem && item.c == color,
                          )
                          if (isExam) {
                            color = `${COLOR.stroke_bg}55`
                            strokeColor = `#${COLOR.exam}`
                            strokeDasharray = '2,2'
                            strokeWidth = 1
                          } else {
                            if (mode.state == TEST_MODE.check) {
                              className = '--success'
                              const itemAnswer = answer.find((m) => m.shape == shapeItem && m.color == color)
                              if (itemAnswer) {
                                color = `${COLOR.correct}20`
                                strokeColor = `#${COLOR.correct}`
                                itemAnswerGroupIndex = itemAnswer.indexAnswer
                                strokeWidth = 1
                                strokeDasharray = '2,2'
                              } else {
                                itemAnswerGroupIndex = -1
                                color = `${color}20`
                                strokeColor = `#${COLOR.correct}`
                                strokeWidth = 0
                              }
                            } else {
                              if (mode.state == TEST_MODE.play) {
                                if (itemAnswerGroupIndex != -1) {
                                  //matched
                                  color = `${COLOR.stroke_bg}22`
                                  strokeColor = `#${COLOR.stroke_bg}`
                                  strokeDasharray = '2,2'
                                  strokeWidth = 1
                                }
                              }
                              if (mode.state == TEST_MODE.review) {
                                const itemAnswer = answer.find((m) => m.shape == shapeItem && m.color == color)
                                if (itemAnswerGroupIndex !== -1) {
                                  className = '--danger'
                                  color = `${COLOR.danger}20`
                                  strokeColor = `#${COLOR.danger}`
                                  strokeWidth = 1
                                  strokeDasharray = '2,2'

                                  if (itemAnswer != null) {
                                    if (itemAnswer.indexAnswer == itemAnswerGroupIndex) {
                                      color = `${COLOR.correct}20`
                                      strokeColor = `#${COLOR.correct}`
                                      strokeWidth = 1
                                      strokeDasharray = '2,2'
                                      className = '--success'
                                    }
                                  }
                                } else {
                                  color = `${color}20`
                                  strokeColor = `#${COLOR.stroke_bg}`
                                  strokeWidth = 0
                                  strokeDasharray = '2,2'
                                }
                              }
                            }
                          }
                          const id = `${shapeItem}_${color}`
                          const disable =
                            mode.state !== TEST_MODE.play || itemAnswerGroupIndex != -1 || isExam ? true : false
                          return (
                            <div
                              style={disable ? { pointerEvents: 'none' } : {}}
                              key={Guid.newGuid()}
                              className={`item-shape ${className}`}
                              id={id}
                              data-shape={shapeItem}
                              data-color={color}
                            >
                              <div className="item-shape-svg">
                                {renderShape(shapeItem, `#${color}`, heightItemRef.current.right, {
                                  strokeDasharray,
                                  strokeColor,
                                  strokeWidth,
                                })}
                              </div>
                              <div
                                className="item-shape-draw"
                                draggable={!(disable || isMobile.current)}
                                onClick={(e) => {
                                  e.preventDefault()
                                  if (isMobile.current) return
                                  onClickItemRight(shapeItem, color)
                                }}
                                onDrag={(e) => {
                                  if (elementTouchRef.current == null) return
                                  e.stopPropagation()
                                  const boxSvgDragRef = elementTouchRef.current.getBoundingClientRect()
                                  const topDrag = e.clientY - boxSvgDragRef.height / 2
                                  const leftDrag = e.clientX - boxSvgDragRef.width / 2
                                  elementTouchRef.current.style.top = topDrag + 'px'
                                  elementTouchRef.current.style.left = leftDrag + 'px'
                                }}
                                onDragStart={(e) => {
                                  if (isMobile.current) return
                                  const listItemLeft = document.querySelectorAll(
                                    '.pt-o-game-draw-shape-1__right .item-draw-shape',
                                  )
                                  listItemLeft.forEach((m) => {
                                    if (!m.classList.contains('--example')) {
                                      m.classList.add('--selected')
                                    }
                                  })
                                  mode.state === TEST_MODE.play && itemAnswerGroupIndex == -1 && !isExam
                                  elementTouchRef.current && elementTouchRef.current.remove()
                                  const svgElement = e.currentTarget.parentElement.querySelector('.item-shape-svg')
                                  const ratio = (heightItemRef.current.left - 8) / svgElement.offsetHeight
                                  const eventInfo = {
                                    currentTarget: e.currentTarget,
                                    clientX: e.clientX,
                                    clientY: e.clientY,
                                  }

                                  //create element show draw
                                  elementTouchRef.current = cloneElementDraw(
                                    shapeItem,
                                    color,
                                    ratio,
                                    svgElement,
                                    eventInfo,
                                  )
                                  e.currentTarget.parentElement.classList.add('--dragging')
                                  dragStart(e, shapeItem, color, 'item-shape')
                                }}
                                onDragEnd={(e) => {
                                  elementTouchRef.current && elementTouchRef.current.remove()
                                  removeElementSelectAndDragging()
                                }}
                                onTouchStart={(e) => {
                                  if (!isMobile.current) return
                                  elementTouchRef.current && elementTouchRef.current.remove()
                                  enableScrollView(false)
                                  if (leftSelectedIndex.current != null) {
                                    onClickItemRight(shapeItem, color)
                                    e.preventDefault()
                                  } else {
                                    touchStartItemRight(e, shapeItem, color)
                                  }
                                }}
                                onTouchMove={(e) => {
                                  if (elementTouchRef.current == null) return
                                  e.stopPropagation()
                                  enableScrollView(false)
                                  e.currentTarget.style.touchAction="none"
                                  const boxSvgDragRef = elementTouchRef.current.getBoundingClientRect()
                                  const topDrag = e.changedTouches[0].clientY - boxSvgDragRef.height / 2
                                  const leftDrag = e.changedTouches[0].clientX - boxSvgDragRef.width / 2
                                  e.currentTarget.style.touchAction="none"
                                  elementTouchRef.current.style.top = topDrag + 'px'
                                  elementTouchRef.current.style.left = leftDrag + 'px'
                                }}
                                onTouchCancel={(e) => {
                                  enableScrollView(true)
                                  elementTouchRef.current && elementTouchRef.current.remove()
                                  removeElementSelectAndDragging()
                                }}
                                onTouchEnd={(e) => {
                                  enableScrollView(true)
                                  e.preventDefault()
                                  removeElementSelectAndDragging()
                                  if (elementTouchRef.current == null) return
                                  touchEndItemRight(e)
                                }}
                              ></div>
                              {
                                <div
                                  className="item-shape-placeholder"
                                  style={{
                                    position: 'absolute',
                                    top: '50%',
                                    left: '50%',
                                    transform: 'translate(-50%, -50%)',
                                  }}
                                >
                                  {renderShape(shapeItem, `#${COLOR.stroke_bg}22`, heightItemRef.current.right, {
                                    strokeDasharray: '2,2',
                                    strokeColor: `#${COLOR.stroke_bg}`,
                                    strokeWidth: 1,
                                  })}
                                </div>
                              }
                              {itemAnswerGroupIndex != -1 && (
                                <span draggable={false} className={`text-index-matching --text-in-shape__${shapeItem}`}>
                                  {itemAnswerGroupIndex + 1}
                                </span>
                              )}
                              {isExam && (
                                <span
                                  draggable={false}
                                  className={`text-index-matching ${
                                    isExam ? '--example' : ''
                                  } --text-in-shape__${shapeItem}`}
                                >
                                  Ex
                                </span>
                              )}
                            </div>
                          )
                        })}
                      </div>
                    )
                  })}
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </GameWrapper>
  )
}
