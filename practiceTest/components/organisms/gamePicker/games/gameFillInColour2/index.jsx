import { useRef, useState, useContext, useEffect } from 'react'

import { useWindowSize } from 'utils/hook'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'

const LIST_COLOR=[
  { color:'000000',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '00000080'
  },
  { color:'ffffff',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'd4dde780'
  },
  { color:'880015',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '88001580'
  },
  { color:'ed1c24',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'ed1c2480'
  },
  { color:'ff7f27',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'ff7f2780'
  },
  { color:'fcfb2d',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'fcfb2d80'
  },
  { color:'62aa2d',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '62aa2d80'
  },
  { color:'b5e61d',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'b5e61d80'
  },
  {color:'3d009e',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity:'3d009e80'
  },
  {color:'038af9',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '038af980'
  },
  {color:'8400ab',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: '8400ab80'
  },
  {color:'c8bfe7',
    icon:'/images/icons/icon-selected-colour-black.png',
    opacity: 'c8bfe780'
  },
  {color:'773829',
  icon:'/images/icons/icon-selected-colour-white.png',
  opacity: '77382980'
  },
  {color:'afafaf',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'afafaf80'
  },
  {color:'fb2576',
    icon:'/images/icons/icon-selected-colour-white.png',
    opacity: 'fb257680'
  }
]
export default function GameFillInColour2 ({data}){
  const [width, height] = useWindowSize()
  const renderUIGameFC2 = (data) => {
    if (width <= 1024 && window.matchMedia('(orientation: portrait)').matches) {
      return <GameFillInColour2Container key={`mobile_${data.id}_portrait`} data={data} />
    }
    return <GameFillInColour2Container key={`webpc_${data.id}`} data={data} />
  }
  return <>{renderUIGameFC2(data)}</>
}
const GameFillInColour2Container = ({ data }) => {
  const answer = data?.answers.filter(item => !item.isExam) ?? []
  const indexExample = data?.answers?.findIndex(item => item.isExam)
  const trueAnswer = answer
  const { getProgressActivityValue, setProgressActivityValue } = useProgress()
  const instruction = data?.instruction || ''
  const { mode, currentQuestion } = useContext(PageTestContext)

  const defaultAnswer = getProgressActivityValue(
    answer.map((item)=>({
      color: '',
      color_Correct: item.color
    }))
  )
  const intervalRef = useRef(null)
  const [answerGroup, setAnswerGroup] = useState(defaultAnswer)
  
  const colourSelected = useRef('')
  const [scaleNumber, setScaleNumber] = useState(0)
  const ratioRef = useRef(0)
  const [offsetImage, setOffsetImage] = useState({ top: 0, left: 0 })
  const ctxImageData = useRef(Array.from({length:data?.answers.length},()=>[]))
  const isPortrait = useRef(window.matchMedia('(orientation: portrait)').matches)
  useEffect(() => {
    return () => {
        setScaleNumber(0)
    }
  }, [])
  useEffect(()=>{
    intervalRef.current = setInterval(() => {
      const container = document.getElementById('pt-o-game-fill-in-colour-2-content-id')
      const contentImage = container.querySelector('.__content_image')
      const imageElement = container.querySelector('.image-instruction')
      const wraperImage = container.querySelector('.wrap-image')
      const isPortraitTemp = window.matchMedia('(orientation: portrait)').matches
      if(!imageElement || !imageElement.complete || imageElement.naturalWidth == 0) return
      const ratio = imageElement.width / imageElement.naturalWidth
      if(imageElement.height == 0 || imageElement.width == 0 || wraperImage.offsetHeight == 0 || wraperImage.offsetWidth == 0) return

      if(imageElement.height > contentImage.offsetHeight){
        imageElement.style.height = Math.floor(contentImage.offsetHeight) + 'px'
        imageElement.style.width = ''
      }
      if(ratio!=ratioRef.current){
        const listImage = wraperImage.querySelectorAll('.item-image-content')
        listImage.forEach((m)=>{
          onLoadImageItem(m)
        })
        setScaleNumber(ratio)
        ratioRef.current = ratio
      }else
      if(isPortraitTemp != isPortrait.current){
        isPortrait.current = isPortraitTemp
        const listImage = wraperImage.querySelectorAll('.item-image-content')
        listImage.forEach((m) => {
          onLoadImageItem(m)
        })
      }
    },200)
    return () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current)
      }
    }
  },[])
  const [width, height] = useWindowSize()
  useEffect(() => {
    if (width == 0 || height == 0) return
    const isPortraitTemp = window.matchMedia('(orientation: portrait)').matches
    const container = document.getElementById('pt-o-game-fill-in-colour-2-content-id')
    const imageInstruction = container.querySelector('.image-instruction')
    const listImage = container.querySelectorAll('.item-image-content')
    
    if(isPortraitTemp != isPortrait.current){
      imageInstruction.style.height = ''
      imageInstruction.style.width = ''
      setScaleNumber(0)
      listImage.forEach((m,index) => {
        const canvas = container.querySelector(`#canvas-image-item-${index}`)
        if(!canvas) return
        canvas.style.display = 'none'
        clearCanvas(canvas)
      })
    }

  }, [width, height])

  const getPointDraw = (elementImg, index,ratio) => {
    const canvasItem = document.getElementById(`canvas-image-item-${index}`)
    if(!canvasItem) return
    const ctx = canvasItem.getContext('2d')
    const imgHeight = elementImg.naturalHeight * ratio
    const imgWidth = elementImg.naturalWidth * ratio
    ctx.drawImage(elementImg, 0, 0, imgWidth, imgHeight)
    for(let i = imgHeight; i > 0; i--){
      const pixelData = ctx.getImageData(imgWidth / 2 + 8, i, 1, 1).data
      if (
        pixelData[0] !== 0 ||
        pixelData[1] !== 0 ||
        pixelData[2] !== 0 ||
        pixelData[3] !== 0
      ) {
        const posLeftIT = elementImg.parentElement.querySelector('.icon-success-image')
        const posLeftIF = elementImg.parentElement.querySelector('.icon-danger-image')
        if(!posLeftIF) return
        if(!posLeftIT) return
        posLeftIT.style.left = `${imgWidth / 2 + 8}px`
        posLeftIF.style.left = `${imgWidth / 2 + 8}px`

        posLeftIT.style.top = `${i - 8}px`
        posLeftIF.style.top = `${i - 8}px`
        break;
        // return { x: imgWidth / 2 + 8, y: i - 8}
      }
    }
  }
  useEffect(()=>{
    if(mode.state === TEST_MODE.check){
      const divImageItem = document.querySelectorAll('.item-image')
      divImageItem.forEach((item, mIndex)=>{
        const canvasItem = document.querySelector(`#canvas-image-item-${mIndex}`)
        const ctx = canvasItem.getContext('2d')
        clearCanvas(canvasItem)
        
        if(mIndex !== indexExample){
          const image = item.querySelector('.item-image-content')
          image.style.display = 'block'
          const indexAnswer = parseInt(item.dataset['ianswer'])
          ctx.beginPath()
          ctx.drawImage(image, 0, 0, canvasItem.width, canvasItem.height)
          ctx.globalCompositeOperation = 'source-in'
            
          ctx.fillStyle = `#${trueAnswer[indexAnswer]?.color}80`
          ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
          // ctx.fill()
          image.style.display = 'none'
        }else{
          const image = item.querySelector('.item-image-content')
          image.style.display = 'block'
          canvasItem.width = 1
          canvasItem.width = image.offsetWidth
          canvasItem.height = image.offsetHeight
          ctx.beginPath()
          ctx.drawImage(image, 0, 0, canvasItem.width, canvasItem.height)
          ctx.globalCompositeOperation = 'source-in'
            
          ctx.fillStyle = `#${data?.answers[indexExample].color}80`
          ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
          // ctx.fill()
          image.style.display = 'none'
        }
      })
    }
    if(mode.state === TEST_MODE.review){
      // console.log("check")
      const divImageItem = document.querySelectorAll('.item-image')
      divImageItem.forEach((item, mIndex)=>{
        const canvasItem = document.querySelector(`#canvas-image-item-${mIndex}`)
        const ctx = canvasItem.getContext('2d')
        clearCanvas(canvasItem)
        
        if(mIndex !== indexExample){
          const image = item.querySelector('.item-image-content')
          image.style.display = 'block'
          const indexAnswer = parseInt(item.dataset['ianswer'])
          ctx.beginPath()
          ctx.drawImage(image, 0, 0, canvasItem.width, canvasItem.height)
          ctx.globalCompositeOperation = 'source-in'
            
          ctx.fillStyle = answerGroup[indexAnswer]?.color.length > 0  ? `#${answerGroup[indexAnswer]?.color}80` : '#00000000'
          ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
          // ctx.fill()
          image.style.display = 'none'
        }else{
          const image = item.querySelector('.item-image-content')
          image.style.display = 'block'
          canvasItem.width = 1
          canvasItem.width = image.offsetWidth
          canvasItem.height = image.offsetHeight
          ctx.beginPath()
          ctx.drawImage(image, 0, 0, canvasItem.width, canvasItem.height)
          ctx.globalCompositeOperation = 'source-in'
            
          ctx.fillStyle = `#${data?.answers[indexExample].color}80`
          ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
          // ctx.fill()
          image.style.display = 'none'
        }
      })
    }
    const elements = document.querySelectorAll("#pt-o-game-fill-in-colour-2-content-id .item-image.--answer-div")
    elements.forEach((imageElement, index)=>{
      setTimeout(() => {
        const classList =  [...imageElement.classList];
        imageElement.classList=[]
        imageElement.classList.add("item-image")
        imageElement.classList.add("--answer-div")
        setTimeout(() => {
          classList.forEach(m=> {
            imageElement.classList.add(m)
          })
        }, 0);
      }, 10);
    })
  },[mode.state])

    //change question
    useEffect(() => {
      const defaultAnswer = getProgressActivityValue(
        answer.map((item)=>({
          color: '',
          color_Correct: item.color
        }))
      )
      setAnswerGroup(defaultAnswer)
  }, [currentQuestion])

  const handleClickImage = (e) => {
    if(colourSelected.current.length < 2) return
    const targetElement = e.currentTarget
    const rect = targetElement.getBoundingClientRect();
    const x = e.clientX - rect.left; //x position within the element.
    const y = e.clientY - rect.top; //y position within the element.

    const itemTempPosition = []
    answer.forEach((item)=>{
      const position = item.position.split(':')
      const minX = position[0]* scaleNumber
      const minY = position[1]* scaleNumber
      const maxX = position[2]* scaleNumber
      const maxY = position[3]* scaleNumber
      if(x >= minX && x <= maxX && y >= minY && y <= maxY){
        itemTempPosition.push(item)
      }
    })
    if(itemTempPosition.length > 0){
      for(let i = itemTempPosition.length - 1; i >= 0 ; i--){
        const elementItem = document.getElementById(itemTempPosition[i]?.key)
        if(elementItem.classList.contains('--example-div')) continue
        const indexAnswer = answer.findIndex(item => item.key === itemTempPosition[i]?.key)
        const indexA = elementItem.dataset['index']
        const imageItem = elementItem.querySelector('.item-image-content')
        const canvasItemA = elementItem.querySelector(`#canvas-image-item-${indexA}`)
        const ctxItemA = canvasItemA.getContext('2d')
        const rect = elementItem.getBoundingClientRect();
        let a = Math.round(e.clientX - rect.x); //x position within the element.
        let b = Math.round(e.clientY - rect.y); //y position within the element
        const imageData = ctxImageData.current[indexA]
        if(a > canvasItemA.width){
          a = canvasItemA.width
        }
        if(b > canvasItemA.height){
          b = canvasItemA.height
        }
        let pixelPosition =(a*4) + ((b*canvasItemA.width*4));
        const pixelDataCurrent = [imageData[(pixelPosition)],
        imageData[(pixelPosition + 1)],
        imageData[(pixelPosition + 2)],
        imageData[(pixelPosition + 3)]
      ]
      if(pixelDataCurrent[0] !== 0 || pixelDataCurrent[1] !== 0 || pixelDataCurrent[2] !== 0 || pixelDataCurrent[3]!== 0){
          if(colourSelected.current === answerGroup[indexAnswer].color) continue
          //canvas draw color
          e.stopPropagation()
          clearCanvas(canvasItemA)

          if(colourSelected.current.length > 2){

            ctxItemA.drawImage(imageItem,0,0, canvasItemA.width, canvasItemA.height)
            ctxItemA.beginPath()
            ctxItemA.drawImage(imageItem, 0, 0, canvasItemA.width, canvasItemA.height)
            ctxItemA.globalCompositeOperation = 'source-in'

            ctxItemA.fillStyle = `#${colourSelected.current}80`
            ctxItemA.fillRect(0,0, canvasItemA.width, canvasItemA.height)
            // ctxElementTarget.fill()
            imageItem.style.display = 'none'
            
            if(mode.state === TEST_MODE.play && answerGroup[indexAnswer]) {
                //handler data
              answerGroup[indexAnswer].color = colourSelected.current
              setAnswerGroup([...answerGroup])
              setProgressActivityValue(
                answerGroup, 
                answerGroup.map((item)=> item.color === item.color_Correct)
              )
            }
          }
          if(colourSelected.current.length === 2){
            ctxItemA.drawImage(imageItem, 0, 0, canvasItemA.width, canvasItemA.height)
            ctxItemA.globalCompositeOperation = 'source-in'

            imageItem.style.display = 'none'
  
            if(mode.state === TEST_MODE.play && answerGroup[indexAnswer]) {
              //handler data
            answerGroup[indexAnswer].color = ''
            setAnswerGroup([...answerGroup])
            setProgressActivityValue(
              answerGroup, 
              answerGroup.map((item)=> item.color === item.color_Correct)
            )
            }
          }
          break
        }
      }
    }
    
  }

  const clearCanvas = (canvas) => {
    if(!canvas) return;
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    const _width = canvas.width
    canvas.width = 1
    canvas.width = _width
    canvas.height = canvas.height
  }
  const handleChangeColor = (e) => {
    const elementTarget = e.currentTarget
    const dataIndex = parseInt(elementTarget.dataset['index'])
    colourSelected.current = elementTarget.dataset['color']
    const contentElement = document.querySelector('.wrap-image')
    const listColor = document.querySelectorAll('.__colour_item')
    const removeElement = document.querySelector('.__remove_colour')
    listColor.forEach((item, mIndex)=>{
      if(dataIndex !== mIndex){
        item.style.border = 'none'
        item.style.boxShadow = `0 0 0 1px #d4dde7`
        item.children[0].style.visibility = 'hidden'
        removeElement.src='/images/icons/ic-remove-colour.png'
      }else{
        item.style.border = '1px solid #000'
        item.style.boxShadow = `0 0 0 4px #${LIST_COLOR[dataIndex].opacity}`
        item.children[0].style.visibility = 'visible'
        contentElement.style.cursor = `url('/images/icons/ic-cursor-${LIST_COLOR[dataIndex].color}.png'),auto`
      }
    })
  }

const handlerRemove =(e)=>{
  const targetElement = e.currentTarget
  const contentElement = document.querySelector('.wrap-image')
  colourSelected.current = '-1'
  targetElement.src = '/images/icons/ic-selected-remove-colour.png'
  contentElement.style.cursor = `url('/images/icons/ic-cursor-remove.png'),auto`
  const listColor = document.querySelectorAll('.__colour_item')
  listColor.forEach(item=>{
    item.style.border = 'none'
    item.style.boxShadow = `0 0 0 1px #d4dde7`
    item.children[0].style.visibility = 'hidden'
  })
}

const checkDanger = (i) => {
    if (
      (i >= 0 && mode.state === TEST_MODE.review && 
        ((answerGroup[i].color.length > 0 && 
          answerGroup[i].color !== answerGroup[i].color_Correct)))
    )
        return '--danger'
    return ''
}

const checkSuccess = (i) => {
    if (
        (i >= 0 && mode.state === TEST_MODE.review && 
          (answerGroup[i].color === answerGroup[i].color_Correct)) || 
            mode.state === TEST_MODE.check
    )
        return '--success'
    return ''
}

const generateClassName = (i) => {
  if(i >= 0){
    let className = ''
    className = checkSuccess(i)
    if(!className){
      className = checkDanger(i)
    }
    if(!className){
      className = 'image-default'
    }
    return className
  }
} 

const onLoadImageInstruction = (e) => {
  const element = e.currentTarget
  if(element.height > element.parentElement.offsetHeight && element.parentElement.offsetHeight !=0){
    element.style.height = element.parentElement.offsetHeight + 'px'
  }
  const ratio = element.width / element.naturalWidth
  setScaleNumber(ratio)
  element.style.visibility = 'visible'
  ratioRef.current = ratio
  setOffsetImage({ top: element.offsetTop, left: element.offsetLeft })
}

const onLoadImageItem = (elementTarget) => {
  if(!elementTarget) return
  const index = elementTarget.parentElement.dataset['index']
  getPointDraw(elementTarget, index, scaleNumber)
  const canvasItem = document.getElementById(`canvas-image-item-${index}`)
  if(!canvasItem) return
  const ctx = canvasItem.getContext('2d')
  clearCanvas(canvasItem)
  const _width = canvasItem.width
  canvasItem.width = 1
  canvasItem.width = _width
  canvasItem.height = canvasItem.height
  const imageDraw = elementTarget
  if(Number(index) === indexExample){
    setTimeout(()=>{
      ctx.beginPath()
      ctx.drawImage(imageDraw, 0, 0, canvasItem.width, canvasItem.height)
      ctx.globalCompositeOperation = 'source-in'
      ctx.fillStyle = `#${data?.answers[indexExample].color}80`
      ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
    },100)
  }else{
    const indexAnswer = Number(elementTarget.parentElement.dataset['ianswer'])
    let colorFill = ''
    if(mode.state === TEST_MODE.check){
      colorFill = `#${trueAnswer[indexAnswer].color}80`
    }else{
      if(answerGroup[indexAnswer] && answerGroup[indexAnswer].color.length > 0){
        colorFill = `#${answerGroup[indexAnswer].color}80`
      }
    }
    if(colorFill.length > 0){
      setTimeout(()=>{
        ctx.beginPath()
        ctx.drawImage(imageDraw, 0, 0, canvasItem.width, canvasItem.height)
        ctx.globalCompositeOperation = 'source-in'
        ctx.fillStyle = colorFill
        ctx.fillRect(0,0, canvasItem.width, canvasItem.height)
      },100)
    }
  }
  canvasItem.style.display = 'block'
  elementTarget.style.display = 'none'
}

let iAnswer = -1
  return (
    <GameWrapper className="pt-o-game-fill-in-colour-2">
      <div
        data-animate="fade-in"
        className="pt-o-game-fill-in-colour-2__container"
      >
        <>
          <div
            className="pt-o-game-fill-in-colour-2__right"
          >
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-fill-in-colour-2-content-id">
              <div className='__temp_column'>
              </div>
              <div className='__content_image'>
                <div className="wrap-image" 
                style={{
                  objectFit: 'contain',
                  maxHeight: '100%',
                  position: 'relative',
                  maxWidth: '100%',
                }}
                  onClick={mode.state === TEST_MODE.play && handleClickImage.bind(null)}
                >
                    <img
                      className="image-instruction"
                      data-animate="fade-in"
                      alt="image-instruction"
                      src={data?.imageInstruction}
                      style={{ visibility: 'hidden', maxHeight: '100%'}}
                      onLoad={(e)=>{
                        onLoadImageInstruction(e)
                      }}
                    />
                    {data?.answers?.map((item, mIndex) => {
                      if(!item.isExam){
                        iAnswer++
                      }
                      // if (!scaleNumber) return null
                      const p = item.position.split(':')
                      const width = (p[2] - p[0]) * scaleNumber
                      const height = (p[3] - p[1]) * scaleNumber
                      const left = p[0] * scaleNumber
                      const top = p[1] * scaleNumber

                      const style = mode.state != TEST_MODE.play ? { pointerEvents: 'none' } : {}
                      style.position = 'absolute'
                      style.left = `${offsetImage.left + left}px`
                      style.top = `${offsetImage.top + top}px`
                      style.width = `${Math.floor(width)}px`
                      style.height = `${Math.floor(height)}px`
                      return(
                      <div
                        key={item.key}
                        id={item.key}
                        data-index={`${mIndex}`}
                        data-ianswer={`${item.isExam ? -1 : iAnswer}`}
                        // onClick={(e)=> mode.state === TEST_MODE.play && handleDrawImage(e)}
                        className={`item-image ${item.isExam ? '--example-div' : `--answer-div ${generateClassName(iAnswer)}`}`}
                        style={style}
                      >
                        <img 
                          src={item.image}
                          data-animate="fade-in"
                          alt=''
                          className={`item-image-content ${mIndex}`}
                          onLoad={(e)=>{
                            onLoadImageItem(e.currentTarget)
                          }}
                        />
                        <canvas
                          id={`canvas-image-item-${mIndex}`}
                          height={Math.floor(height)}
                          width={Math.floor(width)}
                        ></canvas>
                        {!item.isExam &&(
                        <>
                          <img 
                            src='/images/icons/ic-true-so.png'
                            alt=''
                            className='icon-success-image'
                          />
                          <img 
                            src='/images/icons/ic-false-so.png'
                            alt=''
                            className='icon-danger-image'
                          />
                        </>)}
                      </div>
                    )})}
                </div>
              </div>
              <div className='__content_colour'>
                <div className='__colour_list'>
                  {LIST_COLOR.map((item,mIndex)=>
                    <div key={`color__${mIndex}`}
                      className='__colour_item'
                      data-index={mIndex}
                      data-color={item.color}
                      data-opacity={item.opacity}
                      onClick={(e)=> mode.state === TEST_MODE.play && handleChangeColor(e)}
                      style={{
                        backgroundColor: `#${item.color}`,
                        cursor: mode.state === TEST_MODE.play ? 'pointer' : 'default'
                      }}
                    >
                      <img 
                        className='icon-selected'
                        src={item.icon}
                        alt=''
                      />
                    </div>
                  )}
                </div>
                <div className='__remove_colour_wrap'>
                  <img 
                    src='/images/icons/ic-remove-colour.png'
                    alt=''
                    className='__remove_colour'
                    onClick={(e)=> mode.state === TEST_MODE.play && handlerRemove(e)}
                    style={{
                      cursor: mode.state === TEST_MODE.play ? 'pointer' : 'default'
                    }}
                  />
                </div>
              </div>
            </div>
          </div>
        </>
      </div>
    </GameWrapper>
  )
}
