import { useWindowSize } from 'utils/hook'

import GameDrawLine2Container from './gameDrawLine2Container'
//các đoạn timeout add và remove class có style filter để fix bug không apply sự thay đổi filter trên macos, iphone ver 16
function GameDrawLine2({ data }) {
  const [width, height] = useWindowSize()
  const renderUIGameDrawLine = (data) => {
    if (width <= 1024 && window.matchMedia('(orientation: portrait)').matches) {
      return <GameDrawLine2Container key={`m_${data.id}_portrait`} data={data} />
    }
    return <GameDrawLine2Container key={`pc_${data.id}`} data={data} />
  }
  return <>{renderUIGameDrawLine(data)}</>
}

export default GameDrawLine2

