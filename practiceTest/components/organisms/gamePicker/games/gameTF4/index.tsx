import { useState, useEffect, useContext } from 'react'

import { Toggle } from 'rsuite'

import { TEST_MODE } from 'practiceTest/interfaces/constants'
import { PageTestContext } from 'practiceTest/interfaces/contexts'
import { useWindowSize } from 'utils/hook'

import useProgress from '../../../../../hook/useProgress'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { convertStrToHtml } from "@/utils/string";

function GameTF4({ data }: any) {
  const answers = data?.answers || []
  const instruction = data?.instruction || ''
  const imageList = data.imageInstruction.split('#')
  const dataList = answers.map((item: any, index: number) => {
    return {
      text: item.text,
      isCorrect: item.isCorrect,
      image: imageList[index],
    }
  })
  const examList = dataList
    .filter((item: any) => item.text.startsWith('*'))
    .map((item: any) => {
      return {
        text: item.text.replace('*', ''),
        isCorrect: item.isCorrect,
        image: item.image,
      }
    })

  const answerList = dataList.filter((item: any) => !item.text.startsWith('*'))
  const exampleImageList = examList.map((item: any) => item.image)
  const answerImageList = answerList.map((item: any) => item.image)
  const trueAnswers = answerList.map((item: any) => item.isCorrect)
  const { getProgressActivityValue, setProgressActivityValue } = useProgress()
  const defaultRadio = getProgressActivityValue(
    Array.from(Array(answerList.length), () => false),
  )
  const [action, setAction] = useState(defaultRadio)

  useEffect(
    () =>
    mode.state == TEST_MODE.play && setProgressActivityValue(
        action,
        trueAnswers.map((item: any, i: number) =>
          item === action[i] ? true : false,
        ),
      ),
    [],
  )
  const { mode, currentQuestion } = useContext(PageTestContext)
  const [isZoomA, setIsZoomA] = useState(false)
  const [isZoomE, setIsZoomE] = useState(false)
  //change question
  useEffect(() => {
    const defaultRadio = getProgressActivityValue(
      Array.from(Array(answerList.length), () => false),
    )
    setAction(defaultRadio)
  }, [currentQuestion])

  const checkRadio = (index: number, bool: any) => {
    // update value
    const radioValueArr = action
    radioValueArr[index] = bool
    // check isCorrect
    const statusArr: any[] = []
    trueAnswers.forEach((item: any, i: number) =>
      statusArr.push(item === radioValueArr[i] ? true : false),
    )
    // save progress
    setProgressActivityValue(radioValueArr, statusArr)
    setAction([...radioValueArr])
  }
  const handleRadioClick = (i: number, val: any) => checkRadio(i, val)
  const [zoomStrA, setZoomStrA] = useState('')
  const [zoomStrE, setZoomStrE] = useState('')

  const { userAnswer } = useProgress()

  const checkDanger = (index: number) => {
    if (mode.state === TEST_MODE.review && userAnswer.status[index] === false)
      return '--danger'
    return ''
  }

  const checkSuccess = (index: number) => {
    if (
      (mode.state === TEST_MODE.review && userAnswer.status[index] === true) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = (index: number) => {
    let className = ''
    className += checkDanger(index)
    className += ' '
    className += checkSuccess(index)
    return className
  }

  let answerClassName = ''
  for (let i = 0; i < examList.length; i++) {
    if (examList[i].text.length > 300) {
      answerClassName = '--flex-content'
      break
    }
  }
  const [height, setHeight] = useState(0)
  const [countLine, setCountLine] = useState(0)
  const lineHeight = 22
  const [width] = useWindowSize()
  const padding = width < 1024 ? 10 : 20
  useEffect(() => {
    const el: any = document.getElementById('span_text')
    setHeight(el.offsetHeight + padding)
    setCountLine(el.offsetHeight / lineHeight)
    renderClassLineHeight()
  }, [width])

  const renderClassLineHeight = () => {
    let className = ''

    if (countLine > 2) {
      className = '--countLine'
    } else {
      className = ''
    }
    return className
  }
  return (
    <GameWrapper className="pt-o-game-true-false-type4">
      <div style={{ height: 'inherit' }}>
        <div style={{ height: 'inherit' }}>
          <div
            className={`pt-o-game-true-false-type4__instruction ${renderClassLineHeight()}`}
            id="span_text"
          >
            <span>{instruction}</span>
          </div>
          <div
            className="pt-o-game-true-false-type4__container"
            style={{
              maxHeight: `calc(100% - ${height}px)`,
            }}
          >
            {examList.length > 0 && (
              <div className="pt-o-game-true-false-type4__exam-answers">
                <div className="header_exam">
                  <span>{examList.length > 1 ? 'Examples:' : 'Example:'}</span>
                </div>
                {zoomStrE.length > 0 && (
                  <ImageZooming
                    data={{
                      alt: `instruction_example`,
                      src: `${zoomStrE}`,
                    }}
                    status={{
                      state: isZoomE,
                      setState: setIsZoomE,
                    }}
                  />
                )}
                <div className={`item_exam  ${answerClassName}`}>
                  {examList.map((item: any, index: number) => (
                    <div key={index + 1} className="item_exam__list">
                      <>
                        <div className="__image">
                          <img
                            src={item.image}
                            alt="image-example"
                            height={100}
                            width={100}
                            onClick={() => {
                              setIsZoomE(true)
                              setZoomStrE(exampleImageList[index])
                            }}
                          />
                        </div>
                      </>
                      <div className="__content_text">
                        <span dangerouslySetInnerHTML={{__html: convertStrToHtml(item.text)}}></span>
                      </div>
                      <div
                        className={`__result ${
                          item.isCorrect ? 'True' : 'False'
                        }`}
                      >
                        <span>{item.isCorrect ? 'True' : 'False'}</span>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            )}
            <div className="pt-o-game-true-false-type4__play-answers">
              <div className="header_play-answers">
                <span>
                  {answerList.length > 1 ? 'Questions:' : 'Question:'}
                </span>
              </div>
              {zoomStrA.length > 0 && (
                <ImageZooming
                  data={{
                    alt: `instruction_answer`,
                    src: `${zoomStrA}`,
                  }}
                  status={{
                    state: isZoomA,
                    setState: setIsZoomA,
                  }}
                />
              )}
              {answerList.map((item: any, index: number) => (
                <div key={index + 1} className="item__answers">
                  <div className="item__answers__list">
                    <>
                      <div
                        className="__image"
                        onClick={() => {
                          setIsZoomA(true)
                          setZoomStrA(answerImageList[index])
                        }}
                      >
                        <img src={item.image} alt="image-questions" />
                      </div>
                    </>
                    <div
                      className={`__content_text ${generateClassName(index)}`}
                    >
                      <span dangerouslySetInnerHTML={{__html: convertStrToHtml(item.text)}}></span>
                    </div>
                    <div className="__result">
                      <Toggle
                        size="md"
                        checked={
                          mode.state === TEST_MODE.check
                            ? trueAnswers[index]
                            : action[index]
                        }
                        checkedChildren="True"
                        unCheckedChildren="False"
                        onChange={() => {
                          mode.state === TEST_MODE.play &&
                            handleRadioClick(index, !action[index])
                        }}
                      />
                    </div>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </GameWrapper>
  )
}

export default GameTF4
