import { useContext, useState } from 'react'

import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'
import Scrollbars from 'react-custom-scrollbars'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { FormatText } from '../../../../molecules/formatText'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { convertStrToHtml } from "../../../../../../utils/string";

export const GameMG4 = ({ data }) => {
  const answers = data?.answers
  const answersExample = data?.answersExample || []
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const audioScript = data.audioScript
  let defaultRightColumn = []
  let leftColumn = []
  let trueAnswer = []
  let wrongAnswer = Array.from(Array(answers.length), (e, i) => i)

  let checkClassItemLeft30 =
    answers.filter((m) => m.right && m.right.length > 200).length > 0 || (answersExample.length>0 && answersExample[0].right.length > 200)
  let checkClassItemRight30 =
    answers.filter((m) => m.left && m.left.length > 200).length > 0 || (answersExample.length>0 && answersExample[0].left.length > 200)
  answers.forEach((item, i) => {
    item.left && leftColumn.push(item.left)
    defaultRightColumn.push({
      id: `item-${i}`,
      content: item.right,
      index: i,
      isMatched: false,
    })
    if (item.rightAnswerPosition !== -1) {
      trueAnswer.push(item.rightAnswerPosition)
      wrongAnswer = wrongAnswer.filter(
        (filter) => filter !== item.rightAnswerPosition,
      )
    }

    ///check class
    if (item.left && item.left.length > 50) {
      checkClassItemLeft30 = false
    }
    if (item.right && item.right.length > 50) {
      checkClassItemRight30 = false
    }
  })
  if(answersExample.length>0){
    if(answersExample[0].left.length>50){
      checkClassItemLeft30 = false
    }
    if(answersExample[0].right.length>50){
      checkClassItemRight30 = false
    }
  }


  trueAnswer = trueAnswer.concat(wrongAnswer)

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()
  const { mode } = useContext(PageTestContext)

  const [isZoom, setIsZoom] = useState(false)
  if (userAnswer.value !== null) {
    let sortDefaultRightColumn = []
    userAnswer.value.forEach((item, i) => {
      const target = defaultRightColumn[item.index]
      if (target) {
        target.isMatched = item.isMatched
        sortDefaultRightColumn.push(target)
      }
    })
    defaultRightColumn = sortDefaultRightColumn
  }
  const orderRightColumn = getProgressActivityValue(
    Array.from(Array(defaultRightColumn.length), (e, i) => ({
      index: i,
      isMatched: false,
    })),
  )
  let defaultArr = []
  orderRightColumn.forEach((item, i) => {
    const target = defaultRightColumn[i]
    if (target) {
      target.isMatched = item.isMatched
      defaultArr.push(target)
    }
  })
  const [rightColumn, setRightColumn] = useState({ items: defaultArr })

  const checkDanger = (i) => {
    if (
      userAnswer.value !== null &&
      mode.state === TEST_MODE.review &&
      userAnswer.value[i] &&
      userAnswer.value[i].index !== trueAnswer[i] &&
      userAnswer.value[i].isMatched
    )
      return '--danger'
    return ''
  }

  const checkSuccess = (i) => {
    if (
      (userAnswer.value !== null &&
        mode.state === TEST_MODE.review &&
        userAnswer.value[i] &&
        userAnswer.value[i].index === trueAnswer[i] &&
        userAnswer.value[i].isMatched) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = (i) => {
    let className = ''
    className += checkDanger(i)
    className += ' '
    className += checkSuccess(i)
    return className
  }

  const getStyle = (style, snapshot) => {
    if (!snapshot.isDragging) return {}
    if (!snapshot.isDropAnimating) return style
    return {
      ...style,
      transitionDuration: `0.001s`,
    }
  }

  const onDragEnd = (result) => {
    // dropped outside the list
    if (!result.destination) return
    const items = reorder(
      rightColumn.items,
      result.source.index,
      result.destination.index,
    )
    const newArr = items.map((item) => item)
    const newArrClone = newArr.slice(0, trueAnswer.length);
    setProgressActivityValue(
      newArr.map((item) => ({
        index: item.index,
        isMatched: item.isMatched,
      })),
      leftColumn.map((item, i) =>
       ( trueAnswer[i] === newArrClone[i].index && newArrClone[i].isMatched == true)
          ? true
          : false,
      ),
    )
    setRightColumn({ items })
  }

  const reorder = (list, startIndex, endIndex) => {
    // a little function to help us with reordering the result
    const result = Array.from(list)
    // drag at same positon
    if (startIndex === endIndex) {
      if (result[startIndex].isMatched) result[startIndex].isMatched = false
      else result[startIndex].isMatched = true
      return result
    }
    // drag to other position
    const temp = result[endIndex]
    const deafultStartMatched = result[startIndex].isMatched
    result[endIndex] = result[startIndex]
    result[endIndex].isMatched = true
    result[startIndex] = temp
    result[startIndex].isMatched = deafultStartMatched
    return result
  }

  let leftClass =
    (checkClassItemLeft30 && checkClassItemRight30) ||
    (!checkClassItemLeft30 && !checkClassItemRight30)
      ? ''
      : checkClassItemLeft30
      ? '__33'
      : ''
  let rightClass =
    (checkClassItemLeft30 && checkClassItemRight30) ||
    (!checkClassItemLeft30 && !checkClassItemRight30)
      ? ''
      : checkClassItemRight30
      ? '__33'
      : ''
  if (leftClass && !rightClass) {
    rightClass = '__66'
  }else
  if (rightClass && !leftClass) {
    leftClass = '__66'
  }
  const checkSizeLg = leftColumn.some(m=>m.length >=55) || rightColumn.items?.some(m=>m?.content?.length >=55);
  const checkSizeXL = leftColumn.some(m=>m.length >=100) || rightColumn.items?.some(m=>m?.content?.length >=100);
  let classSize = ""
  if(checkSizeLg){
    classSize = "--lg"
  }
  if(checkSizeXL){
    classSize += " --xl"
  }
  
  return (
    <GameWrapper className="pt-o-game-mg4">
      <>
        {imageInstruction && (
          <div className="pt-o-game-mg4__left">
            <div className="__image">
              <img
                alt="image"
                className="__image-container"
                src={imageInstruction}
                onMouseDown={() => setIsZoom(true)}
              ></img>
              <ImageZooming
                data={{ alt: 'img', src: `${imageInstruction}` }}
                status={{
                  state: isZoom,
                  setState: setIsZoom,
                }}
              />
              <span className="__title-note">{audioScript}</span>
            </div>
          </div>
        )}
        <div
          className={`pt-o-game-mg4__right ${imageInstruction ? '' : '__100w'}`}
        >
          <div className="__header" id="header-instruction">
            <FormatText tag="p">{instruction}</FormatText>
          </div>
          <div className="__content">
            <Scrollbars universal={true}>
            {imageInstruction && (
                <div className="__image">
                  <img
                    alt="image"
                    className="__image-container"
                    src={imageInstruction}
                    onMouseDown={() => setIsZoom(true)}
                  ></img>
                  <span className="__title-note">{audioScript}</span>
                </div>
              )}
              {answersExample && answersExample.length > 0 && (
                <div>
                  <div className="__example">
                    <span className="__title">Example:</span>
                    <div className="__center-column">
                      <div className={`__left-item ${leftClass}`}>
                        <span className="__left-item__text"
                              dangerouslySetInnerHTML={{__html:convertStrToHtml(answersExample[0].left)}}>
                        </span>
                        <div className="__left-item__linked">
                          <div></div>
                          <div></div>
                        </div>
                      </div>
                      <div className={`__right-item ${rightClass}`}>
                        <div className="__right-item__linked">
                          <div></div>
                          <div></div>
                        </div>
                        <span className="__right-item__text"
                              dangerouslySetInnerHTML={{__html:convertStrToHtml(answersExample[0].right)}}>
                        </span>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className="__title-question">
                <span className="__title">{`Question${leftColumn.length > 1 ? 's' : ''}:`}</span>
              </div>
              <div>
                <div className={`__left ${leftClass}`}>
                  {leftColumn.map((item, i) => (
                    <div
                      key={i}
                      className={`__tag ${classSize} ${
                        rightColumn.items[i].isMatched === true ||
                        mode.state === TEST_MODE.check
                          ? '--matched'
                          : ''
                      } ${generateClassName(i)}`}
                    >
                      <span dangerouslySetInnerHTML={{
                        __html: convertStrToHtml(item)
                      }}></span>
                    </div>
                  ))}
                </div>
                <div
                  className={`__right ${rightClass}`}
                  style={{
                    pointerEvents:
                      mode.state === TEST_MODE.play ? 'all' : 'none',
                  }}
                >
                  <DragDropContext
                    onDragEnd={(result) =>
                      mode.state === TEST_MODE.play && onDragEnd(result)
                    }
                  >
                    <Droppable droppableId="droppable">
                      {(provided, snapshot) => (
                        <div
                          {...provided.droppableProps}
                          ref={provided.innerRef}
                        >
                          {rightColumn.items.map((item, index) => {
                            const text = mode.state === TEST_MODE.check
                                ? answers[trueAnswer[index]] &&
                                answers[trueAnswer[index]].right
                                : item.content
                            console.log({text});
                            return (
                                <div key={item.id} className={`__item ${classSize}`}>
                                  <Draggable draggableId={item.id} index={index}>
                                    {(provided, snapshot) => (
                                        <div
                                            className={`__tag ${classSize} ${
                                                snapshot.isDragging ? "--dragging" : ""
                                            } ${
                                                item.isMatched === true ||
                                                mode.state === TEST_MODE.check
                                                    ? "--matched"
                                                    : ""
                                            } ${generateClassName(index)}`}
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={getStyle(
                                                provided.draggableProps.style,
                                                snapshot
                                            )}
                                        >
                                          {<span dangerouslySetInnerHTML={{__html:convertStrToHtml(text)}}></span>}
                                          <div className="__circle"></div>
                                        </div>
                                    )}
                                  </Draggable>
                                </div>
                            );
                          })}
                          {provided.placeholder}
                        </div>
                      )}
                    </Droppable>
                  </DragDropContext>
                </div>
              </div>
            </Scrollbars>
          </div>
        </div>
      </>
    </GameWrapper>
  )
}
