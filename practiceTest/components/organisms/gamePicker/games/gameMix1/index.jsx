import { useContext, useEffect, useState, Fragment } from 'react'

import PageEnd from '@rsuite/icons/PageEnd'
import Scrollbars from 'react-custom-scrollbars'
import { Icon, Toggle } from 'rsuite'

import { convertStrToHtml } from 'utils/string';

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { checkIsScrollable } from '../../../../../interfaces/functionary'
import { CustomHeading } from '../../../../atoms/heading'
import { CustomText } from '../../../../atoms/text'
import { FormatText } from '../../../../molecules/formatText'
import { BlockPaper } from '../../../../organisms/blockPaper'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradient } from '../../../blockBottomGradient'


export const GameMix1 = ({ data }) => {
  const questionList = data?.questionList || []
  const questionInstruction = data?.questionInstruction || ''
  const instruction = data?.instruction || ''
  
  const dataTF = questionList.filter(item => item.activityType === 8)[0]
  const dataMC5 = questionList.filter(item => item.activityType === 11)[0]

  const answers = dataTF?.answers || []
  const questionsGroup = dataMC5?.questionsGroup || []
  
  const trueAnswers = answers.map((item) => item.isCorrect)

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } = useProgress()
  const { mode,currentQuestion } = useContext(PageTestContext)

  const defaultData = getProgressActivityValue(
    [
      Array.from(Array(answers.length), () => false),
      Array.from(Array(questionsGroup.length), (e, i) => null),
    ]
  );

  const [action, setAction] = useState(defaultData[0])
  const [statusArrSwitch, setStatusArrSwitch] = useState(defaultData[0])

  const [radio, setRadio] = useState(defaultData[1])
  const [statusArrRadio, setStatusArrRadio] = useState(defaultData[1])

  const trueAnswer2 = questionsGroup.map((list) =>
    list.answers.findIndex((item) => item.isCorrect === true),
  )

  const generateClassName2 = (i, j) => {
    let className = ''
    className += checkPrimary(i, j)
    className += ' '
    className += checkDanger(i, j)
    className += ' '
    className += checkSuccess(i, j)
    return className
  }

  const checkPrimary = (i, j) => {
    if (
      mode.state === TEST_MODE.play &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--checked'
    return ''
  }

  const checkDanger = (i, j) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.status[1] && userAnswer.status[1][i] === false &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--danger'
    return ''
  }
  
  const checkSuccess = (i, j) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.status[1] && userAnswer.status[1][i] === true &&
        radio[i] !== undefined &&
        radio[i] === j) ||
      (mode.state === TEST_MODE.check && trueAnswer2[i] === j)
    )
      return '--success'
    return ''
  }
  useEffect(()=>{
    let statusArr = []
    trueAnswers.forEach((item, i) =>
      statusArr.push(item === action[i]),
    )
    let statusArr2 = []
    trueAnswer2.forEach((item, i) =>
      statusArr2.push(item === radio[i]),
    )
    mode.state === TEST_MODE.play && setProgressActivityValue([action, radio], [statusArr, statusArr2])
  },[])

  const changeSwitch = (index, bool) => {
    // update value
    let radioValueArr = action
    radioValueArr[index] = bool
    // check isCorrect
    let statusArr = []
    trueAnswers.forEach((item, i) =>
      statusArr.push(item === radioValueArr[i]),
    )
    
    // set status game TF2
    setStatusArrSwitch(statusArr)

    // save progress
    setProgressActivityValue([radioValueArr, radio], [statusArr, statusArrRadio])
    
    // display
    setAction([...radioValueArr])
  }

  const handleSelect = (i, j) => {
    let beforeArr = radio
    if (beforeArr[i] === undefined) return
    beforeArr[i] = j
    // check isCorrect
    let statusArr = []
    trueAnswer2.forEach((item, i) =>
      statusArr.push(item === beforeArr[i]),
    )

    // set status game MC5
    setStatusArrRadio(statusArr)
    // save progress
    setProgressActivityValue([ action, beforeArr], [statusArrSwitch, statusArr])
    // display
    setRadio([...beforeArr])
  }
  //change question
  useEffect(() => {
    const defaultData = getProgressActivityValue(
      [
        Array.from(Array(answers.length), () => false),
        Array.from(Array(questionsGroup.length), (e, i) => null),
      ]
    );
    let statusArr = []
    trueAnswers.forEach((item, i) =>
        statusArr.push(item === defaultData[0][i]),
    )
    let statusArr2 = []
    trueAnswer2.forEach((item, i) =>
        statusArr2.push(item === defaultData[1][i]),
    )
    setAction(defaultData[0])
    setStatusArrSwitch(statusArr)
    setRadio(defaultData[1])
    setStatusArrRadio(statusArr2)
  }, [currentQuestion])

  return (
    <GameWrapper className="pt-o-game-mix1">
      <div className="pt-o-game-mix1__left">
        <BlockPaper>
          <div className="__instrustion">{instruction}</div>
          <div className="__content">
            <span
                dangerouslySetInnerHTML={{
                  __html: convertStrToHtml(questionInstruction),
                }}
              ></span>
          </div>
        </BlockPaper>
      </div>
      <div className="pt-o-game-mix1__right">
        <BlockBottomGradient className="__container">
          <div className="__content">
            <div className="__list">
              {/* <Scrollbars universal={true}> */}
                <div className="des1">
                  <FormatText tag="span">{dataTF?.instruction}</FormatText>
                </div>
                {answers.map((item, i) => (
                  <RadioItem
                    key={i}
                    content={convertStrToHtml(item.text)}
                    index={i}
                    trueAnswer={trueAnswers[i]}
                    state={action}
                    setState={changeSwitch}
                  />
                ))}
                <div className="des2">
                  <FormatText tag="span">{dataMC5?.instruction}</FormatText>
                </div>
                {questionsGroup.map((item, i) => (
                  <div key={i} className="__radio-group">
                    <CustomHeading tag="h6" className="__radio-heading">
                      {convertStrToHtml(item.question).split('%s%').map((splitItem, j) => (
                        <Fragment key={j}>
                          {j % 2 === 1 && <div className="__space"></div>}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: (splitItem),
                            }}
                          ></span>
                        </Fragment>
                      ))}
                    </CustomHeading>
                    <div className="__radio-list">
                      {item.answers.map((childItem, j) => (
                        <div
                          key={j}
                          className={`__radio-item ${generateClassName2(i, j)}`}
                          style={{
                            pointerEvents:
                              mode.state === TEST_MODE.play ? 'all' : 'none',
                          }}
                          onClick={() =>
                            mode.state === TEST_MODE.play && handleSelect(i, j)
                          }
                        >
                          <span
                            dangerouslySetInnerHTML={{
                              __html: convertStrToHtml(childItem.text),
                            }}
                          ></span>
                        </div>
                      ))}
                    </div>
                  </div>
                ))}
              {/* </Scrollbars> */}
            </div>
          </div>
        </BlockBottomGradient>
      </div>
    </GameWrapper>
  )
}

const RadioItem = ({ content, index, trueAnswer, state, setState }) => {
  const { mode } = useContext(PageTestContext)

  const { userAnswer } = useProgress()

  const checkDanger = () => {
    if (mode.state === TEST_MODE.review && userAnswer.status[0] && userAnswer.status[0][index] === false)
      return '--danger'
    return ''
  }

  const checkSuccess = () => {
    if (
      (mode.state === TEST_MODE.review && userAnswer.status[0] && userAnswer.status[0][index] === true) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = () => {
    let className = ''
    className += checkDanger()
    className += ' '
    className += checkSuccess()
    return className
  }

  const handleRadioClick = (i, val) => setState(i, val)

  return (
    <div className="__item">
      <div className={`__text ${generateClassName()}`}>
        <FormatText tag="p">{content}</FormatText>
      </div>
      <div
        className="__toggle"
        style={{
          pointerEvents: mode.state === TEST_MODE.play ? 'all' : 'none',
        }}
      >
        <Toggle
          className="__switcher"
          checked={mode.state === TEST_MODE.check ? trueAnswer : state[index]}
          checkedChildren="True"
          unCheckedChildren="False"
          onChange={() =>
            mode.state === TEST_MODE.play &&
            handleRadioClick(index, !state[index])
          }
        />
      </div>
    </div>
  )
}
