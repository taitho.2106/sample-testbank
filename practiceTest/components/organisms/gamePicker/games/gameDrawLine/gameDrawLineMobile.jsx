import { useRef, useState, useContext, useEffect } from 'react'

import { useWindowSize } from 'utils/hook'
import { isIOS } from 'utils/log'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'
function GameDrawLineMobile({ data, ref }) {
  const imageWidthOriginRef = useRef(0)
  const isAppleDevice = useRef(isIOS() || navigator.platform.toUpperCase().indexOf('MAC') >= 0)
  let indexAnswer = 0
  const indexExample = data.answers.findIndex((m) => m.isExam)
  if (indexExample != -1) {
    const itemExample = data.answers.find((m) => m.isExam)
    data.answers.splice(indexExample, 1)
    data.answers.splice(0, 0, itemExample)
  }
  data.answers = data?.answers.map((item, index) => {
    const itemAnswer = item.isExam ? item : { ...item, indexAnswer }
    if (!item.isExam) {
      indexAnswer += 1
    }
    return itemAnswer
  })
  const answer = data?.answers ?? []

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } = useProgress()

  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const instruction = data?.instruction || ''

  const [countDown, setCountDown] = useState(null)
  const [scaleNumber, setScaleNumber] = useState(0)
  const scaleNumberRef = useState(0)
  const [countButtonOneLine, setCountButtonOneLine] = useState(3)
  const [isPlaying, setIsPLaying] = useState(false)

  const { mode, currentQuestion } = useContext(PageTestContext)
  const audio = useRef(null)
  const idButton = useRef('')
  const idImage = useRef('')
  const intervalRef = useRef(null)

  const defaultAnswer = getProgressActivityValue(
    answer
      .filter((item) => !item.isExam)
      .map((item, index) => ({
        index: index,
        isMatched: false,
      })),
  )
  const answerGroup = useRef(defaultAnswer)

  useEffect(() => {
    if (userAnswer.value && userAnswer.value.length > 0) {
      answerGroup.current = userAnswer.value
    }
  }, [currentQuestion])

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0) setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }
  useEffect(() => {
    //fix ios apply more class has style drop-shadow
    if(isAppleDevice.current){
      const listImage = document.querySelectorAll("#pt-o-game-draw-line-1-content-id .--matching-div .item-image-content")
      listImage.forEach(el=>{
        const listClass = [... el.classList];
        while(el.classList.length>0){
          el.classList.remove(el.classList[0])
        }
        setTimeout(() => {
          el.classList.add(...listClass)
        }, 0);
      })
    }
  }, [mode.state])
  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  const onClickButton = (index, e) => {
    e.preventDefault()
    idButton.current = e.currentTarget.id
    const element = document.getElementById(e.currentTarget.id)
    const iconButtonMatching = element.querySelector('.icon-matching')
    const listButtonElement = document.querySelectorAll('.pt-o-game-draw-line-1 .block-item_button')
    iconButtonMatching.style.visibility = 'visible'

    listButtonElement.forEach((ele) => {
      if (ele?.classList.contains('selected') && ele.id != idButton.current) {
        ele.classList.remove('selected')
      }
    })

    if (element?.classList.contains('matched') || element?.classList.contains('matching')) {
      const imageMapingId = element.dataset['imageid']
      clearCanvas()
      doMatching(idButton.current, imageMapingId, true)
      return
    }
    element.classList.add('selected')
    clearCanvas()
    //hidden icon matching button.
    listButtonElement.forEach((ele) => {
      if (ele.id != idButton.current) {
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'hidden'
      }
      if (ele?.classList.contains('matching')) {
        ele.classList.remove('matching')
      }
    })

    const divImageList = document.querySelectorAll('.pt-o-game-draw-line-1 .item-image')
    divImageList.forEach((ele) => {
      const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
      iconMatchings.forEach((icon) => {
        if (!ele.dataset['buttonid']) {
          icon.style.visibility = 'visible'
        } else {
          icon.style.visibility = 'hidden'
        }
      })
      const imageElement = ele.querySelector('.item-image-content')
      if (imageElement && imageElement?.classList.contains('--image-selected')) {
        imageElement.classList.remove('--image-selected')
        setTimeout(() => {
          imageElement?.classList.add('--image-default')
        }, 0)
      }
    })

    //matching
    if (idButton.current && idImage.current) {
      setTimeout(() => {
        doMatching(idButton.current, idImage.current, true)
      }, 0)
    }
  }

  const onClickDeleted = (buttonId) => {
    const elementButton = document.getElementById(buttonId)

    const divImageMatchingId = elementButton.dataset['imageid']
    if (!divImageMatchingId) return

    //hidden icon matching image
    const divImageList = document.querySelectorAll('.pt-o-game-draw-line-1 .item-image')
    divImageList.forEach((ele) => {
      if (!ele.dataset['buttonid']) {
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => {
          icon.style.visibility = 'hidden'
        })
      }
    })

    const imageResult = elementButton.querySelector('.image-button')
    imageResult.style.display = 'none'
    imageResult.src = ''

    const divImageElement = document.getElementById(divImageMatchingId)
    const imageElement = divImageElement.querySelector('.item-image-content')

    //remove matched
    elementButton.dataset['imageid'] = ''
    divImageElement.dataset['buttonid'] = ''

    //update answer progress
    const indexImage = divImageElement.dataset['index']
    const indexButton = elementButton.dataset['index']
    if (indexImage && indexButton) {
      if (mode.state == TEST_MODE.play && answerGroup.current[parseInt(indexImage)]) {
        answerGroup.current[parseInt(indexImage)].index = parseInt(indexImage)
        answerGroup.current[parseInt(indexImage)].isMatched = false
        setProgressActivityValue(
          answerGroup.current,
          answerGroup.current.map((item, i) => {
            return item.isMatched && i == item.index
          }),
        )
      }
    }
    if (elementButton?.classList.contains('matched')) {
      elementButton.classList.remove('matched')
    }
    if (elementButton?.classList.contains('matching')) {
      elementButton.classList.remove('matching')
    }
    if (!elementButton?.classList.contains('selected')) {
      elementButton.classList.add('selected')
    }
    idButton.current = buttonId
    idImage.current = ''

    if (imageElement?.classList.contains('--image-matched')) {
      imageElement.classList.remove('--image-matched')
    }
    if (imageElement?.classList.contains('--image-selected')) {
      imageElement.classList.remove('--image-selected')
    }
    setTimeout(() => {
      imageElement?.classList.add('--image-default')
    }, 0)
    clearCanvas()
  }

  const onScrollListButton = () => {
    let matchingButton = document.querySelector('.block-item_button_right.matching')
    if (matchingButton) {
      const iconMatchingBtn = matchingButton.querySelector('.icon-matching')
      const divImageId = matchingButton.dataset['imageid']
      const divImageElement = document.getElementById(divImageId)
      const iconMatchingImage = divImageElement.querySelector(`.icon-matching-image`)
      clearCanvas()
      const diftX = 7
      const diftY = 7
      DrawLine(
        iconMatchingBtn,
        iconMatchingImage,
        diftX,
        diftY,
        matchingButton.classList.contains('--example') ? '#67ab8d' : '#2692ff',
      )
    }
  }
  useEffect(() => {
    return () => {
      clearCanvas()
      setScaleNumber(0)
    }
  }, [])
  const [width, height] = useWindowSize()
  useEffect(() => {
    if (width == 0 || height == 0) return
    if (width >= 768) {
      setCountButtonOneLine(3)
    } else {
      setCountButtonOneLine(2)
    }
    // console.log("baclll--0000000",{width,height})
    // console.log("baclll--1111",{width,height:window.innerHeight})
    // console.log("baclll--2222",{width,height:screen.height})
  }, [width, height])
  useEffect(() => {
    intervalRef.current = setInterval(() => {
      const imageInstruction = document.getElementById('dl1-image-instruction-id')
      if(!imageInstruction || !imageInstruction.complete || imageInstruction.naturalWidth == 0) return;
      const ratio = imageInstruction.width / imageInstruction.naturalWidth
      
      if(ratio != scaleNumberRef.current){
        const listImage = document.querySelectorAll("#pt-o-game-draw-line-1-content-id .item-image-content")
        listImage.forEach(m=>{
          if(m.complete){
            drawPointMatching(m,ratio)
          }
        })
        clearCanvas()
          const canvas = document.getElementById('canvas-draw-line-1')
          const content = document.getElementById('pt-o-game-draw-line-1-content-id')
          canvas.width = content.offsetWidth
          canvas.height = content.offsetHeight
          setScaleNumber(ratio)
          scaleNumberRef.current = ratio
      }
          
    }, 200);
    return () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current)
      }
    }
  }, [])
  const onClickImage = (index, value, e) => {
    e.preventDefault()
    e.stopPropagation()

    idImage.current = e.currentTarget.id
    //handler
    const element = e.currentTarget
    const imageElement = element.querySelector('.item-image-content')
    clearCanvas()
    const divImageAnswer = document.querySelectorAll('.pt-o-game-draw-line-1 .item-image')
    divImageAnswer.forEach((ele, i) => {
      const imageEle = ele.querySelector('.item-image-content')
      removeClassImageStatus(imageEle, '')
      if (ele.id != idImage.current) {
        setTimeout(() => {
          imageEle?.classList.add('--image-default')
        }, 0)
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => (icon.style.visibility = 'hidden'))
      } else {
        imageEle?.classList.remove('--image-default')
        setTimeout(() => {
          imageElement?.classList.add('--image-selected')
        }, 0)
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => (icon.style.visibility = 'visible'))
      }
    })
    if (imageElement?.classList.contains('--image-matched')) {
      const buttonMatchingId = element.dataset['buttonid']
      doMatching(buttonMatchingId, idImage.current, true)
      return
    }
    const listButtonElement = document.querySelectorAll('.block-item__list .block-item_button')
    //hidden icon matching button.
    listButtonElement.forEach((ele) => {
      ele.classList.remove('matching')

      if (!ele.dataset['imageid']) {
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'visible'
        if (!ele?.classList.contains('selected')) {
          ele.classList.add('selected')
        }
      } else {
        ele.classList.remove('selected')
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'hidden'
      }
    })
    if (idButton.current && idImage.current) {
      setTimeout(() => {
        doMatching(idButton.current, idImage.current, true)
      }, 0)
    }
  }
  const removeClassImageStatus = (element, ignoreClass) => {
    if (!element) return
    const classStatus = ['--image-selected', '--success', '--danger', '--image-default']
    classStatus.forEach((className) => {
      if (className != ignoreClass) {
        element.classList.remove(className)
      }
    })
  }
  const doMatching = (idBtn, idDivImg, isShowMatching = false) => {
    //matching button and image and show line
    try {
      if (idBtn && idDivImg) {
        const buttonMatching = document.getElementById(idBtn)
        const divImageMatching = document.getElementById(idDivImg)
        const buttonImage = buttonMatching.querySelector('.image-button')
        if (!buttonMatching || !divImageMatching || divImageMatching.childNodes.length == 0) return
        const imageMatching = divImageMatching.childNodes[0]
        //update answer progress
        const indexImage = divImageMatching.dataset['index']
        const indexButton = buttonMatching.dataset['index']
        if (indexImage && indexButton) {
          if (mode.state == TEST_MODE.play && answerGroup.current[parseInt(indexImage)]) {
            answerGroup.current[parseInt(indexImage)].index = parseInt(indexButton)
            answerGroup.current[parseInt(indexImage)].isMatched = true
            setProgressActivityValue(
              answerGroup.current,
              answerGroup.current.map((item, i) => {
                return item.isMatched && i == item.index
              }),
            )
          }
        }
        // mode state
        if (mode.state != TEST_MODE.play) {
          if (!buttonMatching.classList.contains('--example')) {
            if (indexButton != indexImage) {
              if (buttonMatching?.classList.contains('--success')) {
                buttonMatching.classList.remove('--success')
              }
              if (!buttonMatching?.classList.contains('--danger')) {
                buttonMatching.classList.add('--danger')
              }

              if (divImageMatching?.classList.contains('--success')) {
                divImageMatching.classList.remove('--success')
              }
              setTimeout(() => {
                divImageMatching.classList.remove('--default')
                setTimeout(() => {
                  divImageMatching.classList.add('--danger')
                }, 0)
              }, 0)
            } else {
              if (buttonMatching?.classList.contains('--danger')) {
                buttonMatching.classList.remove('--danger')
              }
              if (!buttonMatching?.classList.contains('--success')) {
                buttonMatching.classList.add('--success')
              }
              if (divImageMatching?.classList.contains('--danger')) {
                divImageMatching.classList.remove('--danger')
              }
              setTimeout(() => {
                divImageMatching.classList.remove('--default')
                setTimeout(() => {
                  divImageMatching.classList.add('--success')
                }, 0)
              }, 0)
            }
          }
        } else {
          const listButton = document.querySelectorAll('.pt-o-game-draw-line-1 .block-item_button')
          listButton.forEach((ele) => {
            if (ele?.classList.contains('selected')) {
              ele.classList.remove('selected')
            }
            if (ele?.classList.contains('matching')) {
              ele.classList.remove('matching')
            }
          })

          const listDivImageSelected = document.querySelectorAll('.pt-o-game-draw-line-1 .item-image')
          listDivImageSelected.forEach((ele) => {
            const eleImage = ele.childNodes[0]

            if (eleImage) {
              if (ele.id != idDivImg) {
                setTimeout(() => {
                  eleImage.classList.add('--image-default')
                }, 0)
                if (eleImage?.classList.contains('--image-selected')) {
                  eleImage.classList.remove('--image-selected')
                }
              }
            }
          })
        }
        if (mode.state != TEST_MODE.check) {
          buttonMatching.dataset['imageid'] = idDivImg
          divImageMatching.dataset['buttonid'] = idBtn
        }
        if (imageMatching) {
          buttonImage.src = imageMatching.src
          buttonImage.style.display = 'block'
        }

        // if(buttonMatching?.classList.contains("matched") && !buttonMatching?.classList.contains("--example")){
        //   buttonMatching.classList.remove("matched")
        // }

        if (!buttonMatching?.classList.contains('matched')) {
          buttonMatching.classList.add('matched')
        }
        const listIconImageMatching = document.querySelectorAll('.pt-o-game-draw-line-1 .icon-matching-image')
        listIconImageMatching.forEach((ele) => (ele.style.visibility = 'hidden'))
        const listIconButtonMatching = document.querySelectorAll(
          '.pt-o-game-draw-line-1 .block-item_button .icon-matching',
        )
        listIconButtonMatching.forEach((ele) => (ele.style.visibility = 'hidden'))

        divImageMatching.childNodes[0].classList.add('--image-matched')
        if (isShowMatching) {
          //show matching
          //buttonMatching.scrollIntoView();
          buttonMatching.classList.add('selected')
          buttonMatching.classList.add('matching')
          divImageMatching.childNodes[0].classList.remove('--image-default')
          setTimeout(() => {
            divImageMatching.childNodes[0].classList.add('--image-selected')
          }, 0)
          const canvas = document.getElementById('canvas-draw-line-1')
          const ctx = canvas.getContext('2d')
          const rectCanvas = canvas.getBoundingClientRect()

          const iconMatchingBtn = buttonMatching.querySelector('.icon-matching')
          const rectIconBtn = iconMatchingBtn.getBoundingClientRect()
          const xBtn = rectIconBtn.x - rectCanvas.x + 7
          const yBtn = rectIconBtn.top - rectCanvas.top + 7
          const iconImageMatching = divImageMatching.querySelector('.icon-matching-image')
          const rectImg = iconImageMatching.getBoundingClientRect()
          const xImg = rectImg.x - rectCanvas.x + 7
          const yImg = rectImg.top - rectCanvas.top + 7
          //draw

          ctx.beginPath()
          ctx.moveTo(xBtn, yBtn)
          ctx.lineWidth = 2
          ctx.lineCap = 'round'
          ctx.strokeStyle = buttonMatching?.classList.contains('--example') ? '#67ab8d' : '#2692ff'
          ctx.lineTo(xImg, yImg)
          ctx.stroke()

          idButton.current = ''
          idImage.current = ''

          iconImageMatching.style.visibility = 'visible'
          iconMatchingBtn.style.visibility = 'visible'
        }
      }
    } catch (error) {
      console.error('doMatching', error)
    }
  }
  const DrawLine = (elementFrom, elementTo, diffX, diffY, color) => {
    const canvas = document.getElementById('canvas-draw-line-1')
    const ctx = canvas.getContext('2d')
    const rectCanvas = canvas.getBoundingClientRect()

    const rectIconBtn = elementFrom.getBoundingClientRect()
    const xBtn = rectIconBtn.x - rectCanvas.x + 7
    const yBtn = rectIconBtn.top - rectCanvas.top + 7
    const rectImg = elementTo.getBoundingClientRect()
    const xImg = rectImg.x - rectCanvas.x + diffX
    const yImg = rectImg.top - rectCanvas.top + diffY

    //draw
    ctx.beginPath()
    ctx.moveTo(xBtn, yBtn)
    ctx.lineWidth = 2
    ctx.lineCap = 'round'
    ctx.strokeStyle = color
    ctx.lineTo(xImg, yImg)
    ctx.stroke()

    elementFrom.style.visibility = 'visible'
    elementTo.style.visibility = 'visible'
  }
  const clearCanvas = () => {
    const canvas = document.getElementById('canvas-draw-line-1')
    if (!canvas) return
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    const content = document.getElementById('pt-o-game-draw-line-1-content-id')
    canvas.width = content.offsetWidth
    canvas.height = content.offsetHeight
  }
  const setIsShowTapescript = (isShow) => {
    const elementScript = document.getElementById('pt-o-game-draw-line-1-script')
    const elementRight = document.querySelector('.pt-o-game-draw-line-audio__right')
    const elementLeft = document.querySelector('.pt-o-game-draw-line-audio__left')
    if (isShow) {
      elementScript.style.display = 'block'
      elementLeft.style.display = 'none'
      elementRight.style.display = 'none'
    } else {
      elementScript.style.display = 'none'
      elementLeft.style.display = 'flex'
      elementRight.style.display = 'flex'
    }
  }
  const drawPointMatching = (element, scale)=>{ 
    try {
      const divImageElement = element.parentElement
      const iconImageElement = divImageElement.querySelector('.icon-matching-image')
      const canvasImage = document.createElement('canvas')
      const ctxLeft = canvasImage.getContext('2d', { willReadFrequently: true })
      const imgHeight = element.naturalHeight
      const imgWidth = element.naturalWidth
      canvasImage.width = imgWidth
      canvasImage.height = imgHeight
      ctxLeft.drawImage(element, 0, 0, imgWidth, imgHeight)
      for (let j = imgHeight; j > 0; j--) {
        const pixelData = ctxLeft.getImageData(imgWidth / 2, j, 1, 1).data
        if (pixelData[0] !== 0 || pixelData[1] !== 0 || pixelData[2] !== 0 || pixelData[3] !== 0) {
          iconImageElement.style.visibility = 'hidden'
          iconImageElement.style.position = 'absolute'
          iconImageElement.style.top = `${j * scale - 6}px`
          iconImageElement.style.left = `${(imgWidth * scale) / 2}px`
          break
        }
      }
    } catch (error) {
      console.error('drawPointMatching', error)
    }
  }
  const drawPoint2 = (e) => {
    try {
      drawPointMatching(e.currentTarget,scaleNumber)
    } catch (error) {
      console.error('drawPoint2', error)
    }
  }
  const linkImage = (e, index) => {
    const element = e.currentTarget
    const divImageMatching = element.parentElement
    const showImageAnswer = (src, buttonMatching) => {
      const thumbnailAnswer = buttonMatching.querySelector('.image-button')
      thumbnailAnswer.src = src
      thumbnailAnswer.style.display = 'block'
    }
    if (answerGroup.current[index] && answerGroup.current[index].isMatched) {
      const answersList = answer.filter((item) => !item.isExam)
      const idItemButton = answersList[answerGroup.current[index].index].key
      clearCanvas()
      // const isShowLine = mode.state == TEST_MODE.play
      const buttonMatching = document.getElementById(idItemButton)

      const idItemImage = divImageMatching.id
      buttonMatching.dataset['imageid'] = idItemImage
      divImageMatching.dataset['buttonid'] = idItemButton
      showImageAnswer(element.src, buttonMatching)
    } else {
      if (divImageMatching.classList.contains('--example-div')) {
        const buttonMatching = document.getElementById(divImageMatching.id.replace('image_', ''))
        showImageAnswer(element.src, buttonMatching)
      }
    }
  }
  const generateClassImage = (item, index) => {
    const className = {
      image: '',
      div: '',
    }
    if (item.isExam) {
      className.image = ' --example-image --image-matched'
      return className
    }
    switch (mode.state) {
      case TEST_MODE.play:
        className.image += ' --image-default'
        const value1 = userAnswer?.value
        if (value1 && value1[index] && value1[index].isMatched) {
          className.image += ' --image-matched'
        }
        break
      case TEST_MODE.check:
        className.div += ' --success'
        className.image += ' --image-matched'
        break
      case TEST_MODE.review:
        const value = userAnswer?.value
        if (value && value[index] && value[index].isMatched) {
          if (value[index].index == item.indexAnswer) {
            className.div += ' --success'
            className.image += ' --image-matched'
          } else {
            if (value) className.div += ' --danger'
            className.image += ' --image-matched'
          }
        } else {
          className.image += ' --image-default'
        }
        break
    }
    return className
  }
  const generateClassButton = (item) => {
    let className = 'default'
    if (item.isExam) {
      className = ' matched --example'
    } else
      switch (mode.state) {
        case TEST_MODE.play:
          if (userAnswer?.value?.find((m) => m.index == item.indexAnswer && m.isMatched)) {
            className += ' matched'
          }
          break
        case TEST_MODE.check:
          className += ' --success'
          className += ' matched'
          break
        case TEST_MODE.review:
          if (userAnswer?.value && Array.isArray(userAnswer?.value)) {
            const indexUserAnswer = userAnswer.value.findIndex((m) => m.index == item.indexAnswer && m.isMatched)
            if (indexUserAnswer != -1) {
              if (indexUserAnswer == item.indexAnswer) {
                className += ' --success'
                className += ' matched'
              } else {
                className += ' --danger'
                className += ' matched'
              }
            }
          }
          break
      }
    return className
  }
  const getImageButton = (item) => {
    if (mode.state != TEST_MODE.check && !item.isExam) {
      if (userAnswer?.value && Array.isArray(userAnswer.value)) {
        const indexUser = userAnswer.value.findIndex((m) => m.index == item.indexAnswer && m.isMatched)
        if (indexUser == -1) return ''
        const itemCorrect = data.answers.find((m) => m.indexAnswer == indexUser)
        return itemCorrect?.image
      }
      return ''
    } else {
      return item.image
    }
  }
  const onLoadImageInstruction = (e) => {
    imageWidthOriginRef.current = e.currentTarget.naturalWidth
    const ratio = e.currentTarget.width / e.currentTarget.naturalWidth;
    setScaleNumber(ratio)
    e.currentTarget.style.visibility = 'visible'
    scaleNumberRef.current = ratio;
    const canvas = document.getElementById('canvas-draw-line-1')
    const content = document.getElementById('pt-o-game-draw-line-1-content-id')
    canvas.width = content.offsetWidth
    canvas.height = content.offsetHeight
  }
  const answerTop = []
  const answerBottom = []
  if (countButtonOneLine.length - 1 <= countButtonOneLine) {
    answerTop.push([...answer])
  } else {
    let countLine = Math.ceil(answer.length / 2)
    if (countLine < countButtonOneLine) {
      countLine = countButtonOneLine
    }
    answer.forEach((m, i) => {
      if (i < countLine) {
        answerTop.push(m)
      } else {
        answerBottom.push(m)
      }
    })
  }
  return (
    <GameWrapper className="pt-o-game-draw-line-audio pt-o-game-draw-line-1">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() => setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))}
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-draw-line-audio__container">
        <>
          <div className="__paper" id="pt-o-game-draw-line-1-script" style={{ display: 'none' }}>
            <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
          <BlockWave
            className={`pt-o-game-draw-line-audio__left 
              ${mode.state !== TEST_MODE.play ? 'pt-o-game-draw-line__result' : ''}`}
          >
            <AudioPlayer className="__player" isPlaying={isPlaying} setIsPlaying={() => setIsPLaying(!isPlaying)} />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <div className="pt-o-game-draw-line-audio__right">
            {mode.state !== TEST_MODE.play && (
              <CustomButton className="__tapescript" onClick={() => setIsShowTapescript(true)}>
                Tapescript
              </CustomButton>
            )}
            <div className="__header">
              <FormatText tag="p">{instruction}</FormatText>
            </div>
            <div className="__content" id="pt-o-game-draw-line-1-content-id">
              <div className="block-item"
              style={{padding:"1rem"}}>
                <div
                  className="wrap-image"
                  style={{
                    objectFit: 'contain',
                    maxHeight: '100%',
                    position: 'relative',
                    maxWidth: 'calc(100% - 0.7rem)',
                    marginLeft: '0.35rem',
                    marginRight: '0.35rem',
                    display:"flex"
                  }}
                >
                  <img
                    data-animate="fade-in"
                    className="image-instruction"
                    id="dl1-image-instruction-id"
                    alt="image-instruction"
                    // src={data?.imageInstruction}
                    style={{ visibility: 'hidden' }}
                    src={data?.imageInstruction}
                    onLoad={(e) => {
                      onLoadImageInstruction(e)
                    }}
                  />
                  {data?.answers?.map((item, mIndex) => {
                    if (!scaleNumber || scaleNumber > 1) return null
                    const p = item.position.split(':')
                    const width = (p[2] - p[0]) * scaleNumber
                    const height = (p[3] - p[1]) * scaleNumber
                    const left = p[0] * scaleNumber
                    const top = p[1] * scaleNumber

                    const style = mode.state != TEST_MODE.play ? { pointerEvents: 'none' } : {}
                    style.position = 'absolute'
                    style.left = `${left}px`
                    style.top = `${top}px`
                    style.width = `${width}px`
                    style.height = `${height}px`
                    const className = generateClassImage(item, item.indexAnswer)
                    return (
                      <div
                        data-index={item.indexAnswer}
                        key={`image_${item.key}`}
                        id={`image_${item.key}`}
                        onClick={onClickImage.bind(null, mIndex, item.image)}
                        style={style}
                        className={`item-image ${item.isExam ? '--example-div' : '--matching-div'} ${className.div}`}
                        data-buttonid={item.isExam ? item.key : ''}
                        data-animate="fade-in"
                      >
                        <img
                          alt=""
                          src={item.image}
                          className={`item-image-content ${className.image} ${
                            isAppleDevice.current ? '--is-apple-device' : ''
                          }`}
                          onLoad={(e) => {
                            drawPoint2(e)
                            linkImage(e, item.indexAnswer)
                          }}
                        ></img>
                        <img
                          alt=""
                          src={`/images/icons/ic-matching${item.isExam ? '-ex' : ''}.png`}
                          className="icon-matching-image --example-icon-matching"
                          style={{ visibility: 'hidden' }}
                        ></img>
                      </div>
                    )
                  })}
                </div>
              </div>
              <div className="block-item --item-content">
                <div className="block-item__list" onScroll={onScrollListButton}>
                  {answerTop.length > 0 && (
                    <div className="block-item__list__sub --top">
                      {answerTop.map((item, index) => {
                        const buttonImageUrl = getImageButton(item)
                        return (
                          <div
                            data-index={item.indexAnswer}
                            id={item.key}
                            key={item.key}
                            className={`block-item_button block-item_button_right ${generateClassButton(item)} ${
                              index >= Math.ceil(answer.length / 2) ? '--break-block' : ''
                            }`}
                            onClick={onClickButton.bind(null, index)}
                            style={mode.state != TEST_MODE.play ? { pointerEvents: 'none' } : {}}
                            data-imageid={item.isExam ? `image_${item.key}` : ''}
                          >
                            <button className="content-button">{item.answer}</button>
                            <img
                              className="icon-deleted"
                              height={25}
                              src="/images/icons/ic-delete-img.png"
                              alt="img-d"
                              onClick={onClickDeleted.bind(null, item.key)}
                            />
                            <img
                              style={{ visibility: 'hidden' }}
                              className="icon-matching"
                              height={10}
                              src={`/images/icons/ic-matching${item.isExam ? '-ex' : ''}.png`}
                              alt="img"
                            />
                            <img
                              height={20}
                              alt="img"
                              className="image-button"
                              src={buttonImageUrl}
                              style={buttonImageUrl ? {} : { display: 'none' }}
                            />
                          </div>
                        )
                      })}
                    </div>
                  )}
                  {answerBottom.length > 0 && (
                    <div className="block-item__list__sub --bottom">
                      {answerBottom.map((item, index) => {
                        const buttonImageUrl = getImageButton(item)
                        return (
                          <div
                            data-index={item.indexAnswer}
                            id={item.key}
                            key={item.key}
                            className={`block-item_button block-item_button_right ${generateClassButton(item)} ${
                              index >= Math.ceil(answer.length / 2) ? '--break-block' : ''
                            }`}
                            onClick={onClickButton.bind(null, index)}
                            style={mode.state != TEST_MODE.play ? { pointerEvents: 'none' } : {}}
                            data-imageid={item.isExam ? `image_${item.key}` : ''}
                          >
                            <button className="content-button">{item.answer}</button>
                            <img
                              className="icon-deleted"
                              height={25}
                              src="/images/icons/ic-delete-img.png"
                              alt="img-d"
                              onClick={onClickDeleted.bind(null, item.key)}
                            />
                            <img
                              style={{ visibility: 'hidden' }}
                              className="icon-matching"
                              height={10}
                              src={`/images/icons/ic-matching${item.isExam ? '-ex' : ''}.png`}
                              alt="img"
                            />
                            <img
                              height={20}
                              alt="img"
                              className="image-button"
                              src={buttonImageUrl}
                              style={buttonImageUrl ? {} : { display: 'none' }}
                            />
                          </div>
                        )
                      })}
                    </div>
                  )}
                </div>
              </div>
              <canvas id="canvas-draw-line-1"></canvas>
            </div>
          </div>
        </>
      </div>
    </GameWrapper>
  )
}

export default GameDrawLineMobile
