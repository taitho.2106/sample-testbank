import { useContext, useState, useEffect, useRef, useMemo, useCallback } from 'react'

import { isIOS } from 'utils/log'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'

export const GameDD2 = ({ data }) => {
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const instruction = data?.instruction || ''
    let answers = data?.answers || []
    let indexAnswer = 0
    const tempDragElement = useRef(null)
    const indexExample = answers.findIndex((m) => m.isExam)
    if (indexExample != -1) {
        const itemExample = answers.find((m) => m.isExam)
        answers.splice(indexExample, 1)
        answers.splice(0, 0, itemExample)
    }
    answers = answers.map((item, index) => {
        const itemAnswer = item.isExam ? item : { ...item, indexAnswer }
        if (!item.isExam) {
            indexAnswer += 1
        }
        return itemAnswer
    })

    const { mode, currentQuestion } = useContext(PageTestContext) 
    const { userAnswer, getProgressActivityValue, setProgressActivityValue } = useProgress()
    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)   
    const [touchStartTimeStamp, setTouchStartTimeStamp] = useState(0)
    const audio = useRef(null)
    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        else {
            const countDownValue = audio.current.duration - audio.current.currentTime
            if (countDownValue > 0)
                setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
            else {
                setIsPLaying(false)
                audio.current.currentTime = 0
            }
        }
    }
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])
    const defaultAnswer = getProgressActivityValue(
        answers
          .filter((item) => !item.isExam)
          .map((item, index) => ({
            index: index,
            isMatched: false,
            value: '',
        })),
    )

    const setIsShowTapescript = (isShow) => {
        const elementScript = document.getElementById('pt-o-game-dd2-script')
        const elementRight = document.querySelector('.pt-o-game-dd2__right')
        const elementLeft = document.querySelector('.pt-o-game-dd2__left')
        if (isShow) {
            elementScript.style.display = 'block'
            elementLeft.style.display = 'none'
            elementRight.style.display = 'none'
        } else {
            elementScript.style.display = 'none'
            elementLeft.style.display = 'flex'
            elementRight.style.display = 'flex'
        }
    }

    const answersGroup = useRef(defaultAnswer)
    const answersList = answers.filter((item) => !item.isExam)
    // console.log("answersGroup----", answersGroup)

    const [allKeyWord, setAllKeyWord] = useState([])
    useEffect(() => {
        let currentIndex = answersList.length
        let randomIndex
        if ([TEST_MODE.play].includes(mode.state)) {
            while(currentIndex !== 0){
                randomIndex = Math.floor(Math.random() * currentIndex)
                currentIndex--
                [answersList[currentIndex], answersList[randomIndex]] = [answersList[randomIndex], answersList[currentIndex]];
            }
            setAllKeyWord([...answersList])
        } else if(mode.state == TEST_MODE.check) {
            setAllKeyWord([...answersList])
        } else if(mode.state == TEST_MODE.review) {
            const temps = answersList.filter((m, index)=> answersGroup.current.findIndex((x, indexAnswerGroup)=> x.index== m.indexAnswer && x.isMatched) == -1)
            setAllKeyWord([...temps])
        }
    }, [data.id, mode.state])

    const buttonId = useRef('')
    const spanId = useRef('')
    const isButtonDrag = useRef(true)
    
    const dragEndMobile = (el, e) => {
        const topButtonDrag = e.clientY - tempDragElement.current.offsetHeight/2 
        const leftButtonDrag = e.clientX - tempDragElement.current.offsetWidth/2 
        
        const buttonId = tempDragElement.current.dataset["buttonid"];
        const swapId = tempDragElement.current.dataset["swapid"];
        const originButton = document.querySelector(`#${buttonId}`)
        const listDragDropArea = document.querySelectorAll(`.drag-drop-area`)
        if(swapId){
            // kéo từ vùng trong ra List bên ngoài lại
            const boxCancelMatched = document.querySelector(".keyword-list-section");
            const dropListDimension = boxCancelMatched.getBoundingClientRect()
            const { top, left, width, height } = dropListDimension
            let inListBox =
            leftButtonDrag >= left &&
            topButtonDrag >= top
            if(inListBox){                              
                const idMoveOutSpan = tempDragElement.current.id.replace('div--', 'span_')
                const moveOutKeyWordSpan = document.getElementById(idMoveOutSpan)
                const moveOutKeyWordCell = document.getElementById(idMoveOutSpan).parentNode
                const moveOutText = tempDragElement.current.innerText
                const moveOutIndex = moveOutKeyWordCell.dataset['index']
                const buttonOriginId = answersList.findIndex((m)=> m.answer === moveOutText)
                const buttonDragShow = document.getElementById(`div-${answersList[buttonOriginId].key}`) 
                if(buttonDragShow.classList.contains("--item-left-hidden")){
                    buttonDragShow.classList.remove("--item-left-hidden")
                }
                buttonDragShow.style.display = 'flex'
                moveOutKeyWordSpan.innerText  = ''
                moveOutKeyWordCell.classList.remove('dropped-text')
                
                answersGroup.current[moveOutIndex].value = ''
                answersGroup.current[moveOutIndex].isMatched = false
                setProgressActivityValue(answersGroup.current, answersGroup.current.map((item, mIndex) =>
                    { 
                        return item.index && item.isMatched && item.value 
                    })
                )
                return
            }
        }  
        for (let i = 0; i < listDragDropArea.length; i++){
            const m = listDragDropArea[i]
            const dropBoxDimension = m.getBoundingClientRect()
            const { top, left, width, height } = dropBoxDimension
            let inTopBox =
            leftButtonDrag >= left &&
            leftButtonDrag <= left + width  &&
            topButtonDrag >= top &&
            topButtonDrag <= top + height
            if(inTopBox && buttonId){
                // kéo button từ ngoài list vào
                // originButton.style.display = 'none'
                m.childNodes[1].childNodes[0].innerText = tempDragElement.current.innerText
                m.childNodes[1].classList.add('dropped-text')
                // if(originButton.classList.contains("--item-left-hidden")){
                //     originButton.classList.remove("--item-left-hidden")
                // }
                // handle answersGroup    
                const btnIndexMobile = originButton.dataset['index']
                const spanIndexMobile = m.childNodes[1].dataset['index']
                answersGroup.current[spanIndexMobile].index = parseInt(btnIndexMobile)
                answersGroup.current[spanIndexMobile].isMatched = true
                answersGroup.current[spanIndexMobile].value = tempDragElement.current.innerText
                setProgressActivityValue(answersGroup.current, answersGroup.current.map((item, mIndex) =>
                    { 
                        return mIndex == item.index && item.isMatched == true && item.value == item.value            
                    })
                )
                return;
            } else 
            if(inTopBox && swapId ){
                // kéo thả giữa các ô trong bảng            
                const currentKeyWordSpan = m.childNodes[1].childNodes[0].innerText
                const idPreviousSpan = tempDragElement.current.id.replace('div--', 'span_')
                const previousKeyWordSpan = document.getElementById(idPreviousSpan)
                const previousKeyWordCell = document.getElementById(idPreviousSpan).parentNode
                const spanIndexCurrentMobile = parseInt(m.childNodes[1].dataset['index'])
                const spanIndexPrevMobile = parseInt(previousKeyWordCell.dataset['index'])
                if(!currentKeyWordSpan){
                    // kéo thả vào 1 ô trống
                    previousKeyWordSpan.innerText = ''
                    previousKeyWordCell.classList.remove('dropped-text')
                    m.childNodes[1].childNodes[0].innerText = tempDragElement.current.innerText
                    m.childNodes[1].classList.add('dropped-text')

                    //handle answerGroup
                    const buttonOriginId = answersList.findIndex((m)=> m.answer === tempDragElement.current.innerText)
                    answersGroup.current[spanIndexCurrentMobile].index = buttonOriginId
                    answersGroup.current[spanIndexCurrentMobile].value = tempDragElement.current.innerText
                    answersGroup.current[spanIndexCurrentMobile].isMatched = true

                    answersGroup.current[spanIndexPrevMobile].value = ''
                    answersGroup.current[spanIndexPrevMobile].isMatched =  false
                    setProgressActivityValue(answersGroup.current, answersGroup.current.map((item, mIndex) =>
                        { 
                            return item.index && item.isMatched && item.value 
                        })
                    )
                    return;
                }
                else{
                    // kéo thả vào 1 ô đã có dữ liệu
                    previousKeyWordSpan.innerText = currentKeyWordSpan
                    m.childNodes[1].childNodes[0].innerText = tempDragElement.current.innerText
                    const buttonCurrentOriginId = answersList.findIndex((m)=> m.answer === tempDragElement.current.innerText)
                    const buttonPrevOriginId = answersList.findIndex((m)=> m.answer === currentKeyWordSpan)

                    //handle answerGroup 
                    answersGroup.current[spanIndexCurrentMobile].index = buttonCurrentOriginId
                    answersGroup.current[spanIndexCurrentMobile].value = tempDragElement.current.innerText
                    answersGroup.current[spanIndexCurrentMobile].isMatched = true

                    answersGroup.current[spanIndexPrevMobile].index = buttonPrevOriginId
                    answersGroup.current[spanIndexPrevMobile].value = currentKeyWordSpan
                    answersGroup.current[spanIndexPrevMobile].isMatched =  true
                    setProgressActivityValue(answersGroup.current, answersGroup.current.map((item, mIndex) =>
                        { 
                            return item.index && item.isMatched && item.value 
                        })
                    )
                    return;
                }
            }    
        }
    }

    const keyWordSection = document.querySelector('.keyword-list-section')
    const dragStart = (e, item, typeElement) => {
        if(typeElement === 'button'){
            buttonId.current = item.key
            e.dataTransfer?.setData('buttonId', item.key)
            isButtonDrag.current = true
        }
        else{
            spanId.current = item.key
            e.dataTransfer?.setData('spanId', 'span_'+item.key)
            isButtonDrag.current = false
            keyWordSection.classList.add('--selected')
        }
    }

    const dragEnter = (e, item) => {
        //
    }

    const allowDrop = (e) => e.preventDefault()

    const drop = (e, item) => {
        e.stopPropagation();
        e.preventDefault()
        const buttonId = e.dataTransfer.getData('buttonId')
        const buttonElement = document.getElementById(buttonId)
        const spanElement = document.getElementById(`span_${item.key}`)
        // console.log("spanElement----", spanElement)
        if(buttonElement && spanElement){
            // TH1: Kéo từ List vào ô drop lần đầu 
            const temp = spanElement.innerText
            if(temp.length > 0 ){
                // keo thả từ ngoài List vao 1 ô có sẵn data 
                const buttonOriginId = answersList.findIndex((m)=> m.answer === temp)
                const buttonDragShow = document.getElementById(`div-${answersList[buttonOriginId].key}`)
                // console.log("buttonOriginId----", buttonOriginId)
                // console.log("buttonDragShow----", buttonDragShow);
                buttonDragShow.style.display = 'flex'
                if(buttonDragShow.classList.contains("--item-left-hidden")){
                    buttonDragShow.classList.remove("--item-left-hidden")
                }

                spanElement.setAttribute('data-btnid', `${buttonId}`)
                const buttonElementId = answersList.findIndex((m)=> m.answer === buttonElement.innerText)
                // console.log("buttonElementId---", buttonElementId);
                spanElement.innerText = buttonElement.innerText    
                buttonElement.parentNode.parentNode.style.display = 'none'
                const buttonSwapToSpan = document.querySelector(`#div-${answersList[buttonElementId].key}`).children[0]
                // console.log("buttonSwapToSpan---", buttonSwapToSpan);
                buttonSwapToSpan.style.display = 'flex'
            }
            else {
                spanElement.setAttribute('data-btnid', `${buttonId}`)
                spanElement.innerText = buttonElement.innerText
                spanElement.parentNode.classList.add('dropped-text')
                buttonElement.parentNode.parentNode.style.display = 'none'
            }

            // handle answersGroup
            const tempID = spanElement.dataset['btnid'] 
            // console.log("tempID----", tempID)
            const btnIndex = document.querySelector(`#div-${tempID}`).dataset['index']
            const spanIndex = answersList.findIndex((m)=> m.key === item.key)
            answersGroup.current[spanIndex].index = parseInt(btnIndex)
            answersGroup.current[spanIndex].isMatched = true
            answersGroup.current[spanIndex].value = spanElement.innerText
            setProgressActivityValue(answersGroup.current, answersGroup.current.map((item, mIndex) =>
                { 
                    return mIndex == item.index && item.isMatched == true && item.value == item.value            
                })
            )
        }
        else{
            if(spanElement){
                // TH2:  kéo thả giữa 2 vị trí drop bất kỳ 
                const spanId = e.dataTransfer.getData('spanId')
                // console.log("spanId----", spanId)
                // console.log("spanElement---", spanElement);
                const spanWillDrag = document.getElementById(spanId)
                const btnIdWillDrag = spanWillDrag.dataset['btnid'] ?? ''
                const btnIdDragged = spanElement.dataset['btnid'] ?? ''
                const temp = spanElement.innerText ?? ''
                spanElement.innerText = spanWillDrag.innerText
                spanWillDrag.innerText = temp
                if(spanElement.innerText.length > 0 ){
                    spanElement.parentNode.classList.add('dropped-text')
                    spanElement.setAttribute('data-btnid', `${btnIdWillDrag}`)
                }
                else{
                    spanElement.parentNode.classList.remove('dropped-text')
                    spanElement.setAttribute('data-btnid', `${btnIdWillDrag}`)
                }
                if(spanWillDrag.innerText.length > 0 ){
                    spanWillDrag.parentNode.classList.add('dropped-text')
                    spanWillDrag.setAttribute('data-btnid', `${btnIdDragged}`)
                }
                else{
                    spanWillDrag.parentNode.classList.remove('dropped-text')
                    spanWillDrag.setAttribute('data-btnid', `${btnIdDragged}`)
                }
                // handle answersGroup
                const textSpanInit = document.getElementById(`${spanId}`).innerText
                const spanElementText = spanElement.innerText

                // tìm data-index tương ứng trong answerList 
                const buttonCurrentOriginId = answersList.findIndex((m)=> m.answer === textSpanInit)
                const buttonPrevOriginId = answersList.findIndex((m)=> m.answer === spanElementText)
                // xác đinh dataset của các keyword cell
                const parentInitBtnId = document.querySelector(`#${spanId}`).parentNode.dataset['index']
                const parentCurrentBtnId = spanElement.parentNode.dataset['index']
                if(buttonCurrentOriginId !== -1){
                    // TH kéo thả giữa 2 ô đều có giá trị
                    answersGroup.current[parentInitBtnId].index = buttonCurrentOriginId
                    answersGroup.current[parentInitBtnId].value = spanWillDrag.innerText
                    answersGroup.current[parentCurrentBtnId].index = buttonPrevOriginId
                    answersGroup.current[parentCurrentBtnId].value = spanElement.innerText
                    setProgressActivityValue(answersGroup.current, answersGroup.current.map((item, mIndex) =>
                        { 
                            return item.index && item.isMatched && item.value    
                        }))
                }
                else if (buttonCurrentOriginId === -1){
                    // TH di chuyển tới 1 ô thả trống
                    answersGroup.current[parentInitBtnId].isMatched = false
                    answersGroup.current[parentInitBtnId].value = ''
                    answersGroup.current[parentCurrentBtnId].index = buttonPrevOriginId
                    answersGroup.current[parentCurrentBtnId].isMatched = true
                    answersGroup.current[parentCurrentBtnId].value = spanElement.innerText
                    setProgressActivityValue(answersGroup.current, answersGroup.current.map((item, mIndex) =>
                        { 
                            return item.index && item.isMatched && item.value           
                        }))
                }
            }
            else{
                // TH3: di chuyển button về vị trí List ban đầu
                const spanElement = document.getElementById('span_'+spanId.current)
                if(spanElement){
                    const spanElementText = spanElement.innerText
                    const buttonOriginId = answersList.findIndex((m)=> m.answer === spanElementText)
                    const buttonDragShow = document.getElementById(`div-${answersList[buttonOriginId].key}`) 
                    // console.log("buttonDragShow---", buttonDragShow)
                    buttonDragShow.style.display = 'flex'
                    if(buttonDragShow.classList.contains("--item-left-hidden")){
                        buttonDragShow.classList.remove("--item-left-hidden")
                    }
                    spanElement.innerText = ''
                    spanElement.parentNode.classList.remove('dropped-text')
                    //handle answersGroup
                    const spanEmptyIndex = parseInt(spanElement.parentNode.dataset['index'])
                    answersGroup.current[spanEmptyIndex].index = spanEmptyIndex
                    answersGroup.current[spanEmptyIndex].isMatched = false
                    answersGroup.current[spanEmptyIndex].value = ''
                    setProgressActivityValue(answersGroup.current, answersGroup.current.map((item, mIndex) =>
                        { 
                            return item.index && item.isMatched == false && item.value              
                        })
                    )
                }
            }
        }
    }

    useEffect(() => {

        const listKeyWordInputCell = document.querySelectorAll('.keyword-input-cell')
        const listSelectImage = document.querySelectorAll('.image-keyword-item')
        const iconCheckTrue = document.querySelectorAll('.icon-checked-true')
        const iconCheckFalse = document.querySelectorAll('.icon-checked-false')
        switch (mode.state) {
            case TEST_MODE.review:
                for(let i = 0; i < answersGroup.current.length; i++){
                    if(answersGroup.current[i] !== ''){
                        listKeyWordInputCell[i].childNodes[0].innerText = answersGroup.current[i].value
                    }
                    if(
                        listKeyWordInputCell[i].classList.contains('--danger') ||
                        listSelectImage[i].classList.contains('--danger') ||
                        listKeyWordInputCell[i].classList.contains('--success') ||
                        listSelectImage[i].classList.contains('--success') ||
                        listKeyWordInputCell[i].classList.contains('--empty') ||
                        listSelectImage[i].classList.contains('--empty') 
                    ){
                        listKeyWordInputCell[i].classList.remove('--danger')
                        listSelectImage[i].classList.remove('--danger')
                        listKeyWordInputCell[i].classList.remove('--success')
                        listSelectImage[i].classList.remove('--success')
                        listKeyWordInputCell[i].classList.remove('--empty')
                        listSelectImage[i].classList.remove('--empty')
                    }
                    // check and display success answer
                    if(i === answersGroup.current[i].index && answersGroup.current[i].isMatched === true){
                        listKeyWordInputCell[i].classList.add('--success')
                        listSelectImage[i].classList.add('--success')
                        listKeyWordInputCell[i].classList.add('dropped-text')
                        iconCheckFalse[i].style.display = 'none'
                    }
                    // check and display success answer
                    if(i !== answersGroup.current[i].index && answersGroup.current[i].isMatched === true){
                        listKeyWordInputCell[i].classList.add('--danger')
                        listSelectImage[i].classList.add('--danger')
                        listKeyWordInputCell[i].classList.add('dropped-text')
                        iconCheckTrue[i].style.display = 'none'
                    }
                    if(answersGroup.current[i].isMatched === false){
                        listKeyWordInputCell[i].classList.add('--empty')
                        listSelectImage[i].classList.add('--empty')
                        iconCheckTrue[i].style.display = 'none'
                        iconCheckFalse[i].style.display = 'none'
                    }
                }
                
            break;
            case TEST_MODE.check:
                for(let i = 0; i < answersGroup.current.length; i++){
                    if(
                        listKeyWordInputCell[i].classList.contains('--danger') ||
                        listSelectImage[i].classList.contains('--danger')
                    ){
                        listKeyWordInputCell[i].classList.remove('--danger')
                        listSelectImage[i].classList.remove('--danger')
                    }
                    if(
                        listKeyWordInputCell[i].classList.contains('--empty') ||
                        listSelectImage[i].classList.contains('--empty')
                    ){
                        listKeyWordInputCell[i].classList.remove('--empty')
                        listSelectImage[i].classList.remove('--empty')
                    }
                    if(
                        listKeyWordInputCell[i].classList.contains('--success')||
                        listSelectImage[i].classList.contains('--success')
                    ){
                        listKeyWordInputCell[i].classList.add('--success')
                        listSelectImage[i].classList.add('--success')
                        iconCheckTrue[i].style.display = 'block'
                    }
                }
            break;
        }
    }, [mode.state, data.id])
    
    // Used to detect whether the users browser is an mobile browser
    const isMobile = useMemo(() =>{
        if (sessionStorage.desktop)
        // desktop storage
        return false
        else if (localStorage.mobile)
        // mobile storage
        return true

        // alternative
        const mobile = [
        'iphone',
        'ipad',
        'android',
        'blackberry',
        'nokia',
        'opera mini',
        'windows mobile',
        'windows phone',
        'iemobile',
        'tablet',
        'mobi',
        ]
        let ua = navigator.userAgent.toLowerCase()
        for (let i in mobile) if (ua.indexOf(mobile[i]) > -1) return true

        // nothing found.. assume desktop
        const ios = isIOS()
    
        return ios
    }, [])

    const enableScrollView = useCallback((mode)=>{
        const contentLeft = document.querySelector(".pt-o-game-dd2__right .drag-drop-main-section")
        const contentRight = document.querySelector(".pt-o-game-dd2__right .keyword-list-section")
        if(mode){
            contentLeft && contentLeft.classList.remove("disable-scroll-ds1")
            contentRight && contentRight.classList.remove("disable-scroll-ds1")
            document.body.classList.remove("disable-scroll-ds1")
        }else{
            contentLeft && contentLeft.classList.add("disable-scroll-ds1")
            contentRight && contentRight.classList.add("disable-scroll-ds1")
            document.body.classList.add("disable-scroll-ds1")
        }
    }, [])

    useEffect(() => {
        return ()=> {
            enableScrollView(true)
        }
    }, [])

    const isIosDevice = useMemo(() =>{
        return isIOS()
    }, [])
    const [isZoom, setIsZoom] = useState(false)
    const [imageStr, setImageStr] = useState('')
    return (
        <GameWrapper className="pt-o-game-dd2">
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>
            <div className='pt-o-game-dd2__container'>
                <div className="__paper" id="pt-o-game-dd2-script" style={{ display: 'none' }}>
                    <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
                        <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                    </div>
                    <BlockPaper>
                        <div className="__content">
                            <FormatText>{audioScript}</FormatText>
                        </div>
                    </BlockPaper>
                </div>
                <BlockWave className={`pt-o-game-dd2__left ${mode.state !== TEST_MODE.play ? 'pt-o-game-dd2__result' : ''}`}>
                    <AudioPlayer
                        className="__player"
                        isPlaying={isPlaying}
                        setIsPlaying={() => setIsPLaying(!isPlaying)}
                    />
                    <span className="__duration">{countDown}</span>
                </BlockWave>
                <div className={`pt-o-game-dd2__right`}>
                    {mode.state !== TEST_MODE.play && (
                        <CustomButton
                            className="__tapescript"
                            onClick={() => setIsShowTapescript(true)}
                        >
                        Tapescript
                        </CustomButton>
                    )}
                    <div className={`__header`} id="span_text">
                        <FormatText tag="p">{instruction}</FormatText>
                    </div>
                    <ImageZooming
                        data={{ alt: '', src: `${imageStr}` }}
                        status={{
                            state: isZoom,
                            setState: setIsZoom,
                        }}
                    />
                    <div className="__content">
                        <div className={`drag-drop-main-section`}>
                            {answers.map((item, caIndex) => {
                            const isMatched = answersGroup.current[item.indexAnswer] && answersGroup.current[item.indexAnswer].isMatched                                
                                return (
                                    <div
                                        key={item.key + '_' + caIndex}
                                        className={item.isExam ? `example-area` : `drag-drop-area ${indexExample === 0 ? caIndex - 1 : caIndex}`}
                                        onDrop={(e) => {
                                            if (!isMobile){
                                                if (mode.state === TEST_MODE.play && !item.isExam) {
                                                    e.stopPropagation()
                                                    drop(e, item)
                                                } 
                                            }
                                        }}
                                        onDragOver={(e) => {
                                            if (!isMobile){
                                                mode.state === TEST_MODE.play && !item.isExam &&
                                                allowDrop(e)
                                            }
                                        }}
                                        onDragEnter={(e) => {
                                            if (!isMobile){
                                                if (mode.state === TEST_MODE.play && !item.isExam) {
                                                    dragEnter(e, item, 'span')
                                                }}
                                            }
                                        }
                                    >
                                        <div 
                                            className={!item.isExam ? `image-keyword-item ${indexExample === 0 ? caIndex - 1 : caIndex}` : `image-example-item`}
                                            draggable={false}
                                        >
                                            <img 
                                                alt=''
                                                src={item.image}
                                                id={`image_${item.key}`}
                                                draggable={false}
                                                onClick={()=>{
                                                    setIsZoom(true)
                                                    setImageStr(item.image)
                                                }}
                                            />
                                            {(
                                                mode.state === TEST_MODE.check || 
                                                mode.state === TEST_MODE.review) &&
                                                !item.isExam && (
                                                <div className={`icon-checked-true`}>
                                                    <img 
                                                        src={'/images/icons/ic-dropped-true.png'}
                                                        alt=''
                                                    />
                                                </div>
                                            )}
                                            {mode.state === TEST_MODE.review &&
                                                !item.isExam && (
                                                <div className={`icon-checked-false`}>
                                                    <img 
                                                        src={'/images/icons/ic-dropped-false.png'}
                                                        alt=''
                                                    />
                                                </div>
                                            )}
                                        </div>
                                        {item.isExam && (
                                        <div className={`example-input-cell`}>
                                            <span>{item.answer.replace('*','')}</span>
                                        </div>
                                        )}
                                        {!item.isExam && 
                                        [TEST_MODE.review, TEST_MODE.play].includes(mode.state) && (
                                        <div 
                                            className={`keyword-input-cell ${isMatched ? "dropped-text" : "" }`}
                                            draggable={!isMobile &&isMatched ? 'true': 'false'}
                                            data-index={indexExample === 0 ? caIndex - 1 : caIndex}
                                            style={{
                                                pointerEvents:
                                                mode.state === TEST_MODE.play
                                                    ? 'all'
                                                    : 'none',                                                    
                                            }} 
                                            onDragStart={(e) => {                                                
                                                mode.state === TEST_MODE.play &&
                                                dragStart(e, item, 'span')                                                
                                            }}
                                            onDragEnd={(e) => {
                                                if(keyWordSection.classList.contains('--selected')){
                                                    keyWordSection.classList.remove('--selected')
                                                }
                                            }}
                                            onDragEnter={(e) => {
                                                e.stopPropagation();
                                                e.preventDefault()
                                            }}

                                            onTouchMove={(el) => {
                                                const touch = el.changedTouches[0]                                                
                                                if(isMatched){
                                                    tempDragElement.current.style.top = touch?.clientY - tempDragElement.current.offsetHeight/2 +"px"      
                                                    tempDragElement.current.style.left = touch?.clientX - tempDragElement.current.offsetWidth/2 +"px"                                                                                                           
                                                    // enableScrollView(false)
                                                }
                                            }}
                                            onTouchStart={(el) => {
                                                setTouchStartTimeStamp(el.timeStamp)
                                                // if(touchStartTimeStamp  < 5000){}
                                                const touch = el.changedTouches[0]
                                                if(isMatched){
                                                    const tempELement2  = document.createElement("div");
                                                    tempELement2.style.width = `${el.currentTarget.offsetWidth}px`
                                                    tempELement2.style.height = `${el.currentTarget.offsetHeight}px`
                                                    tempELement2.style.position = "fixed"
                                                    tempELement2.style.top = touch?.clientY +"px"      
                                                    tempELement2.style.left = touch?.clientX +"px"        
                                                    tempELement2.style.pointerEvents = "all"
                                                    tempELement2.dataset["swapid"] = 'div--'+item.key
                                                    tempELement2.id = 'div--'+item.key
                                                    tempELement2.classList.add("temp-drag-item")
                                                    tempELement2.innerHTML = el.currentTarget.childNodes[0].innerHTML;
                                                    const templateElement2 = document.querySelector(".pt-o-game-dd2");
                                                    templateElement2.append(tempELement2)
                                                    tempDragElement.current = tempELement2;
                                                    keyWordSection.classList.add('--selected')                                                    
                                                    enableScrollView(false)
                                                }
                                            }}
                                            onTouchEnd={(el) => {
                                                const touch = el.changedTouches[0]
                                                const e = {
                                                  clientX: touch?.clientX,
                                                  clientY: touch?.clientY,
                                                }
                                                if(isMatched  ){
                                                    tempDragElement.current & tempDragElement.current.remove()
                                                    mode.state === TEST_MODE.play && dragEndMobile(el, e)
                                                    enableScrollView(true)
                                                }
                                                if(keyWordSection.classList.contains('--selected')){
                                                    keyWordSection.classList.remove('--selected')
                                                }
                                            }}
                                            onTouchCancel = {(el) => {
                                                enableScrollView(true)
                                                tempDragElement.current & tempDragElement.current.remove()
                                            }}                
                                        >
                                            <span 
                                                className={''}                                                
                                                id={`span_${item.key}`}
                                                style={{
                                                    pointerEvents:
                                                    mode.state === TEST_MODE.play
                                                        ? 'all'
                                                        : 'none',                                                    
                                                }}                                                
                                            >
                                                {isMatched ? answersGroup.current[item.indexAnswer].value: ''}
                                            </span>
                                        </div>
                                        )}
                                        {!item.isExam && 
                                        mode.state === TEST_MODE.check && (
                                        <div 
                                            className={`keyword-input-cell dropped-text --success`}
                                            data-index={!item.isExam ? caIndex - 1 : ''}
                                        >
                                            <span className={''} id={`span_${item.key}`}>
                                                {item.answer}
                                            </span>
                                        </div>
                                        )}
                                    </div>
                                )
                            })}
                        </div>
                        <div 
                            className={`keyword-list-section`}
                            draggable="false"
                            onDrop={(e) => {
                                if (mode.state === TEST_MODE.play) {
                                    e.stopPropagation()
                                    drop(e, true)
                                } 
                            }}
                            onDragOver={(e) => {
                                mode.state === TEST_MODE.play && allowDrop(e)
                            }}
                        >
                        {[TEST_MODE.review, TEST_MODE.play].includes(mode.state) &&
                            allKeyWord.map((item, kIndex) => {
                                const isMatched = answersGroup.current.findIndex(m=> m.isMatched && m.index == item.indexAnswer) !=-1
                                return (
                                <div
                                    id={'div-'+item.key}
                                    key={kIndex}
                                    className={`__drag-item ${isMatched ? "--item-left-hidden" : "" }`}
                                    data-index={item.indexAnswer}
                                    draggable="false"                            
                                >
                                    <div
                                        className={`__drag-content`}
                                        style={{
                                            pointerEvents:
                                            mode.state === TEST_MODE.play
                                                ? 'all'
                                                : 'none',
                                        }}
                                        draggable={!isMobile ? 'true' :  'false'}
                                        onDragStart={(e) => {
                                            mode.state === TEST_MODE.play && !isMobile
                                            dragStart(e, item, 'button')
                                        }}
                                        onDragEnd={(e) => {
                                            if(keyWordSection.classList.contains('--selected')){
                                                keyWordSection.classList.remove('--selected')
                                            }
                                        }}
                                        onTouchMove={(el) => {
                                            const touch = el.changedTouches[0]
                                            const _top = !isIosDevice ? (touch?.pageY - tempDragElement.current.offsetHeight/2) : touch?.clientY - tempDragElement.current.offsetHeight/2
                                            const _left = touch?.clientX - tempDragElement.current.offsetWidth/2
                                            tempDragElement.current.style.top = _top +"px"      
                                            tempDragElement.current.style.left = _left +"px"     
                                            // enableScrollView(false)
                                        }}
                                        onTouchStart={(el) => {
                                            setTouchStartTimeStamp(el.timeStamp)
                                            const touch = el.changedTouches[0]
                                            const tempELement  = document.createElement("div");
                                            tempELement.style.width = `${el.currentTarget.offsetWidth}px`
                                            tempELement.style.height = `${el.currentTarget.offsetHeight}px`
                                            tempELement.style.position = "fixed"

                                            const _top = isIosDevice ? touch?.clientY : (touch?.pageY - el.currentTarget.offsetHeight/2)
                                            const _left = isIosDevice ? touch?.clientX : (touch?.pageX - el.currentTarget.offsetWidth/2)

                                            tempELement.style.top = _top +"px"  
                                            tempELement.style.left = _left +"px"        
                                            tempELement.style.pointerEvents = "none"
                                            tempELement.style.zIndex = "199"
                                            tempELement.dataset["buttonid"] = 'div-'+item.key
                                            tempELement.id = 'div-'+item.key
                                            tempELement.classList.add("temp-drag-item")
                                            tempELement.innerHTML = el.currentTarget.innerHTML;
                                            const templateElement = document.querySelector(".pt-o-game-dd2");
                                            templateElement.append(tempELement)
                                            tempDragElement.current = tempELement;
                                            enableScrollView(false)
                                        }}
                                        onTouchEnd={(el) => {
                                            setTouchStartTimeStamp(el.timeStamp)
                                            const detectTime =
                                                Math.floor(el.timeStamp) -
                                                Math.floor(touchStartTimeStamp)
                                            if(detectTime > 300){
                                                enableScrollView(true)
                                                const touch = el.changedTouches[0]
                                                const e = {
                                                clientX: touch?.clientX,
                                                clientY: touch?.clientY,
                                                }
                                                tempDragElement.current & tempDragElement.current.remove()
                                                mode.state === TEST_MODE.play && dragEndMobile(el, e)
                                                if(keyWordSection.classList.contains('--selected')){
                                                    keyWordSection.classList.remove('--selected')
                                                }
                                            }
                                            enableScrollView(true)                                        
                                            tempDragElement.current & tempDragElement.current.remove()
                                        }}
                                        onTouchCancel = {(el) => {    
                                            enableScrollView(true)                                        
                                            tempDragElement.current & tempDragElement.current.remove()
                                        }}
                                    >
                                        <span id={item.key} style={{userSelect:"none"}}>
                                            {item.answer}
                                        </span>
                                    </div>    
                                </div>   
                               )                         
                            }
                            )}                            
                        </div>
                    </div>
                </div>
            </div>
        </GameWrapper>
    )
}