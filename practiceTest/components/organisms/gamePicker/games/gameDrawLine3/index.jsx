import { useRef, useState, useContext, useEffect } from 'react'

import { useWindowSize } from 'utils/hook'
import { isIOS } from 'utils/log'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import GameDrawLine3Mobile from './gameDrawLine3Mobile'
function GameDrawLine3({ data }) {
  const [width, height] = useWindowSize()
  const renderUIGameDrawLine = (data) => {
    if (width <= 1024 && window.matchMedia('(orientation: portrait)').matches) {
      return <GameDrawLine3Mobile key={`m_${data.id}`} data={data} />
    }
    return <GameDrawLineWeb key={`pc_${data.id}`} data={data} />
  }
  return <>{renderUIGameDrawLine(data)}</>
}

export default GameDrawLine3

function GameDrawLineWeb({ data }) {
  const isAppleDevice = useRef(isIOS() || navigator.platform.toUpperCase().indexOf('MAC') >= 0)
  const imageWidthOriginRef = useRef(0)
  const answer = data?.answers ?? []
  const ansLeft = []
  const ansRight = []
  let indexAnswer = 0
  const indexExample = answer.findIndex((m) => m.isExam)
  if (indexExample != -1) {
    const itemExample = answer.find((m) => m.isExam)
    answer.splice(indexExample, 1)
    answer.splice(0, 0, itemExample)
  }
  data.answers = data?.answers.map((item, index) => {
    const itemAnswer = item.isExam ? item : { ...item, indexAnswer }
    if (!item.isExam) {
      indexAnswer += 1
    }
    return itemAnswer
  })
  answer.map((item, index) => {
    if (index % 2 === 0) {
      ansLeft.push(item)
    } else {
      ansRight.push(item)
    }
  })

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } = useProgress()
  const instruction = data?.instruction || ''

  const [scaleNumber, setScaleNumber] = useState(0)
  const scaleNumberRef = useState(0)
  const [offsetImage, setOffsetImage] = useState({ top: 0, left: 0 })

  const { mode, currentQuestion } = useContext(PageTestContext)
  const idButton = useRef('')
  const idImage = useRef('')
  const intervalRef = useRef(null)
  const defaultAnswer = getProgressActivityValue(
    answer
      .filter((item) => !item.isExam)
      .map((item, index) => ({
        index: index,
        isMatched: false,
      })),
  )
  const answerGroup = useRef(defaultAnswer)
  //change question
  useEffect(() => {
    if (userAnswer.value && userAnswer.value.length > 0) {
      answerGroup.current = userAnswer.value
    }
  }, [currentQuestion])
  useEffect(() => {
    return () => {
      clearCanvas()
      setScaleNumber(0)
    }
  }, [])
  useEffect(() => {
    //fix ios apply more class has style drop-shadow
    if(isAppleDevice.current){
      const listImage = document.querySelectorAll("#pt-o-game-draw-line-3-content-id .--matching-div .item-image-content")
      listImage.forEach(el=>{
        const listClass = [... el.classList];
        while(el.classList.length>0){
          el.classList.remove(el.classList[0])
        }
        setTimeout(() => {
          el.classList.add(...listClass)
        }, 0);
      })
    }
  }, [mode.state])
  const [width, height] = useWindowSize()
  useEffect(() => {
    if (width == 0 || height == 0) return
  }, [width, height])
  useEffect(() => {
    intervalRef.current = setInterval(() => {
      const imageInstruction = document.getElementById('dl3-image-instruction-id')
      if(!imageInstruction || !imageInstruction.complete || imageInstruction.naturalWidth == 0) return;

      const ratio = imageInstruction.width / imageInstruction.naturalWidth
      if(imageInstruction.height == 0 
        || imageInstruction.width ==0 
        || imageInstruction.parentElement.offsetHeight ==0 
        || imageInstruction.parentElement.offsetWidth ==0) return;
      if(imageInstruction.height > imageInstruction.parentElement.offsetHeight){
        imageInstruction.style.height = imageInstruction.parentElement.offsetHeight + "px"
        
      }
      if(ratio != scaleNumberRef.current){
        const listImage = document.querySelectorAll("#pt-o-game-draw-line-3-content-id .item-image-content")
        listImage.forEach(m=>{
          if(m.complete){
            drawPointMatching(m,ratio)
          }
        })
        clearCanvas()
          const canvas = document.getElementById('canvas-draw-line-3')
          const content = document.getElementById('pt-o-game-draw-line-3-content-id')
          canvas.width = content.offsetWidth
          canvas.height = content.offsetHeight
          setScaleNumber(ratio)
          scaleNumberRef.current = ratio
      }
    }, 200);
    return () => {
      if (intervalRef.current) {
        clearInterval(intervalRef.current)
      }
    }
  }, [])
  const onClickButton = (index, position, e) => {
    e.preventDefault()
    idButton.current = e.currentTarget.id
    const element = document.getElementById(e.currentTarget.id)
    const iconButtonMatching = element.querySelector('.icon-matching')
    // if(element.childNodes.length > 0){
    //   element.childNodes[1].style.visibility = 'visible'
    // }
    const listButtonElement = document.querySelectorAll('.pt-o-game-draw-line-3 .block-item_button')
    iconButtonMatching.style.visibility = 'visible'
    listButtonElement.forEach((ele) => {
      if (ele?.classList.contains('selected') && ele.id != idButton.current) {
        ele.classList.remove('selected')
      }
    })
    if (element?.classList.contains('matched') || element?.classList.contains('matching')) {
      const imageMapingId = element.dataset['imageid']
      clearCanvas()
      doMatching(idButton.current, imageMapingId, true)
      return
    }
    element.classList.add('selected')

    clearCanvas()
    //hidden icon matching button and remove matching
    listButtonElement.forEach((ele) => {
      if (ele.id != idButton.current) {
        ele.classList.remove('matching')
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'hidden'
      }
      if (ele?.classList.contains('matching')) {
        ele.classList.remove('matching')
      }
    })

    const divImageAnswer = document.querySelectorAll('.pt-o-game-draw-line-3 .item-image')
    divImageAnswer.forEach((ele) => {
      const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
      iconMatchings.forEach((icon) => {
        if (icon?.classList.contains(`--${position}`) && !ele.dataset['buttonid']) {
          icon.style.visibility = 'visible'
        } else {
          icon.style.visibility = 'hidden'
        }
      })
      const imageElement = ele.querySelector('.item-image-content')

      if (imageElement && imageElement?.classList.contains('--image-selected')) {
        imageElement.classList.remove('--image-selected')
        setTimeout(() => {
          imageElement?.classList.add('--image-default')
        }, 0)
      }
    })
    //matching
    if (idButton.current && idImage.current) {
      setTimeout(() => {
        doMatching(idButton.current, idImage.current, true)
      }, 0)
    }
  }

  const onClickDeleted = (buttonId, position) => {
    const elementButton = document.getElementById(buttonId)

    const divImageMatchingId = elementButton.dataset['imageid']
    if (!divImageMatchingId) return

    //hidden icon matching image
    const divImageList = document.querySelectorAll('.pt-o-game-draw-line-3 .item-image')
    divImageList.forEach((ele) => {
      if (!ele.dataset['buttonid']) {
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => {
          if (icon?.classList.contains(`--${position}`)) {
            icon.style.visibility = 'visible'
          } else {
            icon.style.visibility = 'hidden'
          }
        })
      }
    })

    const imageResult = elementButton.querySelector('.image-button')
    imageResult.style.display = 'none'
    imageResult.src = ''

    const divImageElement = document.getElementById(divImageMatchingId)
    const imageElement = divImageElement.querySelector('.item-image-content')

    //remove matched
    elementButton.dataset['imageid'] = ''
    divImageElement.dataset['buttonid'] = ''

    //update answer progress
    const indexImage = divImageElement.dataset['index']
    const indexButton = elementButton.dataset['index']
    if (indexImage && indexButton) {
      if (mode.state == TEST_MODE.play && answerGroup.current[parseInt(indexImage)]) {
        answerGroup.current[parseInt(indexImage)].index = parseInt(indexImage)
        answerGroup.current[parseInt(indexImage)].isMatched = false
        setProgressActivityValue(
          answerGroup.current,
          answerGroup.current.map((item, i) => {
            return item.isMatched && i == item.index
          }),
        )
      }
    }

    if (elementButton?.classList.contains('matched')) {
      elementButton.classList.remove('matched')
    }
    if (elementButton?.classList.contains('matching')) {
      elementButton.classList.remove('matching')
    }
    if (!elementButton?.classList.contains('selected')) {
      elementButton.classList.add('selected')
    }
    idButton.current = buttonId
    idImage.current = ''

    if (imageElement?.classList.contains('--image-matched')) {
      imageElement.classList.remove('--image-matched')
    }
    if (imageElement?.classList.contains('--image-selected')) {
      imageElement.classList.remove('--image-selected')
    }
    setTimeout(() => {
      imageElement?.classList.add('--image-default')
    }, 0)
    clearCanvas()
  }

  const onClickImage = (index, value, e) => {
    e.preventDefault()
    e.stopPropagation()

    idImage.current = e.currentTarget.id
    //handler
    const element = e.currentTarget
    const imageElement = element.querySelector('.item-image-content')
    clearCanvas()
    const divImageAnswer = document.querySelectorAll('.pt-o-game-draw-line-3 .item-image')
    divImageAnswer.forEach((ele, i) => {
      const imageEle = ele.querySelector('.item-image-content')
      if (ele.id != idImage.current) {
        removeClassImageStatus(imageEle, '')
        setTimeout(() => {
          imageEle?.classList.add('--image-default')
        }, 0)
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => (icon.style.visibility = 'hidden'))
      } else {
        removeClassImageStatus(imageEle, '--image-matched')
        setTimeout(() => {
          imageElement?.classList.add('--image-selected')
        }, 0)
        const iconMatchings = ele.querySelectorAll(`.icon-matching-image`)
        iconMatchings.forEach((icon) => (icon.style.visibility = 'visible'))
      }
    })
    if (imageElement?.classList.contains('--image-matched')) {
      const buttonMatchingId = element.dataset['buttonid']
      doMatching(buttonMatchingId, idImage.current, true)
      return
    }
    const listButtonElement = document.querySelectorAll('.block-item__list .block-item_button')
    //hidden icon matching button.

    listButtonElement.forEach((ele) => {
      ele.classList.remove('matching')
      if (!ele.dataset['imageid']) {
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'visible'
        if (!ele?.classList.contains('selected')) {
          ele.classList.add('selected')
        }
      } else {
        ele.classList.remove('selected')
        const iconMatching = ele.querySelector('.icon-matching')
        iconMatching.style.visibility = 'hidden'
      }
    })
    if (idButton.current && idImage.current) {
      setTimeout(() => {
        doMatching(idButton.current, idImage.current, true)
      }, 0)
    }
  }

  const clearCanvas = () => {
    const canvas = document.getElementById('canvas-draw-line-3')
    if (!canvas) return
    const ctx = canvas.getContext('2d')
    ctx.clearRect(0, 0, canvas.width, canvas.height)
    const content = document.getElementById('pt-o-game-draw-line-3-content-id')
    canvas.width = content.offsetWidth
    canvas.height = content.offsetHeight
  }
  const removeClassImageStatus = (element, ignoreClass) => {
    if (!element) return
    const classStatus = ['--image-selected', '--success', '--danger', '--image-default']
    classStatus.forEach((className) => {
      if (className != ignoreClass) {
        element.classList.remove(className)
      }
    })
  }
  const doMatching = (idBtn, idDivImg, isShowMatching = false) => {
    try {
      if (idBtn && idDivImg) {
        const buttonMatching = document.getElementById(idBtn)
        const divImageMatching = document.getElementById(idDivImg)
        const buttonImage = buttonMatching.querySelector('.image-button')
        if (!buttonMatching || !divImageMatching) return
        const imageMatching = divImageMatching.querySelector('.item-image-content')

        //update answer progress
        const indexImage = divImageMatching.dataset['index']
        const indexButton = buttonMatching.dataset['index']
        if (indexImage && indexButton) {
          if (mode.state == TEST_MODE.play && answerGroup.current[parseInt(indexImage)]) {
            answerGroup.current[parseInt(indexImage)].index = parseInt(indexButton)
            answerGroup.current[parseInt(indexImage)].isMatched = true
            setProgressActivityValue(
              answerGroup.current,
              answerGroup.current.map((item, i) => {
                return item.isMatched && i == item.index
              }),
            )
          }
        }
        // mode state
        if (mode.state != TEST_MODE.play) {
          if (!buttonMatching.classList.contains('--example')) {
            if (indexButton != indexImage) {
              if (buttonMatching?.classList.contains('--success')) {
                buttonMatching.classList.remove('--success')
              }
              if (!buttonMatching?.classList.contains('--danger')) {
                buttonMatching.classList.add('--danger')
              }

              if (divImageMatching?.classList.contains('--success')) {
                divImageMatching.classList.remove('--success')
              }
              setTimeout(() => {
                divImageMatching.classList.remove('--default')
                setTimeout(() => {
                  divImageMatching.classList.add('--danger')
                }, 0)
              }, 0)
            } else {
              if (buttonMatching?.classList.contains('--danger')) {
                buttonMatching.classList.remove('--danger')
              }
              if (!buttonMatching?.classList.contains('--success')) {
                buttonMatching.classList.add('--success')
              }
              if (divImageMatching?.classList.contains('--danger')) {
                divImageMatching.classList.remove('--danger')
              }
              setTimeout(() => {
                divImageMatching.classList.remove('--default')
                setTimeout(() => {
                  divImageMatching.classList.add('--success')
                }, 0)
              }, 0)
            }
          }
        } else {
          const listButton = document.querySelectorAll('.pt-o-game-draw-line-3 .block-item_button')
          listButton.forEach((ele) => {
            if (ele?.classList.contains('selected')) {
              ele.classList.remove('selected')
            }
            if (ele?.classList.contains('matching')) {
              ele.classList.remove('matching')
            }
          })

          const listDivImageSelected = document.querySelectorAll('.pt-o-game-draw-line-3 .item-image')
          listDivImageSelected.forEach((ele) => {
            const eleImage = ele.childNodes[0]

            if (eleImage) {
              if (ele.id != idDivImg) {
                setTimeout(() => {
                  eleImage.classList.add('--image-default')
                }, 0)
                if (eleImage?.classList.contains('--image-selected')) {
                  eleImage.classList.remove('--image-selected')
                }
              }
            }
          })
        }
        if (mode.state != TEST_MODE.check) {
          buttonMatching.dataset['imageid'] = idDivImg
          divImageMatching.dataset['buttonid'] = idBtn
        }
        if (imageMatching) {
          buttonImage.src = imageMatching.src
          buttonImage.style.display = 'block'
        }

        // if(buttonMatching?.classList.contains("matched") && !buttonMatching?.classList.contains("--example")){
        //   buttonMatching.classList.remove("matched")
        // }

        if (!buttonMatching?.classList.contains('matched')) {
          buttonMatching.classList.add('matched')
        }
        const listIconImageMatching = document.querySelectorAll('.pt-o-game-draw-line-3 .icon-matching-image')
        listIconImageMatching.forEach((ele) => (ele.style.visibility = 'hidden'))
        const listIconButtonMatching = document.querySelectorAll(
          '.pt-o-game-draw-line-3 .block-item_button .icon-matching',
        )
        listIconButtonMatching.forEach((ele) => (ele.style.visibility = 'hidden'))
        divImageMatching.childNodes[0]?.classList.add('--image-matched')
        if (isShowMatching) {
          //show matching
          // buttonMatching.scrollIntoView();
          buttonMatching.classList.add('selected')
          buttonMatching.classList.add('matching')
          divImageMatching.childNodes[0].classList.remove('--image-default')
          setTimeout(() => {
            divImageMatching.childNodes[0].classList.add('--image-selected')
          }, 0)
          const positionButton = buttonMatching.dataset['position']
          const canvas = document.getElementById('canvas-draw-line-3')
          const ctx = canvas.getContext('2d')
          const rectCanvas = canvas.getBoundingClientRect()

          if (positionButton === 'left' && buttonMatching?.childNodes.length > 0) {
            // console.log("lefttttttt");
            const iconMatchingBtn = buttonMatching.querySelector('.icon-matching')
            const rectIconBtn = iconMatchingBtn.getBoundingClientRect()
            const xBtn = rectIconBtn.x - rectCanvas.x + 7
            const yBtn = rectIconBtn.top - rectCanvas.top + 7
            const iconImageMatching = divImageMatching.querySelector('.--left')
            // console.log("iconImageMatching====",iconImageMatching)
            const rectImg = iconImageMatching.getBoundingClientRect()
            const xImg = rectImg.x - rectCanvas.x
            const yImg = rectImg.top - rectCanvas.top + 6
            //draw
            ctx.beginPath()
            ctx.moveTo(xBtn, yBtn)
            ctx.lineWidth = 2
            ctx.lineCap = 'round'
            ctx.strokeStyle = buttonMatching?.classList.contains('--example') ? '#67ab8d' : '#2692ff'
            ctx.lineTo(xImg, yImg)
            ctx.stroke()

            idButton.current = ''
            idImage.current = ''
            divImageMatching.childNodes[0].classList.add('--image-matched')
            iconImageMatching.style.visibility = 'visible'
            iconMatchingBtn.style.visibility = 'visible'
            buttonMatching.classList.replace('selected', 'matching')
          } else if (positionButton === 'right' && buttonMatching?.childNodes.length > 0) {
            const iconMatchingBtn = buttonMatching.querySelector('.icon-matching')
            const iconImageMatching = divImageMatching.querySelector('.--right')
            const rectIconBtn = iconMatchingBtn.getBoundingClientRect()
            const xBtn = rectIconBtn.x - rectCanvas.x + 7
            const yBtn = rectIconBtn.top - rectCanvas.top + 7
            const rectImg = iconImageMatching.getBoundingClientRect()
            const xImg = rectImg.x - rectCanvas.x + 6
            const yImg = rectImg.top - rectCanvas.top + 6

            //draw
            ctx.beginPath()
            ctx.moveTo(xBtn, yBtn)
            ctx.lineWidth = 2
            ctx.lineCap = 'round'
            ctx.strokeStyle = buttonMatching?.classList.contains('--example') ? '#67ab8d' : '#2692ff'
            ctx.lineTo(xImg, yImg)
            ctx.stroke()

            idButton.current = ''
            idImage.current = ''

            iconImageMatching.style.visibility = 'visible'
            iconMatchingBtn.style.visibility = 'visible'
          }
        }
      }
    } catch (error) {
      console.error('doMatching', error)
    }
  }
  const DrawLine = (elementFrom, elementTo, diffX, diffY, color) => {
    const canvas = document.getElementById('canvas-draw-line-3')
    const ctx = canvas.getContext('2d')
    const rectCanvas = canvas.getBoundingClientRect()

    const rectIconBtn = elementFrom.getBoundingClientRect()
    const xBtn = rectIconBtn.x - rectCanvas.x + 7
    const yBtn = rectIconBtn.top - rectCanvas.top + 7
    const rectImg = elementTo.getBoundingClientRect()
    const xImg = rectImg.x - rectCanvas.x + diffX
    const yImg = rectImg.top - rectCanvas.top + diffY

    //draw
    ctx.beginPath()
    ctx.moveTo(xBtn, yBtn)
    ctx.lineWidth = 2
    ctx.lineCap = 'round'
    ctx.strokeStyle = color
    ctx.lineTo(xImg, yImg)
    ctx.stroke()

    elementFrom.style.visibility = 'visible'
    elementTo.style.visibility = 'visible'
  }
  const onScrollListButton = (position) => {
    let matchingButton = document.querySelector('.block-item_button_left.matching')
    if (position == 'right') {
      matchingButton = document.querySelector('.block-item_button_right.matching')
    }
    if (matchingButton) {
      const iconMatchingBtn = matchingButton.querySelector('.icon-matching')
      const divImageId = matchingButton.dataset['imageid']
      const divImageElement = document.getElementById(divImageId)
      const iconMatchingImage = divImageElement.querySelector(`.icon-matching-image.--${position}`)
      clearCanvas()
      const diftX = position == 'left' ? 0 : 6
      const diftY = 6
      DrawLine(
        iconMatchingBtn,
        iconMatchingImage,
        diftX,
        diftY,
        matchingButton.classList.contains('--example') ? '#67ab8d' : '#2692ff',
      )
    }
  }
  const drawPointMatching = (element, scale)=>{
    try {
      const imageElements = element.parentElement
      const iconImageRight = imageElements.querySelector('.icon-matching-image.--right')
      const iconImage = imageElements.querySelector('.icon-matching-image.--left')
      const canvasImage = document.createElement('canvas')
      const ctxLeft = canvasImage.getContext('2d', { willReadFrequently: true })
      const imgHeight = element.naturalHeight
      const imgWidth = element.naturalWidth
      canvasImage.width = imgWidth
      canvasImage.height = imgHeight
      ctxLeft.drawImage(element, 0, 0, imgWidth, imgHeight)
      if (iconImageRight) {
        // iconImageRight.style.visibility = 'hidden'
      }
      if (!iconImage) return
      for (let j = 0; j < imgWidth; j++) {
        const pixelData = ctxLeft.getImageData(j, imgHeight / 2, 1, 1).data
        if (pixelData[0] !== 0 || pixelData[1] !== 0 || pixelData[2] !== 0 || pixelData[3] !== 0) {
          iconImage.style.position = 'absolute'
          iconImage.style.top = `${(imgHeight * scale) / 2 - 6}px`
          iconImage.style.left = `${j * scale - 6}px`
          break
        }
      }
      for (let j = imgWidth; j > 0; j--) {
        const pixelData = ctxLeft.getImageData(j, imgHeight / 2, 1, 1).data
        if (pixelData[0] !== 0 || pixelData[1] !== 0 || pixelData[2] !== 0 || pixelData[3] !== 0) {
          iconImageRight.style.position = 'absolute'
          iconImageRight.style.top = `${(imgHeight * scale) / 2 - 6}px`
          iconImageRight.style.left = `${j * scale}px`
          break
        }
      }
    } catch (error) {
      console.error('drawPointMatching', error)
    }
  }
  const drawPoint2 = (e) => {
    drawPointMatching(e.currentTarget,scaleNumber)
  }
  const linkImage = (e, index) => {
    const element = e.currentTarget
    setTimeout(() => {
      element.style.visibility = 'visible'
    }, 0)
    const divImageMatching = element.parentElement
    const showImageAnswer = (src, buttonMatching) => {
      const thumbnailAnswer = buttonMatching.querySelector('.image-button')
      thumbnailAnswer.src = src
      thumbnailAnswer.style.display = 'block'
    }
    if (answerGroup.current[index] && answerGroup.current[index].isMatched) {
      const answersList = answer.filter((item) => !item.isExam)
      const idItemButton = answersList[answerGroup.current[index].index].key
      clearCanvas()
      // const isShowLine = mode.state == TEST_MODE.play
      const buttonMatching = document.getElementById(idItemButton)

      const idItemImage = divImageMatching.id
      buttonMatching.dataset['imageid'] = idItemImage
      divImageMatching.dataset['buttonid'] = idItemButton
      showImageAnswer(element.src, buttonMatching)
    } else {
      if (divImageMatching.classList.contains('--example-div')) {
        const buttonMatching = document.getElementById(divImageMatching.id.replace('image_', ''))
        showImageAnswer(element.src, buttonMatching)
      }
    }
  }
  const generateClassImage = (item, index) => {
    const className = {
      image: '',
      div: '',
    }
    if (item.isExam) {
      className.image = ' --example-image --image-matched'
      return className
    }
    switch (mode.state) {
      case TEST_MODE.play:
        className.image += ' --image-default'
        const value1 = userAnswer?.value
        if (value1 && value1[index] && value1[index].isMatched) {
          className.image += ' --image-matched'
        }
        break
      case TEST_MODE.check:
        className.div += ' --success'
        className.image += ' --image-matched'
        break
      case TEST_MODE.review:
        const value = userAnswer?.value
        if (value && value[index] && value[index].isMatched) {
          if (value[index].index == item.indexAnswer) {
            className.div += ' --success'
            className.image += ' --image-matched'
          } else {
            if (value) className.div += ' --danger'
            className.image += ' --image-matched'
          }
        } else {
          className.image += ' --image-default'
        }
        break
    }
    return className
  }
  const generateClassButton = (item) => {
    let className = 'default'
    if (item.isExam) {
      className = ' matched --example'
    } else
      switch (mode.state) {
        case TEST_MODE.play:
          if (userAnswer?.value?.find((m) => m.index == item.indexAnswer && m.isMatched)) {
            className += ' matched'
          }
          break
        case TEST_MODE.check:
          className += ' --success'
          className += ' matched'
          break
        case TEST_MODE.review:
          if (userAnswer?.value && Array.isArray(userAnswer?.value)) {
            const indexUserAnswer = userAnswer.value.findIndex((m) => m.index == item.indexAnswer && m.isMatched)
            if (indexUserAnswer != -1) {
              if (indexUserAnswer == item.indexAnswer) {
                className += ' --success'
                className += ' matched'
              } else {
                className += ' --danger'
                className += ' matched'
              }
            }
          }
          break
      }
    return className
  }
  const getImageButton = (item) => {
    if (mode.state != TEST_MODE.check && !item.isExam) {
      if (userAnswer?.value && Array.isArray(userAnswer.value)) {
        const indexUser = userAnswer.value.findIndex((m) => m.index == item.indexAnswer && m.isMatched)
        if (indexUser == -1) return ''
        const itemCorrect = data.answers.find((m) => m.indexAnswer == indexUser)
        return itemCorrect?.image
      }
      return ''
    } else {
      return item.image
    }
  }
  const onLoadImageInstruction = (e) => {
    imageWidthOriginRef.current = e.currentTarget.naturalWidth
    if(e.currentTarget.height > e.currentTarget.parentElement.offsetHeight && e.currentTarget.parentElement.offsetHeight!=0){
      e.currentTarget.style.height = e.currentTarget.parentElement.offsetHeight + "px"
    }
    const ratio = e.currentTarget.width / e.currentTarget.naturalWidth;
    setScaleNumber(ratio)
    e.currentTarget.style.visibility = 'visible'
    scaleNumberRef.current = ratio;
    const canvas = document.getElementById('canvas-draw-line-3')
    const content = document.getElementById('pt-o-game-draw-line-3-content-id')
    canvas.width = content.offsetWidth
    canvas.height = content.offsetHeight
    setOffsetImage({ top: e.currentTarget.offsetTop, left: e.currentTarget.offsetLeft })
  }
  const isLargeButtonAnswer = data.answers.some((m) => m.answer.length > 12)

  return (
    <GameWrapper className="pt-o-game-draw-line-3">
      <div className="pt-o-game-draw-line-3__container">
        <div className="pt-o-game-draw-line-3__right">
          <div className="__header">
            <FormatText tag="p">{instruction}</FormatText>
          </div>
          <div className="__content" id="pt-o-game-draw-line-3-content-id">
            <div className="block-item" onScroll={() => onScrollListButton('left')}>
              <div className="block-item__list">
                {ansLeft.map((itemLeft, index) => {
                  const buttonImageUrl = getImageButton(itemLeft)
                  return (
                    <div
                      id={itemLeft.key}
                      key={itemLeft.key}
                      data-position="left"
                      className={`block-item_button block-item_button_left ${generateClassButton(itemLeft)}`}
                      data-index={itemLeft.indexAnswer}
                      onClick={(e) => onClickButton(index, 'left', e)}
                      style={mode.state != TEST_MODE.play ? { pointerEvents: 'none' } : {}}
                      data-imageid={itemLeft.isExam ? `image_${itemLeft.key}` : ''}
                    >
                      <button className={`${isLargeButtonAnswer ? '--lg' : ''}`}>{itemLeft.answer}</button>
                      <img
                        style={mode.state !== TEST_MODE.play ? { display: 'none' } : {}}
                        className="icon-deleted"
                        height={25}
                        src="/images/icons/ic-delete-img.png"
                        alt="img-d"
                        onClick={onClickDeleted.bind(null, itemLeft.key, 'left')}
                      />
                      <img
                        style={{ visibility: 'hidden' }}
                        className={`icon-matching ${itemLeft.isExam ? '--example-matching' : ''}`}
                        height={10}
                        src={`/images/icons/ic-matching${itemLeft.isExam ? '-ex' : ''}.png`}
                        alt="img-m"
                      />
                      <img
                        key={buttonImageUrl}
                        id={buttonImageUrl}
                        style={buttonImageUrl ? {} : { display: 'none' }}
                        height={30}
                        src={buttonImageUrl}
                        alt="img"
                        className="image-button"
                      />
                    </div>
                  )
                })}
              </div>
            </div>
            <div className="block-item">
              <div
                className="wrap-image"
                style={{ objectFit: 'contain', maxHeight: '100%', position: 'relative', maxWidth: '100%' }}
              >
                <img
                  data-animate="fade-in"
                  className="image-instruction"
                  alt="image-instruction"
                  id="dl3-image-instruction-id"
                  src={data?.imageInstruction}
                  style={{ visibility: 'visible', maxHeight: '100%' }}
                  onLoad={(e) => {
                    onLoadImageInstruction(e)
                  }}
                />
                {data?.answers?.map((item, mIndex) => {
                  if (!scaleNumber) return null
                  const p = item.position.split(':')
                  const width = (p[2] - p[0]) * scaleNumber
                  const height = (p[3] - p[1]) * scaleNumber
                  const left = p[0] * scaleNumber
                  const top = p[1] * scaleNumber

                  const style = mode.state != TEST_MODE.play ? { pointerEvents: 'none' } : {}
                  style.position = 'absolute'
                  style.left = `${offsetImage.left + left}px`
                  style.top = `${offsetImage.top + top}px`
                  style.width = `${width}px`
                  style.height = `${height}px`
                  const className = generateClassImage(item, item.indexAnswer)
                  return (
                    <div
                      key={`image_${item.key}`}
                      id={`image_${item.key}`}
                      onClick={onClickImage.bind(null, mIndex, item.image)}
                      className={`item-image ${item.isExam ? '--example-div' : '--matching-div'} ${className.div}`}
                      data-index={item.indexAnswer}
                      data-buttonid={item.isExam ? item.key : ''}
                      style={style}
                      data-scale_number={scaleNumber}
                    >
                      <img
                        alt=""
                        src={item.image}
                        className={`item-image-content ${className.image} ${
                          isAppleDevice.current ? '--is-apple-device' : ''
                        }`}
                        data-animate="fade-in"
                        style={{ visibility: 'hidden' }}
                        onLoad={(e) => {
                          drawPoint2(e)
                          linkImage(e, item.indexAnswer)
                        }}
                      ></img>
                      <img
                        alt=""
                        src={`/images/icons/ic-matching${item.isExam ? '-ex' : ''}.png`}
                        className="icon-matching-image --left --matching-icon-matching"
                      ></img>
                      <img
                        alt=""
                        src={`/images/icons/ic-matching${item.isExam ? '-ex' : ''}.png`}
                        className="icon-matching-image --right --matching-icon-matching"
                      ></img>
                    </div>
                  )
                })}
              </div>
            </div>
            <div className="block-item" onScroll={() => onScrollListButton('right')}>
              <div className="block-item__list">
                {ansRight.map((itemRight, index) => {
                  const buttonImageUrl = getImageButton(itemRight)
                  return (
                    <div
                      id={itemRight.key}
                      key={itemRight.key}
                      data-position="right"
                      className={`block-item_button block-item_button_right ${generateClassButton(itemRight)}`}
                      data-index={itemRight.indexAnswer}
                      onClick={(e) => onClickButton(index, 'right', e)}
                      style={mode.state != TEST_MODE.play ? { pointerEvents: 'none' } : {}}
                      data-imageid={itemRight.isExam ? `image_${itemRight.key}` : ''}
                    >
                      <button className={`${isLargeButtonAnswer ? '--lg' : ''}`}>{itemRight.answer}</button>
                      <img
                        style={mode.state !== TEST_MODE.play ? { display: 'none' } : {}}
                        className="icon-deleted"
                        height={25}
                        src="/images/icons/ic-delete-img.png"
                        alt="img-d"
                        onClick={onClickDeleted.bind(null, itemRight.key, 'right')}
                      />
                      <img
                        className={`icon-matching --right ${itemRight.isExam ? '--example-matching' : ''}`}
                        height={10}
                        src={`/images/icons/ic-matching${itemRight.isExam ? '-ex' : ''}.png`}
                        alt="img-m"
                        style={{ visibility: 'hidden' }}
                      />
                      <img
                        key={buttonImageUrl}
                        id={buttonImageUrl}
                        style={buttonImageUrl ? {} : { display: 'none' }}
                        height={30}
                        src={buttonImageUrl}
                        alt="img"
                        className="image-button"
                      />
                    </div>
                  )
                })}
              </div>
            </div>
            <canvas id="canvas-draw-line-3"></canvas>
          </div>
        </div>
      </div>
    </GameWrapper>
  )
}
