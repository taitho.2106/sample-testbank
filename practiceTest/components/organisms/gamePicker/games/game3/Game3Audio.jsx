import { Fragment, useContext, useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { useWindowSize } from 'utils/hook'
import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomHeading } from '../../../../atoms/heading'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradientWithHeader } from '../../../blockBottomGradient/BlockBottomGradientWithHeader'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'

export const Game3Audio = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const question = data?.question || ''
  const questionInput = answers.filter((item) => !item.answer.startsWith('*'))

  const indexEx = answers.findIndex((item) => item.answer.startsWith('*'))
  const trueAnswer = answers
    .filter((m) => !m.answer.startsWith('*'))
    .map((item) => item.answer)

  // const replaceQuestion = question.replace(/%[0-9]+%/g, '%s%')
  const questionArr = convertStrToHtml(question).split('%s%')

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()

  const { mode, currentPart } = useContext(PageTestContext)

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)
  const [isZoom, setIsZoom] = useState(false)

  const defaultTextArr = getProgressActivityValue(
    Array.from(Array(questionInput.length), () => ''),
  )
  const [fillTextArr, setFillTextArr] = useState([...defaultTextArr])

  const audio = useRef(null)
  const contentRef = useRef(null)

  const checkDanger = (i) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.value &&
      userAnswer.value[i] !== '' &&
      userAnswer.status[i] === false
    )
      return '--danger'
    return ''
  }

  const checkEmpty = (i) => {
    if (fillTextArr[i] === '' && mode.state !== TEST_MODE.check)
      return '--empty'
    return ''
  }

  const checkSuccess = (i) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.value &&
        userAnswer.value[i] !== '' &&
        userAnswer.status[i] === true) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = (i) => {
    let className = ''
    className += checkEmpty(i)
    className += ' '
    className += checkDanger(i)
    className += ' '
    className += checkSuccess(i)
    return className
  }

  const onPaste = (event) => {
    const index = parseInt(event.target.dataset['index'])
    event.preventDefault()
    const data = event.clipboardData
      .getData('text')
      .replace(/\r\n/g, ' ')
      .trim()
    event.target.innerHTML = data
    fillTextArr[index] = data
    setFillTextArr([...fillTextArr])
  }

  const handleInputBlur = () => {
    const answerValue = [...fillTextArr]
    let statusArr = []
    trueAnswer.forEach((item, i) => {
      const splitCheck = item.toLowerCase().trim().split('%/%').map(m=>m?.trim()).filter(m=>m)
      const textI = answerValue[i]
      statusArr.push(
        splitCheck.includes(
          textI
            .replace(/&nbsp;/g, ' ')
            .trim()
            .toLowerCase(),
        ) ||
          splitCheck.includes(
            textI
              .replace(/&nbsp;/g, ' ')
              .trim()
              .toLowerCase() + '.',
          )
          ? true
          : false,
      )
    })

    setProgressActivityValue(answerValue, statusArr)
  }

  const handleInputChange = (index, text) => {
    const textArr = [...fillTextArr]
    if (typeof textArr[index] !== 'string') return
    textArr[index] = text
    setFillTextArr(textArr)
  }

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDownValue = audio.current.duration - audio.current.currentTime
    if (countDownValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }

  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  useEffect(() => {
    if (!contentRef?.current) return
    const spaceList = contentRef.current.querySelectorAll('.__editable')
    
    spaceList.forEach((item, i) => {
      if ([TEST_MODE.review, TEST_MODE.play].includes(mode.state)) {
        if (userAnswer?.value) {
          item.innerHTML = userAnswer.value[i] || ''
          if (!userAnswer.value[i]) item.classList.add('--empty')
        } else {
          item.innerHTML = ''
          item.classList.add('--empty')
        }

        if (i !== 0 && mode.state === TEST_MODE.play) return
        // item.focus()
      } else if (mode.state === TEST_MODE.check) {
        item.innerHTML = trueAnswer[i].split('%/%').map(m=>m?.trim()).filter(m=>m).join(" / ")
      }
    })
  }, [contentRef, mode.state, isShowTapescript, data.id])

  useEffect(() => {
    if (indexEx !== -1) {
      const example = contentRef.current.querySelectorAll('.__editableEX')
      example.forEach((item, i) => {
        item.innerHTML = answers[indexEx].answer.substring(1)
      })
    }
  }, [])

  const [width] = useWindowSize()
  const paddingBottom = width < 960 ? 1 : 4
  useEffect(() => {
    const el = document.querySelector(
      '.pt-o-block-bottom-gradient-with-header__header',
    )
    const node = document.querySelector(
      '.pt-o-block-bottom-gradient-with-header__content',
    )
    node.style.height = `calc(100% - ${paddingBottom}rem - ${el.offsetHeight}px)`
  }, [])
  let x = -1
  return (
    <GameWrapper className="pt-o-game-3-audio">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-3-audio__container" data-animate="fade-in">
        {isShowTapescript ? (
          <div className="__paper">
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
        ) : (
          <>
            <BlockWave
              className={`pt-o-game-3-audio__left ${
                mode.state !== TEST_MODE.play ? 'pt-o-game-3-audio__result' : ''
              }`}
            >
              <AudioPlayer
                className="__player"
                isPlaying={isPlaying}
                setIsPlaying={() => setIsPLaying(!isPlaying)}
              />
              <span className="__duration">{countDown}</span>
            </BlockWave>
            <div
              className={`pt-o-game-3-audio__right ${
                imageInstruction ? '--image' : ''
              }`}
            >
              {mode.state !== TEST_MODE.play && (
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                >
                  Tapescript
                </CustomButton>
              )}
              <BlockBottomGradientWithHeader
                headerChildren={
                  <CustomHeading tag="h6" className="__heading">
                    <FormatText tag="span">{instruction}</FormatText>
                  </CustomHeading>
                }
              >
                {imageInstruction && (
                  <>
                    <div className="__image-instruction">
                      <CustomImage
                        alt="Image instruction"
                        src={`${imageInstruction}`}
                        yRate={0}
                        onClick={() => setIsZoom(true)}
                      />
                    </div>
                    <ImageZooming
                      data={{
                        alt: 'Image Instruction',
                        src: `${imageInstruction}`,
                      }}
                      status={{
                        state: isZoom,
                        setState: setIsZoom,
                      }}
                    />
                  </>
                )}
                <Scrollbars universal={true}>
                  <div ref={contentRef} className="__content">
                    {questionArr.map((item, i) => {
                      if (i === 0 || i === indexEx + 1) {
                      } else {
                        x = x + 1
                      }
                      return (
                        <Fragment key={i}>
                          {i !== 0 && i !== indexEx + 1 && (
                            <span
                              data-index={x}
                              className={`__editable ${generateClassName(x)}`}
                              contentEditable={
                                mode.state === TEST_MODE.play ? true : false
                              }
                              style={{
                                pointerEvents:
                                  mode.state === TEST_MODE.play
                                    ? 'all'
                                    : 'none',
                              }}
                              onBlur={() =>
                                mode.state === TEST_MODE.play &&
                                handleInputBlur()
                              }
                              onInput={(e) => {
                                mode.state === TEST_MODE.play &&
                                  handleInputChange(
                                    e.target.dataset.index,
                                    e.target.innerHTML,
                                  )
                              }}
                              onPaste={(e) => {
                                mode.state === TEST_MODE.play && onPaste(e)
                              }}
                            ></span>
                          )}
                          {i !== 0 && i === indexEx + 1 && (
                            <span
                              className={`__editableEx`}
                              contentEditable={false}
                            >
                              {answers[indexEx].answer.substring(1)}
                            </span>
                          )}
                          {/* <FormatText tag="span">{item}</FormatText> */}
                          <span
                            dangerouslySetInnerHTML={{
                              __html: item,
                            }}
                          ></span>
                        </Fragment>
                      )
                    })}
                  </div>
                </Scrollbars>
              </BlockBottomGradientWithHeader>
            </div>
          </>
        )}
      </div>
    </GameWrapper>
  )
}
