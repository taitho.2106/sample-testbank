import { useContext, useState, useEffect, useRef } from 'react'

import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'

export const GameMC10 = ({ data }) => {
    const audioInstruction = data?.audioInstruction || ''
    const audioScript = data?.audioScript || ''
    const instruction = data?.instruction || ''
    const imageInstruction = data?.imageInstruction?.split('#') || []
    const questionsGroup = data?.questionsGroup || []

    const questionsGroupExample = questionsGroup.filter((m) =>
        m.question.startsWith('*'),
    )
    const questionsGroupGeneral = questionsGroup.filter(
        (m) => !m.question.startsWith('*'),
    )
    const trueAnswer = questionsGroupGeneral.map((list) =>
        list.answers.findIndex((item) => item.isCorrect === true),
    )

    const audio = useRef(null)
    const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
        useProgress()
    const { mode, currentQuestion } = useContext(PageTestContext)
    const [isZoomE, setIsZoomE] = useState(false)
    const [isZoomA, setIsZoomA] = useState(false)
    const [zoomStrA, setZoomStrA] = useState('')
    const [radio, setRadio] = useState([])

    const [countDown, setCountDown] = useState(null)
    const [isPlaying, setIsPLaying] = useState(false)
    const [isShowTapescript, setIsShowTapescript] = useState(false)

    useEffect(() => {
        const defaultRadio = getProgressActivityValue(
        Array.from(Array(questionsGroupGeneral.length), (e, i) => null),
        )
        setRadio(defaultRadio)
    }, [currentQuestion])

    const handleRadioClick = (i, j) => {
        let beforeArr = radio
        if (beforeArr[i] === undefined) return
        beforeArr[i] = j
        // check isCorrect
        let statusArr = []
        trueAnswer.forEach((item, i) =>
            statusArr.push(item === beforeArr[i] ? true : false),
        )
        // save progress
        setProgressActivityValue(beforeArr, statusArr)
        // display
        setRadio([...beforeArr])
    }

    const generateClassName = (i, j) => {
        let className = ''
        className += checkPrimary(i, j)
        className += ' '
        className += checkDanger(i, j)
        className += ' '
        className += checkSuccess(i, j)
        return className
    }

    const checkPrimary = (i, j) => {
        if (
            mode.state === TEST_MODE.play &&
            radio[i] !== undefined &&
            radio[i] === j
        )
            return '--checked'
        return ''
    }

    const checkDanger = (i, j) => {
        if (
            mode.state === TEST_MODE.review &&
            userAnswer.status[i] === false &&
            radio[i] !== undefined &&
            radio[i] === j
        )
            return '--danger'
        return ''
    }

    const checkSuccess = (i, j) => {
        if (
            (mode.state === TEST_MODE.review &&
                userAnswer.status[i] === true &&
                radio[i] !== undefined &&
                radio[i] === j) ||
            (mode.state === TEST_MODE.check && trueAnswer[i] === j)
        )
            return '--success'
        return ''
    }

    const handleAudioTimeUpdate = () => {
        if (!audio?.current) return
        else {
            const countDownValue = audio.current.duration - audio.current.currentTime
            if (countDownValue > 0)
                setCountDown(formatDateTime(Math.ceil(countDownValue) * 1000))
            else {
                setIsPLaying(false)
                audio.current.currentTime = 0
            }
        }
    }
    
    useEffect(() => {
        if (!audio?.current) return
        if (isPlaying) audio.current.play()
        else audio.current.pause()
    }, [isPlaying, audio])

    return (
        <GameWrapper className="pt-o-game-mc10">
            <audio
                ref={audio}
                style={{ display: 'none' }}
                onLoadedMetadata={() =>
                    setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
                }
                onTimeUpdate={() => handleAudioTimeUpdate()}
            >
                <source src={audioInstruction} />
            </audio>

            <div data-animate="fade-in" className='pt-o-game-mc10__container'>
            {isShowTapescript ? (
                <div className="__paper">
                    <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
                        <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
                    </div>
                    <BlockPaper>
                        <div className="__content">
                            <FormatText>{audioScript}</FormatText>
                        </div>
                    </BlockPaper>
                </div>
            ) : (
            <>
                <BlockWave className={`pt-o-game-mc10__left ${mode.state !== TEST_MODE.play ? 'pt-o-game-mc10__result' : ''}`}>
                    <AudioPlayer
                        className="__player"
                        isPlaying={isPlaying}
                        setIsPlaying={() => setIsPLaying(!isPlaying)}
                    />
                    <span className="__duration">{countDown}</span>
                </BlockWave>
                <div
                    className={`pt-o-game-mc10__right`}
                    data-animate="fade-in"
                >
                    {mode.state !== TEST_MODE.play && (
                        <CustomButton
                            className="__tapescript"
                            onClick={() => setIsShowTapescript(true)}
                        >
                        Tapescript
                        </CustomButton>
                    )}
                    <div className={`__header`} id="span_text">
                        <FormatText tag="p">{instruction}</FormatText>
                    </div>
                    <div className="__content">
                        {questionsGroupExample.length > 0 && (
                            <div className="__content-example">
                                <div className="__content-example__header">Example:</div>
                                <hr />
                                {questionsGroupExample.map((answer, answerIndex) => {
                                    const html = convertStrToHtml(
                                        answer.question.substring(1),
                                    ).replaceAll('%s%', `<div class="__space"></div>`)
                                    return (
                                        <div
                                            key={`example-${answerIndex}`}
                                            className="__content-example__item"
                                        >
                                            {html && <div className="__item__title">
                                                <span
                                                dangerouslySetInnerHTML={{
                                                    __html: html,
                                                }}
                                                ></span>
                                            </div>}
                                            <div className="__item__container">
                                                <div className="__item__image">
                                                    <div>
                                                        <img
                                                            alt=""
                                                            src={
                                                                imageInstruction[
                                                                questionsGroupExample.length > 0 ? 0 : null
                                                                ]
                                                            }
                                                            onClick={() => setIsZoomE(true)}
                                                        />
                                                    </div>
                                                </div>
                                                <ImageZooming
                                                    data={{
                                                        alt: 'Image Instruction',
                                                        src: `${
                                                        imageInstruction[
                                                            questionsGroupExample.length > 0 ? 0 : null
                                                        ]
                                                        }`,
                                                    }}
                                                    status={{
                                                        state: isZoomE,
                                                        setState: setIsZoomE,
                                                    }}
                                                />
                                                <div className="__item__question">
                                                    <div className={`mc10-a-multichoiceanswers answer`}>
                                                        <div className="mc10-a-multichoiceanswers__answer">
                                                            <ul className="mc10-a-multichoiceanswers__input ">
                                                                {answer.answers.map((item, index) => (
                                                                <li key={`question-ans-${index}`}>
                                                                    <input
                                                                        type="checkbox"
                                                                        name={`mc10-id${index}`}
                                                                        className="option-input checkbox"
                                                                        defaultChecked={item.isCorrect}
                                                                        style={{ pointerEvents: 'none' }}
                                                                    />
                                                                    <label
                                                                        dangerouslySetInnerHTML={{
                                                                            __html: convertStrToHtml(item.text),
                                                                        }}
                                                                    ></label>
                                                                </li>
                                                                ))}
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        )}
                        <ImageZooming
                            data={{
                                alt: 'Image Instruction',
                                src: `${zoomStrA}`,
                            }}
                            status={{
                                state: isZoomA,
                                setState: setIsZoomA,
                            }}
                        />
                        <div className="__content-question">
                                <div className="__content-question__header">{`Question${questionsGroupGeneral.length > 1 ? 's' : ''}:`}</div>
                                <hr />
                                {questionsGroupGeneral.map((answer, answerIndex) => {
                                    const html = convertStrToHtml(answer.question).replaceAll(
                                        '%s%',
                                        `<div class="__space"></div>`,
                                    )
                                    return (
                                        <div
                                            key={`question-mc10-${answerIndex}`}
                                            className="__content-question__item"
                                        >
                                            {
                                                html && 
                                                <div className="__item__title">
                                                    <span
                                                        className="text"
                                                        dangerouslySetInnerHTML={{
                                                        __html: html,
                                                        }}
                                                    ></span>
                                                </div>
                                            }
                                            <div className="__item__container">
                                                <div className="__item__image">
                                                    <div>
                                                        <img
                                                            alt=""
                                                            src={
                                                                imageInstruction[
                                                                    questionsGroupExample.length > 0
                                                                    ? answerIndex + 1
                                                                    : answerIndex
                                                                ]
                                                            }
                                                            onClick={() => {
                                                            setIsZoomA(true),
                                                                setZoomStrA(
                                                                    imageInstruction[
                                                                        questionsGroupExample.length > 0
                                                                        ? answerIndex + 1
                                                                        : answerIndex
                                                                    ],
                                                                )
                                                            }}
                                                        />
                                                    </div>
                                                </div>
                                                <div className="__item__question">
                                                    <div className="__radio-list">
                                                    {answer.answers.map((item, mIndex) => {
                                                        return (
                                                        <div
                                                            key={`__radio-item-${mIndex}`}
                                                            className={`__radio-item ${generateClassName(
                                                                answerIndex,
                                                                mIndex,
                                                            )}`}
                                                            style={{
                                                                pointerEvents:
                                                                    mode.state === TEST_MODE.play
                                                                    ? 'all'
                                                                    : 'none',
                                                            }}
                                                            onClick={() =>
                                                                mode.state === TEST_MODE.play &&
                                                                handleRadioClick(answerIndex, mIndex)
                                                            }
                                                        >
                                                            <label
                                                                className="pt-a-text"
                                                                dangerouslySetInnerHTML={{
                                                                    __html: convertStrToHtml(item.text),
                                                                }}
                                                            ></label>
                                                        </div>
                                                        )
                                                    })}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                        </div>
                    </div>
                </div>
            </>
            )}
            </div>
        </GameWrapper>
    )
}
