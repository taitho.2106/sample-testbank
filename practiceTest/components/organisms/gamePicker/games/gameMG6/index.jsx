import { useContext, useState } from 'react'

import { DragDropContext, Draggable, Droppable } from 'react-beautiful-dnd'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { FormatText } from '../../../../molecules/formatText'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'

export const GameMG6 = ({ data }) => {
  const answers = data?.answers
  const answersExample = data?.answersExample || []
  const instruction = data?.instruction || ''
  const audioScript = data.audioScript
  let defaultRightColumn = []
  let leftColumn = []
  let trueAnswer = []
  let wrongAnswer = Array.from(Array(answers.length), (e, i) => i)

  answers.forEach((item, i) => {
    item.left && leftColumn.push(item.left)
    defaultRightColumn.push({
      id: `item-${i}`,
      content: item.right,
      index: i,
      isMatched: false,
    })
    if (item.rightAnswerPosition !== -1) {
      trueAnswer.push(item.rightAnswerPosition)
      wrongAnswer = wrongAnswer.filter(
        (filter) => filter !== item.rightAnswerPosition,
      )
    }

  })

  trueAnswer = trueAnswer.concat(wrongAnswer)

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()
  const { mode } = useContext(PageTestContext)

  const [isZoom, setIsZoom] = useState(false)
  const [imageStr, setImageStr] = useState('')
  if (userAnswer.value !== null) {
    let sortDefaultRightColumn = []
    userAnswer.value.forEach((item, i) => {
      const target = defaultRightColumn[item.index]
      if (target) {
        target.isMatched = item.isMatched
        sortDefaultRightColumn.push(target)
      }
    })
    defaultRightColumn = sortDefaultRightColumn
  }
  const orderRightColumn = getProgressActivityValue(
    Array.from(Array(defaultRightColumn.length), (e, i) => ({
      index: i,
      isMatched: false,
    })),
  )
  let defaultArr = []
  orderRightColumn.forEach((item, i) => {
    const target = defaultRightColumn[i]
    if (target) {
      target.isMatched = item.isMatched
      defaultArr.push(target)
    }
  })
  const [rightColumn, setRightColumn] = useState({ items: defaultArr })

  const checkDanger = (i) => {
    if (
      userAnswer.value !== null &&
      mode.state === TEST_MODE.review &&
      userAnswer.value[i] &&
      userAnswer.value[i].index !== trueAnswer[i] &&
      userAnswer.value[i].isMatched
    )
      return '--danger'
    return ''
  }

  const checkSuccess = (i) => {
    if (
      (userAnswer.value !== null &&
        mode.state === TEST_MODE.review &&
        userAnswer.value[i] &&
        userAnswer.value[i].index === trueAnswer[i] &&
        userAnswer.value[i].isMatched) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = (i) => {
    let className = ''
    className += checkDanger(i)
    className += ' '
    className += checkSuccess(i)
    return className
  }

  const getStyle = (style, snapshot) => {
    if (!snapshot.isDragging) return {}
    if (!snapshot.isDropAnimating) return style
    return {
      ...style,
      transitionDuration: `0.001s`,
    }
  }

  const onDragEnd = (result) => {
    // dropped outside the list
    if (!result.destination) return
    const items = reorder(
      rightColumn.items,
      result.source.index,
      result.destination.index,
    )
    const newArr = items.map((item) => item)
    const newArrClone = newArr.slice(0, trueAnswer.length);
    setProgressActivityValue(
      newArr.map((item) => ({
        index: item.index,
        isMatched: item.isMatched,
      })),
      leftColumn.map((item, i) =>
       ( trueAnswer[i] === newArrClone[i].index && newArrClone[i].isMatched == true)
          ? true
          : false,
      ),
    )
    setRightColumn({ items })
  }

  const reorder = (list, startIndex, endIndex) => {
    // a little function to help us with reordering the result
    const result = Array.from(list)
    // drag at same positon
    if (startIndex === endIndex) {
      if (result[startIndex].isMatched) result[startIndex].isMatched = false
      else result[startIndex].isMatched = true
      return result
    }
    // drag to other position
    const temp = result[endIndex]
    const deafultStartMatched = result[startIndex].isMatched
    result[endIndex] = result[startIndex]
    result[endIndex].isMatched = true
    result[startIndex] = temp
    result[startIndex].isMatched = deafultStartMatched
    return result
  }

  const seeMore = () => {
    const element = document.getElementsByClassName('pt-o-game-mg4')[0]
    scrollTo(element, element.scrollHeight, 600)
  }

  function scrollTo(element, to, duration) {
    if (duration <= 0) return
    const difference = Math.abs(to - element.scrollTop)
    const perTick = (difference / duration) * 10

    setTimeout(function () {
      element.scrollTop = scrollToBottom
        ? element.scrollTop - perTick
        : element.scrollTop + perTick
      if (element.scrollTop === to) return
      scrollTo(element, to, duration - 10)
    }, 10)
    setScrollToBottom(!scrollToBottom)
  }

  
  return (
    <GameWrapper className="pt-o-game-mg6">
      <>
        <div
          className={`pt-o-game-mg6__right`}
        >
          <div className="__header" id="header-instruction">
            <FormatText tag="p">{instruction}</FormatText>
          </div>
          <div className="__content">
          <ImageZooming
              data={{ alt: 'img', src: `${imageStr}` }}
              status={{
                state: isZoom,
                setState: setIsZoom,
              }}
            />
              {answersExample && answersExample.length > 0 && (
                <div>
                  <div className="__example">
                    <span className="__title">Example:</span>
                    <div className="__center-column">
                      <div className={`__left-item`}>
                        <div className="__left-item__image">
                          <img 
                            src={answersExample[0].left}
                            alt=''
                            onClick={()=>{
                              setIsZoom(true)
                              setImageStr(answersExample[0].left)
                            }}
                          />
                        </div>
                        <div className="__left-item__linked">
                          <div></div>
                          <div></div>
                        </div>
                      </div>
                      <div className={`__right-item`}>
                        <div className="__right-item__linked">
                          <div></div>
                          <div></div>
                        </div>
                        <div className="__right-item__image">
                          <img 
                            src={answersExample[0].right}
                            alt=''
                            onClick={()=>{
                              setIsZoom(true)
                              setImageStr(answersExample[0].right)
                            }}
                          />
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              )}
              <div className="__title-question">
                <span className="__title">{`Question${leftColumn.length > 1 ? 's' : ''}:`}</span>
              </div>
              <div>
                <div className={`__left`}>
                  {leftColumn.map((item, i) => (
                    <div
                      key={i}
                      className={`__tag ${
                        rightColumn.items[i].isMatched === true ||
                        mode.state === TEST_MODE.check
                          ? '--matched'
                          : ''
                      } ${generateClassName(i)}`}
                    >
                      <div className="__left-item__image">
                          <img 
                            src={item}
                            alt=''
                            onClick={()=>{
                              setIsZoom(true)
                              setImageStr(item)
                            }}
                          />
                      </div>
                    </div>
                  ))}
                </div>
                <div
                  className={`__right`}
                  // style={{
                  //   pointerEvents:
                  //     mode.state === TEST_MODE.play ? 'all' : 'none',
                  // }}
                >
                  <DragDropContext
                    onDragEnd={(result) =>
                      mode.state === TEST_MODE.play && onDragEnd(result)
                    }
                  >
                    <Droppable droppableId="droppable">
                      {(provided, snapshot) => (
                        <div
                          {...provided.droppableProps}
                          ref={provided.innerRef}
                        >
                          {rightColumn.items.map((item, index) => (
                            <div key={item.id} className="__item">
                              <Draggable isDragDisabled={mode.state !== TEST_MODE.play ? true : false} draggableId={item.id} index={index}>
                                {(provided, snapshot) => (
                                  <div
                                    className={`__tag ${
                                      snapshot.isDragging ? '--dragging' : ''
                                    } ${
                                      item.isMatched === true ||
                                      mode.state === TEST_MODE.check
                                        ? '--matched'
                                        : ''
                                    } ${generateClassName(index)}`}
                                    ref={provided.innerRef}
                                    {...provided.draggableProps}
                                    {...provided.dragHandleProps}
                                    style={getStyle(
                                      provided.draggableProps.style,
                                      snapshot,
                                    )}
                                  >
                                    <div className="__right-item__image">
                                      <img 
                                        src={mode.state === TEST_MODE.check
                                          ? answers[trueAnswer[index]] &&
                                            answers[trueAnswer[index]].right
                                          : item.content}
                                        alt=''
                                        onClick={()=>{
                                          setIsZoom(true)
                                          setImageStr(mode.state === TEST_MODE.check
                                            ? answers[trueAnswer[index]] &&
                                              answers[trueAnswer[index]].right
                                            : item.content)
                                        }}
                                      />
                                  </div>
                                    <div className="__circle"></div>
                                  </div>
                                )}
                              </Draggable>
                            </div>
                          ))}
                          {provided.placeholder}
                        </div>
                      )}
                    </Droppable>
                  </DragDropContext>
                </div>
              </div>
          </div>
        </div>
      </>
    </GameWrapper>
  )
}
