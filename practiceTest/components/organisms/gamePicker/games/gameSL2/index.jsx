import { Fragment, useContext, useEffect, useRef, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomHeading } from '../../../../atoms/heading'
import { CustomImage } from '../../../../atoms/image'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
// import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradientWithHeader } from '../../../blockBottomGradient/BlockBottomGradientWithHeader'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'
import Dropdown from 'rsuite/Dropdown';
import PageEnd from '@rsuite/icons/PageEnd'
import { checkIsScrollable } from '../../../../../interfaces/functionary'
import { ImageZooming } from '../../../../molecules/modals/imageZooming'
import { convertStrToHtml } from '../../../../../../utils/string'

export const GameSL2 = ({ data }) => {
  const answers = data?.answers || []
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const imageInstruction = data?.imageInstruction || ''
  const instruction = data?.instruction || ''
  const question = data?.question || ''

  const trueAnswer = answers.map((answer) =>
    answer.answerList
      .trim()
      .split('/')
      .findIndex((item) => item.trim() === answer.answer.trim()),
  )

  const questionArr = question.split('%s%')

  const pickers = answers.map((item) => item.answerList.split('/'))

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()

  const { mode,currentQuestion } = useContext(PageTestContext)

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)
  const [currentGroupAnswer, setCurrentGroupAnswer] = useState(null)
  const [isOpenCollapse, setIsOpenCollapse] = useState(false)
  // const [topDrawer,setTopDrawer]=useState(0)
  const [isZoom, setIsZoom] = useState(false)
  const [zoomUrl, setZoomUrl] = useState('');

  const defaultArray = getProgressActivityValue(
    Array.from(Array(pickers.length), () => null),
  )
  const [chosenAnswers, setChosenAnswers] = useState(defaultArray)

  const drawer = useRef(null)

  const defaultTextArr = getProgressActivityValue(
    Array.from(Array(questionArr.length), () => ''),
  )
  const [fillTextArr, setFillTextArr] = useState([...defaultTextArr])

  const audio = useRef(null)
  const contentRef = useRef(null)

  useEffect(() => {
    if (userAnswer.group !== 1 && userAnswer.value === null)
      setProgressActivityValue(
        Array.from(Array(pickers.length), () => null),
        Array.from(Array(pickers.length), () => false),
      )

    window.addEventListener('click', closeCollapse)
    return () => window.removeEventListener('click', closeCollapse)
  }, [])

  useEffect(() => {
    if (drawer?.current && mode.state === TEST_MODE.play)
      drawer.current.scrollTo({ top: 0, behaviour: 'smooth' })
  }, [isOpenCollapse, drawer.current])

  const checkDanger = (i) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.value &&
      userAnswer.value[i] !== '' &&
      userAnswer.status[i] === false
    )
      return '--danger'
    return ''
  }

  const checkInline = (i) => {
    if (
      ([TEST_MODE.play, TEST_MODE.review].includes(mode.state) &&
        chosenAnswers[i] !== null &&
        pickers[i][chosenAnswers[i]]) ||
      mode.state === TEST_MODE.check
    )
      return '--inline'
    return ''
  }

  const checkEmpty = (i) => {
    if (fillTextArr[i] === '' && mode.state !== TEST_MODE.check)
      return '--empty'
    return ''
  }

  const checkSuccess = (i) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.value &&
        userAnswer.value[i] !== '' &&
        userAnswer.status[i] === true) ||
      mode.state === TEST_MODE.check
    )
      return '--success'
    return ''
  }

  const generateClassName = (i) => {
    let className = ''
    className += checkEmpty(i)
    className += ' '
    className += checkInline(i)
    className += ' '
    className += checkDanger(i)
    className += ' '
    className += checkSuccess(i)
    return className
  }

  const closeCollapse = (e) => {
    if (
      e.target.classList.contains('__space') ||
      e.target.classList.contains('__drawer') ||
      e.target.closest('.__drawer')
    )
      return
    setIsOpenCollapse(false)
    //setTopDrawer(0)
  }

  const handlePickerClick = (groupIndex, index) => {

    // update value
    let currentChosenAnswers = chosenAnswers
    currentChosenAnswers[groupIndex] = index
    // check isCorrect
    let statusArr = []
    trueAnswer.forEach((item, i) =>
      statusArr.push(item === currentChosenAnswers[i] ? true : false),
    )

    // save progress
    setProgressActivityValue(currentChosenAnswers, statusArr)
    // display
    setChosenAnswers([...currentChosenAnswers])
    setIsOpenCollapse(false)
  }

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }
  //change question
  useEffect(() => {
    const defaultArray = getProgressActivityValue(
      Array.from(Array(pickers.length), () => null),
    )
    setChosenAnswers(defaultArray)
  }, [currentQuestion])
  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  function renderDropdown(j){
    return (
      <Dropdown key={j} 
        title={
          mode.state === TEST_MODE.check ? pickers[j][trueAnswer[j]] : chosenAnswers[j] !== null && pickers[j][chosenAnswers[j]]
        }
        placement="bottomStart"
        onSelect={(eventKey, e) => handlePickerClick(j, eventKey)}
        className={generateClassName(j)}
        disabled={mode.state !== TEST_MODE.play}
      >
        {pickers[j].map((item, index) => 
          <Dropdown.Item key={j + index} eventKey={index}>{item}</Dropdown.Item>,
        )}
      </Dropdown>
    )
  }

  const renderQuestion = (question, j) => {
    const finalSentences = question.split('%s%');
    return finalSentences.map((item, index) => {
      let newItem = ''

      if(item && index === 0){
        newItem = item.endsWith(' ') ? item : item + ' '
      }
      if(item && index !== 0){
        newItem = item.startsWith(' ') ? item : ' ' + item
      }
      return (
        <Fragment key={j + index + ''}>
            {index !== 0 && renderDropdown(j)}
            {newItem && <FormatText tag="span">{convertStrToHtml(newItem)}</FormatText>}
        </Fragment>
      )
    })

  }


  return (
    <GameWrapper className="pt-o-game-sl2-audio">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      <div className="pt-o-game-sl2-audio__container" data-animate="fade-in">
        {isShowTapescript ? (
          <div className="__paper">
            <div
              className="__toggle"
              onClick={() => setIsShowTapescript(false)}
            >
              <CustomImage alt="" src="/pt/images/icons/ic-plus.svg" />
            </div>
            <BlockPaper>
              <div className="__content">
                <FormatText>{audioScript}</FormatText>
              </div>
            </BlockPaper>
          </div>
        ) : (
          <>
            <BlockWave
              className={`pt-o-game-sl2-audio__left ${mode.state !== TEST_MODE.play ? 'pt-o-game-sl2-audio__result' : ''
                }`}
            >
              <AudioPlayer
                className="__player"
                isPlaying={isPlaying}
                setIsPlaying={() => setIsPLaying(!isPlaying)}
              />
              <span className="__duration">{countDown}</span>
            </BlockWave>
            <div
              className={`pt-o-game-sl2-audio__right ${imageInstruction ? '--image' : ''
                }`}
            >
              {mode.state !== TEST_MODE.play && (
                <CustomButton
                  className="__tapescript"
                  onClick={() => setIsShowTapescript(true)}
                >
                  Tapescript
                </CustomButton>
              )}
              <BlockBottomGradientWithHeader
                headerChildren={
                  <CustomHeading tag="h6" className="__heading">
                    <FormatText tag="span">{instruction}</FormatText>
                  </CustomHeading>
                }
              >
                {/* <Scrollbars universal={true}> */}
                  <div ref={contentRef} className="__content">
                    { zoomUrl.length > 0 &&
                      <ImageZooming
                        data={{
                          alt: 'Image Instruction',
                          src: `${zoomUrl}`,
                        }}
                        status={{
                          state: isZoom,
                          setState: setIsZoom,
                        }}
                      />
                    }
                    {answers.map((item, j) => {
                      return (
                        <Fragment key={j}>
                          <div
                            className="__item"
                          >
                            <CustomImage
                              alt=""
                              src={item?.imageInstruction}
                              yRate={0}
                              onClick={() => {
                                setIsZoom(true)
                                setZoomUrl(item?.imageInstruction)
                              }}
                            />
                            <div className="select">
                              {renderQuestion(item?.question, j)}
                            </div>
                          </div>
                        </Fragment>
                      )
                    })}
                  </div>
                {/* </Scrollbars> */}
              </BlockBottomGradientWithHeader>
            </div>
          </>
        )}
      </div>
    </GameWrapper>
  )
}
