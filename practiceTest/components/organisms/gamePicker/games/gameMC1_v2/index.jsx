import { Fragment } from 'react'
import { useContext, useState, useEffect } from 'react'

import PageEnd from '@rsuite/icons/PageEnd'
import Scrollbars from 'react-custom-scrollbars'

import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { checkIsScrollable } from '../../../../../interfaces/functionary'
import { CustomHeading } from '../../../../atoms/heading'
import { CustomText } from '../../../../atoms/text'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradient } from '../../../blockBottomGradient'
import { BlockPaper } from '../../../blockPaper'

export const GameMC1_v2 = ({ data }) => {
  const instruction = data?.instruction || ''
  const questionInstruction = data?.questionInstruction || ''
  const questionsGroup = data?.questionsGroup || []
  const exampleLists = []
  const questionLists = []
  for(let i = 0; i< questionsGroup.length; i++){
    const item = questionsGroup[i]
    if(item.isExample){
      exampleLists.push(item)
    }else{
      questionLists.push(item)
    }
  }
  const trueAnswer = questionLists.map((list) =>
    list.answers.findIndex((item) => item.isCorrect === true),
  )

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()

  const { mode,currentQuestion } = useContext(PageTestContext)
  const [radio, setRadio] = useState([])

  const [isScrollable, setIsScrollable] = useState(false)
  const [scrollToBottom, setScrollToBottom] = useState(false)

  const checkPrimary = (i, j) => {
    if (
      mode.state === TEST_MODE.play &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--checked'
    return ''
  }

  const checkDanger = (i, j) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.status[i] === false &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--danger'
    return ''
  }

  const checkSuccess = (i, j) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.status[i] === true &&
        radio[i] !== undefined &&
        radio[i] === j) ||
      (mode.state === TEST_MODE.check && trueAnswer[i] === j)
    )
      return '--success'
    return ''
  }

  const generateClassName = (i, j) => {
    let className = ''
    className += checkPrimary(i, j)
    className += ' '
    className += checkDanger(i, j)
    className += ' '
    className += checkSuccess(i, j)
    return className
  }

  const handleRadioClick = (i, j) => {
    let beforeArr = radio
    if (beforeArr[i] === undefined) return
    beforeArr[i] = j
    // check isCorrect
    let statusArr = []
    trueAnswer.forEach((item, i) =>
      statusArr.push(item === beforeArr[i] ? true : false),
    )
    // save progress
    setProgressActivityValue(beforeArr, statusArr)
    // display
    setRadio([...beforeArr])
  }

  const seeMore = () => {
    const element = document.getElementsByClassName('pt-o-game-mc1-v2')[0]
    scrollTo(element, element.scrollHeight, 600)
  }

  function scrollTo(element, to, duration) {
    if (duration <= 0) return
    const difference = Math.abs(to - element.scrollTop)
    const perTick = (difference / duration) * 10

    setTimeout(function () {
      element.scrollTop = scrollToBottom
        ? element.scrollTop - perTick
        : element.scrollTop + perTick
      if (element.scrollTop === to) return
      scrollTo(element, to, duration - 10)
    }, 10)
    setScrollToBottom(!scrollToBottom)
  }
  //change question
  useEffect(() => {
    const defaultRadio = getProgressActivityValue(
      Array.from(Array(questionLists.length), (e, i) => null),
    )
    setRadio(defaultRadio)
  }, [currentQuestion])

  useEffect(() => {
    const element = document.getElementsByClassName('pt-o-game-mc1-v2')[0]
    setIsScrollable(checkIsScrollable(element))
    element.addEventListener('scroll', () => {
      if (
        element.scrollHeight - Math.ceil(element.scrollTop) <=
        element.clientHeight
      ) {
        setScrollToBottom(true)
      }
      if (element.scrollTop === 0) {
        setScrollToBottom(false)
      }
    })
    return () => {
      element.removeEventListener('scroll', () => {
        //
      })
    }
  }, [])

  return (
    <GameWrapper className="pt-o-game-mc1-v2">
      <div className="pt-o-game-mc1-v2__right">
        <div className="__header">
          <CustomHeading tag="h6" className="__description">
            <FormatText tag="span">{instruction}</FormatText>
          </CustomHeading>
        </div>
        <div className="__content">
          <>
            <>
              {exampleLists.length > 0 && (
                <div className='__content__group_examples'>
                  <div className='title_example'>
                    <span>Example</span>
                  </div>
                  {exampleLists.map((item, i) => {
                    const html = convertStrToHtml(item.question).replaceAll("%s%",`<div class="__space"></div>`);
                    return (
                      <div key={`ex-${i}`} className="__radio-group">
                        <CustomHeading tag="h6" className="__radio-heading">
                        <span
                                dangerouslySetInnerHTML={{
                                  __html:html ,
                                }}
                              ></span>
                        </CustomHeading>
                        <div className="__radio-list" style={{
                          paddingLeft:'2rem'
                        }}>
                          {item.answers.map((childItem, j) => (
                            <div
                            key={`ex-${i}_${j}`}
                            className={`mc1-a-multichoiceanswers answer`}
                          >
                            <div className="mc1-a-multichoiceanswers__answer">
                              <ul className="mc1-a-multichoiceanswers__input ">
                                <li>
                                  <input
                                    type="checkbox"
                                    name={`mc1-id${i}`}
                                    className="option-input checkbox"
                                    defaultChecked={childItem.isCorrect}
                                    style={{ pointerEvents: 'none' }}
                                  />
                                  <label
                                    dangerouslySetInnerHTML={{
                                      __html: convertStrToHtml(childItem.text),
                                    }}
                                  ></label>
                                </li>
                              </ul>
                            </div>
                          </div>
                          ))}
                        </div>
                      </div>
                    )
                  })}
                </div>
              )}
            </>
            <div className='__content__group_questions'>
              <div className='title_question'>
                <span>{questionLists.length > 1 ? 'Questions' : 'Question'}</span>
              </div>
            {questionLists.map((item, i) => {
              const html = convertStrToHtml(item.question).replaceAll("%s%",`<div class="__space"></div>`);
              return (
                <div key={i} className="__radio-group">
                  <CustomHeading tag="h6" className="__radio-heading">
                  <span
                          dangerouslySetInnerHTML={{
                            __html:html ,
                          }}
                        ></span>
                  </CustomHeading>
                  <div className="__radio-list">
                    {item.answers.map((childItem, j) => (
                      <div
                        key={`${i}_${j}`}
                        className={`__radio-item ${generateClassName(i, j)}`}
                        style={{
                          pointerEvents:
                            mode.state === TEST_MODE.play ? 'all' : 'none',
                        }}
                        onClick={() =>
                          mode.state === TEST_MODE.play && handleRadioClick(i, j)
                        }
                      >
                         <p className='pt-a-text'
                          dangerouslySetInnerHTML={{
                            __html: convertStrToHtml(childItem.text),
                          }}
                        ></p>
                        {/* <CustomText tag="p">{childItem.text}</CustomText> */}
                      </div>
                    ))}
                  </div>
                </div>
              )
            })}
            </div>
            
          </>
        </div>
      </div>
      {isScrollable && (
        <div className="pt-o-game-mc1-v2__see-more" onClick={seeMore}>
          <PageEnd
            rotate={scrollToBottom ? -90 : 90}
            color="white"
            style={{ fontSize: 28 }}
          />
        </div>
      )}
    </GameWrapper>
  )
}
