import { Fragment } from 'react'
import { useContext, useEffect, useRef, useState } from 'react'

import { convertStrToHtml } from 'utils/string'

import useProgress from '../../../../../hook/useProgress'
import { TEST_MODE } from '../../../../../interfaces/constants'
import { PageTestContext } from '../../../../../interfaces/contexts'
import { formatDateTime } from '../../../../../utils/functions'
import { CustomButton } from '../../../../atoms/button'
import { CustomHeading } from '../../../../atoms/heading'
import { CustomImage } from '../../../../atoms/image'
import { CustomText } from '../../../../atoms/text'
import { AudioPlayer } from '../../../../molecules/audioPlayer'
import { FormatText } from '../../../../molecules/formatText'
import { GameWrapper } from '../../../../templates/gameWrapper'
import { BlockBottomGradient } from '../../../blockBottomGradient'
import { BlockPaper } from '../../../blockPaper'
import { BlockWave } from '../../../blockWave'

export const Game11Audio = ({ data }) => {
  const instruction = data?.instruction || ''
  const audioInstruction = data?.audioInstruction || ''
  const audioScript = data?.audioScript || ''
  const questionsGroup = data?.questionsGroup || []
  const examplesList = []
  const questionList = []
  for (let i = 0; i < questionsGroup.length; i++) {
    const item = questionsGroup[i]
    if (item.isExample) {
      examplesList.push(item)
    } else {
      questionList.push(item)
    }
  }
  const trueAnswer = questionList.map((list) =>
    list.answers.findIndex((item) => item.isCorrect === true),
  )

  const { userAnswer, getProgressActivityValue, setProgressActivityValue } =
    useProgress()

  const { mode, currentQuestion } = useContext(PageTestContext)

  const [countDown, setCountDown] = useState(null)
  const [isPlaying, setIsPLaying] = useState(false)
  const [isShowTapescript, setIsShowTapescript] = useState(false)

  const defaultRadio = getProgressActivityValue(
    Array.from(Array(questionList.length), (e, i) => null),
  )
  const [radio, setRadio] = useState(defaultRadio)

  const audio = useRef(null)

  const checkPrimary = (i, j) => {
    if (
      mode.state === TEST_MODE.play &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--checked'
    return ''
  }

  const checkDanger = (i, j) => {
    if (
      mode.state === TEST_MODE.review &&
      userAnswer.status[i] === false &&
      radio[i] !== undefined &&
      radio[i] === j
    )
      return '--danger'
    return ''
  }

  const checkSuccess = (i, j) => {
    if (
      (mode.state === TEST_MODE.review &&
        userAnswer.status[i] === true &&
        radio[i] !== undefined &&
        radio[i] === j) ||
      (mode.state === TEST_MODE.check && trueAnswer[i] === j)
    )
      return '--success'
    return ''
  }

  const generateClassName = (i, j) => {
    let className = ''
    className += checkPrimary(i, j)
    className += ' '
    className += checkDanger(i, j)
    className += ' '
    className += checkSuccess(i, j)
    return className
  }

  const handleRadioClick = (i, j) => {
    let beforeArr = radio
    if (beforeArr[i] === undefined) return
    beforeArr[i] = j
    // check isCorrect
    let statusArr = []
    trueAnswer.forEach((item, i) =>
      statusArr.push(item === beforeArr[i] ? true : false),
    )
    // save progress
    setProgressActivityValue(beforeArr, statusArr)
    // display
    setRadio([...beforeArr])
  }

  const handleAudioTimeUpdate = () => {
    if (!audio?.current) return
    const countDowntValue = audio.current.duration - audio.current.currentTime
    if (countDowntValue > 0)
      setCountDown(formatDateTime(Math.ceil(countDowntValue) * 1000))
    else {
      setIsPLaying(false)
      audio.current.currentTime = 0
    }
  }
  //change question
  useEffect(() => {
    const defaultRadio = getProgressActivityValue(
      Array.from(Array(questionList.length), (e, i) => null),
    )
    setRadio(defaultRadio)
  }, [currentQuestion])
  useEffect(() => {
    if (!audio?.current) return
    if (isPlaying) audio.current.play()
    else audio.current.pause()
  }, [isPlaying, audio])

  return (
    <GameWrapper className="pt-o-game-11-audio">
      <audio
        ref={audio}
        style={{ display: 'none' }}
        onLoadedMetadata={() =>
          setCountDown(formatDateTime(Math.ceil(audio.current.duration) * 1000))
        }
        onTimeUpdate={() => handleAudioTimeUpdate()}
      >
        <source src={audioInstruction} />
      </audio>
      {isShowTapescript ? (
        <div className="__paper">
          <div className="__toggle" onClick={() => setIsShowTapescript(false)}>
            <CustomImage alt="close" src="/pt/images/icons/ic-plus.svg" />
          </div>
          <BlockPaper>
            <div className="__content">
              <FormatText>{audioScript}</FormatText>
            </div>
          </BlockPaper>
        </div>
      ) : (
        <>
          <BlockWave
            className={`pt-o-game-11-audio__left ${
              mode.state !== TEST_MODE.play ? 'pt-o-game-11-audio__result' : ''
            }`}
          >
            <AudioPlayer
              className="__player"
              isPlaying={isPlaying}
              setIsPlaying={() => setIsPLaying(!isPlaying)}
            />
            <span className="__duration">{countDown}</span>
          </BlockWave>
          <BlockBottomGradient className="pt-o-game-11-audio__right">
            {mode.state !== TEST_MODE.play && (
              <CustomButton
                className="__tapescript"
                onClick={() => setIsShowTapescript(true)}
              >
                Tapescript
              </CustomButton>
            )}
            <div className="__header">
              <CustomHeading tag="h6" className="__description">
                <FormatText tag="span">{instruction}</FormatText>
              </CustomHeading>
            </div>
            <div className="__content">
              <>
                {examplesList.length > 0 && (
                  <div className='__content_question_group --example'>
                    <div className='question_header'>
                      <span>Example</span>
                    </div>
                    <>
                      {examplesList.map((item, i) => (
                        <div key={`ex-${i}`} className="__radio-group">
                          <CustomHeading tag="h6" className="__radio-heading">
                          <span
                              dangerouslySetInnerHTML={{
                                __html: convertStrToHtml(
                                    item.question,
                                ).replaceAll(
                                    '%s%',
                                    `<div class="__space"></div>`,
                                ),
                              }}
                          ></span>
                          </CustomHeading>
                          <div className="__radio-list">
                            {item.answers.map((childItem, j) => (
                              <div
                              key={`ex-${i}_${j}`}
                              className={`mc56-a-multichoiceanswers answer`}
                            >
                              <div className="mc56-a-multichoiceanswers__answer">
                                <ul className="mc56-a-multichoiceanswers__input ">
                                  <li>
                                    <input
                                      type="checkbox"
                                      name={`mc56-id${i}`}
                                      className="option-input checkbox"
                                      defaultChecked={childItem.isCorrect}
                                      style={{ pointerEvents: 'none' }}
                                    />
                                    <label
                                      dangerouslySetInnerHTML={{
                                        __html: convertStrToHtml(
                                          childItem.text,
                                        ),
                                      }}
                                    ></label>
                                  </li>
                                </ul>
                              </div>
                            </div>
                            ))}
                          </div>
                        </div>
                      ))}
                    </>
                  </div>
                )}
                {questionList.length > 0 && (
                  <div className='__content_question_group'>
                    <div className='question_header'>
                      <span>
                        {questionList.length > 1 ? 'Questions' : 'Question'}
                      </span>
                    </div>
                    <div>
                      {questionList.map((item, i) => (
                        <div key={i} className="__radio-group">
                          <CustomHeading tag="h6" className="__radio-heading">
                          <span
                              dangerouslySetInnerHTML={{
                                __html: convertStrToHtml(
                                    item.question,
                                ).replaceAll(
                                    '%s%',
                                    `<div class="__space"></div>`,
                                ),
                              }}
                          ></span>
                          </CustomHeading>
                          <div className="__radio-list">
                            {item.answers.map((childItem, j) => (
                              <div
                                key={`${i}_${j}`}
                                className={`__radio-item ${generateClassName(
                                  i,
                                  j,
                                )}`}
                                style={{
                                  pointerEvents:
                                    mode.state === TEST_MODE.play
                                      ? 'all'
                                      : 'none',
                                }}
                                onClick={() =>
                                  mode.state === TEST_MODE.play &&
                                  handleRadioClick(i, j)
                                }
                              >
                                <label
                                    dangerouslySetInnerHTML={{
                                      __html: convertStrToHtml(
                                          childItem.text,
                                      ),
                                    }}
                                ></label>
                              </div>
                            ))}
                          </div>
                        </div>
                      ))}
                    </div>
                  </div>
                )}
              </>
            </div>
          </BlockBottomGradient>
        </>
      )}
    </GameWrapper>
  )
}
