import { useState } from 'react'
import { useMemo } from 'react'
import { Fragment, useContext, useEffect } from 'react'

import { questionListTransform } from '../../../api/dataTransform'
import { PageTestContext } from '../../../interfaces/contexts'
import { Game1 } from './games/game1'
import { Game1Image } from './games/game1/Game1Image'
import { Game10 } from './games/game10'
import { Game11 } from './games/game11'
import { Game11Audio } from './games/game11/Game11Audio'
import { Game12 } from './games/game12'
import { Game12Audio } from './games/game12/Game12Audio'
import { Game13 } from './games/game13'
import { Game14 } from './games/game14'
import { Game15 } from './games/game15'
import { Game15Audio } from './games/game15/Game15Audio'
import { Game2 } from './games/game2'
import { Game3 } from './games/game3'
import { Game3Audio } from './games/game3/Game3Audio'
import { Game4 } from './games/game4'
import { Game5 } from './games/game5'
import { Game6 } from './games/game6'
import { Game7 } from './games/game7'
import { Game8 } from './games/game8'
import { Game8Audio } from './games/game8/Game8Audio'
import { Game9 } from './games/game9'
import { Game9Audio } from './games/game9/Game9Audio'
import { GameDD2 } from './games/gameDD2'
import GameDrawLine from './games/gameDrawLine'
import GameDrawLine2 from './games/gameDrawLine2'
import GameDrawLine3 from './games/gameDrawLine3'
import GameDrawShape1 from './games/gameDrawShape1'
import { GameFB4_v2 } from './games/gameFB4_v2'
import { GameFB6 } from './games/gameFB6'
import { GameFB7 } from './games/gameFB7'
import { GameFB8 } from './games/gameFB8'
import GameFillInColour1 from './games/gameFillInColour1'
import GameFillInColour2 from './games/gameFillInColour2'
import { GameMC10 } from './games/gameMC10'
import { GameMC11 } from './games/gameMC11'
import { GameMC1_v2 } from './games/gameMC1_v2/index'
import { GameMC2 } from './games/gameMC2'
import { GameMC7 } from './games/gameMC7'
import { GameMC8 } from './games/gameMC8'
import { GameMC9 } from './games/gameMC9'
import { GameMG4 } from './games/gameMG4'
import { GameMG5 } from './games/gameMG5'
import { GameMG6 } from './games/gameMG6'
import { GameMix1 } from './games/gameMix1'
import { GameMix2 } from './games/gameMix2'
import GameSelectObject1 from './games/gameSelectObject1'
import { GameSL2 } from './games/gameSL2'
import { GameTF3 } from './games/gameTF3'
import GameTF4 from './games/gameTF4'
import { GameTF5 } from './games/gameTF5'

export const GamePicker = () => {
  const { currentPart, currentQuestion, partData } = useContext(PageTestContext)
  const questionList = questionListTransform(partData, currentPart.index);
  const [firstLoad, setFirstLoad] = useState(true)

  useEffect(()=>{
    if( firstLoad ){
      setFirstLoad(false)
      const currentArr = [0]
      if (questionList[0].group > 1)
        for (let i = 1; i < questionList[0].group; i++) currentArr.push(i)
      currentQuestion.setIndex(currentArr)
    }
  },[firstLoad])

  return (
    <div className="pt-o-game-picker">
      {questionList.map((item, i) => {
        // first item in arr currentQuestion
        if (currentQuestion.index[0] === i) {
          switch (item.activityType) {
            case 1:
              return item.imageInstruction ? (
                <Game1Image key={`pt-1img-question-${item.id}`} data={item} />
              ) : (
                <Game1 key={`pt-1-question-${item.id}`} data={item} />
              )
            case 2:
              return <Game2 key={`pt-game2-question-${item.id}`} data={item} />
            case 3:
              return item.audioInstruction ? (
                <Game3Audio key={`pt-game3-audio-question-${item.id}`} data={item} />
              ) : (
                item.questionInstruction ?
                <GameFB6 key={`pt-fb6-question-${item.id}`} data={item} />
                : <Game3 key={`pt-game3-question-${item.id}`} data={item} />
              )
            case 4:
              return <Game4 key={`pt-game4-question-${item.id}`} data={item} />
            case 5:
              return <Game5 key={`pt-game5-question-${item.id}`} data={item} />
            case 6:
              return <Game6 key={`pt-dd1-question-${item.id}`} data={item} />
            case 7:
              return <Game7 key={`pt-game7-question-${item.id}`} data={item} />
            case 8:
              return item.audioInstruction ? (
                <Game8Audio key={`pt-game8-audio-question-${item.id}`} data={item} />
              ) : item.imageInstruction ? (
                <GameTF4 key={`pt-tf4-question-${item.id}`} data={item}/>
              ) : (
                <Game8 key={`pt-game8--question-${item.id}`} data={item} />
              )
            case 9:
              return item.audioInstruction ? (
                <Game9Audio key={`pt-game9-audio-question-${item.id}`} data={item} />
              ) : (
                <Game9 key={`pt-game9-question-${item.id}`} data={item} />
              )
            case 10:
              return <Game10 key={`pt-game10-question-${item.id}`} data={item} />
            case 11:
              return item.audioInstruction ? (
                <Game11Audio key={`pt-game11-audio-question-${item.id}`} data={item} />
              ) : (
                <Game11 key={`pt-game11-question-${item.id}`} data={item} />
              )
            case 12:
              // return <Game12 key={i} data={item} />
              return item.audioInstruction ? (
                <Game12Audio key={`pt-mr-audio-question-${item.id}`} data={item} />
              ) : (
                <Game12 key={`pt-mr-question-${item.id}`} data={item} />
              )
            case 13:
              return <Game13 key={`pt-game13-question-${item.id}`} data={item} />
            case 14:
              return <Game14 key={`pt-game14-question-${item.id}`}  data={item} />
            case 15:
              return item.audioInstruction ? (
                <Game15Audio key={`pt-game15-audio-question-${item.id}`}  data={item} />
              ) : (
                <Game15 key={`pt-game15-question-${item.id}`} data={item} />
              )
            case 16:
              return <GameSL2 key={`pt-sl2-question-${item.id}`} data={item}/>
            case 17:
              return <GameMix1 key={`pt-mix1-question-${item.id}`} data={item}/>
            case 18:
              return <GameMix2 key={`pt-mix2-question-${item.id}`} data={item}/>
            case 19:
              return <GameTF3 key={`pt-tf3-question-${item.id}`} data={item}/>
            case 20: //MC1_v2
              return <GameMC1_v2 key={`pt-mc1_v2-question-${item.id}`} data={item} />
            case 21: //MC2_v2
              return <GameMC2 key={`pt-mc2-question-${item.id}`} data={item} />
            case 22:
              return <GameMC7 key={`pt-mc7-question-${item.id}`} data={item} />
            case 23: //MG4
              return <GameMG4 key={`pt-question-${item.id}`} data={item} />
            case 24: //MC9
              return <GameMC9 key={`pt-mc9-question-${item.id}`} data={item} />
            case 25: //FB4
              return <GameFB4_v2 key={`pt-fb4-question-${item.id}`} data={item} />
            case 26: //FB7 new
              return <GameFB7 key={`pt-fb7-question-${item.id}`} data={item} />
            case 27: //FB8 new
              return <GameFB8 key={`pt-fb8-question-${item.id}`} data={item} />
            case 28: 
              return <GameDrawLine key={`pt-draw-line-question-${item.id}`} data={item} />
            case 29: 
              return <GameTF5 key={`pt-tf5-question-${item.id}`} data={item}/>
            case 30:
              return <GameMC8 key={`pt-mc8-question-${item.id}`} data={item} />
            case 31:
              return <GameSelectObject1 key={`pt-so1-question-${item.id}`} data={item} />
            case 32:
              return <GameMC10 key={`pt-mc10-question-${item.id}`} data={item} />
            case 34:
              return <GameDrawLine2 key={`pt-draw-line2-question-${item.id}`} data={item} />
            case 36:
              return <GameDD2 key={`pt-dd2-question-${item.id}`} data={item} />
            case 37:
              return <GameMG5 key={`pt-matching5-question-${item.id}`} data={item} />
            case 38:
              return <GameDrawShape1 key={`pt-ds1-question-${item.id}`} data={item} />
            case 33:
              return <GameMG6 key={`pt-matching6-question-${item.id}`} data={item} />
            case 39:
              return <GameFillInColour1 key={`pt-fc1-question-${item.id}`} data={item} />
            case 40:
            return <GameDrawLine3 key={`pt-draw-line3-question-${item.id}`} data={item} />  
            case 41:
              return <GameMC11 key={`pt-mc11-question-${item.id}`} data={item} />
            case 42:
              return <GameFillInColour2 key={`pt-fc2-question-${item.id}`} data={item} />
            default:
              return <Fragment key={i}></Fragment>
          }
        } else return <Fragment key={i}></Fragment>
      })}
    </div>
  )
}
