import { Modal, Button} from 'rsuite'
import React from 'react'

type PropType = {
    className?: string
    isActive: boolean
    closeModal: () => void
}

export const ModalCopyLink = ({
    className = '',
    isActive,
    closeModal,
}: PropType) => {

    const arrImagesExample = [
    {
        url: '/images/instructions/extra1.png',
    },
    {
        url: '/images/instructions/extra2.png',
    },
    {
        url: '/images/instructions/extra3.png',
    },
    {
        url: '/images/instructions/extra4.png',
    },
    {
        url: '/images/instructions/extra5.png',
    },
    {
        url: '/images/instructions/extra6.png',
    },
    ]

    return (
    <Modal
        className={`copy-link-modal ${className} copy-link-modal-custom`}
        size="md"
        open={isActive}
        style={{
            overflow: 'hidden',
            // maxHeight: '100%',
            background: 'rgba(39,44,54,0.3)',
        }}
        onClose={closeModal}
        backdrop={false}
    >
        <Modal.Body style={{ overflow: 'hidden' }}>
            <div className="modal-header">
            {' '}
            Copy the result link success
                <Button className="modal-toggle" onClick={closeModal}>
                    <img src="/images/icons/ic-close-darker.png" alt="close" />
                </Button>
            </div>
            <div className="modal-sub-content">
                <div className="instruction-text">
                    Press ctrl+v on the keyboard to paste and send a result link to your
                    teacher
                </div>
                <Button className="accept-btn" onClick={closeModal}>
                    {' '}
                    OK
                </Button>
            </div>
        </Modal.Body>
    </Modal>
    )
}
