import { CSSProperties, useContext, useEffect, useState } from "react";

import dayjs from "dayjs";
import { signOut } from "next-auth/client";
import { useRouter } from "next/dist/client/router";
import { Button } from "rsuite";

import { paths, STATUS_CODE_EXAM, USER_ROLES } from "@/interfaces/constants";
import { paths as pathsApi } from "api/paths";
import { callApi } from "api/utils";
import { PageTestContext } from "../../../interfaces/contexts";
import useTranslation from "@/hooks/useTranslation";

type PropsType = {
    className?: string
    style?: CSSProperties
    gradeName?: string
    testName?: string
    testTime?: string
    onStart?: (data: any) => void
    user?: any
    setClassUnitTestInfo?: any
}

export const StartTestScreen = ({ className = "", style, gradeName, testName, testTime, onStart, user, setClassUnitTestInfo }: PropsType) => {
    const { t } = useTranslation()
    const { push, asPath, query, isReady } = useRouter();
    const { setTestInfo } = useContext(PageTestContext);

    const [infoTest, setInfoTest] = useState(null);
    const [isLoad, setIsLoad] = useState(false)
    const fetchUnitTestAssignInfo = async () => {
        try {
            setIsLoad(true)
            const response: any = await callApi(`${pathsApi.api_unit_test_assigned_info}/${query?.idAssign}`);
            setIsLoad(false)
            if (response.code === 200) {
                setInfoTest(response?.data);
                setClassUnitTestInfo(response?.data)
            }
        } catch (e) {
            console.log({ e });
        }
    };

    useEffect(() => {
        if (isReady && query?.idAssign) {
            fetchUnitTestAssignInfo().finally();
        }
    }, []);
    const onLogOut = async () => {
        await signOut({ callbackUrl: "/login", redirect: false });
        await push({
            pathname: paths.signIn,
            query: {
                next: asPath
            }
        });
    };
    const onClickBtn = (url: string) => {
        if (user) {
            if (!query?.idAssign) {
                onStart && onStart(user);
            } else {
                switch (user.user_role_id) {
                    case USER_ROLES.Student:
                        onStart && onStart(user);
                        break;
                    default:
                        document.querySelector("body").removeAttribute("id");
                        onLogOut();
                        break;
                }
            }
        } else {
            document.querySelector("body").removeAttribute("id");
            push({
                pathname: url,
                query: {
                    next: asPath
                }
            });
        }
    };

    const onPropBtn = () => {
        if (!user) {
            return (
                <div className="form-group">
                    <p className="instruction">{t('practice-test')['login-mess']}</p>
                    <div className="form-field form-button">
                        <Button
                            className="submit-btn"
                            appearance="primary"
                            color="blue"
                            disabled={false}
                            onClick={() => onClickBtn(paths.signIn)}
                        >
                            {t('sign-in-menu')}
                        </Button>
                    </div>
                    <div className="bottom-gradient" />
                </div>
            )
        }
        if (query?.idAssign && user.user_role_id !== USER_ROLES.Student) {
            return (
                <div className="form-group">
                    <p className="instruction">{t('practice-test')['login-student-mess']}</p>
                    <div className="form-field form-button">
                        <Button
                            className="submit-btn"
                            appearance="primary"
                            color="blue"
                            disabled={false}
                            onClick={() => onClickBtn(paths.signIn)}
                        >
                            {t('practice-test')['login-acc-student']}
                        </Button>
                    </div>
                    <div className="bottom-gradient" />
                </div>
            )
        }

        if(!infoTest && query?.idAssign) {
            return (
                <div className="form-group unit-test">
                    <p className="instruction">
                        {t('practice-test')['not-auth-access']}
                    </p>
                    <span className="des">
                        {t('practice-test')['not-auth-access-des']}
                    </span>
                    <div className="bottom-gradient" />
                </div>
            )
        }
        const isNotPractice = infoTest?.unit_type === 1 && infoTest?.lastSubmitted || infoTest?.examStatus !== STATUS_CODE_EXAM.ONGOING;
        const isExpired = infoTest?.examStatus === STATUS_CODE_EXAM.ENDED && infoTest.lastSubmitted === null;
        if (infoTest && isNotPractice) {
            return (
                <div className={`form-group ${isExpired ? "unit-test" : infoTest?.examStatus === STATUS_CODE_EXAM.UPCOMING ? "unit-test" : "expired"}`}>
                    <p className="instruction">
                        {isExpired ? t('practice-test')['expired-time'] :
                            infoTest?.examStatus === STATUS_CODE_EXAM.UPCOMING ?
                                t('practice-test')['up-coming-time'] :
                                t('practice-test')['submitted-time']
                        }
                    </p>
                    <span className="des">
                        {isExpired
                            ? t('practice-test')['not-access-to-limit']
                            : infoTest?.examStatus === STATUS_CODE_EXAM.UPCOMING ?
                                t(t('practice-test')['start-with-time'],[dayjs(infoTest?.start_date).format("HH:mm DD/MM/YYYY")]) :
                                t(t('practice-test')['submitted-with-time'],[dayjs(infoTest?.lastSubmitted?.created_date).format("DD/MM/YYYY HH:mm")])
                        }
                    </span>
                    <div className="bottom-gradient" />
                </div>
            )
        }
        if (infoTest && isExpired) {
            return (
                <div className="form-group unit-test">
                    <p className="instruction">
                        {t('practice-test')['expired-time']}
                    </p>
                    <span className="des">
                        {t('practice-test')['not-access-to-limit']}
                    </span>
                    <div className="bottom-gradient" />
                </div>
            )
        }
        return (
            <div className="form-group">
                <p className="instruction">{t('practice-test')['click-start']}</p>
                <div className="form-field form-button">
                    <Button
                        className="submit-btn"
                        appearance="primary"
                        color="blue"
                        disabled={false}
                        onClick={() => onClickBtn('')}
                    >
                        {t('practice-test')['get-started']}
                    </Button>
                </div>
                <div className="bottom-gradient" />
            </div>
        )
    }

    return (
        <div
            className={`m-start-test-screen ${className}`}
            style={style}
        >
            <div className="m-start-test-screen__container">
                <div className="header">
                    <img src="/images/practice-test/book.png" alt="lession" className="img-book" />
                    <div className="test-name">
                        <div className="test-title">{gradeName} - {testName}</div>
                        <div className="test-time">{t('table')['submit-time']}: {testTime} {t('time')['minutes']}</div>
                    </div>
                </div>
            </div>
            <div className="body">
                <div className="left">
                    <img src="/images/practice-test/test-start.png" alt="start-img" className="img-start" />
                </div>
                <div className="right">
                    {!isLoad && onPropBtn()}
                </div>
            </div>
        </div>
    );
};
