import { useContext, useState } from 'react'

import dayjs from "dayjs";
import Scrollbars from 'react-custom-scrollbars'

import { toFixedNumber } from "../../../../utils/number";
import { PageTestContext } from '../../../interfaces/contexts'
export const HistoryBoard = ({ data }) => {
  
  const { partData, progress, isOpenHistoryBoard } = useContext(PageTestContext)
  const dateSubmit = dayjs(data.created_date).format('DD/MM/YYYY | HH:mm')
  const dataHistory = {
    bookInfo: {
      name: data.book_name,
      submitedDate: dateSubmit,
    },
    studentInfo: {
      name: data.user_name,
      phone: data.user_phone,
      email: data.user_email
    },
  }
  // console.log('dasdsadasds adsad', partData)
  let skillData = [...partData]
  skillData.forEach((item) => {
    item.skill = item.list[0].list[0].skill
  })
  // console.log('dasdsadasds adsad1111')
  const countAnserPerSkill = (index) => {
    const value = {
      correctPoint: 0,
      totalPoint: 0,
    }
    const progressItem = progress.state[index]
    progressItem.forEach((item) => {
      value.totalPoint += item.point
      if (item?.status) {
        if (Array.isArray(item.status)) {
          let subPoint = 0
          const typeQuestion = item.activityType
          if (typeQuestion === 12){ //template MR
            const answerTotal = item.question.answers.filter(item => item.isCorrect).length
            if(!item?.status.includes(false) && item?.status.length === answerTotal){
              subPoint += item.point
            }
          }
          else{
            item.status.forEach((subItem) => {
              if (Array.isArray(subItem)) {
                subItem.forEach((subSub) => {
                  if (subSub) {
                    subPoint += item.point
                  }
                })
              } else {
                if (subItem) {
                  subPoint += item.point
                }
              }
            })
          }
          value.correctPoint += subPoint
        } else {
          value.correctPoint += item.point
        }
      }
    })
    return value
  }
  
  return (
    <div className={`pt-o-test-history-board__overlay-background ${
      isOpenHistoryBoard.state ? '--open' : ''
    }`}>
      <div
      className={`pt-o-test-history-board ${
        isOpenHistoryBoard.state ? '--open' : ''
      }`}
    >
      <div className="pt-o-test-history-board__container">
        <div className="__title">
          <span>EXAM RESULT:</span>
        </div>
        <div className="__content-info">
          <div className="__info-book">
            <img
              src="/pt/images/histories/ic-book.png"
              alt="book"
              className="img-book"
            />
            <div>
              <div className="name-book">{dataHistory.bookInfo.name}</div>
              {/* <div className="submit-date">
                {`Submited Date: ${dataHistory.bookInfo.submitedDate}`}
              </div> */}
            </div>
          </div>
          <div className="__info-student">
            <div>
              <div>
                <img alt="u" src="/pt/images/histories/ic-user-white.png"></img>
                <span>{`${dataHistory.studentInfo.name} Score:`}</span>
              </div>
             {dataHistory.studentInfo.phone && <div title={dataHistory.studentInfo.phone}>
                <img
                  alt="u"
                  src="/pt/images/histories/ic-phone-white.png"
                ></img>
                <span>{dataHistory.studentInfo.phone}</span>
              </div>}
              {dataHistory.studentInfo.email && <div title={dataHistory.studentInfo.email}>
                <img
                className='email'
                  alt="u"
                  src="/pt/images/histories/ic-email-white.png"
                ></img>
                <span>{dataHistory.studentInfo.email}</span>
              </div>} 
            </div>
            <div>
            <div>
                <div className="__total">
                  {toFixedNumber(data.point)}/
                  {data.max_point}
                </div>
              </div>
            </div>
            <div>

              <div>
                <div className="submit-date">
                  {`Submited Date: ${dataHistory.bookInfo.submitedDate}`}
                </div>
              </div>
            </div>
          </div>
          <div className="__info-test">
            <div className="__result">
              <div className="__label">
                <img alt="u" src="/pt/images/histories/ic-result.png"></img>
                <span>Result information:</span>
              </div>
            </div>
            <div className="__progress-list">
              {skillData &&
                skillData.map((item, i) => {
                  const countAnswer = countAnserPerSkill(i).correctPoint
                  return (
                    <div key={i} className="__progress-item">
                      <div className="__info">
                        <span className="__name">
                          <div className="__circle"></div>
                          {`Part ${i + 1}: ${item.name}`}
                        </span>
                        <div>
                          <span className='__dot'>....................................................................................................................................................................................</span>
                          <span className="__value">
                            {toFixedNumber(countAnswer)}/
                            {item.totalPoint}
                          </span>
                        </div>
                      </div>
                    </div>
                  )
                  {
                    /* } */
                  }
                })}
            </div>
          </div>
        </div>
        <div className="__content-toggle">
          <div
            onMouseDown={() => {
              event.preventDefault()
              isOpenHistoryBoard.setState(!isOpenHistoryBoard.state)
            }}
          >
            <img
              alt="t"
              className="__image-arrow"
              style={
                isOpenHistoryBoard.state
                  ? { transform: 'rotate(90deg)' }
                  : { transform: 'rotate(-90deg)' }
              }
              src="/images/icons/ic-right-ctrl.png"
            ></img>
          </div>
        </div>
      </div>
    </div>
    </div>
  )
}
