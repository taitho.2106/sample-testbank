import { useContext, useState } from 'react'

import Scrollbars from 'react-custom-scrollbars'

import { useWindowSize } from 'utils/hook'

import { questionListTransform } from '../../../api/dataTransform'
import usePagination from '../../../hook/usePagination'
import { COLOR_TEST_TYPE } from '../../../interfaces/constants'
import { PageTestContext } from '../../../interfaces/contexts'
import { CustomButton } from '../../atoms/button'
import { CustomHeading } from '../../atoms/heading'
import { CustomImage } from '../../atoms/image'
import { CustomText } from '../../atoms/text'

export const SummaryBoard = () => {
  const { paginate } = usePagination()

  const { currentPart, partData, progress, testInfo, isOpenSummaryBoard } =
    useContext(PageTestContext)
  const [width, height] = useWindowSize()
  const numberPerPage = width < 1024 ? 6 : 12
  const questionGroup = partData.map((item, i) => ({
    name: item.name,
    questionList: questionListTransform(partData, i),
  }))
  const checkDoneQuestionType6 = (i, j,selectorList, selectorGroup)=>{
    if (selectorGroup[0].group === 1) {
      if(
        selectorGroup[0].value !== null &&
        Array.isArray(selectorGroup[0].value) &&
        Array.isArray(selectorGroup[0].value[0]) &&
        selectorGroup[0].value[0].length > 0
      ){
        return true;
      }
      
    } else {
      const firstIndex = selectorList.findIndex(
        (item) => item.id === selectorGroup[0].id,
      )
      if(
        selectorList[firstIndex].value !== null &&
        Array.isArray(selectorList[firstIndex].value) &&
        selectorList[firstIndex].value[j - firstIndex] !== null &&
        Array.isArray(selectorList[firstIndex].value[j - firstIndex]) && 
        selectorList[firstIndex].value[j - firstIndex].length > 0
      ){
        return true;
      }
    }
    return false;
  }
  const checkDoneQuestionTypeDefault = (i, j,selectorList, selectorGroup,)=>{
    if (selectorGroup[0].group === 1) {
      if (selectorGroup[0].value !== null && selectorGroup[0].value !== ''){
        return true;
      }
    } else {
      const firstIndex = selectorList.findIndex(
        (item) => item.id === selectorGroup[0].id,
      )
      if (selectorList[firstIndex].value !== null &&
        selectorList[firstIndex].value !== '' &&
        Array.isArray(selectorList[firstIndex].value) &&
        selectorList[firstIndex].value[j - firstIndex] !== null &&
        selectorList[firstIndex].value[j - firstIndex] !== ''
      ){
        return true;
      }
    }
    return false;
  }
  const checkDoneQuestionTypeMG = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion &&  userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value) &&
      (userAnswerQuestion.value[indexSubQuestion]?.isMatched)
    ){
      return true;
    }
    return false;
  }
  const checkDoneQuestionTypeMR = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
      const valueItem = (userAnswerQuestion.value[indexSubQuestion]);
      if( valueItem || Number.isInteger(valueItem)){
        return true;
      }
    }
    return false;
  }
  const checkDoneQuestionTypeMIX1 = (i, j,selectorList, selectorGroup)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
        const itemArr = userAnswerQuestion.value;
        // part1 and 2: LS1, MC5
        const arr = [...itemArr[0]||[], ...itemArr[1]||[]];
        if(arr[indexSubQuestion] === false || arr[indexSubQuestion] === true){ //boolean
          return true;
        }
        if(Number.isInteger(arr[indexSubQuestion])){ //number.
          return true;
        }
    }
    return false;
  }
  const checkDoneQuestionTypeMIX2 = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
        const itemArr = userAnswerQuestion.value;
        // part1 and 2: TF2, MC5
        const arr = [...itemArr[0]||[], ...itemArr[1]||[]];
        if(arr[indexSubQuestion] || arr[indexSubQuestion] == false){ //boolean
          return true;
        }
    }
    return false;
  }
  const checkDoneQuestionTypeMC11 = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
        const itemArr = userAnswerQuestion.value;
        const subQuestion = itemArr[indexSubQuestion];
        if(subQuestion && Array.isArray(subQuestion) && subQuestion.some(m=>m?.value)){
          return true;
        }
    }
    return false;
  }
  const checkDoneQuestionTypeDD2 = (i, j,selectorList, selectorGroup,)=>{
    // selectorGroup index start in selector list
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
    )
    const userAnswerQuestion = selectorGroup[0];
    const indexSubQuestion = j - firstIndex;
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value))
      {
        const itemArr = userAnswerQuestion.value;
        const subQuestion = itemArr[indexSubQuestion];
        if(subQuestion && subQuestion.isMatched){
          return true;
        }
    }
    return false;
  }
  const checkDoneQuestionTypeDLFC = (i, j,selectorList, selectorGroup)=>{
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
      )
      const userAnswerQuestion = selectorList[firstIndex];
    if(
      userAnswerQuestion && userAnswerQuestion.value !== null &&
      Array.isArray(userAnswerQuestion.value) &&
      (userAnswerQuestion.value[j - firstIndex]?.isMatched  //DL
        || userAnswerQuestion.value[j - firstIndex]?.isSelected
        || userAnswerQuestion.value[j - firstIndex]?.color
        )
    ){
      return true;
    }
    return false;
  }
  const checkDoneQuestionTypeSO = (i, j,selectorList, selectorGroup)=>{
    const firstIndex = selectorList.findIndex(
      (item) => item.id === selectorGroup[0].id,
      )
      const userAnswerQuestion = selectorList[firstIndex];
      let userValue= userAnswerQuestion?.value || null;
      if(
        userValue &&
        Array.isArray(userValue)
      ){
       const userSelected = userValue.map(m=>m.isSelected);
       userSelected.sort((m,n)=>{
        if(m){
          return -1;
        }
        return 1;
       })
        if(userSelected[j - firstIndex]){
          return true;
        }
      }
    return false;
  }
  const checkDone = (i, j) => {
    let className = ''
    let currentQuestionType 



    const selectorList = progress.state[i]
    const selectorGroup = selectorList.filter(
      (item) => item.id === selectorList[j].id,
    )
    if (selectorGroup[0].id === questionGroup[i].questionList[j].id){
      currentQuestionType = questionGroup[i].questionList[j].activityType
    }
    if (selectorGroup.length > 0) {
      switch(currentQuestionType){
        case 6:
          if(checkDoneQuestionType6(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 28:

        case 34:
        case 39:
        case 40:
        case 42:
          if(checkDoneQuestionTypeDLFC(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 31:
          if(checkDoneQuestionTypeSO(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 9: 
        case 23: 
        case 33: 
        case 37:
          if(checkDoneQuestionTypeMG(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 12: 
          if(checkDoneQuestionTypeMR(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 17: 
          if(checkDoneQuestionTypeMIX1(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 18: 
          if(checkDoneQuestionTypeMIX2(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 41: 
          if(checkDoneQuestionTypeMC11(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        case 36: 
          if(checkDoneQuestionTypeDD2(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
        default:
          if(checkDoneQuestionTypeDefault(i,j,selectorList,selectorGroup)){
            className +="--done "
          }
          break;
      }
    }
    return className
  }

  const getDoneNumber = (i) => {
    let total = 0
    let currentQuestionType 
    const selectorList = progress.state[i]
    selectorList.forEach((item, j) => {
      const selectorGroup = selectorList.filter((e) => e.id === item.id)
      if (selectorGroup[0].id === questionGroup[i].questionList[j].id){
        currentQuestionType = questionGroup[i].questionList[j].activityType
      }
      if (selectorGroup.length > 0) {
        switch(currentQuestionType){
          case 6:
            if(checkDoneQuestionType6(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
          case 28:
          case 34:
          case 39:
          case 40:
          case 42:
            if(checkDoneQuestionTypeDLFC(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
          case 31:
            if(checkDoneQuestionTypeSO(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
          case 9: 
          case 23: 
          case 33: 
          case 37:
            if(checkDoneQuestionTypeMG(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
            case 12: 
            if(checkDoneQuestionTypeMR(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
          case 17: 
            if(checkDoneQuestionTypeMIX1(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
          case 18: 
            if(checkDoneQuestionTypeMIX2(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
          case 41: 
            if(checkDoneQuestionTypeMC11(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
          case 36: 
            if(checkDoneQuestionTypeDD2(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
          default:
            if(checkDoneQuestionTypeDefault(i,j,selectorList,selectorGroup)){
              total++
            }
            break;
        }
      }
    })
    return total
  }

  return (
    <div
      className={`pt-o-summary-board ${
        isOpenSummaryBoard.state ? '--open' : ''
      }`}
    >
      <div
        className="pt-o-summary-board__container"
        style={{ cursor: isOpenSummaryBoard.state ? 'default' : 'pointer' }}
        onClick={() =>
          !isOpenSummaryBoard.state && isOpenSummaryBoard.setState(true)
        }
      >
        <div
          className="__toggle"
          onClick={() => isOpenSummaryBoard.setState(!isOpenSummaryBoard.state)}
        >
          <CustomImage
            className="__image-bg"
            alt="Toggle background"
            src="/pt/images/backgrounds/bg-summary.png"
            yRate={0}
          />
          <CustomImage
            className="__image-icon"
            alt="Toggle icon"
            src={
              isOpenSummaryBoard.state
                ? '/pt/images/icons/ic-arrow-left.svg'
                : '/pt/images/icons/ic-menu.svg'
            }
          />
          <CustomHeading tag="h6" className="__heading">
            SUMMARY BOARD
          </CustomHeading>
        </div>
        <div className="__header">
          <CustomHeading tag="h6" className="__title">
            SUMMARY BOARD
          </CustomHeading>
          <div className="__info">
            <CustomImage
              className="__icon"
              alt="document"
              src={COLOR_TEST_TYPE[currentPart.index % 4]?.icon || ''}
              yRate={0}
            />
            <CustomHeading tag="h6" className="__title">
              {testInfo.gradeName} {testInfo.testName}
            </CustomHeading>
          </div>
        </div>
        <div className="__content">
          <Scrollbars universal={true}>
            <div className="__part-list">
              {questionGroup.map((item, i) => (
                <div key={i} className="__part-item">
                  <div className="__top">
                    <CustomHeading tag="h6" className="__name">
                      Part {i + 1}: <span>{item.name}</span>
                    </CustomHeading>
                    <div className="__total">
                      <CustomText tag="span">
                        {getDoneNumber(i)}/{item.questionList.length}
                      </CustomText>
                    </div>
                  </div>
                  <div className="__bottom">
                    {item.questionList.map((e, j) => (
                      <CustomButton
                        key={j}
                        className={`__btn ${checkDone(i, j)}`}
                        onClick={() => {
                          if (screen.width < 1199)
                            isOpenSummaryBoard.setState(false)
                          paginate(questionGroup[i].questionList, [j], i,numberPerPage)
                        }}
                      >
                        {j + 1}
                      </CustomButton>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </Scrollbars>
        </div>
      </div>
    </div>
  )
}
