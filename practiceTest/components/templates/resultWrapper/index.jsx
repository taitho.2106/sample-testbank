// @ts-nocheck
import React , { useContext, useEffect, useState } from 'react'

import { useRouter } from 'next/router'
import { Progress } from 'rsuite'

import { paths } from 'api/paths'
import api_method from 'lib/api'
import ga from "@/utils/ga";

import { COLOR_RESULT_PROGRESS } from '../../../interfaces/constants'
import { PageTestContext } from '../../../interfaces/contexts'
import { CustomButton } from '../../atoms/button'
import { CustomHeading } from '../../atoms/heading'
import { CustomImage } from '../../atoms/image'
import { CustomText } from '../../atoms/text'
import { Fireworks } from '../../molecules/fireworks'
import { ModalCopyLink } from '../../organisms/modalCopyLink'
import { useSession } from "next-auth/client";
import useTranslation from "../../../../hooks/useTranslation";

export const ResultWrapper = ({ data, classUnitTestInfo }) => {
  const { locale, subPath } = useTranslation()
  let skillData = data
  skillData.forEach((item) => {
    item.skill = item.list[0].list[0].skill
  })
  const [dataCopyResultUrl, setDataCopyResultUrl] = useState(null)
  const router = useRouter()
  const [session] = useSession()
  const user = session?.user
  // const gradeStringId = router?.query?.gradeId ? router.query.gradeId : ''
  const testStringId = router?.query?.testId ? router.query.testId : ''
  // const testId = hashId(testStringId.replace('test-', ''), 'test', false)

  const { mode, progress, testInfo, partData, studentInfo, thirdPartyData } =
    useContext(PageTestContext)
  const countAnswerPerSkill = (index) => {
    const value = {
      correctPoint: 0,
      totalPoint: 0,
    }
    const progressItem = progress.state[index]
    progressItem.forEach((item, i) => {
      value.totalPoint += item.point
      if (item?.status) {
        if (Array.isArray(item.status)) {
          let subPoint = 0
          const typeQuestion = item.activityType
          if (typeQuestion === 12){ //template MR
            const answerTotal = item.question.answers.filter(item => item.isCorrect).length
            if(!item?.status.includes(false) && item?.status.length === answerTotal){
              subPoint += item.point
            }
          }
          else{
            item.status.forEach((subItem) => {
              if (Array.isArray(subItem)) {
                subItem.forEach((subSub) => {
                  if (subSub) {
                    subPoint += item.point
                  }
                })
              } else {
                if (subItem) {
                  subPoint += item.point
                }
              }
            })
          }
          value.correctPoint += subPoint
        } else {
          value.correctPoint += item.point
        }
      }
    })
    return value
  }

  const countTotalAnswer = () => {
    const progressTotal = progress.state
    const value = {
      correctPoint: 0,
      totalPoint: 0,
    }
    progressTotal.forEach((progressItem, index) => {
      value.totalPoint += skillData[index].totalPoint
      if (Array.isArray(progressItem)) {
        progressItem.forEach((item, i) => {
          if (item?.status) {
            if (Array.isArray(item.status)) {
              let subPoint = 0
              const typeQuestion = item.activityType
              if (typeQuestion === 12){ //template MR
                const answerTotal = item.question.answers.filter(item => item.isCorrect).length
                if(!item?.status.includes(false) && item?.status.length === answerTotal){
                  subPoint += item.point
                }
              }
              else{
                item.status.forEach((subItem) => {
                  if (Array.isArray(subItem)) {
                    subItem.forEach((subSub) => {
                      if (subSub) {
                        subPoint += item.point
                      }
                    })
                  } else {
                    if (subItem) {
                      subPoint += item.point
                    }
                  }
                })
              }
              value.correctPoint += subPoint
            } else {
              value.correctPoint += item.point
            }
          }
        })
      }
    })
    return value
  }
  let tempData = {}
  const submitResult = async () => {
    const isSubmit = await saveDataResult()
    if (!thirdPartyData && isSubmit) {
      const userQuery = router?.query?.practiceTestSlug
      const class_unit_test_id = router.query?.idAssign
      const teacherId = userQuery.split('===')[0]
      const formData = { ...studentInfo }
      formData.teacherId = teacherId
      formData.point = `${
        Math.round(countTotalAnswer().correctPoint * 100) / 100
      } / ${Math.round(countTotalAnswer().totalPoint)}`
      formData.testName = testInfo?.testName
      formData.gradeName = testInfo?.gradeName
      formData.historyURL = getShareUrl(tempData?.unit_test_result_id, tempData?.unit_test_id,)
      formData.class_unit_test_id = +class_unit_test_id
      formData.name = user?.full_name
      formData.email = user?.email || ''
      formData.class = classUnitTestInfo?.class?.name || ''
      if (teacherId && user?.id !== teacherId) {
        await api_method.post(paths.api_send_email_result, formData)
      }
    }
  }
  const saveDataResult = async () => {
    let dataSend = {
      unit_test_id: testInfo.id,
      point: 0,
      max_point: 0,
      third_party_code: thirdPartyData?.code,
      third_party_device_id: thirdPartyData?.info?.DeviceId,
      third_party_user_id: thirdPartyData?.info?.User_Id,
      user_name: studentInfo?.name ? studentInfo?.name : null,
      user_class: studentInfo?.class ? studentInfo.class : null,
      user_email: studentInfo?.email ? studentInfo.email : null,
      sections: [],
      class_unit_test_id: +router.query?.idAssign
    }

    const progressTotal = progress.state
   
    progressTotal.forEach((progressItem, index) => {
      const section = {
        unit_test_section_id: partData[index]?.id,
        section_name: partData[index]?.name,
        max_point: partData[index]?.totalPoint,
        point: 0,
        questions: [],
      }

      // list questionid added to section.
      const listQuestionId = []

      if (Array.isArray(progressItem)) {
        // list question
        progressItem.forEach((item) => {
          const question = {
            question_id: item.id,
            total_question: item.group,
            point: 0,
            max_point: item.group * item.point,
            user_answer: null,
          }
          if (item?.status) {
            if (Array.isArray(item.status)) {
              let subPoint = 0
              const typeQuestion = item.activityType
              if (typeQuestion === 12){ //template MR
                const answerTotal = item.question.answers.filter(item => item.isCorrect).length
                if(!item?.status.includes(false) && item?.status.length === answerTotal){
                  subPoint += item.point
                }
              }
              else{
                item.status.forEach((subItem) => {
                  if (Array.isArray(subItem)) {
                    subItem.forEach((subSub) => {
                      if (subSub) {
                        subPoint += item.point
                      }
                    })
                  } else {
                    if (subItem) {
                      subPoint += item.point
                    }
                  }
                })}
              question.point += subPoint
            } else {
              question.point += item.point
            }
          }
          // add question to section
          if (listQuestionId.indexOf(item.id) == -1) {
            listQuestionId.push(item.id)
            question.user_answer = JSON.stringify({
              status: item.status,
              value: item.value,
            })
            section.questions.push(question)
            section.point += question.point
          }
        })
      }
      //add section to data send.
      dataSend.sections.push(section)
      dataSend.max_point += section.max_point
      dataSend.point += section.point
    })
    // console.log("data send@@@@", dataSend)

    const res = await api_method.post(paths.api_unit_test_submit, dataSend)
    if (res && res.status == 200 && res.data.code === 200) {
      let dataResult = {}
      dataResult.unit_test_id = dataSend.unit_test_id;
      dataResult.unit_test_result_id = res.data.data.id
      tempData = dataResult
      setDataCopyResultUrl(dataResult)
      ga.unitTestUse({
        id: testInfo?.id,
        name: testInfo?.testName,
      });
      return true
    }
    return false
  }
  useEffect(() => submitResult(), [])
  const [showModalCopy, setShowModalCopy] = useState(false)
  const execCommandCopy = (message) => {
    let textArea = document.createElement("textarea");
        textArea.value = message;
        // make the textarea out of viewport
        textArea.style.position = "fixed";
        textArea.style.left = "-999999px";
        textArea.style.top = "-999999px";
        document.body.appendChild(textArea);
        textArea.focus();
        textArea.select();
        textArea.addEventListener("copy",function(ev){
          console.log("ev.target.value",ev.target.value);
          ev.clipboardData.setData('text/plain', ev.target.value);
          ev.preventDefault();
          textArea.remove();
        })
         
        return new Promise((res, rej) => {
            // here the magic happens
            document.execCommand('copy') ? res() : rej();
        });
  }
  const handleCopyURL = (unit_test_result_id, unit_test_id) => {
    const protocol = window.location.protocol
    const hostname = window.location.hostname
    const port = hostname === 'localhost' ? `:${window.location.port}` : ''
    const url = `${protocol}//${hostname}${port}${subPath}/practice-test/result===${unit_test_id}===${unit_test_result_id}`
    if (navigator?.clipboard?.writeText) {
      navigator.clipboard.writeText(url).then(
        () => {
          console.log('copy success')
        },
        () => {
          console.log('copy error')
          execCommandCopy(url).then(()=> console.log("success")).catch(()=> console.log("error"))
        },
      )
    } else {
      console.log("start copy---2",url)
      execCommandCopy(url).then(()=> console.log("success")).catch(()=> console.log("error"))
    }
  }

  const getShareUrl = (unit_test_result_id, unit_test_id) => {
    const protocol = window.location.protocol
    const hostname = window.location.hostname
    const port = hostname === 'localhost' ? `:${window.location.port}` : ''
    return `${protocol}//${hostname}${port}${subPath}/practice-test/result===${unit_test_id}===${unit_test_result_id}`
  }

  return (
    <>
      <div className="pt-t-result-wrapper" data-animate="fade-in">
        <div className="pt-t-result-wrapper__container">
          <div className="__header">
            <Fireworks />
            <div className='box-header'>
              <CustomImage
                className="__medal"
                alt="medal"
                src="/pt/images/collections/clt-medal.png"
                yRate={0}
              />
              <div className='content-header'>
                <CustomHeading tag="h4" className="__heading">
                  {`${testInfo.gradeName} - ${testInfo.testName}`}
                </CustomHeading>
                <CustomText tag="p" className="__description">
                  Congratulations on completing the test! Here are the results:
                </CustomText>
              </div>
            </div>
          </div>
          <div className="__content">
            {dataCopyResultUrl && (
              <div className="__third-party-link">
                <CustomText tag="p" className="__text-info">
                  COPY & SEND THIS RESULT LINK TO YOUR TEACHER
                </CustomText>
                <a
                  className="__url-link"
                  href={getShareUrl(
                    dataCopyResultUrl?.unit_test_result_id,
                    dataCopyResultUrl?.unit_test_id,
                  )}
                >
                  {getShareUrl(
                    dataCopyResultUrl?.unit_test_result_id,
                    dataCopyResultUrl?.unit_test_id,
                  )}
                </a>
                <CustomButton
                  className="__btn-copy-url"
                  appearance="primary"
                  onClick={() => {
                    setShowModalCopy(true)
                    handleCopyURL(
                      dataCopyResultUrl?.unit_test_result_id,
                      dataCopyResultUrl?.unit_test_id,
                    )
                  }}
                >
                  Copy the result link
                </CustomButton>
              </div>
            )}
            <div className="__total">
              Total: {Math.round(countTotalAnswer().correctPoint * 100) / 100}/
              {Math.round(countTotalAnswer().totalPoint)}
            </div>
            <div className="__progress-list">
              {skillData &&
                skillData.map((item, i) => {
                  const countAnswer = countAnswerPerSkill(i).correctPoint
                  return (
                    <div key={i} className="__progress-item">
                      <div className="__info">
                        <label className="__label">{item.name}</label>
                        <span className="__value">
                          {Math.round(countAnswer * 100) / 100}/
                          {item.totalPoint}
                        </span>
                      </div>
                      <Progress.Line
                        className="__filter"
                        percent={(countAnswer / item.totalPoint) * 100}
                        showInfo={false}
                        strokeColor={
                          COLOR_RESULT_PROGRESS[
                            i % COLOR_RESULT_PROGRESS.length
                          ]
                        }
                      />
                    </div>
                  )
                  {
                    /* } */
                  }
                })}
            </div>
          </div>
          <div className="__footer">
            {/* <CustomButton
            className="__btn --cancel"
            appearance="ghost"
            onClick={() => router.push('/[gradeId]', `/${gradeStringId}`)}
          >
            Back to menu
          </CustomButton> */}
            <CustomButton
              className="__btn --submit"
              appearance="primary"
              onClick={() => mode.setState('review')}
            >
              View the answer
            </CustomButton>
          </div>
        </div>
      </div>
      <ModalCopyLink
        isActive={showModalCopy}
        closeModal={() => setShowModalCopy(false)}
      />
    </>
  )
}
