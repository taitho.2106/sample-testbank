import { BLOCK_GAME_TYPE } from '../../../interfaces/types'
import { CenterWrapper } from '../../templates/centerWrapper'

export const GameWrapper = ({ className = '', style, children }) => {

  return (
    <CenterWrapper className={`pt-t-game-wrapper ${className}`} style={style}>
      {children}
    </CenterWrapper>
  )
}

GameWrapper.propTypes = BLOCK_GAME_TYPE
