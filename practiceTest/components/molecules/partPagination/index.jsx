import { useEffect, useRef } from 'react'
import { useMemo } from 'react'
import { useContext, useState } from 'react'

import { useWindowSize } from 'utils/hook'

import { questionListTransform } from '../../../api/dataTransform'
import usePagination from '../../../hook/usePagination'
import { TEST_MODE } from '../../../interfaces/constants'
import { PageTestContext } from '../../../interfaces/contexts'
import { CustomButton } from '../../atoms/button'
import { CustomImage } from '../../atoms/image'

export const PartPagination = () => {
  useEffect(()=>{
    if(modePre.current !== TEST_MODE.play){
      paginate(questionList, [0], null, numberPerPage)
    }
  },[modePre])
  const { currentGroupBtn, currentGroupBtn6, currentPart, partData,mode } =
    useContext(PageTestContext)
  const modePre = useRef(mode?.state)
  const questionList = questionListTransform(partData, currentPart.index)

  const [width, height] = useWindowSize()
  const numberPerPage = width < 1024 ? 6 : 12
  const { getNumberBtnState, paginate } = usePagination() 
  
  const handleNextGroupBtnClick = () => {
    if(numberPerPage === 12){
      currentGroupBtn.setIndex(currentGroupBtn.index + 1)
    }
    if(numberPerPage === 6){
      currentGroupBtn6.setIndex(currentGroupBtn6.index + 1)
    }
  }

  const handlePrevGroupBtnClick = () => {
    if(numberPerPage === 6){
      currentGroupBtn6.setIndex(currentGroupBtn6.index - 1)
    }
    if(numberPerPage === 12){
      currentGroupBtn.setIndex(currentGroupBtn.index - 1)
    }
  }
  const renderClassPrev = () => {
    let className = ''
    if(numberPerPage === 12){
      if(questionList.length <= numberPerPage || currentGroupBtn.index === 0 ){
        className = '--disabled'
      }else{
        className = ''
      }
    }
    if(numberPerPage === 6){
      if(questionList.length <= numberPerPage || currentGroupBtn6.index === 0 ){
        className = '--disabled'
      }else{
        className = ''
      }
  }
  return className
}
  const renderClassNext = () =>{
    let className = ''
    if(numberPerPage === 12){
      if(questionList.length <= numberPerPage ||
        currentGroupBtn.index === Math.ceil(questionList.length / 12) - 1 ){
        className = '--disabled'
      }else{
        className = ''
      }
    }
    if(numberPerPage === 6){
      if(questionList.length <= numberPerPage ||
        currentGroupBtn6.index === Math.ceil(questionList.length / 6) - 1 ){
        className = '--disabled'
      }else{
        className = ''
      }
  }
  return className   
}
const classSizeButton = questionList.length>=100?"--lg":""
  return (
    <div className="pt-m-part-pagination">
      <div className="__prev">
        <CustomButton
          className={renderClassPrev()}
          onClick={() => handlePrevGroupBtnClick()}
        >
          <CustomImage
            alt="prev arrow"
            src="/pt/images/icons/ic-long-arrow-alt-left-blue.svg"
          />
        </CustomButton>
      </div>
      <div className="__list">
        {Array.from(Array(questionList.length), (e, i) => {
          if(numberPerPage === 12){
            if (
              i < numberPerPage * currentGroupBtn.index ||
              i >= numberPerPage * (currentGroupBtn.index + 1)
            ) {
              return null
            }
            return (
              <CustomButton
                key={i}
                className={`${getNumberBtnState(i, numberPerPage)} ${classSizeButton}`}
                onClick={() => paginate(questionList, [i], null, numberPerPage)}
              >
                {i + 1}
              </CustomButton>
            )
          }else{
            if (
              i < numberPerPage * currentGroupBtn6.index ||
              i >= numberPerPage * (currentGroupBtn6.index + 1)
            ) {
              return null
            }
            return (
              <CustomButton
                key={i}
                className={`${getNumberBtnState(i, numberPerPage)} ${classSizeButton}`}
                onClick={() => paginate(questionList, [i], null, numberPerPage)}
              >
                {i + 1}
              </CustomButton>
            )
          } 
        })}
      </div>
      <div className="__next">
        <CustomButton
          className={renderClassNext()}
          onClick={() => handleNextGroupBtnClick()}
        >
          <CustomImage
            alt="next arrow"
            src="/pt/images/icons/ic-long-arrow-alt-left-blue.svg"
          />
        </CustomButton>
      </div>
    </div>
  )
}
