import { useContext, useEffect } from 'react'

import { useWindowSize } from 'utils/hook'

import { questionListTransform } from '../../../api/dataTransform'
import usePagination from '../../../hook/usePagination'
import { PageTestContext } from '../../../interfaces/contexts'
import { CustomButton } from '../../atoms/button'
import { CustomImage } from '../../atoms/image'

export const PartDirection = () => {
  const {
    currentGroupBtn,
    currentGroupBtn6,
    currentPart,
    currentQuestion,
    partData,
  } = useContext(PageTestContext)
  const { getDirectionBtnState, paginate } = usePagination()

  const [width, height] = useWindowSize()
  const numberPerPage = width < 1024 ? 6 : 12

  const questionList = questionListTransform(partData, currentPart.index)

  const handleNextBtnClick = () => {
    // next part
    if (currentQuestion.index.includes(questionList.length - 1)) {
      if (currentPart.index >= partData.length - 1) return
      currentPart.setIndex(() => currentPart.index + 1)
      currentGroupBtn.setIndex(0)
      currentGroupBtn6.setIndex(0)
      paginate(questionListTransform(partData, currentPart.index + 1), [0])
      return
    }

    const target = currentQuestion.index[currentQuestion.index.length - 1] + 1
    // next group
    // if (currentQuestion.index[currentQuestion.index.length - 1] % numberPerPage >= numberPerPage -1) {
    //   if (currentQuestion.index && (questionList.length > (currentGroupBtn.index + 1) * numberPerPage)) {
    //     currentGroupBtn.setIndex(currentGroupBtn.index + 1)
    //   }
    // }
    if (target >= numberPerPage * (currentGroupBtn.index + 1)) {
      const groupIndex = Math.ceil((target + 1) / 12) - 1
      currentGroupBtn.setIndex(() => groupIndex)
    }

    if (target >= numberPerPage * (currentGroupBtn6.index + 1)) {
      const groupIndex = Math.ceil((target + 1) / 6) - 1
      currentGroupBtn6.setIndex(() => groupIndex)
    }
    paginate(questionList, target, null, numberPerPage)
  }

  const handlePrevBtnClick = () => {
    // previous part
    if (currentQuestion.index.includes(0)) {
      if (currentPart.index !== 0) {
        currentPart.setIndex(currentPart.index - 1)
        paginate(questionListTransform(partData, currentPart.index - 1), [0])
      }
      return
    }

    const target = currentQuestion.index[0] - 1

    // prev group
    if (
      target <= numberPerPage * (currentGroupBtn.index + 1) &&
      target >= numberPerPage &&
      currentQuestion.index.find(
        (m) => m[0] + 1 <= numberPerPage * (currentGroupBtn.index + 1),
      )
    ) {
      currentGroupBtn.setIndex(currentGroupBtn.index - 1)
    }

    if (
      target <= numberPerPage * (currentGroupBtn6.index + 1) &&
      target >= numberPerPage &&
      currentQuestion.index.find(
        (m) => m[0] + 1 <= numberPerPage * (currentGroupBtn6.index + 1),
      )
    ) {
      currentGroupBtn6.setIndex(currentGroupBtn6.index - 1)
    }
    // paginate
    paginate(questionList, target, null, numberPerPage)
  }

  const handleKeyPress = (e) => {
    switch (e.keyCode) {
      case 37:
         //trigger blur element focused();
         let focusedPre = document.activeElement
         if (!focusedPre || focusedPre == document.body) focusedPre = null
         else if (document.querySelector)
         focusedPre = document.querySelector(':focus')
         focusedPre && focusedPre.blur()
        handlePrevBtnClick()
        break
      case 39:
        //trigger blur element focused();
        let focusedNext = document.activeElement
        if (!focusedNext || focusedNext == document.body) focusedNext = null
        else if (document.querySelector)
          focusedNext = document.querySelector(':focus')
        focusedNext && focusedNext.blur()
        handleNextBtnClick()
        break
      default:
        break
    }
  }

  useEffect(() => {
    window.addEventListener('keyup', handleKeyPress)
    return () => window.removeEventListener('keyup', handleKeyPress)
  })

  return (
    <div className="pt-m-part-direction">
      <CustomButton
        className={`__prev ${getDirectionBtnState(
          'prev',
          questionList.length,
        )}`}
        onClick={() => handlePrevBtnClick()}
      >
        {currentPart.index > 0 && currentQuestion.index.includes(0) ? (
          <span>Previous Part</span>
        ) : (
          <CustomImage
            alt="chevron bar left"
            src="/pt/images/icons/ic-chevron-bar-left.svg"
          />
        )}
      </CustomButton>
      <CustomButton
        className={`__next ${getDirectionBtnState(
          'next',
          questionList.length,
        )}`}
        onClick={() => handleNextBtnClick()}
      >
        {currentQuestion.index.includes(questionList.length - 1) &&
        (currentPart.index < partData.length - 1 ||
          !currentQuestion.index.includes(questionList.length - 1)) ? (
          <span>Next Part</span>
        ) : (
          <CustomImage
            alt="chevron bar left"
            src="/pt/images/icons/ic-chevron-bar-left.svg"
          />
        )}
      </CustomButton>
    </div>
  )
}
