import mysql from 'serverless-mysql'

export const db = mysql({
  config: {
    host: process.env.NEXT_PUBLIC_MYSQL_HOST,
    database: process.env.NEXT_PUBLIC_MYSQL_DATABASE,
    user: process.env.NEXT_PUBLIC_MYSQL_USER,
    password: process.env.NEXT_PUBLIC_MYSQL_PASSWORD,
    port: parseInt(process.env.NEXT_PUBLIC_MYSQL_PORT),
    timezone: 'utc'
  },
})

export async function query<T>(q: string, values: any[] = []) {
  try {
    const results = await db.query<T>(q,values)
    await db.end()
    return results
  } catch (e: any) {
    throw Error(e.message)
  }
}

export async function queryWithTransaction<T>(listQ: any[]) {
  try {
    let tran = db.transaction()
    for (const q of listQ) {
      tran = tran.query(q)
    }
    const result = await tran
      .rollback((e: any) => {
        console.log(e)
      })
      .commit()
    await db.end()
    return result
  } catch (e: any) {
    throw Error(e.message)
  }
}
export function escapeSearchString(str:string){
  return stringEscape(str)
 return str.replaceAll('%','\\%')
  .replaceAll('_','\\_').replaceAll('"','\\"').replaceAll("'","\\'").replaceAll("\\","\\\\")
}
function stringEscape(s:string) {
  return s ? s.replace(/\\/g,'\%').replace(/\%/g,'\\%').replace(/\_/g,'\\_').replace(/\n/g,'\\n').replace(/\t/g,'\\t').replace(/\v/g,'\\v').replace(/'/g,"\\'").replace(/"/g,'\\"').replace(/[\x00-\x1F\x80-\x9F]/g,hex) : s;
  function hex(c:string) { const v = '0'+c.charCodeAt(0).toString(16); return '\\x'+v.substr(v.length-2); }
}