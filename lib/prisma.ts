// ts-ignore 7017 is used to ignore the error that the global object is not
// defined in the global scope. This is because the global object is only
// defined in the global scope in Node.js and not in the browser.

import { PrismaClient, Prisma } from '@prisma/client'

// PrismaClient is attached to the `global` object in development to prevent
// exhausting your database connection limit.
//
// Learn more:
// https://pris.ly/d/help/next-js-best-practices

const globalForPrisma = global as unknown as { prisma: PrismaClient }

export const prisma = globalForPrisma.prisma || new PrismaClient({
    log: [
        {
            emit: "event",
            level: "query",
        },
    ],
})

// prisma.$on("query" as any, async (e: any) => {
//     console.log(`${e.query} ${e.params}`)
// });

if (process.env.NODE_ENV !== 'production') globalForPrisma.prisma = prisma

const prismaJoin = Prisma.join;
const prismaRaw = Prisma.raw;

export { prismaJoin, prismaRaw };

export default prisma