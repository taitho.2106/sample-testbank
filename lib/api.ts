import axios, { AxiosError } from 'axios'

export const post = async (url: string, data: any, config?: any) => {
  try {
    const res = await axios.post(url, data, config)
    return res
  } catch (err) {
    return (err as AxiosError)?.response
  }
}
export const get = async (url: string, config?: any) => {
    try {
      const res = await axios.get(url,config)
      return res
    } catch (err) {
      return (err as AxiosError)?.response
    }
  }
  export const put = async (url: string, data: any, config?: any) => {
    try {
      const res = await axios.put(url, data, config)
      return res
    } catch (err) {
      return (err as AxiosError)?.response
    }
  }
const api = {get,post,put}
export default api;

