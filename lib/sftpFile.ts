import Client from 'ssh2-sftp-client';

import {USER_GUIDE_VIDEO_SFTP_PATH} from '../interfaces/constants'
const config:any = {
  host: process.env.NEXT_PUBLIC_HOST_SFTP,
  port: process.env.NEXT_PUBLIC_PORT_SFTP,
  username: process.env.NEXT_PUBLIC_USERNAME_SFTP,
  password: process.env.NEXT_PUBLIC_PASSWORD_SFTP,
  readyTimeout: 10000,
};

//upload file to ftps
export const UploadFileSftp=(file:any)=>{
    const client = new Client;
    return new Promise(function(resolve, reject) {
        client.connect(config)
        .then(async() => {
          const remotePath = USER_GUIDE_VIDEO_SFTP_PATH+"/"+file.originalFilename;
          await client.fastPut(file.filepath, remotePath);
         
          //Returns the attributes associated with the object pointed to by remotePath
          const stats = await client.stat(remotePath);
          
          //compare file before uploaded
          if(stats && file && stats.size == file.size){
              return process.env.NEXT_PUBLIC_DOMAIN_MEDIA+remotePath;
          }
          client.end();
          throw new Error("Upload file error. File size is not correct");
        })
        .then((result) => {
            client.end();
            resolve({status:true, response:result});
        })
        .catch(err => {
          resolve({status:false, message:err});
        });
      });
}