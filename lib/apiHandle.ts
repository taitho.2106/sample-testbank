import { initResource } from "@/hooks/useTranslation";
import { USER_ROLES } from "@/interfaces/constants";
import { NextApiRequest, NextApiResponse } from "next";
import { getSession } from "next-auth/client";
import { getLastPackage } from "services/package";
import { paginationQuerySchema } from "validations";
import { Schema, ValidationError } from "yup";
import { toNum } from "../utils";

const roleConfig = Object.freeze({
    admin: -1, // custom
    operator: USER_ROLES.Operator,
    teacher: USER_ROLES.Teacher,
    student: USER_ROLES.Student,
});

type HandlerConfig = {
    handler: (req: NextApiRequest, res: NextApiResponse, session?: any, paginationData?: any, additional?: any) => any
    authRequired?: boolean
    accessRoles?: number[]
    bodySchema?: any,
    querySchema?: Schema,
    pageable?: boolean,
    requireServicePackage?: string[],
}

type HandlerConfigGroup = {
    [index: string]: HandlerConfig
}

const getYupError = (error: any) => {
    if (error instanceof ValidationError) {
        return error.inner.map((el: any) => ({ field: el.path, message: el.errors }));
    }

    return [];
}

const apiResponseDTO = (isSuccess: boolean, data: any, message = 'Success', code = 200, errors?: any) => ({
    result: isSuccess,
    message,
    data,
    code,
    errors,
    serverTime: new Date(),
});

const errorResponseDTO = (code: string | number, message: string, errors?: any) => ({
    result: false,
    code,
    message,
    errors,
});

const validateInputData = async (res: NextApiResponse, schema: Schema, data: any) => {
    try {
        await schema.validate(data, { abortEarly: false });
    } catch (error) {
        console.log(error);
        const formError = getYupError(error);

        res.status(400).json(errorResponseDTO(400, 'Form error', formError));
        return false;
    }

    return true;
}

const paginationData = (req: NextApiRequest) => {
    const { page, size }: any = req.query;
    const pageNum = toNum(page, 0);
    const sizeNum = toNum(size, 20);

    return {
        skip: pageNum * sizeNum,
        take: sizeNum,
    }
}

const createHandler = (handlers: HandlerConfigGroup) => async (req: NextApiRequest, res: NextApiResponse) => {
    const { method } = req;
    const handlerConfig = handlers[method];

    if (!handlerConfig) {
        return res.status(405).json({ code: 0, message: 'Method Not Allowed' });
    }

    const {
        handler,
        authRequired,
        accessRoles,
        bodySchema,
        pageable,
        querySchema,
        requireServicePackage,
    } = handlerConfig;
    const localeDefault = req.cookies.locale
    const { t } = initResource(localeDefault);
    const session: any = await getSession({ req });
    if (authRequired) {
        if (!session) {
            return res.status(401).json({ code: 401, message: 'Unauthorized' });
        }

        const user: any = session.user;
        if (accessRoles) {
            const userRole = user?.is_admin ? -1 : user?.user_role_id;

            if (!accessRoles.includes(userRole)) {
                return res.status(403).json({ code: 401, message: 'Forbidden' });
            }
        } 

        if (requireServicePackage) {
            const userServicePackage = await getLastPackage(user.id);
            if (!userServicePackage || userServicePackage.isExpired) {
                return res.status(400).json(errorResponseDTO(400, 'User package expired or not existed!'));
            }

            if (requireServicePackage.length && !requireServicePackage.includes(userServicePackage.code)) {
                return res.status(400).json(errorResponseDTO(400, 'User package invalid!'));
            }

            session.servicePackage = userServicePackage;
        }
    }

    if (pageable && !(await validateInputData(res, paginationQuerySchema, req.query))) {
        return;
    }

    if (querySchema && !(await validateInputData(res, querySchema, req.query))) {
        return;
    }

    if (bodySchema) {
        const bodySchemaValidate = typeof bodySchema === 'function' ? bodySchema(t) : bodySchema;
        if (!(await validateInputData(res, bodySchemaValidate, req.body))) {
            return;
        }
    }

    return handler(req, res, session, paginationData(req), { t });
}

const getAuditData = (session: any, isCreate = false) => {
    const curDate = new Date();
    const user: any = session.user;

    const data: any = {
        updated_by: user.id,
        updated_date: curDate,
    }

    if (isCreate) {
        data.created_by = user.id;
        data.created_date = curDate;
    }

    return data;
}

const paginationResponse = (content: any[], total: number, paginationData: any) => {
    const { take: size, skip } = paginationData;

    return {
        content,
        total,
        totalPage: size ? Math.ceil(total / size) : 0,
        currentPage: (skip / size) + 1,
    }
}

const transformToArray = (value: any, transformFunction: any, filterFunction = Boolean) => {
    return (Array.isArray(value) ? value : [value])
        .filter(filterFunction)
        .map(transformFunction);
}

export { apiResponseDTO, errorResponseDTO, getAuditData, getYupError, paginationResponse, roleConfig, transformToArray };

export default createHandler;