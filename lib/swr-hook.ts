import { useRef } from "react";

import { useSession } from "next-auth/client";
import useSWR from "swr";

import { PACKAGES_FOR_STUDENT, paths as PATHS, USER_ROLES } from "@/interfaces/constants";
import { userInRight } from "@/utils/index";
import { paths } from "api/paths";
import { QuestionDataType, RoleDataType, UnitTestDataType, UserDataType, UserGuideDataType } from "interfaces/types";
import { useRouter } from "next/router";
import dayjs from "dayjs";


function fetcher(url: string) {
  return window.fetch(url).then((res) => res.json())
}

export function useQuestions(
  pageIndex: number,
  limit = 10,
  query: any = {},
  isMine = false,
) {
  const { data, error, mutate } = useSWR<any, any>(
    `${paths.api_questions}?page=${pageIndex}&limit=${limit}&p=${
      query?.publisher ?? ''
    }&s=${query?.series ?? ''}&g=${query?.grade ?? ''}&sk=${
      query?.skills ?? ''
    }&l=${query?.level ?? ''}&qt=${query?.question_type ?? ''}&q=${
      encodeURIComponent(query?.question_text?.trim() ?? '')
    }&m=${isMine ? 1 : 0}&cd=${query?.created_date ?? ''}`,
    fetcher,
  )
  return {
    questions: data?.data as QuestionDataType[],
    isLoading: !error && !data,
    isError: error,
    totalRecords: data?.totalRecords,
    mutateQuestions: mutate,
  }
}

export function useQuestion(id: string) {
    //TODO: improve, trick imposing no cache for a SWR call
    const random = useRef(Date.now())
    const key = [`${paths.api_questions}/${id}`, random];
  const { data, error } = useSWR(
    id !== '-1' ? key : null,
    fetcher
  )
  return {
    question: data,
    isError: error,
  }
}

export function useUsers(pageIndex: number, limit = 10, filter: any) {
  const { data, error, mutate } = useSWR<any, any>(
    `${paths.api_users}?page=${pageIndex}&limit=${limit}&name=${filter?.name}&role=${filter?.role}&status=${filter?.status}&created_date=${filter?.created_date}`,
    fetcher,
  )

  return {
    users: data?.data as UserDataType[],
    isLoading: !error && !data,
    isError: error,
    totalRecords: data?.totalRecords,
    mutateUsers: mutate,
  }
}

export function useUser(id: string) {
  const { data, error } = useSWR<UserDataType, any>(
    id !== '-1' ? `${paths.api_users}/${id}` : null,
    fetcher,
  )
  return {
    user: data,
    isError: error,
  }
}

export function useRoles() {
  const { data, error } = useSWR<RoleDataType[], any>(
    paths.api_users_roles,
    fetcher,
  )

  return {
    roles: data,
    isLoading: !error && !data,
    isError: error,
  }
}

export function useUnitTest(pageIndex: number, limit = 10) {
  const { data, error, mutate } = useSWR<any, any>(
    `${paths.api_unit_test}?page=${pageIndex}&limit=${limit}`,
    fetcher,
  )
  return {
    unitTests: data?.data as UnitTestDataType[],
    isLoading: !error && !data,
    isError: error,
    totalRecords: data?.totalRecords,
    mutateUnitTest: mutate,
  }
}

export function useUserInfo() {
  const [session] = useSession()
  const userSession: any = session?.user;

  const { data, error, mutate } = useSWR<UserDataType, any>(
    userSession ? paths.api_users_info : null,
    fetcher,
  )

  const userDisplayName = data?.full_name || userSession?.full_name || userSession?.phone || '---';

  return {
    user: data,
    isError: error,
    mutateUserInfo: mutate,
    userDisplayName,
  }
}
//  Add useGuide hook
export function useGuide(id: string) {
  const { data, error } = useSWR<UserGuideDataType, any>(
    id !== '-1' ? `${paths.api_user_guide}/${id}` : null,
    fetcher,
  )
  return {
    userGuide: data,
    isError: error,
  }
}

export function useDateNow() {
  const {data, error, mutate} = useSWR<any, any>(paths.api_date_now, fetcher)
  return {
    data, error, mutate
  }
}

const path = [PATHS.home, PATHS.userInfo, PATHS.transactionHistory]
const dateTemp = 24 * 60 * 60 * 1000
const studentDefaultPlan: any = {
  id: 0,
  code: PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE,
  name: "Phá đảo đề thi miễn phí",
  price: 0,
  isExpired: false,
  serverTime: new Date().toISOString(),
}
const dataPlanDefault: any = {
  id: 0,
  code: '',
  name: '',
  price: 0,
  isExpired: false,
  start_date: "",
  end_date: "",
  serverTime: new Date().toISOString(),
  quantity: 0,
  limit: 0,
  unit_test_types: [] as number[],
}

export function usePackageUser() {
  const [session] = useSession();
  const isStudent = userInRight([-1, USER_ROLES.Student], session);
  const isTeacher = userInRight([-1, USER_ROLES.Teacher], session);
  const isSystem = userInRight([USER_ROLES.Operator], session);

  const { push, pathname } = useRouter();

  const {
    data,
    error,
    mutate
  } = useSWR<any, any>(isStudent ? paths.api_current_combo : isTeacher ? paths.api_user_package : null, fetcher);
  let dataResult = null
  if (data?.data) {
    const dataItem = data?.data?.item;
    dataResult = { ...data?.data, ...dataItem, serverTime: data?.serverTime };

    const currentDate = dayjs(dataResult.serverTime);
    const dateExpired = Math.max(dayjs(dataResult?.end_date).diff(currentDate, "milliseconds"), 0);
    if (isTeacher && !path.includes(pathname)) {
      if (dateExpired < dateTemp && dateExpired !== 0) {
        setTimeout(() => {
          if (pathname !== "/_error") {
            push(`${PATHS.payment}?type=upgrade`);
          }
        }, dateExpired);
      }
    }

    if (dataResult.isExpired && isStudent) {
      dataResult = { ...studentDefaultPlan };
    } else {
      if (dateExpired < dateTemp && dateExpired !== 0) {
        setTimeout(() => {
          dataResult = { ...studentDefaultPlan };
          mutate()
        }, dateExpired);
      }
    }
  }else{
    dataResult = studentDefaultPlan
  }
  return {
    dataPackage: dataResult,
    mutateUserPackage: mutate,
    isLoading: !error && !data && !isSystem,
    isError: error
  };
}