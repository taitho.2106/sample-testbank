import { useState, useEffect } from "react";

function useCountDown(totalTime) {
	const [countdown, setCountDown] = useState(totalTime);
	
	useEffect(() => {
		setCountDown(totalTime);
	}, [totalTime]);
	
	useEffect(() => {
		const id = setInterval(() => {
			updateTime();
		}, 1000);
		return () => {
			clearInterval(id);
		};
	}, [countdown]);
	
	function updateTime() {
		setCountDown((countDown) => {
			const remainTime = countDown - 1000;
			if (remainTime > 0) {
				return remainTime;
			}
			return 0;
		});
	}
	
	const countdownSeconds = Math.floor(countdown / 1000);
	
	const seconds = countdownSeconds % 60;
	const hours = Math.floor(countdownSeconds / 3600);
	const minutes = Math.floor((countdownSeconds % 3600) / 60);
	
	return {
		countdown,
		hours,
		minutes,
		seconds
	};
}

export default useCountDown;
