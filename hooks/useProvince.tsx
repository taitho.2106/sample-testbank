import { paths } from "api/paths";
import { callApi } from "api/utils";
import { useEffect, useState } from "react";

const provinceDefaultOption = { code: 0, display: "Không xác định" };

const useProvinces = () => {

    const [optionProvinces, setOptionProvinces] = useState([]);

    useEffect(() => {

        const fetchData = async () =>{
            const response: any = await callApi(paths.api_get_province_list);

            setOptionProvinces([provinceDefaultOption, ...response.data.map((item: any)=>{
                return {
                    code: item.id, 
                    display: item.name
                }})])
        }

        fetchData();

    }, [])

    return { optionProvinces };
}

export default useProvinces
