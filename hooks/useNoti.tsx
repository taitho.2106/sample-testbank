import { useRef } from 'react'
import { toaster } from 'rsuite'

import { AlertNoti } from '../components/atoms/alertNoti/index'

const useNoti = (delay =  2000) : any => {
  const duration = delay
  const countNoti = useRef(0)
  const getNoti = (
    type: 'success' | 'warning' | 'error' | 'info',
    placement:
      | 'topCenter'
      | 'bottomCenter'
      | 'topStart'
      | 'topEnd'
      | 'bottomStart'
      | 'bottomEnd',
    message: any,
  ) => {
    countNoti.current +=1;
    const key = toaster.push(<AlertNoti type={type} message={message} duration={duration}/>, {
      placement,
    })
    setTimeout(() => {
      toaster.remove(key);
      countNoti.current -=1;
      if(countNoti.current <=0){
        toaster.clear();
      }
    }, duration)
  }

  return { getNoti }
}

export default useNoti
