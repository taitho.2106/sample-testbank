import { useState, useEffect } from "react";

import {SeriesStructType } from "@/interfaces/types";
import { callApi } from "api/utils";
const useSeries = () => {
    const [getSeries, setGetSeries] = useState([] as SeriesStructType[])
    useEffect(()=>{
        const fetchData = async () =>{
            const response:any = await callApi(`/api/common/series?groups=1,2,3`,'get','token')
            setGetSeries(response.data.map((item:any)=>{
                return {
                    code:item.id, 
                    display:item.name,
                    image:item.image,
                    type:item.type,
                    grade_eduhome_id : item.grade_eduhome_id,
                    grade_id : item.grade_id,
                    group: item.group
                }}))
        }
        fetchData()
    },[])
    return {getSeries}
}

export default useSeries;
