import { useState, useEffect } from "react";

import { StructType } from "@/interfaces/types";
import { callApi } from "api/utils";

const useThirdParty = () => {
    const [getThirdParty, setGetThirdParty] = useState([] as StructType[])
    useEffect(()=>{
        const fetchData = async () =>{
            const response:any = await callApi(`/api/common/third-party`,'get','token')
            setGetThirdParty(response.data.map((item:any)=>{
                return {
                    code:item.id, 
                    display:item.display
                }}))
        }
        fetchData()
    },[])
    return {getThirdParty}
}

export default useThirdParty;