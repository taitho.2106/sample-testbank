import { useContext } from "react"

import { studentPricings } from "@/componentsV2/Home/data/price"
import { AppContext } from "@/interfaces/contexts"
import { getPackageLevel } from "@/utils/user"
import { useDateNow } from "../lib/swr-hook";
import { dayjs } from "../utils/date";
const usePackages = (type = 'teacher') => {
    const { listPlans, listPlansStudent, listComboServicePackage } = useContext(AppContext);
    const { data } = useDateNow();
    const packageDescriptions = listPlans?.map(item => ({ ...studentPricings[type].find(x => x.code === item.code), ...item }))
                                  .sort((a, b) => getPackageLevel(a.code) - getPackageLevel(b.code));
    if (!!packageDescriptions.length && data) {
        const packageCombo = listComboServicePackage?.map(item => ({ ...studentPricings.teacher.find(x => x.code === item.code), ...item }))
                                                     .filter(item => dayjs(data).isBetween(item.startTime, item.endTime, "day", "[]"));
        packageDescriptions.splice(2, 0, ...packageCombo);
    }
    const packageDescriptionsForStudent = listPlansStudent.map(item => ({...studentPricings['student'].find(x => x.code === item.code), ...item}))
                                        .sort((a,b) => getPackageLevel(a.code) - getPackageLevel(b.code));

    return {
        packageDescriptions,
        packageDescriptionsForStudent,
    }
}

export default usePackages;