import { sendRequest } from 'services/api';
import { useCallback, useEffect, useState } from 'react';
import { delayResult } from '../utils';

const useFetch = (apiConfig, {
    immediate = false,
    mappingData,
    params = {},
    pathParams = {},
    delay = 0,
} = {}) => {
    const [ loading, setLoading ] = useState(false);
    const [ data, setData ] = useState(null);
    const [ error, setError ] = useState(null);

    const execute = useCallback(
        async ({ onCompleted, onError, ...payload } = {}) => {
            setLoading(true);
            setError(null);
            try {
                const { data } = await delayResult(sendRequest(apiConfig, { params, pathParams, ...payload }), delay);

                setData(mappingData ? mappingData(data) : data?.data);
                onCompleted && onCompleted(data);
                return data;
            } catch (error) {
                setError(error);
                onError && onError(error);
                return error;
            } finally {
                setLoading(false);
            }
        },
        [],
    );

    useEffect(() => {
        if (immediate) {
            execute();
        }
    }, [ execute, immediate ]);

    return { loading, execute, data, error, setData };
};

export default useFetch;
