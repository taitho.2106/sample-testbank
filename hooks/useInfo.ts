import { USER_ROLES } from "@/interfaces/constants";
import useTranslation from "./useTranslation";

const useInfo = (user:any) => {
    const { t } = useTranslation();
    const getRoleUser = () => {
        if(user?.is_admin === 1) return t('role')['admin']

        switch (user?.user_role_id){
            case USER_ROLES.Operator: return t('role')['operator'];
            case USER_ROLES.Teacher: return t('role')['teacher'];
            case USER_ROLES.Student: return t('role')['student'];
            default: return t('role')['sale']
        }
    }

    return {
        getRoleUser
    }
};

export default useInfo;