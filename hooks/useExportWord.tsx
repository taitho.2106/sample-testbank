import { paths } from "api/paths"
import api from "lib/api"

import useNoti from "./useNoti"
import useTranslation from "./useTranslation"

const useExportWord = () => {
    const { t } = useTranslation()
    const { getNoti } = useNoti()

    const downloadFile = async (index: any) => {
        const url = `${paths.api_unit_test_export}/download?id=${index}`
        window.open(url) || window.location.assign(url)
    }

    const handleExportWord = async (index: any) => {
        const response = await api.get(
            `${paths.api_unit_test_export}/create?id=${index}`,
        )
        if (response) {
            const dataRes: any = response?.data
            if (response.status == 200) {
                if (dataRes) {
                    if (dataRes.code == 1) {
                        downloadFile(dataRes.data)
                        getNoti('success', 'topCenter', t("common-get-noti")["export-word-succeed"])
                    } else {
                        getNoti('error', 'topCenter', t("common-get-noti")[dataRes.message])
                    }
                }
            } else {
                getNoti('error', 'topCenter', t("common-get-noti")[dataRes.message])
            }
        }
    }

    return { handleExportWord }
}

export default useExportWord