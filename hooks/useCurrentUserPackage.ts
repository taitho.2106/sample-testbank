import { useContext } from "react";

import dayjs from "dayjs";
import { useSession } from "next-auth/client";

import {
    PACKAGE_LEVEL_CONFIG,
    PACKAGE_LEVEL_FOR_STUDENT_CONFIG,
    PACKAGES, PACKAGES_FOR_STUDENT,
    USER_ROLES
} from "@/interfaces/constants";
import { AppContext } from "@/interfaces/contexts";
import { getUpgradeAblePackageLevels } from "@/utils/user";
import { usePackageUser } from "lib/swr-hook";

import { userInRight } from "../utils";
import useTranslation from "./useTranslation";

const useCurrentUserPackage = () => {
    const { listPlans, listPlansStudent } = useContext(AppContext);
    const [session] = useSession();
    const isSystem = userInRight([USER_ROLES.Operator], session)
    const isStudent = userInRight([-1, USER_ROLES.Student], session)
    const { dataPackage, isLoading } = usePackageUser();
    const { t } = useTranslation()

    let isExpired = !isSystem;

    let namePackage = {
        code: PACKAGES.BASIC.CODE,
        name: PACKAGES.BASIC.NAME
    }

    let listPackage = listPlans || []
    let arrConfig = PACKAGE_LEVEL_CONFIG


    // const isHighestPackage = getUpgradeAblePackageLevels(namePackage?.code, arrConfig).length === 0;

    const getPackageByCode = (code: string) => {
        return listPackage?.find((item: any) => item.code === code) || {};
    }

    const getUpgradeAblePackages = () => {
        const upgradeAblePackages = getUpgradeAblePackageLevels(namePackage?.code, arrConfig)
        return listPackage.filter((item: any) => upgradeAblePackages.includes(item.code));
    };

    if (!isLoading) {
        const currentDate = new Date(dataPackage.serverTime)
        const countDate = Math.max(dayjs(dataPackage?.end_date).diff(currentDate, "days"), 0)
        const countHour = Math.max(dayjs(dataPackage?.end_date).diff(currentDate, 'hours') % 24, 1)
        namePackage = dataPackage;
        isExpired = dataPackage.isExpired
        const isFree = dataPackage.code === PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE
        const text = [countDate && `${countDate} ${t('package')['days']}`,countHour && `${countHour} ${t('package')['hours']}`].filter(Boolean).join(' ')
        const textTimeLeft = isExpired ? `${t('package')['isExpired']}` : isFree ? `${t('package')['free']}` : text
        const isHighestPackage = getUpgradeAblePackageLevels(namePackage?.code, arrConfig).length === 0;

        listPackage = isStudent ? listPlansStudent.filter((item:any) => item.code !== PACKAGES_FOR_STUDENT.BEAT_FREE_EXAM.CODE) : listPlans
        arrConfig = isStudent ? PACKAGE_LEVEL_FOR_STUDENT_CONFIG : PACKAGE_LEVEL_CONFIG
        const getPackageByCode = (code: string) => {
            return listPackage?.find((item: any) => item.code === code) || {};
        }

        const getUpgradeAblePackages = () => {
            const upgradeAblePackages = getUpgradeAblePackageLevels(dataPackage?.code, arrConfig)
            return listPackage.filter((item: any) => upgradeAblePackages.includes(item.code));
        };
        return {
            namePackage,
            isExpired,
            dataPlan: dataPackage,
            listPlans,
            isLoading,
            isHighestPackage,
            getPackageByCode,
            getUpgradeAblePackages,
            textTimeLeft,
            listPlansStudent,
            listPackage,
        };
    }
    return {
        namePackage,
        listPlans,
        isExpired,
        listPlansStudent,
        getPackageByCode,
        getUpgradeAblePackages,
        listPackage,
        isLoading,
    }
}

export default useCurrentUserPackage