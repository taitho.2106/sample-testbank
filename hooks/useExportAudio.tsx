import api from 'lib/api'

import useNoti from './useNoti'
import useTranslation from './useTranslation'

const useExportAudio = () => {
    const { t } = useTranslation()
    const { getNoti } = useNoti()

    const handleExportAudio = async (index: any) => {
        const response = await api.get(
            `/api/unit-test/export-only-audio?id=${index}`,
        )
        if (response) {
            const dataRes: any = response?.data

            if (response.status == 200) {
                if (dataRes) {
                    const url = `/api/unit-test/export-only-audio?id=${index}`
                    window.open(url) || window.location.assign(url)
                    getNoti('success', 'topCenter', t("common-get-noti")["export-audio-succeed"])
                }
            } else {
                getNoti('error', 'topCenter', t("common-get-noti")[dataRes.message])
            }
        }
    }

    return { handleExportAudio }
}

export default useExportAudio