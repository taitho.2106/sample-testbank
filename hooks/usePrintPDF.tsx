import { useState, useRef, useCallback } from 'react';

import { useReactToPrint } from "react-to-print";

import { paths } from 'api/paths';
import { callApi } from 'api/utils';
import { TemplateExportPDF } from 'components/molecules/unitTestTable/TemplateExportPDF';

export const usePrintPDF = () => {
    const [dataUnitTest, setDataUnitTest] = useState(null);
    const printRef = useRef(null);
    const overlayBackground = document.getElementById('background-print-pdf')

    const getUnitTestDetail = useCallback(async (id: number) => {
        try {
            const response: any = await callApi(`${paths.api_unit_test}/${id}`);
            if (response.status === 200) {
                setDataUnitTest(response.data);
            }
        } catch (error) {
            console.log('Error fetching unit test details PDF', { error })
        }
    }, []);

    const contentPrint = useCallback(() => printRef.current, []);

    const handlePrint = useReactToPrint({
        content: contentPrint,
        removeAfterPrint: true,
        onBeforePrint: () => {
            overlayBackground.style.display = 'block';
        },
        onAfterPrint: () => {
            overlayBackground.style.display = 'none';
        },
    });

    const handlePrintUnitTest = useCallback(async (idUnitTest: number) => {
        await getUnitTestDetail(idUnitTest);
        handlePrint?.();
    }, [
        getUnitTestDetail,
        handlePrint
    ]);

    const printPDFComponents = (
        <div style={{ display: 'none' }}>
            <TemplateExportPDF ref={printRef} data={dataUnitTest} />
        </div>
    );

    return {
        handlePrintUnitTest,
        printPDFComponents
    };
}