import Excel from 'exceljs';

import { callApi } from 'api/utils'

const useExportExcelListIDs = () => {

    const getURL = (user: any, id: number) => {
        const userId = user?.id
        const protocol = window.location.protocol
        const hostname = window.location.hostname
        const port = hostname === 'localhost' ? `:${window.location.port}` : ''
        const url = `${protocol}//${hostname}${port}/practice-test/${userId}===${id}`
        return url
    }

    const handleExportExcel = async (user: any, id: number) => {
        const response: any = await callApi(`/api/unit-question/${id}`, 'get')
        if (response) {
            const unitTestName = response?.name
            const sections = response?.sections
            const linkURL = getURL(user, id)

            const rowArrayTitle = [
                { code: 'A', key: 'A', display: 'Tên đề thi' },
                { code: 'B', key: 'B', display: 'Link đề thi' },
            ]

            const result = sections.map((item: any) => ({
                partName: item.name,
                list: item.list.map((listItem: any) => listItem.id),
            }));

            const cellObject = result.map((item: any, index: number) => ({
                code: String.fromCharCode(67 + index), // Start from 'C'
                key: item.partName,
                display: item.partName,
            }));

            const workbook = new Excel.Workbook()
            const worksheet = workbook.addWorksheet('Sheet1')
            const tableObj = cellObject
            const columns = []
            const totalColumns = tableObj.length

            for (const element of rowArrayTitle) {
                const itemResult = element
                columns.push({
                    header: itemResult.display,
                    key: itemResult.key,
                    width: 30
                })
                const cell = worksheet.getCell(`${itemResult.code}1`)
                cell.font = {
                    size: 11,
                    bold: true,
                    color: { argb: 'FF9900' },
                }
                cell.alignment = {
                    vertical: 'middle',
                    horizontal: 'left',
                    wrapText: true,
                }
            }

            for (let i = 0; i < totalColumns; i++) {
                const itemCell = tableObj[i]
                columns.push({
                    header: itemCell.display,
                    key: itemCell.key,
                    width: 30
                })
                const cell = worksheet.getCell(`${itemCell.code}1`)
                cell.fill = {
                    type: 'pattern',
                    bgColor: {
                        argb: '34A853',
                    },
                    pattern: 'solid',
                    fgColor: { argb: '34A853' },
                }
                cell.font = {
                    size: 11,
                    bold: true,
                    color: { argb: 'FFFFFF' },
                }
                cell.alignment = {
                    vertical: 'middle',
                    horizontal: 'left',
                    wrapText: true,
                }
                cell.border = {
                    top: { style: 'thin', color: { argb: '79C440' } },
                    left: { style: 'thin', color: { argb: '79C440' } },
                    bottom: { style: 'thin', color: { argb: '79C440' } },
                    right: { style: 'thin', color: { argb: '79C440' } },
                }
            }
            worksheet.columns = columns
            worksheet.getColumn(tableObj.length).style.alignment = {
                vertical: 'middle',
                horizontal: 'left',
                wrapText: true,
            }
            worksheet.getColumn(tableObj.length).style.font = {
                size: 11,
            }

            const rowA2 = worksheet.getRow(2);
            const cellA2 = rowA2.getCell(1);
            cellA2.value = unitTestName;
            cellA2.font = {
                size: 11,
                color: { argb: '000000' },
            }
            cellA2.alignment = {
                vertical: 'middle',
                horizontal: 'left',
                wrapText: true,
            }

            const rowB2 = worksheet.getRow(2);
            const cellB2 = rowB2.getCell(2);
            cellB2.value = {
                text: linkURL,
                hyperlink: linkURL
            };
            cellB2.font = {
                size: 11,
                underline: true,
                color: { argb: 'FF0000FF' },
            }

            const totalRows = Math.max(...result.map((item: any) => item.list.length));
            for (let rowNum = 2; rowNum <= totalRows + 1; rowNum++) {
                const row = worksheet.getRow(rowNum);
                for (let colNum = 3; colNum <= totalColumns + 2; colNum++) {
                    const colKey = tableObj[colNum - 3].key;
                    const cell = row.getCell(colNum);
                    const matchingPart = result.find((item: any) => item.partName === colKey);
                    if (matchingPart) {
                        const ids = matchingPart.list;
                        cell.value = ids[rowNum - 2];
                        cell.alignment = {
                            vertical: 'middle',
                            horizontal: 'left',
                        }
                    }
                }
            }

            const buffer = await workbook.xlsx.writeBuffer()
            const url = URL.createObjectURL(
                new Blob([buffer], {
                    type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
                }),
            )
            const link = document.createElement('a')
            link.setAttribute("download", `${unitTestName} - List of Question IDs.xlsx`);
            link.href = url
            link.click()
        } else {
            console.error('Error exporting Excel');
        }
    }
    return { handleExportExcel }
}

export default useExportExcelListIDs