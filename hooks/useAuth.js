import { useSession } from 'next-auth/client';
import { USER_ROLES } from 'interfaces/constants';

const useAuth = () => {
    const [session] = useSession();
    const user = session?.user;

    const isSystem = user?.is_admin === 1 || user?.user_role_id == USER_ROLES.Operator;

    return { user, isSystem };
};

export default useAuth;