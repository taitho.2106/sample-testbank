import ga from "@/utils/ga";
import { useRouter } from "next/router";
import { useEffect } from "react";

const usePageTracking = (): any => {
    const router = useRouter();

    useEffect(() => {
        const handleRouteChange = (url: string) => {
            ga.pageView(url);
        }
        router.events.on('routeChangeComplete', handleRouteChange)

        return () => {
            router.events.off('routeChangeComplete', handleRouteChange)
        }
    }, [router.events])

    return null;
};

export default usePageTracking;