import { useMemo } from "react"

import { useRouter } from "next/router"

import enWords from '../languages/en.json'
import viWords from '../languages/vi.json'

const resources: any = {
    en: enWords,
    vi: viWords
}

const getResource = (locale: string) => {
    const resourcesSelected = resources[locale];
    return resourcesSelected || viWords;
}

const formatMessage = (resourcesSelected:any, key: string, options?: string[], defaultMessage?: string) => {
    let translatedValue = resourcesSelected?.[key]
        ? resourcesSelected[key]
        // eslint-disable-next-line @typescript-eslint/ban-types
        : (viWords[key as keyof Object] || defaultMessage || key); //fallback lang viWords

    if (options?.length) {
        options.map((item, index) => {
            translatedValue = translatedValue.replace(`{${index}}`, item);
        });
    }

    return translatedValue;
}

const useTranslation = () => {
    const { push, locale, asPath, defaultLocale, replace } = useRouter()

    const translationList: any = useMemo(() => {
        return getResource(locale);
    }, [ locale ])

    const t = (key: string, options?: string[], defaultMessage?: string) => {
        return formatMessage(translationList, key, options, defaultMessage);
    }

    const switchLocale = (locale: string) => {
        replace(asPath, asPath, { locale })
    }

    const subPath = locale === defaultLocale ? '' : `/${locale}`
    return { locale, t, switchLocale, subPath }
}

const initResource = (locale = 'en') => {
    const resourcesSelected = getResource(locale);
    const t = (key: string, options?: string[], defaultMessage?: string) => {
        return formatMessage(resourcesSelected, key, options, defaultMessage);
    }

    return { t };
}

export default useTranslation

export {
    initResource
}
