import { useState, useEffect, useMemo } from "react";

import { GradeStructType } from "@/interfaces/types";
import { callApi } from "api/utils";


const useGrade = () => {
    const [getGrade, setGetGrade] = useState([] as GradeStructType[])

    const gradeList = useMemo(() => getGrade.filter(item => /^G[0-9]{1,2}$/.test(`${item.code}`)) || [], [ getGrade ]);

    useEffect(()=>{
        const fetchData = async () =>{
            const response:any = await callApi(`/api/common/grade`,'get','token')

            setGetGrade(response.data.map((item:any)=>{
                return {
                    code:item.id, 
                    display:item.name,
                    grade_eduhome_id:item.grade_eduhome_id
                }}))
        }
        fetchData()
    },[])
    return { getGrade, gradeList };
}

export default useGrade;
