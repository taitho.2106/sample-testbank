const shortClassUnitTestSelect = {
    id: true,
    start_date: true,
    end_date: true,
    unit_type: true,
    unitTest: {
        select: {
            id: true,
            template_id: true,
            name: true,
            template_level_id: true,
            series_id: true,
            total_question: true,
            total_point: true,
            time: true,
            unit_type: true,
            unit_test_origin_id: true,
            series_old_id: true,
            unit_test_type: true,
            is_international_exam: true,
            package_level: true,
        }
    },
}

const classUnitTestSelect = {
    ...shortClassUnitTestSelect,
    class: {
        select: {
            id: true,
            name: true,
        },
    },
    group: {
        select: {
            id: true,
            name: true,
        },
    },
    student: {
        select: {
            id: true,
            full_name: true,
            phone: true,
        }
    }
}

export { classUnitTestSelect, shortClassUnitTestSelect };