const studentDTO: any = {
    id: true,
    user_name: true,
    full_name: true,
    email: true,
    address: true,
    avatar: true,
    created_date: true,
    phone: true,
    birthday: true,
    gender: true,
}

export default studentDTO;