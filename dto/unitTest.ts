import { STATUS_CODE_EXAM } from "@/interfaces/constants";
import { dayjs, getDateStatusFromStartEnd } from "@/utils/date";

const mapClassUnitTestToDTO = (classUnitTest: any, currentDate: any) => {
    const examStatus = getDateStatusFromStartEnd(classUnitTest.start_date, classUnitTest.end_date);
    const result = {
        ...classUnitTest,
        examStatus,
    }

    if (examStatus === STATUS_CODE_EXAM.ONGOING) {
        result.timeout = dayjs(result.end_date).diff(currentDate);
    }

    if (examStatus === STATUS_CODE_EXAM.UPCOMING) {
        result.timeout = dayjs(result.start_date).diff(currentDate);
    }

    return result;
}

const mapListClassUnitTestToDTO = (listClassUnitTest: any[]) => {
    const currentDate = dayjs();
    return listClassUnitTest.map(unitTest => mapClassUnitTestToDTO(unitTest, currentDate))
}

export { mapClassUnitTestToDTO, mapListClassUnitTestToDTO }